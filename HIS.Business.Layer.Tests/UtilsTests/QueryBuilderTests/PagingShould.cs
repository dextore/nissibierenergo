﻿using NUnit.Framework;
using HIS.Business.Layer.Utils.QueryBuilder;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;

namespace HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests
{
    [TestFixture]
    public class PagingShould
    {
        public IPaging Initialize(int entityOnPage, int currentPage)
        {
            return new Paging(entityOnPage, currentPage);
        }

        [TestCase(12,1, "OFFSET 0 ROWS\nFETCH NEXT 12 ROWS ONLY\n")]
        [TestCase(10, 2, "OFFSET 10 ROWS\nFETCH NEXT 10 ROWS ONLY\n")]
        [TestCase(10, 3, "OFFSET 20 ROWS\nFETCH NEXT 10 ROWS ONLY\n")]
        public void when_paging_FormatPaggingQuery_withParams_shouldReturn_result(int entityOnPage, int currentPage, string result)
        {
            var paging = Initialize(entityOnPage, currentPage);
            var actual = paging.FormatPaggingQuery();

            Assert.That(result, Is.EqualTo(actual));
        }
    }
}

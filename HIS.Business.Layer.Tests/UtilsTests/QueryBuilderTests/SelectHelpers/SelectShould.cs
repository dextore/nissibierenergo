﻿using System.Collections.Generic;
using HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests.InfrostructureClasses;
using HIS.Business.Layer.Utils.QueryBuilder;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.DAL.DB.Handlers.QueryBuilder;
using NUnit.Framework;

namespace HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests.SelectHelpers
{
    [TestFixture]
    public class SelectShould : SelectShouldBase
    {
        public override void Intialize()
        {
            _queryBuilderHandler = SelectHelpersInit.QueryBuilderHandlerStubInitialize();
            StubForEntity(40);
        }

        [Test]
        public void when_select_getfields_should_return_collection()
        {
            ISelect select = new Select(40, _queryBuilderHandler);
            
            var actual = select.GetFields();
            var expected = new List<CaptionAndAliase>()
            {
                new CaptionAndAliase("Наименование", "[Contact//Name]"),
                new CaptionAndAliase("Контактные телефоны", "[Contact//PhoneList]"),
                new CaptionAndAliase("E-mail", "[Contact//EmailList]"),
                new CaptionAndAliase("Тип контакта", "[Contact//ContactType]"),
            };

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void select_get_columns_from_implaments_table()
        {
            ISelect select = new Select(40, _queryBuilderHandler);
            
            
            var actual = select.GetFieldsFromImplamentTypeForEntity();

            CollectionAssert.AreEqual(new List<string>
            {
                "BAId",
                "ContactTypeId",
                "Note"
            },actual);
        }

        [TestCase("SELECT\n\t[Contact].Note AS [Contact//Name],\n\t[AMIV21].Value AS [Contact//PhoneList],\n\t[AMIV22].Value AS [Contact//EmailList],\n\t[Contact].ContactTypeId AS [Contact//ContactType],\n\t[Contact].BAId AS [Contact//BAId]\n", 
            40)]
        [TestCase(@"SELECT
	[MV1].Value AS [Contract//Name],
	[MVP6].Value AS [Contract//Supplier],
	[Contract].StartDate AS [Contract//StartDate],
	[Contract].EndDate AS [Contract//EndDate],
	[MVP16].Value AS [Contract//LegalSubject],
	[MVP200].Value AS [Contract//ContractActivityScheme],
	[MVP201].Value AS [Contract//ContractStateScheme],
	[MVP202].Value AS [Contract//ContractValidateScheme],
	[MVP203].Value AS [Contract//ContractCalcScheme],
	[MVP204].Value AS [Contract//ContractFinalPaymentScheme],
	[MVP205].Value AS [Contract//ContractAdvanceScheme],
	[MV10210].Value AS [Contract//LS_test],
	[Contract].BAId AS [Contract//BAId],
	[Contract].IsProject AS [Contract//IsProject],
	[Contract].Number AS [Contract//Number]
", 45)]
        public void when_select_make_format_query_should(string expected, int baType)
        {
            ISelect select = new Select(baType, _queryBuilderHandler);
            
            var actual = select.FormatSelectQuery();

            Assert.AreEqual(expected, actual);
        }
    }

    [TestFixture]
    public class FieldGeneratorShould : SelectShouldBase
    {
        public override void Intialize()
        {
            _queryBuilderHandler = SelectHelpersInit.QueryBuilderHandlerStubInitialize();
            StubForEntity(40);
        }

        [Test]
        public void delay_raz()
        {
            //var fg = new FieldGenerator(40, _queryBuilderHandler);
            //fg.GenerateFieldDictionary();
            //Assert.Pass();
            var qbh = new QueryBuilderHandler();
            qbh.GetColumnsNameByImplamentTypeName("CRContacts");
            Assert.Pass();
        }
    }
}

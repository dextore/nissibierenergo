﻿using NUnit.Framework;
using System.Linq;
using HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests.InfrostructureClasses;
using HIS.Business.Layer.Utils.QueryBuilder;
using HIS.Business.Layer.Utils.QueryBuilder.Models;


namespace HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests.SelectHelpers
{
    [TestFixture]
    public class AliasesFormaterShould : SelectShouldBase
    {
        public override void Intialize()
        {
            _queryBuilderHandler = SelectHelpersInit.QueryBuilderHandlerStubInitialize();
            _aliasesFormater = new AliasFormater();
            StubForEntity(40);
        }

        [Test]
        public void aliaseformater_parse_should()
        {
            var captionAndAliase = _aliasesFormater.FormatMarkersAliases(_stubEntity, _stubMarkers);

            var parsed = _aliasesFormater.Parse(captionAndAliase);
            bool actual = parsed.All(x => x.MvcAliaseEntity == "Contact") &&
                          parsed.Any(x => x.MvcAliaseMarker == "EmailList");

            Assert.IsTrue(actual);
        }

        [TestCase("Наименование", "[Contact//Name]")]
        [TestCase("E-mail", "[Contact//EmailList]")]
        [TestCase("Тип контакта", "[Contact//ContactType]")]
        [TestCase("Контактные телефоны", "[Contact//PhoneList]")]
        public void when_aliasFormater_formatMarkersAliases_should_return_collection_with(string caption, string alias)
        {
            var expected = new CaptionAndAliase(caption, alias);
            
            var captionAndAliase = _aliasesFormater.FormatMarkersAliases(_stubEntity, _stubMarkers);

            CollectionAssert.Contains(captionAndAliase,expected);
        }

        
    }
    
    
}

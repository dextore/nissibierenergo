﻿using System;
using System.Threading.Tasks;
using HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests.InfrostructureClasses;
using HIS.Business.Layer.Utils.QueryBuilder;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using NUnit.Framework;

namespace HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests
{
    [TestFixture]
    public class OrderingShould
    {
        
        [TestCase("name", SortingOrder.Asc, "name Asc\n")]
        [TestCase("qwerty", SortingOrder.Desc, "qwerty Desc\n")]
        public async Task when_ordering_FormatOrderingQuery_withParams_should_return_result(string orderingField, SortingOrder sortingOrder, string result)
        {
            var ordering = OrderingTestHelper.Initialize(orderingField, sortingOrder); 
            
          //  Assert.That(await ordering.FormatItemString(), Is.EqualTo(result));
        }
        
        [TestCase("", SortingOrder.Desc)]
        [TestCase("", SortingOrder.Asc)]
        public void when_ordering_FormatOrderingQuery_with_wrong_Params_should_throw(string orderingField, SortingOrder sortingOrder)
        {
            Assert.That(() => new Ordering(orderingField, sortingOrder), Throws.TypeOf<ArgumentException>()
                                                                               .With
                                                                               .Message
                                                                               .EqualTo("Ordering field cant't be Empty"));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests.InfrostructureClasses;
using NUnit.Framework;
using HIS.Business.Layer.Utils.QueryBuilder;
using HIS.Business.Layer.Utils.QueryBuilder.Exceptions;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;
using HIS.Business.Layer.Utils.QueryBuilder.Models;

namespace HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests
{
    public enum ActionType
    {
        Paging,
        Ordering,
        Filtering
    }

    [TestFixture]
    public class QueryBuilderShould : SelectShouldBase
    {
        public IQueryBuilder InitializeQueryBuilder()
        {
            return null;// new QueryBuilder(null, _queryBuilderHandler);
        }

        [TestCase(true, ActionType.Paging, true)]
        [TestCase(false, ActionType.Paging, false)]
        [TestCase(true, ActionType.Ordering, true)]
        [TestCase(false, ActionType.Ordering, false)]
        [TestCase(true, ActionType.Filtering, true)]
        [TestCase(false, ActionType.Filtering, false)]
        public void when_queryBuilder_addSomeOne_someOne_should_be_created(bool whantCreate, ActionType actionType, bool result)
        {
            var qb = InitializeQueryBuilder();
            bool switchCheckresult = false;

            if (whantCreate)
                switchCheckresult = CheckThisBuilder(qb, actionType);

            Assert.That(switchCheckresult, Is.EqualTo(result));
        }

        [Test]
        public void when_queryBuilder_tryed_addPaging_but_does_not_add_ordering_before_should_throw()
        {
            var qb = InitializeQueryBuilder();
            Assert.That(() => qb.AddPaging(12, 1), Throws.TypeOf<OrderingMustBeBeforePaging>()
                                                          .With
                                                          .Message
                                                          .EqualTo("Paging can't be without Ordering"));
        }

        [Test]
        public void when_queryBuilder_tryed_double_AddPaging_should_throw()
        {
            var qb = InitializeQueryBuilder();
            Assert.That(() => qb.AddOrdering("test", SortingOrder.Desc)
                                .AddPaging(12,2)
                                .AddFiltering("field",FiltersOperator.Equal, "sd")
                                .AddPaging(12,3), Throws.TypeOf<PaggingAlreadyAdd>()
                                                        .With
                                                        .Message
                                                        .EqualTo("Paging can't be add when it is"));
        }

        [Test]
        public void when_queryBuilder_addingTwoOrdering_and_addingPaging_qb_should()
        {
            var qb = InitializeQueryBuilder();
            qb.AddOrdering("testField", SortingOrder.Desc)
                .AddOrdering("testTwoField", SortingOrder.Asc)
                .AddPaging(10, 2)
                .BuildQuery();


            Assert.AreEqual("ORDER BY testField Desc,\ntestTwoField Asc\nOFFSET 10 ROWS\nFETCH NEXT 10 ROWS ONLY\n", qb.SqlQuery);
        }

        [Test]
        public void when_queryBuilder_addThreeFilters_then_addingTwoOrdering_then_addPaging_qb_should()
        {
            var qb = InitializeQueryBuilder();
            qb.AddFiltering("name", FiltersOperator.Less, "stringTest")
                .AddFiltering("date", FiltersOperator.Equal, new DateTime(2017, 02, 12))
                .AddFiltering("number", FiltersOperator.LessEqual, 148)
                .AddOrdering("name", SortingOrder.Desc)
                .AddOrdering("number", SortingOrder.Asc)
                .AddPaging(15, 1)
                .BuildQuery();

            Assert.AreEqual("WHERE name < 'stringTest' And date = {d'2017-02-12'} And number <= 148 \nORDER BY name Desc,\nnumber Asc\nOFFSET 0 ROWS\nFETCH NEXT 15 ROWS ONLY\n",
                qb.SqlQuery);
        }

        [Test]
        public void when_queryBuilder_GetCaptionsAndAliases_without_call_select_before_should_throw()
        {
            var qb = InitializeQueryBuilder();

            Assert.That(() => qb.GetCaptionsAndAndAliases(), Throws.TypeOf<SelectMustAddfFirst>()
                                                                   .With
                                                                   .Message
                                                                   .EqualTo("Need create Select before get captions!"));
        }

        [Test]
        public void when_queryBuilder_Select_then_GetCaptionsAndAndAliases_should_return_aliasesAndCaptions()
        {
            var qb = InitializeQueryBuilder();
            var actual = qb.Select(40)
                           .Play()
                           .GetCaptionsAndAndAliases();

            var expected = new List<CaptionAndAliase>()
            {
                new CaptionAndAliase("Наименование", "[Contact//Name]"),
                new CaptionAndAliase("Контактные телефоны", "[Contact//PhoneList]"),
                new CaptionAndAliase("E-mail", "[Contact//EmailList]"),
                new CaptionAndAliase("Тип контакта", "[Contact//ContactType]")
            };

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void Complete_test()
        {
            var qb = InitializeQueryBuilder();
            var finalQuery = qb.Select(40)
                .From(40)
                .AddOrdering("[Contact//PhoneList]", SortingOrder.Desc)
                .AddFiltering("[Contact//ContactType]", FiltersOperator.Equal, "1")
                .AddPaging(2, 1)
                .BuildQuery();
            Assert.AreEqual("SELECT\n\t[Contact].Note AS [Contact//Name],\n\t[AMIV21].Value AS [Contact//PhoneList]," +
                            "\n\t[AMIV22].Value AS [Contact//EmailList],\n\t[Contact].ContactTypeId AS [Contact//ContactType]," +
                            "\n\t[Contact].BAId AS [Contact//BAId]\nFROM CRContacts AS [Contact]\nLEFT JOIN [BABaseAncestors] AS B" +
                            "\n\tON B.BAId = [Contact].BAId\nLEFT JOIN [MABaseAncestorMarkerValues] AS [MV1]\n\tON [MV1].BAId = B.BAId" +
                            "\n\tAND [MV1].MarkerId = 1\nLEFT JOIN [MABaseAncestorMarkerItemValues] AS [AMIV21]\n\t" +
                            "ON [AMIV21].BAId = B.BAId\n\tAND [AMIV21].MarkerId = 21\nLEFT JOIN [MABaseAncestorMarkerItemValues]" +
                            " AS [AMIV22]\n\tON [AMIV22].BAId = B.BAId\n\tAND [AMIV22].MarkerId = 22\nLEFT JOIN [MABaseAncestorMarkerValues]" +
                            " AS [MV24]\n\tON [MV24].BAId = B.BAId\n\tAND [MV24].MarkerId = 24\nWHERE [Contact].ContactTypeId = '1'" +
                            "\nORDER BY [Contact//PhoneList] Desc\nOFFSET 0 ROWS\nFETCH NEXT 2 ROWS ONLY\n"
                , finalQuery.Result);
        }

        [Test]
        public void Complete_test_secondWhere()
        {
            var qb = InitializeQueryBuilder();
            var finalQuery = qb.Select(40)
                .From(40)
                .AddOrdering("[Contact//PhoneList]", SortingOrder.Desc)
                .AddFiltering("[Contact//ContactType]", FiltersOperator.Equal, "1")
                .AddFiltering("[Contact//PhoneList]", FiltersOperator.NoEqual, null)
                .AddPaging(2, 1)
                .BuildQuery();
            var temp = finalQuery.Result;
            Assert.AreEqual("SELECT\n\t[Contact].Note AS [Contact//Name],\n\t[AMIV21].Value AS [Contact//PhoneList]," +
                            "\n\t[AMIV22].Value AS [Contact//EmailList],\n\t[Contact].ContactTypeId AS [Contact//ContactType]," +
                            "\n\t[Contact].BAId AS [Contact//BAId]\nFROM CRContacts AS [Contact]\nLEFT JOIN [BABaseAncestors] AS B" +
                            "\n\tON B.BAId = [Contact].BAId\nLEFT JOIN [MABaseAncestorMarkerValues] AS [MV1]\n\tON [MV1].BAId = B.BAId" +
                            "\n\tAND [MV1].MarkerId = 1\nLEFT JOIN [MABaseAncestorMarkerItemValues] AS [AMIV21]\n\tON [AMIV21].BAId = B.BAId" +
                            "\n\tAND [AMIV21].MarkerId = 21\nLEFT JOIN [MABaseAncestorMarkerItemValues] AS [AMIV22]\n\tON [AMIV22].BAId = B.BAId" +
                            "\n\tAND [AMIV22].MarkerId = 22\nLEFT JOIN [MABaseAncestorMarkerValues] AS [MV24]\n\tON [MV24].BAId = B.BAId" +
                            "\n\tAND [MV24].MarkerId = 24\nWHERE [Contact].ContactTypeId = '1' And [AMIV21].Value != ''" +
                            " \nORDER BY [Contact//PhoneList] Desc\nOFFSET 0 ROWS\nFETCH NEXT 2 ROWS ONLY\n"
                , finalQuery.Result);
        }

        #region UtilsMethods
        public bool CheckThisBuilder(IQueryBuilder builder, ActionType actionType)
        {
            bool result = false;
            switch (actionType)
            {
                case ActionType.Paging:
                    {
                        builder.AddOrdering("stub", SortingOrder.Asc)
                            .AddPaging(12, 1)
                            .BuildQuery();
                        result = builder.IsPagging;
                        break;
                    }
                case ActionType.Ordering:
                    {
                        builder.AddOrdering("test", SortingOrder.Asc)
                            .BuildQuery();
                        result = builder.IsOrdering;
                        break;
                    }
                case ActionType.Filtering:
                    {
                        builder.AddFiltering("test", FiltersOperator.Less, "value")
                            .BuildQuery();
                        result = builder.IsFiltering;
                        break;
                    }
            }

            return result;
        }

        #endregion


        public override void Intialize()
        {
            _queryBuilderHandler = SelectHelpersInit.QueryBuilderHandlerStubInitialize();
        }
    }
}

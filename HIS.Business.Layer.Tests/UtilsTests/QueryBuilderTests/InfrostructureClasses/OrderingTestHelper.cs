﻿using HIS.Business.Layer.Utils.QueryBuilder;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;
using HIS.Business.Layer.Utils.QueryBuilder.Models;

namespace HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests.InfrostructureClasses
{
    public static class OrderingTestHelper
    {
        public static IGroupMember Initialize(string orderingField, SortingOrder sortingOrder)
        {
            return new Ordering(orderingField, sortingOrder);
        }

        public static Group<IGroupMember> InitializeOrderingGroup()
        {
            return new OrderingGroup(QuerySection.OrderBy);
        }
    }
}

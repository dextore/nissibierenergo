﻿using System.Collections.Generic;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces.SelectHelper;
using HIS.DAL.Client.Models.QueryBuilder;
using HIS.DAL.DB.Handlers.QueryBuilder;
using NUnit.Framework;

namespace HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests.InfrostructureClasses
{
    public abstract class SelectShouldBase
    {
        protected IQueryBuilderHandler _queryBuilderHandler;
        protected IAliasesFormater _aliasesFormater;
        protected EntityQueryBuilderInfo _stubEntity;
        protected IEnumerable<MarkersQueryBuilder> _stubMarkers;
        public void StubForEntity(long baid)
        {
            _stubEntity = _queryBuilderHandler.GetEntityQueryBuilderInfo(baid);
            _stubMarkers = _queryBuilderHandler.GetMarkersForBaTypeId(baid);
        }

        [SetUp]
        public abstract void Intialize();
    }
}

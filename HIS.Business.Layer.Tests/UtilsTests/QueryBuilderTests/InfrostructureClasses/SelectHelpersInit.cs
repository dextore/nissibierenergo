﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.QueryBuilder;
using HIS.DAL.DB.Handlers.QueryBuilder;
using NSubstitute;
using NSubstitute.Core.Arguments;

namespace HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests.InfrostructureClasses
{
    public static class SelectHelpersInit
    {
        public static IQueryBuilderHandler QueryBuilderHandlerStubInitialize()
        {
            IQueryBuilderHandler queryBuilderHandler = Substitute.For<IQueryBuilderHandler>();

            #region Сущности для рекурсии от 45(Договор)

            queryBuilderHandler.GetEntityQueryBuilderInfo(Arg.Is<long>(45)).Returns(new EntityQueryBuilderInfo
            {
                TypeId = 45,
                Name = "Договор",
                ImplementTypeName = "CAContracts",
                MvcAlias = "Contract"
            });
            queryBuilderHandler.GetEntityQueryBuilderInfo(Arg.Is<long>(13)).Returns(new EntityQueryBuilderInfo
            {
                TypeId = 13,
                Name = "Поставщик",
                ImplementTypeName = "LSSuppliers",
                MvcAlias = "Supplier"
            });

            queryBuilderHandler.GetEntityQueryBuilderInfo(Arg.Is<long>(30)).Returns(new EntityQueryBuilderInfo
            {
                TypeId = 30,
                Name = "Банк",
                ImplementTypeName = "Banks",
                MvcAlias = "Bank"
            });
            queryBuilderHandler.GetEntityQueryBuilderInfo(Arg.Is<long>(10)).Returns(new EntityQueryBuilderInfo
            {
                TypeId = 10,
                Name = "Субъект права",
                ImplementTypeName = "LSLegalSubjects",
                MvcAlias = "LegalSubject"
            });

            queryBuilderHandler.GetEntityQueryBuilderInfo(Arg.Is<long>(11)).Returns(new EntityQueryBuilderInfo
            {
                TypeId = 11,
                Name = "Юридическое лицо",
                ImplementTypeName = "LSOrganisations",
                MvcAlias = "Organisation"
            });

            queryBuilderHandler.GetEntityQueryBuilderInfo(Arg.Is<long>(40)).Returns(new EntityQueryBuilderInfo
            {
                TypeId = 40,
                Name = "Контакт",
                ImplementTypeName = "CRContacts",
                MvcAlias = "Contact"
            });

            queryBuilderHandler.GetEntityQueryBuilderInfo(Arg.Is<long>(41)).Returns(new EntityQueryBuilderInfo
            {
                TypeId = 41,
                Name = "Номер телефона",
                ImplementTypeName = "CRPhones",
                MvcAlias = "Phone"
            });

            queryBuilderHandler.GetEntityQueryBuilderInfo(Arg.Is<long>(42)).Returns(new EntityQueryBuilderInfo
            {
                TypeId = 42,
                Name = "E-mail",
                ImplementTypeName = "CREmails",
                MvcAlias = "Email"
            });

            #endregion

            #region Маркеры для сущностей от 45(Договор)
            queryBuilderHandler.GetMarkersForBaTypeId(Arg.Is<long>(42)).Returns(new List<MarkersQueryBuilder>()
            {
                #region TestData          
                new MarkersQueryBuilder()
                {
                    MarkerId = 1,
                    Name = "E-mail",
                    MvcAlias = "Name",
                    ImplementTypeField = "Email",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 10,
                    Name = "Фамилия",
                    MvcAlias = "LastName",
                    ImplementTypeField = "FirstName",
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 11,
                    Name = "Имя",
                    MvcAlias = "FirstName",
                    ImplementTypeField = "MiddleName",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 12,
                    Name = "Отчество",
                    MvcAlias = "MiddleName",
                    ImplementTypeField = "LastName",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 25,
                    Name = "Подтверждение согласия на рассылку",
                    MvcAlias = "MailConfirmType",
                    ImplementTypeField = "SendConfirmTypeId",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 26,
                    Name = "Назначение рассылки",
                    MvcAlias = "MailType",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = true
                }
                #endregion
            });

            queryBuilderHandler.GetMarkersForBaTypeId(Arg.Is<long>(41)).Returns(new List<MarkersQueryBuilder>()
            {
                #region TestData          
                new MarkersQueryBuilder()
                {
                    MarkerId = 1,
                    Name = "Номер телефона",
                    MvcAlias = "Name",
                    ImplementTypeField = "Number",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 10,
                    Name = "Фамилия",
                    MvcAlias = "LastName",
                    ImplementTypeField = "FirstName",
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 11,
                    Name = "Имя",
                    MvcAlias = "FirstName",
                    ImplementTypeField = "MiddleName",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 12,
                    Name = "Отчество",
                    MvcAlias = "MiddleName",
                    ImplementTypeField = "LastName",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 23,
                    Name = "Тип телефона",
                    MvcAlias = "PhoneType",
                    ImplementTypeField = "PhoneTypeId",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 25,
                    Name = "Подтверждение согласия на рассылку",
                    MvcAlias = "MailConfirmType",
                    ImplementTypeField = "SendConfirmTypeId",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 26,
                    Name = "Назначение рассылки",
                    MvcAlias = "MailType",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = true
                }
                #endregion
            });

            queryBuilderHandler.GetMarkersForBaTypeId(Arg.Is<long>(40)).Returns(new List<MarkersQueryBuilder>()
            {
                #region TestData          
                new MarkersQueryBuilder()
                {
                    MarkerId = 1,
                    Name = "Наименование",
                    MvcAlias = "Name",
                    ImplementTypeField = "Note",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 21,
                    Name = "Контактные телефоны",
                    MvcAlias = "PhoneList",
                    ImplementTypeField = null,
                    BaTypeId = 41,
                    IsPeriodic = false,
                    IsCollectible = true
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 22,
                    Name = "E-mail",
                    MvcAlias = "EmailList",
                    ImplementTypeField = null,
                    BaTypeId = 42,
                    IsPeriodic = false,
                    IsCollectible = true
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 24,
                    Name = "Тип контакта",
                    MvcAlias = "ContactType",
                    ImplementTypeField = "ContactTypeId",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                }
                #endregion
            });

            queryBuilderHandler.GetMarkersForBaTypeId(Arg.Is<long>(11)).Returns(new List<MarkersQueryBuilder>()
            {
                #region TestData          
                new MarkersQueryBuilder()
                {
                    MarkerId = 1,
                    Name = "Наименование",
                    MvcAlias = "Name",
                    ImplementTypeField = "Name",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 2,
                    Name = "Полное наименование",
                    MvcAlias = "FullName",
                    ImplementTypeField = "FullName",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 3,
                    Name = "Код",
                    MvcAlias = "Code",
                    ImplementTypeField = "Code",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 20,
                    Name = "Контакты",
                    MvcAlias = "ContactList",
                    ImplementTypeField = "ContactList",
                    BaTypeId = 40,
                    IsPeriodic = true,
                    IsCollectible = true
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 50,
                    Name = "Группа налогообложения",
                    MvcAlias = "TaxRate",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 100,
                    Name = "ИНН",
                    MvcAlias = "INN",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 101,
                    Name = "КПП",
                    MvcAlias = "KPP",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 102,
                    Name = "ОКПО",
                    MvcAlias = "OKPO",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 103,
                    Name = "ОГРН",
                    MvcAlias = "OGRN",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 104,
                    Name = "ОКАТО",
                    MvcAlias = "OKATO",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 105,
                    Name = "ОКВЭД",
                    MvcAlias = "OKVED",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 106,
                    Name = "ОКТМО",
                    MvcAlias = "OKTMO",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 900,
                    Name = "Отделение (Hermes)",
                    MvcAlias = "Hermes.DivisionName",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 901,
                    Name = "Идентификатор отделения (Hermes)",
                    MvcAlias = "Hermes.DivId",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 902,
                    Name = "Идентификатор абонента (Hermes)",
                    MvcAlias = "Hermes.AbId",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 903,
                    Name = "Код абонента (Hermes)",
                    MvcAlias = "Hermes.AbCode",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                }
                #endregion
            });
            queryBuilderHandler.GetMarkersForBaTypeId(Arg.Is<long>(30)).Returns(new List<MarkersQueryBuilder>());

            queryBuilderHandler.GetMarkersForBaTypeId(Arg.Is<long>(10)).Returns(new List<MarkersQueryBuilder>()
            {
                #region TestData          
                new MarkersQueryBuilder()
                {
                    MarkerId = 1,
                    Name = "Наименование",
                    MvcAlias = "Name",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                }
                #endregion
            });
            queryBuilderHandler.GetMarkersForBaTypeId(Arg.Is<long>(13)).Returns(new List<MarkersQueryBuilder>()
            {
                #region TestData          
                new MarkersQueryBuilder()
                {
                    MarkerId = 1,
                    Name = "Наименование",
                    MvcAlias = "Name",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 30,
                    Name = "Банки",
                    MvcAlias = "BankList",
                    ImplementTypeField = null,
                    BaTypeId = 30,
                    IsPeriodic = true,
                    IsCollectible = true
                }
                #endregion
            });

            queryBuilderHandler.GetMarkersForBaTypeId(Arg.Is<long>(45)).Returns(new List<MarkersQueryBuilder>()
            {
                #region TestData          
                new MarkersQueryBuilder()
                {
                    MarkerId = 1,
                    Name = "Наименование",
                    MvcAlias = "Name",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 6,
                    Name = "Поставщик",
                    MvcAlias = "Supplier",
                    ImplementTypeField = null,
                    BaTypeId = 13,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 8,
                    Name = "Дата начала действия",
                    MvcAlias = "StartDate",
                    ImplementTypeField = "StartDate",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 9,
                    Name = "Дата окончания действия",
                    MvcAlias = "EndDate",
                    ImplementTypeField = "EndDate",
                    BaTypeId = null,
                    IsPeriodic = false,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 16,
                    Name = "Субъект права",
                    MvcAlias = "LegalSubject",
                    ImplementTypeField = null,
                    BaTypeId = 10,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 200,
                    Name = "Действие договора",
                    MvcAlias = "ContractActivityScheme",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 201,
                    Name = "Состояние договора",
                    MvcAlias = "ContractStateScheme",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 202,
                    Name = "Дата вступления в силу договора",
                    MvcAlias = "ContractValidateScheme",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 203,
                    Name = "Расчет по договору",
                    MvcAlias = "ContractCalcScheme",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 204,
                    Name = "Дата окончательного расчета по договору",
                    MvcAlias = "ContractFinalPaymentScheme",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 205,
                    Name = "Схема авансов по договору",
                    MvcAlias = "ContractAdvanceScheme",
                    ImplementTypeField = null,
                    BaTypeId = null,
                    IsPeriodic = true,
                    IsCollectible = false
                },
                new MarkersQueryBuilder()
                {
                    MarkerId = 10210,
                    Name = "Юр. лицо (тест)",
                    MvcAlias = "LS_test",
                    ImplementTypeField = null,
                    BaTypeId = 11,
                    IsPeriodic = false,
                    IsCollectible = false
                }
                #endregion
            });


            #endregion

            #region Названия колонок

            queryBuilderHandler.GetColumnsNameByImplamentTypeName(Arg.Is<string>("CRContacts"))
                .Returns(new List<string>
                {
                    "BAId",
                    "ContactTypeId",
                    "Note"
                });

            queryBuilderHandler.GetColumnsNameByImplamentTypeName(Arg.Is<string>("LSSuppliers"))
                .Returns(new List<string>
                {
                    "SupplierId",
                    "LSId"
                });

            queryBuilderHandler.GetColumnsNameByImplamentTypeName(Arg.Is<string>("Banks"))
                .Returns(new List<string>
                {
                    "BAId",
                    "Name",
                    "ParentId",
                    "BIK",
                    "INN",
                    "RKC",
                    "OKPO",
                    "AOGuid"
                });

            queryBuilderHandler.GetColumnsNameByImplamentTypeName(Arg.Is<string>("LSLegalSubjects"))
                .Returns(new List<string>
                {
                    "BAId",
                    "Name",
                    "FullName",
                    "Code",
                    "ParentId",
                    "LastName",
                    "FirstName",
                    "MiddleName",
                    "BirthDate",
                    "Gender",
                    "INN",
                    "Note"
                });

            queryBuilderHandler.GetColumnsNameByImplamentTypeName(Arg.Is<string>("LSOrganisations"))
                .Returns(new List<string>
                {
                    "BAId",
                    "Code",
                    "Name",
                    "FullName"
                });

            queryBuilderHandler.GetColumnsNameByImplamentTypeName(Arg.Is<string>("CAContracts"))
                .Returns(new List<string>
                {
                    "BAId",
                    "StartDate",
                    "EndDate",
                    "IsProject",
                    "Number"
                });

            queryBuilderHandler.GetColumnsNameByImplamentTypeName(Arg.Is<string>("CRPhones"))
                .Returns(new List<string>
                {
                    "BAId",
                    "Name",
                    "PhoneTypeId",
                    "FirstName",
                    "MiddleName",
                    "LastName",
                    "FullName",
                    "SendConfirmTypeId",
                });

            queryBuilderHandler.GetColumnsNameByImplamentTypeName(Arg.Is<string>("CRPhones"))
                .Returns(new List<string>
                {
                    "BAId",
                    "Email",
                    "FirstName",
                    "MiddleName",
                    "LastName",
                    "FullName",
                    "SendConfirmTypeId",
                });
            #endregion

            return queryBuilderHandler;
        }

    }
}

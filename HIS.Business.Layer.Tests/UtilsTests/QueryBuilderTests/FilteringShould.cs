﻿using System;
using System.Threading.Tasks;
using HIS.Business.Layer.Utils.QueryBuilder;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using NUnit.Framework;

namespace HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests
{
    [TestFixture]
    public class FilteringShould
    {
        [TestCase("name", FiltersOperator.Equal, "test", "name = 'test'\n")]
        [TestCase("name", FiltersOperator.Less, "test", "name < 'test'\n")]
        [TestCase("name", FiltersOperator.More, "test", "name > 'test'\n")]
        [TestCase("name", FiltersOperator.LessEqual, "test", "name <= 'test'\n")]
        [TestCase("name", FiltersOperator.MoreEqual, "test", "name >= 'test'\n")]
        [TestCase("name", FiltersOperator.NoEqual, "test", "name != 'test'\n")]
        [TestCase("name", FiltersOperator.Equal, 1, "name = 1\n")]
        public async Task when_filtering_FormatFilteringString_withParams_should(string field, FiltersOperator filtersOperator, 
            object value, string result)
        {
            IGroupMember filter = new Filtering(field,filtersOperator,value);
            var actual = await filter.FormatItemString();
            Assert.That( result, Is.EqualTo(actual));
        }

        //SELECT {d'2017-02-13'}
        [TestCase("date", FiltersOperator.Equal, "date = {d\'2017-02-12\'}\n")]
        [TestCase("date", FiltersOperator.Less, "date < {d\'2017-02-12\'}\n")]
        [TestCase("date", FiltersOperator.More, "date > {d\'2017-02-12\'}\n")]
        [TestCase("date", FiltersOperator.LessEqual, "date <= {d\'2017-02-12\'}\n")]
        [TestCase("date", FiltersOperator.MoreEqual, "date >= {d\'2017-02-12\'}\n")]
        [TestCase("date", FiltersOperator.NoEqual, "date != {d\'2017-02-12\'}\n")]
        public void when_filtering_FormatFilteringString_withDate_should(string field, FiltersOperator filtersOperator, string result)
        {
            var testDate = new DateTime(2017, 02, 12);
            IGroupMember filter = new Filtering(field, filtersOperator, testDate);
            
            Assert.That( () => filter.FormatItemString(), Is.EqualTo(result));
        }
    }
}

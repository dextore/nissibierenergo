﻿using System.Threading.Tasks;
using NUnit.Framework;
using HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests.InfrostructureClasses;
using HIS.Business.Layer.Utils.QueryBuilder.Models;

namespace HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests
{
    [TestFixture]
    public class OrderingGroupShould
    {

        [TestCase("name", SortingOrder.Asc, "ORDER BY name Asc\n")]
        [TestCase("qwerty", SortingOrder.Desc, "ORDER BY qwerty Desc\n")]
        public async Task when_create_orderingGroup_with_one_ordering_orderingGroup_should_reutrn(string fieldName, SortingOrder sortOrder, string result)
        {
            var orderingGroup = OrderingTestHelper.InitializeOrderingGroup();
            var ordering = OrderingTestHelper.Initialize(fieldName, sortOrder);

            orderingGroup.GroupList.Add(ordering);
            
            Assert.That(async () => await orderingGroup.FormatGroupQuery(), Is.EqualTo(result));
        }

        [Test]
        public void when_create_orderingGroup_with_two_ordering_orderingGroup_should_return()
        {
            var orderingGroup = OrderingTestHelper.InitializeOrderingGroup();
            orderingGroup.GroupList.Add(OrderingTestHelper.Initialize("first", SortingOrder.Asc));
            orderingGroup.GroupList.Add(OrderingTestHelper.Initialize("second", SortingOrder.Desc));

            Assert.That(async () => await orderingGroup.FormatGroupQuery(), Is.EqualTo("ORDER BY first Asc,\nsecond Desc\n"));
        }

    }
}

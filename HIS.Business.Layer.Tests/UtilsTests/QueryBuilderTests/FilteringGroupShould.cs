﻿using System;
using HIS.Business.Layer.Utils.QueryBuilder;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using NUnit.Framework;

namespace HIS.Business.Layer.Tests.UtilsTests.QueryBuilderTests
{
    [TestFixture]
    public class FilteringGroupShould
    {
        [Test]
        public void when_filteringGroup_FormatFilteringGroupQuery_withParams_should()
        {
            var filteringGroup = new FilteringGroup(FilteringGroupOperator.And, QuerySection.Where);
            var filterOne = new Filtering("name", FiltersOperator.Equal, "test");
            var filterTwo = new Filtering("date", FiltersOperator.Less, new DateTime(2017,02,12));

            filteringGroup.GroupList.Add(filterOne);
            filteringGroup.GroupList.Add(filterTwo);

            Assert.That(() => filteringGroup.FormatGroupQuery(), Is.EqualTo(
                "WHERE name = 'test' And date < {d'2017-02-12'} \n"));
        }
    }
}

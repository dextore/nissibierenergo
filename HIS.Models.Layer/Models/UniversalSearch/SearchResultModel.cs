﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.UniversalSearch
{
    [TsClass(Name = "SearchResultModel", Module = "UniversalSearch")]
    public class SearchResultModel
    {
        [TsProperty(Name = "items")]
        [JsonProperty(PropertyName = "items")]
        public IEnumerable<SimpleSearchItemModel> Items { get; set; }

        [TsProperty(Name = "rowCount")]
        [JsonProperty(PropertyName = "rowCount")]
        public long RowCount { get; set; }
    }
}

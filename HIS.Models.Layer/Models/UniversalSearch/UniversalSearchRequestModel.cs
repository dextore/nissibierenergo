﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.UniversalSearch
{
    [TsClass(Name = "UniversalSearchRequestModel", Module = "UniversalSearch")]
    public class UniversalSearchRequestModel
    {
        [TsProperty(Name = "from")]
        [JsonProperty(PropertyName = "from")]
        public long From { get; set; }

        [TsProperty(Name = "to")]
        [JsonProperty(PropertyName = "to")]
        public long To { get; set; }

        [TsProperty(Name = "value")]
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [TsProperty(Name = "showDeleted")]
        [JsonProperty(PropertyName = "showDeleted")]
        public bool ShowDeleted { get; set; }

        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "BATypeId")]
        public int? BATypeId { get; set; }

    }
}

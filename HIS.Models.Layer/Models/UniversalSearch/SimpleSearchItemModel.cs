﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.UniversalSearch
{
    [TsClass(Name = "SimpleSearchItemModel", Module = "UniversalSearch")]
    public class SimpleSearchItemModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BAId { get; set; }

        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BATypeId { get; set; }

        [TsProperty(Name = "typeName")]
        [JsonProperty(PropertyName = "typeName")]
        public string TypeName { get; set; }

        [TsProperty(Name = "stateName")]
        [JsonProperty(PropertyName = "stateName")]
        public string StateName { get; set; }

        [TsProperty(Name = "code")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "INN")]
        [JsonProperty(PropertyName = "INN")]
        public string INN { get; set; }

        [TsProperty(Name = "legalFormName")]
        [JsonProperty(PropertyName = "legalFormName")]
        public string LegalFormName { get; set; }
    }
}

﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Operations
{
    [TsClass(Name = "OperationUpdateModel", Module = "Operations")]
    public class OperationUpdateModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BAId { get; set; }

        [TsProperty(Name = "operationId")]
        [JsonProperty(PropertyName = "operationId")]
        public int OperationId { get; set; }
    }
}

﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Operations
{
    [TsClass(Name = "StateInfoModel", Module = "States")]
    public class StateInfoModel
    {
        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BATypeId { get; set; }

        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int StateId { get; set; }

        [TsProperty(Name = "stateName")]
        [JsonProperty(PropertyName = "stateName")]
        public string StateName { get; set; }

        [TsProperty(Name = "isFirstState")]
        [JsonProperty(PropertyName = "isFirstState")]
        public bool IsFirstState { get; set; }

        [TsProperty(Name = "isDelState")]
        [JsonProperty(PropertyName = "isDelState")]
        public bool IsDelState { get; set; }
    }
}

﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Operations
{
    [TsClass(Name = "EntityOperationModel", Module = "Operations")]
    public class EntityOperationModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BAId { get; set; }

        [TsProperty(Name = "typeId")]
        [JsonProperty(PropertyName = "typeId")]
        public int TypeId { get; set; }

        [TsProperty(Name = "operationId")]
        [JsonProperty(PropertyName = "operationId")]
        public int OperationId { get; set; }

        [TsProperty(Name = "isFirstState")]
        [JsonProperty(PropertyName = "isFirstState")]
        public bool IsFirstState { get; set; }

        [TsProperty(Name = "isDelState")]
        [JsonProperty(PropertyName = "isDelState")]
        public bool IsDelState { get; set; }

        [TsProperty(Name = "operationName")]
        [JsonProperty(PropertyName = "operationName")]
        public string OperationName { get; set; }

        [TsProperty(Name = "srcStateId")]
        [JsonProperty(PropertyName = "srcStateId")]
        public int SrcStateId { get; set; }

        [TsProperty(Name = "DestStateId")]
        [JsonProperty(PropertyName = "DestStateId")]
        public int DestStateId { get; set; }

        [TsProperty(Name = "isBlocked")]
        [JsonProperty(PropertyName = "isBlocked")]
        public bool IsBlocked { get; set; }
    }
}

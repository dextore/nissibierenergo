﻿namespace HIS.Models.Layer.Models.Contracts
{
    /// <summary>
    /// Модель пункта меню
    /// </summary>
    public class CheckedMenuItemModel : MenuItemModel
    {
        /// <summary>
        /// Признак выбранного элемента в меню
        /// </summary>
        public bool isChecked { get;  set;  }        
    }
}
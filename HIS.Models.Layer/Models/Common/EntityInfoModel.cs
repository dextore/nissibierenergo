﻿using System.Collections.Generic;
using HIS.Models.Layer.Models.Markers;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Common
{
    [TsClass(Name = "EntityInfoModel", Module = "Entities")]
    public class EntityInfoModel
    {
        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BATypeId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "mvcAlias")]
        [JsonProperty(PropertyName = "mvcAlias")]
        public string MVCAlias { get; set; }

        [TsProperty(Name = "parentBATypeId")]
        [JsonProperty(PropertyName = "parentBATypeId")]
        public int? ParentBATypeId { get; set; }

        [TsProperty(Name = "isImplementType ")]
        [JsonProperty(PropertyName = "isImplementType ")]
        public bool IsImplementType { get; set; }

        [TsProperty(Name = "isGroupType")]
        [JsonProperty(PropertyName = "isGroupType")]
        public bool IsGroupType { get; set; }

        [TsProperty(Name = "implementTypeName")]
        [JsonProperty(PropertyName = "implementTypeName")]
        public string ImplementTypeName { get; set; }

        [TsProperty(Name = "searchTypeId")]
        [JsonProperty(PropertyName = "searchTypeId")]
        public int SearchTypeId { get; set; }

        [TsProperty(Name = "states")]
        [JsonProperty(PropertyName = "states")]
        public IEnumerable<EntityStatesModel> States { get; set; }

        [TsProperty(Name = "markersInfo")]
        [JsonProperty(PropertyName = "markersInfo")]
        public IEnumerable<MarkerInfoModel> MarkersInfo { get; set; }
    }

    [TsClass(Name = "EntityStatesModel", Module = "Entities")]
    public class EntityStatesModel
    {
        [TsProperty(Name = "id")]
        [JsonProperty(PropertyName = "id")]
        public int StateId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }


    }
}

﻿using Newtonsoft.Json;
using TypeLite;
namespace HIS.DAL.Client.Models.Common
{
    [TsClass(Name = "EntityViewItemModel", Module = "Entities")]
    public class EntityViewItemModel
    {
        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int MarkerId { get; set; }

        [TsProperty(Name = "viewName")]
        [JsonProperty(PropertyName = "viewName")]
        public string ViewName { get; set; }

        [TsProperty(Name = "itemId")]
        [JsonProperty(PropertyName = "itemId")]
        public int ItemId { get; set; }

        [TsProperty(Name = "itemName")]
        [JsonProperty(PropertyName = "itemName")]
        public string ItemName { get; set; }

        [TsProperty(Name = "isActive")]
        [JsonProperty(PropertyName = "isActive")]
        public bool IsActive { get; set; }

        [TsProperty(Name = "orderNum")]
        [JsonProperty(PropertyName = "orderNum")]
        public int OrderNum { get; set; }
    }
}

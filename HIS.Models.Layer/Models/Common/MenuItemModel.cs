﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Contracts
{
    [TsClass(Name = "MenuItemModel", Module = "MainMenu")]
    public class MenuItemModel
    {
        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "id")]
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [TsProperty(Name = "text")]
        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [TsProperty(Name = "disabled")]
        [JsonProperty(PropertyName = "disabled")]
        public bool Disabled { get; set; }

        [TsProperty(Name = "icon")]
        [JsonProperty(PropertyName = "icon")]
        public string Icon { get; set; }

        [TsProperty(Name = "moduleName")]
        [JsonProperty(PropertyName = "moduleName")]
        public string ModuleName { get; set; }

        [TsProperty(Name = "mvcAliases")]
        [JsonProperty(PropertyName = "mvcAliases")]
        public IEnumerable<string> MVCAliases { get; set; }

        [TsProperty(Name = "items")]
        [JsonProperty(PropertyName = "items")]
        public IEnumerable<MenuItemModel> items { get; set; }
    }
}
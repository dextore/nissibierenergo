﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.InventoryObjectsMovementHistory
{
    [TsClass(Name = "InventoryAssetsModel", Module = "InventoryObjectsMovementHistory")]
    public class InventoryAssetsModel
    {
        [TsProperty(Name = "bAId")]
        [JsonProperty(PropertyName = "bAId")]
        public long? BAId { get; set; }
        [TsProperty(Name = "inventoryNumber")]
        [JsonProperty(PropertyName = "inventoryNumber")]
        public string InventoryNumber { get; set; }
        [TsProperty(Name = "serialNumber")]
        [JsonProperty(PropertyName = "serialNumber")]
        public string SerialNumber { get; set; }
        [TsProperty(Name = "releaseDate")]
        [JsonProperty(PropertyName = "releaseDate")]
        public DateTime? ReleaseDate { get; set; }
        [TsProperty(Name = "createdDate")]
        [JsonProperty(PropertyName = "createdDate")]
        public DateTime? CreatedDate { get; set; }
        [TsProperty(Name = "docName")]
        [JsonProperty(PropertyName = "docName")]
        public string DocName { get; set; }
        [TsProperty(Name = "inventoryName")]
        [JsonProperty(PropertyName = "inventoryName")]
        public string InventoryName { get; set; }
        [TsProperty(Name = "mRPName")]
        [JsonProperty(PropertyName = "mRPName")]
        public string MRPName { get; set; }
        [TsProperty(Name = "companyAreaName")]
        [JsonProperty(PropertyName = "companyAreaName")]
        public string CompanyAreaName { get; set; }
        [TsProperty(Name = "lSName")]
        [JsonProperty(PropertyName = "lSName")]
        public string LSName { get; set; }
        [TsProperty(Name = "barCode")]
        [JsonProperty(PropertyName = "barCode")]
        public string BarCode { get; set; }
        [TsProperty(Name = "employeeName")]
        [JsonProperty(PropertyName = "employeeName")]
        public string EmployeeName { get; set; }
        [TsProperty(Name = "location")]
        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }
        [TsProperty(Name = "checkDate")]
        [JsonProperty(PropertyName = "checkDate")]
        public DateTime? CheckDate { get; set; }
        [TsProperty(Name = "state")]
        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }
        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int StateId { get; set; }
    }
}

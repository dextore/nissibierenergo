﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.InventoryObjectsMovementHistory
{
    [TsClass(Name = "InventoryAssetsMovementHistoryModel", Module = "InventoryObjectsMovementHistory")]
    public class InventoryAssetsMovementHistoryModel
    {
        [TsProperty(Name = "recId")]
        [JsonProperty(PropertyName = "recId")]
        public long? RecId { get; set; }
        [TsProperty(Name = "inventoryAssetId")]
        [JsonProperty(PropertyName = "inventoryAssetId")]
        public long? InventoryAssetId { get; set; }
        [TsProperty(Name = "inventoryAssetNumber")]
        [JsonProperty(PropertyName = "inventoryAssetNumber")]
        public string InventoryAssetNumber { get; set; }
        [TsProperty(Name = "docId")]
        [JsonProperty(PropertyName = "docId")]
        public long? DocId { get; set; }
        [TsProperty(Name = "docName")]
        [JsonProperty(PropertyName = "docName")]
        public string DocName { get; set; }
        [TsProperty(Name = "mRPId")]
        [JsonProperty(PropertyName = "mRPId")]
        public long? MRPId { get; set; }
        [TsProperty(Name = "mRPName")]
        [JsonProperty(PropertyName = "mRPName")]
        public string MRPName { get; set; }
        [TsProperty(Name = "location")]
        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }
        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime StartDate { get; set; }
        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime EndDate { get; set; }
    }
}

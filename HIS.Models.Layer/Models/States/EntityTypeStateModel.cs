﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.States
{
    /// <summary>
    /// Состояние сущности для передачи на клиента
    /// </summary>
    [TsClass(Name = "EntityTypeStateModel", Module = "States")]
    public class EntityTypeStateModel
    {
        /// <summary>
        /// Тип сущьности
        /// </summary>
        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BATypeId { get; set; }

        /// <summary>
        /// Идентификатор состояния
        /// </summary>
        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int StateId { get; set; }

        /// <summary>
        /// Наименование состояния
        /// </summary>
        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Начальное состояния
        /// </summary>
        [TsProperty(Name = "isFirstState")]
        [JsonProperty(PropertyName = "isFirstState")]
        public bool IsFirstState { get; set; }

        /// <summary>
        ///  Состояние удален
        /// </summary>
        [TsProperty(Name = "isDelState")]
        [JsonProperty(PropertyName = "isDelState")]
        public bool IsDelState { get; set; }
    }
}

﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Authentication
{
    /// <summary>
    /// Модель входа в систему
    /// </summary>
    [TsClass(Name = "LoginModel", Module = "Auth")]
    public class LoginModel
    {
        /// <summary>
        /// Имя пользователя
        /// </summary>
        [TsProperty(Name = "userName")]
        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [TsProperty(Name = "password")]
        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }

        /// <summary>
        /// Алиас подключаемой БД
        /// </summary>
        [TsProperty(Name = "dbalias")]
        [JsonProperty(PropertyName = "dbalias")]
        public string DBAlias { get; set; }    
    }
}

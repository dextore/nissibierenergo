﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Authentication
{
    [TsClass(Name = "AuthResultModel", Module = "Auth")]
    public class AuthResultModel
    {
        [TsProperty(Name = "result")]
        [JsonProperty(PropertyName = "result")]
        public bool Result { get; set; }
        [TsProperty(Name = "error")]
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }
    }
}

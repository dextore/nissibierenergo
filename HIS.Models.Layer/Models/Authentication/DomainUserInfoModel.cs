﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Authentication
{


    /// <summary>
    /// Модель данных по доменному пользователю
    /// </summary>
    [TsClass(Name = "DomainUserInfoModel", Module = "Auth")]
    public class DomainUserInfoModel
    {
        /// <summary>
        /// Пользователь
        /// </summary>
        [TsProperty(Name = "displayName")]
        [JsonProperty(PropertyName = "displayName")]
        public string DisplayName { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [TsProperty(Name = "description")]
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [TsProperty(Name = "employeeId")]
        [JsonProperty(PropertyName = "employeeId")]
        public string EmployeeId { get; set; }

        [TsProperty(Name = "voiceTelephoneNumber")]
        [JsonProperty(PropertyName = "voiceTelephoneNumber")]
        public string VoiceTelephoneNumber { get; set; }

        [TsProperty(Name = "samAccountName")]
        [JsonProperty(PropertyName = "samAccountName")]
        public string SamAccountName { get; set; }
    }
}

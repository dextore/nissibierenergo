﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Authentication
{
    /// <summary>
    /// Модель описания БД для работы
    /// </summary>
    [TsClass(Name = "AreaDataBaseModel", Module = "Auth")]
    public class AreaDataBaseModel
    {
        /// <summary>
        /// Алиас
        /// </summary>
        [TsProperty(Name = "alias")]
        [JsonProperty(PropertyName = "alias")]
        public string Alias { get; set; }

        /// <summary>
        /// Наименование БД
        /// </summary>
        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }


        /// <summary>
        /// Описание БД
        /// </summary>
        [TsProperty(Name = "desc")]
        [JsonProperty(PropertyName = "desc")]
        public string Desc { get; set; }
    }
}
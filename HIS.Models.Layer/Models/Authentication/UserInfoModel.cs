﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Authentication
{
    [TsClass(Name = "UserInfoModel", Module = "Auth")]
    public class UserInfoModel
    {
        /// <summary>
        /// Имя пользователя
        /// </summary>
        [TsProperty(Name = "userName")]
        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; set; }
    }
}

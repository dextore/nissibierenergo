﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Nomenclatures
{
    [TsClass(Name = "NomenclatureSaveModel", Module = "Nomenclatures")]
    public class NomenclatureSaveModel
    {
        [TsProperty(Name = "nomenclatureId")]
        [JsonProperty(PropertyName = "nomenclatureId")]
        public long? NomenclatureId { get; set; }

        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BATypeId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "buyNatMeaningId")]
        [JsonProperty(PropertyName = "buyNatMeaningId")]
        public int BuyNatMeaningId { get; set; }

        [TsProperty(Name = "saleNatMeaningId")]
        [JsonProperty(PropertyName = "saleNatMeaningId")]
        public int SaleNatMeaningId { get; set; }

        [TsProperty(Name = "stockNatMeaningId")]
        [JsonProperty(PropertyName = "stockNatMeaningId")]
        public int StockNatMeaningId { get; set; }

        [TsProperty(Name = "taxRateId")]
        [JsonProperty(PropertyName = "taxRateId")]
        public int TaxRateId { get; set; }
    }
}
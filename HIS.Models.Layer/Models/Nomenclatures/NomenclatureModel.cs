﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Nomenclatures
{
    [TsClass(Name = "NomenclatureModel", Module = "Nomenclatures")]
    public class NomenclatureModel
    {
        [TsProperty(Name = "nomenclatureId")]
        [JsonProperty(PropertyName = "nomenclatureId")]
        public long? NomenclatureId { get; set; }

        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BATypeId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "buyNatMeaningId")]
        [JsonProperty(PropertyName = "buyNatMeaningId")]
        public int BuyNatMeaningId { get; set; }

        [TsProperty(Name = "buyNatMeaningName")]
        [JsonProperty(PropertyName = "buyNatMeaningName")]
        public string BuyNatMeaningName { get; set; }

        [TsProperty(Name = "buyNatMeaningNote")]
        [JsonProperty(PropertyName = "buyNatMeaningNote")]
        public string BuyNatMeaningNote { get; set; }

        [TsProperty(Name = "saleNatMeaningId")]
        [JsonProperty(PropertyName = "saleNatMeaningId")]
        public int SaleNatMeaningId { get; set; }

        [TsProperty(Name = "saleNatMeaningName")]
        [JsonProperty(PropertyName = "saleNatMeaningName")]
        public string SaleNatMeaningName { get; set; }

        [TsProperty(Name = "saleNatMeaningNote")]
        [JsonProperty(PropertyName = "saleNatMeaningNote")]
        public string SaleNatMeaningNote { get; set; }

        [TsProperty(Name = "stockNatMeaningId")]
        [JsonProperty(PropertyName = "stockNatMeaningId")]
        public int StockNatMeaningId { get; set; }

        [TsProperty(Name = "stockNatMeaningName")]
        [JsonProperty(PropertyName = "stockNatMeaningName")]
        public string StockNatMeaningName { get; set; }

        [TsProperty(Name = "stockNatMeaningNote")]
        [JsonProperty(PropertyName = "stockNatMeaningNote")]
        public string StockNatMeaningNote { get; set; }

        [TsProperty(Name = "taxRateId")]
        [JsonProperty(PropertyName = "taxRateId")]
        public int TaxRateId { get; set; }

        [TsProperty(Name = "taxRateName")]
        [JsonProperty(PropertyName = "taxRateName")]
        public string TaxRateName { get; set; }

        [TsProperty(Name = "taxRateNote")]
        [JsonProperty(PropertyName = "taxRateNote")]
        public string TaxRateNote { get; set; }
    }
}
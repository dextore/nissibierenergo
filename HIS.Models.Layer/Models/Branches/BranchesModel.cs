﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Branches
{
    [TsClass(Name = "BranchModel", Module = "Branch")]
    public class BranchModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long? Baid { get; set; }
        [TsProperty(Name = "parentId")]
        [JsonProperty(PropertyName = "parentId")]
        public long ParentId { get; set; }
        [TsProperty(Name = "brnNum")]
        [JsonProperty(PropertyName = "brnNum")]
        public string BrnNum { get; set; }
        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [TsProperty(Name = "treeLevel")]
        [JsonProperty(PropertyName = "treeLevel")]
        public int TreeLevel { get; set; }

    }
}

﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Branches
{
    [TsClass(Name = "BranchItemModel", Module = "Branch")]
    public class BranchItemModel : BranchModel
    {
        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime? StartDate { get; set; }
        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }
        [TsProperty(Name = "itemId")]
        [JsonProperty(PropertyName = "itemId")]
        public long? ItemId { get; set; }
    }
}

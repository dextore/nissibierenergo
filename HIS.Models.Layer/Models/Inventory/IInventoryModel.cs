﻿using Newtonsoft.Json;
using TypeLite;
namespace HIS.Models.Layer.Models.Inventory
{
    [TsClass(Name = "IInventoryModel", Module = "Inventory")]
    public class IInventoryModel
    {
        [TsProperty(Name = "id")]
        [JsonProperty(PropertyName = "id")]
        public long? BAId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "code")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [TsProperty(Name = "parentId")]
        [JsonProperty(PropertyName = "parentId")]
        public long? ParentId { get; set; }

        [TsProperty(Name = "caId")]
        [JsonProperty(PropertyName = "caId")]
        public long? CAId { get; set; }

        [TsProperty(Name = "buyUnitId")]
        [JsonProperty(PropertyName = "buyUnitId")]
        public int? BuyUnitId { get; set; }

        [TsProperty(Name = "saleUnitId")]
        [JsonProperty(PropertyName = "saleUnitId")]
        public int? SaleUnitId { get; set; }

        [TsProperty(Name = "stockUnitId")]
        [JsonProperty(PropertyName = "stockUnitId")]
        public int? StockUnitId { get; set; }

        [TsProperty(Name = "resourceTypeId")]
        [JsonProperty(PropertyName = "resourceTypeId")]
        public int? ResourceTypeId { get; set; }

        [TsProperty(Name = "kindId")]
        [JsonProperty(PropertyName = "kindId")]
        public int? KindId { get; set; }

        [TsProperty(Name = "typeId")]
        [JsonProperty(PropertyName = "typeId")]
        public int? TypeId { get; set; }


    }
}

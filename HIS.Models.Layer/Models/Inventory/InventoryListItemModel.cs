﻿using Newtonsoft.Json;
using TypeLite;
namespace HIS.Models.Layer.Models.Inventory
{
    [TsClass(Name = "InventoryListItemModel", Module = "Inventory")]
    public class InventoryListItemModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long? BAId { get; set; }

        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BATypeId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "code")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [TsProperty(Name = "caId")]
        [JsonProperty(PropertyName = "caId")]
        public long? CAId { get; set; }

        [TsProperty(Name = "companyName")]
        [JsonProperty(PropertyName = "companyName")]
        public string CompanyName { get; set; }

        [TsProperty(Name = "buyUnitId")]
        [JsonProperty(PropertyName = "buyUnitId")]
        public int? BuyUnitId { get; set; }

        [TsProperty(Name = "buyUnitName")]
        [JsonProperty(PropertyName = "buyUnitName")]
        public string BuyUnitName { get; set; }

        [TsProperty(Name = "saleUnitId")]
        [JsonProperty(PropertyName = "saleUnitId")]
        public int? SaleUnitId { get; set; }

        [TsProperty(Name = "saleUnitName")]
        [JsonProperty(PropertyName = "saleUnitName")]
        public string SaleUnitName { get; set; }

        [TsProperty(Name = "stockUnitId")]
        [JsonProperty(PropertyName = "stockUnitId")]
        public int? StockUnitId { get; set; }

        [TsProperty(Name = "stockUnitName")]
        [JsonProperty(PropertyName = "stockUnitName")]
        public string StockUnitName { get; set; }

        [TsProperty(Name = "kindId")]
        [JsonProperty(PropertyName = "kindId")]
        public int KindId { get; set; }

        [TsProperty(Name = "kindName")]
        [JsonProperty(PropertyName = "kindName")]
        public string KindName { get; set; }

        [TsProperty(Name = "parentId")]
        [JsonProperty(PropertyName = "parentId")]
        public long? ParentId { get; set; }

        [TsProperty(Name = "parentName")]
        [JsonProperty(PropertyName = "parentName")]
        public string ParentName { get; set; }
     
        [TsProperty(Name = "typeId")]
        [JsonProperty(PropertyName = "typeId")]
        public int? TypeId { get; set; }

        [TsProperty(Name = "typeName")]
        [JsonProperty(PropertyName = "typeName")]
        public string TypeName { get; set; }

        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int? StateId { get; set; }

        [TsProperty(Name = "stateName")]
        [JsonProperty(PropertyName = "stateName")]
        public string StateName { get; set; }
    }
}

﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Inventory
{
     [TsClass(Name = "InventoryListFiltersModel", Module = "Inventory")]
     public  class InventoryListFiltersModel
     {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long? BAId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "caId")]
        [JsonProperty(PropertyName = "caId")]
        public long? CAId { get; set; }

        [TsProperty(Name = "kindName")]
        [JsonProperty(PropertyName = "kindName")]
        public string KindName { get; set; }

        [TsProperty(Name = "parentName")]
        [JsonProperty(PropertyName = "parentName")]
        public string ParentName { get; set; }

        [TsProperty(Name = "stateName")]
        [JsonProperty(PropertyName = "stateName")]
        public string StateName { get; set; }

        [TsProperty(Name = "typeName")]
        [JsonProperty(PropertyName = "typeName")]
        public string TypeName { get; set; }

        [TsProperty(Name = "buyUnitName")]
        [JsonProperty(PropertyName = "buyUnitName")]
        public string BuyUnitName { get; set; }

        [TsProperty(Name = "saleUnitName")]
        [JsonProperty(PropertyName = "saleUnitName")]
        public string SaleUnitName { get; set; }

        [TsProperty(Name = "stockUnitName")]
        [JsonProperty(PropertyName = "stockUnitName")]
        public string StockUnitName { get; set; }

    }
}

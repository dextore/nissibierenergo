﻿using System.Collections.Generic;
using HIS.Models.Layer.Models.Markers;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Inventory
{
    [TsClass(Name = "InventoryDataModel", Module = "Inventory")]
    public class InventoryDataModel
    {
        [TsProperty(Name = "BAId")]
        [JsonProperty(PropertyName = "BAId")]
        public long? BAId { get; set; }

        [TsProperty(Name = "Name")]
        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [TsProperty(Name = "Code")]
        [JsonProperty(PropertyName = "Code")]
        public string Code { get; set; }

        [TsProperty(Name = "CompanyArea")]
        [JsonProperty(PropertyName = "CompanyArea")]
        public long? CompanyArea { get; set; }

        [TsProperty(Name = "CompanyName")]
        [JsonProperty(PropertyName = "CompanyName")]
        public string CompanyName { get; set; }

        [TsProperty(Name = "Inventory")]
        [JsonProperty(PropertyName = "Inventory")]
        public long? Inventory { get; set; }

        [TsProperty(Name = "InventoryName")]
        [JsonProperty(PropertyName = "InventoryName")]
        public string InventoryName { get; set; }


        [TsProperty(Name = "BuyNatMeaning")]
        [JsonProperty(PropertyName = "BuyNatMeaning")]
        public int? BuyNatMeaning { get; set; }

        [TsProperty(Name = "SaleNatMeaning")]
        [JsonProperty(PropertyName = "SaleNatMeaning")]
        public int? SaleNatMeaning { get; set; }

        [TsProperty(Name = "StockNatMeaning")]
        [JsonProperty(PropertyName = "StockNatMeaning")]
        public int? StockNatMeaning { get; set; }

        [TsProperty(Name = "InventoryKind")]
        [JsonProperty(PropertyName = "InventoryKind")]
        public int? InventoryKind { get; set; }

        [TsProperty(Name = "InventoryType")]
        [JsonProperty(PropertyName = "InventoryType")]
        public int? InventoryType { get; set; }

        [TsProperty(Name = "ResourceTypeId")]
        [JsonProperty(PropertyName = "ResourceTypeId")]
        public int? ResourceTypeId { get; set; }

        [TsProperty(Name = "soState")]
        [JsonProperty(PropertyName = "soState")]
        public int? SOState { get; set; }


        [TsProperty(Name = "markerValues")]
        [JsonProperty(PropertyName = "markerValues")]
        public IEnumerable<MarkerValueModel> MarkerValues { get; set; }
    }


  
  


}

﻿using Newtonsoft.Json;
using TypeLite;
namespace HIS.Models.Layer.Models.Inventory
{
    [TsClass(Name = "RefSysUnitsModel", Module = "Inventory")]
    public class RefSysUnitsModel
    {
        [TsProperty(Name = "id")]
        [JsonProperty(PropertyName = "id")]
        public int ID { get; set; }

        [TsProperty(Name = "unitId")]
        [JsonProperty(PropertyName = "unitId")]
        public int UnitId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "unitClassId")]
        [JsonProperty(PropertyName = "unitClassId")]
        public int UnitClassId { get; set; }

        [TsProperty(Name = "code")]
        [JsonProperty(PropertyName = "code")]
        public long? Code { get; set; }

        [TsProperty(Name = "symbol")]
        [JsonProperty(PropertyName = "symbol")]
        public string Symbol { get; set; }
    }
}

﻿using Newtonsoft.Json;
using TypeLite;
namespace HIS.Models.Layer.Models.Inventory
{
    [TsClass(Name = "LSCompanyAreas", Module = "Inventory")]
    public class LSCompanyAreasModel
    {
       
        [TsProperty(Name = "id")]
        [JsonProperty(PropertyName = "id")]
        public long? BAId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "lsId")]
        [JsonProperty(PropertyName = "lsId")]
        public long? LSId { get; set; }

        [TsProperty(Name = "isActive")]
        [JsonProperty(PropertyName = "isActive")]
        public bool IsActive { get; set; }
    }

}

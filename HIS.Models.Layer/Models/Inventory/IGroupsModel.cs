﻿using Newtonsoft.Json;
using TypeLite;
namespace HIS.Models.Layer.Models.Inventory
{
    [TsClass(Name = "IGroupsModel", Module = "Inventory")]
    public class IGroupsModel
    {
        [TsProperty(Name = "id")]
        [JsonProperty(PropertyName = "id")]
        public long? BAId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "parentId")]
        [JsonProperty(PropertyName = "parentId")]
        public long? ParentId { get; set; }


        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }
    }
}

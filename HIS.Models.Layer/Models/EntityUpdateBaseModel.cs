﻿using System.Collections.Generic;
using HIS.Models.Layer.Models.Markers;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models
{
    [TsClass(Name = "EntityUpdateBaseModel", Module = "Entity")]
    public class EntityUpdateBaseModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long? BAId { get; set; }

        [TsProperty(Name = "markerValues")]
        [JsonProperty(PropertyName = "markerValues")]
        public IEnumerable<MarkerValueModel> MarkerValues { get; set; }
    }
}

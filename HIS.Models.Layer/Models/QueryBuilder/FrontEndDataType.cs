﻿namespace HIS.Models.Layer.Models.QueryBuilder
{
    public enum FrontEndDataType
    {
        Number,
        String,
        Boolean,
        Date
    }
}

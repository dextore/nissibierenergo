﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.QueryBuilder
{
    [TsClass(Name = "OrderingModel", Module = "QueryBuilderModel")]
    public class QueryBuilderModel
    {
        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BaTypeId { get; set; }

        [TsProperty(Name = "orderingsFields")]
        [JsonProperty(PropertyName = "orderingsFields")]
        public IEnumerable<OrderingModel> OrderingsFields { get; set; }

        private FilteringGroupOperator? _filteringsGroupOperator;
        [TsProperty(Name = "filteringsGroupOperator")]
        [JsonProperty(PropertyName = "filteringsGroupOperator")]
        public FilteringGroupOperator? FilteringsGroupOperator
        {
            get
            { return this._filteringsGroupOperator ?? (this._filteringsGroupOperator = FilteringGroupOperator.And); }
            set { this._filteringsGroupOperator = value; }

        }


        //TODO: Уйдёт современем, заменится FilteringsTreeFields
        [TsProperty(Name = "filteringsFields")]
        [JsonProperty(PropertyName = "filteringsFields")]
        public IEnumerable<FilteringModel> FilteringsFields { get; set; }




        [TsProperty(Name = "filteringsTreeFields")]
        [JsonProperty(PropertyName = "filteringsTreeFields")]
        public FilteringsTreeField FilteringsTreeFields { get; set; }



        [TsProperty(Name = "from")]
        [JsonProperty(PropertyName = "from")]
        public int From { get; set; }

        [TsProperty(Name = "to")]
        [JsonProperty(PropertyName = "to")]
        public int To { get; set; }

        [TsProperty(Name = "selectedPropertyes")]
        [JsonProperty(PropertyName = "selectedPropertyes")]
        public IEnumerable<string> SelectedPropertyes { get; set; }

        [TsProperty(Name = "needFirstLevelOfTypesHierarchy")]
        [JsonProperty(PropertyName = "needFirstLevelOfTypesHierarchy")]
        public bool NeedFirstLevelOfTypesHierarchy { get; set; }
    }

    public class FilteringsTreeField
    {
        public IEnumerable<FilteringModel> OrFields { get; set; }
        public IEnumerable<FilteringModel> AndFields { get; set; }
    }

    public enum FilteringGroupOperator
    {
        And,
        Or
    }
}

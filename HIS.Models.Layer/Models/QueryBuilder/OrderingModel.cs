﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.QueryBuilder
{
    [TsClass(Name = "OrderingModel", Module = "Querybuilder")]
    public class OrderingModel
    {
        [TsProperty(Name = "orderingField")]
        [JsonProperty(PropertyName = "orderingField")]
        public string OrderingField { get; set; }

        [TsProperty(Name = "sortingOrder")]
        [JsonProperty(PropertyName = "sortingOrder")]
        public SortingOrderModel SortingOrder { get; set; }
    }
    public enum SortingOrderModel
    {
        Asc,
        Desc
    }
}

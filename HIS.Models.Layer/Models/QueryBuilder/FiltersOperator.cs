﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.QueryBuilder
{
    [TsClass(Name = "FilteringModel", Module = "Querybuilder")]
    public class FilteringModel
    {
        [TsProperty(Name = "field")]
        [JsonProperty(PropertyName = "field")]
        public string Field { get;set;}

        [TsProperty(Name = "filterOperator")]
        [JsonProperty(PropertyName = "filterOperator")]
        public FiltersOperator FilterOperator { get; set; }

        [TsProperty(Name = "value")]
        [JsonProperty(PropertyName = "value")]
        public object Value { get; set; }
    }
    
    public enum FiltersOperator
    {
        Equal,
        NoEqual,
        Less,
        More,
        LessEqual,
        MoreEqual,
        Like
    }
}

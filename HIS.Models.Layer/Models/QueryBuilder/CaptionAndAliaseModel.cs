﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.QueryBuilder
{
    [TsClass(Name = "CaptionAndAliaseModel", Module = "Querybuilder")]
    public class CaptionAndAliaseModel
    {
        [TsProperty(Name = "aliase")]
        [JsonProperty(PropertyName = "aliase")]
        public string Aliase { get; set; }
        [TsProperty(Name = "caption")]
        [JsonProperty(PropertyName = "caption")]
        public string Caption { get; set; }
        [TsProperty(Name = "type")]
        [JsonProperty(PropertyName = "type")]
        public FrontEndDataType Type { get; set; }
        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int? MarkerId { get; set; }
        [TsProperty(Name = "markerTypeId")]
        [JsonProperty(PropertyName = "markerTypeId")]
        public int? MarkerTypeId { get; set; }
    }
}
﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Elements
{
    [TsClass(Name = "ElementModel", Module = "Elements")]
    public class ElementModel
    {
        [TsProperty(Name = "elementId")]
        [JsonProperty(PropertyName = "elementId")]
        public long? ElementId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
}
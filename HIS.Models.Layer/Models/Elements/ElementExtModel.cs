﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Elements
{
    [TsClass(Name = "ElementExtModel", Module = "Elements")]
    public class ElementExtModel: ElementModel
    {
        [TsProperty(Name = "elementType")]
        [JsonProperty(PropertyName = "elementType")]
        public string ElementType { get; set; }

        [TsProperty(Name = "elementTypeCode")]
        [JsonProperty(PropertyName = "elementTypeCode")]
        public string ElementTypeCode { get; set; }

        [TsProperty(Name = "address")]
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }
    }
}
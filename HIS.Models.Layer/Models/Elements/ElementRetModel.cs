﻿using Newtonsoft.Json;
using System.Collections.Generic;
using HIS.Models.Layer.Models.Markers;
using TypeLite;

namespace HIS.Models.Layer.Models.Elements
{
    [TsClass(Name = "ElementRetModel", Module = "Elements")]
    public class ElementRetModel : ElementModel
    {
        [TsProperty(Name = "markers")]
        [JsonProperty(PropertyName = "markers")]
        public IEnumerable<MarkerValueModelRet> Markers { get; set; }

        [TsProperty(Name = "schemas")]
        [JsonProperty(PropertyName = "schemas")]
        public IEnumerable<PeriodicMarkerValueModelRet> Schemas { get; set; }
    }
}
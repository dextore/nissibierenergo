﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Documents
{
    [TsClass(Name = "DocumentTreeItemModel", Module = "Documents")]
    public class DocumentTreeItemModel
    {
        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BATypeId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "parentId")]
        [JsonProperty(PropertyName = "parentId")]
        public int? ParentId { get; set; }

        [TsProperty(Name = "treeLevel")]
        [JsonProperty(PropertyName = "treeLevel")]
        public int TreeLevel { get; set; }

        [TsProperty(Name = "branchId")]
        [JsonProperty(PropertyName = "branchId")]
        public int BranchId { get; set; }
    }
}

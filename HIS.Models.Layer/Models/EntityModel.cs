﻿using TypeLite;

namespace HIS.Models.Layer.Models
{
    [TsClass(Name = "EntityModel", Module = "Entity")]
    public class EntityModel: EntityBaseModel 
    {
    }
}

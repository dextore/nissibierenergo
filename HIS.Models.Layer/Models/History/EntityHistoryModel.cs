﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.History
{
    [TsClass(Name = "EntityHistoryModel", Module = "History")]
    public class EntityHistoryModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BAId { get; set; }
        
        [TsProperty(Name = "userId")]
        [JsonProperty(PropertyName = "userId")]
        public int UserId { get; set; }

        [TsProperty(Name = "userName")]
        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; set; }

        [TsProperty(Name = "operationDate")]
        [JsonProperty(PropertyName = "operationDate")]
        public DateTime OperationDate { get; set; }
        
        [TsProperty(Name = "operationName")]
        [JsonProperty(PropertyName = "operationName")]
        public string OperationName { get; set; }

        [TsProperty(Name = "operationId")]
        [JsonProperty(PropertyName = "operationId")]
        public int OperationId { get; set; }

        [TsProperty(Name = "srcStateId")]
        [JsonProperty(PropertyName = "srcStateId")]
        public int? SrcStateId { get; set; }

        [TsProperty(Name = "srcStateName")]
        [JsonProperty(PropertyName = "srcStateName")]
        public string SrcStateName { get; set; }

        [TsProperty(Name = "dstStateId")]
        [JsonProperty(PropertyName = "dstStateId")]
        public int? DstStateId { get; set; }

        [TsProperty(Name = "dstStateName")]
        [JsonProperty(PropertyName = "dstStateName")]
        public string DstStateName { get; set; }

        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int? MarkerId { get; set; }

        [TsProperty(Name = "markerName")]
        [JsonProperty(PropertyName = "markerName")]
        public string MarkerName { get; set; }

        [TsProperty(Name = "markerValue")]
        [JsonProperty(PropertyName = "markerValue")]
        public string MarkerValue { get; set; }

        [TsProperty(Name = "oldMarkerValue")]
        [JsonProperty(PropertyName = "oldMarkerValue")]
        public string OldMarkerValue { get; set; }


        [TsProperty(Name = "markerValueBAId")]
        [JsonProperty(PropertyName = "markerValueBAId")]
        public long? MarkerValueBAId { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime? StartDate { get; set; }

        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }

        [TsProperty(Name = "oldStartDate")]
        [JsonProperty(PropertyName = "oldStartDate")]
        public DateTime? OldStartDate { get; set; }

        [TsProperty(Name = "oldEndDate")]
        [JsonProperty(PropertyName = "oldEndDate")]
        public DateTime? OldEndDate { get; set; }

        [TsProperty(Name = "itemId")]
        [JsonProperty(PropertyName = "itemId")]
        public int? ItemId { get; set; }
        [TsProperty(Name = "recId")]
        [JsonProperty(PropertyName = "recId")]
        public long? RecId { get; set; }

        [TsProperty(Name = "gRecId")]
        [JsonProperty(PropertyName = "gRecId")]
        public long? GRecId { get; set; }

        [TsProperty(Name = "color")]
        [JsonProperty(PropertyName = "color")]
        public long Color { get; set; }
    }
}

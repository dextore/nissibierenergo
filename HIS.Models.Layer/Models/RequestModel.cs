﻿using HIS.Models.Layer.Models.Address;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models
{
    [TsClass(Name = "RequestModel", Module = "Address")]
    public class RequestModel
    {
        [TsProperty(Name = "searchValue")]
        [JsonProperty(PropertyName = "searchValue")]
        public string SearchValue { get; set; }
        [TsProperty(Name = "take")]
        [JsonProperty(PropertyName = "take")]
        public int Take { get; set; }
        [TsProperty(Name = "skip")]
        [JsonProperty(PropertyName = "skip")]
        public int Skip { get; set; }
        [TsProperty(Name = "AOLevel")]
        [JsonProperty(PropertyName = "AOLevel")]
        public AddressObjectLevel? AOLevel { get; set; }
    }
}

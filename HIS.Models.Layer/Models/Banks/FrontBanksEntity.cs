﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Banks
{
    [TsClass(Name = "FrontBanksEntity", Module = "Banks")]
    public class FrontBanksEntity
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long? BaId { get; set; }
        [TsProperty(Name = "bATypeId")]
        [JsonProperty(PropertyName = "bATypeId")]
        public int? BATypeId { get; set; }
        [TsProperty(Name = "parentId")]
        [JsonProperty(PropertyName = "parentId")]
        public int? ParentId { get; set; }
        [TsProperty(Name = "bik")]
        [JsonProperty(PropertyName = "bik")]
        public string Bik { get; set; }
        [TsProperty(Name = "inn")]
        [JsonProperty(PropertyName = "inn")]
        public string Inn { get; set; }
        [TsProperty(Name = "rkc")]
        [JsonProperty(PropertyName = "rkc")]
        public string Rkc { get; set; }
        [TsProperty(Name = "okpo")]
        [JsonProperty(PropertyName = "okpo")]
        public string Okpo { get; set; }

        [TsProperty(Name = "addrId")]
        [JsonProperty(PropertyName = "addrId")]
        public long? AddrId { get; set; }

        [TsProperty(Name = "displayAddr")]
        [JsonProperty(PropertyName = "displayAddr")]
        public string DisplayAddr { get; set; }
        


        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [TsProperty(Name = "corespondetsAccount")]
        [JsonProperty(PropertyName = "corespondetsAccount")]
        public string CorespondetsAccount { get; set; }
        [TsProperty(Name = "acceptancePeriod")]
        [JsonProperty(PropertyName = "acceptancePeriod")]
        public long AcceptancePeriod { get; set; }

        [TsProperty(Name = "bankBranchDisplayValue")]
        [JsonProperty(PropertyName = "bankBranchDisplayValue")]
        public string BankBranchDisplayValue { get; set; }

        //[TsProperty(Name = "changeDate")]
        //[JsonProperty(PropertyName = "changeDate")]
        //public DateTime? ChangeDate { get; set; }

        //[TsProperty(Name = "displayChangeDate")]
        //[JsonProperty(PropertyName = "displayChangeDate")]
        //public string DisplayChangeDate => ((DateTime)ChangeDate).ToString("yyyy-MM-dd");

        //[TsProperty(Name = "actionDate")]
        //[JsonProperty(PropertyName = "actionDate")]
        //public DateTime? ActionDate { get; set; }

        //[TsProperty(Name = "displayActionDate")]
        //[JsonProperty(PropertyName = "displayActionDate")]
        //public string DisplayActionDate => ((DateTime)ChangeDate).ToString("yyyy-MM-dd");

        [TsProperty(Name = "bankStatus")]
        [JsonProperty(PropertyName = "bankStatus")]
        public string Status { get; set; }

        [TsProperty(Name = "markers")]
        [JsonProperty(PropertyName = "markers")]
        public IEnumerable<FrontBankMarker> Markers { get; set; }


    }
}

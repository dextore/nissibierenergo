﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Banks
{
    [TsClass(Name = "FrontBankMarker", Module = "Banks")]
    public class FrontBankMarker
    {

        [TsProperty(Name = "bAId")]
        [JsonProperty(PropertyName = "bAId")]
        public int Baid { get; set; }
        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int MarkerId { get; set; }
        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime StartDate { get; set; }
        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }
        [TsProperty(Name = "value")]
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }
    }
}

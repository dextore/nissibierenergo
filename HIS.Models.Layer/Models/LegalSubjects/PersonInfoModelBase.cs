﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalSubjects
{
    [TsClass(Name = "PersonInfoModalBase", Module = "Persons")]
    public abstract class PersonInfoModalBase
    {
        [TsProperty(Name = "personId")]
        [JsonProperty(PropertyName = "personId")]
        public long PersonId { get; set; }
    }
}

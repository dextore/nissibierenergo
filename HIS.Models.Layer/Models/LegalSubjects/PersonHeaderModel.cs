﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalSubjects
{
    [TsClass(Name = "PersonHeaderModel", Module = "Persons")]
    public class PersonHeaderModel
    {
        [TsProperty(Name = "personId")]
        [JsonProperty(PropertyName = "personId")]
        public long PersonId { get; set; }

        [TsProperty(Name = "fullName")]
        [JsonProperty(PropertyName = "fullName")]
        public string FullName { get; set; }

        [TsProperty(Name = "code")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }
    }
}

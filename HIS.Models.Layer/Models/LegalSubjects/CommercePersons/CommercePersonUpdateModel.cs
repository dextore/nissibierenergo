﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalSubjects.CommercePersons
{
    [TsClass(Name = "CommercePersonUpdateModel", Module = "Commerce")]
    public class CommercePersonUpdateModel: EntityUpdateBaseModel
    {
        [TsProperty(Name = "parentId")]
        [JsonProperty(PropertyName = "parentId")]
        public long? ParentId { get; set; }
    }
}

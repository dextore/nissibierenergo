﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalSubjects.CommercePersons
{
    [TsClass(Name = "CommercePersonModel", Module = "Commerce")]
    public class CommercePersonModel: EntityBaseModel
    {
        [TsProperty(Name = "parentId")]
        [JsonProperty(PropertyName = "parentId")]
        public long? ParentId { get; set; }
    }
}


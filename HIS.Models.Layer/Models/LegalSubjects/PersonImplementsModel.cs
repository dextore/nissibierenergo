﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalSubjects
{
    [TsClass(Name = "PersonImplementsModel", Module = "Persons")]
    public abstract class PersonImplementsModel
    {
        [TsProperty(Name = "personId")]
        [JsonProperty(PropertyName = "personId")]
        public long? PersonId { get; set; }

        [TsProperty(Name = "code")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [TsProperty(Name = "fullName")]
        [JsonProperty(PropertyName = "fullName")]
        public string FullName { get; set; }

        [TsProperty(Name = "firstName")]
        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }

        [TsProperty(Name = "middleName")]
        [JsonProperty(PropertyName = "middleName")]
        public string MiddleName { get; set; }

        [TsProperty(Name = "lastName")]
        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }

        [TsProperty(Name = "birthDate")]
        [JsonProperty(PropertyName = "birthDate")]
        public DateTime? BirthDate { get; set; }

        [TsProperty(Name = "gender")]
        [JsonProperty(PropertyName = "gender")]
        public byte? Gender { get; set; }

        [TsProperty(Name = "inn")]
        [JsonProperty(PropertyName = "inn")]
        public string INN { get; set; }
    }
}

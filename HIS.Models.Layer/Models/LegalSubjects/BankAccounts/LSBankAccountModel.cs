﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalSubjects.BankAccounts
{
    [TsClass(Name = "LSBankAccountModel", Module = "BankAccounts")]
    public class LSBankAccountModel : EntityBaseModel
    {
        [TsProperty(Name = "bankAccountId")]
        [JsonProperty(PropertyName = "bankAccountId")]
        public long BankAccountId { get; set; }

        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int? MarkerId { get; set; }

        [TsProperty(Name = "itemId")]
        [JsonProperty(PropertyName = "itemId")]
        public int? ItemId { get; set; }

        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int StateId { get; set; }

        [TsProperty(Name = "stateName")]
        [JsonProperty(PropertyName = "stateName")]
        public string StateName { get; set; }

        [TsProperty(Name = "number")]
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime? StartDate { get; set; }

        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }

        [TsProperty(Name = "isDefault")]
        [JsonProperty(PropertyName = "isDefault")]
        public bool? IsDefault { get; set; }

        [TsProperty(Name = "bankId")]
        [JsonProperty(PropertyName = "bankId")]
        public long BankId { get; set; }

        [TsProperty(Name = "bankName")]
        [JsonProperty(PropertyName = "bankName")]
        public string BankName { get; set; }
    }
}

﻿using System.Collections.Generic;
using HIS.Models.Layer.Models.Markers;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalSubjects
{
    [TsClass(Name = "PersonModel", Module = "Persons")]
    public class PersonModel: PersonImplementsModel
    {
        [TsProperty(Name = "markerValues")]
        [JsonProperty(PropertyName = "markerValues")]
        public IEnumerable<MarkerValueModel> MarkerValues { get; set; }
    }
}

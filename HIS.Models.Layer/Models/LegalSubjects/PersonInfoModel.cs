﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalSubjects
{
    [TsClass(Name = "PersonInfoModel", Module = "Persons")]
    public class PersonInfoModel : PersonInfoModalBase
    {
        [TsProperty(Name = "markersId")]
        [JsonProperty(PropertyName = "markersId")]
        public IEnumerable<long> MarkersId { get; set; }
    }  
}

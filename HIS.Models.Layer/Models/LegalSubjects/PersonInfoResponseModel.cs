﻿using System.Collections.Generic;
using HIS.Models.Layer.Models.Markers;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalSubjects
{
    [TsClass(Name = "PersonInfoResponseModel", Module = "Persons")]
    public class PersonInfoResponseModel : PersonInfoModalBase
    {
        [TsProperty(Name = "markers")]
        [JsonProperty(PropertyName = "markers")]
        public IEnumerable<MarkerValueModel> Markers { get; set; }
    }
}

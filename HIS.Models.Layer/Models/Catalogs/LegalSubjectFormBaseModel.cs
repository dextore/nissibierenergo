﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Catalogs
{
    [TsClass(Name = "LegalSubjectFormBaseModel", Module = "Catalogs")]
    public abstract class LegalSubjectFormBaseModel
    {
        [TsProperty(Name = "itemId")]
        [JsonProperty(PropertyName = "itemId")]
        public int ItemId { get; set; }

        [TsProperty(Name = "itemName")]
        [JsonProperty(PropertyName = "itemName")]
        public string ItemName { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "shortName")]
        [JsonProperty(PropertyName = "shortName")]
        public string ShortName { get; set; }

        [TsProperty(Name = "isActive")]
        [JsonProperty(PropertyName = "isActive")]
        public bool IsActive { get; set; }

        [TsProperty(Name = "orderNum")]
        [JsonProperty(PropertyName = "orderNum")]
        public int OrderNum { get; set; }
    }
}

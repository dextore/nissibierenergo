﻿using TypeLite;

namespace HIS.Models.Layer.Models.Catalogs
{
    [TsClass(Name = "FAMovReasonsFormModel", Module = "Catalogs")]
    public class FAMovReasonsFormModel : LegalSubjectFormBaseModel
    {
    }
}

﻿using TypeLite;

namespace HIS.Models.Layer.Models.Catalogs
{
    [TsClass(Name = "FALocationsFormModel", Module = "Catalogs")]
    public class FALocationsFormModel : LegalSubjectFormBaseModel
    {
    }
}

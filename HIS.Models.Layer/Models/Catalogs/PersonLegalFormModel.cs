﻿using TypeLite;

namespace HIS.Models.Layer.Models.Catalogs
{
    [TsClass(Name = "PersonLegalFormModel", Module = "Catalogs")]
    public class PersonLegalFormModel : LegalSubjectFormBaseModel
    {
    }
}

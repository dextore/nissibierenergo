﻿using TypeLite;

namespace HIS.Models.Layer.Models.Catalogs
{
    [TsClass(Name = "OrganisationLegalFormModel", Module = "Catalogs")]
    public class OrganisationLegalFormModel: LegalSubjectFormBaseModel
    {
    }
}

﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.ConsignmentNotes
{
    [TsClass(Name = "ConsignmentNotesSpecificationsModel", Module = "ConsignmentNotes")]
    public class ConsignmentNotesSpecificationsModel
    {
        [TsProperty(Name = "pVAT")]
        [JsonProperty(PropertyName = "pVAT")]
        public decimal PVAT { get; set; }
        [TsProperty(Name = "recId")]
        [JsonProperty(PropertyName = "recId")]
        public long RecId { get; set; }
        [TsProperty(Name = "inventoryName")]
        [JsonProperty(PropertyName = "inventoryName")]
        public string InventoryName { get; set; }
        [TsProperty(Name = "consignmentNotesDocumentName")]
        [JsonProperty(PropertyName = "consignmentNotesDocumentName")]
        public string ConsignmentNotesDocumentName { get; set; }
        [TsProperty(Name = "unitName")]
        [JsonProperty(PropertyName = "unitName")]
        public string UnitName { get; set; }

        [TsProperty(Name = "quantity")]
        [JsonProperty(PropertyName = "quantity")]
        public double Quantity { get; set; }
        [TsProperty(Name = "price")]
        [JsonProperty(PropertyName = "price")]
        public decimal Price { get; set; }
        [TsProperty(Name = "priceVAT")]
        [JsonProperty(PropertyName = "priceVAT")]
        public decimal PriceVAT { get; set; }
        [TsProperty(Name = "isVATIncluded")]
        [JsonProperty(PropertyName = "isVATIncluded")]
        public bool IsVATIncluded { get; set; }
        [TsProperty(Name = "vATRate")]
        [JsonProperty(PropertyName = "vATRate")]
        public int VATRate { get; set; }
        [TsProperty(Name = "cost")]
        [JsonProperty(PropertyName = "cost")]
        public decimal Cost { get; set; }
        [TsProperty(Name = "vAT")]
        [JsonProperty(PropertyName = "vAT")]
        public decimal VAT { get; set; }
        [TsProperty(Name = "costVAT")]
        [JsonProperty(PropertyName = "costVAT")]
        public decimal CostVAT { get; set; }
    }

    [TsClass(Name = "ConsignmentNotesSpecificationsUpdateModel", Module = "ConsignmentNotes")]
    public class ConsignmentNotesSpecificationsUpdateModel
    {
        [TsProperty(Name = "pVAT")]
        [JsonProperty(PropertyName = "pVAT")]
        public decimal PVAT { get; set; }
        [TsProperty(Name = "recId")]
        [JsonProperty(PropertyName = "recId")]
        public long? RecId { get; set; }
        [TsProperty(Name = "inventoryId")]
        [JsonProperty(PropertyName = "inventoryId")]
        public long InventoryId { get; set; }
        [TsProperty(Name = "docId")]
        [JsonProperty(PropertyName = "docId")]
        public long DocId { get; set; }
        [TsProperty(Name = "unitId")]
        [JsonProperty(PropertyName = "unitId")]
        public long UnitId { get; set; }

        [TsProperty(Name = "quantity")]
        [JsonProperty(PropertyName = "quantity")]
        public double Quantity { get; set; }
        [TsProperty(Name = "price")]
        [JsonProperty(PropertyName = "price")]
        public decimal Price { get; set; }
        [TsProperty(Name = "priceVAT")]
        [JsonProperty(PropertyName = "priceVAT")]
        public decimal PriceVAT { get; set; }
        [TsProperty(Name = "isVATIncluded")]
        [JsonProperty(PropertyName = "isVATIncluded")]
        public bool IsVATIncluded { get; set; }
        [TsProperty(Name = "vATRate")]
        [JsonProperty(PropertyName = "vATRate")]
        public int VATRate { get; set; }
        [TsProperty(Name = "cost")]
        [JsonProperty(PropertyName = "cost")]
        public decimal Cost { get; set; }
        [TsProperty(Name = "vAT")]
        [JsonProperty(PropertyName = "vAT")]
        public decimal VAT { get; set; }
        [TsProperty(Name = "costVAT")]
        [JsonProperty(PropertyName = "costVAT")]
        public decimal CostVAT { get; set; }
    }

    [TsClass(Name = "ConsignmentNotesSpecificationsFullModel", Module = "ConsignmentNotes")]
    public class ConsignmentNotesSpecificationsFullModel : ConsignmentNotesSpecificationsModel
    {
        [TsProperty(Name = "inventoryId")]
        [JsonProperty(PropertyName = "inventoryId")]
        public long InventoryId { get; set; }
        [TsProperty(Name = "docId")]
        [JsonProperty(PropertyName = "docId")]
        public long DocId { get; set; }
        [TsProperty(Name = "unitId")]
        [JsonProperty(PropertyName = "unitId")]
        public long UnitId { get; set; }
    }
}

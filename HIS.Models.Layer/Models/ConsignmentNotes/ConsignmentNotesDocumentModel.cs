﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.ConsignmentNotes
{
    [TsClass(Name = "ConsignmentNotesDocumentModel", Module = "ConsignmentNotes")]
    public class ConsignmentNotesDocumentModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long? BAId { get; set; }
        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BaTypeId { get; set; }
        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int StateId { get; set; }
        [TsProperty(Name = "stateDisplay")]
        [JsonProperty(PropertyName = "stateDisplay")]
        public string StateDisplay { get; set; }
        [TsProperty(Name = "number")]
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }
        [TsProperty(Name = "docDate")]
        [JsonProperty(PropertyName = "docDate")]
        public DateTime? DocDate { get; set; }
        [TsProperty(Name = "companyAreaName")]
        [JsonProperty(PropertyName = "companyAreaName")]
        public string CompanyAreaName { get; set; }
        [TsProperty(Name = "legalSubjectsName")]
        [JsonProperty(PropertyName = "legalSubjectsName")]
        public string LegalSubjectsName { get; set; }
        [TsProperty(Name = "contractName")]
        [JsonProperty(PropertyName = "contractName")]
        public string ContractName { get; set; }
        [TsProperty(Name = "warehouseName")]
        [JsonProperty(PropertyName = "warehouseName")]
        public string WarehouseName { get; set; }
        [TsProperty(Name = "cost")]
        [JsonProperty(PropertyName = "cost")]
        public decimal? Cost { get; set; }
        [TsProperty(Name = "vAT")]
        [JsonProperty(PropertyName = "vAT")]
        public decimal? VAT { get; set; }
        [TsProperty(Name = "costVAT")]
        [JsonProperty(PropertyName = "costVAT")]
        public decimal? CostVAT { get; set; }
        [TsProperty(Name = "userName")]
        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; set; }
        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }
    }

    [TsClass(Name = "ConsignmentNotesDocumentUpdateModel", Module = "ConsignmentNotes")]
    public class ConsignmentNotesDocumentUpdateModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long? BAId { get; set; }
        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BaTypeId { get; set; }
        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int StateId { get; set; }
        [TsProperty(Name = "number")]
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }
        [TsProperty(Name = "docDate")]
        [JsonProperty(PropertyName = "docDate")]
        public DateTime? DocDate { get; set; }
        [TsProperty(Name = "cAId")]
        [JsonProperty(PropertyName = "cAId")]
        public long? CAId { get; set; }
        [TsProperty(Name = "lSId")]
        [JsonProperty(PropertyName = "lSId")]
        public long? LSId { get; set; }
        [TsProperty(Name = "contractId")]
        [JsonProperty(PropertyName = "contractId")]
        public long? ContractId { get; set; }
        [TsProperty(Name = "wHId")]
        [JsonProperty(PropertyName = "wHId")]
        public long? WHId { get; set; }
        [TsProperty(Name = "cost")]
        [JsonProperty(PropertyName = "cost")]
        public decimal? Cost { get; set; }
        [TsProperty(Name = "vAT")]
        [JsonProperty(PropertyName = "vAT")]
        public decimal? VAT { get; set; }
        [TsProperty(Name = "costVAT")]
        [JsonProperty(PropertyName = "costVAT")]
        public decimal? CostVAT { get; set; }
        [TsProperty(Name = "userId")]
        [JsonProperty(PropertyName = "userId")]
        public int? UserId { get; set; }
        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }
    }

    [TsClass(Name = "ConsignmentNotesDocumentFullModel", Module = "ConsignmentNotes")]
    public class ConsignmentNotesDocumentFullModel : ConsignmentNotesDocumentModel
    {
        [TsProperty(Name = "companyAreaId")]
        [JsonProperty(PropertyName = "companyAreaId")]
        public string CompanyAreaId { get; set; }
        [TsProperty(Name = "legalSubjectId")]
        [JsonProperty(PropertyName = "legalSubjectId")]
        public string LegalSubjectId { get; set; }
        [TsProperty(Name = "contractId")]
        [JsonProperty(PropertyName = "contractId")]
        public string ContractId { get; set; }
        [TsProperty(Name = "whWarehousesId")]
        [JsonProperty(PropertyName = "whWarehousesId")]
        public string WhWarehousesId { get; set; }
    }
}

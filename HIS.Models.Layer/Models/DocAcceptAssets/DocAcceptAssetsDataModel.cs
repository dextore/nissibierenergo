﻿using Newtonsoft.Json;
using TypeLite;
using HIS.Models.Layer.Models.Markers;
using System;
using System.Collections.Generic;

namespace HIS.Models.Layer.Models.DocAcceptAssets
{
    [TsClass(Name = "IDocAcceptAssetsData", Module = "DocAcceptAssets")]
    public class DocAcceptAssetsDataModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long? BAId { get; set; }

        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int? BaTypeId { get; set; }

        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int? StateId { get; set; }

        [TsProperty(Name = "number")]
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        [TsProperty(Name = "docDate")]
        [JsonProperty(PropertyName = "docDate")]
        public DateTime? DocDate { get; set; }

        [TsProperty(Name = "riId")]
        [JsonProperty(PropertyName = "riId")]
        public long? RIId { get; set; }

        [TsProperty(Name = "riName")]
        [JsonProperty(PropertyName = "riName")]
        public string RIName { get; set; }

        [TsProperty(Name = "lsId")]
        [JsonProperty(PropertyName = "lsId")]
        public long? LSId { get; set; }

        [TsProperty(Name = "lsName")]
        [JsonProperty(PropertyName = "lsName")]
        public string LSName { get; set; }

        [TsProperty(Name = "whId")]
        [JsonProperty(PropertyName = "whId")]
        public long? WHId { get; set; }

        [TsProperty(Name = "whName")]
        [JsonProperty(PropertyName = "whName")]
        public string WHName { get; set; }

        [TsProperty(Name = "caId")]
        [JsonProperty(PropertyName = "caId")]
        public long? CAId { get; set; }

        [TsProperty(Name = "caName")]
        [JsonProperty(PropertyName = "caName")]
        public string CAName { get; set; }

        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [TsProperty(Name = "evidence")]
        [JsonProperty(PropertyName = "evidence")]
        public string Evidence { get; set; }

        [TsProperty(Name = "hasSpecification")]
        [JsonProperty(PropertyName = "hasSpecification")]
        public bool HasSpecification { get; set; }

        [TsProperty(Name = "markerValues")]
        [JsonProperty(PropertyName = "markerValues")]
        public IEnumerable<MarkerValueModel> MarkerValues { get; set; }
    }
}

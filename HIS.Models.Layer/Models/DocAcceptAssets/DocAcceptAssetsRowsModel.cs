﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.DocAcceptAssets {

    [TsClass(Name = "IDocAcceptAssetsRows", Module = "DocAcceptAssets")]
    public class DocAcceptAssetsRowsModel
    {
        [TsProperty(Name = "recId")]
        [JsonProperty(PropertyName = "recId")]
        public long? RecId { get; set; }
        
        [TsProperty(Name = "docAcceptId")]
        [JsonProperty(PropertyName = "docAcceptId")]
        public long FADocAcceptId { get; set; }
        
        [TsProperty(Name = "inventoryId")]
        [JsonProperty(PropertyName = "inventoryId")]
        public long InventoryId { get; set; }

        [TsProperty(Name = "inventoryName")]
        [JsonProperty(PropertyName = "inventoryName")]
        public string InventoryName { get; set; }

        [TsProperty(Name = "inventoryNumber")]
        [JsonProperty(PropertyName = "inventoryNumber")]
        public string InventoryNumber { get; set; }

        [TsProperty(Name = "serialNumber")]
        [JsonProperty(PropertyName = "serialNumber")]
        public string SerialNumber { get; set; }
        
        [TsProperty(Name = "releaseDate")]
        [JsonProperty(PropertyName = "releaseDate")]
        public DateTime? ReleaseDate { get; set; }

        [TsProperty(Name = "checkDate")]
        [JsonProperty(PropertyName = "checkDate")]
        public DateTime? CheckDate { get; set; }
     
        [TsProperty(Name = "barCode")]
        [JsonProperty(PropertyName = "barCode")]
        public string BarCode { get; set; }

        [TsProperty(Name = "employeeId")]
        [JsonProperty(PropertyName = "employeeId")]
        public long? EmployeeId { get; set; }

        [TsProperty(Name = "faId")]
        [JsonProperty(PropertyName = "faId")]
        public long? FAId { get; set; }



    }


}


﻿using Newtonsoft.Json;
using TypeLite;


namespace HIS.Models.Layer.Models.DocAcceptAssets
{
    [TsClass(Name = "IReceiptInvoiceSelectModel", Module = "DocAcceptAssets")]
    public class ReceiptInvoiceSelectModel
    {
        [TsProperty(Name = "id")]
        [JsonProperty(PropertyName = "id")]
        public long BAId { get; set; }

        [TsProperty(Name = "value")]
        [JsonProperty(PropertyName = "value")]
        public string Number { get; set; }
    }
}

﻿using Newtonsoft.Json;
using TypeLite;


namespace HIS.Models.Layer.Models.DocAcceptAssets
{
    [TsClass(Name = "ReceiptInvoiceInventSelectModel", Module = "DocAcceptAssets")]
    public class ReceiptInvoiceInventSelectModel
    {
        [TsProperty(Name = "docId")]
        [JsonProperty(PropertyName = "docId")]
        public long DocId { get; set; }

        [TsProperty(Name = "id")]
        [JsonProperty(PropertyName = "id")]
        public long InventoryId { get; set; }
        
        [TsProperty(Name = "caId")]
        [JsonProperty(PropertyName = "caId")]
        public long CAId { get; set; }
        
        [TsProperty(Name = "code")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [TsProperty(Name = "value")]
        [JsonProperty(PropertyName = "value")]
        public string Name { get; set; }

        [TsProperty(Name = "quantity")]
        [JsonProperty(PropertyName = "quantity")]
        public double? Quantity { get; set; }



    }
}

﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Tests
{
    [TsClass(Name = "TestDataRequestModel", Module = "Test")]
    public class TestDataRequestModel
    {
        [TsProperty(Name = "isMessage")]
        [JsonProperty(PropertyName = "isMessage")]
        public bool IsMessage { get; set; }

        [TsProperty(Name = "value")]
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }
}

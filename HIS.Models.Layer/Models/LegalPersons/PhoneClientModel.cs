﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalPersons
{
    [TsClass(Name = "PhoneClientModel", Module = "LegalPersons")]
    public class PhoneClientModel
    {
        [TsProperty(Name = "phoneId")]
        [JsonProperty(PropertyName = "phoneId")]
        public long PhoneId { get; set; }

        [TsProperty(Name = "number")]
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        [TsProperty(Name = "phoneTypeId")]
        [JsonProperty(PropertyName = "phoneTypeId")]
        public int PhoneTypeId { get; set; }

        [TsProperty(Name = "firstName")]
        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }

        [TsProperty(Name = "middleName")]
        [JsonProperty(PropertyName = "middleName")]
        public string MiddleName { get; set; }

        [TsProperty(Name = "lastName")]
        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }

        [TsProperty(Name = "sendConfirmTypeId")]
        [JsonProperty(PropertyName = "sendConfirmTypeId")]
        public int? SendConfirmTypeId { get; set; }

        [TsProperty(Name = "mailTypes")]
        [JsonProperty(PropertyName = "mailTypes")]
        public IEnumerable<MailTypeModel> MailTypes { get; set; }

        [TsProperty(Name = "deleted")]
        [JsonProperty(PropertyName = "deleted")]
        public bool Deleted { get; set; }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalPersons
{
    [TsClass(Name = "ContactModel", Module = "LegalPersons")]
    public class ContactModel
    {
        [TsProperty(Name = "contactId")]
        [JsonProperty(PropertyName = "contactId")]
        public long? ContactId { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime StartDate { get; set; }

        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime EndDate { get; set; }

        [TsProperty(Name = "contactTypeId")]
        [JsonProperty(PropertyName = "contactTypeId")]
        public int ContactTypeId { get; set; }

        [TsProperty(Name = "contactTypeName")]
        [JsonProperty(PropertyName = "contactTypeName")]
        public string ContactTypeName { get; set; }

        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [TsProperty(Name = "phones")]
        [JsonProperty(PropertyName = "phones")]
        public IEnumerable<PhoneModel> Phones { get; set; }

        [TsProperty(Name = "emails")]
        [JsonProperty(PropertyName = "emails")]
        public IEnumerable<EmailModel> Emails { get; set; }
    }
}
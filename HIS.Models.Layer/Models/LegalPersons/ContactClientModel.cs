﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalPersons
{
    [TsClass(Name = "ContactClientModel", Module = "LegalPersons")]
    public class ContactClientModel
    {
        [TsProperty(Name = "contactId")]
        [JsonProperty(PropertyName = "contactId")]
        public long ContactId { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime StartDate { get; set; }

        [TsProperty(Name = "contactTypeId")]
        [JsonProperty(PropertyName = "contactTypeId")]
        public int ContactTypeId { get; set; }

        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [TsProperty(Name = "deleted")]
        [JsonProperty(PropertyName = "deleted")]
        public bool Deleted { get; set; }

        [TsProperty(Name = "phones")]
        [JsonProperty(PropertyName = "phones")]
        public IEnumerable<PhoneClientModel> Phones { get; set; }

        [TsProperty(Name = "emails")]
        [JsonProperty(PropertyName = "emails")]
        public IEnumerable<EmailClientModel> Emails { get; set; }
    }
}
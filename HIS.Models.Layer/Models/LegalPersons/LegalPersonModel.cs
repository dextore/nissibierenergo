﻿using HIS.Models.Layer.Models.Markers;
using System.Collections.Generic;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalPersons
{
    [TsClass(Name = "LegalPersonModel", Module = "LegalPersons")]
    public class LegalPersonModel: LegalPersonImplementedModel
    {
        public IEnumerable<MarkerValueModel> Markers { get; set; }
    }
}
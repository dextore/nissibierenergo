﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalPersons
{
    [TsClass(Name = "MailTypeModel", Module = "LegalPersons")]
    public class MailTypeModel
    {
        [TsProperty(Name = "markerItemId")]
        [JsonProperty(PropertyName = "markerItemId")]
        public int MarkerItemId { get; set; }

        [TsProperty(Name = "value")]
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [TsProperty(Name = "itemName")]
        [JsonProperty(PropertyName = "itemName")]
        public string ItemName { get; set; }
    }
}
﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalPersons
{
    [TsClass(Name = "LegalPersonCollectionModel", Module = "LegalPersons")]
    public class LegalPersonCollectionModel
    {
        [TsProperty(Name = "items")]
        [JsonProperty(PropertyName = "items")]
        public IEnumerable<LegalPersonItemModel> Items { get; set; }

        [TsProperty(Name = "totalCount")]
        [JsonProperty(PropertyName = "totalCount")]
        public int TotalCount { get; set; }
    }
}
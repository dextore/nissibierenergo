﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalPersons
{
    [TsClass(Name = "EmailClientModel", Module = "LegalPersons")]
    public class EmailClientModel
    {
        [TsProperty(Name = "emailId")]
        [JsonProperty(PropertyName = "emailId")]
        public long EmailId { get; set; }

        [TsProperty(Name = "email")]
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [TsProperty(Name = "firstName")]
        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }

        [TsProperty(Name = "middleName")]
        [JsonProperty(PropertyName = "middleName")]
        public string MiddleName { get; set; }

        [TsProperty(Name = "lastName")]
        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }

        [TsProperty(Name = "mailConfirmTypeId")]
        [JsonProperty(PropertyName = "mailConfirmTypeId")]
        public int? MailConfirmTypeId { get; set; }

        [TsProperty(Name = "mailTypes")]
        [JsonProperty(PropertyName = "mailTypes")]
        public IEnumerable<MailTypeModel> MailTypes { get; set; }

        [TsProperty(Name = "deleted")]
        [JsonProperty(PropertyName = "deleted")]
        public bool Deleted { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalPersons
{
    [TsClass(Name = "LegalPersonImplementedModel", Module = "LegalPersons")]
    public class LegalPersonImplementedModel
    {
        [TsProperty(Name = "legalPersonId")]
        [JsonProperty(PropertyName = "legalPersonId")]
        public long? LegalPersonId { get; set; }

        [Required]
        [TsProperty(Name = "code")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [Required]
        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [Required]
        [TsProperty(Name = "fullName")]
        [JsonProperty(PropertyName = "fullName")]
        public string FullName { get; set; }

        [TsProperty(Name = "inn")]
        [JsonProperty(PropertyName = "inn")]
        public string INN { get; set; }

        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BATypeId { get; set; }
    }
}

﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalPersons
{
    [TsClass(Name = "LegalPersonItemModel", Module = "LegalPersons")]
    public class LegalPersonItemModel
    {
        [TsProperty(Name = "legalPersonId")]
        [JsonProperty(PropertyName = "legalPersonId")]
        public long LegalPersonId { get; set; }

        [TsProperty(Name = "code")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "fullName")]
        [JsonProperty(PropertyName = "fullName")]
        public string FullName { get; set; }
    }
}
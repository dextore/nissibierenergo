﻿using System.Collections.Generic;
using HIS.Models.Layer.Models.Markers;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalPersons
{
    [TsClass(Name = "AffiliateOrganisationModel", Module = "LegalPersons")]
    public class AffiliateOrganisationModel: LegalPersonImplementedModel
    {
        [TsProperty(Name = "parentId")]
        [JsonProperty(PropertyName = "parentId")]
        public long ParentId { get; set; }

        [TsProperty(Name = "markerValues")]
        [JsonProperty(PropertyName = "markerValues")]
        public IEnumerable<MarkerValueModel> MarkerValues { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System.Collections.Generic;
using HIS.Models.Layer.Models.Markers;
using TypeLite;

namespace HIS.Models.Layer.Models.LegalPersons
{
    [TsClass(Name = "LegalPersonInfoModel", Module = "LegalPersons")]
    public class LegalPersonInfoModel
    {
        [TsProperty(Name = "legalPersonId")]
        [JsonProperty(PropertyName = "legalPersonId")]
        public long? LegalPersonId { get; set; }

        [TsProperty(Name = "code")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "fullName")]
        [JsonProperty(PropertyName = "fullName")]
        public string FullName { get; set; }

        [TsProperty(Name = "markers")]
        [JsonProperty(PropertyName = "markers")]
        public IEnumerable<MarkerValueModel> Markers { get; set; }

        [TsProperty(Name = "schemas")]
        [JsonProperty(PropertyName = "schemas")]
        public IEnumerable<MarkerValueModel> Schemas { get; set; }

        [TsProperty(Name = "contacts")]
        [JsonProperty(PropertyName = "contacts")]
        public IEnumerable<ContactModel> Contacts { get; set; }
    }
}
﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Contracts
{
    [TsClass(Name = "ContractNomenclatureTermModel", Module = "Contracts")]
    public class ContractNomenclatureTermModel: TermBaseModel
    {
        [TsProperty(Name = "contractNomenclatureId")]
        [JsonProperty(PropertyName = "contractNomenclatureId")]
        public long ContractNomenclatureId { get; set; }
    }
}
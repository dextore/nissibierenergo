﻿using Newtonsoft.Json;
using System;
using TypeLite;

namespace HIS.Models.Layer.Models.Contracts
{
    [TsClass(Name = "ContractNomenclatureSaveModel", Module = "Contracts")]
    public class ContractNomenclatureSaveModel
    {
        [TsProperty(Name = "contractNomenclatureId")]
        [JsonProperty(PropertyName = "contractNomenclatureId")]
        public long? ContractNomenclatureId { get; set; }

        [TsProperty(Name = "contractId")]
        [JsonProperty(PropertyName = "contractId")]
        public long ContractId { get; set; }

        [TsProperty(Name = "supplierId")]
        [JsonProperty(PropertyName = "supplierId")]
        public long SupplierId { get; set; }

        [TsProperty(Name = "nomenclatureId")]
        [JsonProperty(PropertyName = "nomenclatureId")]
        public long NomenclatureId { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime StartDate { get; set; }

        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime EndDate { get; set; }
    }
}
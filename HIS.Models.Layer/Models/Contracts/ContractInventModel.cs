﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Contracts
{
    [TsClass(Name = "ContractInventModel", Module = "ContractInvents")]
    public class ContractInventModel
    {
        [TsProperty(Name = "orderNum")]
        [JsonProperty(PropertyName = "orderNum")]
        public int? OrderNum { get; set; }

        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BAId { get; set; }

        [TsProperty(Name = "contractId")]
        [JsonProperty(PropertyName = "contractId")]
        public long ContractId { get; set; }

        [TsProperty(Name = "inventId")]
        [JsonProperty(PropertyName = "inventId")]
        public long InventId { get; set; }

        [TsProperty(Name = "inventName")]
        [JsonProperty(PropertyName = "inventName")]
        public string InventName { get; set; }

        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int StateId { get; set; }

        [TsProperty(Name = "stateName")]
        [JsonProperty(PropertyName = "stateName")]
        public string StateName { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime? StartDate { get; set; }

        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }

        [TsProperty(Name = "userId")]
        [JsonProperty(PropertyName = "userId")]
        public int? UserId { get; set; }

        [TsProperty(Name = "userName")]
        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; set; }
    }
}

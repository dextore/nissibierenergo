﻿using HIS.Models.Layer.Models.Contracts;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Contracts
{
    [TsClass(Name = "ContractElementTermModel", Module = "Contracts")]
    public class ContractElementTermModel: TermBaseModel
    {
        [TsProperty(Name = "contractElementId")]
        [JsonProperty(PropertyName = "contractElementId")]
        public long ContractElementId { get; set; }
    }
}
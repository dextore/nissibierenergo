﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Contracts
{
    [TsClass(Name = "LSContractModel", Module = "Contracts")]
    public class LSContractModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BAId { get; set; }

        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BATypeId { get; set; }

        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int StateId { get; set; }

        [TsProperty(Name = "number")]
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        [TsProperty(Name = "docDate")]
        [JsonProperty(PropertyName = "docDate")]
        public DateTime? DocDate { get; set; }

        [TsProperty(Name = "caId")]
        [JsonProperty(PropertyName = "caId")]
        public long CAId { get; set; }

        [TsProperty(Name = "lsId")]
        [JsonProperty(PropertyName = "lsId")]
        public long? LSId { get; set; }

        [TsProperty(Name = "typeId")]
        [JsonProperty(PropertyName = "typeId")]
        public int? TypeId { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime? StartDate { get; set; }

        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }

        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [TsProperty(Name = "creatorId")]
        [JsonProperty(PropertyName = "creatorId")]
        public int CreatorId { get; set; }

        [TsProperty(Name = "ukGroup")]
        [JsonProperty(PropertyName = "ukGroup")]
        public string UKGroup { get; set; }

        [TsProperty(Name = "typeName")]
        [JsonProperty(PropertyName = "typeName")]
        public string TypeName { get; set; }

        [TsProperty(Name = "state")]
        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }

        [TsProperty(Name = "isDelState")]
        [JsonProperty(PropertyName = "isDelState")]
        public bool IsDelState { get; set; }

        [TsProperty(Name = "isFirstState")]
        [JsonProperty(PropertyName = "isFirstState")]
        public bool IsFirstState { get; set; }

        [TsProperty(Name = "companyAreaName")]
        [JsonProperty(PropertyName = "companyAreaName")]
        public string CompanyAreaName { get; set; }
    }
}

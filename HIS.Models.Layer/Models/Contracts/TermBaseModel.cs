﻿using Newtonsoft.Json;
using System;
using HIS.Models.Layer.Models.Markers;
using TypeLite;

namespace HIS.Models.Layer.Models.Contracts
{
    [TsClass(Name = "TermBaseModel", Module = "Contracts")]
    public abstract class TermBaseModel: MarkerValueModelBase
    {
        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime? StartDate { get; set; }

        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }

        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }
    }
}
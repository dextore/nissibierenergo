﻿using HIS.Models.Layer.Models.Elements;
using Newtonsoft.Json;
using System;
using TypeLite;

namespace HIS.Models.Layer.Models.Contracts
{
    [TsClass(Name = "ContractElementModel", Module = "Contracts")]
    public class ContractElementModel: ElementExtModel
    {
        [TsProperty(Name = "contractElementId")]
        [JsonProperty(PropertyName = "contractElementId")]
        public long? ContractElementId { get; set; }

        [TsProperty(Name = "contractId")]
        [JsonProperty(PropertyName = "contractId")]
        public long ContractId { get; set; }

        [TsProperty(Name = "nomenclatureId")]
        [JsonProperty(PropertyName = "nomenclatureId")]
        public long NomenclatureId { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime StartDate { get; set; }

        [TsProperty(Name = "parentId")]
        [JsonProperty(PropertyName = "parentId")]
        public long? ParentId { get; set; }

        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }

        [TsProperty(Name = "isProject")]
        [JsonProperty(PropertyName = "isProject")]
        public bool? IsProject { get; set; }

        [TsProperty(Name = "elementCode")]
        [JsonProperty(PropertyName = "elementCode")]
        public string ElementCode { get; set; }

        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [TsProperty(Name = "addrId")]
        [JsonProperty(PropertyName = "addrId")]
        public long? AddrId { get; set; }

        [TsProperty(Name = "orderNum")]
        [JsonProperty(PropertyName = "orderNum")]
        public int OrderNum { get; set; }

        [TsProperty(Name = "hasItems")]
        [JsonProperty(PropertyName = "hasItems")]
        public bool HasItems { get; set; }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using TypeLite;

namespace HIS.Models.Layer.Models.Contracts
{
    [TsClass(Name = "ContractModel", Module = "Contracts")]
    public class ContractModel
    {
        [TsProperty(Name = "contractId")]
        [JsonProperty(PropertyName = "contractId")]
        public long? ContractId { get; set; }

        [TsProperty(Name = "number")]
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime StartDate { get; set; }

        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }

        [TsProperty(Name = "supplierId")]
        [JsonProperty(PropertyName = "supplierId")]
        public long? SupplierId { get; set; }

        [TsProperty(Name = "consumerId")]
        [JsonProperty(PropertyName = "consumerId")]
        public long? ConsumerId { get; set; }

        [TsProperty(Name = "terms")]
        [JsonProperty(PropertyName = "terms")]
        public IEnumerable<ContractTermModel> Terms { get; set; }
    }
}
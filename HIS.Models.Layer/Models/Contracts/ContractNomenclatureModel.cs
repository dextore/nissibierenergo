﻿using HIS.Models.Layer.Models.Nomenclatures;
using Newtonsoft.Json;
using System;
using TypeLite;

namespace HIS.Models.Layer.Models.Contracts
{
    [TsClass(Name = "ContractNomenclatureModel", Module = "Contracts")]
    public class ContractNomenclatureModel : NomenclatureModel
    {
        [TsProperty(Name = "contractNomenclatureId")]
        [JsonProperty(PropertyName = "contractNomenclatureId")]
        public long ContractNomenclatureId { get; set; }

        [TsProperty(Name = "contractId")]
        [JsonProperty(PropertyName = "contractId")]
        public long ContractId { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime StartDate { get; set; }

        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime EndDate { get; set; }

        [TsProperty(Name = "isProject")]
        [JsonProperty(PropertyName = "isProject")]
        public bool? IsProject { get; set; }
    }
}
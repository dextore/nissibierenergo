﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Contracts
{
    [TsClass(Name = "ContractInfoModel", Module = "Contracts")]
    public class ContractInfoModel: ContractModel
    {
        [TsProperty(Name = "supplierName")]
        [JsonProperty(PropertyName = "supplierName")]
        public string SupplierName { get; set; }

        [TsProperty(Name = "consumerName")]
        [JsonProperty(PropertyName = "consumerName")]
        public string ConsumerName { get; set; }
    }
}

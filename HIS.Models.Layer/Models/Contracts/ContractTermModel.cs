﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Contracts
{
    [TsClass(Name = "ContractTermModel", Module = "Contracts")]
    public class ContractTermModel: TermBaseModel
    {
        [TsProperty(Name = "contractId")]
        [JsonProperty(PropertyName = "contractId")]
        public long ContractId { get; set; }

    }
}
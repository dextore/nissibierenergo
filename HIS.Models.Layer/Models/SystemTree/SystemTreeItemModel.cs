﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.SystemTree
{
    [TsClass(Name = "SystemTreeItemModel", Module = "Tree")]
    public class SystemTreeItemModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BAId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public long BATypeId { get; set; }

        [TsProperty(Name = "mvcAlias")]
        [JsonProperty(PropertyName = "mvcAlias")]
        public string MVCAlias { get; set; }

        [TsProperty(Name = "baTypeName")]
        [JsonProperty(PropertyName = "baTypeName")]
        public string BATypeName { get; set; }

        [TsProperty(Name = "baTypeNameShort")]
        [JsonProperty(PropertyName = "baTypeNameShort")]
        public string BATypeNameShort { get; set; }

        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public long StateId { get; set; }

        [TsProperty(Name = "stateName")]
        [JsonProperty(PropertyName = "stateName")]
        public string StateName { get; set; }

        [TsProperty(Name = "children")]
        [JsonProperty(PropertyName = "children")]
        public IEnumerable<SystemTreeItemModel> Children { get; set; }

    }
}

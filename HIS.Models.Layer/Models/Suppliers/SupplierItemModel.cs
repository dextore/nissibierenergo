﻿using HIS.Models.Layer.Models.LegalPersons;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Suppliers
{
    [TsClass(Name = "SupplierItemModel", Module = "Suppliers")]
    public class SupplierItemModel: LegalPersonItemModel
    {
        [TsProperty(Name = "supplierId")]
        [JsonProperty(PropertyName = "supplierId")]
        public long SupplierId { get; set; }
    }
}
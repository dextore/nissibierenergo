﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Entity
{
    [TsClass(Name = "FullTextSearchEntityInfoModel", Module = "Entity")]
    public class FullTextSearchEntityInfoModel
    {
        /// <summary>
        /// ИД сущности
        /// </summary>
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BaId { get; set; }
        /// <summary>
        /// ИД типа сущности
        /// </summary>
        [TsProperty(Name = "typeId")]
        [JsonProperty(PropertyName = "typeId")]
        public int TypeId { get; set; }
        /// <summary>
        /// Наименование типа сущности
        /// </summary>
        [TsProperty(Name = "typeName")]
        [JsonProperty(PropertyName = "typeName")]
        public string TypeName { get; set; }
        /// <summary>
        /// ИД состояния сущности
        /// </summary>
        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int StateId { get; set; }
        /// <summary>
        /// Наименование состояния сущности
        /// </summary>
        [TsProperty(Name = "stateName")]
        [JsonProperty(PropertyName = "stateName")]
        public string StateName { get; set; }
        /// <summary>
        /// Дата начала действия имени сущности
        /// </summary>
        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Дата окончания действия имени сущности
        /// </summary>
        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime EndDate { get; set; }
        /// <summary>
        /// Наименование сущности
        /// </summary>
        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
}
﻿namespace HIS.Models.Layer.Models.Entity
{
    //
    // Типы сущностей в "деревьях" системы
    //
    public enum EntityTreeType
    {
    // Рутовая сущность в системе
        SoRoot = 0x0101,
        // Элемент системы (реальная сущность, ориентироваться по typeId)
        SoElement = 0x0102,
        // Группа элементов (виртуальная сущность, не заведенная в БД)
        SoGroup = 0x0103
    }
}

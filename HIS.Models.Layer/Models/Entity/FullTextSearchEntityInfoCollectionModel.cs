﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Entity
{
    [TsClass(Name = "FullTextSearchEntityInfoCollectionModel", Module = "Entity")]
    public class FullTextSearchEntityInfoCollectionModel
    {
        [TsProperty(Name = "items")]
        [JsonProperty(PropertyName = "items")]
        public IEnumerable<FullTextSearchEntityInfoModel> Items { get; set; }
    }
}
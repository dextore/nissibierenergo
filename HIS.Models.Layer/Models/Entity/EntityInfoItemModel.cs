﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Entity
{
    [TsClass(Name = "EntityInfoItemModel", Module = "Entity")]
    public class EntityInfoItemModel
    {
        /// <summary>
        /// ИД сущности
        /// </summary>
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BaId { get; set; }
        /// <summary>
        /// Тип сущности в "дереве" системы
        /// </summary>
        [TsProperty(Name = "treeType")]
        [JsonProperty(PropertyName = "treeType")]
        public EntityTreeType TreeType { get; set; }
        /// <summary>
        /// ИД типа сущности
        /// </summary>
        [TsProperty(Name = "typeId")]
        [JsonProperty(PropertyName = "typeId")]
        public int TypeId { get; set; }
        /// <summary>
        /// Наименование типа сущности
        /// </summary>
        [TsProperty(Name = "typeName")]
        [JsonProperty(PropertyName = "typeName")]
        public string TypeName { get; set; }
        /// <summary>
        /// Алиас типа сущности
        /// </summary>
        [TsProperty(Name = "mvcAlias")]
        [JsonProperty(PropertyName = "mvcAlias")]
        public string MVCAlias { get; set; }
        /// <summary>
        /// ИД родительского типа сущности
        /// </summary>
        [TsProperty(Name = "parentTypeId")]
        [JsonProperty(PropertyName = "parentTypeId")]
        public int? ParentTypeId { get; set; }
        /// <summary>
        /// Наименование реализации типа сущности
        /// </summary>
        [TsProperty(Name = "implementTypeName")]
        [JsonProperty(PropertyName = "implementTypeName")]
        public string ImplementTypeName { get; set; }
        /// <summary>
        /// ИД состояния сущности
        /// </summary>
        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int StateId { get; set; }
        /// <summary>
        /// Наименование состояния сущности
        /// </summary>
        [TsProperty(Name = "stateName")]
        [JsonProperty(PropertyName = "stateName")]
        public string StateName { get; set; }
        /// <summary>
        /// Наименование сущности
        /// </summary>
        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        /// <summary>
        /// Полное наименование
        /// </summary>
        [TsProperty(Name = "fullName")]
        [JsonProperty(PropertyName = "fullName")]
        public string FullName { get; set; }
        /// <summary>
        /// Описание сущности
        /// </summary>
        [TsProperty(Name = "desc")]
        [JsonProperty(PropertyName = "desc")]
        public string Desc { get; set; }
        /// <summary>
        /// Код сущности
        /// </summary>
        [TsProperty(Name = "code")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }
    }
}
﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Entity
{
    [TsClass(Name = "EntityInfoCollectionModel", Module = "Entity")]
    public class EntityInfoCollectionModel
    {
        [TsProperty(Name = "items")]
        [JsonProperty(PropertyName = "items")]
        public IEnumerable<EntityInfoItemModel> Items { get; set; }
    }
}
﻿using System.Collections.Generic;
using HIS.Models.Layer.Models.Operations;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Entity
{
    [TsClass(Name = "EntityStateModel", Module = "Operations")]
    public class EntityStateModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BAId { get; set; }

        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BATypeId { get; set; }

        [TsProperty(Name = "typeName")]
        [JsonProperty(PropertyName = "typeName")]
        public string TypeName { get; set; }

        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int StateId { get; set; }

        [TsProperty(Name = "stateName")]
        [JsonProperty(PropertyName = "stateName")]
        public string StateName { get; set; }

        [TsProperty(Name = "isFirstState")]
        [JsonProperty(PropertyName = "isFirstState")]
        public bool IsFirstState { get; set; }

        [TsProperty(Name = "isDelState")]
        [JsonProperty(PropertyName = "isDelState")]
        public bool IsDelState { get; set; }

        [TsProperty(Name = "allowedOperations")]
        [JsonProperty(PropertyName = "allowedOperations")]
        public IEnumerable<EntityOperationModel> AllowedOperations { get; set; }
    }
}

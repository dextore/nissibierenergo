﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.AdminPanel
{
    [TsClass(Name = "BaTypesOperationsStateModel", Module = "AdminPanel")]
    public class BaTypesOperationsStateModel
    {
        [TsProperty(Name = "operationId")]
        [JsonProperty(PropertyName = "operationId")]
        public int OperationId { get; set; }
        [TsProperty(Name = "operationName")]
        [JsonProperty(PropertyName = "operationName")]
        public string OperationName { get; set; }
        [TsProperty(Name = "sourceStateName")]
        [JsonProperty(PropertyName = "sourceStateName")]
        public string SourceStateName { get; set; }
        [TsProperty(Name = "sourceStateId")]
        [JsonProperty(PropertyName = "sourceStateId")]
        public int SourceStateId { get; set; }
        [TsProperty(Name = "destinationStateName")]
        [JsonProperty(PropertyName = "destinationStateName")]
        public string DestinationStateName { get; set; }
        [TsProperty(Name = "isBlocked")]
        [JsonProperty(PropertyName = "isBlocked")]
        public bool IsBlocked { get; set; }
    }

    [TsClass(Name = "BaTypesOperationsStateCreateModel", Module = "AdminPanel")]
    public class BaTypesOperationsStateCreateModel
    {
        [TsProperty(Name = "operation")]
        [JsonProperty(PropertyName = "operation")]
        public BaTypesOperationsCreateModel Operation { get; set; }
        [TsProperty(Name = "startState")]
        [JsonProperty(PropertyName = "startState")]
        public BaTypesStatesCreateModel StartState { get; set; }
        [TsProperty(Name = "endState")]
        [JsonProperty(PropertyName = "endState")]
        public BaTypesStatesCreateModel EndState { get; set; }
        [TsProperty(Name = "isBlocked")]
        [JsonProperty(PropertyName = "isBlocked")]
        public bool IsBlocked { get; set; }
    }
}

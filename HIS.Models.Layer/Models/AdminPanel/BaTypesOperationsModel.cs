﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.AdminPanel
{
    [TsClass(Name = "BaTypesOperationsModel", Module = "AdminPanel")]
    public class BaTypesOperationsModel
    {
        [TsProperty(Name = "operationId")]
        [JsonProperty(PropertyName = "operationId")]
        public int OperationId { get; set; }
        [TsProperty(Name = "operationName")]
        [JsonProperty(PropertyName = "operationName")]
        public string OperationName { get; set; }
        [TsProperty(Name = "isCreateOperation")]
        [JsonProperty(PropertyName = "isCreateOperation")]
        public bool IsCreateOperation { get; set; }
        [TsProperty(Name = "isDeleteOperation")]
        [JsonProperty(PropertyName = "isDeleteOperation")]
        public bool IsDeleteOperation { get; set; }
    }

    [TsClass(Name = "BaTypesOperationsCreateModel", Module = "AdminPanel")]
    public class BaTypesOperationsCreateModel
    {
        [TsProperty(Name = "operationId")]
        [JsonProperty(PropertyName = "operationId")]
        public int? OperationId { get; set; }
        [TsProperty(Name = "operationName")]
        [JsonProperty(PropertyName = "operationName")]
        public string OperationName { get; set; }
    }
}

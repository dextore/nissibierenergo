﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.AdminPanel
{
    [TsClass(Name = "BaTypesStatesModel", Module = "AdminPanel")]
    public class BaTypesStatesModel
    {
        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int StateId { get; set; }
        [TsProperty(Name = "stateName")]
        [JsonProperty(PropertyName = "stateName")]
        public string StateName { get; set; }
        [TsProperty(Name = "isFirstState")]
        [JsonProperty(PropertyName = "isFirstState")]
        public bool IsFirstState { get; set; }
        [TsProperty(Name = "isDelState")]
        [JsonProperty(PropertyName = "isDelState")]
        public bool IsDelState { get; set; }
    }

    [TsClass(Name = "BaTypesStatesCreateModel", Module = "AdminPanel")]
    public class BaTypesStatesCreateModel
    {
        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int? StateId { get; set; }
        [TsProperty(Name = "stateName")]
        [JsonProperty(PropertyName = "stateName")]
        public string StateName { get; set; }
    }
}

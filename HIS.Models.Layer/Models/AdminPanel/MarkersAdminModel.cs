﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.AdminPanel
{
    [TsClass(Name = "MarkersAdminModel", Module = "AdminPanel")]
    public class MarkersAdminModel
    {
        [TsProperty(Name = "id")]
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [TsProperty(Name = "mvcAlias")]
        [JsonProperty(PropertyName = "mvcAlias")]
        public string MvcAlias { get; set; }
        [TsProperty(Name = "baTypeLink")]
        [JsonProperty(PropertyName = "baTypeLink")]
        public string BaTypeLink { get; set; }
        [TsProperty(Name = "baTypeLinkId")]
        [JsonProperty(PropertyName = "baTypeLinkId")]
        public int? BaTypeLinkId { get; set; }
        [TsProperty(Name = "markerType")]
        [JsonProperty(PropertyName = "markerType")]
        public string MarkerType { get; set; }
        [TsProperty(Name = "isPeriodic")]
        [JsonProperty(PropertyName = "isPeriodic")]
        public bool IsPeriodic { get; set; }
        [TsProperty(Name = "isCollectible")]
        [JsonProperty(PropertyName = "isCollectible")]
        public bool IsCollectible { get; set; }
        [TsProperty(Name = "isBlocked")]
        [JsonProperty(PropertyName = "isBlocked")]
        public bool IsBlocked { get; set; }
        [TsProperty(Name = "isFixMarkerHistory")]
        [JsonProperty(PropertyName = "isFixMarkerHistory")]
        public bool IsFixMarkerHistory { get; set; }
        [TsProperty(Name = "isImplementTypeField")]
        [JsonProperty(PropertyName = "isImplementTypeField")]
        public bool IsImplementTypeField { get; set; }
        [TsProperty(Name = "isRequired")]
        [JsonProperty(PropertyName = "isRequired")]
        public bool IsRequired { get; set; }
        [TsProperty(Name = "isOptional")]
        [JsonProperty(PropertyName = "isOptional")]
        public bool IsOptional { get; set; }
        [TsProperty(Name = "isInheritToDescendant")]
        [JsonProperty(PropertyName = "isInheritToDescendant")]
        public bool IsInheritToDescendant { get; set; }
        [TsProperty(Name = "implementTypeName")]
        [JsonProperty(PropertyName = "implementTypeName")]
        public string ImplementTypeName { get; set; }
        [TsProperty(Name = "implementTypeField")]
        [JsonProperty(PropertyName = "implementTypeField")]
        public string ImplementTypeField { get; set; }
        [TsProperty(Name = "overrideName")]
        [JsonProperty(PropertyName = "overrideName")]
        public string OverrideName { get; set; }
    }


    [TsClass(Name = "OnlyMarkersDataModel", Module = "AdminPanel")]
    public class OnlyMarkersDataModel
    {
        [TsProperty(Name = "id")]
        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }
        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [TsProperty(Name = "mvcAlias")]
        [JsonProperty(PropertyName = "mvcAlias")]
        public string MvcAlias { get; set; }
        [TsProperty(Name = "baTypeLink")]
        [JsonProperty(PropertyName = "baTypeLink")]
        public string BaTypeLink { get; set; }
        [TsProperty(Name = "baTypeLinkId")]
        [JsonProperty(PropertyName = "baTypeLinkId")]
        public int? BaTypeLinkId { get; set; }
        [TsProperty(Name = "markerType")]
        [JsonProperty(PropertyName = "markerType")]
        public string MarkerType { get; set; }
        [TsProperty(Name = "implementTypeName")]
        [JsonProperty(PropertyName = "implementTypeName")]
        public string ImplementTypeName { get; set; }
        [TsProperty(Name = "markerTypeId")]
        [JsonProperty(PropertyName = "markerTypeId")]
        public int MarkerTypeId { get; set; }
    }
}

﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.AdminPanel
{
    [TsClass(Name = "ItemTableViewModel", Module = "AdminPanel")]
    public class ItemTableViewModel
    {
        [TsProperty(Name = "itemId")]
        [JsonProperty(PropertyName = "itemId")]
        public int? ItemId { get; set; }
        [TsProperty(Name = "itemName")]
        [JsonProperty(PropertyName = "itemName")]
        public string ItemName { get; set; }
        [TsProperty(Name = "isActive")]
        [JsonProperty(PropertyName = "isActive")]
        public bool IsActive { get; set; }
        [TsProperty(Name = "orderNumber")]
        [JsonProperty(PropertyName = "orderNumber")]
        public int OrderNumber { get; set; }
    }
}

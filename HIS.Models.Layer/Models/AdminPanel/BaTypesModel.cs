﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.AdminPanel
{
    [TsClass(Name = "BaTypesModel", Module = "AdminPanel")]
    public class BaTypesModel
    {
        [TsProperty(Name = "typeId")]
        [JsonProperty(PropertyName = "typeId")]
        public long? TypeId { get; set; }
        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [TsProperty(Name = "mvcAlias")]
        [JsonProperty(PropertyName = "mvcAlias")] 
        public string MvcAlias { get; set; }
        [TsProperty(Name = "parent")]
        [JsonProperty(PropertyName = "parent")]
        public string Parent { get; set; }
        [TsProperty(Name = "parentId")]
        [JsonProperty(PropertyName = "parentId")]
        public long? ParentId { get; set; }
        [TsProperty(Name = "fixOperationHistory")]
        [JsonProperty(PropertyName = "fixOperationHistory")]
        public bool FixOperationHistory { get; set; }
        [TsProperty(Name = "isImplementTypeName")]
        [JsonProperty(PropertyName = "isImplementTypeName")]
        public string IsImplementTypeName { get; set; }
        [TsProperty(Name = "isImplementType")]
        [JsonProperty(PropertyName = "isImplementType")]
        public bool IsImplementType { get; set; }
        [TsProperty(Name = "searchType")]
        [JsonProperty(PropertyName = "searchType")]
        public string SearchType { get; set; }
        [TsProperty(Name = "searchTypeId")]
        [JsonProperty(PropertyName = "searchTypeId")]
        public long? SearchTypeId { get; set; }
        [TsProperty(Name = "isGroupType")]
        [JsonProperty(PropertyName = "isGroupType")]
        public bool IsGroupType { get; set; }
        [TsProperty(Name = "shortName")]
        [JsonProperty(PropertyName = "shortName")]
        public string ShortName { get; set; }
        [TsProperty(Name = "fullName")]
        [JsonProperty(PropertyName = "fullName")]
        public string FullName { get; set; }
    }
}

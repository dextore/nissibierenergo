﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressResultRequestModel", Module = "Address")]
    public class AddressResultRequestModel
    {
        [TsProperty(Name = "AOId")]
        [JsonProperty(PropertyName = "AOId")]
        public Guid? AOId { get; set; }

        [TsProperty(Name = "houseId")]
        [JsonProperty(PropertyName = "houseId")]
        public Guid? HouseId { get; set; }

        [TsProperty(Name = "steadId")]
        [JsonProperty(PropertyName = "steadId")]
        public Guid? SteadId { get; set; }

        [TsProperty(Name = "onlyActyal")]
        [JsonProperty(PropertyName = "onlyActyal")]
        public bool OnlyActyal { get; set; }

        [TsProperty(Name = "postCode")]
        [JsonProperty(PropertyName = "postCode")]
        public string PostCode { get; set; }

        [TsProperty(Name = "cadNum")]
        [JsonProperty(PropertyName = "cadNum")]
        public string CadNum { get; set; }
    }
}

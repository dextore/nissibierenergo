﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressUpdateModel", Module = "Address")]
    public class AddressUpdateModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long? BaId { get; set; } 

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "aoId")]
        [JsonProperty(PropertyName = "aoId")]
        public Guid AoId { get; set; }

        [TsProperty(Name = "houseId")]
        [JsonProperty(PropertyName = "houseId")]
        public Guid? HouseId { get; set; }

        [TsProperty(Name = "steadId")]
        [JsonProperty(PropertyName = "steadId")]
        public Guid? SteadId { get; set; }

        [TsProperty(Name = "location")]
        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }

        [TsProperty(Name = "flatNum")]
        [JsonProperty(PropertyName = "flatNum")]
        public string FlatNum { get; set; }

        [TsProperty(Name = "flatType")]
        [JsonProperty(PropertyName = "flatType")]
        public int? FlatType { get; set; }

        [TsProperty(Name = "POBox")]
        [JsonProperty(PropertyName = "POBox")]
        public string POBox { get; set; }

        [TsProperty(Name = "isBuild")]
        [JsonProperty(PropertyName = "isBuild")]
        public bool IsBuild { get; set; }

        [TsProperty(Name = "isActual")]
        [JsonProperty(PropertyName = "isActual")]
        public bool IsActual { get; set; }

        [TsProperty(Name = "searchType")]
        [JsonProperty(PropertyName = "searchType")]
        public AddressSourceType SearchType { get; set; }
    }
}

﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddresFullName", Module = "Address")]
    public class AddressFullName
    {
        [TsProperty(Name = "aoId")]
        [JsonProperty(PropertyName = "aoId")]
        public System.Guid AOId { get; set; }
        /*
        [TsProperty(Name = "AOGuid")]
        [JsonProperty(PropertyName = "AOGuid")]
        public System.Guid AOGuid { get; set; }
        */
        [TsProperty(Name = "houseId")]
        [JsonProperty(PropertyName = "houseId")]
        public Guid? HouseId { get; set; }

        [TsProperty(Name = "steadId")]
        [JsonProperty(PropertyName = "steadId")]
        public Guid? SteadId { get; set; }

        [TsProperty(Name = "addressText")]
        [JsonProperty(PropertyName = "addressText")]
        public string AddressText { get; set; }

        [TsProperty(Name = "sourceType")]
        [JsonProperty(PropertyName = "sourceType")]
        public AddressSourceType SourceType { get; set; }
    }
}

﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "HouseUpdateModel", Module = "Address")]
    public class HouseUpdateModel
    {
        [TsProperty(Name = "houseId")]
        [JsonProperty(PropertyName = "houseId")]
        public Guid? HouseId { get; set; }

        [TsProperty(Name = "houseGuid")]
        [JsonProperty(PropertyName = "houseGuid")]
        public Guid? HouseGuid { get; set; }

        [TsProperty(Name = "houseNum")]
        [JsonProperty(PropertyName = "houseNum")]
        public string HouseNum { get; set; }

        [TsProperty(Name = "buildNum")]
        [JsonProperty(PropertyName = "buildNum")]
        public string BuildNum { get; set; }

        [TsProperty(Name = "structNum")]
        [JsonProperty(PropertyName = "structNum")]
        public string StructNum { get; set; }

        [TsProperty(Name = "strStateId")]
        [JsonProperty(PropertyName = "strStateId")]
        public int? StrStateId { get; set; }

        [TsProperty(Name = "estStateId")]
        [JsonProperty(PropertyName = "estStateId")]
        public int? EstStateId { get; set; }

        [TsProperty(Name = "AOId")]
        [JsonProperty(PropertyName = "AOId")]
        public Guid? AOId { get; set; }

        [TsProperty(Name = "parentGuid")]
        [JsonProperty(PropertyName = "parentGuid")]
        public Guid? ParentGuid { get; set; }
      

        [TsProperty(Name = "sourceType")]
        [JsonProperty(PropertyName = "sourceType")]
        public AddressSourceType SourceType { get; set; }

        [TsProperty(Name = "postalCode")]
        [JsonProperty(PropertyName = "postalCode")]
        public string PostCode { get; set; }

        [TsProperty(Name = "cadNum")]
        [JsonProperty(PropertyName = "cadNum")]
        public string CadNum { get; set; }

        [TsProperty(Name = "latitude")]
        [JsonProperty(PropertyName = "latitude")]
        public float? Latitude { get; set; }

        [TsProperty(Name = "longitude")]
        [JsonProperty(PropertyName = "longitude")]
        public float? Longitude { get; set; }

        [TsProperty(Name = "isBuild")]
        [JsonProperty(PropertyName = "isBuild")]
        public bool IsBuild { get; set; }

    }
}

﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressSettleRequestModel", Module = "Address")]
    public class AddressSettleRequestModel
    {
        [TsProperty(Name = "aoId")]
        [JsonProperty(PropertyName = "aoId")]
        public Guid? AOId { get; set; }

        [TsProperty(Name = "aoGuid")]
        [JsonProperty(PropertyName = "aoGuid")]
        public Guid? AOGuid { get; set; }

        [TsProperty(Name = "fiasAOId")]
        [JsonProperty(PropertyName = "fiasAOId")]
        public Guid FiasAOId { get; set; }

        [TsProperty(Name = "houseId")]
        [JsonProperty(PropertyName = "houseId")]
        public Guid? HouseId { get; set; }

        [TsProperty(Name = "houseGuid")]
        [JsonProperty(PropertyName = "houseGuid")]
        public Guid? HouseGuid { get; set; }

        [TsProperty(Name = "fiasHouseId")]
        [JsonProperty(PropertyName = "fiasHouseId")]
        public Guid? FiasHouseId { get; set; }

        [TsProperty(Name = "steadId")]
        [JsonProperty(PropertyName = "steadId")]
        public Guid? SteadId { get; set; }

        [TsProperty(Name = "steadGuid")]
        [JsonProperty(PropertyName = "steadGuid")]
        public Guid? SteadGuid { get; set; }

        [TsProperty(Name = "fiasSteadId")]
        [JsonProperty(PropertyName = "fiasSteadId")]
        public Guid? FiasSteadId { get; set; }

        [TsProperty(Name = "aoLevelId")]
        [JsonProperty(PropertyName = "aoLevelId")]
        public int AOLevelId { get; set; }

    }
}

﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "HouseUpdateResultModel", Module = "Address")]
    public class HouseUpdateResultModel
    {
        [TsProperty(Name = "houseId")]
        [JsonProperty(PropertyName = "houseId")]
        public Guid HouseId { get; set; }

        [TsProperty(Name = "houseGuid")]
        [JsonProperty(PropertyName = "houseGuid")]
        public Guid HouseGuid { get; set; }

        [TsProperty(Name = "isError")]
        [JsonProperty(PropertyName = "isError")]
        public bool IsError { get; set; }


        [TsProperty(Name = "errorDescription")]
        [JsonProperty(PropertyName = "errorDescription")]
        public string ErrorDescription { get; set; }


    }
}

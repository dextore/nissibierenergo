﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressObjectTypeModel", Module = "Address")]
    public class AddressObjectTypeModel
    {
        [TsProperty(Name = "SCname")]
        [JsonProperty(PropertyName = "SCname")]
        public string SCname { get; set; }

        [TsProperty(Name = "socrName")]
        [JsonProperty(PropertyName = "socrName")]
        public string SocrName { get; set; }
    }
}

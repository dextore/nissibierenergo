﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "FlatTypeModel", Module = "Address")]
    public class FlatTypeModel
    {
        [TsProperty(Name = "flatTypeId")]
        [JsonProperty(PropertyName = "flatTypeId")]
        public int FlatTypeId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "shortName")]
        [JsonProperty(PropertyName = "shortName")]
        public string ShortName { get; set; }

        [TsProperty(Name = "isVisible")]
        [JsonProperty(PropertyName = "isVisible")]
        public bool IsVisible { get; set; }
    }
}

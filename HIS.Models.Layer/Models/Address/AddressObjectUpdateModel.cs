﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressObjectUpdateModel", Module = "Address")]
    public class AddressObjectUpdateModel
    {
        [TsProperty(Name = "AOId")]
        [JsonProperty(PropertyName = "AOId")]
        public Guid? AOId { get; set; }

        [TsProperty(Name = "AOGuid")]
        [JsonProperty(PropertyName = "AOGuid")]
        public Guid? AOGuid { get; set; }

        [TsProperty(Name = "formalName")]
        [JsonProperty(PropertyName = "formalName")]
        public string FormalName { get; set; }

        [TsProperty(Name = "AOLevel")]
        [JsonProperty(PropertyName = "AOLevel")]
        public int AOLevel { get; set; }

        [TsProperty(Name = "shortName")]
        [JsonProperty(PropertyName = "shortName")]
        public string ShortName { get; set; }

        [TsProperty(Name = "postalCode")]
        [JsonProperty(PropertyName = "postalCode")]
        public string PostCode { get; set; }

        [TsProperty(Name = "parentId")]
        [JsonProperty(PropertyName = "parentId")]
        public Guid? ParentId { get; set; }
    }
}

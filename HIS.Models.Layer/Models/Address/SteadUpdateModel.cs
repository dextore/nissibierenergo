﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "SteadUpdateModel", Module = "Address")]
    public class SteadUpdateModel
    {
        [TsProperty(Name = "steadId")]
        [JsonProperty(PropertyName = "steadId")]
        public Guid? SteadId { get; set; }

        [TsProperty(Name = "steadGuid")]
        [JsonProperty(PropertyName = "steadGuid")]
        public Guid? SteadGuid { get; set; }

        [TsProperty(Name = "number")]
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        [TsProperty(Name = "parentId")]
        [JsonProperty(PropertyName = "parentId")]
        public Guid? ParentId { get; set; }

        [TsProperty(Name = "parentGuid")]
        [JsonProperty(PropertyName = "parentGuid")]
        public Guid? ParentGuid { get; set; }

        [TsProperty(Name = "postalCode")]
        [JsonProperty(PropertyName = "postalCode")]
        public string PostCode { get; set; }

        [TsProperty(Name = "cadNum")]
        [JsonProperty(PropertyName = "cadNum")]
        public string CadNum { get; set; }


        [TsProperty(Name = "latitude")]
        [JsonProperty(PropertyName = "latitude")]
        public float? Latitude { get; set; }

        [TsProperty(Name = "longitude")]
        [JsonProperty(PropertyName = "longitude")]
        public float? Longitude { get; set; }
    }
}



﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.FIAS
{
    [TsClass(Name = "AddressRequestModel", Module = "Address")]
    public class AddressRequestModel: RequestModel
    {
        [TsProperty(Name = "AOGuid")]
        [JsonProperty(PropertyName = "AOGuid")]
        public Guid? AOGuid { get; set; }

        [TsProperty(Name = "liveStatus")]
        [JsonProperty(PropertyName = "liveStatus")]
        public int? liveStatus { get; set; }

    }
}

﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressEntityModel", Module = "Address")]
    public class AddressEntityModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BaId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "AOId")]
        [JsonProperty(PropertyName = "AOId")]
        public Guid? AOId { get; set; }

        [TsProperty(Name = "AOGuid")]
        [JsonProperty(PropertyName = "AOGuid")]
        public Guid? AOGuid { get; set; }

        [TsProperty(Name = "houseGuid")]
        [JsonProperty(PropertyName = "houseGuid")]
        public Guid? HouseGuid { get; set; }

        [TsProperty(Name = "houseId")]
        [JsonProperty(PropertyName = "houseId")]
        public Guid? HouseId { get; set; }

        [TsProperty(Name = "steadGuid")]
        [JsonProperty(PropertyName = "steadGuid")]
        public Guid? SteadGuid { get; set; }

        [TsProperty(Name = "steadId")]
        [JsonProperty(PropertyName = "steadId")]
        public Guid? SteadId { get; set; }

        [TsProperty(Name = "location")]
        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }

        [TsProperty(Name = "flatNum")]
        [JsonProperty(PropertyName = "flatNum")]
        public string FlatNum { get; set; }

        [TsProperty(Name = "flatTypeId")]
        [JsonProperty(PropertyName = "flatTypeId")]
        public int? FlatTypeId { get; set; }

        [TsProperty(Name = "POBox")]
        [JsonProperty(PropertyName = "POBox")]
        public string POBox { get; set; }

        [TsProperty(Name = "isBuild")]
        [JsonProperty(PropertyName = "isBuild")]
        public bool IsBuild { get; set; }

        [TsProperty(Name = "isActual")]
        [JsonProperty(PropertyName = "isActual")]
        public bool IsActual { get; set; }
    }
}

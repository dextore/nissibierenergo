﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AOAddressesModel", Module = "Address")]
    public class AOAddressesModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BAId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "aoGuid")]
        [JsonProperty(PropertyName = "aoGuid")]
        public Guid AOGuid { get; set; }

        [TsProperty(Name = "houseGuid")]
        [JsonProperty(PropertyName = "houseGuid")]
        public Guid HouseGuid { get; set; }

        [TsProperty(Name = "steadGuid")]
        [JsonProperty(PropertyName = "steadGuid")]
        public Guid SteadGuid { get; set; }

        [TsProperty(Name = "location")]
        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }

        [TsProperty(Name = "flatNum")]
        [JsonProperty(PropertyName = "flatNum")]
        public string FlatNum { get; set; }

        [TsProperty(Name = "flatTypeId")]
        [JsonProperty(PropertyName = "flatTypeId")]
        public int? FlatTypeId { get; set; }
        
        [TsProperty(Name = "isBuild")]
        [JsonProperty(PropertyName = "isBuild")]
        public bool? IsBuild { get; set; }

        [TsProperty(Name = "isActual")]
        [JsonProperty(PropertyName = "isActual")]
        public bool? IsActual { get; set; }
        
        [TsProperty(Name = "poBox")]
        [JsonProperty(PropertyName = "poBox")]
        public string POBox { get; set; }
        
    }
}

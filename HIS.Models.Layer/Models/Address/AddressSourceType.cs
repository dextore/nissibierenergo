﻿namespace HIS.Models.Layer.Models.Address
{
    public enum AddressSourceType
    {
        Indefined = 0, // Не определено
        Fias = 1, // ФИАС
        Custom = 2 // Ручной ввод
    }
}

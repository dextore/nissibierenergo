﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressObjectUpdateResultModel", Module = "Address")]
    public class AddressObjectUpdateResultModel 
    {
        [TsProperty(Name = "AOId")]
        [JsonProperty(PropertyName = "AOId")]
        public Guid AOId { get; set; }

        [TsProperty(Name = "AOGuid")]
        [JsonProperty(PropertyName = "AOGuid")]
        public Guid AOGuid { get; set; }


        [TsProperty(Name = "isError")]
        [JsonProperty(PropertyName = "isError")]
        public bool IsError { get; set; }


        [TsProperty(Name = "errorDescription")]
        [JsonProperty(PropertyName = "errorDescription")]
        public string ErrorDescription { get; set; }

    }
}

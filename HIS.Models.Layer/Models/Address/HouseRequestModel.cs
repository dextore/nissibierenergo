﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "HouseRequestModel", Module = "Address")]
    public class HouseRequestModel
    {
        [TsProperty(Name = "AOGuid")]
        [JsonProperty(PropertyName = "AOGuid")]
        public Guid? AOGuid { get; set; }

        [TsProperty(Name = "houseId")]
        [JsonProperty(PropertyName = "houseId")]
        public Guid? HouseId { get; set; }
    }
}

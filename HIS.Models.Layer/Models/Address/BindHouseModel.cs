﻿using System;
using Newtonsoft.Json;
using TypeLite;


namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "BindHousesModel", Module = "Address")]
    public class BindHouseModel
    {
        [TsProperty(Name = "firstHouseId")]
        [JsonProperty(PropertyName = "firstHouseId")]
        public Guid FirstHouseId { get; set; }

        [TsProperty(Name = "secondHouseId")]
        [JsonProperty(PropertyName = "secondHouseId")]
        public Guid SecondHouseId { get; set; }

        [TsProperty(Name = "isBind")]
        [JsonProperty(PropertyName = "isBind")]
        public bool IsBind { get; set; }
    }
}

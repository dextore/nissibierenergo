﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressFindResult", Module = "Address")]
    public class AddressFindResult
    {
        [TsProperty(Name = "AOId")]
        [JsonProperty(PropertyName = "AOId")]
        public Guid? AOId { get; set; }

        [TsProperty(Name = "houseId")]
        [JsonProperty(PropertyName = "houseId")]
        public Guid? HouseId { get; set; }

        [TsProperty(Name = "steadId")]
        [JsonProperty(PropertyName = "steadId")]
        public Guid? SteadId { get; set; }

        [TsProperty(Name = "addressText")]
        [JsonProperty(PropertyName = "addressText")]
        public string AddressText { get; set; }

        [TsProperty(Name = "postalCode")]
        [JsonProperty(PropertyName = "postalCode")]
        public string PostalCode { get; set; }

        [TsProperty(Name = "houseNum")]
        [JsonProperty(PropertyName = "houseNum")]
        public string HouseNum { get; set; }

        [TsProperty(Name = "buildNum")]
        [JsonProperty(PropertyName = "buildNum")]
        public string BuildNum { get; set; }

        [TsProperty(Name = "strucNum")]
        [JsonProperty(PropertyName = "strucNum")]
        public string StrucNum { get; set; }

        [TsProperty(Name = "buildStateName")]
        [JsonProperty(PropertyName = "buildStateName")]
        public string BuildStateName { get; set; }

        [TsProperty(Name = "actualStateName")]
        [JsonProperty(PropertyName = "actualStateName")]
        public string ActualStateName { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public System.DateTime? StartDate { get; set; }

        [TsProperty(Name = "cadNum")]
        [JsonProperty(PropertyName = "cadNum")]
        public string CadNum { get; set; }

        [TsProperty(Name = "AOGuid")]
        [JsonProperty(PropertyName = "AOGuid")]
        public System.Guid? AOGuid { get; set; }

        [TsProperty(Name = "sourceType")]
        [JsonProperty(PropertyName = "sourceType")]
        public AddressSourceType? SourceType { get; set; }
    }
}

﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "SteadRequestModel", Module = "Address")]
    public class SteadRequestModel
    {
        [TsProperty(Name = "AOGuid")]
        [JsonProperty(PropertyName = "AOGuid")]
        public Guid? AOGuid { get; set; }

        [TsProperty(Name = "steadId")]
        [JsonProperty(PropertyName = "steadId")]
        public Guid? SteadId { get; set; }
    }
}

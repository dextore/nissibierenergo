﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressObjectModel", Module = "Address")]
    public class AddressObjectModel
    {
        [TsProperty(Name = "AOId")]
        [JsonProperty(PropertyName = "AOId")]
        public Guid AOId { get; set; }

        [TsProperty(Name = "AOGuid")]
        [JsonProperty(PropertyName = "AOGuid")]
        public Guid AOGuid { get; set; }

        [TsProperty(Name = "formalName")]
        [JsonProperty(PropertyName = "formalName")]
        public string FormalName { get; set; }

        [TsProperty(Name = "shortName")]
        [JsonProperty(PropertyName = "shortName")]
        public string ShortName { get; set; }

        [TsProperty(Name = "sourceType")]
        [JsonProperty(PropertyName = "sourceType")]
        public AddressSourceType SourceType { get; set; }

        [TsProperty(Name = "postCode")]
        [JsonProperty(PropertyName = "postCode")]
        public string PostCode { get; set; }
    }
}

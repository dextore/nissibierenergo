﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressStructureStatus", Module = "Address")]
    public class AddressStructureStatus
    {
        [TsProperty(Name = "strStatId")]
        [JsonProperty(PropertyName = "strStatId")]
        public int StrStatId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "shortName")]
        [JsonProperty(PropertyName = "shortName")]
        public string ShortName { get; set; }

        [TsProperty(Name = "dataVersionId")]
        [JsonProperty(PropertyName = "dataVersionId")]
        public int DataVersionId { get; set; }

        [TsProperty(Name = "isDelete")]
        [JsonProperty(PropertyName = "isDelete")]
        public bool IsDelete { get; set; }
    }
}

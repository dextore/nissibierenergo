﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressComparisonsDpModel", Module = "Address")]
    public class AddressComparisonsDpModel : AddressComparisonsModel
    {
        [TsProperty(Name = "items")]
        [JsonProperty(PropertyName = "items")]
        public List<AddressComparisonsDpModel> Items { get; set; }

    }
}

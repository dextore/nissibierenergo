﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressComparisons", Module = "Address")]
    public class AddressComparisonsModel
    {
        [TsProperty(Name = "AOid")]
        [JsonProperty(PropertyName = "AOid")]
        public Guid? AOid { get; set; }

        [TsProperty(Name = "houseId")]
        [JsonProperty(PropertyName = "houseId")]
        public Guid? HouseId { get; set; }

        [TsProperty(Name = "steadId")]
        [JsonProperty(PropertyName = "steadId")]
        public Guid? SteadId { get; set; }

        [TsProperty(Name = "AOGuid")]
        [JsonProperty(PropertyName = "AOGuid")]
        public Guid? AOGuid { get; set; }

        [TsProperty(Name = "parentGuid")]
        [JsonProperty(PropertyName = "parentGuid")]
        public Guid? ParentGuid { get; set; }

        [TsProperty(Name = "AOLevel")]
        [JsonProperty(PropertyName = "AOLevel")]
        public int? AOLevel { get; set; }

        [TsProperty(Name = "fullName")]
        [JsonProperty(PropertyName = "fullName")]
        public string FullName { get; set; }

        [TsProperty(Name = "fiasAOId")]
        [JsonProperty(PropertyName = "fiasAOId")]
        public Guid? FiasAOId { get; set; }

        [TsProperty(Name = "fiasHouseId")]
        [JsonProperty(PropertyName = "fiasHouseId")]
        public Guid? FiasHouseId { get; set; }

        [TsProperty(Name = "fiasSteadId")]
        [JsonProperty(PropertyName = "fiasSteadId")]
        public Guid? FiasSteadId { get; set; }

        [TsProperty(Name = "fiasAOGuid")]
        [JsonProperty(PropertyName = "fiasAOGuid")]
        public Guid? FiasAOGuid { get; set; }

        [TsProperty(Name = "fiasParentGuid")]
        [JsonProperty(PropertyName = "fiasParentGuid")]
        public Guid? FiasParentGuid { get; set; }

        [TsProperty(Name = "fiasFullName")]
        [JsonProperty(PropertyName = "fiasFullName")]
        public string FiasFullName { get; set; }

        [TsProperty(Name = "sourceTypeId")]
        [JsonProperty(PropertyName = "sourceTypeId")]
        public string SourceTypeId { get; set; }
    }
}

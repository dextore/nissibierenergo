﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressCatalogBase", Module = "Address")]
    public abstract class AddressCatalogBase
    {
        [TsProperty(Name = "AOId")]
        [JsonProperty(PropertyName = "AOId")]
        public Guid AOId { get; set; }

        [TsProperty(Name = "AOGuid")]
        [JsonProperty(PropertyName = "AOGuid")]
        public Guid? AOGuid { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string NAME { get; set; }

        [TsProperty(Name = "sourceType")]
        [JsonProperty(PropertyName = "sourceType")]
        public AddressSourceType SourceType { get; set; }
    }
}

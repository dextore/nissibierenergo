﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressHouse", Module = "Address")]
    public class AddressHouse
    {
        [TsProperty(Name = "houseId")]
        [JsonProperty(PropertyName = "houseId")]
        public Guid HouseId { get; set; }

        [TsProperty(Name = "houseGuid")]
        [JsonProperty(PropertyName = "houseGuid")]
        public Guid HouseGuid { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "houseNum")]
        [JsonProperty(PropertyName = "houseNum")]
        public string HouseNum { get; set; }

        [TsProperty(Name = "postalCode")]
        [JsonProperty(PropertyName = "postalCode")]
        public string PostalCode { get; set; }

        [TsProperty(Name = "cadNum")]
        [JsonProperty(PropertyName = "cadNum")]
        public string CadNum { get; set; }

        [TsProperty(Name = "liveStatus")]
        [JsonProperty(PropertyName = "liveStatus")]
        public int LiveStatus { get; set; }

        [TsProperty(Name = "sourceType")]
        [JsonProperty(PropertyName = "sourceType")]
        public AddressSourceType SourceType { get; set; }
    }
}

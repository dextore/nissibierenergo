﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressEstateStatus", Module = "Address")]
    public class AddressEstateStatus
    {
        [TsProperty(Name = "estStatId")]
        [JsonProperty(PropertyName = "estStatId")]
        public int EstStatId { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "dataVersionId")]
        [JsonProperty(PropertyName = "dataVersionId")]
        public int DataVersionId { get; set; }

        [TsProperty(Name = "isDelete")]
        [JsonProperty(PropertyName = "isDelete")]
        public bool IsDelete { get; set; }
    }
}

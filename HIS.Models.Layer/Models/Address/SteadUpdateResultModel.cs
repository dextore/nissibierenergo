﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "SteadUpdateResultModel", Module = "Address")]
    public class SteadUpdateResultModel
    {
        [TsProperty(Name = "steadId")]
        [JsonProperty(PropertyName = "steadId")]
        public Guid? SteadId { get; set; }

        [TsProperty(Name = "steadGuid")]
        [JsonProperty(PropertyName = "steadGuid")]
        public Guid? SteadGuid { get; set; }

        [TsProperty(Name = "isError")]
        [JsonProperty(PropertyName = "isError")]
        public bool IsError { get; set; }


        [TsProperty(Name = "errorDescription")]
        [JsonProperty(PropertyName = "errorDescription")]
        public string ErrorDescription { get; set; }
    }
}

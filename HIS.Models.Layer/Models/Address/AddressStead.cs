﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressStead", Module = "Address")]
    public class AddressStead
    {
        [TsProperty(Name = "steadId")]
        [JsonProperty(PropertyName = "steadId")]
        public System.Guid SteadId { get; set; }

        [TsProperty(Name = "steadGuid")]
        [JsonProperty(PropertyName = "steadGuid")]
        public System.Guid SteadGuid { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "postalCode")]
        [JsonProperty(PropertyName = "postalCode")]
        public string PostalCode { get; set; }

        [TsProperty(Name = "cadNum")]
        [JsonProperty(PropertyName = "cadNum")]
        public string CadNum { get; set; }

        [TsProperty(Name = "liveStatus")]
        [JsonProperty(PropertyName = "liveStatus")]
        public int LiveStatus { get; set; }

        [TsProperty(Name = "sourceType")]
        [JsonProperty(PropertyName = "sourceType")]
        public AddressSourceType SourceType { get; set; }
    }
}

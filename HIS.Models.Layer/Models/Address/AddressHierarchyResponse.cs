﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressHierarchyResponse", Module = "Address")]
    public class AddressHierarchyResponse
    {
        [TsProperty(Name = "aOId")]
        [JsonProperty(PropertyName = "aOId")]
        public Guid AOId { get; set; }

        [TsProperty(Name = "houseId")]
        [JsonProperty(PropertyName = "houseId")]
        public Guid? HouseId { get; set; }
        
        [TsProperty(Name = "steadId")]
        [JsonProperty(PropertyName = "steadId")]
        public Guid? SteadId { get; set; }

        [TsProperty(Name = "aOGuid")]
        [JsonProperty(PropertyName = "aOGuid")]
        public Guid? AOGuid { get; set; }

        [TsProperty(Name = "parentGuid")]
        [JsonProperty(PropertyName = "parentGuid")]
        public Guid? ParentGuid { get; set; }

        [TsProperty(Name = "aOLevel")]
        [JsonProperty(PropertyName = "aOLevel")]
        public AddressObjectLevel AOLevel { get; set; }

        [TsProperty(Name = "sourceType")]
        [JsonProperty(PropertyName = "sourceType")]
        public AddressSourceType SourceType { get; set; }
    }
}

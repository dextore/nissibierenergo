﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Address
{
    [TsClass(Name = "AddressObjectRequestModel", Module = "Address")]
    public class AddressObjectRequestModel
    {
        [TsProperty(Name = "AOGuid")]
        [JsonProperty(PropertyName = "AOGuid")]
        public Guid? AOGuid { get; set; }
    }
}

﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.InventoryTransactions
{
    [TsClass(Name = "InventoryTransactionModel", Module = "Warehouses")]
    public class InventoryTransactionModel
    {
        [TsProperty(Name = "recId")]
        [JsonProperty(PropertyName = "recId")]
        public long RecId { get; set; }

        [TsProperty(Name = "caId")]
        [JsonProperty(PropertyName = "caId")]
        public long CAId { get; set; }

        [TsProperty(Name = "companyName")]
        [JsonProperty(PropertyName = "companyName")]
        public string CompanyName { get; set; }

        [TsProperty(Name = "docId")]
        [JsonProperty(PropertyName = "docId")]
        public long DocId { get; set; }

        [TsProperty(Name = "inventoryId")]
        [JsonProperty(PropertyName = "inventoryId")]
        public long InventoryId { get; set; }

        [TsProperty(Name = "inventoryName")]
        [JsonProperty(PropertyName = "inventoryName")]
        public string InventoryName { get; set; }

        [TsProperty(Name = "accId")]
        [JsonProperty(PropertyName = "accId")]
        public long AccId { get; set; }

        [TsProperty(Name = "accountName")]
        [JsonProperty(PropertyName = "accountName")]
        public string AccountName { get; set; }

        [TsProperty(Name = "accountNum")]
        [JsonProperty(PropertyName = "accountNum")]
        public string AccountNum { get; set; }

        [TsProperty(Name = "warehouseId")]
        [JsonProperty(PropertyName = "warehouseId")]
        public long WarehouseId { get; set; }

        [TsProperty(Name = "warehouseName")]
        [JsonProperty(PropertyName = "warehouseName")]
        public string WarehouseName { get; set; }

        [TsProperty(Name = "recTypeId")]
        [JsonProperty(PropertyName = "recTypeId")]
        public int RecTypeId { get; set; }

        [TsProperty(Name = "recTypeName")]
        [JsonProperty(PropertyName = "recTypeName")]
        public string RecTypeName { get; set; }

        [TsProperty(Name = "isIncome")]
        [JsonProperty(PropertyName = "isIncome")]
        public bool IsIncome { get; set; }

        [TsProperty(Name = "quantity")]
        [JsonProperty(PropertyName = "quantity")]
        public double Quantity { get; set; }

        [TsProperty(Name = "VAT")]
        [JsonProperty(PropertyName = "VAT")]
        public decimal VAT { get; set; }

        [TsProperty(Name = "priceVAT")]
        [JsonProperty(PropertyName = "priceVAT")]
        public double PriceVAT { get; set; }

        [TsProperty(Name = "isVATIncluded")]
        [JsonProperty(PropertyName = "isVATIncluded")]
        public bool IsVATIncluded { get; set; }

        [TsProperty(Name = "storeCost")]
        [JsonProperty(PropertyName = "storeCost")]
        public decimal StoreCost { get; set; }

        [TsProperty(Name = "sellCost")]
        [JsonProperty(PropertyName = "sellCost")]
        public decimal SellCost { get; set; }

        [TsProperty(Name = "sellCostVAT")]
        [JsonProperty(PropertyName = "sellCostVAT")]
        public decimal SellCostVAT { get; set; }

        [TsProperty(Name = "operationDate")]
        [JsonProperty(PropertyName = "operationDate")]
        public DateTime OperationDate { get; set; }
    }
}

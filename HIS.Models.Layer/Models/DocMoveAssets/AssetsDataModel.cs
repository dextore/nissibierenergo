﻿using Newtonsoft.Json;
using TypeLite;
using System;

namespace HIS.Models.Layer.Models.DocMoveAssets
{
    [TsClass(Name = "AssetsDataModel", Module = "DocMoveAssets")]
    public class AssetsDataModel
    {

        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BAId { get; set; }

        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BATypeId { get; set; }

        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int StateId { get; set; }

        [TsProperty(Name = "inventoryNumber")]
        [JsonProperty(PropertyName = "inventoryNumber")]
        public string InventoryNumber { get; set; }

        [TsProperty(Name = "serialNumber")]
        [JsonProperty(PropertyName = "serialNumber")]
        public string SerialNumber { get; set; }

        [TsProperty(Name = "releaseDate")]
        [JsonProperty(PropertyName = "releaseDate")]
        public DateTime? ReleaseDate { get; set; }

        [TsProperty(Name = "checkDate")]
        [JsonProperty(PropertyName = "checkDate")]
        public DateTime? CheckDate { get; set; }
        
        [TsProperty(Name = "docAcceptId")]
        [JsonProperty(PropertyName = "docAcceptId")]
        public long? FADocAcceptId { get; set; }

        [TsProperty(Name = "inventoryId")]
        [JsonProperty(PropertyName = "inventoryId")]
        public long? InventoryId { get; set; }

        [TsProperty(Name = "mlpId")]
        [JsonProperty(PropertyName = "mlpId")]
        public long? MLPId { get; set; }

        [TsProperty(Name = "lsId")]
        [JsonProperty(PropertyName = "lsId")]
        public long? LSId { get; set; }

        [TsProperty(Name = "barCode")]
        [JsonProperty(PropertyName = "barCode")]
        public string BarCode { get; set; }

        [TsProperty(Name = "employeeId")]
        [JsonProperty(PropertyName = "employeeId")]
        public long? EmployeeId { get; set; }





    }
}


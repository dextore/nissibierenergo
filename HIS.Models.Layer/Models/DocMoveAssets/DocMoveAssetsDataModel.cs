﻿using Newtonsoft.Json;
using TypeLite;
using HIS.Models.Layer.Models.Markers;
using System;
using System.Collections.Generic;

namespace HIS.Models.Layer.Models.DocMoveAssets

{
    [TsClass(Name = "DocMoveAssetsData", Module = "DocMoveAssets")]
    public class DocMoveAssetsDataModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long? BAId { get; set; }

        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int? BaTypeId { get; set; }

        [TsProperty(Name = "stateId")]
        [JsonProperty(PropertyName = "stateId")]
        public int? StateId { get; set; }

        [TsProperty(Name = "number")]
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        [TsProperty(Name = "docDate")]
        [JsonProperty(PropertyName = "docDate")]
        public DateTime? DocDate { get; set; }
        
        [TsProperty(Name = "srcLSId")]
        [JsonProperty(PropertyName = "srcLSId")]
        public long? SrcLSId { get; set; }

        [TsProperty(Name = "srcLSName")]
        [JsonProperty(PropertyName = "srcLSName")]
        public string SrcLSName { get; set; }

        [TsProperty(Name = "dstLSId")]
        [JsonProperty(PropertyName = "dstLSId")]
        public long? DstLSId { get; set; }

        [TsProperty(Name = "dstLSName")]
        [JsonProperty(PropertyName = "dstLSName")]
        public string DstLSName { get; set; }

        [TsProperty(Name = "srcMLPId")]
        [JsonProperty(PropertyName = "srcMLPId")]
        public long? SrcMLPId { get; set; }

        [TsProperty(Name = "srcMLPName")]
        [JsonProperty(PropertyName = "srcMLPName")]
        public string SrcMLPName { get; set; }

        [TsProperty(Name = "dstMLPId")]
        [JsonProperty(PropertyName = "dstMLPId")]
        public long? DstMLPId { get; set; }

        [TsProperty(Name = "dstMLPName")]
        [JsonProperty(PropertyName = "dstMLPName")]
        public string DstMLPName { get; set; }

        [TsProperty(Name = "srcLocationId")]
        [JsonProperty(PropertyName = "srcLocationId")]
        public int? SrcLocationId { get; set; }

        [TsProperty(Name = "dstLocationId")]
        [JsonProperty(PropertyName = "dstLocationId")]
        public int? DstLocationId { get; set; }

        [TsProperty(Name = "caId")]
        [JsonProperty(PropertyName = "caId")]
        public long? CAId { get; set; }

        [TsProperty(Name = "caName")]
        [JsonProperty(PropertyName = "caName")]
        public string CAName { get; set; }
        
        [TsProperty(Name = "reasonId")]
        [JsonProperty(PropertyName = "reasonId")]
        public long? ReasonId { get; set; }

        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [TsProperty(Name = "evidence")]
        [JsonProperty(PropertyName = "evidence")]
        public string Evidence { get; set; }

        [TsProperty(Name = "hasSpecification")]
        [JsonProperty(PropertyName = "hasSpecification")]
        public bool HasAssets { get; set; }

        [TsProperty(Name = "markerValues")]
        [JsonProperty(PropertyName = "markerValues")]
        public IEnumerable<MarkerValueModel> MarkerValues { get; set; }
    }
}

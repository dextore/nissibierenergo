﻿using System;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.DocMoveAssets {

    [TsClass(Name = "DocMoveAssetsRow", Module = "DocMoveAssets")]
    public class DocMoveAssetsRowModel
    {
        [TsProperty(Name = "recId")]
        [JsonProperty(PropertyName = "recId")]
        public long? RecId { get; set; }
        
        [TsProperty(Name = "docMoveId")]
        [JsonProperty(PropertyName = "docMoveId")]
        public long FADocMovId { get; set; }
        
        [TsProperty(Name = "faId")]
        [JsonProperty(PropertyName = "faId")]
        public long FAId { get; set; }

        [TsProperty(Name = "faName")]
        [JsonProperty(PropertyName = "faName")]
        public string FAName { get; set; }

        [TsProperty(Name = "serialNumber")]
        [JsonProperty(PropertyName = "serialNumber")]
        public string SerialNumber { get; set; }
        
        [TsProperty(Name = "releaseDate")]
        [JsonProperty(PropertyName = "releaseDate")]
        public DateTime? ReleaseDate { get; set; }

        [TsProperty(Name = "checkDate")]
        [JsonProperty(PropertyName = "checkDate")]
        public DateTime? CheckDate { get; set; }
  
        [TsProperty(Name = "barCode")]
        [JsonProperty(PropertyName = "barCode")]
        public string BarCode { get; set; }

        [TsProperty(Name = "evidenceId")]
        [JsonProperty(PropertyName = "evidenceId")]
        public long? EvidenceId { get; set; }

        [TsProperty(Name = "evidenceName")]
        [JsonProperty(PropertyName = "evidenceName")]
        public string EvidenceName { get; set; }

        [TsProperty(Name = "evidence")]
        [JsonProperty(PropertyName = "evidence")]
        public string Evidence { get; set; }



    }


}


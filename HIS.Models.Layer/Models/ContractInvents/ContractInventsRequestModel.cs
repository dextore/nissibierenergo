﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.ContractInvents
{
    [TsClass(Name = "ContractInventsRequestModel", Module = "ContractInvents")]
    public class ContractInventsRequestModel
    {
        [TsProperty(Name = "contractId")]
        [JsonProperty(PropertyName = "contractId")]
        public long ContractId { get; set; }
        [TsProperty(Name = "states")]
        [JsonProperty(PropertyName = "states")]
        public IEnumerable<int> States { get; set; }
    }
}

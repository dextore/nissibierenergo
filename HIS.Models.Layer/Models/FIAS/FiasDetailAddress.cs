﻿using System;

namespace HIS.Models.Layer.Models.FIAS
{
    public class FiasDetailAddress
    {
        public Guid AOID { get; set; }
        public Guid? AOGUID { get; set; }
        public string ADDRESSTEXT { get; set; }
        public string FORMALNAME { get; set; }
        public string OFFNAME { get; set; }
        public bool LIVESTATUS { get; set; }
        public string POSTALCODE { get; set; }
        public string OKATO { get; set; }
        public string OKTMO { get; set; }
        public DateTime? UPDATEDATE { get; set; }
        public string UPDATEDATETEXT { get; set; }
        public string LIVESTATUSTEXT { get; set; }
        public string PLAINCODE { get; set; }
        public string CODE { get; set; }
    }
}

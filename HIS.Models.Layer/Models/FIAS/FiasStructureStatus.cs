﻿namespace HIS.Models.Layer.Models.FIAS
{
    public class FiasStructureStatus
    {
        public int STRSTATID { get; set; }
        public string NAME { get; set; }
        public string SHORTNAME { get; set; }
        public int DATAVERSIONID { get; set; }
        public bool ISDELETE { get; set; }
    }
}

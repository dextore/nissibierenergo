﻿using System;

namespace HIS.Models.Layer.Models.FIAS
{
    public class FiasDetailSearch
    {
        public byte? TypeSearch { get; set; }
        public Guid? RFSubject { get; set; }
        public Guid? District { get; set; }
        public Guid? Region { get; set; }
        public Guid? City { get; set; }
        public Guid? InRegion { get; set; }
        public Guid? Settlement { get; set; }
        public Guid? ElmStructure { get; set; }
        public Guid? Street { get; set; }
        public Guid? Territory { get; set; }
        public Guid? AddTerritoryStreet { get; set; }
        public Guid? House { get; set; }
        public string PlainCode { get; set; }
    }
}

﻿using System;

namespace HIS.Models.Layer.Models.FIAS
{
    // Информация об адресе
    public class FiasAddressInfo
    {
        public System.Guid AOID { get; set; }
        public Guid? AOGUID { get; set; }
        public string FORMALNAME { get; set; }
        public string REGIONCODE { get; set; }
        public string REGIONNAME { get; set; }

        public string AUTOCODE { get; set; }
        public string AREACODE { get; set; }
        public string CITYCODE { get; set; }
        public string CTARCODE { get; set; }
        public string PLACECODE { get; set; }
        public string STREETCODE { get; set; }
        public string EXTRCODE { get; set; }
        public string SEXTCODE { get; set; }
        public string OFFNAME { get; set; }
        public string POSTALCODE { get; set; }
        public string IFNSFL { get; set; }
        public string TERRIFNSFL { get; set; }
        public string IFNSUL { get; set; }
        public string TERRIFNSUL { get; set; }
        public string OKATO { get; set; }
        public string OKTMO { get; set; }
        public DateTime? UPDATEDATE { get; set; }
        public string SHORTNAME { get; set; }
        public string SOCRNAME { get; set; }
        public string CODE { get; set; }
        public string PLAINCODE { get; set; }
        public string ACTSTATUSNAME { get; set; }
        public string CENTSTATUSNAME { get; set; }
        public string OPERSTATUSNAME { get; set; }
        public string CURRSTATUSNAME { get; set; }
        public string DOCNAME { get; set; }
        public string ADDRESSTEXT { get; set; }
        public bool? LIVESTATUS { get; set; }
        public string SHORTADDRESSTEXT { get; set; }
    }
}

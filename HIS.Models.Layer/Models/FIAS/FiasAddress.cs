﻿using System;

namespace HIS.Models.Layer.Models.FIAS
{
    public class FiasAddress
    {
        public Guid? AOID { get; set; }
        public Guid? HOUSEID { get; set; }
        public Guid? STEADID { get; set; }
        public string ADDRESSTEXT { get; set; }
        public string POSTALCODE { get; set; }
        public string HOUSENUM { get; set; }
        public string BUILDNUM { get; set; }
        public string STRUCTNUM { get; set; }
        public string BUILDSTATENAME { get; set; }
        public string LIVESTATUSTEXT { get; set; }
        public DateTime? STARTDATE { get; set; }
        public Guid? AOGUID { get; set; }
    }
}

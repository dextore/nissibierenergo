﻿using System;

namespace HIS.Models.Layer.Models.FIAS
{
    // Земельный участок
    public class FiasAddressStead
    {
        public System.Guid STEADGUID { get; set; }
        public System.Guid STEADID { get; set; }
        public string NUMBER { get; set; }
        public string POSTALCODE { get; set; }
        public string IFNSFL { get; set; }
        public string IFNSUL { get; set; }
        public string OKATO { get; set; }
        public string OKTMO { get; set; }
        public DateTime? UPDATEDATE { get; set; }
        public Guid? PARENTGUID { get; set; }
        public DateTime? STARTDATE { get; set; }
        public int DATAVERSIONID { get; set; }
        public DateTime? ENDDATE { get; set; }
        public Guid? NORMDOC { get; set; }
        public bool ISDELETE { get; set; }
        public byte? LIVESTATUS { get; set; }
        public string LIVESTATUSTEXT { get; set; }
    }
}

﻿using System;

namespace HIS.Models.Layer.Models.FIAS
{
    public class FiasFindedAddressesWithHousest
    {
        public System.Guid AOID { get; set; }
        //public System.Guid AOGuid { get; set; }
        public Guid? HOUSEID { get; set; }
        public Guid? STEADID { get; set; }
        public string ADDRESSTEXT { get; set; }
    }
}

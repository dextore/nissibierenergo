﻿namespace HIS.Models.Layer.Models.FIAS
{
    public class FiasEstateStatus
    {
        public int ESTSTATID { get; set; }
        public string NAME { get; set; }
        public int DATAVERSIONID { get; set; }
        public bool ISDELETE { get; set; }
    }
}

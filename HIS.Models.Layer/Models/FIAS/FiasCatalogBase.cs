﻿using System;

namespace HIS.Models.Layer.Models.FIAS
{
    public abstract class FiasCatalogBase
    {
        public Guid AOID { get; set; }
        public Guid? AOGUID { get; set; }
        public string NAME { get; set; }
    }
}

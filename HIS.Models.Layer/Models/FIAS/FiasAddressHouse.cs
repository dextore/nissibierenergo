﻿using System;

namespace HIS.Models.Layer.Models.FIAS
{
    // Здание/сооружение
    public class FiasAddressHouse
    {
        public string POSTALCODE { get; set; }
        public string IFNSFL { get; set; }
        public string TERRIFNSFL { get; set; }
        public string IFNSUL { get; set; }
        public string TERRIFNSUL { get; set; }
        public string OKATO { get; set; }
        public string OKTMO { get; set; }
        public DateTime? UPDATEDATE { get; set; }
        public string HOUSENUM { get; set; }
        public int? ESTSTATUS { get; set; }
        public string BUILDNUM { get; set; }
        public string STRUCNUM { get; set; }
        public int? STRSTATUS { get; set; }
        public Guid HOUSEID { get; set; }
        public Guid? HOUSEGUID { get; set; }
        public Guid? AOGUID { get; set; }
        public DateTime? STARTDATE { get; set; }
        public DateTime? ENDDATE { get; set; }
        public int? STATSTATUS { get; set; }
        public Guid? NORMDOC { get; set; }
        public int? COUNTER { get; set; }
        public int DATAVERSIONID { get; set; }
        public bool ISDELETE { get; set; }
        public byte? LIVESTATUS { get; set; }
        public string LIVESTATUSTEXT { get; set; }
        public string ESTSTATUSNAME { get; set; }
        public string STRSTATUSNAME { get; set; }
    }
}

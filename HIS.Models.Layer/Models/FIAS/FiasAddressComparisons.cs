﻿using System;

namespace HIS.Models.Layer.Models.FIAS
{
    public class FiasAddressComparisons
    {
        public Guid? AOid { get; set; }
        public Guid? HouseId { get; set; }
        public Guid? SteadId { get; set; }
        public Guid? AOGuid { get; set; }
        public Guid? ParentGuid { get; set; }
        public int? AOLevel { get; set; }
        public string FullName { get; set; }
        public Guid? FAOId { get; set; }
        public Guid? FHouseId { get; set; }
        public Guid? FSteadId { get; set; }
        public Guid? FAOGuid { get; set; }
        public Guid? FParentGuid { get; set; }
        public string FFullName { get; set; }
        public string SourceTypeId { get; set; }
    }
}

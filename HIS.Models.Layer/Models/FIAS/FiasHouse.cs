﻿using System;

namespace HIS.Models.Layer.Models.FIAS
{
    // Дома по адресному объекту (полное наименование)
    public class FiasHouse 
    {
        public Guid HouseId { get; set; }
        public string Number { get; set; }
    }
}

﻿namespace HIS.Models.Layer.Models.FIAS
{
    public class FiasAddressObjectType
    {
        public string SCNAME { get; set; }
        public string SOCRNAME { get; set; }
    }
}

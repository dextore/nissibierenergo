﻿namespace HIS.Models.Layer.Models.FIAS
{
    public class FiasFindedAddress
    {
        public System.Guid AOID { get; set; }
        public System.Guid AOGUID { get; set; }
        public string ADDRESSTEXT { get; set; }
        public bool LIVESTATUS { get; set; }
    }
}

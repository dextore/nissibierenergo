﻿using System;

namespace HIS.Models.Layer.Models.FIAS
{
    // Земельные участки по адресному объекту
    public class FiasStead
    {
        public Guid SteadId { get; set; }
        public string Number { get; set; }
    }
}

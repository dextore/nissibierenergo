﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "EntityCaptionModel", Module = "Markers")]
    public class EntityCaptionModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BAId { get; set; }

        [TsProperty(Name = "caption")]
        [JsonProperty(PropertyName = "caption")]
        public string Caption { get; set; }
    }
}

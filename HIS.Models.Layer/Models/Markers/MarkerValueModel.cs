﻿using Newtonsoft.Json;
using System;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "MarkerValueModel", Module = "Markers")]
    public class MarkerValueModel 
    {
        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int MarkerId { get; set; }

        [TsProperty(Name = "markerType")]
        [JsonProperty(PropertyName = "markerType")]
        public MarkerType MarkerType { get; set; }

        [TsProperty(Name = "MVCAliase")]
        [JsonProperty(PropertyName = "MVCAliase")]
        public string MVCAliase { get; set; }

        [TsProperty(Name = "itemId")]
        [JsonProperty(PropertyName = "itemId")]
        public int? ItemId { get; set; }

        [TsProperty(Name = "value")]
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime? StartDate { get; set; }

        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }

        [TsProperty(Name = "isVisible")]
        [JsonProperty(PropertyName = "isVisible")]
        public bool IsVisible { get; set; }

        [TsProperty(Name = "isBlocked")]
        [JsonProperty(PropertyName = "isBlocked")]
        public bool IsBlocked { get; set; }

        [TsProperty(Name = "note")]
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [TsProperty(Name = "displayValue")]
        [JsonProperty(PropertyName = "displayValue")]
        public string DisplayValue { get; set; }

        [TsProperty(Name = "isDeleted")]
        [JsonProperty(PropertyName = "isDeleted")]
        public bool IsDeleted { get; set; } = false;
    }
}
﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "MarkerValueModelBase", Module = "Markers")]
    public abstract class MarkerValueModelBase
    {
        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int MarkerId { get; set; }

        [TsProperty(Name = "markerName")]
        [JsonProperty(PropertyName = "markerName")]
        public string MarkerName { get; set; }

        [TsProperty(Name = "markerTypeId")]
        [JsonProperty(PropertyName = "markerTypeId")]
        public int MarkerTypeId { get; set; }

        [TsProperty(Name = "markerType")]
        [JsonProperty(PropertyName = "markerType")]
        public string MarkerType { get; set; }

        [TsProperty(Name = "entityTypeId")]
        [JsonProperty(PropertyName = "entityTypeId")]
        public int? EntityTypeId { get; set; }

        [TsProperty(Name = "range")]
        [JsonProperty(PropertyName = "range")]
        public IEnumerable<MarkerValueListItemModel> Range { get; set; }

        [TsProperty(Name = "value")]
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "MarkerValueModelRet", Module = "Markers")]
    public class MarkerValueModelRet
    {
        [Required]
        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int MarkerId { get; set; }

        [Required]
        [TsProperty(Name = "value")]
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }
}
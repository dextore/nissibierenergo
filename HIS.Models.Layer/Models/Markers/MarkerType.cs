﻿namespace HIS.Models.Layer.Models.Markers
{
    public enum MarkerType
    {
        MtBoolean = 1,   //1	 Логический
        MtInteger = 2,   //2	 Целочисленный
        MtDecimal = 3,   //3	 Вещественный
        MtString = 4,    //4	 Символьный
        MtList = 5,      //5	 Перечень
        MtMoney = 6,     //6	 Денежный
        MtDateTime = 10, //10 Дата и Время
        MtEntityRef = 11,//11 Ссылка на сущность
        MtAddress = 12,   //12 Ссылка на сущность адреса
        MtReference = 13  //13 Ссылка на справочник
    }
}

﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "MarkerValueListItemModel", Module = "Markers")]
    public class MarkerValueListItemModel
    {
        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int MarkerId { get; set; }

        [TsProperty(Name = "id")]
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
}
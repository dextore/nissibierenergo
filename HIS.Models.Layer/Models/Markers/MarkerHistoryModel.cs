﻿using Newtonsoft.Json;
using System;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "MarkerHistoryModel", Module = "Markers")]
    public class MarkerHistoryModel
    {
        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime? StartDate { get; set; }

        [TsProperty(Name = "endDate")]
        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }

        [TsProperty(Name = "sValue")]
        [JsonProperty(PropertyName = "sValue")]
        public string SValue { get; set; }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "PeriodicMarkerValueModelRet", Module = "Markers")]
    public class PeriodicMarkerValueModelRet
    {
        [Required]
        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int MarkerId { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime? StartDate { get; set; }

        [TsProperty(Name = "value")]
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [TsProperty(Name = "deleted")]
        [JsonProperty(PropertyName = "deleted")]
        public bool Deleted { get; set; }
    }
}
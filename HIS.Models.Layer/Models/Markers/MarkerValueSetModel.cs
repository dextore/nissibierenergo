﻿using Newtonsoft.Json;
using System;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "MarkerValueSetModel", Module = "Markers")]
    public class MarkerValueSetModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BAId { get; set; }

        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int MarkerId { get; set; }

        [TsProperty(Name = "value")]
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [TsProperty(Name = "startDate")]
        [JsonProperty(PropertyName = "startDate")]
        public DateTime StartDate { get; set; }
    }
}

﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "MarkerInfoModel", Module = "Markers")]
    public class MarkerInfoModel
    {
        [TsProperty(Name = "id")]
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [TsProperty(Name = "refBaTypeId")]
        [JsonProperty(PropertyName = "refBaTypeId")]
        public int? RefBaTypeId { get; set; }

        [TsProperty(Name = "markerType")]
        [JsonProperty(PropertyName = "markerType")]
        public MarkerType MarkerType { get; set; }

        [TsProperty(Name = "name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [TsProperty(Name = "label")]
        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [TsProperty(Name = "isPeriodic")]
        [JsonProperty(PropertyName = "isPeriodic")]
        public bool IsPeriodic { get; set; }

        [TsProperty(Name = "isCollectible")]
        [JsonProperty(PropertyName = "isCollectible")]
        public bool IsCollectible { get; set; }

        [TsProperty(Name = "isBlocked")]
        [JsonProperty(PropertyName = "isBlocked")]
        public bool IsBlocked { get; set; }

        [TsProperty(Name = "isRequired")]
        [JsonProperty(PropertyName = "isRequired")]
        public bool IsRequired { get; set; }

        [TsProperty(Name = "isOptional")]
        [JsonProperty(PropertyName = "isOptional")]
        public bool IsOptional { get; set; }

        [TsProperty(Name = "isImplemented")]
        [JsonProperty(PropertyName = "isImplemented")]
        public bool IsImplemented { get; set; }

        [TsProperty(Name = "implementTypeName")]
        [JsonProperty(PropertyName = "implementTypeName")]
        public string ImplementTypeName { get; set; }

        [TsProperty(Name = "catalogImplementTypeName")]
        [JsonProperty(PropertyName = "catalogImplementTypeName")]
        public string CatalogImplementTypeName { get; set; }

        [TsProperty(Name = "implementTypeField")]
        [JsonProperty(PropertyName = "implementTypeField")]
        public string ImplementTypeField { get; set; }

        [TsProperty(Name = "list")]
        [JsonProperty(PropertyName = "list")]
        public IEnumerable<ListItemModel> List { get; set; }

        [TsProperty(Name = "searchTypeId")]
        [JsonProperty(PropertyName = "searchTypeId")]
        public int? SearchTypeId { get; set; }
    }

    [TsClass(Name = "ListItemModel", Module = "Markers")]
    public class ListItemModel
    {
        [TsProperty(Name = "itemId")]
        [JsonProperty(PropertyName = "itemId")]
        public int ItemId { get; set; }

        [TsProperty(Name = "value")]
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }
}

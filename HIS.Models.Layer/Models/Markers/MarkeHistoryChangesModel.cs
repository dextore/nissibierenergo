﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "MarkeHistoryChangesModel", Module = "Markers")]
    public class MarkeHistoryChangesModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BaId { get; set; }

        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int MarkerId { get; set; }

        [TsProperty(Name = "removed")]
        [JsonProperty(PropertyName = "removed")]
        public IEnumerable<PeriodicMarkerValueModel> Removed { get; set; }
        [TsProperty(Name = "changes")]
        [JsonProperty(PropertyName = "changes")]
        public IEnumerable<PeriodicMarkerValueModel> Changes { get; set; }
    }
}

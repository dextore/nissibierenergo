﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "ReferenceMarkerValueItemModel", Module = "Markers")]
    public class ReferenceMarkerValueItemModel
    {
        [Required]
        [TsProperty(Name = "ItemId")]
        [JsonProperty(PropertyName = "ItemId")]
        public int ItemId { get; set; }

        [Required]
        [TsProperty(Name = "ItemName")]
        [JsonProperty(PropertyName = "ItemName")]
        public string ItemName { get; set; }
    }
}

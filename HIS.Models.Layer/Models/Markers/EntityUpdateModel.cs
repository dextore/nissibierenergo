﻿using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "EntityUpdateModel", Module = "Markers")]
    public class EntityUpdateModel : EntityMarkersModel
    {
        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public long BATypeId { get; set; }
    }
}

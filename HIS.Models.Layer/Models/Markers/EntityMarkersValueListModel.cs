﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "EntityMarkersValueListModel", Module = "Markers")]
    public class EntityMarkersValueListModel
    {
        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int MarkerId { get; set; }

        [TsProperty(Name = "values")]
        [JsonProperty(PropertyName = "values")]
        public IEnumerable<MarkerItemValueModel> Values { get; set; }
    }
}
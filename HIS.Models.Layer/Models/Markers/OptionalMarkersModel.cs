﻿using Newtonsoft.Json;
using TypeLite;
namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "OptionalMarkersModel", Module = "Markers")]
    public class OptionalMarkersModel
    {
        [TsProperty(Name = "baTypeId")]
        [JsonProperty(PropertyName = "baTypeId")]
        public int BATypeId { get; set; }

        [TsProperty(Name = "optionalMarkerId")]
        [JsonProperty(PropertyName = "optionalMarkerId")]
        public int OptionalMarkerId { get; set; }

        [TsProperty(Name = "markerId")]
        [JsonProperty(PropertyName = "markerId")]
        public int MarkerId { get; set; }

        [TsProperty(Name = "isVisible")]
        [JsonProperty(PropertyName = "isVisible")]
        public bool IsVisible { get; set; }

    }
}

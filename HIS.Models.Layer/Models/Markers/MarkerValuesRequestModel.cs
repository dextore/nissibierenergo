﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace HIS.Models.Layer.Models.Markers
{
    [TsClass(Name = "MarkerValuesRequestModel", Module = "Markers")]
    public class MarkerValuesRequestModel
    {
        [TsProperty(Name = "baId")]
        [JsonProperty(PropertyName = "baId")]
        public long BAId { get; set; }

        [TsProperty(Name = "markerIds")]
        [JsonProperty(PropertyName = "markerIds")]
        public IEnumerable<int> MarkerIds { get; set; }

        [TsProperty(Name = "names")]
        [JsonProperty(PropertyName = "names")]
        public IEnumerable<string> Names { get; set; }
    }
}

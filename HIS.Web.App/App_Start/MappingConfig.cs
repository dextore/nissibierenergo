﻿using System;
using System.Collections.Generic;
using AutoMapper;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.DAL.Client.Models.Address;
using HIS.DAL.Client.Models.AdminPanel;
using HIS.DAL.Client.Models.Auth;
using HIS.DAL.Client.Models.BankAccounts;
using HIS.DAL.Client.Models.Banks;
using HIS.DAL.Client.Models.BaseAncestors;
using HIS.DAL.Client.Models.Branches;
using HIS.DAL.Client.Models.Catalogs;
using HIS.DAL.Client.Models.Common;
using HIS.DAL.Client.Models.ConsignmentNotes;
using HIS.DAL.Client.Models.Contracts;
using HIS.DAL.Client.Models.Documents;
using HIS.DAL.Client.Models.Elements;
using HIS.DAL.Client.Models.Inventory;
using HIS.DAL.Client.Models.History;
using HIS.DAL.Client.Models.InventoryTransactions;
using HIS.DAL.Client.Models.LegalSubjects;
using HIS.DAL.Client.Models.LegalSubjects.AffiliateOrganisations;
using HIS.DAL.Client.Models.LegalSubjects.CommercePerson;
using HIS.DAL.Client.Models.LegalSubjects.Persons;
using HIS.DAL.Client.Models.LegalSubjects.Persons.Contacts;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.Client.Models.Nomenclatures;
using HIS.DAL.Client.Models.Operations;
using HIS.DAL.Client.Models.RegPoints;
using HIS.DAL.Client.Models.Suppliers;
using HIS.DAL.Client.Models.DocAcceptAssets;
using HIS.DAL.Client.Models.DocMoveAssets;
using HIS.DAL.Client.Models.InventoryObjectsMovementHistory;
using HIS.DAL.Client.Models.States;
using HIS.DAL.Client.Models.SystemSearch;
using HIS.DAL.Client.Models.SystemTree;
using HIS.DAL.DB.Context;
using HIS.Models.Layer.Contracts;
using HIS.Models.Layer.Models.Address;
using HIS.Models.Layer.Models.AdminPanel;
using HIS.Models.Layer.Models.Authentication;
using HIS.Models.Layer.Models.Banks;
using HIS.Models.Layer.Models.Branches;
using HIS.Models.Layer.Models.Catalogs;
using HIS.Models.Layer.Models.ConsignmentNotes;
using HIS.Models.Layer.Models.Contracts;
using HIS.Models.Layer.Models.Documents;
using HIS.Models.Layer.Models.Elements;
using HIS.Models.Layer.Models.Entity;
using HIS.Models.Layer.Models.Inventory;
using HIS.Models.Layer.Models.History;
using HIS.Models.Layer.Models.InventoryTransactions;
using HIS.Models.Layer.Models.LegalPersons;
using HIS.Models.Layer.Models.LegalSubjects;
using HIS.Models.Layer.Models.LegalSubjects.BankAccounts;
using HIS.Models.Layer.Models.LegalSubjects.CommercePersons;
using HIS.Models.Layer.Models.Markers;
using HIS.Models.Layer.Models.Nomenclatures;
using HIS.Models.Layer.Models.Operations;
using HIS.Models.Layer.Models.QueryBuilder;
using HIS.Models.Layer.Models.Suppliers;
using HIS.Models.Layer.Models.DocAcceptAssets;
using HIS.Models.Layer.Models.DocMoveAssets;
using HIS.Models.Layer.Models.InventoryObjectsMovementHistory;
using HIS.Models.Layer.Models.States;
using HIS.Models.Layer.Models.SystemTree;
using HIS.Models.Layer.Models.UniversalSearch;
using HIS.Web.App.Models;

//using HIS.Web.App.Models.LegalPersons;
//using HIS.Web.App.Models.Common;
//using HIS.Web.App.Models.Markers;
//using HIS.Web.App.Models.Contracts;
//using HIS.DAL.Client.Models.Nomenclatures;
//using HIS.Web.App.Models.Suppliers;
//using HIS.DAL.Client.Models.Elements;
//using HIS.Web.App.Models.Elements;

namespace HIS.Web.App
{
    public class MappingConfig
    {
        public static MapperConfiguration InitializeAutoMapper()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {

                cfg.CreateMap<StateStorageModel, StateStorageItem>();

                // Legal Person
                cfg.CreateMap<EFLSLegalSubject, LegalPersonImplemented>().ForMember(dest => dest.LegalPersonId, opt => opt.MapFrom(src => src.BAId));
                cfg.CreateMap<EFLSLegalSubject, LegalPersonInfo>().ForMember(dest => dest.LegalPersonId, opt => opt.MapFrom(src => src.BAId));

                //cfg.CreateMap<LSOrganisations, OrganizationItemViewDTO>()
                //.ForMember(dest => dest.OrganizationId, opt => opt.MapFrom(src => src.BAId));

                // request 

                cfg.CreateMap<EFLSLegalSubject, LegalPersonImplemented>().ForMember(dest => dest.LegalPersonId, opt => opt.MapFrom(src => src.BAId));

                cfg.CreateMap<LegalPersonImplemented, LegalPersonItemModel>();
                cfg.CreateMap<LegalPersonCollection, LegalPersonCollectionModel>();
                cfg.CreateMap<LegalPersonInfo, LegalPersonInfoModel>();
                cfg.CreateMap<MarkerValueInfo, MarkerValueModel>();
                cfg.CreateMap<MarkerValueModel, MarkerValueInfo>();
                cfg.CreateMap<MarkerValueSetModel, MarkerValueSet>();
                //cfg.CreateMap<MarkerValuePeriodInfo, MarkerValueModel>();
                cfg.CreateMap<ContactInfo, ContactModel>();
                cfg.CreateMap<ContactPhoneInfo, PhoneModel>();
                cfg.CreateMap<ContactEmailInfo, EmailModel>();
                cfg.CreateMap<MailTypeInfo, MailTypeModel>();

                //regPoint
                cfg.CreateMap<Element, ElementModel>();
                cfg.CreateMap<ElementRetModel, ElementRet>();

                //cfg.CreateMap<Contact, ContactItemModel>();
                cfg.CreateMap<MarkerValueListItem, MarkerValueListItemModel>();

                cfg.CreateMap<PeriodicMarkerValue, PeriodicMarkerValueModel>();
                //cfg.CreateMap<PeriodicMarkerHistory, MarkerHistoryModel>();
                //cfg.CreateMap<Contact, ContactItemModel>();
                cfg.CreateMap<LegalPerson, EFLSLegalSubject>().ForMember(dest => dest.BAId, opt => opt.MapFrom(src => src.LegalPersonId.HasValue ? src.LegalPersonId : -1));
                cfg.CreateMap<LegalPersonModel, LegalPerson>();

                cfg.CreateMap<LegalPerson, LegalPersonModel>();

                cfg.CreateMap<MarkerValueModelRet, MarkerValueRet>();
                cfg.CreateMap<PeriodicMarkerValueModelRet, MarkerValuePeriodRet>();
                cfg.CreateMap<EntityInfoItem, EntityInfoItemModel>();
                cfg.CreateMap<EntityInfoCollection, EntityInfoCollectionModel>();
                cfg.CreateMap<ContactModel, Contact>();
                cfg.CreateMap<PhoneModel, ContactPhone>();
                cfg.CreateMap<EmailModel, ContactEmail>();
                cfg.CreateMap<MailTypeModel, MailType>();
                cfg.CreateMap<EntityInfoTreeItem, EntityInfoTreeItemModel>();
                cfg.CreateMap<EntityInfoTreeCollection, EntityInfoTreeCollectionModel>();
                cfg.CreateMap<FullTextSearchEntityInfoItem, FullTextSearchEntityInfoModel>();
                cfg.CreateMap<FullTextSearchEntityInfoItemCollection, FullTextSearchEntityInfoCollectionModel>();

                cfg.CreateMap<Contract, ContractModel>();
                cfg.CreateMap<ContractTerm, ContractTermModel>();
                cfg.CreateMap<MarkerValueListItem, MarkerValueListItemModel>();

                cfg.CreateMap<ContractModel, Contract>();
                cfg.CreateMap<ContractInfo, ContractInfoModel>();

                cfg.CreateMap<ContractTermModel, ContractTerm>();

                cfg.CreateMap<EFCAContract, Contract>().ForMember(dest => dest.EndDate, 
                                                                  opt => opt.MapFrom(src => src.EndDate.HasValue ? src.EndDate.Value : new DateTime(9999, 1, 1) ))
                                                       .ForMember(dest => dest.ContractId, opt => opt.MapFrom(src => src.BAId));

                cfg.CreateMap<Nomenclature, NomenclatureModel>();
                cfg.CreateMap<ContractNomenclature, ContractNomenclatureModel>();

                cfg.CreateMap<NomenclatureSaveModel, NomenclatureSave>();
                cfg.CreateMap<ContractNomenclatureSaveModel, ContractNomenclatureSave>();

                cfg.CreateMap<SupplierItem, SupplierItemModel>();
                cfg.CreateMap<MarkerItemValue, MarkerItemValueModel>();
                cfg.CreateMap<EntityMarkersValueList, EntityMarkersValueListModel>();

                cfg.CreateMap<ContractNomenclatureTerm, ContractNomenclatureTermModel>();
                cfg.CreateMap<ContractNomenclatureTermModel, ContractNomenclatureTerm>();

                cfg.CreateMap<ContractElement, ContractElementModel>();
                cfg.CreateMap<ContractElementModel, ContractElement>();

                cfg.CreateMap<ElementExt, ElementExtModel>();

                cfg.CreateMap<ContractElementTerm, ContractElementTermModel>();
                cfg.CreateMap<ContractElementTermModel, ContractElementTerm>();

                cfg.CreateMap<EntityOperation, EntityOperationModel>();

                cfg.CreateMap<EntityCaption, EntityCaptionModel>();

                cfg.CreateMap<AddressUpdateResult, AddressUpdateResultModel>();
                cfg.CreateMap<FlatType, FlatTypeModel>();

                cfg.CreateMap<AddressObjectUpdateModel, AddressObjectUpdate>();
                cfg.CreateMap<HouseUpdateModel, HouseUpdate>();
                cfg.CreateMap<SteadUpdateModel, SteadUpdate>();

                cfg.CreateMap<AddressObjectUpdateResult, AddressObjectUpdateResultModel>();
                cfg.CreateMap<HouseUpdateResult, HouseUpdateResultModel>();
                cfg.CreateMap<SteadUpdateResult, SteadUpdateResultModel>();

                cfg.CreateMap<AddressObject, AddressObjectModel>();
                cfg.CreateMap<HouseUpdate, HouseUpdateModel>();
                cfg.CreateMap<AddressEntity, AddressEntityModel>();
                cfg.CreateMap<AddressComparisonsModel, AddressComparisonsDpModel>();

                cfg.CreateMap<AreaDataBase, AreaDataBaseModel>();

                cfg.CreateMap<CaptionAndAliase, CaptionAndAliaseModel>();
                cfg.CreateMap<OrderingModel, Ordering>();
                cfg.CreateMap<FilteringModel, Filtering>();
                
                 cfg.CreateMap<MarkerInfo, MarkerInfoModel>()
                     .ForMember(dest => dest.MarkerType, opt => 
                         opt.MapFrom(src => (MarkerType)Enum.ToObject(typeof(MarkerType), src.MarkerType)));

                cfg.CreateMap<ListItem, ListItemModel>();

                cfg.CreateMap<BanksEntity, FrontBanksEntity>();
                cfg.CreateMap<FrontBanksEntity, BanksEntity>();

                cfg.CreateMap<MarkeHistoryChangesModel, MarkeHistoryChanges>();

                cfg.CreateMap<MarkeHistoryChangesModel, MarkeHistoryChanges>();
                cfg.CreateMap<PeriodicMarkerValueModel, PeriodicMarkerValue>();

                cfg.CreateMap<Person, PersonModel>();

                cfg.CreateMap<PersonUpdateModel, PersonUpdate>();
                cfg.CreateMap<PersonInfoModel, PersonInfo>();
                cfg.CreateMap<PersonInfo, PersonInfoModel>();
                cfg.CreateMap<PersonInfoResponse, PersonInfoResponseModel>();
                cfg.CreateMap<PersonInfoResponseModel, PersonInfoResponse>();
                cfg.CreateMap<PersonInfoUpdateModel, PersonInfoUpdate>();
                cfg.CreateMap<PersonInfoUpdate, PersonInfoUpdateModel>();

                cfg.CreateMap<IEnumerable<BanksEntity>, IEnumerable<FrontBanksEntity>>();
                cfg.CreateMap<BanksEntity, FrontBanksEntity>();
                cfg.CreateMap<FrontBanksEntity, BanksEntity>();

                cfg.CreateMap<FrontBankMarker, BankMarker>();
                cfg.CreateMap<BankMarker, FrontBankMarker>();

                cfg.CreateMap<PersonHeader, PersonHeaderModel>();
                cfg.CreateMap<EntityState, EntityStateModel>();

                cfg.CreateMap<CommercePersonUpdateModel, CommercePersonUpdate>();
                cfg.CreateMap<CommercePersonUpdate, CommercePersonUpdateModel>();

                cfg.CreateMap<CommercePerson, CommercePersonModel>();

                cfg.CreateMap<ReferenceMarkerValueItem, ReferenceMarkerValueItemModel>();

                cfg.CreateMap<BranchModel, Branch>();
                cfg.CreateMap<Branch, BranchModel>();
                /// IInventory (Номенклатурный справочкин)
                cfg.CreateMap<InventoryListItem, InventoryListItemModel>();
                cfg.CreateMap<InventoryListFiltersModel, InventoryListFilters>();
                cfg.CreateMap<IInventory, IInventoryModel>();
                cfg.CreateMap<InventoryData, InventoryDataModel>();
                cfg.CreateMap<InventoryDataModel, InventoryData>();
                cfg.CreateMap<IGroups, IGroupsModel>();
                cfg.CreateMap<LSCompanyAreas, LSCompanyAreasModel>();
                cfg.CreateMap<RefSysUnits, RefSysUnitsModel>();
                cfg.CreateMap<OptionalMarkers, OptionalMarkersModel>();
               

                cfg.CreateMap<BranchItem, BranchItemModel>();
                cfg.CreateMap<BranchItemModel, BranchItem>();
                cfg.CreateMap<MarkerValueModel,MarkerValue>()
                    .ForMember(dest => dest.MarkerType,
                        opt => opt.MapFrom(src =>
                            (int)src.MarkerType));

                cfg.CreateMap<MarkerValue, MarkerValueModel>()
                    .ForMember(dest => dest.MarkerType, 
                        opt => opt.MapFrom(src => 
                            (MarkerType)Enum.ToObject(typeof(MarkerType), src.MarkerType))); 

                cfg.CreateMap<LSBankAccount, LSBankAccountModel>();
                cfg.CreateMap<LSBankAccountModel, LSBankAccount>();

                cfg.CreateMap<AffiliateOrganisation, AffiliateOrganisationModel>();
                cfg.CreateMap<AffiliateOrganisationModel, AffiliateOrganisation>();

                cfg.CreateMap<PersonLegalForm, PersonLegalFormModel>();
                cfg.CreateMap<OrganisationLegalForm, OrganisationLegalFormModel>();
                cfg.CreateMap<FAMovReasonsForm, FAMovReasonsFormModel>();
                cfg.CreateMap<FALocationsForm, FALocationsFormModel>();


                cfg.CreateMap<EntityHistory, EntityHistoryModel>();

                cfg.CreateMap<DocumentTreeItem, DocumentTreeItemModel>();

                cfg.CreateMap<InventoryTransaction, InventoryTransactionModel>();


                cfg.CreateMap<ConsignmentNotesDocumentModel, ConsignmentNotesDocument>();
                cfg.CreateMap<ConsignmentNotesDocument, ConsignmentNotesDocumentModel>();
                cfg.CreateMap<ConsignmentNotesDocumentUpdateModel, ConsignmentNotesDocumentUpdate>();
                cfg.CreateMap<ConsignmentNotesDocumentUpdate, ConsignmentNotesDocumentUpdateModel>();

                cfg.CreateMap<ConsignmentNotesSpecificationsModel, ConsignmentNotesSpecifications>();
                cfg.CreateMap<ConsignmentNotesSpecifications, ConsignmentNotesSpecificationsModel>();
                cfg.CreateMap<ConsignmentNotesSpecificationsUpdateModel, ConsignmentNotesSpecificationsUpdate>();
                cfg.CreateMap<ConsignmentNotesSpecificationsUpdate, ConsignmentNotesSpecificationsUpdateModel>();
                cfg.CreateMap<ConsignmentNotesFullDocument, ConsignmentNotesDocumentFullModel>();
                cfg.CreateMap<ConsignmentNotesDocumentFullModel, ConsignmentNotesFullDocument>();

                cfg.CreateMap<ConsignmentNotesSpecificationsFull, ConsignmentNotesSpecificationsFullModel>();
                cfg.CreateMap<ConsignmentNotesSpecificationsFullModel, ConsignmentNotesSpecificationsFull>();

                // акт приема ТМЦ
                cfg.CreateMap<DocAcceptAssetsRow, DocAcceptAssetsRowsModel>();
                cfg.CreateMap<DocAcceptAssetsRowsModel,DocAcceptAssetsRow>();
                cfg.CreateMap<DocAcceptAssetsDataModel, DocAcceptAssetsData>();
                cfg.CreateMap<DocAcceptAssetsData, DocAcceptAssetsDataModel>();
                cfg.CreateMap<ReceiptInvoiceSelect, ReceiptInvoiceSelectModel>();
                // документ перемещения/выбытия инвентарных объектов
                cfg.CreateMap<DocMoveAssetsData, DocMoveAssetsDataModel>();
                cfg.CreateMap<DocMoveAssetsDataModel, DocMoveAssetsData>();
                cfg.CreateMap<DocMoveAssetsRow, DocMoveAssetsRowModel>();
                cfg.CreateMap<DocMoveAssetsRowModel, DocMoveAssetsRow>();
                cfg.CreateMap<AssetsData, AssetsDataModel>();
                cfg.CreateMap<EntityViewItem, EntityViewItemModel>();
               


                cfg.CreateMap<SimpleSearchItem, SimpleSearchItemModel>();
                cfg.CreateMap<SearchResult, SearchResultModel>();

                cfg.CreateMap<InventoryAssets, InventoryAssetsModel>();
                cfg.CreateMap<InventoryAssetsModel, InventoryAssets>();
                cfg.CreateMap<InventoryAssetsMovementHistory, InventoryAssetsMovementHistoryModel>();
                cfg.CreateMap<InventoryAssetsMovementHistoryModel, InventoryAssetsMovementHistory>();

                cfg.CreateMap<EntityTypeState, EntityTypeStateModel>();
                cfg.CreateMap<LSContract, LSContractModel>();

                cfg.CreateMap<EntityUpdateModel, EntityUpdate>();
                cfg.CreateMap<BaTypesView, BaTypesModel>();
                cfg.CreateMap<BaTypesModel, BaTypesView>();
                cfg.CreateMap<MarkersAdminView, MarkersAdminModel>();
                cfg.CreateMap<MarkersAdminModel, MarkersAdminView>();
                cfg.CreateMap<ItemTableView, ItemTableViewModel>();
                cfg.CreateMap<ItemTableViewModel, ItemTableView>();
                cfg.CreateMap<BaTypesStatesView, BaTypesStatesModel>();
                cfg.CreateMap<BaTypesStatesModel, BaTypesStatesView>();
                cfg.CreateMap<BaTypesOperationsView, BaTypesOperationsModel>();
                cfg.CreateMap<BaTypesOperationsModel, BaTypesOperationsView>();
                cfg.CreateMap<BaTypesOperationsStateView, BaTypesOperationsStateModel>();
                cfg.CreateMap<BaTypesOperationsStateModel, BaTypesOperationsStateView>();
                cfg.CreateMap<SystemTreeItem, SystemTreeItemModel>();
                cfg.CreateMap<OnlyMarkersDataView, OnlyMarkersDataModel>();
                cfg.CreateMap<OnlyMarkersDataModel, OnlyMarkersDataView>();

                cfg.CreateMap<ContractInvent, ContractInventModel>();
                cfg.CreateMap<BaTypesStatesCreateView, BaTypesStatesCreateModel>();
                cfg.CreateMap<BaTypesStatesCreateModel, BaTypesStatesCreateView>();
                cfg.CreateMap<BaTypesOperationsCreateView, BaTypesOperationsCreateModel>();
                cfg.CreateMap<BaTypesOperationsCreateModel, BaTypesOperationsCreateView>();

                cfg.CreateMap<BaTypesOperationsStateCreateModel, BaTypesOperationsStateCreateView>();
                cfg.CreateMap<BaTypesOperationsStateCreateView, BaTypesOperationsStateCreateModel>();
            });

            return config;
        }
    }
}
using HIS.Web.App.Filters;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Filters;

namespace HIS.Web.App
{

    public static class WebApiConfig
    {

        //private const string ApiVersion = "v000001";
 
        public static void Register(HttpConfiguration config) {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { action = "Get", id = RouteParameter.Optional }
            );

            RegisterWebApiFilters(config.Filters);            
            JsonFormatterConfigure();

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
        }

        private static void RegisterWebApiFilters(HttpFilterCollection filters)
        {
            filters.Add(new AppExceptionFilterAttribute());
        }

        private static void JsonFormatterConfigure()
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings =
            new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Local,
            };
        }
    }

}

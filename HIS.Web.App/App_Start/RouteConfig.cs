using System.Web.Mvc;
using System.Web.Routing;

namespace HIS.Web.App
{
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Auth",
                url: "Auth/{action}/{id}",
                defaults: new { controller = "ExAuth", id = UrlParameter.Optional },
                namespaces: new[] { "HIS.Web.App.Controllers.API" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Security.Claims;
using HIS.Auth.DBContext;
using HIS.Auth.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using MohammadYounes.Owin.Security.MixedAuth;
using Owin;

[assembly: OwinStartup(typeof(HIS.Web.App.App_Start.OWINStartup))]

namespace HIS.Web.App.App_Start
{
    public class OWINStartup
    {
        public void Configuration(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(AuthDataBaseContext.Create);
            app.CreatePerOwinContext<HISUserManager>(HISUserManager.Create);
            app.CreatePerOwinContext<HISSignInManager>(HISSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            var cookieOptions = new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString(string.Empty),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = context => SecurityStampValidator.OnValidateIdentity<HISUserManager, HISUser, int>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentityCallback: (manager, user) => user.GenerateUserIdentityAsync(manager, context),
                        //regenerateIdentityCallback: (manager, user) => user.GenerateUserIdentityAsync(manager, context.Ide), 
                        getUserIdCallback: id => id.GetUserId<int>()).Invoke(context)
                }                
            };
            app.UseCookieAuthentication(cookieOptions);
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            //Enable Mixed Authentication
            //As we are using LogonUserIdentity, its required to run in PipelineStage.PostAuthenticate
            //Register this after any middleware that uses stage marker PipelineStage.Authenticate
            //See http://www.asp.net/aspnet/overview/owin-and-katana/owin-middleware-in-the-iis-integrated-pipeline      
            // Enable mixed auth
            app.UseMixedAuth(new MixedAuthOptions()
            {
                Provider = new MixedAuthProvider()
                {
                    OnImportClaims = identity =>
                    {
                        List<Claim> claims = new List<Claim>();
                        using (var principalContext = new PrincipalContext(ContextType.Domain)) //or ContextType.Machine
                        {
                            using (UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(principalContext, identity.Name))
                            {
                                if (userPrincipal != null)
                                {
                                    claims.Add(new Claim(ClaimTypes.Email, userPrincipal.EmailAddress ?? string.Empty));
                                    claims.Add(new Claim(ClaimTypes.Surname, userPrincipal.Surname ?? string.Empty));
                                    claims.Add(new Claim(ClaimTypes.GivenName, userPrincipal.GivenName ?? string.Empty));
                                }
                            }
                        }
                        return claims;
                    }                                        
                }               
            }, cookieOptions);
        }
    }
}

using System.Web.Optimization;

namespace HIS.Web.App {

    public class BundleConfig {

        public static void RegisterBundles(BundleCollection bundles) {

            var scriptBundle = new ScriptBundle("~/Scripts/bundle");
            var styleBundle = new StyleBundle("~/Content/bundle");

            // jQuery
            //scriptBundle
            //    .Include("~/Scripts/jquery-3.1.1.min.js");
            scriptBundle
                .Include("~/Scripts/jquery-3.1.1.js");

            scriptBundle
                .Include("~/Scripts/knockout-3.4.2.js");

            // Bootstrap
            scriptBundle
                .Include("~/Scripts/bootstrap.js");

            // Bootstrap
            styleBundle
                .Include("~/Content/bootstrap.css");

            // Custom site styles
            styleBundle
                .Include("~/Content/Site.css");

            bundles.Add(scriptBundle);
            bundles.Add(styleBundle);

#if !DEBUG
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}
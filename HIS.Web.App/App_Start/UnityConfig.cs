using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using HIS.DAL.DB.Handlers.Auth;
using HIS.FIAS.Handlers;
using HIS.FIAS.Interfaces;
using Microsoft.Owin;
using Unity;
using Unity.Injection;
using Unity.Lifetime;
using Unity.RegistrationByConvention;
using Unity.WebApi;


namespace HIS.Web.App
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterInstance(MappingConfig.InitializeAutoMapper().CreateMapper());

            container.RegisterInstance<IUnityContainer>(container);
            
            //����������� FIAS
            var url = System.Web.Configuration.WebConfigurationManager.AppSettings["fiasUrl"];
            var fiasService = new FiasHandler(url);

            container.RegisterInstance<IFiasHandler>(fiasService);

            AppDomain.CurrentDomain.Load("HIS.Business.Layer");
            AppDomain.CurrentDomain.Load("HIS.DAL");
            AppDomain.CurrentDomain.Load("HIS.Auth");

            var assemblies = AppDomain.CurrentDomain.GetAssemblies()
                                       .Where(a => a.GetName().Name.Contains("HIS.Business.Layer") ||
                                                   a.GetName().Name.Contains("HIS.DAL") ||
                                                   a.GetName().Name.Contains("HIS.Auth")).ToList();

            container.RegisterTypes(
                AllClasses.FromAssemblies(assemblies),
                WithMappings.FromMatchingInterface,
                WithName.Default,
                WithLifetime.PerThread);

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container); // Resolver ��� WebAPI
            DependencyResolver.SetResolver(new Unity.Mvc5.UnityDependencyResolver(container));             // Reslover ��� MVC
        }
    }
}
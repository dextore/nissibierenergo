﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace HIS.Web.App.Models
{
    public class EntityInfoTreeCollectionModel
    {
        [JsonProperty(PropertyName = "items")]
        public IEnumerable<EntityInfoTreeItemModel> Items { get; set; }
    }
}
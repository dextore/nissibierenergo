﻿using HIS.Models.Layer.Models.Contracts;

namespace HIS.Web.App.Models
{
    public class GroupMenuItemModel : MenuItemModel
    {
        public string groupName { get; set; }
    }
}
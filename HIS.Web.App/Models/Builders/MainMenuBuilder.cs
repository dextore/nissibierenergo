using System.Collections.Generic;
using HIS.DAL.Client.Models.Common;
using HIS.Models.Layer.Models.Contracts;


namespace HIS.Web.App.Models.Builders
{
    public class MainMenuBuilder
    {
        public static IEnumerable<MenuItemModel> GenerateMainMenu(IEnumerable<MenuItem> menu, List<MenuItemModel> menuToAdd = null)
        {
            List<MenuItemModel> mainMenu = menuToAdd ?? new List<MenuItemModel>();

            foreach (MenuItem mItem in menu)
            {
                MenuItemModel addItem = null;
                switch (mItem.GetType().ToString())
                {
                    case "MenuItem":
                        addItem = new MenuItemModel()
                        {
                            Name = mItem.Name,
                            Text = mItem.Desc
                        };
                        break;
                    case "CheckedMenuItem":
                        addItem = new CheckedMenuItemModel()
                        {
                            Name = mItem.Name,
                            Text = mItem.Desc,
                            isChecked = (mItem as CheckedMenuItem).IsChecked
                        };
                        break;
                }



                if (addItem != null)
                {
                    mainMenu.Add(addItem);
                }
            }

            return mainMenu;
        }

        public static IEnumerable<MenuItemModel> GenerateMainMenu()
        {
            MainToolBarCommands commands = new MainToolBarCommands();
            var commandItems = commands.getCommands();

            List<MenuItemModel> mainMenu = new List<MenuItemModel>()
            {
                new MenuItemModel
                {
                    Name = "Contractors",
                    Text = "Контрагенты",
                    items = new List<MenuItemModel>
                    {
                        new MenuItemModel
                        {
                            Name = "fiz-person-tree-module",
                            Text = "Физические лица",
                            Icon = "icon icon-ios-people",
                            ModuleName = "fiz-person-tree-module",
                            MVCAliases = new[] {"Person", "CommercePerson"}
                        },
                        new MenuItemModel
                        {
                            Name = "legal-person-tree-module",
                            Text = "Юридические лица",
                            ModuleName = "legal-person-tree-module",
                            MVCAliases = new[] {"Organisation", "AffiliateOrganisation"}
                        },
                        new MenuItemModel
                        {
                            Name = "contract-tree-module",
                            Text = "Договоры",
                            Icon = "ion ion-android-contacts",
                            ModuleName = "contract-tree-module",
                            MVCAliases = new[] {"Contract"}
                        }


                    }
                },
                new MenuItemModel
                {
                    Name = "Documents",
                    Text = "Документы",
                    items = new List<MenuItemModel>
                    {
                        new MenuItemModel
                        {
                            Name = "documents-tree-module",
                            Text = "Документы",
                            ModuleName = "documents-tree-module",
                        },
                        new MenuItemModel
                        {
                            Name = "consignment-notes-module",
                            Text = "Приходные накладные",
                            ModuleName = "consignment-notes-module",
                            MVCAliases = new[] {"ReceiptInvoice"}
                        },
                        new MenuItemModel
                        {
                            Name = "document-accept-assets-module",
                            Text = "Акты приема ТМЦ",
                            ModuleName = "document-accept-assets-module",
                            MVCAliases = new[] {"FADocAcceptFAssets"}
                        },
                        new MenuItemModel
                        {
                            Name = "document-move-assets-module",
                            Text = "Перемещение/выбытие инвентарных объектов",
                            ModuleName = "document-move-assets-module",
                            MVCAliases = new[] {"FAssetsMovement"}
                        }
                    }
                },
                new MenuItemModel
                {
                    Name = "ElementsOfScheme",
                    Text = "Элементы схемы",
                    items = new List<MenuItemModel>
                    {
                        new MenuItemModel
                        {
                            Name = "real-scheme-element-module",
                            Text = "Элементы",
                            ModuleName = "real-scheme-element-module",
                            MVCAliases = new[] { "RealSchemeElement" }
                        },
                        new MenuItemModel
                        {
                            Name = "ObjectModule",
                            Text = "Объекты",
                            ModuleName = "objects-module"
                        },
                        new MenuItemModel
                        {
                            Name = "RegPointModule",
                            Text = "Точки учета",
                            ModuleName = "RegPointModule"
                        },
                        new MenuItemModel
                        {
                            Name = "fAssets-module",
                            Text = "Список инвентарных объектов",
                            ModuleName = "fAssets-module",
                            MVCAliases = new[] {"FAssets"}
                        } 
                    }
                },
                new MenuItemModel
                {
                    Name = "References",
                    Text = "Справочники",
                    items = new List<MenuItemModel>
                    {
                        new MenuItemModel
                        {
                            Name = "banks-module",
                            Text = "Справочник банков",
                            ModuleName = "banks-module",
                            MVCAliases = new[] {"Bank"}
                        },
                        new MenuItemModel
                        {
                            Name = "InventoryReferenceModule",
                            Text = "Номенклатурный справочник",
                            ModuleName = "references-inventory-module",
                            MVCAliases = new[] {"Inventory"}
                        }
                    }

                },
                new MenuItemModel
                {
                    Name = "Administration",
                    Text = "Администрирование",
                    items = new List<MenuItemModel>
                    {
                        new MenuItemModel
                        {
                            Name = "Administration-Entitys",
                            Text = "Администрирование сущностей и маркеров",
                            items = new List<MenuItemModel>
                            {
                                new MenuItemModel
                                {
                                    Name = "admin-panel-module",
                                    Text = "Типы сущностей",
                                    ModuleName = "admin-panel-module"
                                },
                                new MenuItemModel
                                {
                                    Name = "adminPanelOnlyMarkersList-panel-module",
                                    Text = "Администрирование маркеров",
                                    ModuleName = "adminPanelOnlyMarkersList-panel-module"
                                },
                                new MenuItemModel
                                {
                                    Name = "adminPanelOnlyStateList-panel-module",
                                    Text = "Администрирование состояний",
                                    ModuleName = "adminPanelOnlyStateList-panel-module"
                                },
                                new MenuItemModel
                                {
                                    Name = "adminPanelOnlyOperationList-panel-module",
                                    Text = "Администрирование операций",
                                    ModuleName = "adminPanelOnlyOperationList-panel-module"
                                },
                                new MenuItemModel()
                                {
                                    Name = "Administration-Entitys-Href",
                                    Text = "<a href='http://srv-newis:8080'>ЕИС Гермес.WEB (ref)</a>",
                                    //Text = "http://srv-newis:8080"
                                },
                                new MenuItemModel()
                                {
                                    Name = "Administration-Entitys-Href",
                                    Text =
                                        "<a href='http://srv-newis:8080/admin/EntityTypes'>ЕИС Гермес.WEB/EntityTypes (ref)</a>"
                                    //Text = "http://srv-newis:8080/admin/EntityTypes"
                                },
                                new MenuItemModel()
                                {
                                    Name = "Administration-Entitys-Href",
                                    Text =
                                        "<a href='http://srv-newis:8080/admin/Markers'>ЕИС Гермес.WEB/Markers (ref)</a>"
                                    //Text = "http://srv-newis:8080/admin/Markers"
                                }
                            }
                        },
                        new MenuItemModel
                        {
                            Name = "AddressMappingModule",
                            Text = "Сопоставление адресов",
                            ModuleName = "address-mapping-module"
                        },
                        new MenuItemModel
                        {
                            Name = "DevelopmentModules",
                            Text = "Для разработки",
                            items = new List<MenuItemModel>
                            {
                                new MenuItemModel
                                {
                                    Name = "ModuleTest",
                                    Text = "Тестовый модуль",
                                    ModuleName = "tests-module"
                                },
                                new MenuItemModel
                                {
                                    Name = "ModuleRights",
                                    Text = "Права (роли, карточки доступа и т.п.)",
                                    ModuleName = "RightsModule"
                                },
                                new MenuItemModel
                                {
                                    Name = "ModulePhisicalPersons",
                                    Text = "Физические лица",
                                    Icon = "ion ion-ios-people",
                                    ModuleName = "PhysicalPersonsModule"
                                },
                                new MenuItemModel
                                {
                                    Name = "ModuleLegalPersons",
                                    Text = "Юридические лица",
                                    ModuleName = "legal-person-module"
                                },
                                new MenuItemModel
                                {
                                    Name = "ConPointModule",
                                    Text = "Точки потребления",
                                    ModuleName = "ConPointModule"
                                },
                                new MenuItemModel
                                {
                                    Name = "TestBanksModule",
                                    Text = "Справочник банков",
                                    ModuleName = "banks-module-test",
                               
                                },
                            }

                        },


                    }
                }
            };
            return mainMenu;
        }
    } 
}
﻿using System.Collections.Generic;

namespace HIS.Web.App.Models.Builders
{
    public class MainToolBarCommands
    {
        public List<CommanItem> getCommands()
        {
            return new List<CommanItem>
            {
                new CommanItem
                {
                    Icon = "doc",
                    Hint = "Создать",
                    Id = "tbNew",
                    Name = "New"
                },
                new CommanItem
                {
                    Icon = "edit",
                    Hint = "Изменить",
                    Id = "tbEdit",
                    Name = "Edit"
                },
                new CommanItem
                {
                    Icon = "save",
                    Hint = "Сохранить",
                    Id = "tbSave",
                    Name = "Save"
                },
                new CommanItem
                {
                    Icon = "revert",
                    Hint = "Отказаться",
                    Id = "tbCancel",
                    Name = "Cancel"
                },
                new CommanItem
                {
                    Icon = "ion ion-trash-a",
                    Hint = "Удалить",
                    Id = "tbDelete",
                    Name = "Delete"
                },
                new CommanItem
                {
                    Icon = "ion ion-android-folder-open",
                    Hint = "Открыть",
                    Id = "tbOpen",
                    Name = "Open"
                },
                new CommanItem
                {
                    Icon = "find",
                    Hint = "Поиск",
                    Id = "tbFind",
                    Name = "Find"
                },
                new CommanItem
                {
                    Icon = "refresh",
                    Hint = "Обновить",
                    Id = "tbRefresh",
                    Name = "Refresh"
                },
                new CommanItem
                {
                    Icon = "Content/Icons/preview.svg",
                    Hint = "Просмотр",
                    Id = "tbPreview",
                    Name = "Preview"
                },
                new CommanItem
                {
                    Icon = "ion ion-printer",
                    Hint = "Печать",
                    Id = "tbPrint",
                    Name = "Print"
                }
            };
        }

    }

    public class CommanItem
    {
        public string Icon { get; set; }
        public string Hint { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
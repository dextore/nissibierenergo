﻿using Newtonsoft.Json;

namespace HIS.Web.App.Models
{
    public class StateStorageModel
    {
        [JsonProperty(PropertyName = "key")]
        public string Key { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace HIS.Web.App.Models.Authentication
{
    public class HISExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Domain Name")]
        public string Name { get; set; }
    }
}
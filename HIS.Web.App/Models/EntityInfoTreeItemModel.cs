﻿using Newtonsoft.Json;
using System.Collections.Generic;
using HIS.Models.Layer.Models.Entity;
using TypeLite;

namespace HIS.Web.App.Models
{
    [TsClass(Name = "EntityInfoTreeItemModel", Module = "Entyty")]
    public class EntityInfoTreeItemModel : EntityInfoItemModel
    {
        /// <summary>
        /// ИД в дереве 
        /// </summary>
        [TsProperty(Name = "treeId")] 
        [JsonProperty(PropertyName = "treeId")]
        public long TreeId { get; set; }
        /// <summary>
        /// Ссылка на родительские элемент в дереве
        /// </summary>
        [TsProperty(Name = "parentTreeId")]
        [JsonProperty(PropertyName = "parentTreeId")]
        public long? ParentTreeId { get; set; }
        /// <summary>
        /// Признак наличия вложенных элементов
        /// </summary>
        [TsProperty(Name = "isHasChildrenItems")]
        [JsonProperty(PropertyName = "isHasChildrenItems")]
        public bool IsHasChildrenItems { get; set; }
        /// <summary>
        /// Родительский идентификатор именно СУЩНОСТИ для текущего элемента дерева
        /// </summary>
        [TsProperty(Name = "parentBaId")]
        [JsonProperty(PropertyName = "parentBaId")]
        public long? ParentBaId { get; set; }
        /// <summary>
        /// Вложенные элементы сущности
        /// </summary>
        [TsProperty(Name = "items")]
        [JsonProperty(PropertyName = "items")]
        public IEnumerable<EntityInfoTreeItemModel> Items { get; set; }
    }
}
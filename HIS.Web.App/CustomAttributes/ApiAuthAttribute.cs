﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Controllers;
using HIS.DAL.Client.Services.Auth;


namespace HIS.Web.App.CustomAttributes
{
    /// <summary>
    /// Атрибут проверки прав, ролей, пользоветелей для API контроллеров 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ApiAuthAttribute : AuthorizeAttribute
    {   
        public string Rights { get; set; }

        /// <summary>
        /// Unity-контейнер
        /// </summary>
        private IAuthService AuthService => (IAuthService) GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IAuthService));

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            bool isAuthenticated = actionContext.RequestContext.Principal.Identity.IsAuthenticated;
            if (!isAuthenticated) return false;

            bool checkUsers;
            bool checkRoles;
            bool checkRights;

            if (!string.IsNullOrEmpty(Rights))
            {
                checkRights = AuthService.CheckRights(Rights);
                if (checkRights) return true;
            }
            else
            {
                checkRights = true;
            }

            if (!string.IsNullOrEmpty(Roles))
            {
                checkRoles = AuthService.CheckRoles(Roles);
                if (checkRoles) return true;
            }
            else
            {
                checkRoles = true;
            }

            if (!string.IsNullOrEmpty(Users))
            {
                checkUsers = AuthService.CheckUsers(Users);
                if (checkUsers) return true;
            }
            else
            {
                checkUsers = true;
            }

            return checkRights && checkRoles && checkUsers;
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            HttpRequestMessage request = actionContext.Request;
            HttpRequestHeaders requestHeaders = request.Headers;
            HttpResponseMessage responseMessage;

            if (requestHeaders.Contains("X-Requested-With") && requestHeaders.GetValues("X-Requested-With").FirstOrDefault() == "XMLHttpRequest") //IsAjaxRequest
            {                
                responseMessage = new HttpResponseMessage()
                {
                    Content = new StringContent("{\"Result\":\"Error\", \"Message\":\"Пользователь не авторизован, либо нет прав на выполнение данного метода.\"}")
                };
                responseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                actionContext.Response = responseMessage;
            }
            else
            {
                responseMessage = new HttpResponseMessage()
                {
                    Content = new StringContent("Пользователь не авторизован, либо нет прав на выполнение данного метода.")
                };
            }            

            actionContext.Response = responseMessage;
        }
    }
}

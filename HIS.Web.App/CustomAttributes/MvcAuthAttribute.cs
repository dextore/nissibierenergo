﻿using System;
using System.Web;
using System.Web.Mvc;
using HIS.DAL.Client.Services.Auth;

namespace HIS.Web.App.CustomAttributes
{
    /// <summary>
    /// Атрибут проверки прав, ролей, пользоветелей для MVC контроллеров 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class MvcAuthAttribute : AuthorizeAttribute
    {
        public string Rights { get; set; }

        /// <summary>
        /// Unity-контейнер
        /// </summary>
        private IAuthService AuthService => DependencyResolver.Current.GetService<IAuthService>();

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool isAuthenticated = httpContext.User.Identity.IsAuthenticated;
            if (!isAuthenticated) return false;

            bool checkUsers;
            bool checkRoles;
            bool checkRights;

            if (!string.IsNullOrEmpty(Rights))
            {
                checkRights = AuthService.CheckRights(Rights);
                if (checkRights) return true;
            }
            else
            {
                checkRights = true;
            }

            if (!string.IsNullOrEmpty(Roles))
            {
                checkRoles = AuthService.CheckRoles(Roles);
                if (checkRoles) return true;
            }
            else
            {
                checkRoles = true;
            }

            if (!string.IsNullOrEmpty(Users))
            {
                checkUsers = AuthService.CheckUsers(Users);
                if (checkUsers) return true;
            }
            else
            {
                checkUsers = true;
            }            

            return checkRights && checkRoles && checkUsers;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}
﻿declare namespace sysTree {
    interface ISysTreeItem {
        baId: number;
        name: string;
        stateId: number;
        stateName: string;
        baTypeId: number;
        baTypeName: string;
        mvcAlias: string;
    }
}
﻿

declare namespace webix {
    namespace ui {

        class textExtended implements webix.ui.baseview {
            adjust(): void;
            attachEvent(type: textEventName, functor: WebixCallback, id?: string): string | number;
            bind(target: any, rule?: WebixCallback, format?: string): void;
            blockEvent(): void;
            blur(): void;
            callEvent(name: string, params: any[]): boolean;
            define(property: string, value: any): void;
            destructor(): void;
            detachEvent(id: string): void;
            disable(): void;
            enable(): void;
            focus(): void;
            getChildViews(): any[];
            getFormView(): webix.ui.baseview;
            getInputNode(): HTMLElement;
            getNode(): any;
            getParentView(): any;
            getTopParentView(): webix.ui.baseview;
            getValue(): string;
            hasEvent(name: string): boolean;
            hide(): void;
            isEnabled(): boolean;
            isVisible(): boolean;
            mapEvent(map: any): void;
            queryView(config: any, mode?: string): any;
            refresh(): void;
            render(id: string | number, data: any, type: string): void;
            resize(): void;
            setBottomText(text: string): void;
            setValue(value: string): void;
            show(force?: boolean, animation?: boolean): void;
            sync(source: any, filter: WebixCallback, silent: boolean): void;
            unbind(): void;
            unblockEvent(): void;
            validate(): boolean;

            $compareValue: WebixCallback;
            $getSize(): any[];
            $getValue(): string;
            $height: number;
            $prepareValue: WebixCallback;
            $render: WebixCallback;
            $renderIcon: WebixCallback;
            $renderInput(config: any): HTMLElement;
            $renderLabel(config: any, id: string | number): string;
            $scope: any;
            $setSize(x: number, y: number): boolean;
            $setValue(value: string): void;
            $skin: WebixCallback;
            $view: HTMLElement;
            $width: number;
            config: textConfig;
            name: string;
            on_click: WebixCallback;
            touchable: boolean;
        }

        class datepickerExtended implements webix.ui.baseview {
            getValidateTextValue(): boolean;
            adjust(): void;
            attachEvent(type: datepickerEventName, functor: WebixCallback, id?: string): string | number;
            bind(target: any, rule?: WebixCallback, format?: string): void;
            blockEvent(): void;
            blur(): void;
            callEvent(name: string, params: any[]): boolean;
            define(property: string, value: any): void;
            destructor(): void;
            detachEvent(id: string): void;
            disable(): void;
            enable(): void;
            focus(): void;
            getChildViews(): any[];
            getFormView(): webix.ui.baseview;
            getInputNode(): HTMLElement;
            getNode(): any;
            getParentView(): any;
            getPopup(): webix.ui.baseview;
            getText(): string;
            getTopParentView(): webix.ui.baseview;
            getValue(): string;
            hasEvent(name: string): boolean;
            hide(): void;
            isEnabled(): boolean;
            isVisible(): boolean;
            mapEvent(map: any): void;
            queryView(config: any, mode?: string): any;
            refresh(): void;
            render(id: string | number, data: any, type: string): void;
            resize(): void;
            setBottomText(text: string): void;
            setValue(value: string): void;
            show(force?: boolean, animation?: boolean): void;
            sync(source: any, filter: WebixCallback, silent: boolean): void;
            unbind(): void;
            unblockEvent(): void;
            validate(): boolean;
            $compareValue: WebixCallback;
            $getSize(): any[];
            $getValue(): string;
            $height: number;
            $prepareValue: WebixCallback;
            $render: WebixCallback;
            $renderIcon: WebixCallback;
            $renderInput(obj: any, html: string, id: string | number): string;
            $renderLabel(config: any, id: string | number): string;
            $scope: any;
            $setSize(x: number, y: number): boolean;
            $setValue(value: string): void;
            $skin: WebixCallback;
            $view: HTMLElement;
            $width: number;
            config: datepickerConfig;
            name: string;
            on_click: WebixCallback;
            touchable: boolean;
        }
    }
}

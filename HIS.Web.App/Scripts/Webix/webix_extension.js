﻿/*
 * this modify text component  
 */

webix.protoUI({
    name: "textExtended",
    $cssName: "webix_view webix_control webix_el_text",
    $setValue: function (value) {

        if (this._settings.readonly) {
            return;
        }

        this.getInputNode().value = this._pattern(value);
    },
  
}, webix.ui.text);

/*
* This is modify datepicker component
*/
webix.protoUI({
    name: "datepickerExtended",
    $cssName: "search protoUi-with-icons-1",
    _isFocused:false,
    isRenderComponent: true,
    $init: function () {
        this.$ready.push(this._init_popup);
        this.$ready.push(this._addPopupListener);
    },
    _addPopupListener: function () {

        this.getPopup().getBody().attachEvent("onDateClear", webix.bind(function (id) {

            if (this._settings.readonly != true && this._settings.readonly != "true" || this.isRenderComponent === true) {

                this._settings.text = "";
                this._set_visible_text();
                this.isRenderComponent = false;
            }

        }, this));

    },
    defaults: {
        template: function (obj, common) {
            if (common._settings.type == "time") {
                common._settings.icon = common._settings.timeIcon;
            }
            var t = obj.type; obj.type = "";
            var res = obj.editable ? common.$renderInput(obj) : common._render_div_block(obj, common);
            obj.type = t;
            return res;
        },
        editable: true,
        stringResult: false,
        timepicker: false,
        icon: "calendar",
        icons: true,
        timeIcon: "clock-o",
        separator: ", "
    },
    on_click: {
        "webix_input_icon": function (e) {
            // add show popup and remove show from imput
        }
    },
    _onBlur: function () {
        this._isFocused = false;
        if (this._settings.text == this.getText() || (webix.isUndefined(this._settings.text) && !this.getText())) {
            return;
        }
        var value = this.getPopup().getValue();
        if (value)
            this.setValue(value);
    },
    $skin: function () {
        this.defaults.inputPadding = webix.skin.$active.inputPadding;
    },
    getPopup: function () {
        return webix.$$(this._settings.popup);
    },
    _init_popup: function () {
        var obj = this._settings;
        if (obj.suggest)
            obj.popup = obj.suggest;
        else if (!obj.popup) {

            var timepicker = this._settings.timepicker;
            obj.popup = obj.suggest = this.suggest_setter({
                type: "calendar",
                point: this._settings.point === false ? false : true,
                height: 240 + (timepicker ? 30 : 0),
                width: 250,
                padding: 0,
                body: {
                    multiselect: this._settings.multiselect,
                    timepicker: timepicker,
                    type: this._settings.type,
                    icons: this._settings.icons
                }
            });

        }
        this._init_once = function () { };
    },
    $prepareValue: function (value) {

        if (this._settings.multiselect) {
            if (typeof value === "string")
                value = value.split(this._settings.separator);
            else if (value instanceof Date) {
                value = [value];
            } else if (!value) {
                value = [];
            }

            for (var i = 0; i < value.length; i++) {
                value[i] = this._prepareSingleValue(value[i]);
            }
            return value;

        } else {
            return this._prepareSingleValue(value);
        }
    },
    _prepareSingleValue: function (value) {

        var type = this._settings.type;
        var timeMode = type == "time";

        //setValue("1980-12-25")
        if (!isNaN(parseFloat(value)))
            value = "" + value;

        if (typeof value == "string" && value) {
            var formatDate = null;
            if ((type == "month" || type == "year") && this._formatDate) {
                formatDate = this._formatDate;
            }
            else
                formatDate = (timeMode ? webix.i18n.parseTimeFormatDate : webix.i18n.parseFormatDate);
            value = formatDate(value);
        }

        if (value) {

            if (timeMode) {
                if (webix.isArray(value)) {
                    var time = new Date();
                    time.setHours(value[0]);
                    time.setMinutes(value[1]);
                    value = time;
                }
            }
            if (isNaN(value.getTime()))
                value = "";
        }

        return value;
    },
    _get_visible_text: function (value) {

        if (this._settings.multiselect) {
            return []
                .concat(value)
                .map((function (a) { return this._get_visible_text_single(a); }).bind(this))
                .join(this.config.separator);
        } else
            return this._get_visible_text_single(value);
    },
    _get_visible_text_single: function (value) {

        var timeMode = this._settings.type == "time";
        var timepicker = this.config.timepicker;
        var formatStr = this._formatStr || (timeMode ? webix.i18n.timeFormatStr : (timepicker ? webix.i18n.fullDateFormatStr : webix.i18n.dateFormatStr));

        if (this._settings.pattern) {
            if (typeof value !== "object") {
                return this._pattern(value);
            }

        }
        return formatStr(value);

    },
    _set_visible_text: function () {

        var node = this.getInputNode();
        if (node.value == webix.undefined) {
            node.innerHTML = this._pattern(this._settings.text) || this._get_div_placeholder();
        }
        else {
            node.value = this._settings.text || "";
        }

    },
    $compareValue: function (oldvalue, value) {

        if (!oldvalue && !value) return true;
        return webix.Date.equal(oldvalue, value);

    },
    $render: function (obj) {

        this.isRenderComponent = true;
        if (webix.isUndefined(obj.value)) {
            this.isRenderComponent = false;
            return;

        }
        obj.value = this.$prepareValue(obj.value);
        this.$setValue(obj.value);
        this.isRenderComponent = false;

        webix._event(this.getInputNode(), "focus", webix.bind(function (e) {
            this._isFocused = true;
        }, this));

        webix._event(this.getInputNode(), "keydown", webix.bind(function (e) {

            if (e.code == "Backspace" || e.code == "Delete") {
                var selected = this._getSelectedText();
                if (selected != "" && (selected == this._settings.text)) {
                    this._settings.text = "";
                    this._set_visible_text();
                    this.setValue(null);
                }
            }

          

        }, this));
    },
    _getSelectedText: function() {
        var text = "";
        if (text = window.getSelection) {
            text = window.getSelection().toString();
        } else { 
            //для IE
            text = document.selection.createRange().text;
        }
        return text;
    },
    setValue: function (value) {

        value = this.$prepareValue(value);
        var oldvalue = this._settings.value;

        if (value == null && this._isFocused == false) {
            this._settings.text = "";
            this._set_visible_text();
        }

        if (this.$compareValue(oldvalue, value)) return false;

        if (this._rendered_input)
            this.$setValue(value);

        this._settings.value = value;
        this.callEvent("onChange", [value, oldvalue]);
    },
    $setValue: function (value) {
        if (this._settings.readonly != true && this._settings.readonly != "true" || this.isRenderComponent === true) {

            var nodeText = this.getInputNode().value;
            this._settings.text = (value ? this._get_visible_text(value) : "");


            if (this._settings.text == "" && nodeText != ""
                && nodeText.length > 1) {
                this._settings.text = nodeText;
            }
            this._set_visible_text();
            this.isRenderComponent = false;
        }
    },
    format_setter: function (value) {
        if (value) {
            if (typeof value === "function")
                this._formatStr = value;
            else {
                this._formatStr = webix.Date.dateToStr(value);
                this._formatDate = webix.Date.strToDate(value);
            }
        }
        else
            this._formatStr = this._formatDate = null;
        return value;

    },
    getInputNode: function () {

        return this._settings.editable ? this._dataobj.getElementsByTagName('input')[0] : this._dataobj.getElementsByTagName("DIV")[1];

    },
    getValue: function () {

        if (this._settings.multiselect) {
            var value = this._settings.value;
            if (!value) return [];

            var result = []
                .concat(value)
                .map((function (a) { return this._get_value_single(a); }).bind(this));

            if (this._settings.stringResult)
                return result.join(this._settings.separator);

            return result;
        }

        return this._get_value_single(this._settings.value);
    },
    _get_value_single: function (value) {

        var type = this._settings.type;
        var timeMode = (type == "time");
        var timepicker = this.config.timepicker;

        if (!this._rendered_input)
            value = this.$prepareValue(value) || null;

        else if (this._settings.editable) {
            var formatDate = this._formatDate || (timeMode ? webix.i18n.timeFormatDate : (timepicker ? webix.i18n.fullDateFormatDate : webix.i18n.dateFormatDate));
            value = formatDate(this.getInputNode().value);
        }

        if (this._settings.stringResult) {
            var formatStr = webix.i18n.parseFormatStr;
            if (timeMode)
                formatStr = webix.i18n.parseTimeFormatStr;

            if (this._formatStr && (type == "month" || type == "year")) {
                formatStr = this._formatStr;
            }

            if (this._settings.multiselect)
                return [].concat(value).map((function (a) { return a ? formatStr(a) : ""; }));
            return (value ? formatStr(value) : "");
        }

        if (typeof value === "object" && value != null && !this.isValidTextDate()) {
            return null;
        }

        return value || null;
    },
    isValidTextDate: function () {

        var stringDate = this.getInputNode().value;
        if (!stringDate) {
            return false;
        }
        if (stringDate.length < 10) {
            return false;
        }
        var input = stringDate.match(/\d+/g);
        var date = new Date(input[2], input[1] - 1, input[0]);

        return (date.getFullYear() === parseInt(input[2]))
            && (date.getMonth() == parseInt(input[1] - 1))
            && (date.getDate() === parseInt(input[0]));
    },
    getText: function () {

        var node = this.getInputNode();
        return (node ? (typeof node.value == "undefined" ? (this.getValue() ? node.innerHTML : "") : node.value) : "");

    },
    getValidateTextValue: function () {
        var text = this.getText();
        if (text != "") {
            return this.isValidTextDate();
        }
        return true;
    }


}, webix.ui.text);


webix.ui.datafilter.countTableRows = webix.extend({
    refresh: function (master, node, value) {
        node.firstChild.innerHTML = master.count();
    }
}, webix.ui.datafilter.summColumn);
///////////////////////////////////////////////////
///     webix.editors.dateExtended 
////////////////////////////////////////////////
webix.editors.dateExtended = webix.extend({
    _value: null,
    _displayValue: "",
    _textControl: null,
    popupType: "calendar",
    focus: function () {
        this.getEditInputNode().focus();
    },
    _getLabel:function() {
        var text = this.config.header && this.config.header[0] ? this.config.header[0].text : this.config.editValue || this.config.label;
        return(text || "").toString().replace(/<[^>]*>/g, "");
    },
    render: function () {

        this._textControl = webix.ui({
            view: "text",
            pattern: { mask: "##.##.####", allow: /[0-9]/g }
        });
        var node = this._textControl.getNode();
        node.classList.add("webix_dt_editor");
        var suggest = this.config.suggest = this._create_suggest(this.config.suggest);
        if (suggest) {
            var popupBody = this.getPopup().getBody();
            popupBody.attachEvent("onDateClear", webix.bind(function (value) {
                this.setValue(value);
                this.focus();
            }, this));
            popupBody.attachEvent("onDateSelect", webix.bind(function (value) {
                this.setValue(value);
                this.focus();
            }, this));
            popupBody.attachEvent("onTodaySet", webix.bind(function (value) {
                this.setValue(value);
                this.focus();
            }, this));
        }
        webix._event(node.firstChild, "keyup", webix.bind(function(e) {
            this._prepareSingleValue();
        }, this));
        return node;
    },
    afterRender: function () {
        //удаляем стили webix-a
        this.getEditInputNode().style.width = null;
        this.getEditInputNode().style.height = null;
        this.node.getElementsByTagName('div')[0].style.width = null;
        this.node.getElementsByTagName('div')[0].style.height = null;
        this.showPopup();

    },
    _prepareSingleValue: function () {
        var value = this._textControl.getValue();
        var setText = this._textControl._pattern(value);
        var format = webix.Date.strToDate("%d.%m.%Y");
        var setDate = format(setText);
        this._value = null;
        if ((setDate instanceof Date) && this.isValidTextDate()) {
            this._value = setDate;
            if (value.length > 8) {
                this.setValue(setDate);
            }
        }
    },
    _create_suggest: function (config) {
        if (this.config.popup) {
            return this.config.popup.config.id;
        }
        else if (config) {

            if (typeof config == "string") return config;
            if (config.linkInput) return config._settings.id;

            var type = {
                view: "popup", width: 250, height: 250, padding: 0,
                body: { view: "calendar", icons: true, borderless: true }
            };
            return webix.ui(type).config.id;
        } else
            return this._shared_suggest(config);
    },
    _shared_suggest: function () {
        var e = webix.editors.dateExtended;
        return (e._suggest = e._suggest || this._create_suggest(true));
    },
    getPopup: function () {
        return webix.$$(this.config.suggest);
    },
    showPopup: function () {
        var popup = this.getPopup();
        var input = this.getInputNode();
        popup.show(input);
    },
    /// get value
    getValue: function () {
        return this._value;
    },
    isValidTextDate: function () {

        var stringDate = this.getEditInputNode().value;
        if (!stringDate) {
            return false;
        }
        if (stringDate.length < 10) {
            return false;
        }
        var input = stringDate.match(/\d+/g);
        var date = new Date(input[2], input[1] - 1, input[0]);
        return (date.getFullYear() === parseInt(input[2]))
            && (date.getMonth() == parseInt(input[1] - 1))
            && (date.getDate() === parseInt(input[0]));
    },
    _get_visible_text: function (value) {
        var format = webix.Date.dateToStr("%d.%m.%Y", false);
        return format(value);
    },
    setValue: function (value) {
        this._value = null;
        if (value instanceof Date) {
            this._value = value;
        }
        this._displayValue = this._get_visible_text(value);
        this._set_visible_text();

    },
    _set_visible_text: function () {
        var node = this.getEditInputNode();
        if (node.value == webix.undefined) {
            node.value = "";
        } else {
            node.value = this._displayValue || "";
        }
    },
    getEditInputNode: function () {
        return this.node.getElementsByTagName('input')[0];
    }

}, webix.editors.text);

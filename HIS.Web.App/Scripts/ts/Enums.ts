namespace HIS.Models.Layer.Models.Address {
	export const enum AddressObjectLevel {
		Null = 0,
		RFSubjects = 1,
		District = 2,
		Region = 3,
		InRegion = 35,
		City = 4,
		Settlement = 6,
		ElmStructure = 65,
		Street = 7,
		Stead = 75,
		House = 8
	}
	export const enum AddressSourceType {
		Indefined = 0,
		Fias = 1,
		Custom = 2
	}
}
namespace HIS.Models.Layer.Models.Entity {
	export const enum EntityTreeType {
		SoRoot = 257,
		SoElement = 258,
		SoGroup = 259
	}
}
namespace HIS.Models.Layer.Models.Markers {
	export const enum MarkerType {
		MtBoolean = 1,
		MtInteger = 2,
		MtDecimal = 3,
		MtString = 4,
		MtList = 5,
		MtMoney = 6,
		MtDateTime = 10,
		MtEntityRef = 11,
		MtAddress = 12,
		MtReference = 13
	}
}
namespace HIS.Models.Layer.Models.QueryBuilder {
	export const enum FilteringGroupOperator {
		And = 0,
		Or = 1
	}
	export const enum FiltersOperator {
		Equal = 0,
		NoEqual = 1,
		Less = 2,
		More = 3,
		LessEqual = 4,
		MoreEqual = 5,
		Like = 6
	}
	export const enum FrontEndDataType {
		Number = 0,
		String = 1,
		Boolean = 2,
		Date = 3
	}
	export const enum SortingOrderModel {
		Asc = 0,
		Desc = 1
	}
}


﻿import Events = require("./Events");

export interface IEventSubscriber {
    subscribe(subscription: Events.ISubscription);
    unsubscribe(subscription: Events.ISubscription);
    destroy();
}

export abstract class EventSubscriberBase implements IEventSubscriber {

    private _subscribers: Events.ISubscription[] = [];

    subscribe(subscription: Events.ISubscription) {
        this._subscribers.push(subscription);
    }

    unsubscribe(subscription: Events.ISubscription) {
        const idx = this._subscribers.indexOf(subscription);
        if (idx >= 0) {
            this._subscribers.slice(idx, 1);
        }
        subscription.unsubscribe();
    }

    destroy() {
        this._subscribers.forEach(subscriber => {
            subscriber.unsubscribe();
        });
    }


}

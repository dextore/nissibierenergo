﻿//export enum EventType {
//    defaultEvent = "default_event",
//    incomingMessageEvent = "incoming_message_event",
//}

export interface ICallback<ArgType> {
    (arg: ArgType, context?: any);
}

export interface ISpecifiedCallback<ArgType> {
    (subscriber: any, arg: ArgType, context?: any);
}

export interface ICallbackDesc<ArgType> {
    callback: ICallback<ArgType>;
    subscriber: any;
}

/** Базовый интерфейс подписки на событие. Минимальная функциональность. Можем просто отписаться и все. */
export interface ISubscription {
    unsubscribe: { (): void };
}

/** Типизированная версия. Включает ссылки на событие и callback */
export interface ITypedSubscription<ArgType> extends ISubscription {
    callback: ICallback<ArgType>;
    event: IEvent<ArgType>;
}

export interface IEvent<ArgType> {
    subscribe(callback: ICallback<ArgType>, subscriber: any): ITypedSubscription<ArgType>;
    unsubscribe(callback: ICallback<ArgType>): void;
    trigger: ICallback<ArgType>;
    //triggerToSpecified: ISpecifiedCallback<ArgType>;
    //triggerWithoutSpecified: ISpecifiedCallback<ArgType>;
}

export class Event<ArgType> implements IEvent<ArgType>
{
    private _сallbacks: ICallbackDesc<ArgType>[] = [];

    /** Подписаться на событие
    * @param {ICallback<ArgType>} callback Callback, который будет вызван при вызове функции
    * @param {any} subscriber Контекст, в котором должен быть вызван callback
    * @returns {ITypedSubscription<ArgType>} Объект типизированной подписки
    */
    public subscribe(callback: ICallback<ArgType>, subscriber: any): ITypedSubscription<ArgType> {
        var that = this;

        var res: ITypedSubscription<ArgType> = {
            callback: callback,
            event: that,
            unsubscribe() { that.unsubscribe(callback); }
        };

        this._сallbacks.push({ callback: callback, subscriber: subscriber });

        return res;
    }

    public unsubscribe(callback: ICallback<ArgType>): void {
        //var filteredList: ICallbackDesc<ArgType>[] = [];

        this._сallbacks = this._сallbacks.filter((item) => {
            return item.callback !== callback;
        });

        //for (var i = 0; i < eventCallbacks.length; i++) {
        //    if (this.eventCallbacks[i].Callback !== callback) {
        //        filteredList.push(this._сallbacks[i]);
        //    }
        //}

        //this._сallbacks = filteredList;
    }

    public trigger: ICallback<ArgType> = (arg: ArgType, context?: any) => {

        this._сallbacks.forEach((item) => {
            item.callback.apply(item.subscriber, [arg, context]);
        });
        //for (var i = 0; i < callbacks.length; i++) {
        //    callbacks[i].Callback.apply(callbacks[i].Subscriber, [arg, context]);
        //}
    }

    private cbFunc(callback: ICallback<ArgType>, subscriber: any, arg: ArgType, context?: any) {
        callback.apply(subscriber, [arg, context]);
    }

    //public triggerToSpecified: ISpecifiedCallback<ArgType> = (subscriber: IncomingMessageHandler.IIncomingMessageHandler, arg: ArgType, context?: any) => {

    //    this._сallbacks.filter(c => c.subscriber.MessageHandlerId === subscriber.MessageHandlerId)
    //                   .forEach((item) => {
    //        setTimeout(this.cbFunc, 0, item.callback, item.subscriber, arg, context);
    //    });

    //    //var callbacks: ICallbackDesc<ArgType>[] = this.Callbacks.filter(c => c.Subscriber.MessageHandlerId === subscriber.MessageHandlerId );
    //    //for (var i = 0; i < callbacks.length; i++) {
    //    //    //callbacks[i].Callback.apply(callbacks[i].Subscriber, [arg, context]);
    //    //    setTimeout(this.cbFunc, 0, callbacks[i].Callback.apply(callbacks[i].Subscriber, [arg, context])));
    //    //}
    //}    

    //public triggerWithoutSpecified: ISpecifiedCallback<ArgType> = function (subscriber: IncomingMessageHandler.IIncomingMessageHandler, arg: ArgType, context?: any) {

    //    this._сallbacks.filter(c => c.subscriber.MessageHandlerId !== subscriber.MessageHandlerId)
    //                   .forEach((item) => {
    //        setTimeout(this.cbFunc, 0, item.callback, item.subscriber, arg, context);
    //    });

    //    //const callbacks: ICallbackDesc<ArgType>[] = this.Callbacks.filter(c => c.Subscriber.MessageHandlerId !== subscriber.MessageHandlerId);
    //    //for (let i = 0; i < callbacks.length; i++) {
    //    //    //callbacks[i].Callback.apply(callbacks[i].Subscriber, [arg, context]);
    //    //    setTimeout(this.cbFunc, 0, this.cbFunc( callbacks[i].Callback.apply(callbacks[i].Subscriber, [arg, context])));            
    //    //}
    //}
}

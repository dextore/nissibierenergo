﻿//
// Типы поиска
//
export enum SystemSearchType {
    CONTRACT_CODE = 0x0101,
    NAME = 0x0102,
    INN = 0x0103,
    ADDRESS = 0x0104
}
﻿export class TemplateHelper {
    static DatatableColumnBooleanTemplate() {
        return (obj, common, value) => {
            if (!value)
                return "<div class='webix_checkbox notchecked'> Нет </div>";
            else
                return "<div class='webix_checkbox checked'> Да </div>";
        };
    }
}
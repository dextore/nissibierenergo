﻿import BAT = require("../Common/BATypes");
import MainTabsView = require("../MainView/IMainTabsView");

export class ModuleHelper {

    private static modulesId: { [mvcAliase: string]: string } = {}; 

    static init(menuItems: MainMenu.IMenuItemModel[]) {
        ModuleHelper.loadItems(menuItems);
    }

    static aliasModuleExists(mvcAlias: string): boolean {
        return !!ModuleHelper.modulesId[mvcAlias];
    }

    static showModuleByAlias(mvcAlias: string, mainTabs: MainTabsView.IMainTabbarView) {
        const moduleId = ModuleHelper.modulesId[mvcAlias];
        if (!moduleId)
            return;

        mainTabs.showModule(moduleId);
    }

    private static loadItems(menuItems: MainMenu.IMenuItemModel[]) {
        menuItems.forEach(menuItem => {
            if (menuItem.mvcAliases && menuItem.mvcAliases.length) {
                menuItem.mvcAliases.forEach(alias => {
                    ModuleHelper.modulesId[alias] = (menuItem.moduleName) ? menuItem.moduleName : menuItem.id.toString();
                })
            }
            if (menuItem.items && menuItem.items.length > 0)
                ModuleHelper.loadItems(menuItem.items);
        });
    }

}

﻿export function isInnValid(inn: string): boolean {
    var result: boolean = false;
    let innLength: number = inn.length;
    if (innLength === 0) return false;

    var reg = "[0-9]";
    var checkParams = new InnParams();

    if ((inn === "0000000000")
        || (inn === "000000000000"))
            return result;

    if ((innLength >= 10) && (innLength <= 12) && (inn.match(reg))) {
        let sDgt10 = 0;
        let sDgt11 = 0;
        let sDgt12 = 0;
        let tempInn = inn + "00";
        
        checkParams.parametrs.forEach(checkParam => {
            let digitString = tempInn.substr(checkParam.pos, 1);
            let digit = Number(digitString);
            sDgt10 += digit * checkParam.k10;
            sDgt11 += digit * checkParam.k11;
            sDgt12 += digit * checkParam.k12;
        });

        sDgt10 = sDgt10 % 11 % 10;
        sDgt11 = sDgt11 % 11 % 10;
        sDgt12 = sDgt12 % 11 % 10;

        if ((innLength == 10) && (inn === (inn.substring(0, innLength - 1) + sDgt10.toString()))) {
            result = true;
        }

        if ((innLength == 12) && (inn === (inn.substring(0, innLength - 2) + sDgt11.toString() + sDgt12.toString()))) {
            result = true;
        }
    }

    return result; 
}

export function onlyNumberValues(id, event) {
    const numberStr = '1234567890';
    if (id === 8      //  backspace
        || id === 9   // tab
        || id === 46  // del
        || id === 39  // arrow
        || id === 38  // arrow
        || id === 37) // arrow
        return true;
    if (numberStr.indexOf(String.fromCharCode(id)) < 0)
        return false;
}

class InnParams {
    parametrs: InnParam[];

    constructor (){
        this.parametrs = this._inizilaize();
    }

    private _inizilaize(): InnParam[] {
        return [
            new InnParam(0, 2, 7, 3),
            new InnParam(1, 4, 2, 7),
            new InnParam(2, 10, 4, 2),
            new InnParam(3, 3, 10, 4),
            new InnParam(4, 5, 3, 10),
            new InnParam(5, 9, 5, 3),
            new InnParam(6, 4, 9, 5),
            new InnParam(7, 6, 4, 9),
            new InnParam(8, 8, 6, 4),
            new InnParam(9, 0, 8, 6),
            new InnParam(10, 0, 0, 8)
        ];
    }
}

class InnParam {
    pos: number;
    k10: number;
    k11: number;
    k12: number;

    constructor(pos: number, k10: number, k11: number, k12: number) {
        this.pos = pos;
        this.k10 = k10;
        this.k11 = k11;
        this.k12 = k12;
    }
}


export class ValidateRules {
    static bankAccount(value: string) {
        return !!value && value.length === 20 && /^\d+$/.test(value);
    }
}

﻿export interface IControlInfo {
    name: string;
    labelWidth: number;
}
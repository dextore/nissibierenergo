﻿import Api = require("../Api/ApiExporter");
import Catalogs = require("../Api/ApiCatalogs");

export function initData(): Promise<boolean> {

    const promise = webix.promise.defer();

    Api.getApiCommons().getEntitiesInfo()
        .then(data => {
            info = new SystemEntitityType(data);
            (promise as any).resolve(true);
        }).catch(() => {
            (promise as any).reject(false);
        });

    Catalogs.ApiCatalogs.init();

    return promise;
}

export interface ISystemEntitityTypes {
    getByAliase(alias: string): SystemEntityTypeInfo;
    getByBATypeId(baTypeId: number): SystemEntityTypeInfo;
    entitiesInfo: SystemEntityTypeInfo[];
    getByParent(baTypeId: number): SystemEntityTypeInfo[];
}

export function entityTypes(): ISystemEntitityTypes {
    return info;
}

class SystemEntitityType {
    private _entities: { [id: string]: SystemEntityTypeInfo };
    private _entitiesById: { [id: string]: SystemEntityTypeInfo };

    get entitiesInfo(): SystemEntityTypeInfo[] {
        return Object.keys(this._entities).map(i => this._entities[i]);
    }

    constructor(entities: Entities.IEntityInfoModel[]) {
        this._entities = {};
        this._entitiesById = {};
        entities.forEach(item => {
            this._entities[item.mvcAlias] = new SystemEntityTypeInfo(item);
            this._entitiesById[item.baTypeId.toString()] = this._entities[item.mvcAlias];
        });
    }

    getByAliase(alias: string) {
        return this._entities[alias];
    }

    getByBATypeId(baTypeId: number) {
        return this._entitiesById[baTypeId.toString()];
    }

    getByParent(baTypeId: number): SystemEntityTypeInfo[] {
        const result: SystemEntityTypeInfo[] = [];

        getChild(baTypeId, this.entitiesInfo, result);

        return result;

        function getChild(baTypeId: number, entitiesInfo: SystemEntityTypeInfo[], result: SystemEntityTypeInfo[]) {
            entitiesInfo.forEach(item => {
                if (item.entityInfo.parentBATypeId === baTypeId) {
                    result.push(item);
                    getChild(item.entityInfo.baTypeId, entitiesInfo, result);
                }
            });
            return;
        }
    }
}


export class SystemEntityTypeInfo {
    private _markersInfo: { [id: string]: Markers.IMarkerInfoModel };
    private _markersInfoArray: Markers.IMarkerInfoModel[];

    get entityInfo() {
        return this._entity;
    } 

    get markersInfo() {
        return this._markersInfo;
    }

    // TODO нужно будет убрать
    get markersInfoArray() {
        return this._entity.markersInfo;
    }

    constructor(private readonly _entity: Entities.IEntityInfoModel) {
        this._markersInfo = {};
        _entity.markersInfo.forEach(item => {
            this._markersInfo[item.name] = item;
        });   
    }
}

let info: ISystemEntitityTypes = null;
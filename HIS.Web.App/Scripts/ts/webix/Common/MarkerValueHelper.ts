﻿export function checkDataMarker(markerValues: Markers.IMarkerValueModel[]) {
    if (!!markerValues && markerValues.length) {
        markerValues.filter(item => {
            return item.markerType === HIS.Models.Layer.Models.Markers.MarkerType.MtDateTime;
        }).forEach(item => {
            if (Boolean(item.value))
                item.value = new Date(item.value) as any;
        });
    }
}

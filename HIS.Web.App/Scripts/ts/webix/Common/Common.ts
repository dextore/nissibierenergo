﻿//
// Функция асинхронного запроса GET к UR
//
export function get(endpoint: string, options: any = null, cbError: (error: any) => any = null): Promise<any> {
    const promise = webix.promise.defer();
    webix.ajax()
        .headers({ "Content-type": "application/json", "Accept": "*/*" })
        .get(encodeURI(endpoint), options)
        .then(result => {
            const model = result.json();
            const data = checkDateFields(model);
            (promise as any).resolve(data);
        }).catch(error => {
            if (cbError) {
                cbError(error);
                (promise as any).reject(error);
            } else {
                showError(error, (endpoint) as any);
                (promise as any).reject(error);
            }
        });
    return promise;
}

//
// Функция асинхронного запроса POST к URL
//
export function post(endpoint: string, data: any, showErrorMessage: boolean = true) { //cbError: (error: any) => any = null) {
    const promise = webix.promise.defer();
    webix.ajax()
        .headers({ "Content-type": "application/json", "Accept": "*/*" })
        .post(endpoint, JSON.stringify(data))
        .then(result => {
            const model = result.json();
            const resultModel = checkDateFields(model);
            (promise as any).resolve(resultModel);
        }).catch(error => {
            if (showErrorMessage) {
                showError(error, (endpoint) as any);
            }
            (promise as any).reject(error);
        });
    return promise;
} 

export function propertyValue(obj, propertyName, cbGenStr: (propertyName: string, propertyValue: any) => string = null) {
    let val: string = "";
    if (obj.hasOwnProperty(propertyName)) {
        if (cbGenStr) {
            return cbGenStr(propertyName, obj[propertyName]);
        } else {
            val = `${propertyName}: ${obj[propertyName]}`;
        }
    }
    return val;
}

// Сонвертация дат в модели.
export function checkDateFields(data: any) {
    const jsonData = JSON.stringify(data);
    return JSON.parse(jsonData, reviver);
}

//проверка на не пустой объект
export function isEmptyObject(obj: object): boolean {

    if (obj == null || obj == undefined) {
        return true;
    }
    if (Object.keys(obj).length === 0) {
        return true;
    }
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
}

//
// Функция отображения ошибки
//
export function showError(error, endpoint = null) {
    console.error(error);

    if (error.status === 500) {
        const response = JSON.parse(error.response);

        switch (response.ExceptionType) {
            case "System.Data.SqlClient.SqlException":
            case "HIS.Business.Layer.Utils.QueryBuilder.ExceptionsDebugException":
            case "HIS.Business.Layer.Exceptions.DataValidationException":
                confirmError(`${response.Message} - ${response.ExceptionMessage}`);
                return;
        }
        if (response.ExceptionMessage) {
            confirmError(`${response.ExceptionMessage}`);
            return;
        }
        if (response.Message) {
            confirmError(`${response.Message}`);
            return;
        }
        confirmError(`${response.responseText}`);
        return;
    }

    confirmError(`${error.status} - ${error.statusText} - ${error.responseURL}`);

    //let message = (!endpoint) ? "" : endpoint + " ";

    //message += propertyValue(error, "status");
    //message += propertyValue(error, "statusText");

    //if (error.hasOwnProperty("responseJSON")) {
    //    const json = error.responseJSON;
    //    message += propertyValue(json, "ExceptionType");
    //    message += propertyValue(json, "ExceptionMessage");
    //    message += propertyValue(json, "Message");
    //} else {
    //    message += propertyValue(error, "Message");
    //    message += propertyValue(error, "message");
    //}

    //webix.message(message, "error", 10000);
}

function messageError(message: string) {
    webix.message({
        type: "error",
        text: message,
        expire: -1
    });
}

function confirmError(message: string) {
    webix.alert({
        title: "<b>Ошибка!</b>",
        type: "alert-error",
        text: `${message}`,
        ok: "Закрыть"
    });    
}

const dateFields: { [id: string]: any } = {};
dateFields["startDate"] = 1;
dateFields["oldStartDate"] = 1;
dateFields["endDate"] = 1;
dateFields["oldEndDate"] = 1;
dateFields["includeDate"] = 1;
dateFields["birthDate"] = 1;
dateFields["operationDate"] = 1;
dateFields["checkDate"] = 1;
dateFields["releaseDate"] = 1;
dateFields["transDate"] = 1;
dateFields["docDate"] = 1;
dateFields["createdDate"] = 1;


function reviver(key: any, value: any) {
    return (dateFields[key]) ? (!value ? null : new Date(value)) : value;
    //return (dateFields[key]) ? new Date(value) : value;
} 




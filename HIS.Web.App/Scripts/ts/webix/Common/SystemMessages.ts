﻿import EV = require("./Events");



export enum ESystemEventType {
    TREE_SELECTED_CHANGE = "tree_selected_schange",
}

export interface IEventArgs {
    eventType: ESystemEventType;
    args: any; 
}

export class SystemMessages {

    private static _events: { [id: string]: EV.IEvent<any> } = {};

    static subscribe(eventType: ESystemEventType, callback: EV.ICallback<any>, subscriber: any): EV.ITypedSubscription<any> {
        this._events[eventType] = this._events[eventType] || new EV.Event<any>();
        return this._events[eventType].subscribe(callback, subscriber);
    }

    static unsubscribe(callback: EV.ICallback<IEventArgs>): void {
        const events = this._events;
        for (let id in events) {
            return this._events[id].unsubscribe(callback);
        }
    }

    static trigger(eventType: ESystemEventType, eventArgs: any, context?: any): void {
        this._events[eventType] && this._events[eventType].trigger(eventArgs, context);
    }

}
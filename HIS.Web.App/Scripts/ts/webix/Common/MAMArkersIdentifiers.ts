﻿export const enum MAMArkersIdentifiers {
    Name = 1,
    FullName = 2,
    Address = 5,
    CompanyArea = 6,
    FactAddress = 16,
    ContactList = 20,
    PhoneList = 21,
    EmailList = 22,
    PhoneType = 23,
    ContactType = 24,
    MailConfirmType = 25,
    MailType = 26,
    PostAddress = 122,
    OrganisationLegalForm = 131,
    BankAccount = 132
}
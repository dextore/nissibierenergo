﻿import Base = require("./DialogBase");
import Btn = require("../Components/Buttons/ButtonsExporter");
import MB = require("../Components/Markers/MarkerControlBase");


export interface IDialogForm extends Base.IDialog {
    model: Markers.IMarkerValueModel[];
}

export abstract class DialogFormBase extends Base.DialogBase implements IDialogForm {

    private _dlgButons: Btn.IButton[] = [];
    private _model: { [id: string]: any } = {};
    private _markersControls: { [id: string]: MB.IMarkerControl } = {};

    get formId() { return `${this.viewName}-form-id`; }

    get fieldsInfo() {
        return this._fieldsInfo;
    }

    get markersControls() {
        return this._markersControls;
    }

    protected constructor(viewName: string, private readonly _fieldsInfo: { [id: string]: Markers.IMarkerInfoModel }, private readonly _values: Markers.IMarkerValueModel[]) {
        super(viewName);
    }

    protected initFormButtons(dlgButons: Btn.IButton[]) {
        dlgButons.push({} as any);
        dlgButons.push(Btn.getOkButton(this.viewName, () => { this.okClose(); }));
        dlgButons.push(Btn.getCancelButton(this.viewName, () => { this.cancelClose(); }));
    }

    protected abstract getFormElements(markersControls: { [id: string]: MB.IMarkerControl }, fieldsInfo: { [id: string]: Markers.IMarkerInfoModel
    }): any[];

    private getDlgButtonsConfig() {
        this.initFormButtons(this._dlgButons);

        const controls = this._dlgButons.map(item => {
            if (typeof item.init === "function")
                return item.init();
            return item;
        });

        return { view: "toolbar", height: 40, cols: controls };
    }

    bindModel() {
        this._values.forEach(item => {
            const markersControls = this._markersControls;
            for (let idx in markersControls) {
                if (markersControls.hasOwnProperty(idx)) {
                    if (this._markersControls[idx].markerInfo.id === item.markerId) {
                        this._markersControls[idx].setInitValue(item, this._model);
                    }
                }
            }
        });
        ($$(this.formId) as webix.ui.form).setValues(this._model);
        ($$(this.formId) as webix.ui.form).clearValidation();
    }

    get model() {
        return this.getFormData();
    }

    protected getFormData() {
        ($$(this.formId) as webix.ui.form).getValues(this._model);
        const result: Markers.IMarkerValueModel[] = [];
        const markersControls = this._markersControls;
        for (let idx in markersControls) {
            if (markersControls.hasOwnProperty(idx)) {
                const marker = this._markersControls[idx].getValue(this._model);
                result.push(marker);
            }
        }
        return result;
    }

    okClose(): void {
        if (!($$(this.formId) as webix.ui.form).validate()) {
            return;
        }
        super.okClose();
    }

    contentConfig() {
        return {
            rows: [
                {
                    view: "form",
                    id: this.formId,
                    autoheight: true,
                    elements: this.getFormElements(this._markersControls, this._fieldsInfo)
                },
                this.getDlgButtonsConfig()
            ]
        }
    }

    protected windowConfig() {
        return {
            height: 700,
        };
    }

}

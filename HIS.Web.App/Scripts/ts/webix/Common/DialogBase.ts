﻿import Base = require("./EventSubscriberBase");
import Tab = require("../MainView/Tabs/MainTabBase");


export interface IDialog extends Base.IEventSubscriber {
    showModal(callBack?: ()=>void); 
    showModalContent(module: Tab.IMainTab, callBack?: () => void); 
    showOnSlider(module: Tab.IMainTab, callBack?: () => void);
    close();
    viewId: string;
}

export enum DlgMode {
    Modal = 0,
    Content = 1,
    Slider = 2
}

export abstract class DialogBase extends Base.EventSubscriberBase implements IDialog {

    private _hPadding = 0;
    private _vPadding = 0;
    private _tabItem: Tab.IMainTab;
    private _callBack?: () => void;
    private _dialogMode: DlgMode;
    private _posStore = { left: 0, top: 0 };

    protected fullscreen: boolean = false;

    protected get dialogMode() {
        return this._dialogMode;
    }

    protected get tabItem() {
        return this._tabItem;
    }

    protected get hPadding() {
        return this._hPadding;
    }

    protected set hPadding(value: number) {
        this._hPadding = value;
    }

    protected get vPadding() {
        return this._vPadding;
    }

    protected set vPadding(value: number) {
        this._vPadding = value;
    }

    get viewId() {
        return `${this._viewName}-view-id`;
    }

    protected get viewName() {
        return this._viewName;
    }

    showModal(callBack?: () => void) {
        this._dialogMode = DlgMode.Modal;
        this.show(callBack);
    }

    showModalContent(module: Tab.IMainTab, callBack?: () => void) {
        this._dialogMode = DlgMode.Content;
        this._tabItem = module;
        this._tabItem.addContentWindow(this.windowId);
        //$$(this._tabItem.viewId).disable();
        this.show(callBack);
    }

    showOnSlider(module: Tab.IMainTab, callBack?: () => void) {
        this._dialogMode = DlgMode.Slider;
        this._callBack = callBack;
        this._tabItem = module;
        ($$(module.viewId) as webix.ui.multiview).addView(this.init());
        $$(this.viewId).show();
        if (this.fullscreen)
            $$(this.viewId).adjust();
        this.bindModel();
    }

    close() {
        if (this.window) {
            this.window.close();
            return;
        }
    }

    destroy(): void {
        if (this._tabItem) {
            this._tabItem.closeSlider(this);
        }
        super.destroy();
    }

    protected okClose() {
        if (this._callBack) {
            this._callBack();
            return;
        }
        this.close();
    }

    protected cancelClose() {
        this.close();
    }

    protected get windowId() {
        return `${this._viewName}-window-id`;
    }

    protected get window() {
        return $$(this.windowId) as webix.ui.window;
    }

    protected constructor(private readonly _viewName: string, private readonly _maxMinButton: boolean = false) {
        super();
    }

    private show(callBack?: () => void) {
        this._callBack = callBack;

        let width = 0;
        let height = 0;
        if (this.vPadding > 0 || this.hPadding) {
            const rect = (this.dialogMode === DlgMode.Content)
                ? $$(this._tabItem.viewId).getNode().getBoundingClientRect()
                : document.body.getBoundingClientRect();
            width = rect.width - (this._hPadding * 2);
            height = rect.height - (this._vPadding * 2);
        }
        this.createWindow(width, height);

        (this.window as any).show();
        this.bindModel();
        this.afterShowDialog();
    }

    private toggleFullScreen() {
        const win = $$(this.windowId) as webix.ui.window;
        let currentMode = win.config.fullscreen;

        if (!currentMode) {
            this._posStore.left = win.config.left;
            this._posStore.top = win.config.top;
            win.setPosition(0, 0);
        } else if (this._posStore.top)
            win.setPosition(this._posStore.left, this._posStore.top);

        win.config.resize = currentMode;
        win.config.move = currentMode;
        win.config.fullscreen = !currentMode;
        win.resize();
        // switch between 'maximize' and 'restore' icons
        win.getHead().showBatch(currentMode ? 1 : 2);
    }

    protected afterShowDialog() {
    }

    private createWindow(width: number = null, height: number = null) {
        try {
            webix.ui(this.config(width, height));
        } catch (err) {
            console.log(err);
        }

        this.ready();
        const win = $$(this.windowId) as webix.ui.window;
        this._posStore.top = win.config.top;
        this._posStore.left = win.config.left;
    }

    protected rendered() {};
    protected ready() {};

    private config(width: number = null, height: number = null) {
        const self = this;

        const config = self.windowConfig();
        const internalConfig = internalWindowConfig();

        return webix.extend(internalConfig, config, true);

        function internalWindowConfig() {
            return {
                view: "window",
                id: self.windowId,
                position: "center",
                head: {
                    view: "toolbar",
                    margin: -4,
                    visibleBatch: 1,
                    cols: self.getHeaderButtons()
                },
                modal: !self._tabItem,
                //fullscreen: (self.dialogMode === DlgMode.Modal) ? self.fullscreen : false,
                height: height || 500,
                width: width || 600,
                move: true,
                resize: true,
                body: {
                    id: self.viewId,
                    autoheight: true,
                    cols: [self.contentConfig()]
                },
                on: {
                    onDestruct: () => {
                        if (self._tabItem) {
                            self._tabItem.removeContentWindow(self.windowId);
                        }
                        self.destroy();
                    },
                    //onHide: () => {
                    //    //self.cancelClose();
                    //},
                    //onKeyPress: (code, e) => {
                    //    webix.message(code);
                    //}
                }
            }
        }
    }

    private getHeaderButtons() {
        return this._maxMinButton
            ? [
                { view: "label", label: this.headerLabel() },
                { view: "icon", icon: "window-maximize", click: () => { this.toggleFullScreen() }, batch: 1 },
                { view: "icon", icon: "window-restore", click: () => { this.toggleFullScreen() }, batch: 2 },
                { view: "icon", icon: "times", click: () => { this.cancelClose(); } }
            ]
            : [
                { view: "label", label: this.headerLabel() },
                { view: "icon", icon: "times", click: () => { this.cancelClose(); } }
            ];
    }

    protected abstract headerLabel(): string;
    protected abstract contentConfig();

    protected bindModel() { };

    protected windowConfig() {
        return {};
    }

    private init(): any {
        const self = this;
        return {
            id: this.viewId,
            cols: this.fullscreen ? [self.contentConfig()] : [{}, { rows: [{}, self.contentConfig(), {}]},{}],
            on: {
                onDestruct: () => {
                    //self.destroy();
                    //self._tabItem.closeSlider(this);
                }
            }
        }
    }

}

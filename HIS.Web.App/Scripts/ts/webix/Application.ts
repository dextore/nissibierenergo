﻿import Api = require("./Api/ApiExporter");
import Main = require("./MainView/MainView");
import Login = require("./LoginForm/LoginForm");

webix.i18n.setLocale("ru-RU");
webix.UIManager.removeHotKey("esc");

class HisApp {

    init() {
        webix.ready(() => {
            Api.getApiAuthentication()
                .getUserInfo()
                .then(userInfo => {
                    if (userInfo !== null) {
                        webix.ui.fullScreen();
                        this.showMainForm();
                        return;
                    }
                    this.showLoginForm();
                });
        });
    }

    private destroyMainForm() {
        this.showLoginForm();
    }

    private showLoginForm() {
        const loginForm = Login.getLoginForm();
        loginForm.show().then(() => {
            this.showMainForm();
        });
    }

    private showMainForm() {
        const mainView = Main.getMainForm();
        const layout = webix.ui(mainView.init());

        (layout as webix.ui.layout).attachEvent("onDestruct", () => { this.destroyMainForm() });
        mainView.afterRender();
    }

}

const hisApp = new HisApp();
hisApp.init();

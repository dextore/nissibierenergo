﻿import Common = require("../Common/CommonExporter");

export interface IApiAuthentication {
    getDomainUserInfo(domainUserName: string): Promise<Auth.IDomainUserInfoModel>;
    getUserInfo(): Promise<Auth.IUserInfoModel>;
    formLogin(userInfo: Auth.ILoginModel): Promise<Auth.IAuthResultModel>;
    domainLogin(dbalias: string): Promise<Auth.IAuthResultModel>;
    logoff(): Promise<boolean>;
    checkRights(rights: string): Promise<boolean>;
    checkRoles(roles: string): Promise<boolean>;
    checkUsers(users: string): Promise<boolean>;
    getDataBaseAreas(): Promise<Auth.IAreaDataBaseModel[]>;
}

export function getApiAuthentication(): IApiAuthentication {
    return apiAuthentication;
}

class ApiAuthentication implements IApiAuthentication {

    getDomainUserInfo(domainUserName: string): Promise<Auth.IDomainUserInfoModel> {
        const endpoint = `api/Auth/GetDomainUserInfo`;
        return Common.get(endpoint, { domainUser: domainUserName });
    }

    getUserInfo(): Promise<Auth.IUserInfoModel> {
        const endpoint = `api/Auth/GetUserInfo`;
        return Common.get(endpoint, null);
    }

    formLogin(userInfo: Auth.ILoginModel): Promise<Auth.IAuthResultModel> {
        const promise = webix.promise.defer();
        const endpoint = `api/Auth/FormLogin`;
        Common.post(endpoint, (userInfo as any), false)
            .then(() => {
                (promise as any).resolve({ result: true, error: null });
            }).catch((error) => {
                const errMessage = (error.status === 401) ? "Неверное имя пользователи или пароль." : this.parseError(error);
                (promise as any).reject({ result: false, error: errMessage });
            });
        return promise;
    }

    domainLogin(dbalias: string): Promise<Auth.IAuthResultModel> {
        const promise = webix.promise.defer();
        const endpoint = `Auth/ExternalLogin`;
        Common.post(endpoint, { dbalias: dbalias }, false)
            .then(() => {
                    (promise as any).resolve({ result: true, error: null });
            }).catch((error) => {
                const errMessage = this.parseError(error);
                (promise as any).reject({ result: false, error: errMessage });
            });
        return promise;
    }

    private parseError(error): string {
        let errMessage: string = "";

        if ("responseJSON" in error) {
            const json = error.responseJSON;
            errMessage += Common.propertyValue(json, "ExceptionType", genErrStr);
            errMessage += Common.propertyValue(json, "ExceptionMessage", genErrStr);
            errMessage += Common.propertyValue(json, "Message", genErrStr);
        } else if ("response" in error) {
            const json = JSON.parse(error.response);
            errMessage += Common.propertyValue(json, "Message", genErrStr);
        } else {
            errMessage += Common.propertyValue(error, "Message", genErrStr);
            errMessage += Common.propertyValue(error, "message", genErrStr);
        }

        if (errMessage === "") {
            errMessage = "Неопределенная ошибка аутентификации";
        }
        return errMessage;

        function genErrStr(propertyName: string, propertyValue: any): string {
            return `${propertyValue}`;
        }
    }

    logoff(): Promise<boolean> {
        const promise = webix.promise.defer();
        const endpoint = `api/Auth/Logoff`;

        Common.post(endpoint, null)
            .then((data) => {
                (promise as any).resolve(data);
            })
            .catch((error) => {
                (promise as any).reject(error);
            });

        return promise;
    }

    checkRights(rights: string): Promise<boolean> {
        const promise = webix.promise.defer();
        var endpoint = `api/Auth/CheckRights`;

        Common.get(endpoint, { rights: rights })
          .then((data: boolean) => {
            (promise as any).resolve(data);
          })
          .catch((error) => {
              (promise as any).reject(error);
        });
        return promise;    
    }

    checkRoles(roles: string): Promise<boolean> {
        const promise = webix.promise.defer();
        var endpoint = `api/Auth/CheckRoles`;

        Common.get(endpoint, { roles: roles })
            .then((data: boolean) => {
                (promise as any).resolve(data);
            })
            .catch((error) => {
                (promise as any).reject(error);
            });
        return promise; 
    }

    checkUsers(users: string): Promise<boolean> {
        const promise = webix.promise.defer();
        var endpoint = `api/Auth/CheckUsers`;

        Common.get(endpoint, { users: users })
            .then((data: boolean) => {
                (promise as any).resolve(data);
            })
            .catch((error) => {
                (promise as any).reject(error);
            });
        return promise;
    }

    getDataBaseAreas(): Promise<Auth.IAreaDataBaseModel[]> {
        const endpoint = `api/Auth/GetDataBaseAreas`;
        return Common.get(endpoint);
    }
}

const apiAuthentication = new ApiAuthentication();
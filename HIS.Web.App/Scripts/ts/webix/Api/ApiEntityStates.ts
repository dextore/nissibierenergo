﻿import Common = require("../Common/CommonExporter");

export class ApiEntityStates {

    public static getStates(baTypeId: number): Promise<States.IEntityTypeStateModel[]> {
        const endpoint = `api/entitystates/getStates/${baTypeId}`;
        return Common.get(endpoint);
    }
}
﻿import Common = require("../Common/CommonExporter");
import Elm = require("./ApiElemens");

export function getApiObjects(): Elm.ApiElements {
    return apiObjects;
}

class ApiObjects implements Elm.ApiElements {

    get(): Promise<any> {
        const endpoint = "api/Objects/Get";
        return Common.get(endpoint);
   }

    getExt(): Promise<Elements.IElementExtModel> {
        const endpoint = "api/Objects/GetObjects";
        return Common.get(endpoint) as Promise<Elements.IElementExtModel>;
    }

    getItemExt(objectId: number): Promise<Elements.IElementExtModel> {
        const endpoint = "api/Objects/GetObject";
        const data = { objectId: objectId }; 
        return Common.get(endpoint, data) as Promise<Elements.IElementExtModel>;
    }

    getItem(id: number): Promise<any> {
        const endpoint = `api/Objects/GetItem?id=${id}`;
        return Common.get(endpoint);
    }

    save(model: Elements.IElementRetModel): Promise<any> {
        const endpoint = "api/Objects/Save";
        return Common.post(endpoint, model);
    }

    getBATypeId(): number {
        //TO DO переделать на запрос или константы!!!
        return 22;
    };
}

let apiObjects = new ApiObjects();
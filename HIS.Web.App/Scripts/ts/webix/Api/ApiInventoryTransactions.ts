﻿import Common = require("../Common/CommonExporter");

export class ApiInventoryTransactions {
    static get(baId: number): Promise<Warehouses.IInventoryTransactionModel[]> {
        const endpoint = `api/InventoryTransactions/${baId}`;
        return Common.get(endpoint) as Promise<Warehouses.IInventoryTransactionModel[]>;
    }
}

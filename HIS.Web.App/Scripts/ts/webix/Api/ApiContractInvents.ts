﻿import Common = require("../Common/CommonExporter");

export class ApiContractInvents {

    static get(model: ContractInvents.IContractInventsRequestModel): Promise<ContractInvents.IContractInventModel[]> {
        const endpoint = `api/contractinvents/get`;
        return Common.post(endpoint, model);
    }

    static getItem(baId: number): Promise<ContractInvents.IContractInventModel> {
        const endpoint = `api/contractinvents/getitem/${baId}`;
        return Common.get(endpoint);
    }
}

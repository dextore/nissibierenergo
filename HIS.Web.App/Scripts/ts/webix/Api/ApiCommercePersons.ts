﻿import Common = require("../Common/CommonExporter");

export interface IApiCommercePersons {
    getPerson(personId: number): Promise<Commerce.ICommercePersonModel>; 
    getOPFByParent(parentId: number): Promise<Commerce.ICommercePersonModel>;
    getAddresseList(baId: number): Promise<Markers.IMarkerValueModel[]>;
    update(model: Commerce.ICommercePersonUpdateModel): Promise<Commerce.ICommercePersonModel>;
    saveAddresList(model: Entity.IEntityModel): Promise<Markers.IMarkerValueModel[]>;
}

export function getApiCommercePersons(): IApiCommercePersons {
    return apiCommercePersons;
}

class ApiCommercePersons implements IApiCommercePersons {

    getPerson(personId: number): Promise<Commerce.ICommercePersonModel> {
        const endpoint = `api/commercePersons/GetPerson/${personId}`;
        return Common.get(endpoint);
    }

    getOPFByParent(parentId: number): Promise<Commerce.ICommercePersonModel> {
        const endpoint = `api/commercePersons/GetOPFByParent/${parentId}`;
        return Common.get(endpoint);
    }

    getAddresseList(baId: number): Promise<Markers.IMarkerValueModel[]> {
        const endpoint = `api/commercePersons/getAddresseList/${baId}`;
        return Common.get(endpoint);
    }

    update(model: Commerce.ICommercePersonUpdateModel): Promise<Commerce.ICommercePersonModel> {
        const endpoint = `api/commercePersons/Update`;
        return Common.post(endpoint, model);
    }

    saveAddresList(model: Entity.IEntityModel): Promise<Markers.IMarkerValueModel[]> {
        const endpoint = `api/commercePersons/saveAddresList`;
        return Common.post(endpoint, model);
    }
}

let apiCommercePersons = new ApiCommercePersons();

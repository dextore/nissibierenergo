﻿import Common = require("../Common/CommonExporter")


export interface IApiAddress
{
    addrSearchFull(findStr: string): Promise<Address.IAddresFullName[]>;

    getAddrFullByHouse(AOID: System.IGuid, HOUSEID: System.IGuid): Promise<Address.IAddresFullName>;
    getAddrFullByStead(AOID: System.IGuid, STEADID: System.IGuid): Promise<Address.IAddresFullName>;
    getAddrFullByAOID(AOID: System.IGuid): Promise<Address.IAddresFullName>;

    //simpleAddrSearch(findStr: string): Promise<FIAS.IFiasFindedAddress[]>;
    //simpleAddrByAOID(AOID: System.IGuid): Promise<FIAS.IFiasFindedAddress>;
    getAddressInfo(AOID: System.IGuid): Promise<Address.IAddressAOIdAOGuid>;
    getRFSubjects(options: Address.IRequestModel): Promise<Address.IAddressCatalog[]>;
    //getDistricts(options: Request.IFiasSelectModel): Promise<FIAS.IFiasDistrict[]>;
    getRegions(options: Address.IAddressRequestModel): Promise<Address.IAddressCatalog[]>;
    getCities(options: Address.IAddressRequestModel): Promise<Address.IAddressCatalog[]>;
    //getInRegions(AOGUID: string): Promise<FIAS.IFiasInRegion[]>;
    getSettlements(options: Address.IAddressRequestModel): Promise<Address.IAddressCatalog[]>;
    getElmStructures(options: Address.IAddressRequestModel): Promise<Address.IAddressCatalog[]>;
    getStreets(options: Address.IAddressRequestModel): Promise<Address.IAddressCatalog[]>;
    getFiasAddressObjetcList(options: Address.IAddressRequestModel): Promise<Address.IAddressCatalog[]>;
    //getAddTerritories(AOGUID: string): Promise<FIAS.IFiasAddTerritory[]>;
    //getAddTerritoryStreets(AOGUID: string): Promise<FIAS.IFiasAddTerritoryStreet[]>;
    //getAddressHouses(AOGUID: string): Promise<FIAS.IFiasAddressHouse[]>;
    getHouses(options: Address.IAddressRequestModel): Promise<Address.IAddressHouse[]>;
    getSteads(options: Address.IAddressRequestModel): Promise<Address.IAddressStead[]>;
    //getDetailAddrSearch(searchModel: FIAS.IFiasDetailSearch): Promise<FIAS.IFiasDetailAddress>;
    AddressUpdate(options: Address.IAddressUpdateModel): Promise<Address.IAddressUpdateResultModel>;
    AddressNameUpdate(options: Address.IAddressUpdateResultModel): Promise<Address.IAddressUpdateResultModel>;
    getFlatTypeList(): Promise<Address.IFlatTypeModel[]>;
    getHouse(options: Address.IHouseRequestModel): Promise<Address.IAddressHouse>;
    getStead(options: Address.ISteadRequestModel): Promise<Address.IAddressStead>;
    getSteadUpdateInfo(steadId: System.IGuid): Promise<Address.ISteadUpdateModel>;
    getHouseUpdateInfo(houseId: System.IGuid): Promise<Address.IHouseUpdateModel>;


    getAddressObjectCatalog(options: Address.IAddressObjectRequestModel): Promise<Address.IAddressCatalog>;
    getAddressList(options: Address.IAddressResultRequestModel): Promise<Address.IAddressFindResult>;
    getAddressObjectTypes(level: number): Promise<Address.IAddressObjectTypeModel[]>;
    getAddressObject(aoId: System.IGuid): Promise<Address.IAddressObjectModel>;
    getAddressEntity(baId: number): Promise<Address.IAddressEntityModel>;
    
    getEstateStatuses(): Promise<Address.IAddressEstateStatus[]>;
    getStructureStatuses(): Promise<Address.IAddressStructureStatus[]>;
    getAddressHierarchy(baId: number): Promise<Address.IAddressHierarchyResponse[]>;
    getAddressMappings(): Promise<Address.IAddressComparisonsDpModel[]>;

    addressObjectUpdate(options: Address.IAddressObjectUpdateModel): Promise<Address.IAddressObjectUpdateResultModel>;
    houseUpdate(options: Address.IHouseUpdateModel): Promise<Address.IHouseUpdateResultModel>;
    steadUpdate(options: Address.ISteadUpdateModel): Promise<Address.ISteadUpdateResultModel>;
    sddressSettle(options: Address.IAddressSettleRequestModel[]): Promise<boolean>;
    bindHouses(options: Address.IBindHousesModel): Promise<boolean>;
}

export function getApiAddress(): IApiAddress {
    return apiAddress;
}

class ApiAddress implements IApiAddress {
    addrSearchFull(findStr: string): Promise<Address.IAddresFullName[]> {
        const endpoint = `api/Address/addrSearchFull?findStr=${encodeURI(findStr)}`;
        return Common.post(endpoint, null) as Promise<Address.IAddresFullName[]>;
    }

    getAddrFullByHouse(AOID: System.IGuid, HOUSEID: string): Promise<Address.IAddresFullName> {
        const endpoint = `api/Address/getAddrFullByHouse?AOID=${AOID}&HOUSEID=${HOUSEID}`;
        return Common.post(endpoint, null) as Promise<Address.IAddresFullName>;
    }

    getAddrFullByStead(AOID: System.IGuid, STEADID: string): Promise<Address.IAddresFullName> {
        const endpoint = `api/Address/GetAddrFullByStead?AOID=${AOID}&STEADID=${STEADID}`;
        return Common.post(endpoint, null) as Promise<Address.IAddresFullName>;
    }

    getAddrFullByAOID(AOID: string): Promise<Address.IAddresFullName> {
        const endpoint = `api/Address/getAddrFullByAOID?AOID=${AOID}`;
        return Common.post(endpoint, null) as Promise<Address.IAddresFullName>;
    }

    //simpleAddrSearch(findStr: string): Promise<FIAS.IFiasFindedAddress[]> {
    //    const endpoint = `api/Address/SimpleAddrSearch?findStr=${findStr}`;
    //    return Common.post(endpoint, null) as Promise<FIAS.IFiasFindedAddress[]>;
    //}

    //simpleAddrByAOID(AOID: string): Promise<FIAS.IFiasFindedAddress> {
    //    const endpoint = `api/Address/SimpleAddrByAOID?AOID=${AOID}`;
    //    return Common.post(endpoint, null) as Promise<FIAS.IFiasFindedAddress>;
    //}

    getAddressInfo(AOID: string): Promise<Address.IAddressAOIdAOGuid> {
        const endpoint = `api/Address/GetAddressInfo?AOID=${AOID}`;
        return Common.post(endpoint, null) as Promise<Address.IAddressAOIdAOGuid>;
    }

    getRFSubjects(options: Address.IRequestModel): Promise<Address.IAddressCatalog[]> {
        const endpoint = `api/Address/GetRFSubjects`;
        return Common.post(endpoint, options) as Promise<Address.IAddressCatalog[]>;
    }

    //getDistricts(options: Request.IFiasSelectModel): Promise<FIAS.IFiasDistrict[]> {
    //    const endpoint = `api/Address/GetDistricts`;
    //    return Common.post(endpoint, options) as Promise<FIAS.IFiasDistrict[]>;
    //}

    getRegions(options: Address.IAddressRequestModel): Promise<Address.IAddressCatalog[]> {
        const endpoint = `api/Address/GetRegions`;
        return Common.post(endpoint, options) as Promise<Address.IAddressCatalog[]>;
    }

    getCities(options: Address.IAddressRequestModel): Promise<Address.IAddressCatalog[]> {
        const endpoint = `api/Address/GetCities`;
        return Common.post(endpoint, options) as Promise<Address.IAddressCatalog[]>;
    }

    //getInRegions(AOGUID: string): Promise<FIAS.IFiasInRegion[]> {
    //    const endpoint = `api/Address/GetInRegions?AOGUID=${AOGUID}`;
    //    return Common.post(endpoint, null) as Promise<FIAS.IFiasInRegion[]>;
    //}

    getSettlements(options: Address.IAddressRequestModel): Promise<Address.IAddressCatalog[]> {
        const endpoint = `api/Address/GetSettlements`;
        return Common.post(endpoint, options) as Promise<Address.IAddressCatalog[]>;
    }

    getElmStructures(options: Address.IAddressRequestModel): Promise<Address.IAddressCatalog[]> {
        const endpoint = `api/Address/GetElmStructures`;
        return Common.post(endpoint, options) as Promise<Address.IAddressCatalog[]>;
    }

    getStreets(options: Address.IAddressRequestModel): Promise<Address.IAddressCatalog[]> {
        const endpoint = `api/Address/GetStreets`;
        return Common.post(endpoint, options) as Promise<Address.IAddressCatalog[]>;
    }

    getFiasAddressObjetcList(options: Address.IAddressRequestModel): Promise<Address.IAddressCatalog[]> {
        const endpoint = `api/Address/GetFiasAddressObjetcList`;
        return Common.post(endpoint, options) as Promise<Address.IAddressCatalog[]>;
    }

    //getAddTerritories(AOGUID: string): Promise<FIAS.IFiasAddTerritory[]> {
    //    const endpoint = `api/Address/GetAddTerritories?AOGUID=${AOGUID}`;
    //    return Common.post(endpoint, null) as Promise<FIAS.IFiasAddTerritory[]>;
    //}

    //getAddTerritoryStreets(AOGUID: string): Promise<FIAS.IFiasAddTerritoryStreet[]> {
    //    const endpoint = `api/Address/GetAddTerritoryStreets?AOGUID=${AOGUID}`;
    //    return Common.post(endpoint, null) as Promise<FIAS.IFiasAddTerritoryStreet[]>;
    //}

    //getAddressHouses(AOGUID: string): Promise<FIAS.IFiasAddressHouse[]> {
    //    const endpoint = `api/Address/GetAddressHouses?AOGUID=${AOGUID}`;
    //    return Common.post(endpoint, null) as Promise<FIAS.IFiasAddressHouse[]>;
    //}

    getHouses(options: Address.IAddressRequestModel): Promise<Address.IAddressHouse[]> {
        const endpoint = `api/Address/GetHouses`;
        return Common.post(endpoint, options) as Promise<Address.IAddressHouse[]>;
    }

    getSteads(options: Address.IAddressRequestModel): Promise<Address.IAddressStead[]> {
        const endpoint = `api/Address/getSteads`;
        return Common.post(endpoint, options) as Promise<Address.IAddressStead[]>;
    }

    //getDetailAddrSearch(searchModel: FIAS.IFiasDetailSearch): Promise<FIAS.IFiasDetailAddress> {
    //    const endpoint = `api/Address/GetDetailAddrSearch`;
    //    return Common.post(endpoint, searchModel) as Promise<FIAS.IFiasDetailAddress>;
    //}

    getAddressMappings(): Promise<Address.IAddressComparisonsDpModel[]> {
        const endpoint = `api/Address/GetAddressComparisons`;
        return Common.post(endpoint, null) as Promise<Address.IAddressComparisonsDpModel[]>;
    }

    AddressUpdate(options: Address.IAddressUpdateModel): Promise<Address.IAddressUpdateResultModel> {
        const endpoint = `api/Address/AddressUpdate`;
        return Common.post(endpoint, options) as Promise<Address.IAddressUpdateResultModel>;
    }

    AddressNameUpdate(options: Address.IAddressUpdateResultModel): Promise<Address.IAddressUpdateResultModel> {
        const endpoint = `api/Address/AddresNameUpdate`;
        return Common.post(endpoint, options) as Promise<Address.IAddressUpdateResultModel>;
    }

    getFlatTypeList(): Promise<Address.IFlatTypeModel[]> {
        const endpoint = `api/Address/GetFlatTypeList`;
        return Common.post(endpoint, null) as Promise<Address.IFlatTypeModel[]>;
    }

    getHouse(options: Address.IHouseRequestModel): Promise<Address.IAddressHouse> {
        const endpoint = `api/Address/getHouse`;
        return Common.post(endpoint, options) as Promise<Address.IAddressHouse>;
    }

    getStead(options: Address.ISteadRequestModel): Promise<Address.IAddressStead> {
        const endpoint = `api/Address/getStead`;
        return Common.post(endpoint, options) as Promise<Address.IAddressStead>;
    }

    getSteadUpdateInfo(steadId: System.IGuid): Promise<Address.ISteadUpdateModel> {
        const endpoint = `api/Address/getSteadUpdateInfo?steadId=${steadId}`;
        return Common.post(endpoint, null) as Promise<Address.ISteadUpdateModel>;
    }

    getHouseUpdateInfo(houseId: System.IGuid): Promise<Address.IHouseUpdateModel> {
        const endpoint = `api/Address/getHouseUpdateInfo?houseId=${houseId}`;
        return Common.post(endpoint, null) as Promise<Address.IHouseUpdateModel>;
    }

    getAddressObjectCatalog(options: Address.IAddressObjectRequestModel): Promise<Address.IAddressCatalog> {
        const endpoint = `api/Address/getAddressObjectCatalog`;
        return Common.post(endpoint, options) as Promise<Address.IAddressCatalog>;
    }

    getAddressList(options: Address.IAddressResultRequestModel): Promise<Address.IAddressFindResult> {
        const endpoint = `api/Address/getAddressList`;
        return Common.post(endpoint, options) as Promise<Address.IAddressFindResult>;
    }

    getAddressObjectTypes(level: number): Promise<Address.IAddressObjectTypeModel[]> {

        const endpoint = `api/Address/GetAddressObjectTypes?level=${level.toString().replace(".", "")}`;
        return Common.post(endpoint, null) as Promise<Address.IAddressObjectTypeModel[]>;
    }

    getEstateStatuses(): Promise<Address.IAddressEstateStatus[]> {
        const endpoint = `api/Address/GetEstateStatuses`;
        return Common.post(endpoint, null) as Promise<Address.IAddressEstateStatus[]>;
    }

    getStructureStatuses(): Promise<Address.IAddressStructureStatus[]> {
        const endpoint = `api/Address/GetStructureStatuses`;
        return Common.post(endpoint, null) as Promise<Address.IAddressStructureStatus[]>;
    }

    getAddressObject(aoId: System.IGuid): Promise<Address.IAddressObjectModel> {
        const endpoint = `api/Address/getAddressObject?aoId=${aoId}`;
        return Common.post(endpoint, null) as Promise<Address.IAddressObjectModel>;
    }

    getAddressEntity(baId: number): Promise<Address.IAddressEntityModel> {
        const endpoint = `api/Address/getAddressEntity?baId=${baId}`;
        return Common.post(endpoint, null) as Promise<Address.IAddressEntityModel>;
    }

    addressObjectUpdate(options: Address.IAddressObjectUpdateModel): Promise<Address.IAddressObjectUpdateResultModel> {
        const endpoint = `api/Address/addressObjectUpdate`;
        return Common.post(endpoint, options) as Promise<Address.IAddressObjectUpdateResultModel>;
    }

    houseUpdate(options: Address.IHouseUpdateModel): Promise<Address.IHouseUpdateResultModel> {
        const endpoint = `api/Address/houseUpdate`;
        return Common.post(endpoint, options) as Promise<Address.IHouseUpdateResultModel>;
    }

    steadUpdate(options: Address.ISteadUpdateModel): Promise<Address.ISteadUpdateResultModel> {
        const endpoint = `api/Address/steadUpdate`;
        return Common.post(endpoint, options) as Promise<Address.ISteadUpdateResultModel>;
    }

    getAddressHierarchy(baId: number): Promise<Address.IAddressHierarchyResponse[]> {
        const endpoint = `api/Address/getAddressHierarchy?baId=${baId}`;
        return Common.post(endpoint, null) as Promise<Address.IAddressHierarchyResponse[]>;
    }

    sddressSettle(options: Address.IAddressSettleRequestModel[]): Promise<boolean> {
        const endpoint = `api/Address/AddressSettle`;
        return Common.post(endpoint, options) as Promise<boolean>;
    }
    bindHouses(options: Address.IBindHousesModel): Promise<boolean> {
        const endpoint = `api/Address/BindHouses`;
        return Common.post(endpoint, options) as Promise<boolean>;
    }

}

let apiAddress = new ApiAddress();
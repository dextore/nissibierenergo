﻿/// <reference path="../../TypeLite.Net4.d.ts" />
import Common = require("../Common/CommonExporter");

export interface IApiBanks {
    get(): Promise<Banks.IFrontBanksEntity[]>;
    getBank(baId: number): Promise<Banks.IFrontBanksEntity>;
    getBranches(baId: number): Promise<Banks.IFrontBanksEntity[]>;
    save(model: Banks.IFrontBanksEntity): Promise<Banks.IFrontBanksEntity>;
    saveMarkerHistory(model: Markers.IMarkeHistoryChangesModel): Promise<Markers.IPeriodicMarkerValueModel>;
    getPeriodicBanksMarkerHistory(baId: number, markerId: number): Promise<Markers.IPeriodicMarkerValueModel[]>;
    getBanksHeader(baid: number): Promise<string>;
}

export function getApiBanks(): IApiBanks {
    return apiBanks;
}

class ApiBanks implements IApiBanks {
    get(): Promise<Banks.IFrontBanksEntity[]> {
        const endpoint = "api/Banks/GetBanks";
        return <Promise<Banks.IFrontBanksEntity[]>>Common.get(endpoint);
    }

    getBank(baId: number): Promise<Banks.IFrontBanksEntity> {
        const endpoint = "api/Banks/GetBank";
        const params = { baId: baId };
        return <Promise<Banks.IFrontBanksEntity>>Common.get(endpoint, params);
    }

    getBranches(baId: number): Promise<Banks.IFrontBanksEntity[]> {
        const endpoint = "api/Banks/GetBranchesByBankId";
        const params = { baId: baId };
        return <Promise<Banks.IFrontBanksEntity[]>>Common.get(endpoint, params);
    }
    
    save(model: Banks.IFrontBanksEntity): Promise<Banks.IFrontBanksEntity> {
        const endpoint = "api/Banks/Save";
        return <Promise<Banks.IFrontBanksEntity>>Common.post(endpoint, model);
    }

    getPeriodicBanksMarkerHistory(baId: number, markerId: number): Promise<Markers.IPeriodicMarkerValueModel[]> {
        const endpoint = `api/Banks/GetPeriodicBanksMarkerHistory/${baId}/${markerId}`;
        return Common.get(endpoint);
    }

    saveMarkerHistory(model: Markers.IMarkeHistoryChangesModel): Promise<Markers.IPeriodicMarkerValueModel> {
        const endpoint = `api/Banks/SavePeriodicMarkerHistory`;
        return Common.post(endpoint, model);
    }

    getBanksHeader(baid: number): Promise<string> {
        const endpoint = `api/Banks/GetBankHeader?baId=${baid}`;
        return Common.get(endpoint);
    }
}

let apiBanks = new ApiBanks();
﻿import Common = require("../Common/CommonExporter");

export class ApiBranchs {
    private static _branchData: any[];

    public static get branchData() {
        if (ApiBranchs._branchData) {
            return ApiBranchs._branchData;
        }

        return ApiBranchs.getAllBranches().then(res => res);
    }
    
    public static getAllBranches():Promise<any> {
        return Common.get("api/Branches/GetBranchesTree");
    }

    public static save(personId: number, branches: Branch.IBranchItemModel[]): Promise<Branch.IBranchItemModel[]> {
        const endpont = `api/Branches/UpdatePersonBranches/${personId}`;
        return Common.post(endpont, branches);
    }

    public static delete(personId: number, branches: Branch.IBranchItemModel[]): Promise<Branch.IBranchItemModel[]> {
        const endpont = `api/Branches/DeletePersonBranches/${personId}`;
        return Common.post(endpont, branches);
    }

    public static getPersonBranches(personId: number) {
        const endpoint = `api/Branches/GetPersonsBranches?comercialPersonId=${personId}`;
        return Common.get(endpoint);
    }

    public static checkBranchItemForCorrect(personId: number, branch: Branch.IBranchItemModel):Promise<string> {
        const endpont = `api/Branches/CheckBranchForCorrect/${personId}`;
        return Common.post(endpont, branch);
    }
}
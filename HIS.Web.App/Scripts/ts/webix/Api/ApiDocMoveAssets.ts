﻿import Common = require("../Common/CommonExporter")


export interface IApiDocMoveAssets {
    getDocMoveAssetsData(baId: number): Promise<DocMoveAssets.IDocMoveAssetsData>;
    isHasAssets(baId: number): Promise<boolean>;
    updateDocMoveAssets(model: DocMoveAssets.IDocMoveAssetsData): Promise<number> ;
    getDocMoveAssetsRow(docMoveId: number, recId: number): Promise<DocMoveAssets.IDocMoveAssetsRow>;
    getDocMoveAssetsRows(docMoveId: number): Promise<DocMoveAssets.IDocMoveAssetsRow[]>;
    updateDocAcceptAssetRow(model: DocMoveAssets.IDocMoveAssetsRow): Promise<DocMoveAssets.IDocMoveAssetsRow>;
    deleteDocAcceptAssetRow(model: DocMoveAssets.IDocMoveAssetsRow): Promise<boolean>;
    getAssetsInfo(baId: number): Promise <DocMoveAssets.IAssetsDataModel>;

}

export function getApiDocMoveAssets(): IApiDocMoveAssets {
    return apiDocMoveAssets;
}

class ApiDocMoveAssets implements IApiDocMoveAssets {

    getDocMoveAssetsData(baId: number): Promise<DocMoveAssets.IDocMoveAssetsData> {
        const endpoint = `API/DocMoveAssets/GetDocMoveAssetsData/?baId=` + baId;
        return Common.post(endpoint, null) as Promise<DocMoveAssets.IDocMoveAssetsData>;
    }
    isHasAssets(baId: number): Promise<boolean> {
        const endpoint = `API/DocMoveAssets/IsHasAssets/?baId=` + baId;
        return Common.post(endpoint, null) as Promise<boolean>;
    }

    updateDocMoveAssets(model: DocMoveAssets.IDocMoveAssetsData): Promise<number> {
        const endpoint = `API/DocMoveAssets/UpdateDocMoveAssets/`;
        return Common.post(endpoint, model) as Promise<number>;
    }

    getDocMoveAssetsRow(docMoveId: number, recId: number): Promise<DocMoveAssets.IDocMoveAssetsRow> {
        const endpoint = `API/DocMoveAssets/GetDocMoveAssetsRow/?docMoveId=${docMoveId}&recId=${recId}`;
        return Common.post(endpoint, null) as Promise<DocMoveAssets.IDocMoveAssetsRow>;
    }

    getDocMoveAssetsRows(docMoveId: number): Promise<DocMoveAssets.IDocMoveAssetsRow[]> {
        const endpoint = `API/DocMoveAssets/GetDocMoveAssetsRows/?docMoveId=${docMoveId}`;
        return Common.post(endpoint, null) as Promise<DocMoveAssets.IDocMoveAssetsRow[]>;
    }

    updateDocAcceptAssetRow(model: DocMoveAssets.IDocMoveAssetsRow): Promise<DocMoveAssets.IDocMoveAssetsRow> {
        const endpoint = `API/DocMoveAssets/UpdateDocAcceptAssetRow/`;
        return Common.post(endpoint, model) as Promise<DocMoveAssets.IDocMoveAssetsRow>;
    }

    deleteDocAcceptAssetRow(model: DocMoveAssets.IDocMoveAssetsRow): Promise<boolean> {
        const endpoint = `API/DocMoveAssets/DeleteDocAcceptAssetRow/`;
        return Common.post(endpoint, model) as Promise<boolean>;
    }

    getAssetsInfo(baId: number): Promise<DocMoveAssets.IAssetsDataModel> {
        const endpoint = `API/DocMoveAssets/GetAssetsInfo/?baId=${baId}`;
        return Common.post(endpoint, null) as Promise<DocMoveAssets.IAssetsDataModel>;
    }
}

let apiDocMoveAssets = new ApiDocMoveAssets()
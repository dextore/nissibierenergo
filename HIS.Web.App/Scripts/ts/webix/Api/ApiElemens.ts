﻿export interface ApiElements {
    get(): Promise<any>;
    getExt(): Promise<Elements.IElementExtModel>;
    getItem(id: number): Promise<any>;
    getItemExt(objectId: number): Promise<Elements.IElementExtModel>;
    save(model: Elements.IElementRetModel): Promise<any>;
    getBATypeId(): number;
}

﻿import Common = require("../Common/CommonExporter");

export class ApiPersons {
    static getPerson(personId: number): Promise<Persons.IPersonModel> {
        const endpoint = `api/persons/GetPerson/${personId}`;
        return Common.get(endpoint);
    }

    static getPersonHeader(personId: number): Promise<Persons.IPersonHeaderModel> {
        const endpoint = `api/persons/GetPersonHeader/${personId}`;
        return Common.get(endpoint);
    }

    static update(person: Persons.IPersonUpdateModel): Promise<Markers.IEntityMarkersModel> {
        const endpoint = "api/persons/update";

        const promise = webix.promise.defer();

        Common.post(endpoint, person).then(result => {
            Common.checkDataMarker(result.markerValues);
            (promise as any).resolve(result);
        }).catch(() => {
            (promise as any).reject();
        });

        return promise;
    }

    static getPersonInformation(person: Persons.IPersonInfoModel): Promise<Persons.IPersonInfoResponseModel> {
        const endpoint = "api/persons/GetPersonInformation";
        return Common.post(endpoint, person);
    }

    static updatePersonInformation(personInfoUpdate: Persons.IPersonInfoUpdateModel):
        Promise<Persons.IPersonInfoResponseModel> {
        const endpoint = "api/persons/UpdatePersonInformation";
        return Common.post(endpoint, personInfoUpdate);
    }
    
}
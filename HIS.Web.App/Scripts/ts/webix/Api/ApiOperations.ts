﻿import Common = require("../Common/CommonExporter");

export interface IApiOperations {
    getAllowedEntityOperations(baId): Promise<Operations.IEntityOperationModel>;
    getEntityStates(baId: number): Promise<States.IStateInfoModel[]>;
    getEntityState(baId): Promise<Operations.IEntityStateModel>;
    stateChange(model: Operations.IOperationUpdateModel): Promise<Operations.IEntityStateModel>;
}

export function getApiOperations(): IApiOperations {
    return apiOperations;
} 

class ApiOperations implements IApiOperations {

    getAllowedEntityOperations(baTypeId: number): Promise<Operations.IEntityOperationModel> {
        const endpoint = `api/Operations/GetAllowedEntityOperations/${baTypeId}`;
        return Common.get(endpoint);
    }

    getEntityStates(baId: number): Promise<States.IStateInfoModel[]> {
        const endpoint = `api/Operations/GetEntityStates/${baId}`;
        return Common.get(endpoint);
    }

    getEntityState(baId: number): Promise<Operations.IEntityStateModel> {
        const endpoint = `api/Operations/GetEntityState/${baId}`;
        return Common.get(endpoint);
    }

    stateChange(model: Operations.IOperationUpdateModel): Promise<Operations.IEntityStateModel> {
        const endpoint = `api/Operations/StateChange`;
        return Common.post(endpoint, model);
    }
}

let apiOperations = new ApiOperations();
﻿import Common = require("../Common/CommonExporter");

export interface IApiCommons {
    getEntitiesInfo(): Promise<Entities.IEntityInfoModel[]>;
    checkErrors(baId: number): Promise<string[]>;
    getEntityViewsItems(baTypeId: number): Promise<Entities.IEntityViewItemModel[]>;
}

export function getApiCommons(): IApiCommons {
    return apiCommons;
}

class ApiCommons implements IApiCommons {
    getEntitiesInfo(): Promise<Entities.IEntityInfoModel[]> {
        const endpoint = `api/Common/GetEntitiesInfo`;
        return Common.get(endpoint, null);
    }
    checkErrors(baId: number): Promise<string[]> {
        const endpoint = `api/Common/CheckErrors/?baId=${baId}`;
        return Common.post(endpoint, null) as Promise<string[]>;
    }

    getEntityViewsItems(baTypeId: number): Promise<Entities.IEntityViewItemModel[]> {
        const endpoint = `api/Common/GetEntityViewsItems/?baTypeId=${baTypeId}`;
        return Common.post(endpoint, null) as Promise<Entities.IEntityViewItemModel[]>;
    }


}

const apiCommons = new ApiCommons()
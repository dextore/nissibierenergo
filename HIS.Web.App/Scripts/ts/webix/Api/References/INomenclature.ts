﻿import Common = require("../../Common/CommonExporter")


export interface IApiNomenclature {
    inventoryUpdate(updateModel: Inventory.IInventoryDataModel): Promise<Inventory.IInventoryDataModel>;
    getInventoryList(filters: Inventory.IInventoryListFiltersModel): Promise<Array<Inventory.IInventoryListItemModel>>;
    getInventories(): Promise<Array<Inventory.IIInventoryModel>>;
    getInventoriesByCompanyId(companyId: number): Promise<Array<Inventory.IIInventoryModel>>;
    getInventoryDataModel(baId: number): Promise<Inventory.IInventoryDataModel>;
    getLsCompanyAreas(): Promise<Array<Inventory.ILSCompanyAreas>>;
    getInventoriesGroup(): Promise<Array<Inventory.IIGroupsModel>>;
    getRefSysUnits(): Promise<Array<Inventory.IRefSysUnitsModel>>;
}

export function getApiNomenclature(): IApiNomenclature {
    return apiNomenclature;
}

class ApiNomenclature implements IApiNomenclature {


    inventoryUpdate(updateModel: Inventory.IInventoryDataModel): Promise<Inventory.IInventoryDataModel> {
        const endpoint = `API/Inventory/InventoryUpdate/`;
        return Common.post(endpoint, updateModel) as Promise<Inventory.IInventoryDataModel>;
    }

    
    getInventoriesByCompanyId(companyId: number): Promise<Array<Inventory.IIInventoryModel>> {
        const endpoint = `API/Inventory/GetInventoriesByCompanyId/?companyId=` + companyId;
        return Common.post(endpoint, null) as Promise<Array<Inventory.IIInventoryModel>>;
    }
    getInventories(): Promise<Array<Inventory.IIInventoryModel>> {
        const endpoint = `API/Inventory/GetInventories/`;
        return Common.post(endpoint, null) as Promise<Array<Inventory.IIInventoryModel>>;
    }
    getInventoryDataModel(baId:number): Promise<Inventory.IInventoryDataModel> {
        const endpoint = `API/Inventory/GetInventoryDataModel/?BAId=`+baId;
        return Common.post(endpoint, null) as Promise<Inventory.IInventoryDataModel>;
    }
    getInventoryList(filters: Inventory.IInventoryListFiltersModel): Promise<Array<Inventory.IInventoryListItemModel>> {
        const endpoint = `API/Inventory/GetInventoriesList`;
        return Common.post(endpoint, filters) as Promise<Array<Inventory.IInventoryListItemModel>>;
    }
    getLsCompanyAreas(): Promise<Array<Inventory.ILSCompanyAreas>> {
        const endpoint = `API/Inventory/GetLsCompanyAreas/`;
        return Common.post(endpoint, null) as Promise<Array<Inventory.ILSCompanyAreas>>;
    }

    getRefSysUnits(): Promise<Array<Inventory.IRefSysUnitsModel>> {
        const endpoint = `API/Inventory/GetRefSysUnits/`;
        return Common.post(endpoint, null) as Promise<Array<Inventory.IRefSysUnitsModel>>;
    }

    getInventoriesGroup(): Promise<Array<Inventory.IIGroupsModel>> {
        const endpoint = `API/Inventory/GetInventoriesGroup/`;
        return Common.post(endpoint, null) as Promise<Array<Inventory.IIGroupsModel>>;

    }
}

let apiNomenclature = new ApiNomenclature();

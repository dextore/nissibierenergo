﻿import Common = require("../Common/CommonExporter");

export class ApiAdminPanel {
    static getEntities(): Promise<AdminPanel.IBaTypesModel> {
        const endpont = `api/AdminPanel/GetEntities`;
        return Common.get(endpont);
    }

    static getMarkers(batypeId: number): Promise<AdminPanel.IMarkersAdminModel> {
        const endpoint = `api/AdminPanel/GetMarkers?batypeId=${batypeId}`;
        return Common.get(endpoint);
    }

    static getAvailableValuesForList(markerId: number): Promise<AdminPanel.IItemTableViewModel[]> {
        const endpoint = `api/AdminPanel/GetAvailableValuesForList?markerId=${markerId}`;
        return Common.get(endpoint);
    }

    static getAvailableValuesForReference(markerId: number): Promise<AdminPanel.IItemTableViewModel[]> {
        const endpoint = `api/AdminPanel/GetAvailableValuesForReference?markerId=${markerId}`;
        return Common.get(endpoint);
    }

    static getAvailableSteteForEntiti(batypeId: number): Promise<AdminPanel.IBaTypesStatesModel> {
        const endpoint = `api/AdminPanel/GetAvailableSteteForEntiti?baTypeId=${batypeId}`;
        return Common.get(endpoint);
    }

    static getAvailableOperationsForEntiti(batypeId: number): Promise<AdminPanel.IBaTypesOperationsModel> {
        const endpoint = `api/AdminPanel/GetAvailableOperationsForEntiti?baTypeId=${batypeId}`;
        return Common.get(endpoint);
    }

    static getAvailablOperationsStateForEntiti(batypeId: number): Promise<AdminPanel.IBaTypesOperationsStateModel> {
        const endpoint = `api/AdminPanel/GetAvailablOperationsStateForEntiti?baTypeId=${batypeId}`;
        return Common.get(endpoint);
    }

    static getAllMarkers(): Promise<AdminPanel.IOnlyMarkersDataModel> {
        const endpoint = `api/AdminPanel/GetMarkers`;
        return Common.get(endpoint);
    }

    static createUpdateEntitiType(data: AdminPanel.IBaTypesModel): Promise<AdminPanel.IBaTypesModel> {
        const endpoint = `api/AdminPanel/CreateUpdateEntitiType`;
        return Common.post(endpoint, data);
    }

    static createUpdateMarker(data: AdminPanel.IOnlyMarkersDataModel): Promise<AdminPanel.IOnlyMarkersDataModel> {
        const endpoint = `api/AdminPanel/CreateUpdateMarker`;
        return Common.post(endpoint, data);
    }

    static deleteMarkerFromBaType(baId: number, data: AdminPanel.IMarkersAdminModel): Promise<boolean> {
        const endpoint = `api/AdminPanel/DeleteMarkerFromBaType?baId=${baId}`;
        return Common.post(endpoint, data);
    }

    static deleteMarker(markerId: number): Promise<boolean> {
        const endpoint = `api/AdminPanel/DeleteMarker?markerId=${markerId}`;
        return Common.post(endpoint, {});
    }

    static addMarkerToBaType(baId: number, data: AdminPanel.IMarkersAdminModel): Promise<AdminPanel.IMarkersAdminModel> {
        const endpoint = `api/AdminPanel/AddMarkerToBaType?baId=${baId}`;
        return Common.post(endpoint, data);
    }

    static canEditLinkOnEntityMarker(markerId: number): Promise<AdminPanel.IOnlyMarkersDataModel> {
        const endpoint = `api/AdminPanel/CanEditLinkOnEntityMarker?markerId=${markerId}`;
        return Common.get(endpoint);
    }

    static createUpdateState(data: AdminPanel.IBaTypesStatesCreateModel): Promise<AdminPanel.IBaTypesStatesCreateModel> {
        const endpoint = `api/AdminPanel/CreateUpdateState`;
        return Common.post(endpoint, data);
    }

    static deleteState(stateId: number): Promise<boolean> {
        const endpoint = `api/AdminPanel/DeleteState?stateId=${stateId}`;
        return Common.post(endpoint, {});
    }

    static createUpdateOperations(data: AdminPanel.IBaTypesOperationsCreateModel): Promise<AdminPanel.IBaTypesOperationsCreateModel> {
        const endpoint = `api/AdminPanel/CreateUpdateOperations`;
        return Common.post(endpoint, data);
    }

    static deleteOperations(operationsId: number): Promise<boolean> {
        const endpoint = `api/AdminPanel/DeleteOperations?operationsId=${operationsId}`;
        return Common.post(endpoint, {});
    }

    static getAllStates(): Promise<AdminPanel.IBaTypesStatesCreateModel[]> {
        const endpoint = `api/AdminPanel/GetAllStates`;
        return Common.get(endpoint);
    }

    static getAllOperations(): Promise<AdminPanel.IBaTypesOperationsCreateModel[]> {
        const endpoint = `api/AdminPanel/GetAllOperations`;
        return Common.get(endpoint);
    }

    static createUpdateListItem(markerId: number, data: AdminPanel.IItemTableViewModel): Promise<AdminPanel.IItemTableViewModel> {
        const endpoint = `api/AdminPanel/CreateUpdateListItem?markerId=${markerId}`;
        return Common.post(endpoint, data);
    }

    static deleteListItem(markerId: number, listItemId: number): Promise<boolean> {
        const endpoint = `api/AdminPanel/DeleteListItem?markerId=${markerId}&listItemId=${listItemId}`;
        return Common.post(endpoint, {});
    }

    static addStateToBaType(baid: number, state: AdminPanel.IBaTypesStatesModel): Promise<AdminPanel.IBaTypesStatesModel> {
        const endpoint = `api/AdminPanel/AddStateToBaType?baid=${baid}`;
        return Common.post(endpoint, state); 
    }

    static addOperationToBaType(baid: number, operation: AdminPanel.IBaTypesOperationsModel): Promise<AdminPanel.IBaTypesOperationsModel> {
        const endpoint = `api/AdminPanel/AddOperationToBaType?baid=${baid}`;
        return Common.post(endpoint, operation);
    }

    static deleteOperationFromBaType(baid: number, operationId: number): Promise<boolean> {
        const endpoint = `api/AdminPanel/DeleteOperationFromBaType?baid=${baid}&operationId=${operationId}`;
        return Common.post(endpoint, {});
    }

    static deleteStateFromBaType(baId: number, stateId: number): Promise<boolean> {
        const endpoint = `api/AdminPanel/DeleteStateFromBaType?baid=${baId}&stateId=${stateId}`;
        return Common.post(endpoint, { }); 
    }

    static addOperationStateToBaType(baId: number, data: AdminPanel.IBaTypesOperationsStateCreateModel): Promise<AdminPanel.IBaTypesOperationsStateModel> {
        const endpoint = `api/AdminPanel/AddOperationStateToBaType?baid=${baId}`;
        return Common.post(endpoint, data); 
    }

    static deleteOperationStateToBaType(baId: number, operationStateId: number, srcStateId: number | string): Promise<boolean> {
        const endpoint = `api/AdminPanel/DeleteOperationStateToBaType?baId=${baId}&operationId=${operationStateId}&srcStateId=${srcStateId}`;
        return Common.post(endpoint, {});
    }
}
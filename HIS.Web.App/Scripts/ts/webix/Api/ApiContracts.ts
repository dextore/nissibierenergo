﻿import Common = require("../Common/CommonExporter");

export interface IApiContrcats {
    //getContracts(): Promise<Contracts.IContractModel[]>;
    //getContract(contractId: number): Promise<Contracts.IContractInfoModel>;
    //getContractsByContractor(contractorId: number): Promise<Contracts.IContractModel[]>;
    getByLegalSubject(legalSubjectId: number): Promise<Contracts.ILSContractModel[]>;
    //save(contract: Contracts.IContractModel): Promise<Contracts.IContractInfoModel>;
}

export function getApiContrcats(): IApiContrcats {
    return apiContrcats;
}


class ApiContrcats implements IApiContrcats {

    //getContracts(): Promise<Contracts.IContractModel[]> {
    //    const endpoint = "api/Contract/GetContracts";
    //    return Common.get(endpoint);
    //}

    //getContract(contractId: number): Promise<Contracts.IContractInfoModel> {
    //    const endpoint = `api/Contract/GetContract?contractId=${contractId}`;
    //    return Common.get(endpoint);
    //}

    //getContractsByContractor(contractorId: number): Promise<Contracts.IContractModel[]> {
    //    const endpoint = `api/Contract/GetContractsByContractor?contractorId=${contractorId}`;
    //    return Common.get(endpoint);
    //}

    getByLegalSubject(legalSubjectId: number): Promise<Contracts.ILSContractModel[]> {
        const endpoint = `api/contract/ByLegalSubject/${legalSubjectId}`;
        return Common.post(endpoint, null);
    }

    //save(contract: Contracts.IContractInfoModel): Promise<Contracts.IContractInfoModel> {
    //    const endpoint = "api/Contract/Save";
    //    return Common.post(endpoint, contract);
    //}
}

let apiContrcats = new ApiContrcats();
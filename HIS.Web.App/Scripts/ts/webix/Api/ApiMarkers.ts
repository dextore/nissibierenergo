﻿import Common = require("../Common/CommonExporter");

export interface IApiMarkers {
    getEntityMarkerValues(model: Markers.IMarkerValuesRequestModel): Promise<Markers.IMarkerValueModel[]>;
    getMarkerValueList(markerId: number): Promise<Markers.IMarkerValueListItemModel[]>;
    getOptionalMarkers(baTypeId: number, markerId: number, itemId: number): Promise<Markers.IOptionalMarkersModel[] >;
    getPeriodicMarkerHistory(baId: number, markerId: number): Promise<Markers.IPeriodicMarkerValueModel[]>;
    getEntityMarkersValue(baId: number): Promise<Markers.IEntityMarkersValueListModel[]>;
    getEntityCaption(baId: number): Promise<Markers.IEntityCaptionModel>;
    getEntityMarkersInfo(baTypeId: number): Promise<Markers.IMarkerInfoModel[]>;
    getCatalogMarkerValueList(markerId: number): Promise<Markers.IReferenceMarkerValueItemModel[]>;

    entityMarkerValues(model: Markers.IMarkerValuesRequestModel): Promise<Markers.IEntityMarkersModel>;
    collectibleMarkerValues(baId: number, name: string): Promise<Markers.IMarkerValueModel[]>;
    getCatalogMarkerValueItems(markerId: number): Promise<any>;

    saveMarkerValue(value: Markers.IMarkerValueSetModel): Promise<Markers.IMarkerValueModel>;
    saveMarkerHistory(model: Markers.IMarkeHistoryChangesModel): Promise<Markers.IPeriodicMarkerValueModel>;
}

export function getApiMarkers(): IApiMarkers {
    return apiMarkers;
}

class ApiMarkers implements IApiMarkers {

    getOptionalMarkers(baTypeId: number, markerId: number, itemId: number): Promise<Markers.IOptionalMarkersModel[] > {
        const endpoint = `api/Markers/GetOptionalMarkers?baTypeId=${baTypeId}&markerId=${markerId}&itemId=${itemId}`;
        return Common.post(endpoint, {});
    }
    

    getEntityMarkerValues(model: Markers.IMarkerValuesRequestModel): Promise<Markers.IMarkerValueModel[]> {
        const endpoint = `api/Markers/GetEntityMarkerValues`;
        return Common.post(endpoint, model);
    }

    getMarkerValueList(markerId: number): Promise<Markers.IMarkerValueListItemModel[]> {
        const endpoint = `api/Markers/GetMarkerValueList?markerId=${markerId}`;
        return Common.get(endpoint);
    }

    getPeriodicMarkerHistory(baId: number, markerId: number): Promise<Markers.IPeriodicMarkerValueModel[]> {
        const endpoint = `api/Markers/PeriodicMarkerHistor/${baId}/${markerId}`;
        return Common.get(endpoint);
    }

    getEntityMarkersValue(baId: number): Promise<Markers.IEntityMarkersValueListModel[]> {
        const endpoint = `api/Markers/GetEntityMarkersValue?baId=${baId}`;
        return Common.get(endpoint);
    }

    getEntityCaption(baId: number): Promise<Markers.IEntityCaptionModel> {
        const endpoint = `api/Markers/GetEntityCaption`;
        const data = { baId: baId };
        return Common.get(endpoint, data);
    }

    getEntityMarkersInfo(baTypeId: number): Promise<Markers.IMarkerInfoModel[]> {
        const endpoint = `api/Markers/EntityMarkersInfo/${baTypeId}`;
        return Common.get(endpoint, null);
    } 

    getCatalogMarkerValueList(markerId: number): Promise<Markers.IReferenceMarkerValueItemModel[]> {
        const endpoint = `api/Markers/CatalogValueList/${markerId}`;
        return Common.get(endpoint, null);
    }

    entityMarkerValues(model: Markers.IMarkerValuesRequestModel): Promise<Markers.IEntityMarkersModel> {
        const endpoint = `api/Markers/EntityMarkerValues`;
        const promise = webix.promise.defer();

        Common.post(endpoint, model).then(result => {
            Common.checkDataMarker(result.markerValues);
            (promise as any).resolve(result);
        }).catch(() => {
            (promise as any).reject();
            });

        return promise;
    }

    collectibleMarkerValues(baId: number, name: string): Promise<Markers.IMarkerValueModel[]> {
        const endpoint = `api/Markers/CollectibleMarkerValues/${baId}/${name}`;
        return Common.post(endpoint, null);
    }

    getCatalogMarkerValueItems(markerId: number): Promise<any> {
        const endpoint = `api/Markers/GetCatalogMarkerValueItems/${markerId}`;
        return Common.get(endpoint);
    }

    saveMarkerValue(value: Markers.IMarkerValueSetModel): Promise<Markers.IMarkerValueModel> {
        const endpoint = `api/Markers/SaveMarkerValue`;
        return Common.post(endpoint, value);
    }


    saveMarkerHistory(model: Markers.IMarkeHistoryChangesModel): Promise<Markers.IPeriodicMarkerValueModel> {
        const endpoint = `api/Markers/SaveMarkerHistory`;
        return Common.post(endpoint, model);
    }
}

var apiMarkers = new ApiMarkers();
﻿export function getData(name: string, request: () => Promise<any>): () => Promise<any> {
    return () => {
        const p = webix.promise.defer();

        const result = Storage.getData(name);

        if (result) {
            (p as any).resolve(result);
        } else {
            request().then(data => {
                Storage.setData(name, data);
                (p as any).resolve(result);
            });
        }

        return p;
    }
}

class Storage {
    private static data: { [id: string]: any } = {}

    static getData(name: string) {
        return Storage.data[name];
    }

    static setData(name: string, value: any) {
        Storage.data[name] = value;
    }

}
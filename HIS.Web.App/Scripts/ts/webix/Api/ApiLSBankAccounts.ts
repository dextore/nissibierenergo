﻿import Common = require("../Common/Common")

export class ApiLSbankAccounts {

    public static getLSBankAccounts(baId: number) {
        const endpoint = `api/LSBankAccounts/GetLSBankAccounts/${baId}`;
        return Common.get(endpoint, null) as Promise<BankAccounts.ILSBankAccountModel[]>;
    }

    public static getAllLSBankAccounts(baId: number) {
        const endpoint = `api/LSBankAccounts/GetAllLSBankAccounts/${baId}`;
        return Common.get(endpoint, null) as Promise<BankAccounts.ILSBankAccountModel[]>;
    }

    public static getLSBankAccount(baId: number, itemId: number) {
        const endpoint = `api/LSBankAccounts/GetLSBankAccount/${baId}/${itemId}`;
        return Common.get(endpoint, null) as Promise<BankAccounts.ILSBankAccountModel>;
    }

    public static update(model: BankAccounts.ILSBankAccountModel) {
        const endpoint = `api/LSBankAccounts/Update`;
        return Common.post(endpoint, model) as Promise<BankAccounts.ILSBankAccountModel>;
    }

    public static remove(baId: number, itemId: number) {
        const endpoint = `api/LSBankAccounts/Remove/${baId}/${itemId}`;
        return Common.post(endpoint, null) as Promise<boolean>;
    }

}
﻿import Common = require("../Common/CommonExporter");

export interface IApiHistory {
    getEntityStateHistory(baId: number): Promise<History.IEntityHistoryModel[]>;
    getEntityMarkersHistory(baId: number): Promise<History.IEntityHistoryModel[]>;
    getEntityData(baId: number): Promise<object>;
}

export function getApiHistory(): IApiHistory {
    return new ApiHistory();
}


class ApiHistory implements IApiHistory {

    getEntityStateHistory(baId: number): Promise<History.IEntityHistoryModel[]> {
        const endpoint = "api/History/GetEntityStateHistory?baId="+baId;
        return Common.post(endpoint, null);
    }
    getEntityMarkersHistory(baId: number): Promise<History.IEntityHistoryModel[]> {
        const endpoint = "api/History/GetEntityMarkersHistory?baId=" + baId;
        return Common.post(endpoint, null);
    }   
    getEntityData(baId: number): Promise<object> {
        const endpoint = "api/History/GetEntityData?baId=" + baId;
        return Common.post(endpoint, null);
    }
}

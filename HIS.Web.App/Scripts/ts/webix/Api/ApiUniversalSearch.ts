﻿import Common = require("../Common/CommonExporter");

export class ApiUniversalSearch {

    static getByName(model: UniversalSearch.IUniversalSearchRequestModel): Promise<UniversalSearch.ISearchResultModel> {
        const endpoint = `api/UniversalSearch/SearchByName`;
        return Common.post(endpoint, model);
    }

    static getByInn(model: UniversalSearch.IUniversalSearchRequestModel): Promise<UniversalSearch.ISearchResultModel> {
        const endpoint = `api/UniversalSearch/SearchByInn`;
        return Common.post(endpoint, model);
    }

}
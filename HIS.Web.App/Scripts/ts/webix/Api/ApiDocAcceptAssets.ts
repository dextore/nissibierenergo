﻿import Common = require("../Common/CommonExporter")


export interface IApiDocAcceptAssets {
    getDocAcceptAssetsRows(docAcceptId: number): Promise<DocAcceptAssets.IIDocAcceptAssetsRows[]>;
    getDocAcceptAssetsRow(docAcceptId: number, recId: number): Promise<DocAcceptAssets.IIDocAcceptAssetsRows>;
    getDocAcceptAssetsData(baId: number): Promise<DocAcceptAssets.IIDocAcceptAssetsData>;
    updateDocAcceptAssets(model: DocAcceptAssets.IIDocAcceptAssetsData): Promise<number>;
    getReceiptInvoiceSelect(caId: number): Promise<DocAcceptAssets.IIReceiptInvoiceSelectModel[]>;
    getReceiptInvoiceInventSelect(docId: number): Promise<DocAcceptAssets.IReceiptInvoiceInventSelectModel>;
    updateDocAcceptAssetRow(model: DocAcceptAssets.IIDocAcceptAssetsRows): Promise<DocAcceptAssets.IIDocAcceptAssetsRows>;
    deleteDocAcceptAssetRow(model: DocAcceptAssets.IIDocAcceptAssetsRows): Promise<boolean>;
    isHasSpecification(baId: number): Promise<boolean>;
}

export function getApiDocAcceptAssets(): IApiDocAcceptAssets {
    return apiDocAcceptAssets;
}

class ApiDocAcceptAssets implements IApiDocAcceptAssets {
    getDocAcceptAssetsRows(docAcceptId:number): Promise<DocAcceptAssets.IIDocAcceptAssetsRows[]> {
        const endpoint = `API/DocAcceptAssets/GetDocAcceptAssetsRows/?docAcceptId=` + docAcceptId;
        return Common.post(endpoint, null) as Promise<DocAcceptAssets.IIDocAcceptAssetsRows[]>;
    }
    getDocAcceptAssetsRow(docAcceptId: number, recId:number): Promise<DocAcceptAssets.IIDocAcceptAssetsRows> {
        const endpoint = `API/DocAcceptAssets/GetDocAcceptAssetsRow/?docAcceptId=` + docAcceptId + `&recId=` + recId;
        return Common.post(endpoint, null) as Promise<DocAcceptAssets.IIDocAcceptAssetsRows>;
    }
    getDocAcceptAssetsData(baId: number): Promise<DocAcceptAssets.IIDocAcceptAssetsData> {
        const endpoint = `API/DocAcceptAssets/GetDocAcceptAssetsData/?baId=` + baId;
        return Common.post(endpoint, null) as Promise<DocAcceptAssets.IIDocAcceptAssetsData>;
    }
    updateDocAcceptAssets(model: DocAcceptAssets.IIDocAcceptAssetsData): Promise<number> {
        const endpoint = `API/DocAcceptAssets/UpdateDocAcceptAssets/`;
        return Common.post(endpoint, model) as Promise<number>;
    }
    getReceiptInvoiceSelect(caId: number): Promise<DocAcceptAssets.IIReceiptInvoiceSelectModel[]> {
        const endpoint = `API/DocAcceptAssets/GetReceiptInvoiceSelect/?caId=` + caId;
        return Common.post(endpoint, null) as Promise<DocAcceptAssets.IIReceiptInvoiceSelectModel[]>;
    }
    getReceiptInvoiceInventSelect(docId: number): Promise<DocAcceptAssets.IReceiptInvoiceInventSelectModel> {
        const endpoint = `API/DocAcceptAssets/GetReceiptInvoiceInventSelect/?docAcceptId=` + docId;
        return Common.post(endpoint, null) as Promise<DocAcceptAssets.IReceiptInvoiceInventSelectModel>;
    }
    updateDocAcceptAssetRow(model: DocAcceptAssets.IIDocAcceptAssetsRows): Promise<DocAcceptAssets.IIDocAcceptAssetsRows> {
        const endpoint = `API/DocAcceptAssets/UpdateDocAcceptAssetRow/`;
        return Common.post(endpoint, model) as Promise<DocAcceptAssets.IIDocAcceptAssetsRows>;
    }
    deleteDocAcceptAssetRow(model: DocAcceptAssets.IIDocAcceptAssetsRows): Promise<boolean> {
        const endpoint = `API/DocAcceptAssets/DeleteDocAcceptAssetRow/`;
        return Common.post(endpoint, model) as Promise<boolean>;
    }
    
    isHasSpecification(baId:number): Promise<boolean> {
        const endpoint = `API/DocAcceptAssets/IsHasSpecification/?baId=` + baId;
        return Common.post(endpoint, null) as Promise<boolean>;
    }

}

let apiDocAcceptAssets = new ApiDocAcceptAssets()
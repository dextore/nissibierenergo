﻿import Common = require("../Common/CommonExporter");

export class ApiLegalPersons {

    static getLegalPerson(legalPersonId: number, markerIds: number[]): Promise<LegalPersons.ILegalPersonModel> {
        const endpoint = `api/LegalPersons/${legalPersonId}`;
        return Common.post(endpoint, markerIds);
    }

    static getParentLegalPerson(affiliateId): Promise<LegalPersons.ILegalPersonModel> { 
        const endpoint = `api/LegalPersons/GetParentLegalPerson/${affiliateId}`;
        return Common.get(endpoint);
    }

    static update(person: LegalPersons.ILegalPersonModel): Promise<LegalPersons.ILegalPersonModel> {
        const endpoint = "api/LegalPersons/Save";
        return Common.post(endpoint, person);
    }

    static updatePersonInformation(personId: number, markers: Markers.IMarkerValueModel[]): Promise<Persons.IPersonInfoResponseModel> {
        const endpoint = `api/LegalPersons/UpdatePersonInformation/${personId}`;
        return Common.post(endpoint, markers);
    }

    static getPersonInformation(personId: number, markers: number[]): Promise<Persons.IPersonInfoResponseModel> {
        const endpoint = `api/LegalPersons/GetPersonInformation/${personId}`;
        return Common.post(endpoint, markers);
    }
}

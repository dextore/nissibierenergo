﻿import Common = require("../Common/CommonExporter");

export class ApiConsignmentNotes {
    static getConsignmentNotesDocuments(): Promise<ConsignmentNotes.IConsignmentNotesDocumentModel[]> {
        return Common.get("api/ConsignmentNotes/GetConsignmentNotesDocuments");
    }

    static getConsignmentNotesDocument(baid: number): Promise<ConsignmentNotes.IConsignmentNotesDocumentModel> {
        return Common.get(`api/ConsignmentNotes/GetConsignmentNotesDocument?baId=${baid}`);
    }

    static getConsignmentNotesFullDocument(baid: number): Promise<ConsignmentNotes.IConsignmentNotesDocumentFullModel> {
        return Common.get(`api/ConsignmentNotes/GetConsignmentNotesFullDocument?baId=${baid}`);
    }

    static getConsignmentNotesSpecificationByDocument(docId: number): Promise<ConsignmentNotes.IConsignmentNotesSpecificationsModel[]> {
        return Common.get(`api/ConsignmentNotes/GetConsignmentNotesSpecificationByDocument?docId=${docId}`);
    }

    static getConsignmentNotesSpecification(recId: number, docId: number):
        Promise<ConsignmentNotes.IConsignmentNotesSpecificationsModel> {
        return Common.get(`api/ConsignmentNotes/GetConsignmentNotesSpecification?recId=${recId}&docId=${docId}`);
    }

    static saveConsignmentNotesDocument(consignmentNotesDocumentUpdate: ConsignmentNotes.
        IConsignmentNotesDocumentUpdateModel): Promise<ConsignmentNotes.IConsignmentNotesDocumentModel> {
        return Common.post(`api/ConsignmentNotes/SaveConsignmentNotesDocument`, consignmentNotesDocumentUpdate);
    }

    static saveConsignmentNotesSpecification(consignmentNotesSpecificationsUpdate: ConsignmentNotes.
        IConsignmentNotesSpecificationsUpdateModel): Promise<ConsignmentNotes.IConsignmentNotesSpecificationsModel> {
        return Common.post(`api/ConsignmentNotes/SaveConsignmentNotesSpecification`,
            consignmentNotesSpecificationsUpdate);
    }
    
    static deleteConsignmentNotesSpecification(recId: number, docId: number): Promise<boolean> {
        return Common.post(`api/ConsignmentNotes/DeleteConsignmentNotesSpecification?recId=${recId}&docId=${docId}`, null);
    }
    
    static getConsignmentNotesSpecificationFullModel(recId: number, docId: number): Promise<ConsignmentNotes.IConsignmentNotesSpecificationsFullModel> {
        return Common.get(`api/ConsignmentNotes/GetConsignmentNotesSpecificationFullModel?recId=${recId}&docId=${docId}`, null);
    }
}
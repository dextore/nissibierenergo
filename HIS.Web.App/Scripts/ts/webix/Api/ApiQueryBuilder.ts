﻿import Common = require("../Common/CommonExporter");

export interface IApiQueryBuilder {
    getFields(baTypeId: number): Promise<Querybuilder.ICaptionAndAliaseModel[]>;
    getData(model: Querybuilder.IOrderingModel): Promise<any[]>;
}

export function getApiQueryBuilder(): IApiQueryBuilder {
    return apiQueryBuilder;
}

class ApiQueryBuilder implements  IApiQueryBuilder {
    getFields(baTypeId: number): Promise<Querybuilder.ICaptionAndAliaseModel[]> {
        const endpoint = `api/QueryBuilder/GetFields?baTypeId=${baTypeId}`;
        return Common.post(endpoint, null);
    }

    getData(queryParams: Querybuilder.IOrderingModel): Promise<any[]> {
        const endpoint = "api/QueryBuilder/GetData";
        return Common.post(endpoint, queryParams);
    }
}

const apiQueryBuilder = new ApiQueryBuilder();
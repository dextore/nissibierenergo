﻿import Common = require("../Common/CommonExporter");

export class ApiDocuments {
    static getTreeItem(): Promise<Documents.IDocumentTreeItemModel[]> {
        const endpoint = `api/Documents/GetTreeItem`;
        return Common.get(endpoint);
    }

    static getFilteredTreeItems(companyAreaId: number, startDate: Date, endDate: Date): Promise<Documents.IDocumentTreeItemModel[]> {
        const format = webix.Date.dateToStr("%Y-%m-%d", false);
        const endpoint = `api/Documents/GetFilteredTreeItems/${companyAreaId}/${format(startDate)}/${format(endDate)}`;
        return Common.get(endpoint);
    }
}
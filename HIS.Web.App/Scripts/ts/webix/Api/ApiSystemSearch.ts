﻿import Common = require("../Common/CommonExporter");

export interface IApiSystemSearch {
    getSearchLegalPersons(searchType: Common.SystemSearchType, searchText: string): Promise<Entity.IEntityInfoCollectionModel>;
    getFullTextSearchEntities(searchText: string): Promise<Entity.IFullTextSearchEntityInfoCollectionModel>;
}

export function getApiSystemSearch(): IApiSystemSearch {
    return apiSystemSearch;
}

class ApiSystemSearch implements IApiSystemSearch {

    getSearchLegalPersons(searchType: Common.SystemSearchType, searchText: string): Promise<Entity.IEntityInfoCollectionModel> {
        const endpoint = `api/SystemSearch/GetSearchEntities?searchType=${searchType}&searchText=${searchText}`;
        return Common.get(endpoint, null);
    }

    getFullTextSearchEntities(searchText: string): Promise<Entity.IFullTextSearchEntityInfoCollectionModel> {
        const endpoint = `api/SystemSearch/GetFullTextSearchEntities?searchText=${searchText}`;
        return Common.get(endpoint, null);
    }

}

let apiSystemSearch = new ApiSystemSearch();
﻿import Common = require("../Common/CommonExporter");

export interface IApiContractTerms {
    getTerms(contractId: number): Promise<Contracts.IContractTermModel[]>;
    getCurrentTerm(contractId: number, markerId: number): Promise<Contracts.IContractTermModel>;
    getTermHistory(contractId: number, markerId: number): Promise<Contracts.IContractTermModel[]>;
    delete(model: Contracts.IContractTermModel): Promise<Contracts.IContractTermModel>;
    save(model: Contracts.IContractTermModel): Promise<Contracts.IContractTermModel>;
}


export function getApiContractTerms(): IApiContractTerms {
    return apiContractTerms;
}

class ApiContractTerms {

    getTerms(contractId: number): Promise<Contracts.IContractTermModel[]> {
        const endpoint = `api/ContractTerms/GetTerms?contractId=${contractId}`;
        return Common.get(endpoint);
    }

    getCurrentTerm(contractId: number, markerId: number): Promise<Contracts.IContractTermModel> {
        const endpoint = `api/ContractTerms/GetCurrentTerm?contractId=${contractId}&markerId=${markerId}`;
        return Common.get(endpoint);
    }

    getTermHistory(contractId: number, markerId: number): Promise<Contracts.IContractTermModel[]> {
        const endpoint = `api/ContractTerms/GetTermHistory?contractId=${contractId}&markerId=${markerId}`;
        return Common.get(endpoint);
    }

    save(model: Contracts.IContractTermModel): Promise<Contracts.IContractTermModel> {
        const endpoint = `api/ContractTerms/Save`;
        return Common.post(endpoint, model);
    }

    delete(model: Contracts.IContractTermModel): Promise<Contracts.IContractTermModel> {
        const endpoint = `api/ContractTerms/Delete`;
        return Common.post(endpoint, model);
    }
}

let apiContractTerms = new ApiContractTerms();
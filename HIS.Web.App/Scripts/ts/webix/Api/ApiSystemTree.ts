﻿import Common = require("../Common/CommonExporter");

export class ApiSystemTree {

    static getTreeItem(baId: number): Promise<Tree.ISystemTreeItemModel> {
        const endpoint = `api/systemtree/gettreeitem?baId=${baId}`;
        return Common.get(endpoint);
    }

    static getTreeItemChildren(baId: number): Promise<Tree.ISystemTreeItemModel[]> {
        const endpoint = `api/systemtree/gettreeitemchildren?baId=${baId}`;
        return Common.get(endpoint);
    }

}
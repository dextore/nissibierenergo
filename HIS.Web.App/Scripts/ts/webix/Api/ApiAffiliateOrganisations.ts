﻿import Common = require("../Common/CommonExporter");

export class ApiAffiliateOrganisations {
    public static getAffiliateOrganisations(organisactionId: number): Promise<LegalPersons.IAffiliateOrganisationModel[]> {
        const endpoint = `api/AffiliateOrganisations/GetOrganisations/${organisactionId}`;
        return Common.get(endpoint);
    }

    public static getAffiliateOrganisation(affiliateOrganisactionId: number, markerIds: number[]):
        Promise<LegalPersons.IAffiliateOrganisationModel> {
        const endpoint = `api/AffiliateOrganisations/GetOrganisation/${affiliateOrganisactionId}`;
        return Common.post(endpoint, markerIds);
    }

    public static update(model: LegalPersons.IAffiliateOrganisationModel): Promise<LegalPersons.IAffiliateOrganisationModel> {
        const endpoint = `api/AffiliateOrganisations/Save`;
        return Common.post(endpoint, model);
    }
}
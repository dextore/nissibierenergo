﻿import Common = require("../Common/CommonExporter");

export class ApiInventoryObjectsMovementHistory {
    static getFaAssetesList(): Promise<InventoryObjectsMovementHistory.IInventoryAssetsModel[]> {
        return Common.get("api/InventoryObjectsMovementHistory/GetFaAssetesList");
    }

    static getFaAssetesMovementHistory(inventoryAssetId: number): Promise<InventoryObjectsMovementHistory.IInventoryAssetsMovementHistoryModel[]> {
        return Common.get(`api/InventoryObjectsMovementHistory/GetFaAssetesMovementHistory?inventoryAssetId=${inventoryAssetId}`);
    }
}

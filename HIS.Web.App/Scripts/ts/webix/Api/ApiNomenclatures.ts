﻿import Common = require("../Common/CommonExporter");

export interface IApiNomenclatures {
    get(): Promise<Nomenclatures.INomenclatureModel[]>;
    save(nomenclature: Nomenclatures.INomenclatureSaveModel): Promise<Nomenclatures.INomenclatureModel>;
}

export function getApiNomenclatures(): IApiNomenclatures {
    return apiNomenclatures;
}


class ApiNomenclatures implements IApiNomenclatures {

    get(): Promise<Nomenclatures.INomenclatureModel[]> {
        const endpoint = "api/Nomenclature/GetNomenclatures";
        return Common.get(endpoint);
    }

    save(nomenclature: Nomenclatures.INomenclatureSaveModel): Promise<Nomenclatures.INomenclatureModel> {
        const endpoint = "api/nomenclature/Save";
        return Common.post(endpoint, nomenclature);
    }

}

let apiNomenclatures = new ApiNomenclatures;
﻿import Common = require("../Common/CommonExporter");

export interface IApiMainMenu {
    getMainMenu(): Promise<MainMenu.IMenuItemModel[]>;
}

export function getApiMainMenu(): IApiMainMenu {
    return apiMainMenu;
}

class ApiMainMenu implements IApiMainMenu {
    getMainMenu(): Promise<MainMenu.IMenuItemModel[]> {
        const endpoint = `api/MainMenu/GetMainMenu`;
        return Common.get(endpoint);
    }
}

const apiMainMenu = new ApiMainMenu();
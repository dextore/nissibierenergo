﻿import Common = require("../Common/CommonExporter");
import Elm = require("./ApiElemens");

export function getApiRegPoints(): Elm.ApiElements {
    return apiRegPoints;
}

class ApiRegPoints implements Elm.ApiElements {

    get(): Promise<any> {
        const endpoint = "api/RegPoints/Get";
        return Common.get(endpoint);
    } 

    getExt(): Promise<Elements.IElementExtModel> {
        const endpoint = "api/RegPoints/GetRegPoints";
        return Common.get(endpoint);
    }

    getItemExt(regPointId: number): Promise<Elements.IElementExtModel> {
        const endpoint = "api/RegPoints/GetRegPoint";
        const options = { regPointId: regPointId }
        return Common.get(endpoint, options);
    }

    getItem(id: number): Promise<any> {
        const endpoint = `api/RegPoints/GetItem?id=${id}`;
        return Common.get(endpoint);
    } 

    save(model: Elements.IElementRetModel): Promise<Elements.IElementModel> {
        const endpoint = "api/RegPoints/Save";
        return Common.post(endpoint, model);
    }

    getBATypeId(): number {
        //TO DO наверное лучше просить с сервекра!!!
        return 24;
    };
}

let apiRegPoints = new ApiRegPoints();
﻿import Common = require("../Common/CommonExporter");
import Elm = require("./ApiElemens");

export function getApiConPoints(): Elm.ApiElements {
    return apiConPoints;
}

class ApiConPoints implements Elm.ApiElements {

    get(): Promise<any> {
        const endpoint = "api/ConPoints/Get";
        return Common.get(endpoint);
    }

    getExt(): Promise<Elements.IElementExtModel> {
        const endpoint = "api/ConPoints/GetConPoints";
        return Common.get(endpoint);
    }

    getItemExt(conPointId: number): Promise<Elements.IElementExtModel> {
        const endpoint = "api/ConPoints/GetConPoint";
        const options = { conPointId: conPointId };
        return Common.get(endpoint, options);
    }

    getItem(id: number): Promise<any> {
        const endpoint = `api/ConPoints/GetItem?id=${id}`;
        return Common.get(endpoint);
    }

    save(model: Elements.IElementRetModel): Promise<any> {
        const endpoint = "api/ConPoints/Save";
        return Common.post(endpoint, model);
    }

    getBATypeId(): number {
        //TO DO наверное лучше просить с сервекра!!!
        return 23;
    };
}

let apiConPoints = new ApiConPoints();
﻿import Common = require("../Common/CommonExporter");

export class ApiEntities {

    static update(model: Markers.IEntityUpdateModel): Promise<number> {
        const endpoint = `api/entities/update`;
        return Common.post(endpoint, model);
    }

}
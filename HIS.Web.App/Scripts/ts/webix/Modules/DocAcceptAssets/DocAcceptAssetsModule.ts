﻿import Common = require("../../Common/CommonExporter");
import Mngr = require("../ModuleManager");
import ListView = require("./DocAcceptAssetsListView");

export class DocAcceptAssetsModule extends Common.MainTabBase {

    protected static _header = "Акты приема ТМЦ";
    protected static _viewName = "document-accept-assets-module";

    private selectedCompany: Inventory.ILSCompanyAreas;
    private listView: ListView.IDocAcceptAssetsList;

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
        this.listView = ListView.getDocAcceptAssetsList(`${this.viewName}-list`, this);

    }
    systemCompanyHandler(company: Inventory.ILSCompanyAreas) {
        this.listView.changeCompany(company);
    }
    getContent(): object {
        return this.listView.init();
    }
    ready() {
        this.listView.ready();
      
    }
    
   
}


Mngr.ModuleManager.registerModule(DocAcceptAssetsModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new DocAcceptAssetsModule(mainView);
});
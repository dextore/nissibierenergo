﻿import Base = require("../../Components/Markers/MarkerControlBase");
import Helper = require("../../Components/Markers/MarkerHelper");
import Api = require("../../Api/ApiExporter");

export function getInvoiceControl(viewName: string,
    markerInfo: Markers.IMarkerInfoModel,
    companyId: number,
    getExtConfig: (content: any) => any = null, ) {
    return new InvoiceControl(viewName, markerInfo, companyId, getExtConfig);
}


class InvoiceControl extends Base.MarkerControlBase implements Base.IMarkerControl {
    get suggestId() { return `${this.controlId}-suggest-id` }
    get control() {
        return $$(this.controlId) as webix.ui.search;
    };
    private _selectedModel = {} as DocAcceptAssets.IIReceiptInvoiceSelectModel;
    private _initModel = {} as DocAcceptAssets.IIReceiptInvoiceSelectModel;
    private _dataModel = [] as DocAcceptAssets.IIReceiptInvoiceSelectModel[];
    
    constructor(id: string,
        markerInfo: Markers.IMarkerInfoModel,
        private readonly _companyId: number,
        getExtConfig: (content: any) => any) {
        super(id, markerInfo, getExtConfig);
    }

    setValueInternal(value: Markers.IMarkerValueModel, model: { [index: string]: any; }) {
        const itemId = (!value || !value.value) ? null : Number(value.value);
        const itemValue = (!value || !value.displayValue) ? null : value.displayValue;
        if (itemId == null)
            return;

        this._initModel = {
            id: itemId,
            value: itemValue
        } as DocAcceptAssets.IIReceiptInvoiceSelectModel;
        this.isChanged = false;
    }

    getValue(model: { [index: string]: any; }): Markers.IMarkerValueModel {

        return {
            displayValue: (!this._selectedModel) ? "" : this._selectedModel.value,
            markerType: this.markerInfo.markerType,
            MVCAliase: this.markerInfo.name,
            itemId: null,
            endDate: null,
            markerId: this.markerInfo.id,
            isVisible: true,
            isBlocked: false,
            note: null,
            startDate: null,
            value: (!this._selectedModel) ? "" : this._selectedModel.id as any,
            isDeleted: this.isChanged && !this._selectedModel
        };
    }
    protected config() {
        let config = webix.extend({
            view: "combo",
            id: this.controlId,
            label: this.markerInfo.label,
            name: this.markerInfo.name,
            validate: (value) => {
                if (this.markerInfo.isRequired) {
                    if (!value) {
                        return false;
                    }
                }
                return true;
            },
            on: {
                onChange: (newv,oldv) => {
                    if (this.isEmptyObject(this._dataModel))
                        return;
                    if (!newv) {
                        this._selectedModel = null;
                    } else {
                        this._selectedModel = this._dataModel.filter(item => item.id === Number(newv))[0];
                    }
                    this.isChanged = true;
                  
                }
            },
            suggest: {
                view: "gridsuggest",
                textValue: "value",
                id: this.suggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    header: false,
                    columns: [
                        { id: "id", header: "id", hidden: true },
                        { id: "value", header: "value", width: 300 }
                    ],
                    url: {
                        $proxy: true,
                        load: () => {
                            Api.getApiDocAcceptAssets()
                                .getReceiptInvoiceSelect(this._companyId).then(
                                data => {
                                    this._dataModel = data;
                                    const list = ($$(this.suggestId) as webix.ui.gridsuggest).getList() as any;
                                    if (this.isEmptyObject(this._initModel)) {
                                        this._dataModel.sort((left, right) => {
                                            return (left.value < right.value) ? -1 : (left.value > right.value) ? 1 : 0;
                                        });
                                        list.clearAll();
                                        list.parse(data, "json");
                                    } else {
                                        const selected = this._dataModel.filter(item => {
                                            return item.id === this._initModel.id;
                                        })[0];
                                        if (this.isEmptyObject(selected)) {
                                            this._dataModel.push(this._initModel);
                                        }
                                        this._dataModel.sort((left, right) => {
                                            return (left.value < right.value) ? -1 : (left.value > right.value) ? 1 : 0; 
                                        });
                                        list.clearAll();
                                        list.parse(this._dataModel, "json");
                                        this._selectedModel = this._initModel;
                                        this.control.setValue(this._selectedModel.id as any);

                                    }


                                });
                        }
                    }
                }
            }
        }, this.getExtConfig(this), true);
        /*
        if (this.markerInfo.isRequired) {
            config = webix.extend(config, Helper.markerHelper.requiredConfig(this.controlId, webix.rules.isNotEmpty));
        }
        */

        return { cols: [config] }

    }

    private isEmptyObject(obj: object): boolean {

        if (obj == null || obj == undefined) {
            return true;
        }
        if (Object.keys(obj).length === 0) {
            return true;
        }
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }  

}

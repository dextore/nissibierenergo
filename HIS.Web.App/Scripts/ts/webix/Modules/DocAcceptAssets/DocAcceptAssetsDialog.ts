﻿import Base = require("../../Common/DialogBase");
import Common = require("../../Common/CommonExporter");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import Api = require("../../Api/ApiExporter");
import MB = require("../../Components/Markers/MarkerControlBase");
import Component = require("../../Components/ComponentBase");
import MarkerFactory = require("../../Components/Markers/MarkerControlFactory");
import EntityRefMarkerControl = require("../../Components/Markers/EntityRefMarkerControl");
import InvoiceControl = require("./InvoiceControl");


export function getDocAcceptAssetsDialog(viewName: string,
    module: Common.MainTabBase,
    markers: Markers.IMarkerInfoModel[],
    dataModel?: DocAcceptAssets.IIDocAcceptAssetsData): IDocAcceptAssetsDialog {
    return new DocAcceptAssetsDialog(viewName, module, markers, dataModel);
};
export interface IDocAcceptAssetsDialog extends Base.IDialog {
    resultBaId: number;
}

class DocAcceptAssetsDialog extends Base.DialogBase {

    get formId() { return `${this.viewName}-form-id`; }

    constructor(viewName: string,
        private readonly _module: Common.MainTabBase,
        private readonly _entityMarkers: Markers.IMarkerInfoModel[],
        private readonly _dataModel?: DocAcceptAssets.IIDocAcceptAssetsData) {
        super(viewName);      
        if (!this._dataModel || this._dataModel.baId == null) {
            this.isCreate = true;
        }
     
    }
    /// порядок расположения маркеров на форме
    private isCreate = false;
    private baId:number;
    private markerOrder = [1, 17, 135, 6, 134, 19, 136];
    private ignoreMarkers = [45];

    protected headerLabel(): string {
        if (this.isCreate) {
            return "Создание акта приема ТМЦ";
        }
        return "Редактирование акта приема ТМЦ";
    }
    get mainTabView(): Common.IMainTabbarView {
        return this._module.mainTabView;
    }
    get formControl() {
        return $$(this.formId) as webix.ui.form;
    }
    get resultBaId() {
        return this.baId;
    }

    protected fieldsInfo: { [id: string]: Markers.IMarkerInfoModel } = {};
    private markersControls: { [id: number]: MB.IMarkerControl } = {};
    protected get labelWidth() { return 200; }

   
    protected okBtn = Btn.getOkButton(this.viewName,
        () => {
            this.okBtn.button.disable();
            if (this.formControl.validate()) {
                if (!this.isChangeForm() && !this.isCreate) {
                    this.baId = this._dataModel.baId;
                    this.okClose();
                    return;
                }
                const model = this.getFormData() as DocAcceptAssets.IIDocAcceptAssetsData;
                Api.getApiDocAcceptAssets().updateDocAcceptAssets(model).then((data) => {
                    this.baId = data;
                    this.okClose();
                });
            }      
            this.okBtn.button.enable();
        });
    protected cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });

    protected contentConfig() {
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
            ]
        };
    }
    protected windowConfig() {
        return {
            autoheight: true,
        };
    }
    private checkSize() {
        const rect = $$(this.windowId).getNode().getBoundingClientRect();
        const diff = $$(this.formId).getNode().scrollHeight - $$(this.formId).getNode().clientHeight + 10;
        $$(this.windowId).$setSize(rect.width, rect.height + diff);
        ($$(this.windowId) as webix.ui.window).define("height", rect.height + diff);
    }

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._module.disableCompany();
    }

    destroy(): void {
        super.destroy();
        this._module.enableCompany();
    }

    bindModel(): void {

        this._entityMarkers.sort((first: Markers.IMarkerInfoModel, second: Markers.IMarkerInfoModel) => {

            const toEndIndex = 999999;
            let firstIndex = this.markerOrder.indexOf(first.id);
            let secondIndex = this.markerOrder.indexOf(second.id);

            if (firstIndex < 0) { firstIndex = toEndIndex; }
            if (secondIndex < 0) { secondIndex = toEndIndex; }

            if (firstIndex === secondIndex) { return 0; }
            if (firstIndex < secondIndex) {  return -1 }            

            return 1;

        });
        this.loadFieldsInfo();
        this.setFormData();     
      
    }
    private loadFieldsInfo() {

        const invoiceMarkerId = 135;

        this._entityMarkers.forEach(item => {

            if (this.ignoreMarkers.indexOf(item.id) < 0) {
                const marker = this.createMarkerControl(item);
                if (marker) {
                  
                    this.formControl.addView(marker.init());
                    if ("baId" in marker) {
                        (marker as any).baId = this._dataModel.baId;
                    }
                    this.fieldsInfo[item.id] = item;
                    if (item.markerType === 11 && item.id !== invoiceMarkerId) {
                        (marker as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
                        (marker as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this._module);
                    }
                    this.markersControls[item.name] = marker;
                }

            }

          
        });


        ($$(this.formId) as webix.ui.form).addView({
            cols: [
                {},
                this.okBtn.init(),
                this.cancelBtn.init()
            ]
        });
        this.checkSize();
    }
    private createMarkerControl(model: Markers.IMarkerInfoModel): Component.IComponent {

        const elementId = `${this.viewName}-${model.name}-id`;
        const self = this;
        let isDisabled: boolean = false;

        if (this.isCreate) {
            isDisabled = false;
        } else if (model.isBlocked) {
            isDisabled = true;
        }
        /// блокируем маркер выбора организации
        if (model.id === 6) {
            isDisabled = true;
        }
        if (model.id === 135) {
            return InvoiceControl.getInvoiceControl(elementId,
                model,
                this._dataModel.caId,
                (self) => {
                    return { labelWidth: this.labelWidth, disabled: isDisabled }
                });
        }
        return MarkerFactory.MarkerControlfactory.getControl(elementId,
            model,
            (self) => {
                return { labelWidth: this.labelWidth, disabled: isDisabled }
            });
    }
    private getFormData(): DocAcceptAssets.IIDocAcceptAssetsData {

        const result = {
            baId: this.isCreate ? null : this._dataModel.baId,
            baTypeId: this.isCreate ? null : this._dataModel.baTypeId, 
            number: this.markersControls["Name"].getValue(this.fieldsInfo).value,
            docDate: this.markersControls["DocDate"].getValue(this.fieldsInfo).value,
            note: this.markersControls["Note"].getValue(this.fieldsInfo).value,
            evidence: this.markersControls["Evidence"].getValue(this.fieldsInfo).value,
            caId: this.markersControls["CompanyArea"].getValue(this.fieldsInfo).value,
            lsId: this.markersControls["LegalSubject"].getValue(this.fieldsInfo).value,
            whId: this.markersControls["WHWarehouses"].getValue(this.fieldsInfo).value,
            riId: this.markersControls["RIReceiptInvoices"].getValue(this.fieldsInfo).value,

        } as DocAcceptAssets.IIDocAcceptAssetsData;
        /// добавить добавление маркеров

        return result;
    }
    private setFormData() {

        if (!this._dataModel) {
            return;
        }
        let model: { [id: string]: any } = {};

        let info = this.markersControls["Name"].markerInfo;
        this.markersControls["Name"].setInitValue(this.getMarkerValue(info, this._dataModel.number as any, null), model);

        info = this.markersControls["DocDate"].markerInfo;
        this.markersControls["DocDate"].setInitValue(this.getMarkerValue(info, this._dataModel.docDate as any, null), model);

        info = this.markersControls["Evidence"].markerInfo;
        this.markersControls["Evidence"].setInitValue(this.getMarkerValue(info, this._dataModel.evidence as any, null), model);
        
        info = this.markersControls["Note"].markerInfo;
        this.markersControls["Note"].setInitValue(this.getMarkerValue(info, this._dataModel.note as any, null), model);
        
        info = this.markersControls["CompanyArea"].markerInfo;
        this.markersControls["CompanyArea"].setInitValue(this.getMarkerValue(info, this._dataModel.caId as any, this._dataModel.caName), model);

        info = this.markersControls["LegalSubject"].markerInfo;
        this.markersControls["LegalSubject"].setInitValue(this.getMarkerValue(info, this._dataModel.lsId as any, this._dataModel.lsName), model);

        info = this.markersControls["WHWarehouses"].markerInfo;
        this.markersControls["WHWarehouses"].setInitValue(this.getMarkerValue(info, this._dataModel.whId as any, this._dataModel.whName), model);

        info = this.markersControls["RIReceiptInvoices"].markerInfo;
        this.markersControls["RIReceiptInvoices"].setInitValue(this.getMarkerValue(info, this._dataModel.riId as any, this._dataModel.riName), model);
        
        this.formControl.setValues(model);
        this.formControl.clearValidation();
     

    }
    private getMarkerValue(info: Markers.IMarkerInfoModel, value: string, displayValue: string): Markers.IMarkerValueModel {
        return {
            displayValue: displayValue,
            itemId: null,
            endDate: null,
            markerId: info.id,
            markerType: info.markerType,
            MVCAliase: info.name,
            note: null,
            isVisible: true,
            isBlocked: false,
            startDate: null,
            value: value,
            isDeleted: false
        }
    }
   
    private isChangeForm(): boolean {
        let result: boolean = false;
        for (let key in this.markersControls) {
            
            if (this.markersControls.hasOwnProperty(key)) {
                if (this.markersControls[key].isChanged === true) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }
}

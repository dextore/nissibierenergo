﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Common/DialogBase");
import Common = require("../../Common/CommonExporter");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import MB = require("../../Components/Markers/MarkerControlBase");
import MarkerFactory = require("../../Components/Markers/MarkerControlFactory");
import EntityRefMarkerControl = require("../../Components/Markers/EntityRefMarkerControl");

export function getDocAcceptAssetsRowDialog(viewName: string,
    module: Common.MainTabBase,
    dataModel: DocAcceptAssets.IIDocAcceptAssetsRows,
    hasInvoice:boolean): IDocAcceptAssetsRowDialog {
    return new DocAcceptAssetsRowDialog(viewName, module, dataModel,hasInvoice);
};
export interface IDocAcceptAssetsRowDialog extends Base.IDialog {
    resultDataModel: DocAcceptAssets.IIDocAcceptAssetsRows;
}

class DocAcceptAssetsRowDialog extends Base.DialogBase {

    private isCreate = false;
    private isChanged = false;
    private inventoryCotrol: MB.IMarkerControl;
    private inventoryInfoModel: Markers.IMarkerInfoModel;


    get formId() { return `${this.viewName}-form-id`; }
    get relaseDateId() { return `${this.formId}-release-date-id`; }
    get checkDateId() { return `${this.formId}-check-date-id`; }
    get inventorySuggestId() { return `${this.formId}-inventory-suggest-id`; }
    get resultDataModel() { return this._dataModel; }
    get mainTabView(): Common.IMainTabbarView {
        return this._module.mainTabView;
    }
    get formControl() {
        return $$(this.formId) as webix.ui.form;
    }
    private isChangeForm(): boolean {

        if (this.isChanged || this.isCreate) {
            return true;
        }
        if (!this._hasInvoice && this.inventoryCotrol.isChanged) {
            return true;
        }
        return false;
    }

    constructor(viewName: string,
        private readonly _module: Common.MainTabBase,
        private _dataModel: DocAcceptAssets.IIDocAcceptAssetsRows,
        private readonly _hasInvoice: boolean) {
        super(viewName);
        if (!this._dataModel.recId) {
            this.isCreate = true;
        }

    }

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._module.disableCompany();
    }

    destroy(): void {
        super.destroy();
        this._module.enableCompany();
    }
  

  

    protected headerLabel(): string {
        if (!this._dataModel.recId) {
            return "Создание спецификации акта приема ТМЦ";
        }
        return "Редактирование спецификации акта приема ТМЦ";
    }
  
    protected okBtn = Btn.getOkButton(this.viewName,
        () => {
            this.okBtn.button.disable();
            if (this.formControl.validate()) {

                if (!this.isChangeForm()) {
                    this.okClose();
                    return false;
                }
                const saveModel = this.getFormData();
                if (!saveModel) {
                    this.okBtn.button.enable();
                    return;
                }

                Api.getApiDocAcceptAssets().updateDocAcceptAssetRow(saveModel).then((result) => {
                    this._dataModel = result;
                    this.okClose();
                });
           
            }
            this.okBtn.button.enable();
        });

    protected cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });

    protected contentConfig() {

        const self = this;
     
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elementsConfig: {
                labelPosition: "left",
                labelAlign: "left",
                labelWidth: 200,
                minWidth: 350,
                animate: false
            },
            rules: {
                inventoryNumber: webix.rules.isNotEmpty
            },
            elements: [
                this.getInventoryControl(),
                {
                    view: "text",
                    name: "inventoryNumber",
                    label: "Инвентарый номер",
                    required: true,
                    on: {
                        onChange: () => {
                            self.isChanged = true;
                        },
                    }
                },
                {
                    view: "text",
                    name: "serialNumber",
                    label: "Заводской(серийный) номер",
                    on: {
                        onChange: () => {
                            self.isChanged = true;
                        },
                    }
                },
                {
                    id: self.relaseDateId,
                    view: "datepickerExtended",
                    name: "releaseDate",
                    label: "Дата выпуска",
                    pattern: { mask: "##.##.####", allow: /[0-9]/g },
                    validate: () => {
                        if (!($$(self.relaseDateId) as webix.ui.datepickerExtended).getValidateTextValue()) {
                            return false;
                        }
                        return true;
                    },
                    on: {
                        onChange: () => {
                            self.isChanged = true;
                            ($$(self.relaseDateId) as webix.ui.datepickerExtended).validate();
                        },
                        onBlur: () => {
                            ($$(self.relaseDateId) as webix.ui.datepickerExtended).validate();
                        }
                    }
                },
                {
                    id: self.checkDateId,
                    view: "datepickerExtended",
                    name: "checkDate",
                    label: "Дата последней гос.поверки",
                    pattern: { mask: "##.##.####", allow: /[0-9]/g },
                    validate: () => {
                        if (!($$(self.checkDateId) as webix.ui.datepickerExtended).getValidateTextValue()) {
                            return false;
                        }
                        return true;
                    },
                    on: {
                        onChange: () => {
                            self.isChanged = true;
                            ($$(self.checkDateId) as webix.ui.datepickerExtended).validate();
                        },
                        onBlur: () => {
                            ($$(self.checkDateId) as webix.ui.datepickerExtended).validate();
                        }
                    }
                },
                {
                    view: "text",
                    name: "barCode",
                    label: "Штрих-код",
                    on: {
                        onChange: () => {
                            self.isChanged = true;
                        },
                    }
                }
            ]
        };
    }
    protected windowConfig() {
        return {
            autoheight: true,
        };
    }
    private checkSize() {
        const rect = $$(this.windowId).getNode().getBoundingClientRect();
        const diff = $$(this.formId).getNode().scrollHeight - $$(this.formId).getNode().clientHeight + 10;
        $$(this.windowId).$setSize(rect.width, rect.height + diff);
        ($$(this.windowId) as webix.ui.window).define("height", rect.height + diff);
    }
    bindModel(): void {
        this.formControl.addView({
            cols: [
                {},
                this.okBtn.init(),
                this.cancelBtn.init()
            ]
        });
        this.checkSize();
        this.setFormData();
    }
    private getInventoryControl(): object {
        const self = this;
        if (this._hasInvoice) {
            return {
                view: "combo",
                name: "inventoryId",
                label: "Номенклатурная единица",
                required: true,
                on: {
                    onChange: () => {
                        self.isChanged = true;
                    },
                },
                suggest: {
                    view: "gridsuggest",
                    textValue: "value",
                    id: self.inventorySuggestId,
                    body: {
                        yCount: 10,
                        scroll: true,
                        autoheight: false,
                        header: true,
                        columns: [
                            { id: "id", header: "id", hidden: true },
                            { id: "value", header: "Номенклатурная единица", width:250 },
                            { id: "quantity", header: "Количество", width: 80  }
                        ],
                        url: {
                            $proxy: true,
                            load: () => {

                                Api.getApiDocAcceptAssets()
                                    .getReceiptInvoiceInventSelect(this._dataModel.docAcceptId).then(
                                        data => {
                                            const list =
                                                ($$(this.inventorySuggestId) as webix.ui.gridsuggest).getList() as any;
                                            list.clearAll();
                                            list.parse(data, "json");

                                        });


                            }
                        }
                    }
                }
            };
        }
        return this.getInventoryMarkerControl().init();
    }
    private getInventoryMarkerControl(): MB.IMarkerControl {
        this.inventoryInfoModel = {
            id: 1000,
            catalogImplementTypeName: "",
            implementTypeField: "",
            implementTypeName: "",
            isBlocked: false,
            isCollectible: false,
            isImplemented: true,
            isOptional: false,
            isPeriodic: false,
            isRequired: true,
            label: "Номенклатурная единица",
            markerType: HIS.Models.Layer.Models.Markers.MarkerType.MtEntityRef,
            name: "inventoryName",
            refBaTypeId: 2,
            list: []
        } as Markers.IMarkerInfoModel;

        this.inventoryCotrol = MarkerFactory.MarkerControlfactory.getControl(this.viewName, this.inventoryInfoModel,
            (content) => {
                return {
                    label: "Номенклатурная единица"
                }
            });
        (this.inventoryCotrol as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
        (this.inventoryCotrol as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this._module);
        return this.inventoryCotrol;
    }
    private getFormData(): DocAcceptAssets.IIDocAcceptAssetsRows {
        const model = this.formControl.getValues() as DocAcceptAssets.IIDocAcceptAssetsRows;
        model.inventoryName = "";
        if (!this._hasInvoice) {
            const inventoryValue = this.inventoryCotrol.getValue(this.inventoryInfoModel);
            model.inventoryId = (!inventoryValue) ? null : Number(inventoryValue.value);
            model.inventoryName = (!inventoryValue) ? "" : inventoryValue.displayValue;
        }
        model.employeeId = null;
        model.docAcceptId = this._dataModel.docAcceptId;
        model.faId = this._dataModel.faId;
        model.recId = this._dataModel.recId;
        return model;
    }
    private setFormData() {
        let model: { [id: string]: any } = this._dataModel;
        if (!this._hasInvoice) {
            this.inventoryCotrol.setInitValue(getMarkerValue(this.inventoryInfoModel, this._dataModel.inventoryId as any, this._dataModel.inventoryName), model);
        }
        this.formControl.setValues(model);
        this.formControl.clearValidation();
        this.isChanged = false;
        if (!this._hasInvoice) {
            this.inventoryCotrol.isChanged = false;
        }
        function getMarkerValue(info: Markers.IMarkerInfoModel, value: string, displayValue: string): Markers.IMarkerValueModel {
            return {
                displayValue: displayValue,
                itemId: null,
                endDate: null,
                markerId: info.id,
                markerType: info.markerType,
                MVCAliase: info.name,
                note: null,
                isVisible: true,
                isBlocked: false,
                startDate: null,
                value: value,
                isDeleted: false
            }
        }


    }

}

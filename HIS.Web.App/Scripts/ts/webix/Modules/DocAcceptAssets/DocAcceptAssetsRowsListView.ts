﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Components/ComponentBase");
import Tab = require("../../MainView/Tabs/MainTabBase");
import Common = require("../../Common/CommonExporter");
import ToolButtons = require("../../Components/Buttons/ToolBarButtons");
import Dialog = require("./DocAcceptAssetsRowsDialog");
import Pager = require("../../Components/Pager/pager");

export interface IDocAcceptAssetsRowsListview extends Base.IComponent {
    ready();
    reloadByModel(model: IDocAcceptAssetsRowsSetModel);
    controlEvent: Common.Event<boolean>;
}

export interface IDocAcceptAssetsRowsSetModel {
    baId: number;
    stateId: number;
    hasInvoice: boolean;
}

export function getDocAcceptAssetsRowsListview(_viewName: string, module: Common.MainTabBase) {
    return new DocAcceptAssetsRowsListview(`${_viewName}-doc-accept-assets-row`, module);
}

export class DocAcceptAssetsRowsListview extends Base.ComponentBase implements IDocAcceptAssetsRowsListview {

    private get tableId() { return `${this._viewName}-datatable-id`; }
    private get pagerId() { return `${this._viewName}-pager-id`; }
    protected get toolBarId() { return `${this._viewName}-toolbar-id`; }
    static readonly _createState = 1;
    private _setModel: IDocAcceptAssetsRowsSetModel;

    get tableControl() {
        return $$(this.tableId) as webix.ui.datatable;
    }
    public reloadByModel(model: IDocAcceptAssetsRowsSetModel): void {
        this._setModel = model;
        this.resetButtons();
        this.tableControl.load(this.tableControl.config.url);
    }
    private _controlEvent = new Common.Event<boolean>();
    get controlEvent(): Common.Event<boolean> {
        return this._controlEvent;
    }



    protected toolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}-tooldar`, "Добавить", this.addRow.bind(this)),
        edit: ToolButtons.getEditBtn(`${this._viewName}-tooldar`, "Изменить", this.edit.bind(this)),
        remove: ToolButtons.getRemoveBtn(`${this._viewName}-tooldar`, "Удалить", this.removeRow.bind(this))
    }

    constructor(private readonly _viewName: string, private readonly _module: Common.MainTabBase) {
        super();      
    }
    init() {
        const pager = Pager.getDataTablePager(this.pagerId);
        return {
            rows: [
                this.initToolbar(),
                this.initTable(),
                pager.init()
            ]

        };
    }
    ready() {
        webix.extend(this.tableControl, webix.ProgressBar);   
    }
    get hasSelected() {
        return (this.tableControl.getSelectedId(false, true) > 0);
    }
    private addRow() {
        const model = {
            docAcceptId: this._setModel.baId,
            recId: null
        } as DocAcceptAssets.IIDocAcceptAssetsRows;

        const dialog = Dialog.getDocAcceptAssetsRowDialog(`${this._viewName}-dialog`, this._module, model,this._setModel.hasInvoice);
        dialog.showModalContent(this._module,
            () => {
                const newRecord = dialog.resultDataModel;
                this.tableControl.add(newRecord);
                this.changeCountRow();
                dialog.close();
            });
    }
    private edit() {
        const selected = this.tableControl.getSelectedItem() as DocAcceptAssets.IIDocAcceptAssetsRows;
        const selectedId = this.tableControl.getSelectedId(false, true);
        if (!selected) {
            webix.message("Необходимо выбрать элемент спецификации для редактирования","error");
            return;
        }
        Api.getApiDocAcceptAssets().getDocAcceptAssetsRow(this._setModel.baId, selected.recId).then((data) => {

            if (data == null) {
                this.tableControl.remove(selectedId);
                webix.message("Запись не найдена","message");
            }
            const dialog = Dialog.getDocAcceptAssetsRowDialog(`${this._viewName}-dialog`, this._module, data, this._setModel.hasInvoice);
            dialog.showModalContent(this._module,
                () => {
                    this.tableControl.updateItem(selectedId , dialog.resultDataModel);
                    dialog.close();
                });

        });
    }

    private changeCountRow() {
        if (this.tableControl.count() < 2) {
            this._controlEvent.trigger(webix.copy(true), this);
        }
    }

 
    private removeRow() {
        const selected = this.tableControl.getSelectedItem();
        if (!selected) {
            return;
        }
        selected.docAcceptId = this._setModel.baId;
        webix.alert({
            title: "Подтверждение",
            type: "confirm-warning",
            text: "Запись будет удалена?",
            ok: "Да",
            cancel: "Нет",
            callback: (result) => {
                if (!result) {
                    return;
                }
                Api.getApiDocAcceptAssets().deleteDocAcceptAssetRow(selected).then(result => {
                    if (result) {
                        this.tableControl.remove(selected.id);
                        this.changeCountRow();
                    }
                })
            }
        });
    }
    private resetButtons() {

        this.toolBar.add.button.disable();
        this.toolBar.remove.button.disable();
        this.toolBar.edit.button.disable();

        if (!this._setModel) { 
            return;
        }
        if (this._setModel.stateId !== DocAcceptAssetsRowsListview._createState) {
            return;
        }
        this.toolBar.add.button.enable();
        if (this.hasSelected) {
            this.toolBar.remove.button.enable()
            this.toolBar.edit.button.enable();

        }
    }
    private initToolbar() {
        return {
            view: "toolbar",
            id: this.toolBarId,
            paddingY: 1,
            height: 30,
            elements: [
                {
                    view: "label", label: "Спецификация акта приема ТМЦ"
                },
                this.toolBar.add.init(),
                this.toolBar.edit.init(),
                this.toolBar.remove.init()            
            ]
        }
    }
    private initTable() {
        const self = this;
        return {
            view: "datatable",
            id: this.tableId,
            select: "row",
            scroll: "y",
            resizeColumn: true,
            pager: this.pagerId,
            datafetch: 5,
            on: {
                onSelectChange: () => {
                    self.resetButtons();
                }
            },         
            footer: true,
            columns: [
                {
                    id: "recId",
                    width: 80,
                    header: ["Id", { content: "textFilter" }],
                    footer: { text: "Итого:", colspan: 3 }
                },
                {
                    id: "inventoryId",
                    hidden: true
                },
                {
                    id: "inventoryName",
                    header: ["Номенклатурная единица", { content: "textFilter" }],
                    sort: "string",
                    adjust: true,
                    fillspace: true
                },
                {
                    id: "inventoryNumber",
                    header: ["Инвентарый номер", { content: "textFilter" }],
                    sort: "string",
                    editor: "text",
                    adjust: true,
                    fillspace: true,
                    footer: {
                        colspan: 6,
                        content: "countTableRows",
                    }
                },
                {
                    id: "serialNumber",
                    header: ["Заводской (серийный) номер", { content: "textFilter" }],
                    sort: "string",
                    editor: "text",
                    adjust: true,
                    fillspace: true
                },
                {
                    id: "releaseDate",
                    header: [
                        "Дата выпуска",
                        { content: "datepickerFilter" }
                    ],
                    sort: "date",
                    width: 150,
                    format: webix.i18n.dateFormatStr,
                    adjust: true,
                    fillspace: true
                },
                {
                    id: "checkDate",
                    header: [
                        "Дата последней гос.поверки",
                        { content: "datepickerFilter" }
                    ],
                    sort: "date",
                    width: 150,
                    format: webix.i18n.dateFormatStr,
                    adjust: true,
                    fillspace: true
                },
                {
                    id: "barCode",
                    header: "Штрих-код",
                    editor: "text",
                    adjust: true,
                    fillspace: true
                },
                {
                    id: "employeeId",
                    header: "Сотрудник организации",
                    adjust: true,
                    fillspace: true
                }
            ],
            url: {
                $proxy: true,
                load: function (view, callback, params) {

                    self.tableControl.clearAll();
                    if ((self.tableControl as any).showProgress)
                        (self.tableControl as any).showProgress();

                    if (self.isEmptyObject(self._setModel)) {
                        (webix.ajax as any).$callback(view, callback, []);
                        if ((self.tableControl as any).hideProgress)
                            (self.tableControl as any).hideProgress();
                        return;
                    }
                    Api.getApiDocAcceptAssets().getDocAcceptAssetsRows(self._setModel.baId).then((data: DocAcceptAssets.IIDocAcceptAssetsRows[]) => {
                        (webix.ajax as any).$callback(view, callback, data);
                        if ((self.tableControl as any).hideProgress)
                            (self.tableControl as any).hideProgress();
                    }).catch((error) => {
                        (webix.ajax as any).$callback(view, callback, []);
                        if ((self.tableControl as any).hideProgress)
                            (self.tableControl as any).hideProgress();
                    });


                

                  //  if ((self.tableControl as any).hideProgress)
                  //      (self.tableControl as any).hideProgress();

                }
            }

        };

    }
   

    private isEmptyObject(obj: object): boolean {

        if (obj == null || obj == undefined) {
            return true;
        }
        if (Object.keys(obj).length === 0) {
            return true;
        }
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }  


}



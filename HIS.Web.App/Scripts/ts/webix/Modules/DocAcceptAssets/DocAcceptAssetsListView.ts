﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Components/ComponentBase");
import ToolButtons = require("../../Components/Buttons/ToolBarButtons");
import Common = require("../../Common/CommonExporter");
import StateEntity = require("../../Components/Controls/EntityOperationStateControl");
import Specification = require("./DocAcceptAssetsRowsListView");
import Dialog = require("./DocAcceptAssetsDialog");
import TransactionDialog = require("../Documents/InventoryTransactions/InventoryTransactionsDlg");
import Warning = require("../../Components/Messages/Warning");
import DataTable = require("../../Components/QueryBuilderListBase/QueryBuilderListBase");
import IQueryBuilderListSetParameters = DataTable.IQueryBuilderListSetParameters;


export interface IDocAcceptAssetsList extends Base.IComponent {
    changeCompany(company: Inventory.ILSCompanyAreas);
    ready();
}

export function getDocAcceptAssetsList(_viewName: string, module: Common.MainTabBase) {
    return new DocAcceptAssetsList(_viewName, module);
}

export class DocAcceptAssetsList extends Base.ComponentBase implements IDocAcceptAssetsList {

    private readonly _entityAlias = "FADocAcceptFAssets";
    private _entityInfo = Common.entityTypes().getByAliase(this._entityAlias).entityInfo;
    private _selectedCompany: Inventory.ILSCompanyAreas;
    private _operationState: StateEntity.IEntityOperationStateControl;
    private _specification: Specification.IDocAcceptAssetsRowsListview;
    private _specificationModel = {} as Specification.IDocAcceptAssetsRowsSetModel;

    // table control with parameters
    private _tableBuilderControl: DataTable.IQueryBuilderList;
    private _showBaId: boolean = false;
    private _aliasesWithoutFilter = ["CompanyArea"];
    private _aliasesOrder = [
        "BAId",
        "Name",
        "DocDate",
        "RIReceiptInvoices",
        "RIReceiptInvoices//Number",
        "LegalSubject",
        "WHWarehouses",
        "Evidence",
        "Note",
        "State",
        "Creator",
        "CompanyArea"];

    private get tableId() { return `${this._viewName}-datatable-id`; }
  
    protected toolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}-tooldar`, "Добавить", this.createEntity.bind(this)),
        edit: ToolButtons.getEditBtn(`${this._viewName}-tooldar`, "Изменить", this.editEntity.bind(this)),
        transaction: ToolButtons.getInventoryTransactionBtn(`${this._viewName}-tooldar`, "Проводки по документу", this.showTransaction.bind(this))
    }
    get tableControl() {
        return $$(this.tableId) as webix.ui.datatable;
    }
    constructor(private readonly _viewName: string, private readonly _module: Common.MainTabBase) {
        super();
        this._specification = Specification.getDocAcceptAssetsRowsListview(_viewName, _module);
        this.initSelectedCompany();
        this.initOperationState();
        this.initDataTableControl();
        this._module.subscribe(this._specification.controlEvent.subscribe(this.reloadStateBySpecification.bind(this),this._module)) ;
    }
    private initOperationState(): void {
        this._operationState = StateEntity.getEntityOperationStateControl(`${this._viewName}-operation-state`,
            "",
            () => {
                return true;
            });
        this.subscribe(this._operationState.statusChangedEvent.subscribe((data) => {
            
            const stateId = this._operationState.state.stateId;
            const baId = this._operationState.state.baId;
            const selectId = this.tableControl.getSelectedId(false, true);
       
            switch (data) {
                case StateEntity.StateChangeAction.Changed:
                    this.showErrors(stateId,baId);
                    this.updateTableRecord(baId, selectId);
                     break;
                case StateEntity.StateChangeAction.Canceled:
                    this.updateTableRecord(baId, selectId);
                    break;
                default:
                    this._specification.reloadByModel(this._specificationModel);
                    break;
            }
            this.resetButtons();
       
            
        }, this));
        this._operationState.addButtons(this.toolBar.transaction, this.toolBar.add, this.toolBar.edit);
    }
    private showErrors(stateId: number, baId: number) {
        if (stateId === 5) {
            Warning.showForEntity(this._viewName,baId);
        }
    }
    private reloadStateBySpecification() {

        const selected = this.tableControl.getSelectedItem() as any;
        if (!selected) {
            return;
        }
        this.setOperationState(selected[this._tableBuilderControl.getBAIdAliase()]);
    }
    private setOperationState(baId: number) {
        this._specificationModel = {} as Specification.IDocAcceptAssetsRowsSetModel;
        const createdState = 1;
        if (baId == null) {
            this._operationState.baId = null;
            return;
        }
        Api.getApiDocAcceptAssets().getDocAcceptAssetsData(baId).then((result) => {
            if (result.stateId === createdState) {
                let forbiddenState = 5;
                if (result.hasSpecification) {
                    forbiddenState = 255;
                }
                (this._operationState as StateEntity.IEntityOperationStateControlFilterOperation).filterAllowOperation = (item) => item.id !== forbiddenState;
            }
            this._specificationModel = {
                baId: result.baId,
                stateId: result.stateId,
                hasInvoice: (parseInt(result.riId as any) > 0)?true:false
            }
            this._operationState.baId = baId;
        });

    }
    private updateTableRecord(baId: number, recId?: number) {

        this._tableBuilderControl.getRowDataByBaId(baId).then(data => {
            const row = data.data[0];
            if (!recId) {
                this.tableControl.add(row, 0);
                this._tableBuilderControl.selectFirstRow();
            } else {
                this.tableControl.updateItem(recId, row);
            }
            this._specificationModel.baId = baId;
            this._specificationModel.stateId = this._operationState.state.stateId;
            this._specification.reloadByModel(this._specificationModel);


        });
    }
    private showTransaction() {
        const selected = this.tableControl.getSelectedItem();
        if (!selected) {
            return;
        }
        const baId = selected[this._tableBuilderControl.getBAIdAliase()] as number;
        const docText = `<b>№ ${selected[this._tableBuilderControl.getFieldAlias(1)]} от
                         ${webix.i18n.dateFormatStr(new Date(selected[this._tableBuilderControl.getFieldAlias(17)]))}</b>`;
        const dialog = TransactionDialog.getInventoryTransactionsDlg(`${this._viewName}-tran-dialog`,
            baId, docText);
        dialog.showModalContent(this._module, () => {
            dialog.close();
        });

    }
    private createEntity() {

        const model = {
            baId: null,
            docDate: new Date(),
            caId: this._selectedCompany.id,
            caName: this._selectedCompany.name
        } as DocAcceptAssets.IIDocAcceptAssetsData;

        const dialog = Dialog.getDocAcceptAssetsDialog(`${this._viewName}-entity-dialog`,
            this._module,
            this._entityInfo.markersInfo,
            model);

        dialog.showModalContent(this._module, () => {
            this.updateTableRecord(dialog.resultBaId);
            dialog.close();
        });

    }
    private editEntity() {
        const selected = this.tableControl.getSelectedItem();
        const selectId = this.tableControl.getSelectedId(false, true);
        if (!selected) {
            webix.message("Необходимо выбрать элемент для редактирования", "error");
            return;
        }
        const baId = selected[this._tableBuilderControl.getBAIdAliase()] as number;

        Api.getApiDocAcceptAssets().getDocAcceptAssetsData(baId).then((data) => {

            const dialog = Dialog.getDocAcceptAssetsDialog(`${this._viewName}-entity-dialog`,
                this._module,
                this._entityInfo.markersInfo,
                data);

            dialog.showModalContent(this._module, () => {
                this.updateTableRecord(dialog.resultBaId, selectId);
                dialog.close();
            });
        });

    }
    private resetButtons() {

        this.toolBar.add.button.enable();
        this.toolBar.edit.button.disable();
        this.toolBar.transaction.button.disable();

        const selected = this.tableControl.getSelectedItem() as any;
        if (!selected || !this._operationState.state) {
            return;
        }

        this.toolBar.transaction.button.enable();
        if (this._operationState.state.stateId === 1) {
            this.toolBar.edit.button.enable();
        }

    }
    private initSelectedCompany() {
        this._selectedCompany = this._module.mainTabView.getSelectedCompany();
    }
    public changeCompany(selectedCompany: Inventory.ILSCompanyAreas) {
        this._selectedCompany = selectedCompany;
        const companyField = this._tableBuilderControl.getCompanyAliase();
        if (companyField != "") {
            this._tableBuilderControl.setFilteringFields([{
                field: companyField,
                filterOperator: 0,
                value: this._selectedCompany.id
            }]);
            this._tableBuilderControl.reload();
        }
    }
    private initDataTableControl() {

        const queryBuilderParam = {
            showBaId: this._showBaId,
            tooltip: true,
            entityInfo: this._entityInfo,
            aliasesOrder: this._aliasesOrder,
            aliasesWithoutFilter: this._aliasesWithoutFilter,

        } as IQueryBuilderListSetParameters;
        this._tableBuilderControl = DataTable.getQueryBuilderList(this.tableId, queryBuilderParam);
        const companyField = this._tableBuilderControl.getCompanyAliase();
        if (companyField != "") {
            this._tableBuilderControl.setFilteringFields([{
                field: companyField,
                filterOperator: 0,
                value: this._selectedCompany.id
            }]);

        }

    }
  
    ready() {
        this.resetButtons();
        this._specification.ready();
        this._tableBuilderControl.ready();
        this.tableControl.attachEvent("onSelectChange", this.onSelectChangeTable.bind(this));
    }

    private onSelectChangeTable() {
        const selected = this.tableControl.getSelectedItem() as any;
        if (!selected) {
            this._specificationModel = {} as Specification.IDocAcceptAssetsRowsSetModel;
            this._specification.reloadByModel(this._specificationModel);
            return;
        }
        this.setOperationState(selected[this._tableBuilderControl.getBAIdAliase()]);
    }

    init() {

        return {
            rows: [
                this._operationState.init(),
                this._tableBuilderControl.init(),
                { view: "resizer" },
                this._specification.init()
            ]
        };

    }


}
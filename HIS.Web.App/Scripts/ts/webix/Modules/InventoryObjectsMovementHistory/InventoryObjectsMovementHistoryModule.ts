﻿import Common = require("../../Common/CommonExporter");
import Mngr = require("../ModuleManager");
import Reg = require("../RegisterModules");
import Form = require("../../Components/Forms/InventoryObjectsMovementHistory/InventoryObjectsMovementHistoryList");

export class InventoryObjectsMovementHistoryModule extends Common.MainTabBase {

    protected static _viewName = Form.modulesName;
    protected static _header = Reg.Register.modules[Form.modulesName].header;

    private _form: Form.IInventoryObjectsMovementHistoryList;

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
        this._form = Form.getInventoryObjectsMovementHistoryList(this);
    }

    systemCompanyHandler(company: Inventory.ILSCompanyAreas): void {
        this._form.selectedCompany = company;
    }

    getContent() {
        return this._form.init();
    }

    ready(): void {
        this._form.ready();
    }
}

Mngr.ModuleManager.registerModule(InventoryObjectsMovementHistoryModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new InventoryObjectsMovementHistoryModule(mainView);
});

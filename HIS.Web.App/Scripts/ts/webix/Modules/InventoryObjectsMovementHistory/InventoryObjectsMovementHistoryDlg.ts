﻿import Base = require("../../Common/DialogBase");
import List = require("./InventoryObjectsMovementHistoryListForDlg");
import Btn = require("../../Components/Buttons/ButtonsExporter");

export interface IInventoryObjectsMovementHistoryDlg extends Base.IDialog {
    
}

export function getInventoryObjectsMovementHistoryDlg(viewName: string,
    baId: number,
    header: string): InventoryObjectsMovementHistoryDlg {
    return new InventoryObjectsMovementHistoryDlg(viewName, header, baId);
};

class InventoryObjectsMovementHistoryDlg extends Base.DialogBase {
    private _closeBtn = Btn.getCloseButton(this.viewName, () => { this.okClose(); });
    private _list;
    headerLabel(): string {
        return `История движения инвентарных объектов по: ${this._header}`;
    }

    contentConfig() {
        return {
            rows: [
                this._list.init(),
                {
                    view: "toolbar",
                    height: 40,
                    elements: [{}, this._closeBtn.init()]
                }
            ]
        };
    }

    protected windowConfig() {
        return {
            autoheight: true,
            padding: 50,
        };
    }

    constructor(viewName: string, private readonly _header: string, baId: number) {
        super(viewName);

        this._list = List.getInventoryTransactionListView(`${this.viewName}-list`, baId);
    }
}

﻿import Base = require("../../Components/ComponentBase");
import Api = require("../../Api/ApiExporter");


export interface IInventoryObjectsMovementHistoryListForDlg extends Base.IComponent {
    
}

export function getInventoryTransactionListView(viewName: string, baId: number): IInventoryObjectsMovementHistoryListForDlg {
    return new InventoryObjectsMovementHistoryListForDlg(viewName, baId);
}

class InventoryObjectsMovementHistoryListForDlg extends Base.ComponentBase implements IInventoryObjectsMovementHistoryListForDlg{
    constructor(private readonly _viewName: string, private readonly _baId: number) {
        super(); 
    }

    get viewName() {
        return this._viewName;
    }

    get dataTableId() {
        return `${this._viewName}-datatable-id`;
    }

    get dataTableControl() {
        return $$(this.dataTableId) as webix.ui.datatable;
    }

    init() {
        return {
            view: "datatable",
            id: this.dataTableId,
            select: "row",
            resizeColumn: true,
            columns: [
                {
                    id: "recId",
                    header: "№",
                    width: 50
                },
                {
                    id: "inventoryAssetNumber",
                    header: "Инвентарный объект",
                    minWidth: 150,
                    template: "<strong>#inventoryAssetNumber#</strong>",
                    fillspace: true,
                },
                {
                    id: "mRPName",
                    header: "МОЛ",
                    minWidth: 150,
                    fillspace: true,
                },
                {
                    id: "location",
                    header: "Местоположение",
                    minWidth: 150,
                    fillspace: true,
                },
                {   
                    id: "startDate",
                    header: "Дата перемещения с",
                    format: webix.i18n.dateFormatStr,
                    width: 150,
                },
                {
                    id: "endDate",
                    header: "Дата поступления на",
                    format: webix.i18n.dateFormatStr,
                    width: 150,
                },
                {
                    id: "docName",
                    header: "Документ",
                    minWidth: 150,
                    fillspace: true,
                }
            ],
            url: {
                $proxy: true,
                load: (view, callback, params) => {
                    const self = this;
                    if (!this._baId)
                        return;

                    this.dataTableControl.showOverlay("Загрузка данных....");
                    Api.ApiInventoryObjectsMovementHistory.getFaAssetesMovementHistory(this._baId)
                        .then(items => {
                            setData(items);
                        }).catch(() => {
                            setData([]);
                        });

                    function setData(items: InventoryObjectsMovementHistory.IInventoryAssetsMovementHistoryModel[]) {
                        (webix.ajax as any).$callback(view, callback, items);
                        self.dataTableControl.hideOverlay();
                    }
                }
            }

        }
    }
}
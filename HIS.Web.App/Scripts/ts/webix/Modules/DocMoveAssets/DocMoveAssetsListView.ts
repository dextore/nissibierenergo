﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Components/ComponentBase");
import Tab = require("../../MainView/Tabs/MainTabBase");
import ToolButtons = require("../../Components/Buttons/ToolBarButtons");
import Common = require("../../Common/CommonExporter");
import StateEntity = require("../../Components/Controls/EntityOperationStateControl");
import Specification = require("./DocMoveAssetsRowsListView");
import Dialog = require("./DocMoveAssetsDialog");
import Warning = require("../../Components/Messages/Warning");
import DataTable = require("../../Components/QueryBuilderListBase/QueryBuilderListBase");
import IQueryBuilderListSetParameters = DataTable.IQueryBuilderListSetParameters;

export interface IDocMoveAssetsList extends Base.IComponent {
    changeCompany(company: Inventory.ILSCompanyAreas);
    ready();
}

export function getDocMoveAssetsList(_viewName: string, module: Common.MainTabBase) {
    return new DocMoveAssetsList(_viewName, module);
}

export class DocMoveAssetsList extends Base.ComponentBase implements IDocMoveAssetsList {
    private readonly _entityAlias = "FADocMovFAssets";
    private _selectedCompany: Inventory.ILSCompanyAreas;
    private _entityInfo = Common.entityTypes().getByAliase(this._entityAlias).entityInfo;
    private _operationState: StateEntity.IEntityOperationStateControl;
    private _specification: Specification.IDocMoveAssetsRowsListView;
    private _specificationModel = {} as Specification.IDocMoveAssetsRowsSetModel;
    private _tableBuilderControl: DataTable.IQueryBuilderList;

    // query builder list parameters 
    private _showBaId: boolean = false;
    private _aliasesWithoutFilter = ["CompanyArea"];
    private _aliasesOrder = [
        "BAId",
        "Name",
        "DocDate",
        "FAMovReasons",
        "SrcMLPId",
        "DstMLPId",
        "SrcLSId",
        "DstLSId",
        "SrcLocation",
        "DstLocation",
        "Evidence",
        "Note",
        "State",
        "Creator",
        "CompanyArea"];


    private get tableId() { return `${this._viewName}-datatable-id`; }
 
    get tableControl() {
        return $$(this.tableId) as webix.ui.datatable;
    }
    protected toolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}-tooldar`, "Добавить", this.createEntity.bind(this)),
        edit: ToolButtons.getEditBtn(`${this._viewName}-tooldar`, "Изменить", this.editEntity.bind(this))
    }
    constructor(private readonly _viewName: string, private readonly _module: Common.MainTabBase) {
        super();
        this.initSelectedCompany();
        this.initOperationState();
        this.initDataTableControl();
        this._specification = Specification.getDocMoveAssetsRowsListView(_viewName, _module);
        this._module.subscribe(this._specification.controlEvent.subscribe(this.reloadStateBySpecification.bind(this), this._module));
    }
    private initDataTableControl() {

        const queryBuilderParam = {
            showBaId: this._showBaId,
            tooltip: true,
            entityInfo: this._entityInfo,
            aliasesOrder: this._aliasesOrder,
            aliasesWithoutFilter: this._aliasesWithoutFilter

    } as IQueryBuilderListSetParameters;
        this._tableBuilderControl = DataTable.getQueryBuilderList(this.tableId, queryBuilderParam);
        const companyField = this._tableBuilderControl.getCompanyAliase();
        if (companyField != "") {
            this._tableBuilderControl.setFilteringFields([{
                field: companyField,
                filterOperator: 0,
                value: this._selectedCompany.id
            }]);
        }
    }
    private initSelectedCompany() {
        this._selectedCompany = this._module.mainTabView.getSelectedCompany();
    }
    private initOperationState(): void {
        this._operationState = StateEntity.getEntityOperationStateControl(`${this._viewName}-operation-state`,
            "",
            () => {
                return true;
            });
        this.subscribe(this._operationState.statusChangedEvent.subscribe((data) => {

            const stateId = this._operationState.state.stateId;
            const baId = this._operationState.state.baId;
            const selectId = this.tableControl.getSelectedId(false, true);

            switch (data) {
            case StateEntity.StateChangeAction.Changed:
                this.showErrors(stateId, baId);
                this.updateTableRecord(baId, selectId);
                break;
            case StateEntity.StateChangeAction.Canceled:
                this.updateTableRecord(baId, selectId);
                break;
            default:
                this._specification.reloadByModel(this._specificationModel);
                break;
            }
            this.resetButtons();
           
        }, this));
        this._operationState.addButtons(this.toolBar.add, this.toolBar.edit);
    }
    private showErrors(stateId: number, baId: number) {
        if (stateId === 5) {
            Warning.showForEntity(this._viewName, baId);
        }
    }

    private reloadStateBySpecification() {
        const selected = this.tableControl.getSelectedItem() as any;
        if (!selected) {
            return;
        }
        this.setOperationState(selected[this._tableBuilderControl.getBAIdAliase()]);
    }
    private setOperationState(baId: number) {
        this._specificationModel = {} as Specification.IDocMoveAssetsRowsSetModel;
        const createdState = 1;
        if (baId == null) {
            this._operationState.baId = null;
            return;
        }

        Api.getApiDocMoveAssets().getDocMoveAssetsData(baId).then(result => {
            if (result.stateId === createdState) {
                let forbiddenState = 5;
                if (result.hasSpecification) {
                    forbiddenState = 255;
                }
                (this._operationState as StateEntity.IEntityOperationStateControlFilterOperation).filterAllowOperation = (item) => item.id !== forbiddenState;
            }
            this._specificationModel = {
                baId: result.baId,
                stateId: result.stateId,
                hasAssets:result.hasSpecification
            }
            this._operationState.baId = baId;
        });
    }
    private updateTableRecord(baId: number, recId?: number) {

        this._tableBuilderControl.getRowDataByBaId(baId).then(data => {
            const row = data.data[0];
            if (!recId) {
                this.tableControl.add(row, 0);
                this._tableBuilderControl.selectFirstRow();
            } else {
                this.tableControl.updateItem(recId, row);
            }
            this._specificationModel.baId = baId;
            this._specificationModel.stateId = this._operationState.state.stateId;
            this._specification.reloadByModel(this._specificationModel);


        });
    }

    private createEntity() {
        
        const model = {
            baId: null,
            docDate:new Date(),
            caId: this._selectedCompany.id,
            caName: this._selectedCompany.name
        } as DocMoveAssets.IDocMoveAssetsData;

        const dialog = Dialog.getDocMoveAssetsDialog(`${this._viewName}-entity-dialog`,
            this._module,
            this._entityInfo.markersInfo,
            model);

        dialog.showModalContent(this._module, () => {
            this.updateTableRecord(dialog.resultBaId);
            dialog.close();
        });
    }
    private editEntity() {
        
        const selected = this.tableControl.getSelectedItem();
        const selectId = this.tableControl.getSelectedId(false, true);
        if (!selected) {
            webix.message("Необходимо выбрать элемент для редактирования", "error");
            return;
        }
        const baId = selected[this._tableBuilderControl.getBAIdAliase()] as number;

        Api.getApiDocMoveAssets().getDocMoveAssetsData(baId).then(data => {
            const dialog = Dialog.getDocMoveAssetsDialog(`${this._viewName}-entity-dialog`,
                this._module,
                this._entityInfo.markersInfo,
                data);

            dialog.showModalContent(this._module, () => {
                this.updateTableRecord(dialog.resultBaId, selectId);
                dialog.close();
            });
        });
    }
    private resetButtons() {
        
        this.toolBar.add.button.enable();
        this.toolBar.edit.button.disable();

        const selected = this.tableControl.getSelectedItem() as any;
        if (!selected || !this._operationState.state) {
            return;
        }
        if (this._operationState.state.stateId === 1) {
            this.toolBar.edit.button.enable();
        }
        
    }
    changeCompany(company: Inventory.ILSCompanyAreas) {
        this._selectedCompany = company;
        const companyField = this._tableBuilderControl.getCompanyAliase();
        if (companyField != "") {
            this._tableBuilderControl.setFilteringFields([{
                field: companyField,
                filterOperator: 0,
                value: this._selectedCompany.id
            }]);
            this._tableBuilderControl.reload();
        }
    }
    ready() {
        this.resetButtons();
        this._tableBuilderControl.ready();
        this._specification.ready();
        this.tableControl.attachEvent("onSelectChange", this.onSelectChangeTable.bind(this));
    }
    private onSelectChangeTable() {
        const selected = this.tableControl.getSelectedItem() as any;
        if (!selected) {
            this._specificationModel = {} as Specification.IDocMoveAssetsRowsSetModel;
            this._specification.reloadByModel(this._specificationModel);
            return;
        }
        this.setOperationState(selected[this._tableBuilderControl.getBAIdAliase()]);
    }
    init() {
    
        return {
            rows: [
                this._operationState.init(),
                this._tableBuilderControl.init(),
                { view: "resizer" },
                this._specification.init()
            ]
        };
        
    }

}
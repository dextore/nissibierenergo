﻿import Common = require("../../Common/CommonExporter");
import Mngr = require("../ModuleManager");
import ListView = require("./DocMoveAssetsListView");

export class DocMoveAssetsModule extends Common.MainTabBase {

    protected static _header = "Перемещение/выбытие инвентарных объектов";
    protected static _viewName = "document-move-assets-module";

    private selectedCompany: Inventory.ILSCompanyAreas;
    private listView: ListView.IDocMoveAssetsList;

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
        this.listView = ListView.getDocMoveAssetsList(`${this.viewName}-list`, this);

    }
    systemCompanyHandler(company: Inventory.ILSCompanyAreas) {
        this.listView.changeCompany(company);
    }
    getContent(): object {
        return this.listView.init();
    }
    ready() {
        this.listView.ready();
    }


}


Mngr.ModuleManager.registerModule(DocMoveAssetsModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new DocMoveAssetsModule(mainView);
});
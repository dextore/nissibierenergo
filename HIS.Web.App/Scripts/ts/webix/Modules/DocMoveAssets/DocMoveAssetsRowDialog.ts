﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Common/DialogBase");
import Common = require("../../Common/CommonExporter");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import MB = require("../../Components/Markers/MarkerControlBase");
import MarkerFactory = require("../../Components/Markers/MarkerControlFactory");
import EntityRefMarkerControl = require("../../Components/Markers/EntityRefMarkerControl");

export function getDocMoveAssetsRowDialog(viewName: string,
    module: Common.MainTabBase,
    dataModel: DocMoveAssets.IDocMoveAssetsRow): IDocMoveAssetsRowDialog {
    return new DocMoveAssetsRowDialog(viewName, module, dataModel);
};
export interface IDocMoveAssetsRowDialog extends Base.IDialog {
    resultDataModel: DocMoveAssets.IDocMoveAssetsRow;
}

class DocMoveAssetsRowDialog extends Base.DialogBase {

    get formId() { return `${this.viewName}-form-id`; }
    get relaseDateId() { return `${this.formId}-release-date-id`; }
    get checkDateId() { return `${this.formId}-check-date-id`; }
    get barcodeId() { return `${this.formId}-barcode-id`; }
    get serialNumberId() { return `${this.formId}-serial-number-id`; }
    get resultDataModel() { return this._dataModel; }

    private assetsCotrol: MB.IMarkerControl;
    private documentCotrol: MB.IMarkerControl;
    private assetsInfoModel: Markers.IMarkerInfoModel;
    private documentInfoModel: Markers.IMarkerInfoModel;
    private tempAssetsModel = {} as DocMoveAssets.IAssetsDataModel;

    constructor(viewName: string,
        private readonly _module: Common.MainTabBase,
        private _dataModel: DocMoveAssets.IDocMoveAssetsRow) {
        super(viewName);
        if (!this._dataModel.recId) {
            this.isCreate = true;
        }
        this.tempAssetsModel.baId = (!this._dataModel.faId) ? null : this._dataModel.faId;
        this.tempAssetsModel.checkDate = (!this._dataModel.checkDate) ? null : this._dataModel.checkDate;
        this.tempAssetsModel.releaseDate = (!this._dataModel.releaseDate) ? null : this._dataModel.releaseDate;
        this.tempAssetsModel.barCode = (!this._dataModel.barCode) ? null : this._dataModel.barCode;
        this.tempAssetsModel.serialNumber = (!this._dataModel.serialNumber) ? null : this._dataModel.serialNumber;

    }

    private isCreate = false;
    private isChanged = false;

    private getChanged(): boolean {
        return (this.isChanged || this.assetsCotrol.isChanged || this.documentCotrol.isChanged);
    }
    protected headerLabel(): string {
        if (!this._dataModel.recId) {
            return "Создание спецификации документа перемещение/выбытие инвентарных объектов";
        }
        return "Редактирование спецификации документа перемещение/выбытие инвентарных объектов";
    }
    get mainTabView(): Common.IMainTabbarView {
        return this._module.mainTabView;
    }
    get formControl() {
        return $$(this.formId) as webix.ui.form;
    }
    protected okBtn = Btn.getOkButton(this.viewName,
        () => {
            this.okBtn.button.disable();
            if (this.formControl.validate()) {
                if (!this.isCreate && !this.getChanged()) {
                    this.okClose();
                    return false;
                }
                const saveModel = this.getFormData();
                if (!saveModel) {
                    this.okBtn.button.enable();
                    return;
                }
                Api.getApiDocMoveAssets().updateDocAcceptAssetRow(saveModel).then((result) => {
                    this._dataModel = result;
                    this.okClose();
                });
            }
            this.okBtn.button.enable();
        });
    protected cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });
    protected contentConfig() {

        const self = this;
     
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elementsConfig: {
                labelPosition: "left",
                labelAlign: "left",
                labelWidth: 240,
                minWidth: 650,
                animate: false
            },
            rules: {
                FAssets: webix.rules.isNotEmpty
            },
            elements: [
               
                this.getAssetsEntityControl().init(),
                {
                    id: self.serialNumberId,
                    view: "text",
                    name: "serialNumber",
                    label: "Заводской(серийный) номер",
                    disabled: true,
                    on: {
                        onChange: () => {
                            self.isChanged = true;
                        },
                    }
                },
                {
                    id: self.relaseDateId,
                    view: "datepickerExtended",
                    name: "releaseDate",
                    label: "Дата выпуска",
                    disabled: true,
                    pattern: { mask: "##.##.####", allow: /[0-9]/g },
                    validate: () => {
                        if (!($$(self.relaseDateId) as webix.ui.datepickerExtended).getValidateTextValue()) {
                            return false;
                        }
                        return true;
                    },
                    on: {
                        onChange: () => {
                            self.isChanged = true;
                            ($$(self.relaseDateId) as webix.ui.datepickerExtended).validate();
                        },
                        onBlur: () => {
                            ($$(self.relaseDateId) as webix.ui.datepickerExtended).validate();
                        }
                    }
                },
                {
                    id: self.checkDateId,
                    view: "datepickerExtended",
                    name: "checkDate",
                    label: "Дата последней гос.поверки",
                    pattern: { mask: "##.##.####", allow: /[0-9]/g },
                    disabled: true,
                    validate: () => {
                        if (!($$(self.checkDateId) as webix.ui.datepickerExtended).getValidateTextValue()) {
                            return false;
                        }
                        return true;
                    },
                    on: {
                        onChange: () => {
                            self.isChanged = true;
                            ($$(self.checkDateId) as webix.ui.datepickerExtended).validate();
                        },
                        onBlur: () => {
                            ($$(self.checkDateId) as webix.ui.datepickerExtended).validate();
                        }
                    }
                },
                {
                    id:self.barcodeId,
                    view: "text",
                    name: "barCode",
                    label: "Штрих-код",
                    disabled:true,
                    on: {
                        onChange: () => {
                            self.isChanged = true;
                        },
                    }
                },
                this.getDocumentControl().init(),
                {
                    view: "text",
                    name: "evidence", 
                    label: "Основание",
                    on: {
                        onChange: () => {
                            
                            self.isChanged = true;
                        },
                    }
                }
            ]
        };
    }
    protected windowConfig() {
        return {
            autoheight: true,
            minWidth: 680,
            width:680

        };
    }
    private checkSize() {
        const rect = $$(this.windowId).getNode().getBoundingClientRect();
        const diff = $$(this.formId).getNode().scrollHeight - $$(this.formId).getNode().clientHeight + 10;
        $$(this.windowId).$setSize(rect.width, rect.height + diff);
        ($$(this.windowId) as webix.ui.window).define("height", rect.height + diff);
    }

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._module.disableCompany();
    }

    destroy(): void {
        super.destroy();
        this._module.enableCompany();
    }

    bindModel(): void {
      
        this.formControl.addView({
            cols: [
                {},
                this.okBtn.init(),
                this.cancelBtn.init()
            ]
        });
        this.checkSize();
        this.setFormData();
    }
    protected afterShowDialog() {

        (this.assetsCotrol.control as webix.ui.search).attachEvent("onChange",
            () => {
                this.okBtn.button.disable();
                const assetsValue = this.assetsCotrol.getValue(this.assetsInfoModel);
                const faId = (!assetsValue) ? null : Number(assetsValue.value);
                this.clearAssetFields();
                this.tempAssetsModel = {} as DocMoveAssets.IAssetsDataModel;

                if (faId > 0) {
                    this.updateDataModel(faId);
                } else {
                    this.reloadDisabledForm();
                }
               
            });
    }
    private updateDataModel(faId:number) {
       
        Api.getApiDocMoveAssets().getAssetsInfo(faId).then(data => {
            this.tempAssetsModel = data;
            ($$(this.serialNumberId) as webix.ui.text).setValue(data.serialNumber);
            ($$(this.barcodeId) as webix.ui.text).setValue(data.barCode);
            ($$(this.relaseDateId) as webix.ui.datepickerExtended).setValue(data.releaseDate as any);
            ($$(this.checkDateId) as webix.ui.datepickerExtended).setValue(data.checkDate as any);
            this.reloadDisabledForm();
        }).catch(error => {
            this.okBtn.button.enable();
        });
    }
    private clearAssetFields() {
        ($$(this.serialNumberId) as webix.ui.text).setValue("");
        ($$(this.barcodeId) as webix.ui.text).setValue("");
        ($$(this.relaseDateId) as webix.ui.datepickerExtended).setValue(null);
        ($$(this.checkDateId) as webix.ui.datepickerExtended).setValue(null);
    }
    private reloadDisabledForm() {

        $$(this.checkDateId).disable();
        $$(this.relaseDateId).disable();
        $$(this.barcodeId).disable();
        $$(this.serialNumberId).disable();

        if (!this.tempAssetsModel.serialNumber) {
            $$(this.serialNumberId).enable();
        }
        if (!this.tempAssetsModel.checkDate) {
            $$(this.checkDateId).enable();
        }
        if (!this.tempAssetsModel.releaseDate) {
            $$(this.relaseDateId).enable();
        }
        if (!this.tempAssetsModel.barCode) {
            $$(this.barcodeId).enable();
        }
        this.okBtn.button.enable();
    }
    private getDocumentControl(): MB.IMarkerControl {
        this.documentInfoModel = {
            id: 10001,
            catalogImplementTypeName: "",
            implementTypeField: "",
            implementTypeName: "",
            isBlocked: false,
            isCollectible: false,
            isImplemented: true,
            isOptional: false,
            isPeriodic: false,
            isRequired: false,
            label: "Документ",
            markerType: HIS.Models.Layer.Models.Markers.MarkerType.MtEntityRef,
            name: "Document",
            refBaTypeId: 1, /// прокинуть из system
            list: []
        } as Markers.IMarkerInfoModel;

        this.documentCotrol = MarkerFactory.MarkerControlfactory.getControl(`${this.viewName}-document`, this.documentInfoModel,
            (content) => {
                return {
                    label: "Инициирующий документ"
                }
            });
        (this.documentCotrol as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
        (this.documentCotrol as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this._module);
        return this.documentCotrol;

    }
    private getAssetsEntityControl(): MB.IMarkerControl {
        const controlId = this.viewName;
        this.assetsInfoModel = {
            id: 1000,
            catalogImplementTypeName: "",
            implementTypeField: "",
            implementTypeName: "",
            isBlocked: false,
            isCollectible: false,
            isImplemented: true,
            isOptional: false,
            isPeriodic: false,
            isRequired: true,
            label: "Инвентарный объект",
            markerType: HIS.Models.Layer.Models.Markers.MarkerType.MtEntityRef,
            name: "FAssets",
            refBaTypeId: 81, /// прокинуть из system
            list: []
        } as Markers.IMarkerInfoModel;

        this.assetsCotrol = MarkerFactory.MarkerControlfactory.getControl(controlId, this.assetsInfoModel,
            (content) => {
                return {
                    label: "Инвентарный объект"
                }
            });
        (this.assetsCotrol as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
        (this.assetsCotrol as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this._module);
       
        return this.assetsCotrol;
    }
    private getFormData(): DocMoveAssets.IDocMoveAssetsRow {

        const assetsValue = this.assetsCotrol.getValue(this.assetsInfoModel);
        const documentValue = this.documentCotrol.getValue(this.documentInfoModel);
      
        const model = this.formControl.getValues() as DocMoveAssets.IDocMoveAssetsRow;
        model.faId = (!assetsValue) ? null : Number(assetsValue.value);
        model.faName = (!assetsValue) ? "" : assetsValue.displayValue;
        model.docMoveId = this._dataModel.docMoveId ;
        model.recId = this._dataModel.recId;
        model.evidenceId = (!documentValue) ? null : Number(documentValue.value);
       return model;
    }
    private setFormData() {

        let model: { [id: string]: any } = this._dataModel;

        this.assetsCotrol.setInitValue(getMarkerValue(this.assetsInfoModel, this._dataModel.faId as any, this._dataModel.faName), model);
        this.documentCotrol.setInitValue(getMarkerValue(this.documentInfoModel , this._dataModel.evidenceId as any, this._dataModel.evidenceName), model);


        this.formControl.setValues(model);
        this.formControl.clearValidation();
        this.reloadDisabledForm();
        this.isChanged = false;


        function getMarkerValue(info: Markers.IMarkerInfoModel, value: string, displayValue: string): Markers.IMarkerValueModel {
            return {
                displayValue: displayValue,
                itemId: null,
                endDate: null,
                markerId: info.id,
                markerType: info.markerType,
                MVCAliase: info.name,
                note: null,
                isVisible: true,
                isBlocked: false,
                startDate: null,
                value: value,
                isDeleted: false
            }
        }
       


    }

}

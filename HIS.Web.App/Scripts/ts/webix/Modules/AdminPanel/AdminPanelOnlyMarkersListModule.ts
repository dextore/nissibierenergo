﻿import Common = require("../../Common/CommonExporter");
import Mngr = require("../ModuleManager");
import Reg = require("../RegisterModules");
import adminPanelOnlyMarkersList = require("../../Components/Forms/AdminPanel/AdminPanelOnlyMarkersList");

export class AdminPanelOnlyMarkersListModule extends Common.MainTabBase {

    protected static _viewName = adminPanelOnlyMarkersList.modulesName;
    protected static _header = Reg.Register.modules[adminPanelOnlyMarkersList.modulesName].header;

    private adminPanelOnlyMarkersList: adminPanelOnlyMarkersList.IAdminPanelOnlyMarkersList;

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
        this.adminPanelOnlyMarkersList = adminPanelOnlyMarkersList.getAdminPanelOnlyMarkersList(this);
    }

    systemCompanyHandler(company: Inventory.ILSCompanyAreas): void {
        this.adminPanelOnlyMarkersList.load();
        this.adminPanelOnlyMarkersList.selectedCompany = company;
    }

    getContent() {
        return {
            rows: [
                this.adminPanelOnlyMarkersList.init()
            ]
        }
    }

    ready(): void {
        this.adminPanelOnlyMarkersList.ready();
    }
}

Mngr.ModuleManager.registerModule(AdminPanelOnlyMarkersListModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new AdminPanelOnlyMarkersListModule (mainView);
});

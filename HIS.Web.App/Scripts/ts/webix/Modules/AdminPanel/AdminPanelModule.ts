﻿import Common = require("../../Common/CommonExporter");
import Mngr = require("../ModuleManager");
import Reg = require("../RegisterModules");
import adminPanelListModule = require("../../Components/Forms/AdminPanel/AdminPanelList");
import markerList = require("../../Components/Forms/AdminPanel/AdminPanelMarkerList");
import availableStateList = require("../../Components/Forms/AdminPanel/AdminAvailableStateList");
import availableOperationsList = require("../../Components/Forms/AdminPanel/AdminAvailableOperationsList");
import availablOperationsStateList = require("../../Components/Forms/AdminPanel/AdminAvailablOperationsStateList");

export class AdminPanelModule extends Common.MainTabBase {

    protected static _viewName = adminPanelListModule.modulesName;
    protected static _header = Reg.Register.modules[adminPanelListModule.modulesName].header;

    private adminPanelList: adminPanelListModule.IAdminPanelList;
    private adminPanelMarkerList: markerList.IAdminPanelMarkerList;
    private adminPanelAvailableStateList: availableStateList.IAvailableStateList;
    private adminAvailableOperationsList: availableOperationsList.IAdminAvailableOperationsList;
    private adminAvailablOperationsStateList: availablOperationsStateList.IAdminAvailablOperationsStateList;

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
        this.adminPanelList = adminPanelListModule.getAdminPanelList(this);
        this.adminPanelMarkerList = markerList.getAdminPanelMarkerList(this, this.adminPanelList);
        this.adminPanelAvailableStateList = availableStateList.getAdminAvailableStateList(this, this.adminPanelList);
        this.adminAvailableOperationsList = availableOperationsList.getAdminAvailableOperationsList(this, this.adminPanelList);
        this.adminAvailablOperationsStateList = availablOperationsStateList.getAdminAvailablOperationsStateList(this, this.adminPanelList);
    }

    systemCompanyHandler(company: Inventory.ILSCompanyAreas): void {
        this.adminPanelList.selectedCompany = company;
        this.adminPanelList.load();
        this.adminPanelMarkerList.selectedCompany = company;
    }

    getContent() {
        return {
            rows: [
                this.adminPanelList.init(),
                { view: "resizer" },
                {
                    rows: [
                        {
                            view: "tabview",
                            cells: [
                                {
                                    header: "Маркеры типа сущности",
                                    body: this.adminPanelMarkerList.init(),
                                },
                                {
                                    header: "Состояния типа сущности",
                                    body: this.adminPanelAvailableStateList.init(),
                                },
                                {
                                    header: "Операции типа сущности",
                                    body: this.adminAvailableOperationsList.init(),
                                },
                                {
                                    header: "Состояния операций типа сущности",
                                    body: this.adminAvailablOperationsStateList.init(),
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    }

    ready(): void {
        this.adminPanelList.ready();
        this.adminPanelMarkerList.ready();
        this.adminPanelAvailableStateList.ready();
        this.adminAvailableOperationsList.ready();
        this.adminAvailablOperationsStateList.ready();
    }
}

Mngr.ModuleManager.registerModule(AdminPanelModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new AdminPanelModule(mainView);
});

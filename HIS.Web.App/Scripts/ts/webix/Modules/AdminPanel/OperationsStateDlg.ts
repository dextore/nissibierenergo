﻿import Base = require("../../Common/DialogBase");
import Common = require("../../Common/CommonExporter");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import Api = require("../../Api/ApiExporter");
import adminAvailableStateListDlgView = require("../../Components/Forms/AdminPanel/AdminAvailableStateListDlgView");
import adminAvailableOperationsListDlgView = require("../../Components/Forms/AdminPanel/AdminAvailableOperationsListDlgView");

export function getOperationsStateDlg(viewName: string, isEdit: boolean, stateDataTable: webix.ui.datatable, selectedCompany: Inventory.ILSCompanyAreas, entityBaId: number) {
    return new OperationsStateDlg(viewName, isEdit, stateDataTable, selectedCompany, entityBaId);
}

class OperationsStateDlg extends Base.DialogBase {
    private _item: AdminPanel.IBaTypesOperationsStateModel;
    protected get formId() {
        return `${this.viewName}-StatesAdminDlg-Dlg-id`;
    };
    protected get labelWidth() {
        return 150 + 140;
    };

    constructor(viewName: string,
        private isEdit: boolean,
        private stateDataTable: webix.ui.datatable,
        private selectedCompany: Inventory.ILSCompanyAreas,
        private entityBaId:number) {
        super(viewName);
        if (isEdit)
            this._item = stateDataTable.getSelectedItem() as AdminPanel.IBaTypesOperationsStateModel;
    }

    private _tabModule: Common.IMainTab;

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._tabModule = module;
        this._tabModule.disableCompany();

        let operationControl = $$(`${this.formId}-OperationsAdminLink-superField`) as any;

        let startStateControl = $$(`${this.formId}-StartStateAdminLink-superField`) as any;
        let endStateControl = $$(`${this.formId}-EndStateAdminLink-superField`) as any;

        if (this.isEdit) {
            
            operationControl.setValue(this._item.operationName);
            startStateControl.operationId = this._item.operationId;

            startStateControl.setValue(this._item.sourceStateName);
            //startStateControl.stateId = this._item.sourceStateId;

            endStateControl.setValue(this._item.destinationStateName);
            //endStateControl.stateId = stateId;

        } else {
            operationControl.setValue("");
            startStateControl.operationId = null;

            startStateControl.setValue("");
            startStateControl.stateId = null;

            endStateControl.setValue("");
            endStateControl.stateId = null;
        }
    }

    headerLabel(): string {
        return `${this.isEdit ? "Редактирование" : "Добавление"} связи операций с состояниями`;
    }

    contentConfig() {
        let field = `field-id`;
        return {
            view: "form",
            id: this.formId,
            minheight: 600,
            autoheight: true,
            elements: [
                {
                    view: "extsearch",
                    id: `${this.formId}-OperationsAdminLink-superField`,
                    css: "protoUI-with-icons-2",
                    icons: ["search", "close"],
                    label: "Операция",
                    labelWidth: this.labelWidth,
                    readonly: true,
                    name: `${this.formId}-OperationsAdminLink-superFieldName`,
                    validate: (value) => {
                        return true;
                    },
                    on: {
                        onSearchIconClick: () => this.showSearchOperationDlg(false),
                        onCloseIconClick: () => this.clearSearchOperationField()
                    }
                },
                {
                    view: "extsearch",
                    id: `${this.formId}-StartStateAdminLink-superField`,
                    css: "protoUI-with-icons-2",
                    icons: ["search", "close"],
                    label: "Начальное cостояние",
                    labelWidth: this.labelWidth,
                    readonly: true,
                    name: `${this.formId}-StartStateAdminLink-superFieldName`,
                    validate: (value) => {
                        return true;
                    },
                    on: {
                        onSearchIconClick: () => this.showSearchStateDlg(false, "StartStateAdminLink"),
                        onCloseIconClick: () => this.clearSearchStateField("StartStateAdminLink")
                    }
                },
                {
                    view: "extsearch",
                    id: `${this.formId}-EndStateAdminLink-superField`,
                    css: "protoUI-with-icons-2",
                    icons: ["search", "close"],
                    label: "Конечное состояние",
                    labelWidth: this.labelWidth,
                    readonly: true,
                    name: `${this.formId}-EndStateAdminLink-superFieldName`,
                    validate: (value) => {
                        return true;
                    },
                    on: {
                        onSearchIconClick: () => this.showSearchStateDlg(false, "EndStateAdminLink"),
                        onCloseIconClick: () => this.clearSearchStateField("EndStateAdminLink")
                    }
                },
                {
                    id: `${this.formId}-isBlocked-${field}`,
                    label: "Признак блокировки перехода",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isBlocked : ""
                },
                {
                    cols: [
                        {},
                        this.okBtn.init(),
                        this.cancelBtn.init()
                    ]
                }
            ],
        }
    }

    protected okBtn = Btn.getOkButton(this.viewName, () => {
        if (($$(this.formId) as webix.ui.form).validate()) {
            const data = this.getDataFromDialog();
            let baid = this.entityBaId;
            
            Api.ApiAdminPanel.addOperationStateToBaType(baid, data).then(result => {
                if (result) {
                    if (!this.isEdit) {
                        let hasItem = this.stateDataTable.find(x => x.operationId === result.operationId);
                        if (hasItem.length === 0)
                            this.stateDataTable.add(result);
                        else 
                            webix.message("Связь операций и состояние с таким ИД уже используется", "error");
                    } else {
                        const { id } = this.stateDataTable.getSelectedItem();
                        this.stateDataTable.updateItem(id, result);
                    }
                    this.stateDataTable.refresh();
                    this.okClose();
                }
            });
        }
    });

    protected cancelBtn = Btn.getCancelButton(this.viewName, () => {
        this.okClose();
    });

    getDataFromDialog() {
        const form = $$(this.formId) as any;
        let data = [];
        let self = this;

        form._collection.forEach(
            (val) => {
                let id = ($$(val.id).config.id as string).substring(0, ($$(val.id).config.id as string).length - "-field-id".length);
                id = id.substring(self.formId.length + 1);
                data[id] = ($$(val.id).config as any).value;
            }
        );

        data["operationId"] = form._cells[0].config.operationId === null ? null : Number(form._cells[0].config.operationId);
        data["startStateId"] = form._cells[1].config.stateId === null ? null : Number(form._cells[1].config.stateId);
        data["endStateId"] = form._cells[2].config.stateId === null ? null : Number(form._cells[2].config.stateId);

        let result: AdminPanel.IBaTypesOperationsStateCreateModel = {
            isBlocked: Boolean(data["isBlocked"]),
            startState: {
                stateId: data["startStateId"],
                stateName: data["StartStateAdminLink-s"],
            },
            endState: {
                stateId: data["endStateId"],
                stateName: data["EndStateAdminLink-s"]
            },
            operation: {
                operationId: data["operationId"],
                operationName: data["OperationsAdminLink-s"]
            }
        };

        return result;
    }

    showSearchStateDlg(isEdit, id) {
        let control = $$(`${this.formId}-${id}-superField`) as any;

        const dlg = adminAvailableStateListDlgView.getAdminAvailableStateListDlgView(
                `${this.formId}-search-dlg`,
                false,
                control,
                this.entityBaId,
                this._tabModule);
        dlg.showModalContent(this._tabModule, () => {
            dlg.close();
        } );
    }

    clearSearchStateField(id) {
        const control = $$(`${this.formId}-${id}-superField`) as any;
        control.setValue("");
        control.config.stateId = null;
    }

    showSearchOperationDlg(isEdit) {
        let control = $$(`${this.formId}-OperationsAdminLink-superField`) as any;
        
        const dlg = adminAvailableOperationsListDlgView.getAdminAvailableOperationsListDlgView(
            `${this.formId}-search-dlg`,
            false,
            control,
            this.entityBaId,
            this._tabModule);

        dlg.showModalContent(this._tabModule, () => {
            dlg.close();
        });
    }

    clearSearchOperationField() {
        const control = $$(`${this.formId}-OperationsAdminLink-superField`) as any;
        control.setValue("");
        control.config.operationId = null;
    }
}
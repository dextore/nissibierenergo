﻿import Common = require("../../Common/CommonExporter");
import Mngr = require("../ModuleManager");
import Reg = require("../RegisterModules");
import adminPanelOnlyStateList = require("../../Components/Forms/AdminPanel/AdminPanelOnlyStateList");

export class AdminPanelOnlyStateListModule extends Common.MainTabBase {

    protected static _viewName = adminPanelOnlyStateList.modulesName;
    protected static _header = Reg.Register.modules[adminPanelOnlyStateList.modulesName].header;

    private _adminPanelOnlyStateList: adminPanelOnlyStateList.IAdminPanelOnlyStateList;

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
        this._adminPanelOnlyStateList = adminPanelOnlyStateList.getAdminPanelOnlyStateList(this);
    }

    systemCompanyHandler(company: Inventory.ILSCompanyAreas): void {
        this._adminPanelOnlyStateList.load();
        this._adminPanelOnlyStateList.selectedCompany = company;
    }

    getContent() {
        return {
            rows: [
                this._adminPanelOnlyStateList.init()
            ]
        }
    }

    ready(): void {
        this._adminPanelOnlyStateList.ready();
    }
}

Mngr.ModuleManager.registerModule(AdminPanelOnlyStateListModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new AdminPanelOnlyStateListModule(mainView);
});
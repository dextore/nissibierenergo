﻿import Base = require("../../Common/DialogBase");
import Common = require("../../Common/CommonExporter");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import Api = require("../../Api/ApiExporter");
import adminPanelOnlyStateListDlgView = require("../../Components/Forms/AdminPanel/AdminPanelOnlyStateListDlgView");

export function getStatesAdminDlg(viewName: string, isEdit: boolean, stateDataTable: webix.ui.datatable, selectedCompany: Inventory.ILSCompanyAreas, entityBaId: number) {
    return new StatesAdminDlg(viewName, isEdit, stateDataTable, selectedCompany, entityBaId);
}

class StatesAdminDlg extends Base.DialogBase {
    private _item: AdminPanel.IBaTypesStatesModel;
    protected get formId() {
        return `${this.viewName}-StatesAdminDlg-Dlg-id`;
    };
    protected get labelWidth() {
        return 150 + 140;
    };

    constructor(viewName: string,
        private isEdit: boolean,
        private stateDataTable: webix.ui.datatable,
        private selectedCompany: Inventory.ILSCompanyAreas,
        private entityBaId:number) {
        super(viewName);
        if (isEdit)
            this._item = stateDataTable.getSelectedItem() as AdminPanel.IBaTypesStatesModel;
    }

    private _tabModule: Common.IMainTab;

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._tabModule = module;
        this._tabModule.disableCompany();

        let control = $$(`${this.formId}-StatesAdminLink-superField`) as any;
        if (this.isEdit) {
            control.setValue(this._item.stateName);
            control.config.stateId = this._item.stateId;
        } else {
            control.setValue("");
            control.config.stateId = null;
        }
    }

    headerLabel(): string {
        return `${this.isEdit ? "Редактирование" : "Добавление"} состояния`;
    }

    contentConfig() {
        let field = `field-id`;
        return {
            view: "form",
            id: this.formId,
            minheight: 600,
            autoheight: true,
            elements: [
                {
                    view: "extsearch",
                    id: `${this.formId}-StatesAdminLink-superField`,
                    css: "protoUI-with-icons-2",
                    icons: ["search", "close"],
                    label: "Состояние",
                    labelWidth: this.labelWidth,
                    readonly: true,
                    name: `${this.formId}-StatesAdminLink-superFieldName`,
                    validate: (value) => {
                        return true;
                    },
                    on: {
                        onSearchIconClick: () => this.showSearchMarkerDlg(false),
                        onCloseIconClick: () => this.clearSearchMarkerField()
                    }
                },
                {
                    id: `${this.formId}-isFirstState-${field}`,
                    label: "Признак начального состояния",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isFirstState : ""
                },
                {
                    id: `${this.formId}-isDelState-${field}`,
                    label: "Признак конечного состояния",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isDelState : ""
                },
                {
                    cols: [
                        {},
                        this.okBtn.init(),
                        this.cancelBtn.init()
                    ]
                }
            ],
        }
    }

    protected okBtn = Btn.getOkButton(this.viewName, () => {
        if (($$(this.formId) as webix.ui.form).validate()) {
            const data = this.getDataFromDialog();
            let baid = this.entityBaId;
            Api.ApiAdminPanel.addStateToBaType(baid, data).then(result => {
                if (result) {
                    if (!this.isEdit) {
                        let hasItem = this.stateDataTable.find(x => x.stateId === result.stateId);
                        if (hasItem.length === 0)
                            this.stateDataTable.add(result);
                        else 
                            webix.message("Состояние с таким ИД уже используется", "error");
                    } else {
                        let { id } = this.stateDataTable.getSelectedItem();
                        this.stateDataTable.updateItem(id, result);
                    }
                    this.stateDataTable.refresh();
                    this.okClose();
                }
            });
        }
    });

    protected cancelBtn = Btn.getCancelButton(this.viewName, () => {
        this.okClose();
    });

    getDataFromDialog() {
        const form = $$(this.formId) as any;
        let data = [];
        let self = this;

        form._collection.forEach(
            (val) => {
                let id = ($$(val.id).config.id as string).substring(0, ($$(val.id).config.id as string).length - "-field-id".length);
                id = id.substring(self.formId.length + 1);
                data[id] = ($$(val.id).config as any).value;
            }
        );
        data["stateId"] = Number(form._cells[0].config.stateId);

        let result: AdminPanel.IBaTypesStatesModel = {
            stateId: data["stateId"],
            stateName: data["StatesAdminLink-s"],
            isDelState: Boolean(data["isDelState"]),
            isFirstState: Boolean(data["isFirstState"])
        };
        return result;
    }

    showSearchMarkerDlg(isEdit) {
        let control = $$(`${this.formId}-StatesAdminLink-superField`) as any;

        const dlg = adminPanelOnlyStateListDlgView.getAdminPanelOnlyStateListDlgView(
            `${this.formId}-search-dlg`,
            false,
            control,
            this.entityBaId,
            this._tabModule);
        dlg.showModalContent(this._tabModule, () => {
            dlg.close();
        } );
    }

    clearSearchMarkerField() {
        const control = $$(`${this.formId}-StatesAdminLink-superField`) as any;
        control.setValue("");
        control.config.stateId = null;
    }
}
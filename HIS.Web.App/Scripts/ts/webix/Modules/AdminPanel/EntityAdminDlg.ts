﻿import Base = require("../../Common/DialogBase");
import Common = require("../../Common/CommonExporter");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import Api = require("../../Api/ApiExporter");
import ToolBarsButtons = require("../../Components/Buttons/ToolBarButtons");
import adminPanelListDlgView = require("../../Components/Forms/AdminPanel/AdminPanelListDlgView");

export function getEntityAdmiDialog(viewName: string, isEdit: boolean, datatable: webix.ui.datatable, selectedCompany: Inventory.ILSCompanyAreas) {
    return new EntityAdminDlg(viewName, isEdit, datatable, selectedCompany);
}

class EntityAdminDlg extends Base.DialogBase {
    private _item: AdminPanel.IBaTypesModel;
    protected get formId() {
        return `${this.viewName}-entityAdmin-Dlg-id`;
    };
    protected get labelWidth() {
        return 150 + 140;
    };

    constructor(viewName: string,
                private isEdit: boolean,
                private datatable: webix.ui.datatable,
                private selectedCompany: Inventory.ILSCompanyAreas) {
        super(viewName);

        if (isEdit)
            this._item = datatable.getSelectedItem() as AdminPanel.IBaTypesModel;
    }

    private _tabModule: Common.IMainTab;

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._tabModule = module;
        this._tabModule.disableCompany();
        let checkbox = $$(`${this.formId}-isImplementType-field-id`) as webix.ui.checkbox;
        let field = $$(`${this.formId}-isImplementTypeName-field-id`) as webix.ui.text;

        checkbox.attachEvent("onChange",
            (newv, oldv) => {
                if (newv === 1) {
                    field.config.required = true;
                    this.okBtn.button.disable();
                } else {
                    field.config.required = false;
                    field.setValue("");
                    this.okBtn.button.enable();
                }
                field.refresh();
            });

        field.attachEvent("onChange",
            (value) => {
                if (field.config.required) {
                    if (value.length > 0)
                        this.okBtn.button.enable();
                    else this.okBtn.button.disable();
                }
            });

        let parentIdField = ($$(`${this.formId}-parent-superField`) as any);
        
        if (this.isEdit) {
            parentIdField.setValue(this._item.parent);
            parentIdField.define("markerId", this._item.parentId);
            parentIdField.disable();
        }
    }

    headerLabel(): string {
        return `${this.isEdit ? "Редактирование" : "Добавление"} типа сущности`;
    }

    private searchField = {
        view: "extsearch",
        id: `${this.formId}-parent-superField`,
        css: "protoUI-with-icons-2",
        icons: ["search", "close"],
        readonly: true,
        label: "Ссылка на тип сущности-родителя",
        labelWidth: this.labelWidth,
        name: `${this.formId}-parent-superFieldName`,
        validate: (value) => {
            return true;
        },
        on: {
            onSearchIconClick: () => this.showBaTypesChooseDlg(),
            onCloseIconClick: () => this.clearParentBaType()
        }
    };

    contentConfig() {
        let field = `field-id`;
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
                {
                    view: "text",
                    id: `${this.formId}-name-${field}`,
                    name: "name",
                    label: "Наименование",
                    labelWidth: this.labelWidth,
                    required: true,
                    value: this.isEdit ? this._item.name : "",
                    validate: (val) => {
                        console.log("name-val: ", val);
                        return val.length > 0;
                    }
                },
                {
                    view: "text",
                    id: `${this.formId}-mvcAlias-${field}`,
                    name: "mvcAlias",
                    label: "Уникальное неизменное обозначение",
                    labelWidth: this.labelWidth,
                    value: this.isEdit ? this._item.mvcAlias : "",
                    required: true,
                    validate: (val) => {
                        console.log("mvcAlias-val: ", val);
                        return val.length > 0;
                    }
                },
                this.searchField,
                {
                    id: `${this.formId}-fixOperationHistory-${field}`,
                    label: "Признак журналирования",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.fixOperationHistory : 1,
                },
                {
                    id: `${this.formId}-isGroupType-${field}`,
                    label: "Признак технологической сущности",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isGroupType : 0,
                },
                {
                    id: `${this.formId}-isImplementType-${field}`,
                    label: "Признак реализованного типа",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isImplementType : 0,
                },
                {
                    id: `${this.formId}-isImplementTypeName-${field}`,
                    label: "Наименование таблицы для хранения экз.",
                    labelWidth: this.labelWidth,
                    view: "text",
                    value: this.isEdit ? this._item.isImplementTypeName : "",
                },
                {
                    id: `${this.formId}-searchType-${field}`,
                    label: "Форма поиска/список",
                    labelWidth: this.labelWidth,
                    view: "select",
                    value: this.isEdit ? this._item.searchTypeId : 2,
                    options: [
                        { "id": 1, "value": "Список" },
                        { "id": 2, "value": "Форма поиска" }
                    ],
                },
                {
                    id: `${this.formId}-shortName-${field}`,
                    label: "Краткое наименование",
                    labelWidth: this.labelWidth,
                    view: "text",
                    value: this.isEdit ? this._item.shortName : "",
                },
                {
                    id: `${this.formId}-fullName-${field}`,
                    label: "Дополнительное полное наименование",
                    labelWidth: this.labelWidth,
                    view: "text",
                    value: this.isEdit ? this._item.fullName : "",
                },

                {
                    cols: [
                        {},
                        this.okBtn.init(),
                        this.cancelBtn.init()
                    ]
                }
            ],
            rules: {
                "name": webix.rules.isNotEmpty,
                "mvcAlias": webix.rules.isNotEmpty
            }
        }
    }

    protected okBtn = Btn.getOkButton(this.viewName, () => {
        console.log(($$(this.formId) as webix.ui.form).validate());
        if (($$(this.formId) as webix.ui.form).validate()) {
            const data: AdminPanel.IBaTypesModel = this.getDataFromDialog();
            Api.ApiAdminPanel.createUpdateEntitiType(data).then(result => {
                if (result) {
                    if (!this.isEdit) {
                        this.datatable.add(result, 0);
                    }
                    else {
                        const { id } = this.datatable.getSelectedItem();
                        this.datatable.updateItem(id, result);
                    }
                    this.okClose();
                }
            }).catch(reason => {
                console.log(reason);
            });
        }
    });

    protected cancelBtn = Btn.getCancelButton(this.viewName, () => {
        this.okClose();
    });

    getDataFromDialog(): AdminPanel.IBaTypesModel {

        const form = $$(this.formId) as any;

        let data = [];
        let self = this;

        form._collection.forEach(
            (val) => {
                let id = ($$(val.id).config.id as string).substring(0, ($$(val.id).config.id as string).length - "-field-id".length);
                id = id.substring(self.formId.length + 1);
                data[id] = ($$(val.id).config as any).value;
            }
        );

        let parrentName = form._cells[2].config.value;
        data["typeId"] = this.isEdit ? this._item.typeId : null;

        let result: AdminPanel.IBaTypesModel = {
            fullName: data["fullName"],
            isGroupType: data["isGroupType"] === undefined ? 0 : data["isGroupType"],
            isImplementType: data["isImplementType"] === undefined ? 0 : data["isImplementType"],
            shortName: data["shortName"],
            typeId: data["typeId"],
            isImplementTypeName: data["isImplementTypeName"],
            mvcAlias: data["mvcAlias"],
            name: data["name"],
            parent: parrentName,
            parentId: parrentName === "" ? null : Number(form._cells[2].config.markerId),
            searchType: "searchType",
            searchTypeId: Number(data["searchType"]),
            fixOperationHistory: data["fixOperationHistory"] === undefined ? 0 : data["fixOperationHistory"],
        } as any;

        return result;
    }

    showBaTypesChooseDlg() {
        const dlg = adminPanelListDlgView.getAdminPanelListDlgView(
            `${this.viewName}-dlg`,
            `${this.formId}-parent-superField`);
        dlg.showModalContent(this._tabModule, () => {
            dlg.close();
        });
        
    }

    clearParentBaType() {
        ($$(`${this.formId}-parent-superField`) as any).setValue("");
        ($$(`${this.formId}-parent-superField`) as any).markerId = null;
    }
}
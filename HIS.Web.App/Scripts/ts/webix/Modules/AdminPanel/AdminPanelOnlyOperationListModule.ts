﻿import Common = require("../../Common/CommonExporter");
import Mngr = require("../ModuleManager");
import Reg = require("../RegisterModules");
import adminPanelOnlyOperationList = require("../../Components/Forms/AdminPanel/AdminPanelOnlyOperationList");

export class AdminPanelOnlyOperationListModule extends Common.MainTabBase {

    protected static _viewName = adminPanelOnlyOperationList.modulesName;
    protected static _header = Reg.Register.modules[adminPanelOnlyOperationList.modulesName].header;

    private _adminPanelOnlyOperationList: adminPanelOnlyOperationList.IAdminPanelOnlyOperationList;

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
        this._adminPanelOnlyOperationList = adminPanelOnlyOperationList.getAdminPanelOnlyOperationList(this);
    }

    systemCompanyHandler(company: Inventory.ILSCompanyAreas): void {
        this._adminPanelOnlyOperationList.load();
        this._adminPanelOnlyOperationList.selectedCompany = company;
    }

    getContent() {
        return {
            rows: [
                this._adminPanelOnlyOperationList.init()
            ]
        }
    }

    ready(): void {
        this._adminPanelOnlyOperationList.ready();
    }
}

Mngr.ModuleManager.registerModule(AdminPanelOnlyOperationListModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new AdminPanelOnlyOperationListModule(mainView);
});

﻿import Base = require("../../Common/DialogBase");
import Common = require("../../Common/CommonExporter");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import Api = require("../../Api/ApiExporter");
import ToolButtons = require("../../Components/Buttons/ToolBarButtons");
import adminPanelOnlyMarkersListDlgView = require("../../Components/Forms/AdminPanel/AdminPanelOnlyMarkersListDlgView");

export function getMarkersAdminDlg(viewName: string, isEdit: boolean, markeresDatatable: webix.ui.datatable, selectedCompany: Inventory.ILSCompanyAreas, entityBaId: number) {
    return new MarkersAdminDlg(viewName, isEdit, markeresDatatable, selectedCompany, entityBaId);
}

class MarkersAdminDlg extends Base.DialogBase {
    private _item: AdminPanel.IMarkersAdminModel;
    protected get formId() {
        return `${this.viewName}-MarkersAdmin-Dlg-id`;
    };
    protected get labelWidth() {
        return 150 + 140;
    };

    constructor(viewName: string,
        private isEdit: boolean,
        private markeresDatatable: webix.ui.datatable,
        private selectedCompany: Inventory.ILSCompanyAreas,
        private entityBaId:number) {
        super(viewName);
        if (isEdit)
            this._item = markeresDatatable.getSelectedItem() as AdminPanel.IMarkersAdminModel;
    }

    private _tabModule: Common.IMainTab;

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._tabModule = module;
        this._tabModule.disableCompany();

        let control = $$(`${this.formId}-MarkerLink-superField`) as any;
        if (this.isEdit) {
            control.setValue(this._item.name);
            control.markerId = this._item.id;
        } else {
            control.setValue("");
            control.markerId = null;
        }
    }

    headerLabel(): string {
        return `${this.isEdit ? "Редактирование" : "Добавление"} маркера`;
    }

    contentConfig() {
        let field = `field-id`;
        return {
            view: "form",
            id: this.formId,
            minheight: 600,
            autoheight: true,
            elements: [
                {
                    view: "extsearch",
                    id: `${this.formId}-MarkerLink-superField`,
                    css: "protoUI-with-icons-2",
                    icons: ["search", "close"],
                    label: "Маркер",
                    labelWidth: this.labelWidth,
                    readonly: true,
                    name: `${this.formId}-MarkerLink-superFieldName`,
                    validate: (value) => {
                        return true;
                    },
                    on: {
                        onSearchIconClick: () => this.showSearchMarkerDlg(false),
                        onCloseIconClick: () => this.clearSearchMarkerField()
                    }
                },
                {
                    id: `${this.formId}-overrideName-${field}`,
                    label: "Переопределенное наименование",
                    labelWidth: this.labelWidth,
                    view: "text",
                    value: this.isEdit ? this._item.overrideName : "",
                },
                {
                    id: `${this.formId}-implementTypeName-${field}`,
                    label: "Таблица хранения значений",
                    labelWidth: this.labelWidth,
                    view: "text",
                    value: this.isEdit ? this._item.implementTypeName : "",
                },
                {
                    id: `${this.formId}-implementTypeField-${field}`,
                    label: "Поле хранения значений",
                    labelWidth: this.labelWidth,
                    view: "text",
                    value: this.isEdit ? this._item.implementTypeField : ""
                },
                {
                    id: `${this.formId}-isPeriodic-${field}`,
                    label: "Признак периодичности",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isPeriodic : ""
                },
                {
                    id: `${this.formId}-isCollectible-${field}`,
                    label: "Признак коллекции",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isCollectible : ""
                },
                {
                    id: `${this.formId}-isBlocked-${field}`,
                    label: "Признак блокировки заполнения",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isBlocked : ""
                },
                {
                    id: `${this.formId}-isFixMarkerHistory-${field}`,
                    label: "Признак фиксирования истории",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isFixMarkerHistory : 1
                },
                {
                    id: `${this.formId}-isImplementTypeField-${field}`,
                    label: "Признак реализованного типа",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isImplementTypeField : ""
                },
                //{
                //    id: `${this.formId}-isInheritToDescendant-${field}`,
                //    label: "Признак наследуемого маркера",
                //    labelWidth: this.labelWidth,
                //    view: "checkbox",
                //    value: this.isEdit ? this._item.isInheritToDescendant : ""
                //},
                {
                    id: `${this.formId}-isRequired-${field}`,
                    label: "Признак обязательности заполнения",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isRequired : ""
                },
                {
                    cols: [
                        {},
                        this.okBtn.init(),
                        this.cancelBtn.init()
                    ]
                }
            ],
        }
    }

    protected okBtn = Btn.getOkButton(this.viewName, () => {
        if (($$(this.formId) as webix.ui.form).validate()) {
            const data = this.getDataFromDialog();
            let baid = this.entityBaId;
            Api.ApiAdminPanel.addMarkerToBaType(baid, data).then(result => {
                if (result) {
                    if (!this.isEdit) {
                        let hasItem = this.markeresDatatable.find(x => x.id === result.id);
                        if (hasItem.length === 0)
                            this.markeresDatatable.add(result);
                        else 
                            webix.message("Маркер с таким ИД уже используется", "error");
                    } else {
                        let { id } = this.markeresDatatable.getSelectedItem();
                        this.markeresDatatable.updateItem(id, result);
                    }
                    this.markeresDatatable.refresh();
                    this.okClose();
                }
            });

        }
    });

    protected cancelBtn = Btn.getCancelButton(this.viewName, () => {
        this.okClose();
    });

    getDataFromDialog() {
        const form = $$(this.formId) as any;
        let data = [];
        let self = this;

        form._collection.forEach(
            (val) => {
                let id = ($$(val.id).config.id as string).substring(0, ($$(val.id).config.id as string).length - "-field-id".length);
                id = id.substring(self.formId.length + 1);
                data[id] = ($$(val.id).config as any).value;
            }
        );
        let markerName = form._cells[0].config.value;
        data["baTypeLinkId"] = Number(form._cells[0].config.markerId);
        let result: AdminPanel.IMarkersAdminModel = {
            name: data["overrideName"] === "" ? data["MarkerLink-s"] : data["overrideName"],
            id: this.isEdit? this._item.id : data["baTypeLinkId"],
            isPeriodic: Boolean(data["isPeriodic"]),
            isCollectible: Boolean(data["isCollectible"]),
            isBlocked: Boolean(data["isBlocked"]),
            isRequired: Boolean(data["isRequired"]),
            isFixMarkerHistory: Boolean(data["isFixMarkerHistory"]),
            isInheritToDescendant: Boolean(data["isInheritToDescendant"]),
            implementTypeName: data["implementTypeName"],
            implementTypeField: data["implementTypeField"],
        } as any;
        return result;
    }

    showSearchMarkerDlg(isEdit) {
        let control = $$(`${this.formId}-MarkerLink-superField`) as any;

        const dlg = adminPanelOnlyMarkersListDlgView.getAdminPanelOnlyMarkersListDlgView(
            `${this.formId}-search-dlg`,
            false,
            control,
            this.entityBaId,
            this._tabModule);
        dlg.showModalContent(this._tabModule, () => {
            dlg.close();
        } );
    }

    clearSearchMarkerField() {
        const field = $$(`${this.formId}-MarkerLink-superField`) as any;
        field.setValue("");
        field.markerId = null;
    }
}
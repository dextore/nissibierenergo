﻿import Base = require("../../Common/DialogBase");
import Common = require("../../Common/CommonExporter");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import Api = require("../../Api/ApiExporter");
import adminPanelOnlyOperationsListDlgView = require("../../Components/Forms/AdminPanel/adminPanelOnlyOperationsListDlgView");

export function getOperationsAdminDlg(viewName: string, isEdit: boolean, operationsDataTable: webix.ui.datatable, selectedCompany: Inventory.ILSCompanyAreas, entityBaId: number) {
    return new OperationsAdminDlg(viewName, isEdit, operationsDataTable, selectedCompany, entityBaId);
}

class OperationsAdminDlg extends Base.DialogBase {
    private _item: AdminPanel.IBaTypesOperationsModel;
    protected get formId() {
        return `${this.viewName}-OperationsAdminDlg-Dlg-id`;
    };
    protected get labelWidth() {
        return 150 + 140;
    };

    constructor(viewName: string,
        private isEdit: boolean,
        private operationDataTable: webix.ui.datatable,
        private selectedCompany: Inventory.ILSCompanyAreas,
        private entityBaId:number) {
        super(viewName);
        if (isEdit)
            this._item = operationDataTable.getSelectedItem() as AdminPanel.IBaTypesOperationsModel;
    }

    private _tabModule: Common.IMainTab;

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._tabModule = module;
        this._tabModule.disableCompany();

        let control = $$(`${this.formId}-OperationsAdminLink-superField`) as any;
        if (this.isEdit) {
            control.setValue(this._item.operationName);
            control.config.operationId = this._item.operationId;
        } else {
            control.setValue("");
            control.config.operationId = null;
        }
    }

    headerLabel(): string {
        return `${this.isEdit ? "Редактирование" : "Добавление"} операции`;
    }

    contentConfig() {
        let field = `field-id`;
        return {
            view: "form",
            id: this.formId,
            minheight: 600,
            autoheight: true,
            elements: [
                {
                    view: "extsearch",
                    id: `${this.formId}-OperationsAdminLink-superField`,
                    css: "protoUI-with-icons-2",
                    icons: ["search", "close"],
                    label: "Операция",
                    labelWidth: this.labelWidth,
                    readonly: true,
                    name: `${this.formId}-OperationsAdminLink-superFieldName`,
                    validate: (value) => {
                        return true;
                    },
                    on: {
                        onSearchIconClick: () => this.showSearchOperationDlg(false),
                        onCloseIconClick: () => this.clearSearchOperationField()
                    }
                },
                {
                    id: `${this.formId}-isCreateOperation-${field}`,
                    label: "Признак операции создания",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isCreateOperation : ""
                },
                {
                    id: `${this.formId}-isDeleteOperation-${field}`,
                    label: "Признак операции удаления (архивирования)",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isDeleteOperation : ""
                },
                {
                    cols: [
                        {},
                        this.okBtn.init(),
                        this.cancelBtn.init()
                    ]
                }
            ],
        }
    }

    protected okBtn = Btn.getOkButton(this.viewName, () => {
        if (($$(this.formId) as webix.ui.form).validate()) {
            const data = this.getDataFromDialog();
            let baid = this.entityBaId;
            Api.ApiAdminPanel.addOperationToBaType(baid, data).then(result => {
                if (result) {
                    if (!this.isEdit) {
                        let hasItem = this.operationDataTable.find(x => x.stateId === result.operationId);
                        if (hasItem.length === 0)
                            this.operationDataTable.add(result);
                        else 
                            webix.message("Операция с таким ИД уже используется", "error");
                    } else {
                        let { id } = this.operationDataTable.getSelectedItem();
                        this.operationDataTable.updateItem(id, result);
                    }
                    this.operationDataTable.refresh();
                    this.okClose();
                }
            });
            this.okClose();
        }
    });

    protected cancelBtn = Btn.getCancelButton(this.viewName, () => {
        this.okClose();
    });

    getDataFromDialog() {
        const form = $$(this.formId) as any;
        let data = [];
        let self = this;

        form._collection.forEach(
            (val) => {
                let id = ($$(val.id).config.id as string).substring(0, ($$(val.id).config.id as string).length - "-field-id".length);
                id = id.substring(self.formId.length + 1);
                data[id] = ($$(val.id).config as any).value;
            }
        );
        data["operationId"] = Number(form._cells[0].config.operationId);

        let result: AdminPanel.IBaTypesOperationsModel = {
            operationId: data["operationId"],
            operationName: data["OperationsAdminLink-s"],
            isCreateOperation: Boolean(data["isCreateOperation"]),
            isDeleteOperation: Boolean(data["isDeleteOperation"])
        };
        return result;
    }

    showSearchOperationDlg(isEdit) {
        let control = $$(`${this.formId}-OperationsAdminLink-superField`) as any;

        const dlg = adminPanelOnlyOperationsListDlgView.getAdminPanelOnlyOperationsListDlgView(
            `${this.formId}-search-dlg`,
            false,
            control,
            this.entityBaId,
            this._tabModule);

        dlg.showModalContent(this._tabModule, () => {
            dlg.close();
        } );
    }

    clearSearchOperationField() {
        const control = $$(`${this.formId}-OperationsAdminLink-superField`) as any;
        control.setValue("");
        control.config.stateId = null;
    }
}
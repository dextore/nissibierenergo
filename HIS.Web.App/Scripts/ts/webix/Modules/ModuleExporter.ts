﻿export * from "./TreeModuleBase"
export * from "./ModuleManager"
export * from "./LegalSubjects/LegalPersonsModule"
export * from "./LegalSubjects/LegalPersonsTreeModule"
export * from "./Objects/ObjectsModule"
export * from "./LegalSubjects/FizPersonTreeModule"
export * from "./Tests/TestsModule"
export * from "./Banks/BanksModule"
export * from "./Banks/BanksModuleTest"
// подключение справочников 
export * from "./Address/AddressMappingModule" // сопоставление адресов
export * from "./References/Nomenclature/InventoryModule" //Номенклатурный справочник 
//
export * from "./RealSchemeElements/RealSchemeElementModule" //элементы реальной схемы

export * from "./Documents/DocumentsModule" // Дерево документов
export * from "./ConsignmentNotes/ConsignmentNotesModule" // приходные накладные 

export * from "./InventoryObjectsMovementHistory/InventoryObjectsMovementHistoryModule" // движение ТМЦ 
export * from "./DocAcceptAssets/DocAcceptAssetsModule" // акт приема ТМЦ
export * from "./DocMoveAssets/DocMoveAssetsModule" //перемещение/выбытие инвентарных объектов

export * from "./Contracts/ContractsTreeModule" //Договор
export * from "./AdminPanel/AdminPanelModule"
export * from "./AdminPanel/AdminPanelOnlyMarkersListModule"
export * from "./AdminPanel/AdminPanelOnlyStateListModule"
export * from "./AdminPanel/AdminPanelOnlyOperationListModule"

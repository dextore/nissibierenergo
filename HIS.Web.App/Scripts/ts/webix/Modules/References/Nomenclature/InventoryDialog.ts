﻿import Base = require("../../../Common/DialogBase");
import Common = require("../../../Common/CommonExporter");
import Btn = require("../../../Components/Buttons/ButtonsExporter");
import Api = require("../../../Api/ApiExporter");
import MB = require("../../../Components/Markers/MarkerControlBase");
import Component = require("../../../Components/ComponentBase");
import MarkerFactory = require("../../../Components/Markers/MarkerControlFactory");
import EntityRefMarkerControl = require("../../../Components/Markers/EntityRefMarkerControl");

export function getInventoryDialog(viewName: string,
    module: Common.MainTabBase,
    markers: Markers.IMarkerInfoModel[],
    dataModel?: Inventory.IInventoryDataModel): IInventoryDialog {
    return new InventoryDialog(viewName, module, markers, dataModel);
};
export interface IInventoryDialog extends Base.IDialog {
    resultBaId:number;
}

class InventoryDialog extends Base.DialogBase {

    get formId() { return `${this.viewName}-form-id`; }

    constructor(viewName: string,
        private _module: Common.MainTabBase,
        private readonly _entityMarkers: Markers.IMarkerInfoModel[],
        private _dataModel?: Inventory.IInventoryDataModel) {
        super(viewName);
        this.setCreateState();
    }
    private setCreateState(): void {
        if (this.isEmptyObject(this._dataModel) || this._dataModel.soState === 2
            || this._dataModel.soState === null || this._dataModel.soState === undefined) {
            this.isCreate = true;
        } else {
            this.isCreate = false;
        }
    }
    private isCreate = false;
    private baId: number;
    //validate
    private markerOrder = [];
    private optionalMarkers = [] as Array<number>;

    protected headerLabel(): string {
        if (this.isCreate) {
            return "Создание номенклатурной единицы";
        }
        return "Редактирование номенклатурной единицы";     
    }
    get mainTabView(): Common.IMainTabbarView {
        return this._module.mainTabView;
    }
    get formControl() {
        return $$(this.formId) as webix.ui.form;
    }
    get resultBaId() {
        return this.baId;
    }
    protected fieldsInfo: { [id: string]: Markers.IMarkerInfoModel } = {};
    private markersControls: { [id: number]: MB.IMarkerControl } = {};
    protected get labelWidth() { return 280; }

   
    protected okBtn = Btn.getOkButton(this.viewName,
        () => {
            this.okBtn.button.disable();
            if (this.formControl.validate()) {

                if (!this.isChangeForm() && !this.isCreate) {
                    this.baId = this._dataModel.BAId;
                    this.okClose();
                    return;
                }
                const model = this.getFormData() as Inventory.IInventoryDataModel;
         
                Api.getApiNomenclature().inventoryUpdate(model).then((data: Inventory.IInventoryDataModel) => {
                    if (!this.isEmptyObject(data)) {
                        this.baId = model.BAId;
                        this.okClose();
                        return;
                    }
                }).catch((error) => {
                    console.log(error);
                });

             
            }      
            this.okBtn.button.enable();
        });
    protected cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });

    protected contentConfig() {
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
            ]
        };
    }
    protected windowConfig() {
        return {
            autoheight: true
        };
    }
    private checkSize() {
        const rect = $$(this.windowId).getNode().getBoundingClientRect();
        const diff = $$(this.formId).getNode().scrollHeight - $$(this.formId).getNode().clientHeight + 10;
        $$(this.windowId).$setSize(rect.width, rect.height + diff);
        ($$(this.windowId) as webix.ui.window).define("height", rect.height + diff);
    }

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._module.disableCompany();
    }

    destroy(): void {
        super.destroy();
        this._module.enableCompany();
    }

    bindModel(): void {

      
        let mvalue = this.getInventoryType();
        if (!mvalue) {
            mvalue = 0;
        }
        const baTypeId = 2;
        const markerId = 56
        this._entityMarkers.sort((first: Markers.IMarkerInfoModel, second: Markers.IMarkerInfoModel) => {

            const toEndIndex = 999999;
            let firstIndex = this.markerOrder.indexOf(first.id);
            let secondIndex = this.markerOrder.indexOf(second.id);

            if (firstIndex < 0) { firstIndex = toEndIndex; }
            if (secondIndex < 0) { secondIndex = toEndIndex; }

            if (firstIndex === secondIndex) { return 0; }
            if (firstIndex < secondIndex) { return -1 }

            return 1;

        });
        
        Api.getApiMarkers().getOptionalMarkers(baTypeId, markerId, mvalue).then((data: Markers.IOptionalMarkersModel[]) => {
            data.forEach(item => {
                if (item.isVisible) {
                    this.optionalMarkers.push(item.optionalMarkerId);
                }
            });
            this.loadFieldsInfo();
            this.setFormData();

        }).catch((error) => {
            console.log(error);
        });
    }
    private getInventoryType(): number {

        let result: number = 0;
        if (this.isEmptyObject(this._dataModel)) {
            return result;
        }
        result = this._dataModel.InventoryType;
        if (result > 0) {
            return result;
        }
        if (this.isEmptyObject(this._dataModel.markerValues)) {
            return result;
        }
        this._dataModel.markerValues.forEach(item => {
            if (item.markerId === 56) {
                result = parseInt(item.value);
                return;
            }
        }, this);

        return result
    }
    private loadFieldsInfo() {

        const maxItems: number = 17;
        let counter: number = 0;
        const cols = { cols: [] };
        let tempColumn = { rows: [] };

        this._entityMarkers.forEach(item => {
            const marker = this.createMarkerControl(item);
            if (marker) {
                if ("baId" in marker) {
                    (marker as any).baId = this._dataModel.BAId;
                }

                if (item.markerType === 11) {
                    (marker as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
                    (marker as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this._module);
                }
                this.markersControls[item.id] = marker as any;
                this.fieldsInfo[item.id] = item;
                tempColumn.rows.push(marker.init());
                counter++;
                if (counter === maxItems) {
                    cols.cols.push(tempColumn);
                    tempColumn = { rows: [] };
                    counter = 0;
                }
            }
        }, this);


        // заполняем пустым местом если маркеров меньше чем максимальное значение
        if (counter < maxItems && counter !== 0) {
            for (var i = counter; i < maxItems; i++) {
                tempColumn.rows.push({});
            }
            cols.cols.push(tempColumn);
        }

        ($$(this.formId) as webix.ui.form).addView(cols);

        ($$(this.formId) as webix.ui.form).addView({
            cols: [
                {},
                this.okBtn.init(),
                this.cancelBtn.init()
            ]
        });

        if (cols.cols.length === 2) {
            this.window.define("width", 1200);
            this.window.resize();
        } else if (cols.cols.length > 2) {
            this.window.define("fullscreen", true);
            this.window.resize();
        }
        this.checkSize();
    }
    private createMarkerControl(model: Markers.IMarkerInfoModel): Component.IComponent {

        const elementId = `${this.viewName}-form`;
        const self = this;
        let isDisabled: boolean = false;

        // проверяем нахождения маркера в основных и опциональных 
        if (model.isOptional && this.optionalMarkers.indexOf(model.id) < 0) {
            return null;
        }
        if (this.isCreate) {
            isDisabled = false;
        } else if (model.isBlocked) {
            isDisabled = true;
        }
        if (model.id === 56 || model.id === 6) {
            isDisabled = true;
        }

        /*
        let filterValue = null;
        if (model.id === 54) {        
            filterValue = this.dataModel.CompanyArea;
        }
        */
        return MarkerFactory.MarkerControlfactory.getControl(elementId,
            model,
            (self) => {
                return { labelWidth: this.labelWidth, disabled: isDisabled }
            });
    }

    private getFormData(): Inventory.IInventoryDataModel{
        const caId = Number(this.markersControls[6].getValue(this.fieldsInfo).value);
        const inventoryId = Number(this.markersControls[54].getValue(this.fieldsInfo).value);

        const data = ($$(this.formId) as webix.ui.form).getValues() as { [id: string]: any };
        const model = {
            BAId: this._dataModel.BAId,
            Name: data["Name"] as string,
            Code: data["Code"],
            CompanyArea: (!caId) ? null : caId,
            BuyNatMeaning: data["BuyNatMeaning"],
            SaleNatMeaning: data["SaleNatMeaning"],
            StockNatMeaning: data["StockNatMeaning"],
            Inventory: (!inventoryId) ? null : inventoryId,
            InventoryKind: data["InventoryKind"],
            InventoryType: data["InventoryType"],
            soState: this._dataModel.soState,
            ResourceTypeId: data["ResourceType"],
            markerValues: []
        } as Inventory.IInventoryDataModel;

        // rewrite algoritm 

        for (let id in this.fieldsInfo) {
            if (this.fieldsInfo.hasOwnProperty(id)) {

                const markerInfo = this.fieldsInfo[id];
                const value = this.markersControls[id].getValue(this.fieldsInfo);
                const marker = {
                    markerId: markerInfo.id,
                    itemId: null,
                    //startDate: (!value.startDate) ? data[`${markerInfo.name}_date`]: value.startDate,
                    startDate: data[`${markerInfo.name}_date`],
                    endDate: null,
                    value: (!value.value) ? data[markerInfo.name] : value.value,
                    note: "",
                    displayValue: "",
                    isDeleted: false
                } as Markers.IMarkerValueModel;

                if (this.isSaveMarker(marker)) {
                    model.markerValues.push(marker);
                }
            }
        }
        return model;
    }
    private isSaveMarker(marker: Markers.IMarkerValueModel): boolean {

        const tableFields = [1, 3, 6, 51, 52, 53, 54, 56, 57 , 58];
        if (tableFields.indexOf(marker.markerId) >= 0) {
            return false;
        }
        if ((marker.value === "" || marker.value === null || marker.value === undefined)) {
            return false;
        }
        if ((this._dataModel.soState === 1 || this._dataModel.soState === 2) && !this.markersControls[marker.markerId].isChanged) {
            return false;
        }
        return true;

    }


    private setFormData() {

        if (!this._dataModel) {
            return;
        }
        // create set model 
        const dataModel = this.initializeSetDataModel();
        //при копировании обнуляем наименование и код
        if (dataModel["BAId"] === null) {
            dataModel["Code"] = "";
            dataModel["Name"] = "";
        }
        if (dataModel["InventoryKind"] == undefined) {
            dataModel["InventoryKind"] = 0;
        }
        this.formControl.setValues(dataModel);
        this.formControl.clearValidation();
       

    }
    private getMarkerInfoById(markerId: number): Markers.IMarkerInfoModel {
        for (const key in this.fieldsInfo) {

            const item = this.fieldsInfo[key] as Markers.IMarkerInfoModel;
            if (item.id === markerId) {
                return item;
            }
        }
        return {} as Markers.IMarkerInfoModel;
    }
    private initializeSetDataModel(): object {
        
        let model: { [id: string]: any } = {};
        for (const key in this._dataModel.markerValues) {
            const item = this._dataModel.markerValues[key] as Markers.IMarkerValueModel;
            const markerinfo = this.getMarkerInfoById(item["markerId"]) as Markers.IMarkerInfoModel;
            (this.markersControls[markerinfo.id]).setInitValue(item, model);
        }
        for (const key in this._dataModel) {
            if (this._dataModel.hasOwnProperty(key) && key !== "markerValues") {
                model[key] = this._dataModel[key];
            }
        }

        let info = this.markersControls[6].markerInfo;
        this.markersControls[6].setInitValue(this.getMarkerValue(info, this._dataModel.CompanyArea as any, this._dataModel.CompanyName), model);

        info = this.markersControls[51].markerInfo;
        this.markersControls[51].setInitValue(this.getMarkerValue(info, this._dataModel.BuyNatMeaning as any, null ), model);

        info = this.markersControls[52].markerInfo;
        this.markersControls[52].setInitValue(this.getMarkerValue(info, this._dataModel.SaleNatMeaning as any, null), model);

        info = this.markersControls[53].markerInfo;
        this.markersControls[53].setInitValue(this.getMarkerValue(info, this._dataModel.StockNatMeaning as any, null), model);
        
        info = this.markersControls[54].markerInfo;
        this.markersControls[54].setInitValue(this.getMarkerValue(info, this._dataModel.Inventory as any, this._dataModel.InventoryName), model);

        info = this.markersControls[58].markerInfo;
        this.markersControls[58].setInitValue(this.getMarkerValue(info, this._dataModel.ResourceTypeId as any, null), model);

        /*
        старый макет заполнения формы , нужно переписать 
        for (const key in this.dataModel) {
            if (this.dataModel.hasOwnProperty(key) && key !== "markerValues") {
                model[key] = this.dataModel[key];
            }
        }
        for (const key in this.dataModel.markerValues) {
            const item = this.dataModel.markerValues[key];
            const markerinfo = this.getMarkerInfoById(item["markerId"]);
            model[markerinfo.name] = item["value"];
            if (markerinfo.isPeriodic) {
                model[markerinfo.name + "_date"] = new Date(item["startDate"].toString());
            }
        }
        */
        return model;
    }
    private getMarkerValue(info: Markers.IMarkerInfoModel, value: string, displayValue: string): Markers.IMarkerValueModel {
        return {
            displayValue: displayValue,
            itemId: null,
            endDate: null,
            markerId: info.id,
            markerType: info.markerType,
            MVCAliase: info.name,
            note: null,
            isVisible: true,
            isBlocked: false,
            startDate: null,
            value: value,
            isDeleted: false
        }
    }
    
    private isChangeForm(): boolean {

        for (let key in this.markersControls) {
            if (this.markersControls.hasOwnProperty(key)) {
                if (this.markersControls[key].isChanged === true) {
                    return true;
                }
            }
        }
        return false;
    }
    private isEmptyObject(obj: object): boolean {

        if (obj == null || obj == undefined) {
            return true;
        }
        if (Object.keys(obj).length === 0) {
            return true;
        }
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }

}

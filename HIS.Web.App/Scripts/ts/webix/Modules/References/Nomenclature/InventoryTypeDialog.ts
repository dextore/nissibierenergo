﻿import Base = require("../../../Common/DialogBase");
import Btn = require("../../../Components/Buttons/ButtonsExporter");
import Api = require("../../../Api/ApiExporter");
import Marker = require("../../../Components/Markers/MarkersExporter");
import Component = require("../../../Components/ComponentBase");
import Common = require("../../../Common/CommonExporter");

export function getInventoryTypeDialog(viewName: string, mainTabView: Common.IMainTabbarView): IInventoryTypeDialog {
    return new InventoryTypeDialog(viewName, mainTabView);
};
export interface IInventoryTypeDialog extends Base.IDialog {
    selected: number;
}

class InventoryTypeDialog extends Base.DialogBase {

    get formId() { return `${this.viewName}-form-id`; }

    constructor(viewName: string, private _mainTabView: Common.IMainTabbarView ) {
        super(viewName);
    }
    protected headerLabel(): string {
        return "Создание номенклатурной единицы";
    }
    protected get labelWidth() { return 240; }

    public get selected(): number { return this.inventoryType; }
    private inventoryType: number = 0;


    protected okBtn = Btn.getOkButton(this.viewName,
        () => {
            if (!($$(this.formId) as webix.ui.form).validate()) {
                return;
            }
            const data = ($$(this.formId) as webix.ui.form).getValues() as { [id: string]: any };
            this.inventoryType = parseInt(data["InventoryType"]);
            this.okClose();
        });
    protected cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });

    protected contentConfig() {
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
            ]
        };
    }
    protected windowConfig() {
        return {
            autoheight: true,
        };
    }

   

    bindModel(): void {

        Api.getApiMarkers().getEntityMarkersInfo(2).then((data) => {

            data.forEach(item => {
                // inventory type marker 
                if (item.id == 56)
                {
                    item.isRequired = true;
                    const marker = this.createMarkerControl(item);
                    ($$(this.formId) as webix.ui.form).addView(marker.init());

                    ($$(this.formId) as webix.ui.form).addView({
                        cols: [
                            {},
                            this.okBtn.init(),
                            this.cancelBtn.init()
                        ]
                    });

                }                                                                                  
            }, this);
        });       
    }

    private createMarkerControl(model: Markers.IMarkerInfoModel): Component.IComponent {
        const elementId = `${this.viewName}-${model.name}-id`;
        return Marker.getMarkerListControl(elementId, model, (self) => { return { labelWidth: this.labelWidth, disabled: false } });
    }


    

}

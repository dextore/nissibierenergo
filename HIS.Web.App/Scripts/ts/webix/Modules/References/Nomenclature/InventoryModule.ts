﻿import Common = require("../../../Common/CommonExporter");
import Mngr = require("../../ModuleManager");
import ListView = require("./InventoryListView");

export class InventoryModule extends Common.MainTabBase {

    protected static _header = "Номенклатурный справочник";
    protected static _viewName = "references-inventory-module";

    private selectedCompany: Inventory.ILSCompanyAreas;
    private listView: ListView.IInventoryList;

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
        this.listView = ListView.getInventoryList(`${this.viewName}-list`, this);

    }
    systemCompanyHandler(company: Inventory.ILSCompanyAreas) {
        this.listView.changeCompany(company);
    }
    getContent(): object {
        return this.listView.init();
    }
    ready() {
        this.listView.ready();
    }

     
}


Mngr.ModuleManager.registerModule(InventoryModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new InventoryModule(mainView);
});
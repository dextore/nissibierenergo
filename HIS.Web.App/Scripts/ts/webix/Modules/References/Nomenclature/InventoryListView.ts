﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../../Components/ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import ToolButtons = require("../../../Components/Buttons/ToolBarButtons");
import Common = require("../../../Common/CommonExporter");
import StateEntity = require("../../../Components/Controls/EntityOperationStateControl");
import Dialog = require("./InventoryDialog");
import TypeDialog = require("./InventoryTypeDialog");
import DataTable = require("../../../Components/QueryBuilderListBase/QueryBuilderListBase");
import IQueryBuilderListSetParameters = DataTable.IQueryBuilderListSetParameters;

export interface IInventoryList extends Base.IComponent {
    changeCompany(company: Inventory.ILSCompanyAreas);
    ready();
}

export function getInventoryList(_viewName: string, module: Common.MainTabBase) {
    return new InventoryList(_viewName, module);
}

export class InventoryList extends Base.ComponentBase implements IInventoryList {
    private readonly _entityAlias = "Inventory";
    private _selectedCompany: Inventory.ILSCompanyAreas;
    private _entityInfo = Common.entityTypes().getByAliase(this._entityAlias).entityInfo;
    private _operationState: StateEntity.IEntityOperationStateControl;
    private _tableBuilderControl: DataTable.IQueryBuilderList;

    // query builder list parameters 
    private _showBaId: boolean = false;
    private _aliasesWithoutFilter = ["CompanyArea"];
    private _aliasesOrder = [
        "BAId",
        "Name",
        "PrintName",
        "Code",
        "Inventory",
        "InventoryKind",
        "InventoryType",
        "BuyNatMeaning",
        "SaleNatMeaning",
        "StockNatMeaning",
        "ResourceType",
        "CheckInterval",
        "InventoryGroup",
        "State",
        "CompanyArea"];
   
    private get tableId() { return `${this._viewName}-datatable-id`; }
 
    get tableControl() {
        return $$(this.tableId) as webix.ui.datatable;
    }
    protected toolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}-tooldar`, "Добавить", this.createEntity.bind(this)),
        edit: ToolButtons.getEditBtn(`${this._viewName}-tooldar`, "Изменить", this.editEntity.bind(this)),
        copy: ToolButtons.getCopyBtn(`${this._viewName}-tooldar`, "Копировать", this.copyEntity.bind(this))
    }
    constructor(private readonly _viewName: string, private readonly _module: Common.MainTabBase) {
        super();
        this.initSelectedCompany();
        this.initOperationState();
        this.initDataTableControl();
    }
    private initDataTableControl() {

        const queryBuilderParam = {
            showBaId: this._showBaId,
            tooltip: true,
            entityInfo: this._entityInfo,
            aliasesOrder: this._aliasesOrder,
            aliasesWithoutFilter: this._aliasesWithoutFilter

        } as IQueryBuilderListSetParameters;

        this._tableBuilderControl = DataTable.getQueryBuilderList(this.tableId, queryBuilderParam);
        const companyField = this._tableBuilderControl.getCompanyAliase();
        if (companyField != "") {
            this._tableBuilderControl.setFilteringFields([{
                field: companyField,
                filterOperator: 0,
                value: this._selectedCompany.id
            }]);
        }
    }
    private initSelectedCompany() {
        this._selectedCompany = this._module.mainTabView.getSelectedCompany();
    }
    private initOperationState(): void {
        this._operationState = StateEntity.getEntityOperationStateControl(`${this._viewName}-operation-state`,
            "",
            () => {
                return true;
            });

        this.subscribe(this._operationState.statusChangedEvent.subscribe((data) => {
            
            const baId = this._operationState.state.baId;
            const selectId = this.tableControl.getSelectedId(false, true);

            switch (data) {
                case StateEntity.StateChangeAction.Changed:
                case StateEntity.StateChangeAction.Canceled:
                    this.updateTableRecord(baId, selectId);
                    break;
            }
            this.resetButtons();
           
        }, this));
        this._operationState.addButtons(this.toolBar.add, this.toolBar.edit, this.toolBar.copy);
    }
    private updateTableRecord(baId: number, recId?: number) {

        this._tableBuilderControl.getRowDataByBaId(baId).then(data => {
            const row = data.data[0];
            if (!recId) {
                this.tableControl.add(row, 0);
                this._tableBuilderControl.selectFirstRow();
            } else {
                this.tableControl.updateItem(recId, row);
            }
        });
    }
    private createEntity() {

        const typeDialog = TypeDialog.getInventoryTypeDialog(`${this._viewName}-inventory-type-dialog`, this._module.mainTabView);
        typeDialog.showModalContent(this._module, () => {
            typeDialog.close();
            const item = {} as Inventory.IInventoryDataModel;
            item.InventoryType = typeDialog.selected;
            if (typeDialog.selected <= 0) {
                return;
            }
            item.CompanyArea = this._selectedCompany.id;
            item.CompanyName = this._selectedCompany.name;

            const dialog = Dialog.getInventoryDialog(`${this._viewName}-dialog`,
                this._module,
                this._entityInfo.markersInfo,
                item);

            dialog.showModalContent(this._module, () => {
                dialog.close();
                this.updateTableRecord(dialog.resultBaId);
            });

        });    

    }
    private editEntity() {

        const selected = this.tableControl.getSelectedItem();
        const selectId = this.tableControl.getSelectedId(false, true);
        if (!selected) {
            webix.message("Необходимо выбрать элемент для редактирования", "error");
            return;
        }
        const baId = selected[this._tableBuilderControl.getBAIdAliase()] as number;

        Api.getApiNomenclature().getInventoryDataModel(baId).then((data: Inventory.IInventoryDataModel) => {
          
            const dialog = Dialog.getInventoryDialog(`${this._viewName}-dialog`,
                this._module,
                this._entityInfo.markersInfo,
                data);
            
            dialog.showModalContent(this._module, () => {
                dialog.close();
                this.updateTableRecord(dialog.resultBaId, selectId);
            });
        });
    }
    private copyEntity() {

        const selected = this.tableControl.getSelectedItem();
        if (!selected) {
            webix.message("Необходимо выбрать элемент для копирования", "error");
            return;
        }
        const baId = selected[this._tableBuilderControl.getBAIdAliase()] as number;
        
        Api.getApiNomenclature().getInventoryDataModel(baId).then((data: Inventory.IInventoryDataModel) => {
            data.BAId = null;
            data.Name = "";
            data.Code = "";
            data.soState = null;

            const dialog = Dialog.getInventoryDialog(`${this._viewName}-dialog`,
                this._module,
                this._entityInfo.markersInfo,
                data);


            dialog.showModalContent(this._module, () => {
                dialog.close();
                this.updateTableRecord(dialog.resultBaId);
            });
        });

    }
    private resetButtons() {
        
        this.toolBar.add.button.enable();
        this.toolBar.edit.button.disable();
        this.toolBar.copy.button.disable();
        const selected = this.tableControl.getSelectedItem() as any;
        if (!selected || !this._operationState.state) {
            return;
        }
        if (this._operationState.state.stateId === 1 || this._operationState.state.stateId === 2) {
            this.toolBar.edit.button.enable();
        }
        this.toolBar.copy.button.enable();
        
    }
    changeCompany(company: Inventory.ILSCompanyAreas) {
        this._selectedCompany = company;
        const companyField = this._tableBuilderControl.getCompanyAliase();
        if (companyField != "") {
            this._tableBuilderControl.setFilteringFields([{
                field: companyField,
                filterOperator: 0,
                value: this._selectedCompany.id
            }]);
            this._tableBuilderControl.reload();
        }
    }
    ready() {
        this.resetButtons();
        this._tableBuilderControl.ready();
        this.tableControl.attachEvent("onSelectChange", this.onSelectChangeTable.bind(this));
    }
    private onSelectChangeTable() {
        const selected = this.tableControl.getSelectedItem() as any;
        if (!selected) {
            return;
        }
        const baid = selected[this._tableBuilderControl.getBAIdAliase()];
        if (!baid) {
            this._operationState.baId = null;
            return;
        } 
        this._operationState.baId = baid;
    }

    init() {
    
        return {
            rows: [
                this._operationState.init(),
                this._tableBuilderControl.init()
            ]
        };
        
    }

}
﻿import Common = require("../../Common/CommonExporter");
import Mngr = require("../ModuleManager");
import Reg = require("../RegisterModules");
import Tree = require("./DocumentsTreeView");
import List = require("./DocumentsListView");
import Toolbar = require("./DocumentsToolBarView");
import ToolBtn = require("../../Components/Buttons/ToolBarButtons");
import History = require("../../Components/History/HistoryEntityDialog");
import Tran = require("./InventoryTransactions/InventoryTransactionsDlg");

export const modulName = "documents-tree-module";

export class DocumentsModule extends Common.MainTabBase {

    protected static _viewName = modulName;
    protected static _header = "Документы";

    private _tree = Tree.getDocumentListView(this, this.viewName);
    private _list = List.getDocumentListView(this, this.viewName);
    private _toolbar = Toolbar.getDocumentsToolBarView(this, this.viewName);

    // Проводки
    private _transactionBtn: ToolBtn.IToolBarButton;
    // История
    private _historyBtn: ToolBtn.IToolBarButton;

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
        this.subscribe(this._toolbar.filterChangeEvent.subscribe((data: Toolbar.IDocumentsFilterData) => {
            this._tree.filter = data;
        }, this));
        this.subscribe(this._tree.selectedTreeItemChange.subscribe((data: Documents.IDocumentTreeItemModel) => {
            this._list.filterData = {
                docType: data,
                companyId: this._tree.filter.companyId,
                startDate: this._tree.filter.startDate,
                endDate: this._tree.filter.endDate
            }
        }, this));

        this._historyBtn = ToolBtn.getHistoryBtn(this.viewName, "История изменений", () => {
            const docInfo = this._list.selectedDocument;
            if (!docInfo)
                return;

            const dialog = History.getHistoryEntityDialog(`${this.viewName}-dialog`, null, docInfo.baId);
            dialog.showModalContent(this, () => {
                dialog.close();
            });
        }); 

        this._transactionBtn = ToolBtn.getInventoryTransactionBtn(this.viewName, "Проводки по документу", 
            () => {
                const docInfo = this._list.selectedDocument;
                if (!docInfo)
                    return;

                const dialog = Tran.getInventoryTransactionsDlg(`${this.viewName}-tran-dialog`, 
                    docInfo.baId,
                    `<b>№ ${docInfo.name} от ${webix.i18n.dateFormatStr(new Date(docInfo.docDate))}</b>`);
                dialog.showModalContent(this, () => {
                    dialog.close();
                });

            });
    }

    systemCompanyHandler(company: Inventory.ILSCompanyAreas) {
        this._toolbar.company = company;
    }

    ready(): void {
        this._list.ready();
        ($$(this._toolbar.toolBarId) as webix.ui.toolbar).addView(this._historyBtn.init());
        this._historyBtn.button.enable();
        ($$(this._toolbar.toolBarId) as webix.ui.toolbar).addView(this._transactionBtn.init());
        this._transactionBtn.button.enable();
    }

    getContent() {
        return {
            rows: [
                this._toolbar.init(),
                {
                    cols: [
                        this._tree.init(),
                        { view: "resizer" },
                        { gravity: 4, cols: [this._list.init()] }
                    ],
                }
            ], 
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        };
    }
}

Mngr.ModuleManager.registerModule(DocumentsModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new DocumentsModule(mainView);
});

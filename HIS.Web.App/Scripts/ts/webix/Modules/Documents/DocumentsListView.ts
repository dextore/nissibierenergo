﻿import Base = require("../../Components/ComponentBase");
import Pager = require("../../Components/Pager/Pager");
import Common = require("../../Common/CommonExporter");
import Code = require("./DocumentsListViewCode"); 

export interface IDocumentListFilterData {
    docType: Documents.IDocumentTreeItemModel;
    companyId: number;
    startDate: Date;
    endDate: Date;
}

export interface IDocumentInfo {
    baId: number;
    docDate: string;
    name: string;
}

export interface IDocumentListView extends Base.IComponent {
    filterData: IDocumentListFilterData;
    ready();
    selectedDocument: IDocumentInfo;
}

export function getDocumentListView(module: Common.MainTabBase, viewName: string): IDocumentListView {
    return new DocumentListView(module, viewName);
}

class DocumentListView extends Base.ComponentBase implements IDocumentListView {

    private _filterData: IDocumentListFilterData;
    private _columnsInfo: Code.IColumnsInfo;

    get viewName() {
        return this._viewName;
    }

    get dataTableId() {
        return `${this.viewName}-datatable-id`;
    }

    get pagerId() {
        return `${this.viewName}-pager-id`;
    }

    get filterData() {
        return this._filterData;
    }

    set filterData(value: IDocumentListFilterData) {
        this._filterData = value;
        this.refreshData();
    }

    get dataTableControl() {
        return $$(this.dataTableId) as webix.ui.datatable;
    }

    get selectedDocument(): IDocumentInfo {
        const rows = this.dataTableControl.getSelectedItem(true);
        if (!rows.length)
            return null;

        const name = Code.DocumentsListViewCode.getFieldAlias(1, this._columnsInfo);
        const docDate = Code.DocumentsListViewCode.getFieldAlias(17, this._columnsInfo);

        const row = rows[0];

        return {
            baId: row[this._columnsInfo.baIdAlias],
            name: row[name],
            docDate: row[docDate]
        };
    }

    constructor(private readonly _module: Common.MainTabBase, private readonly _viewName: string) {
        super();
    }

    private refreshData() {
        //webix.message((!this._filterData) ? "NULL" : this._filterData.docType.name);

        this.clearData();
        if (!this._filterData.docType)
            return;

        Code.DocumentsListViewCode.getFields(this._filterData.docType.baTypeId).then(data => {
            this._columnsInfo = data;
            this.dataTableControl.define("columns", this._columnsInfo.columns);
            this.dataTableControl.refreshColumns();
            this.dataTableControl.load(this.dataTableControl.config.url);
        });
    }

    private clearData() {
        this.dataTableControl.clearAll();
        const columns = [];
        this.dataTableControl.define("columns", columns);
        this.dataTableControl.refreshColumns();
    }

    ready() {
        webix.extend(this.dataTableControl, webix.ProgressBar);
    }

    init() {
        const self = this;
        //15/3
        const pager = Pager.getDataTablePager(this.pagerId);
        return {
            rows: [
                dataTableInit(),
                pager.init()               
            ]
        }
        function dataTableInit() {
            return {
                view: "datatable",
                id: self.dataTableId,
                select: "row",
                //scroll: "y",
                resizeColumn: true,
                pager: self.pagerId,
                datafetch: 15,
                url: {
                    $proxy: true,
                    load: (view, callback, params) => {
                        if ((self.dataTableControl as any).showProgress)
                            (self.dataTableControl as any).showProgress();
                        Code.DocumentsListViewCode.getData(view, params,
                            self._columnsInfo,
                            (self._filterData) ? self._filterData.companyId: null,
                            (self._filterData) ? self._filterData.startDate: null,
                            (self._filterData) ? self._filterData.endDate : null)
                            .then(data => {
                                (webix.ajax as any).$callback(view, callback, data);
                                if ((self.dataTableControl as any).hideProgress)
                                    (self.dataTableControl as any).hideProgress();
                            }).catch(() => {
                                if ((self.dataTableControl as any).hideProgress)
                                    (self.dataTableControl as any).hideProgress();
                            });
                    }
                }
            }
        }
    }

}
﻿import Base = require("../../Components/ComponentBase");
import Common = require("../../Common/CommonExporter");

export interface IDocumentsFilterData {
    companyId: number;
    startDate: Date;
    endDate: Date;
    setFilter: boolean;
}

export interface IDocumentsToolBarView extends Base.IComponent {
    company: Inventory.ILSCompanyAreas;
    filterChangeEvent: Common.Event<IDocumentsFilterData>;
    toolBarId: string;
}

export function getDocumentsToolBarView(module: Common.MainTabBase, viewName: string): IDocumentsToolBarView {
    return new DocumentsToolBarView(module, viewName);
}

class DocumentsToolBarView extends Base.ComponentBase implements IDocumentsToolBarView {

    private _company: Inventory.ILSCompanyAreas;
    private _filterChangeEvent = new Common.Event<IDocumentsFilterData>();

    get viewName() {
        return this._viewName;
    }

    get toolBarId() {
        return `${this._viewName}-toolbar-id`;
    }

    get checkboxId() {
        return `${this._viewName}-checkbox-id`;
    }

    get startDateId() {
        return `${this._viewName}-start-datepicker-id`;
    }

    get endDateId() {
        return `${this._viewName}-end-datepicker-id`;
    }

    get company(): Inventory.ILSCompanyAreas {
        return this._company;
    }

    set company(value: Inventory.ILSCompanyAreas) {
        this._company = value;
        this.sentFilterData();
    }

    get filterChangeEvent() {
        return this._filterChangeEvent;
    } 

    constructor(private readonly _module: Common.MainTabBase, private readonly _viewName: string) {
        super();
    }

    private sentFilterData() {
        const filter = (($$(this.checkboxId) as webix.ui.checkbox).getValue() as any) !== 0;
        const msg: IDocumentsFilterData = {
            companyId: this.company.id,
            startDate: new Date(($$(this.startDateId) as webix.ui.datepicker).getValue()),
            endDate: new Date(($$(this.endDateId) as webix.ui.datepicker).getValue()),
            setFilter: filter
        };
        this.filterChangeEvent.trigger(msg, this);
    }

    init() {
        const endDate = new Date();
        const startDate = new Date();
        startDate.setMonth(startDate.getMonth() - 1);

        return {
            view: "toolbar",
            height: 40,
            id: this.toolBarId,
            elements: [{width: 20},
                {
                    view: "checkbox",
                    id: this.checkboxId,
                    label: "Только существующие документы: ",
                    width: 240,
                    labelWidth: 220,
                    value: 0,
                    on: {
                        onChange: (newv, oldv) => {
                            //webix.message(newv);
                            this.sentFilterData();
                        }
                    }
                },
                {},
                {
                    view: "datepicker",
                    id: this.startDateId,
                    value: startDate,
                    label: "Период с: ",
                    width: 200,
                    labelWidth: 70,
                    icons: false,
                    on: {
                        onChange: (newv, oldv) => {
                            this.sentFilterData();
                        }
                    }
                },
                {width: 20},
                {
                    view: "datepicker",
                    id: this.endDateId,
                    value: endDate,
                    label: "по: ",
                    width: 160,
                    labelWidth: 30,
                    icons: false,
                    on: {
                        onChange: (newv, oldv) => {
                            this.sentFilterData();
                        }
                    }
                },    
                {}
            ]
        }
    }
}
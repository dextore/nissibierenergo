﻿import Api = require("../../Api/ApiExporter");

export interface ITreeDataItem {
    id: number,
    title: string,
    parent_id: number,
    data: ITreeDataItem[],
    item: Documents.IDocumentTreeItemModel
}

export function loadTreeItems(companyAreaId: number = null, startDate: Date = null, endDate: Date = null): Promise<ITreeDataItem[]> {

    const p = webix.promise.defer();

    const promise = (!companyAreaId)
        ? Api.ApiDocuments.getTreeItem()
        : Api.ApiDocuments.getFilteredTreeItems(companyAreaId, startDate, endDate);

    promise.then(items => {
        let idx = 0;
        const parentsMap: { [id: string]: any } = {};
        const data = [];

        items.forEach(item => {
            ++idx;
            const result = {
                id: idx, //item.baTypeId,
                title: item.name,
                parent_id: item.parentId === null ? 0 : parentsMap[item.parentId.toString()],
                data: [],
                item: item
            }

            parentsMap[item.baTypeId.toString()] = result;

            if (item.parentId) {
                const parent = parentsMap[item.parentId.toString()];
                if (parent)
                    parent.data.push(result);
            } else {
                data.push(result);
            }
        });
        (p as any).resolve(data);

    }).catch(err => {
        (p as any).reject();
    });

    return p;
}

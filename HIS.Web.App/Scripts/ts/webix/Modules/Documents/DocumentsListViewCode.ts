﻿import Api = require("../../Api/ApiExporter");

export interface IColumnsInfo {
    baTypeId: number,
    baIdAlias: string,
    baIdColumnInfo: Querybuilder.ICaptionAndAliaseModel, 
    columns: any[],
    columnInfo: { [id: string]: Querybuilder.ICaptionAndAliaseModel }
}


export class DocumentsListViewCode {

    private static _hiddenColumnCaption = { "BATypeId": 1, "IsFirstState": 1, "IsDelState": 1 }
    private static _hiddenMarkerTypes = [11,12,13];

    static getFields(baTypeId: number): Promise<IColumnsInfo> {

        const result = {
            baTypeId: baTypeId,
            baIdAlias: "",
            baIdColumnInfo: null,
            columns: [],
            columnInfo: {}
        };

        return Api.getApiQueryBuilder().getFields(baTypeId).then((data: Querybuilder.ICaptionAndAliaseModel[]) => {

            data.forEach(item => {
                if (item.aliase.indexOf("BAId") >= 0) {
                    result.baIdAlias = item.aliase;
                    result.baIdColumnInfo = item;
                }
                result.columnInfo[item.aliase] = item;
                if (DocumentsListViewCode._hiddenColumnCaption[item.caption]) {
                    return;
                }
                if (DocumentsListViewCode._hiddenMarkerTypes.indexOf(item.markerTypeId) >= 0) {
                    return;
                }
                let columnWidth = webix.html.getTextSize(item.caption, "webix_hcell").width;
                columnWidth = (columnWidth < 100) ? 100 : columnWidth;
                
                switch (item.type) {
                    case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Boolean:
                    case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Number:
                    case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.String:
                        result.columns.push({
                            id: item.aliase,
                            header: [item.caption, { content: "serverFilter" }],
                            width: columnWidth,
                            sort: "server"
                        });
                        break;
                    case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Date:
                        result.columns.push({
                            id: item.aliase,
                            header: [item.caption, { content: "datepickerFilter" }],
                            width: columnWidth,
                            template: (row, common) => {
                                if (row[item.aliase])
                                    return webix.i18n.dateFormatStr(new Date(row[item.aliase]));
                                return "";
                            },
                            sort: "server"
                        });
                        break;
                }
            });

            return (webix.promise as any).resolve(result);
        });
    }

    static getData(view: webix.ui.datatable, params,
        columnsInfo: IColumnsInfo,
        companyId: number,
        startDate: Date,
        endDate: Date): Promise<any> {

        const p = webix.promise.defer();

        if (!columnsInfo || !companyId) {
            (p as any).resolve([]);
            return p;
        }

        const pager = view.getPager();

        const from = (!params) ? 0 : params.start;
        const to = (!params) ? pager.data.size : params.start + params.count;

        const queryParams = {
            baTypeId: columnsInfo.baTypeId, from: from, to: to,
            filteringsFields: [],
            orderingsFields: [(params && params.sort)
                ? {
                    orderingField: params.sort.id, sortingOrder: params.sort.dir === "asc"
                        ? HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Asc
                        : HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Desc
                }
                : {
                    orderingField: columnsInfo.baIdAlias, sortingOrder: HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Asc

                }]
        };

        if (companyId) {
            queryParams.filteringsFields.push({
                field: DocumentsListViewCode.getFieldAlias(6, columnsInfo),
                filterOperator: 0,
                value: companyId
            });
            const dosDataAliase = DocumentsListViewCode.getFieldAlias(17, columnsInfo);
            queryParams.filteringsFields.push({
                field: dosDataAliase,
                filterOperator: 5,
                value: startDate
            });
            queryParams.filteringsFields.push({
                field: dosDataAliase,
                filterOperator: 4,
                value: endDate
            });
        }

        if (params && params.filter) {
            for (let value in params.filter) {
                if (params.filter.hasOwnProperty(value)) {
                    if (!params.filter[value])
                        continue;

                    queryParams.filteringsFields.push({
                        field: value,
                        filterOperator: DocumentsListViewCode.filterOperatorByFieldType(columnsInfo.columnInfo[value].type),
                        value: columnsInfo.columnInfo[value].type === HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Date
                            ? DocumentsListViewCode.filterDateTimeConverter(params.filter[value] as Date)
                            : params.filter[value]
                    });
                }
            }
        }

        Api.getApiQueryBuilder().getData(queryParams as any).then((items) => {

            const result = JSON.parse(items as any);

            const data = queryParams.from === 0
                ? {
                    data: result.items,
                    pos: 0,
                    total_count: result.rowCount
                }
                : {
                    data: result.items,
                    pos: queryParams.from
                };

            (p as any).resolve(data);
        }).catch(() => {
            (p as any).reject();
        });

        return p;
    }


    static getFieldAlias(markerTypeId: number, columnsInfo: IColumnsInfo) {
        for (let idx in columnsInfo.columnInfo) {
            if (columnsInfo.columnInfo[idx].markerId === markerTypeId)
                return columnsInfo.columnInfo[idx].aliase;
        }
    } 

    private static filterOperatorByFieldType(fieldType: HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType) {
        switch (fieldType) {
        case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Boolean:
        case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Date:
        case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Number:
            return HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal;
        case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.String:
            return HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Like;
        default:
            return HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal;
        }
    }

    private static filterDateTimeConverter(date: Date) {
        const tzoffset = date.getTimezoneOffset() * 60000; //offset in milliseconds
        return (new Date((date as any) - tzoffset)).toISOString().slice(0, -1);
    }

}
﻿import Base = require("../../../Common/DialogBase");
import List = require("./InventoryTransactionListView");
import Btn = require("../../../Components/Buttons/ButtonsExporter");

export interface IInventoryTransactionsDlg extends Base.IDialog {

}

export function getInventoryTransactionsDlg(viewName: string,
    baId: number,
    header: string): IInventoryTransactionsDlg {
    return new InventoryTransactionsDlg(viewName, header, baId);
};

class InventoryTransactionsDlg extends Base.DialogBase {

    private _list: List.IInventoryTransactionListView;
    private _closeBtn = Btn.getCloseButton(this.viewName, () => { this.okClose(); });

    protected headerLabel(): string {
        return `Проводки по документу: ${this._header}`;
    }

    constructor(viewName: string,
        private readonly _header: string,
        baId: number) {
        super(viewName, true);
        this.hPadding = 80;
        this.vPadding = 80;
        this._list = List.getInventoryTransactionListView(`${this.viewName}-list`, baId);
        this.fullscreen = true;
    }

    protected contentConfig() {
        return {
            rows: [
                this._list.init(),
                {
                    view: "toolbar",
                    height: 40,
                    elements: [{},this._closeBtn.init()]
                }
            ]
        };
    }

    protected windowConfig() {
        return {
            autoheight: true,
            padding: 50,
        };
    }
}

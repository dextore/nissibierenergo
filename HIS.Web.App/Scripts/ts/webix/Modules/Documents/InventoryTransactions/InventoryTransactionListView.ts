﻿import Base = require("../../../Components/ComponentBase");
import Api = require("../../../Api/ApiExporter");

export interface IInventoryTransactionListView extends Base.IComponent {

}

export function getInventoryTransactionListView(viewName: string, baId: number): IInventoryTransactionListView {
    return new InventoryTransactionListView(viewName, baId);
}

class InventoryTransactionListView extends Base.ComponentBase implements IInventoryTransactionListView {

    get viewName() {
        return this._viewName;
    }

    get dataTableId() {
        return `${this._viewName}-datatable-id`;
    }

    get dataTableControl() {
        return $$(this.dataTableId) as webix.ui.datatable;
    }

    constructor(private readonly _viewName: string, private readonly _baId: number) {
        super();
    }

    init() {
        return {
            view: "datatable",
            id: this.dataTableId,
            select: "row",
            resizeColumn: true,
            columns: [
                {
                    id: "recId",
                    header: "№",
                    width: 50
                },
                {
                    id: "accountName",
                    header: "Счет",
                    minWidth: 150,
                    template: "<strong>#accountNum#</strong> #accountName#",
                    fillspace: true,
                },
                {
                    id: "companyName",
                    header: "Организация",
                    minWidth: 150,
                    fillspace: true,
                },
                {
                    id: "warehouseName",
                    header: "Склад",
                    minWidth: 150,
                    fillspace: true,
                },
                {
                    id: "inventoryName",
                    header: "Номенклатурная единица",
                    minWidth: 150,
                    fillspace: true,
                },
                {
                    id: "operationDate",
                    header: "Дата проводки",
                    format: webix.i18n.dateFormatStr,
                    width: 100,
                },
                {
                    id: "isIncome",
                    header: "Приход/Расход",
                    template: (obj, common) => {
                        return (obj.isIncome === true ? "Приход" : obj.isIncome === false ? "Расход" : "");
                    },
                    width: 120,
                },
                {
                    id: "recTypeName",
                    header: "Прямая/Сторно",
                    width: 150,
                },
                {
                    id: "quantity",
                    header: "Количество",
                    width: 100,
                },
                {
                    id: "priceVAT",
                    header: "Цена с НДС",
                    width: 100
                },
                {
                    id: "VAT",
                    header: "НДС",
                    width: 100,
                },
                {
                    id: "isVATIncluded",
                    header: "НДС включен",
                    width: 100,
                    template: (obj, common) => {
                        return (obj.isVATIncluded === true ? "Да" : obj.isVATIncluded === false ? "Нет" : "");
                    },
                },
                {
                    id: "storeCost",
                    header: "Сумма/склад без НДС",
                    width: 150,
                },
                {
                    id: "sellCost",
                    header: "Сумма/продажа без НДС",
                    width: 200,
                },
                {
                    id: "sellCostVAT",
                    header: "Сумма/продажа НДС",
                    width: 200,
                }
            ],
            url: {
                $proxy: true,
                load: (view, callback, params) => {
                    const self = this;
                    if (!this._baId)
                        return;

                    this.dataTableControl.showOverlay("Загрузка данных....");
                    Api.ApiInventoryTransactions.get(this._baId)
                        .then(items => {
                            setData(items);
                        }).catch(() => {
                            setData([]);
                        });

                    function setData(items: Warehouses.IInventoryTransactionModel[]) {
                        (webix.ajax as any).$callback(view, callback, items);
                        self.dataTableControl.hideOverlay();
                    }
                }
            }

        }
    }
}
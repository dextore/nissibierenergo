﻿import Base = require("../../Components/ComponentBase");
import Common = require("../../Common/CommonExporter");
import Code = require("./DocumentTreeViewCode");
import Toolbar = require("./DocumentsToolBarView"); 

export interface IDocumenTreeView extends Base.IComponent {
    selectedTreeItemChange: Common.Event<Documents.IDocumentTreeItemModel>;
    filter: Toolbar.IDocumentsFilterData;
}

export function getDocumentListView(module: Common.MainTabBase, viewName: string): IDocumenTreeView {
    return new DocumentTreeView(module, viewName);
}

class DocumentTreeView extends Base.ComponentBase implements IDocumenTreeView {

    private _selectedTreeItemChange = new Common.Event<Documents.IDocumentTreeItemModel>();

    private _filter: Toolbar.IDocumentsFilterData = {
        companyId: null,
        startDate: null,
        endDate: null,
        setFilter: false
    };

    get viewName() {
        return this._viewName;
    }

    get treeId() {
        return `${this.viewName}-tree-id`;
    }

    get treeControl() {
        return $$(this.treeId) as webix.ui.tree;
    }

    get filter() {
        return this._filter;
    }

    set filter(value: Toolbar.IDocumentsFilterData) {
        if (this._filter.setFilter !== value.setFilter) {
            this._filter = value;
            this.treeControl.load(this.treeControl.config.url);
            return;
        }

        this._filter = value;
        if (this._filter.setFilter) {
            this.treeControl.load(this.treeControl.config.url);
        } else {
            this._selectedTreeItemChange.trigger(this.getSelectedItem(), this);
        }
    }

    get selectedTreeItemChange(): Common.Event<Documents.IDocumentTreeItemModel> {
        return this._selectedTreeItemChange;
    }

    constructor(private readonly _module: Common.MainTabBase, private readonly _viewName: string) {
        super();
    }

    init() {
        return {
            view: "tree",
            //datatype: "plainjs",
            id: this.treeId,
            select: "true",
            template: (obj, common) => {
                return common.icon(obj, common) + common.folder(obj, common) + "<span>" + obj.title + "</span>";
            },
            url: {
                $proxy: true,
                load: (view, callback, params) => {
                    ($$(this.treeId) as webix.ui.tree).clearAll();
                    Code.loadTreeItems(
                        (this._filter.setFilter) ? this._filter.companyId : null,
                        (this._filter.setFilter) ? this._filter.startDate : null,
                        (this._filter.setFilter) ? this._filter.endDate : null).then(data => {
                        ($$(this.treeId) as webix.ui.tree).parse(data, "json");
                        this.treeControl.select("1", false);
                        this.treeControl.open("1", true);
                    });
                }
            },
            on: {
                onSelectChange: () => {
                    this._selectedTreeItemChange.trigger(this.getSelectedItem(), this);
                }
            }
        };
    }

    protected getSelectedItem(): Documents.IDocumentTreeItemModel {
        const selected = this.treeControl.getSelectedItem(true);
        return selected.length > 0 ? (selected[0] as Code.ITreeDataItem).item : null;
    }
}
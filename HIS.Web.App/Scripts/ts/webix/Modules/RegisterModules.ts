﻿export interface IModuleDescription {
    baTypeId: number;
    header: string; 
}

const registerModules: { [id: string]: IModuleDescription; } = {
    "legal-person-module": { baTypeId: 11, header: "Юридические лица" },
    "objects-module": { baTypeId: 22, header: "Объекты" },
    "banks-module": { baTypeId: 30, header: "Справочник банков" },
    "fiz-person-tree-module": { baTypeId: 12, header: "Физические лица" },
    "consignment-notes-module": {baTypeId: 80, header: "Приходные накладные"},
    "fAssets-module": { baTypeId: 81, header: "Инвентарные объекты" },
    "admin-panel-module": { baTypeId: 0, header: "Администрирование сущностей" },
    "adminPanelOnlyMarkersList-panel-module": { baTypeId: 0, header: "Администрирование маркеров" },
    "adminPanelOnlyStateList-panel-module": { baTypeId: 0, header: "Администрирование состояний" },
    "adminPanelOnlyOperationList-panel-module": { baTypeId: 0, header: "Администрирование операций" }
}; 

export class Register{
    static get modules() {
        return registerModules;
    }
}
﻿import Common = require("../../Common/CommonExporter");
import Api = require("../../Api/ApiExporter");


export interface ITestSelectEntityDialog extends Common.IDialog {
    selected: LegalPersons.ILegalPersonModel;

}

export function getTestSelectEntityDialog(viewName: string): ITestSelectEntityDialog {
    return new TestSelectEntityDialog(viewName);
}

class TestSelectEntityDialog extends Common.DialogBase implements ITestSelectEntityDialog {
    rendered() {}

    get dataTableId() {
        return `${this.viewName}-datatable-id`;
    }

    get selected() {
        const items = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true) as LegalPersons.ILegalPersonModel[];
        return items.length ? items[0] : null;
    }

    constructor(viewName: string) {
        super(viewName);
        this.fullscreen = true;
    }

    protected get selectBtnId() {
        return `${this.viewName}-select-button-id`;
    }

    protected headerLabel(): string { return "Выбор сущности (тест)"; }

    protected contentConfig() {
        return {
            padding: 10,
            rows: [
                {
                    view: "datatable",
                    id: this.dataTableId,
                    scroll: "y", 
                    select: "row",
                    columns: [
                        { id: "code", header: "Код", width: 250 },
                        { id: "name", header: "Наименование", width: 200 },
                        { id: "fullName", header: "Поное наименование", fillspace: true }
                    ],
                    url: {
                        $proxy: true,
                        load: (view, callback, params) => {
                            //Api.getApiLegalPersons().getAll({ searchValue: null })
                            //    .then((data) => {
                            //        (webix.ajax as any).$callback(view, callback, data);
                            //        //(<webix.ui.list>$$(this.dataTableId)).parse(data, "json");
                            //    });
                        }
                    },
                    on: {
                        onSelectChange: () => {
                            const items = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true) as LegalPersons.ILegalPersonModel[];
                            items.length ? $$(this.selectBtnId).enable() : $$(this.selectBtnId).disable();
                        }
                    },
                }, {
                    height: 30,
                    cols: [{},
                        {
                            view: "button",
                            width: 80,
                            id: this.selectBtnId,
                            label: "Выбрать",
                            disabled: true,
                            click: () => {
                                this.okClose();
                            }
                        }, {
                            view: "button",
                            width: 80,
                            id: `${this.viewName}-cancel-button-id`,
                            label: "Отказаться",
                            click: () => {
                                this.cancelClose();
                            }
                        }
                    ]
                }
            ]
        }
    }
}
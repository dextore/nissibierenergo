﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Common/DialogBase");
import Common = require("../../Common/CommonExporter");
import Entity = require("./TestSelectEntityDlg");
import TestEntity = require("./TestEntityForm");
import Interfaces = require("../../Abstractions/InterfacesExporter");


export interface ITestModal extends Base.IDialog {

}

export function getTestModal(viewName: string, mainTabView: Common.IMainTabbarView): ITestModal {
    return new TestModal(viewName, mainTabView);
}

class TestModal extends Base.DialogBase implements ITestModal, Interfaces.ITabFromSubscriber {
    rendered() {}

    formCallback(arg: Interfaces.IFormCallBackArgs): void {
        webix.confirm(`baId: ${arg.baId} IsNew: ${arg.isNew}`, "Тест!!!", () => { });
    }

    private get formId() {
        return `${this.viewName}-form-id`;
    }

    get mainTabView(): Common.IMainTabbarView {
        return this._mainTabView;
    }

    constructor(viewName: string, private _mainTabView: Common.IMainTabbarView) {
        super(viewName);
    }

    private get selectGridsuggestId() {
        return `${this.viewName}-select-gridsuggest-id`;
    }

    private _legalPerson: LegalPersons.ILegalPersonModel = null;

    protected headerLabel(): string { return "Диалоговая форма (тест)"; }

    protected contentConfig() {
        const self = this;
        return {
            view: "form",
            id: this.formId,
            elements: [
                {
                    cols: [{
                        view: "search",
                        label: "Поиск",
                        type: "search",
                        name: "search",
                        placeholder: "поиск..",
                        suggest: {
                            view: "gridsuggest",
                            id: this.selectGridsuggestId,
                            textValue: "fullName",
                            width: 600,
                            body: {
                                columns: [
                                    { id: "code", header: "Код", width: 150 },
                                    { id: "name", header: "Наименование", width: 200 },
                                    { id: "fullName", header: "Поное наименование", fillspace: true }
                                ],
                                dataFeed: (filtervalue: string) => {
                                    if (filtervalue.length < 2) return;

                                    //const sval = filtervalue.toLocaleLowerCase();

                                    //Api.getApiLegalPersons().getAll({ searchValue: filtervalue }).then((data) => {
                                    //    (($$(this.selectGridsuggestId) as webix.ui.gridsuggest).getList() as any).parse(data, "json");
                                    //});
                                }
                            },
                            on: {
                                onValueSuggest: (obj) => {
                                    this._legalPerson = obj;
                                }
                            }
                        },
                        on: {
                            onSearchIconClick: (e) => {
                                const dlg = Entity.getTestSelectEntityDialog("select-entityt-dlg");

                                let promise: Promise<any>;

                                switch (this.dialogMode) {
                                    case Base.DlgMode.Modal:
                                        promise = dlg.showModal(callback);
                                        break;
                                    case Base.DlgMode.Content:
                                        promise = dlg.showModalContent(this.tabItem, callback);
                                        break;
                                    case Base.DlgMode.Slider:
                                        promise = dlg.showOnSlider(this.tabItem, callback);
                                        break;
                                }

                                function callback() {
                                    if (!dlg.selected)
                                        return false;
                                    self._legalPerson = dlg.selected;
                                    (($$(self.formId) as any).elements["search"] as webix.ui.search).setValue(
                                        dlg.selected.fullName);
                                    return true;
                                }
                            },
                            onChange: (newv, oldv) => {
                                if (self._legalPerson && self._legalPerson.fullName !== newv)
                                    self._legalPerson = null;
                            }
                        }
                    },
                    {
                        view: "button", type: "iconButton", icon: "plus", width: 28, click: () => {
                            const tabId = TestEntity.TestEntityForm.getTabId(null);
                            const subscription = TestEntity.TestEntityForm.showForm(this,
                                () => {
                                    const form = new TestEntity.TestEntityForm(this._mainTabView);
                                    return form;
                                },
                                tabId);
                        }
                    },
                    {
                        view: "button", type: "iconButton", icon: "edit", width: 28, click: () => {
                            if (!self._legalPerson)
                                return;
                            const tabId = TestEntity.TestEntityForm.getTabId(self._legalPerson.legalPersonId);
                            const subscription = TestEntity.TestEntityForm.showForm(this,
                                () => {
                                    const form = new TestEntity.TestEntityForm(this._mainTabView, self._legalPerson.legalPersonId);
                                    form.load(self._legalPerson.legalPersonId, self._legalPerson.fullName);
                                    return form;
                                },
                                tabId);
                        }
                    }
                    ]
                }, {
                    view: "combo",
                    id: "combo-id",
                    options: {
                        view: "gridsuggest",
                        data: [
                            { id: 1, name: "One", year: 1988, color: "Red" },
                            { id: 2, name: "Two", year: 1996, color: "Blue" },
                            { id: 3, name: "Three", year: 1997, color: "Green" },
                            { id: 4, name: "Four", year: 2011, color: "Orange" },
                            { id: 5, name: "Five", year: 2000, color: "Grey" },
                            { id: 6, name: "Six", year: 1994, color: "Yellow" }
                        ]
                    }
                },
                {
                    view: "text",
                    id: "text-id",
                },
                {
                    cols: [
                        {},
                        {
                            view: "button",
                            id: "button-id",
                            label: "Закрыть",
                            width: 80,
                            click: () => {
                                this.close();
                            }
                        }
                    ]
                }
            ]
        }
    }
}
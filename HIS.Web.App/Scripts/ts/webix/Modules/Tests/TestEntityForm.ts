﻿import Common = require("../../Common/CommonExporter");
import Form = require("../../Components/Forms/FormExporter");
import Btn = require("../../Components/Buttons/ButtonsExporter");

export interface ITestEntityForm extends Form.ITabForm {

}


export class TestEntityForm extends Form.TabFormBase implements ITestEntityForm {

    protected static _viewName = "test-entity-form";
    protected static _header = "тест сущность ";

    private _okButton = Btn.getOkButton(`${this.viewName}`, this.okBtnClick.bind(this));
    private _cancelButton = Btn.getCancelButton(`${this.viewName}`, this.cancelBtnClick.bind(this));

    init() {
        return {
            id: `${this.viewName}-entity-form-id`,
            rows: [{},
                {
                    cols: [{}, this._okButton.init(), this._cancelButton.init()] 
            }]
        };
    }

    private okBtnClick(id, e) {
        this.triggerCallbacks(1, true);
        this.close();
    }

    private cancelBtnClick(id, e) {
        this.triggerCallbacks(1, false);
        this.close();
    }
}
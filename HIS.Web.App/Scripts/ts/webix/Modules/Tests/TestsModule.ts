﻿import Api = require("../../Api/ApiExporter");
import Common = require("../../Common/CommonExporter");
import Mngr = require("../ModuleManager");
import TestModal = require("./TestModalForm");
import SearchAddress = require("../../Components/Address/SearchAddress");
import AddEditAddress = require("../../Components/Address/AddEditAddress");
import History = require("../../Components/History/HistoryEntityDialog");


export class TestsModule extends Common.MainTabBase {

    protected static _viewName = "tests-module";
    protected static _header = "Тестовый модуль";

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
    }

    getContent() {
        const self = this;
        return {
            //id: this.viewId,
            rows: [{
                cols: [
                    {
                        rows: [
                            { template: "StatCodesLegalSubjects", borderless: true },
                            {

                                //cols: [{},
                                //{
                                //    view: "button",
                                //    value: "Search",
                                //    width: 80,
                                //    height: 45,
                                //    click: () => {
                                //        const form = BranchDlg.getBranchDlg(`${self.viewName}-test-modal-form`);
                                //        form.showModal(() => {
                                //            const tree = ($$(`${self.viewName }-test-modal-form-form-id-tree-id`) as webix.ui.tree);
                                //            let array = tree.getChecked();
                                //            var resultArray:any[] = [];
                                //            array.forEach(itemId => resultArray.push(tree.getItem(itemId)));

                                //            Api.ApiBranchs.save(51014 /* 10005 fizpersonId*/, resultArray).then(result => {
                                //                if (result)
                                //                    console.log(result);
                                //                form.close();
                                //            });
                                //        });
                                //    }
                                //    },
                                //    {}]

                            }
                        ]
                    },
                    {}
                ]
            },
            { view: "resizer" },
            {
                cols: [{
                    rows: [
                       
                        {
                            view: "form",
                            id: "testdatepickerform",
                            elements: [
                                {
                                    id: "datepickerExpansionId",
                                    view: "datepickerExtended",
                                    name: `testname_date`,
                                    width: 200,
                                    pattern: { mask: "##.##.####", allow: /[0-9]/g },
                                    value: null,
                                    validate: (value) => {

                                        if (!value) {
                                            return false;
                                        }
                                        return true;
                                    },
                                    on: {
                                        onChange: (newv, oldv) => {
                                            ($$("datepickerExpansionId") as webix.ui.datepicker).validate();
                                        },
                                        onBlur: () => {
                                            ($$("datepickerExpansionId") as webix.ui.datepicker).validate();
                                        }

                                    }
                                },
                                {
                                    cols: [
                                        {
                                            view: "button",
                                            value: "Значение",
                                            width: 150,
                                            height: 45,
                                            click: () => {

                                                const form = $$("testdatepickerform") as webix.ui.form;
                                                form.setValues({
                                                    "testname_date": new Date('1991-04-04')
                                                } as any);
                                                /*
                                                const value = ($$("datepickerExpansionId") as any).getValue();
                                                webix.message("" + value, "message");
                                                */
                                            }
                                        },
                                        {
                                            view: "button",
                                            value: "Очистить",
                                            width: 150,
                                            height: 45,
                                            click: () => {

                                                const form = $$("testdatepickerform") as webix.ui.form;
                                                form.setValues({
                                                    "testname_date": null
                                                } as any);
                                                /*
                                                const value = ($$("datepickerExpansionId") as any).getValue();
                                                webix.message("" + value, "message");
                                                */
                                            }
                                        }

                                    ]
                                }
                            ]
                        }, {}
                    ]
                }, {
                    rows: [
                        this.getTestDataTable()
                    ]
                }
                ]
            },

            { view: "resizer" },
            {
                cols: [{
                    rows: [{ template: "Модальное окно", borderless: true },
                    {
                        cols: [{}, {
                            view: "button",
                            value: "Открыть",
                            width: 80,
                            height: 45,
                            click: () => {
                                const form = TestModal.getTestModal(`${self.viewName}-test-modal-form`, this.mainTabView);
                                form.showModal();
                            }
                        }, {}
                        ]
                    }, {}]
                }
                ]
            },
            { view: "resizer" },
            {
                cols: [{
                    rows: [{ template: "Контентное окно", borderless: true },
                    {
                        cols: [{}, {
                            view: "button",
                            value: "Открыть",
                            width: 80,
                            height: 45,
                            click: () => {
                                const form = TestModal.getTestModal(`${self.viewName}-test-modal-form`, this.mainTabView);
                                form.showModalContent(this);
                            }
                        }, {}
                        ]
                    }, {}]
                }
                ]
            },
            { view: "resizer" },
            {
                cols: [{
                    rows: [{ template: "Слайдер окно", borderless: true },
                    {
                        cols: [{}, {
                            view: "button",
                            value: "Открыть",
                            width: 80,
                            height: 45,
                            click: () => {
                                const form = TestModal.getTestModal(`${self.viewName}-test-modal-form`, this.mainTabView);
                                form.showOnSlider(this);
                            }
                        }, {}
                        ]
                    }, {}]
                }
                ]
            },
            { view: "resizer" },
            {
                cols: [{
                    rows: [{ template: "Адресный справочник", borderless: true },
                    {
                        cols: [{}, {
                            view: "button",
                            value: "Обычный поиск",
                            width: 200,
                            height: 45,
                            click: () => {
                                /*
                                const searchDialog = SimpleSearchAddress.getSimpleSearchAddress(`${self.viewName}-simple-search-address-form`, this.mainTabView);
                                let promise: Promise<any> = searchDialog.showModalContent(this, () => {
                                    console.log(searchDialog.selected);
                                    webix.message(JSON.stringify(searchDialog.selected));

                                });
                                */
                                const searchDialog = SearchAddress.getSearchAddress(`${self.viewName}-search-address`, null);
                                let promise: Promise<any> = searchDialog.showModal(() => {
                                    console.log(searchDialog.selected);
                                    webix.message(JSON.stringify(searchDialog.selected));
                                    searchDialog.close();
                                });
                            }
                        },
                        {
                            view: "button",
                            value: "Расширеный поиск",
                            width: 200,
                            height: 45,
                            click: () => {
                                const searchDialog = SearchAddress.getSearchAddress(`${self.viewName}-search-address`, true);
                                let promise: Promise<any> = searchDialog.showModal(() => {
                                    console.log(searchDialog.selected);
                                    webix.message(JSON.stringify(searchDialog.selected));
                                    searchDialog.close();

                                });
                            }
                        },
                        {}
                        ]
                    },
                    {
                        cols: [{}, {
                            view: "button",
                            value: "Добавить адрес",
                            width: 200,
                            height: 45,
                            click: () => {
                                const addDialog = AddEditAddress.getAddressDialog(`${self.viewName}-add-edit-address`);
                                let promise: Promise<any> = addDialog.showModal(() => {
                                    console.log(addDialog.selected);
                                    webix.message(JSON.stringify(addDialog.selected));
                                    addDialog.close();
                                });
                            }
                        },
                        {}
                        ]
                    },
                    {}]
                },
                ]
            }
            ]
        }
    }


    private getTestDataTable(): object {

        const tableId = `${this.viewName}-test-table`;
        console.log(tableId);
        return {
            view: "datatable",
            id: tableId,
            select: "row",
            scroll: "y",
            resizeColumn: true,
            editable: true,
            columns: [
                {
                    id: "testDate",
                    header: "Дата выпуска",
                    fillspace: true,
                    format: webix.i18n.dateFormatStr,
                    editor: "dateExtended",
                    sort: "date"
                },
                {
                    id: "secondDate",
                    header: "Дата вторая",
                    fillspace: true,
                    format: webix.i18n.dateFormatStr,
                    editor: "dateExtended",

                }
            ],
            url: {
                $proxy: true,
                load: function (view, callback, params) {

                    const setData = [{ testDate: new Date() }, { testDate: new Date("2010-11-10") }] as object[];

                    (webix.ajax as any).$callback(view, callback, setData);

                }
            }
        };
    }

}

Mngr.ModuleManager.registerModule(TestsModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new TestsModule(mainView);
});


﻿import Base = require("../../Common/DialogBase");
import BaseDialog = require("./BankDlgBase");

export interface IBankDlg extends Base.IDialog {

}

export function getBankDlg(viewName: string,
    bankId: number = null,
    parrentId: number = null,
    dataTable: webix.ui.datatable = null,
    isEdit = false,
    datatableItemId: string | number,
    tabModule: any): IBankDlg {

    return new DialogBank(<BaseDialog.DialogSettings>{
        viewName: viewName,
        bankId: bankId,
        datatableItemId: datatableItemId,
        dataTable: dataTable,
        parrentId: parrentId,
        itIsEditBankDlg: isEdit
    }, tabModule);
}

class DialogBank extends BaseDialog.BankDlgBase implements IBankDlg {
    _baTypeId: number = 30;

    constructor(settings: BaseDialog.DialogSettings, public tabModule: any) {
        super(settings.viewName,
            settings.bankId,
            settings.datatableItemId,
            settings.dataTable,
            settings.parrentId,
            settings.itIsEditBankDlg);
        this.tabModule = tabModule;
    }

    headerLabel(): string { return "Банк"; }
}

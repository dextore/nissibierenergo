﻿import Base = require("../../Common/DialogBase");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import Api = require("../../Api/ApiExporter");
import Component = require("../../Components/ComponentBase");
import Validator = require("../../Common/Validate");
import MF = require("../../Components/Markers/MarkerControlFactory");
import MB = require("../../Components/Markers/MarkerControlBase");
import EntityRefMarkerControl = require("../../Components/Markers/EntityRefMarkerControl");

export abstract class BankDlgBase extends Base.DialogBase {
    protected _parrentId;
    protected _inn;
    private _bankId;
    private _datatableItemId: string | number;
    private _itIsEditBankDlg: boolean;
    private _dataTable: webix.ui.datatable;
    protected _baTypeId: number;
    private _markersControls: { [id: number]: MB.IMarkerControl } = {};

    protected _okBtn = Btn.getOkButton(this.viewName,
        () => {
            let dialogForm = ($$(this.formId) as webix.ui.form);
            if (!dialogForm.validate())
                return;

            if (!this.isChangeForm()) {
                this.okClose();
                return;
            }

            var obj = this.getDataFromDialog();
       
            let self = this;
            Api.getApiBanks().save(obj.itemEntity).then(item => {
                if (item.inn === "-1") {
                    webix.message({
                        type: "error",
                        text: "Банк с таким инн уже есть в базе",
                        expire: -1
                    });
                    return;
                }
                
                if (item) {
                    if (!this._dataTable.isVisible())
                        self._dataTable.show();
                    if (self._itIsEditBankDlg) {
                        self._dataTable.updateItem(obj.datatableId, item);
                    } else {
                        let itemTableId = self._dataTable.add(item);
                        self._dataTable.select(itemTableId, false);
                    }
                    this.okClose();
                }
            });
            
        });
    protected _cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });
    protected _fieldsInfo: { [id: string]: Markers.IMarkerInfoModel } = {};
    protected _model: { [id: string]: any } = {};
    
    constructor(viewName: string,
        bankId,
        datatableItemId: string | number,
        dataTable: webix.ui.datatable,
        parrentId = null,
        itIsEditBankDlg = false,
        public tabModule: any = null,
        inn: string = "") {
        super(viewName);
        this._parrentId = parrentId;
        this._bankId = bankId;
        this._datatableItemId = datatableItemId;
        this._dataTable = dataTable;
        this._itIsEditBankDlg = itIsEditBankDlg;
        this.tabModule = tabModule;
        this._inn = inn;
    }
    bindModel(): void {
        const self = this;
        const promises: Promise<any>[] = [];

        promises.push(Api.getApiMarkers().getEntityMarkersInfo(this._baTypeId));

        if (this._bankId) {
            promises.push(Api.getApiBanks().getBank(this._bankId));
        }

        (webix.promise.all(promises as any) as any).then((result: any[]) => {

           
            loadFieldsInfo(result[0]);

            if (result.length > 1) {
                modelBindToFrom(result.length > 1 ? result[1] : null, result[1].markers);
            }
            else {
                self._model["INN"] = self._inn;
                ($$(self.formId) as webix.ui.form).setValues(self._model);
               // modelBindToFrom(result.length > 1 ? result[1] : null, result[1].markers);
            }
            ($$(self.formId) as webix.ui.form).clearValidation();

        });

        function loadFieldsInfo(result: Markers.IMarkerInfoModel[]) {
            result.forEach(item => {
                    if (!(item.id === 35 || item.id === 36 || item.id === 30)) { // ignor changeDate and actionDate
                        const marker = self.createMarkerControl(item);
                        
                        if (marker) {
                            ($$(self.formId) as webix.ui.form).addView(marker.init());
                            //self._markersControls[item.id] = marker;
                            self._fieldsInfo[item.id] = item;

                            if ("baId" in marker) {
                                (marker as any).baId = self._bankId;
                            }
                            if (item.markerType === 11) {
                                (marker as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
                                (marker as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this._module);
                            }
                            self._markersControls[item.id] = marker as any;
                        }
                    }

                },
                self);
            ($$(self.formId) as webix.ui.form).addView({
                    cols: [
                        {},
                        self._okBtn.init(),
                        self._cancelBtn.init()
                    ]
                }
            );
            self.checkSize();
        }

        function modelBindToFrom(model: Banks.IFrontBanksEntity,
            markers: Banks.IFrontBankMarker[] /*Markers.IMarkerInfoModel[]*/) {

            let info = self._markersControls[5].markerInfo;
            self._markersControls[5].setInitValue(self.getMarkerValue(info, model.addrId as any, model.displayAddr), self._model);

            info = self._markersControls[31].markerInfo;
            self._markersControls[31].setInitValue(self.getMarkerValue(info, model.bik as any, null), self._model);

            info = self._markersControls[32].markerInfo;
            self._markersControls[32].setInitValue(self.getMarkerValue(info, model.rkc as any, null), self._model);

            const setInn = self._inn === "" ? model.inn : self._inn;
            info = self._markersControls[100].markerInfo;
            self._markersControls[100].setInitValue(self.getMarkerValue(info, setInn as any, null), self._model);

            info = self._markersControls[102].markerInfo;
            self._markersControls[102].setInitValue(self.getMarkerValue(info, model.okpo as any, null), self._model);

            //self._model["BIK"] = model.bik;
            //self._model["INN"] = self._inn === "" ? model.inn : self._inn; //model.inn;
            //self._model["RKC"] = model.rkc;
            //self._model["OKPO"] = model.okpo;
            //self._model["Address"] = model.displayAddr;
            //self._model["AddressidHidennValue"] = model.addrId;
            if (model.parentId)
                self._model["Bank"] = model.bankBranchDisplayValue;

            let nameDate:Date = null;
            let acceptDate: Date = null;
            let corrAccountDate: Date = null;

            let setValue: Markers.IMarkerValueModel = null;
            markers.forEach(marker => {

                switch (marker.markerId) {
                    case 1:
                        {
                            nameDate = marker.startDate;
                            //self._model["Name"] = model.name;
                            //self._model[`Name_date`] = marker.startDate;
                            break;
                        }
                    case 33:
                        {
                            corrAccountDate = marker.startDate;
                            //self._model["CorrAccount"] = model.corespondetsAccount;
                            //self._model["CorrAccount_date"] = marker.startDate;
                            break;
                        }
                    case 34:
                        {
                            acceptDate = marker.startDate;
                            //self._model["Accept"] = model.acceptancePeriod;
                            //self._model["Accept_date"] = marker.startDate;
                            break;
                        }
                }
            });
            info = self._markersControls[1].markerInfo;
            setValue = self.getMarkerValue(info, model.name as any, null);
            setValue.startDate = nameDate;
            self._markersControls[1].setInitValue(setValue, self._model);

            info = self._markersControls[33].markerInfo;
            setValue = self.getMarkerValue(info, model.corespondetsAccount as any, null);
            setValue.startDate = corrAccountDate;
            self._markersControls[33].setInitValue(setValue, self._model);

            info = self._markersControls[34].markerInfo;
            setValue = self.getMarkerValue(info, model.acceptancePeriod as any, null);
            setValue.startDate = acceptDate;
            self._markersControls[34].setInitValue(setValue, self._model);

            ($$(self.formId) as webix.ui.form).setValues(self._model);
            ($$(self.formId) as webix.ui.form).clearValidation();

        }
        ($$(self.formId) as webix.ui.form).clearValidation();
    }
    private getMarkerValue(info: Markers.IMarkerInfoModel, value: string, displayValue: string): Markers.IMarkerValueModel {
        return {
            displayValue: displayValue,
            itemId: null,
            endDate: null,
            markerId: info.id,
            markerType: info.markerType,
            MVCAliase: info.name,
            note: null,
            isVisible: true,
            isBlocked: false,
            startDate: null,
            value: value,
            isDeleted: false
        }
    }

    get formId() {
        return `${this.viewName}-form-id`;
    }

    getDataFromDialog(): any {
        const form = $$(this.formId) as webix.ui.form;
        const data = form.elements;
        this._fieldsInfo
        const addressValue = this._markersControls[5].getValue(this._fieldsInfo);

        if (data) {
            var result = <Banks.IFrontBanksEntity>{
                baId: this._bankId,
                bATypeId: this._baTypeId,
                parentId: this._parrentId,
                bik: data.BIK.data.value,
                inn: data.INN.data.value,
                rkc: data.RKC.data.value,
                okpo: data.OKPO.data.value,

                addrId: (!addressValue || !addressValue.value) ? null : addressValue.value as any,
                displayAddr: (!addressValue || !addressValue.displayValue) ? "" : addressValue.displayValue,

                name: data.Name.data.value,
                corespondetsAccount: data.CorrAccount.data.value,
                acceptancePeriod: data.Accept.data.value,

                markers: [
                    { // Наименование
                        bAId: this._bankId,
                        markerId: 1,
                        endDate: null,
                        value: data.Name.data.value,
                        note: "",
                        startDate: data.Name_date.data.value
                    },
                    { // Корреспондентский счет
                        bAId: this._bankId,
                        markerId: 33,
                        endDate: null,
                        value: data.CorrAccount.data.value,
                        note: "",
                        startDate: data.CorrAccount_date.data.value
                    },
                    { // Cрок акцепта (кол-во дней)
                        bAId: this._bankId,
                        markerId: 34,
                        endDate: null,
                        value: data.Accept.data.value,
                        note: "",
                        startDate: data.Accept_date.data.value
                    }
                ]
            };
        };
        
        return {
            itemEntity: result,
            datatableId: this._datatableItemId
        };
    }
    contentConfig() {

        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
            ]
        }
    }
    protected windowConfig() {
        return {
            autoheight: true,
            width: 700
        };
    }
    private checkSize() {
        const rect = $$(this.windowId).getNode().getBoundingClientRect();
        const diff = $$(this.formId).getNode().scrollHeight - $$(this.formId).getNode().clientHeight + 10;
        $$(this.windowId).$setSize(rect.width, rect.height + diff);
        ($$(this.windowId) as webix.ui.window).define("height", rect.height + diff);
    }
    private createMarkerControl(model: Markers.IMarkerInfoModel): Component.IComponent {

        const label_width = 240;

        if (model.id === 1) { // Наименование
            return MF.MarkerControlfactory.getControl(this.viewName, model, (content) => {
                return {
                    labelWidth: label_width,
                    validate: (value) => {
                        if (!(this._markersControls[1] as any).customValidate())
                            return false;
                        //return value.toString().length > 0;
                        return true;
                    }


                }
            });
        }

        if (model.id === 33) { //Корреспондентский счет
            return MF.MarkerControlfactory.getControl(this.viewName, model, (content) => {
                return {
                    labelWidth: label_width,
                    pattern: { mask: "####################", allow: /[0-9]/g },
                    validate: (value) => {
                        if (!(this._markersControls[33] as any).customValidate())
                            return false;
                        return value.toString().length === 20;
                       

                    }

                }
            });   
        }

        if (model.id === 34) { //Акцепт
            return MF.MarkerControlfactory.getControl(this.viewName, model, (content) => {
                return {
                    labelWidth: label_width,
                    pattern: { mask: "###", allow: /[0-9]/g },
                    //validate: (value) => value <= 999 && value > 0,
                    validate: (value) => {
                        if (!(this._markersControls[34] as any).customValidate())
                            return false;
                        if (value.toString().length !== 0) {
                            return value <= 999 && value > 0;
                        }
                        return true;

                    }
                }
            });   
        }

        if (model.id === 100) { // ИНН
            return MF.MarkerControlfactory.getControl(this.viewName, model, (content) => {
                return {
                    labelWidth: label_width,
                    pattern: { mask: "############", allow: /[0-9]/g },
                    disabled: this._parrentId ? true : false,
                    //validate: Validator.isInnValid,
                    validate: (value) => {
                        if (!(this._markersControls[100] as any).customValidate())
                            return false;
                        if (value.toString().length !== 0) {
                            return Validator.isInnValid(value);
                        }
                        return true;
                    }

                }
            });   
        }

        if (model.id === 31) { // БИК
            return MF.MarkerControlfactory.getControl(this.viewName, model, (content) => {
                return {
                    labelWidth: label_width,
                    //validate: (value) => value.toString().length === 9 && value.toString().length > 0,
                    pattern: { mask: "#########", allow: /[0-9]/g },
                    validate: (value) => {
                        if (!(this._markersControls[31] as any).customValidate())
                            return false;
                        return value.toString().length === 9 && value.toString().length > 0;

                    }
                }
            });   
        }

        if (model.id === 102) { // ОКПО
            return MF.MarkerControlfactory.getControl(this.viewName, model, (content) => {
                return {
                    labelWidth: label_width,
                    //validate: (value) => value.toString().length === 8 || value.toString().length === 10,
                    pattern: { mask: "##########", allow: /[0-9]/g },
                    validate: (value) => {
                        if (!(this._markersControls[102] as any).customValidate())
                            return false;
                        if (value.toString().length !== 0) {
                            return value.toString().length === 8 || value.toString().length === 10;
                        }
                        return true;
                    }
                }
            });   
        }

        if (model.id === 5) { // Адресс
            return MF.MarkerControlfactory.getControl(this.viewName, model, (content) => {
                return {
                    labelWidth: label_width
                }
            });   
        }

        if (model.id === 32) { // РКЦ
            return MF.MarkerControlfactory.getControl(this.viewName, model, (content) => {
                return {
                    labelWidth: label_width
                }
            });   
        }
    }
    private isChangeForm(): boolean {
        let result = false as boolean;

        for (let key in this._markersControls) {
            if (this._markersControls.hasOwnProperty(key)) {
                if (this._markersControls[key].isChanged === true) {
                    result = true;
                }
            }
        }
        return result;
    }
}

export class DialogSettings {
    viewName: string;
    bankId: number;
    datatableItemId: string | number;
    dataTable: webix.ui.datatable;
    parrentId: number;
    itIsEditBankDlg: boolean;
}

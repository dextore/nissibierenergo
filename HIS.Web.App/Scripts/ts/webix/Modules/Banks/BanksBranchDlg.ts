﻿import Base = require("../../Common/DialogBase");
import BaseDialog = require("./BankDlgBase");

export interface IBankDlg extends Base.IDialog {

}

export var inn: string;

export function getBankDlg(viewName: string,
    bankId: number = null,
    parrentId: number = null,
    dataTable: webix.ui.datatable = null,
    isEdit = false,
    datatableItemId: string | number,
    tabModule: any): IBankDlg {
    return new DialogBanksBranch(<BaseDialog.DialogSettings>{
        viewName: viewName,
        bankId: bankId,
        datatableItemId: datatableItemId,
        dataTable: dataTable,
        parrentId: parrentId,
        itIsEditBankDlg: isEdit
    }, tabModule);
}

class DialogBanksBranch extends BaseDialog.BankDlgBase {
    _baTypeId: number = 31;

    constructor(settings: BaseDialog.DialogSettings, public tabModule:any) {
        super(settings.viewName,
            settings.bankId,
            settings.datatableItemId,
            settings.dataTable,
            settings.parrentId,
            settings.itIsEditBankDlg,
            tabModule,
            inn);
        this.tabModule = tabModule;
    }

    headerLabel(): string {
        return "Отделение банка";
    }
}
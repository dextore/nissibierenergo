﻿import Base = require("../../Components/ComponentBase");
import Tab = require("../../MainView/Tabs/MainTabBase");
import ToolButtons = require("../../Components/Buttons/ToolBarButtons");
import Common = require("../../Common/CommonExporter");
import StateEntity = require("../../Components/Controls/EntityOperationStateControl");
import Dialog = require("../DocMoveAssets/DocMoveAssetsDialog");
import DataTable = require("../../Components/QueryBuilderListBase/QueryBuilderListBase");
import IQueryBuilderListSetParameters = DataTable.IQueryBuilderListSetParameters;

export interface IDepartamentsList extends Base.IComponent {
    changeCompany(company: Inventory.ILSCompanyAreas);
    reloadByModel(model: IDepartamentsModel);
    ready();
}

export interface IDepartamentsModel {
    baId: number; // baId банка - parentId
    stateId: number;  // состояние сущности
    inn:string;
}


export function getDepartamentsList(_viewName: string, module: Common.MainTabBase) {
    return new DepartamentsList(_viewName, module);
}

export class DepartamentsList extends Base.ComponentBase implements IDepartamentsList {

    private readonly _entityAlias = "BankDepartment";
    private _selectedCompany: Inventory.ILSCompanyAreas;
    private _entityInfo = Common.entityTypes().getByAliase(this._entityAlias).entityInfo;

    private _setModel: IDepartamentsModel;

    private _operationState: StateEntity.IEntityOperationStateControl;
    private _tableBuilderControl: DataTable.IQueryBuilderList;

    // query builder list parameters 
    private _showBaId: boolean = false;
    private _aliasesWithoutFilter = ["CompanyArea"];
    private _aliasesOrder = [
        "BAId",
        "Bank",
        "Name",
        "BIK",
        "CorrAccount",
        "OKPO", 
        "INN",
        "RKC",
        "Address",
        "Accept",
        "State",
        "Creator",
        "CompanyArea"];
    //private _aliasesSkip = ["Bank"];
    private get tableId() { return `${this._viewName}-datatable-id`; }

    get tableControl() {
        return $$(this.tableId) as webix.ui.datatable;
    }
    protected toolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}-tooldar`, "Добавить", this.createEntity.bind(this)),
        edit: ToolButtons.getEditBtn(`${this._viewName}-tooldar`, "Изменить", this.editEntity.bind(this))
    }
    constructor(private readonly _viewName: string, private readonly _module: Common.MainTabBase) {
        super();
        this.initSelectedCompany();
        this.initOperationState();
        this.initDataTableControl();
    }

    public reloadByModel(model: IDepartamentsModel): void {
        this._setModel = model;
        this.resetButtons();
        this.reloadDataTable();
    }

    private reloadDataTable() {
        
        this.setOperationState(null);
        const companyField = this._tableBuilderControl.getCompanyAliase();
        const baTypeAliase = this._tableBuilderControl.getBATypeIdAliase();

        const filteringArray = [{
            field: baTypeAliase,
            filterOperator: 0,
            value: this._entityInfo.baTypeId
        }] as Array<object>;

        if (companyField != "") {
            filteringArray.push({
                field: companyField,
                filterOperator: 0,
                value: this._selectedCompany.id
            });
        }
        const parentAliase = this._tableBuilderControl.getFieldAlias(30);
        if (!this._setModel) {
            const baIdAliase = this._tableBuilderControl.getBAIdAliase();
            filteringArray.push({
                field: baIdAliase,
                filterOperator: 0,
                value: "0"
            });
        } else {
            filteringArray.push({
                field: parentAliase,
                filterOperator: 0,
                value: this._setModel.baId
            });
        }
        this._tableBuilderControl.setFilteringFields(filteringArray);
        this._tableBuilderControl.reload();
    }

    private initDataTableControl() {
      
        const queryBuilderParam = {
            showBaId: this._showBaId,
            tooltip: true,
            entityInfo: this._entityInfo,
            aliasesOrder: this._aliasesOrder,
            aliasesWithoutFilter: this._aliasesWithoutFilter
            //,aliasesSkip: ["Bank"]
   
        } as IQueryBuilderListSetParameters;
        this._tableBuilderControl = DataTable.getQueryBuilderList(this.tableId, queryBuilderParam);

        const baIdAliase = this._tableBuilderControl.getBAIdAliase();
        const companyField = this._tableBuilderControl.getCompanyAliase();
        const baTypeAliase = this._tableBuilderControl.getBATypeIdAliase();

        const filteringArray = [{
            field: baTypeAliase,
            filterOperator: 0,
            value: this._entityInfo.baTypeId
        }] as Array<object>;

        if (companyField != "") {
            filteringArray.push({
                field: companyField,
                filterOperator: 0,
                value: this._selectedCompany.id
            });
        }
        /// зануляем , для вывода пустой таблицы 
        filteringArray.push({
            field: baIdAliase,
            filterOperator: 0,
            value: "0"
        });

        this._tableBuilderControl.setFilteringFields(filteringArray);

    }
    private initSelectedCompany() {
        this._selectedCompany = this._module.mainTabView.getSelectedCompany();
    }
    private initOperationState(): void {
        this._operationState = StateEntity.getEntityOperationStateControl(`${this._viewName}-operation-state`,
            "Отделения банка",
            () => {
                return true;
            });
        this.subscribe(this._operationState.statusChangedEvent.subscribe((data) => {
            const baId = this._operationState.state.baId;
            const selectId = this.tableControl.getSelectedId(false, true);
            this.updateTableRecord(baId, selectId);
            this.resetButtons();
        }, this));
        this._operationState.addButtons(this.toolBar.add, this.toolBar.edit);
    }
    private setOperationState(baId: number) {
        console.log("operation state ");
        if (baId == null) {
            this._operationState.baId = null;
            return;
        }
        this._operationState.baId = baId;

    }
    private updateTableRecord(baId: number, recId?: number) {

        this._tableBuilderControl.getRowDataByBaId(baId).then(data => {
            const row = data.data[0];
            if (!recId) {
                this.tableControl.add(row, 0);
                this._tableBuilderControl.selectFirstRow();
            } else {
                this.tableControl.updateItem(recId, row);
            }
        });
    }

    private createEntity() {

        console.log("clicked add entity button");

    }
    private editEntity() {

        console.log("clicked add entity button");

        /*
        const selected = this.tableControl.getSelectedItem();
        const selectId = this.tableControl.getSelectedId(false, true);
        if (!selected) {
            webix.message("Необходимо выбрать элемент для редактирования", "error");
            return;
        }
        const baId = selected[this._tableBuilderControl.getBAIdAliase()] as number;

        Api.getApiDocMoveAssets().getDocMoveAssetsData(baId).then(data => {
            const dialog = Dialog.getDocMoveAssetsDialog(`${this._viewName}-entity-dialog`,
                this._module,
                this._entityInfo.markersInfo,
                data);

            dialog.showModalContent(this._module, () => {
                this.updateTableRecord(dialog.resultBaId, selectId);
                dialog.close();
            });
        });
        */
    }
    private resetButtons() {

        this.toolBar.add.button.enable();
        this.toolBar.edit.button.disable();

        const selected = this.tableControl.getSelectedItem() as any;
        if (!selected || !this._operationState.state) {
            return;
        }
        if (this._operationState.state.stateId === 1) {
            this.toolBar.edit.button.enable();
        }

    }
    changeCompany(company: Inventory.ILSCompanyAreas) {
        this._selectedCompany = company;
        this.reloadDataTable();
    }
    ready() {
        this.resetButtons();
        this._tableBuilderControl.ready();
        this.tableControl.attachEvent("onSelectChange", this.onSelectChangeTable.bind(this));
    }
    private onSelectChangeTable() {
        const selected = this.tableControl.getSelectedItem() as any;
        if (!selected) {
            this.setOperationState(null);
            return;
        }
        this.setOperationState(selected[this._tableBuilderControl.getBAIdAliase()]);
    }
    init() {
        return {
            rows: [
                this._operationState.init(),
                this._tableBuilderControl.init()
            ]
        };

       

    }
} 

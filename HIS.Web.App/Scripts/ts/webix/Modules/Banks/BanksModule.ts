﻿import Common = require("../../Common/CommonExporter");
import Mngr = require("../ModuleManager");
import Reg = require("../RegisterModules");
import Form = require("../../Components/Forms/Banks/BankList");

export class BanksModule extends Common.MainTabBase {

    protected static _viewName = Form.modulName;
    protected static _header = Reg.Register.modules[Form.modulName].header;

    private _form: Form.IBankList;

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
        this._form = Form.getBankList(this);
    }

    getContent() {
        return this._form.init();
    }

    ready(): void {
        this._form.ready();
    }
}

Mngr.ModuleManager.registerModule(BanksModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new BanksModule(mainView);
});

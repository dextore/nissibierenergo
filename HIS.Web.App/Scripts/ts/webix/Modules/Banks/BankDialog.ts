﻿import Base = require("../../Common/DialogBase");
import Common = require("../../Common/CommonExporter");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import Api = require("../../Api/ApiExporter");
import MB = require("../../Components/Markers/MarkerControlBase");
import Component = require("../../Components/ComponentBase");
import MarkerFactory = require("../../Components/Markers/MarkerControlFactory");
import EntityRefMarkerControl = require("../../Components/Markers/EntityRefMarkerControl");

export function getBanksDialog(viewName: string,
    module: Common.MainTabBase,
    markers: Markers.IMarkerInfoModel[],
    dataModel?: any): IBankDialog {
    return new BanksDialog(viewName, module, markers, dataModel);
};
export interface IBankDialog extends Base.IDialog {
    resultBaId: number;

}

class BanksDialog extends Base.DialogBase {

    get formId() { return `${this.viewName}-form-id`; }

    constructor(viewName: string,
        private readonly _module: Common.MainTabBase,
        private readonly _entityMarkers: Markers.IMarkerInfoModel[],
        private readonly _dataModel?: DocMoveAssets.IDocMoveAssetsData) {
        super(viewName);
        if (!this._dataModel || this._dataModel.baId == null) {
            this.isCreate = true;
        }
    }
    private isCreate = false;
    private baId: number;
    private markerOrder = [1, 17, 6, 137, 409, 410, 411, 412, 413, 414, 136, 7];
    private ignoreMarkers = [45];

    protected headerLabel(): string {

        if (this.isCreate) {
            return "Создание сущности банк";
        }
        return "Редактирование сущности банк";
    }
    get mainTabView(): Common.IMainTabbarView {
        return this._module.mainTabView;
    }
    get formControl() {
        return $$(this.formId) as webix.ui.form;
    }
    get resultBaId() {
        return this.baId;
    }
    protected fieldsInfo: { [id: string]: Markers.IMarkerInfoModel } = {};
    private markersControls: { [id: number]: MB.IMarkerControl } = {};
    protected get labelWidth() { return 280; }


    protected okBtn = Btn.getOkButton(this.viewName,
        () => {
            this.okBtn.button.disable();
            if (this.formControl.validate()) {

                if (!this.isChangeForm() && !this.isCreate) {
                    this.baId = this._dataModel.baId;
                    this.okClose();
                    return;
                }
                const model = this.getFormData();
                console.log("model to save");
                console.log(model);

            }
            this.okBtn.button.enable();
        });
    protected cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });

    protected contentConfig() {
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
            ]
        };
    }
    protected windowConfig() {
        return {
            autoheight: true,
            width: 720

        };
    }
    private checkSize() {
        const rect = $$(this.windowId).getNode().getBoundingClientRect();
        const diff = $$(this.formId).getNode().scrollHeight - $$(this.formId).getNode().clientHeight + 10;
        $$(this.windowId).$setSize(rect.width, rect.height + diff);
        ($$(this.windowId) as webix.ui.window).define("height", rect.height + diff);
    }

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._module.disableCompany();
    }

    destroy(): void {
        super.destroy();
        this._module.enableCompany();
    }

    bindModel(): void {

        this._entityMarkers.sort((first: Markers.IMarkerInfoModel, second: Markers.IMarkerInfoModel) => {

            const toEndIndex = 999999;
            let firstIndex = this.markerOrder.indexOf(first.id);
            let secondIndex = this.markerOrder.indexOf(second.id);

            if (firstIndex < 0) { firstIndex = toEndIndex; }
            if (secondIndex < 0) { secondIndex = toEndIndex; }

            if (firstIndex === secondIndex) { return 0; }
            if (firstIndex < secondIndex) { return -1 }

            return 1;

        });
        this.loadFieldsInfo();
        this.setFormData();

    }
    private loadFieldsInfo() {

        this._entityMarkers.forEach(item => {

            if (this.ignoreMarkers.indexOf(item.id) < 0) {
                const marker = this.createMarkerControl(item);
                if (marker) {

                    this.formControl.addView(marker.init());
                    if ("baId" in marker) {
                        (marker as any).baId = this._dataModel.baId;
                    }
                    this.fieldsInfo[item.id] = item;
                    if (item.markerType === 11) {
                        (marker as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
                        (marker as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this._module);
                    }
                    this.markersControls[item.name] = marker;
                }
            }
        });
        ($$(this.formId) as webix.ui.form).addView({
            cols: [
                {},
                this.okBtn.init(),
                this.cancelBtn.init()
            ]
        });
        this.checkSize();
    }
    private createMarkerControl(model: Markers.IMarkerInfoModel): Component.IComponent {

        const elementId = `${this.viewName}-form`;
        const self = this;
        let isDisabled: boolean = false;

        if (this.isCreate) {
            isDisabled = false;
        } else if (model.isBlocked) {
            isDisabled = true;
        }
        /// блокируем маркер выбора организации
        if (model.id === 6) {
            isDisabled = true;
        }
        return MarkerFactory.MarkerControlfactory.getControl(elementId,
            model,
            (self) => {
                return { labelWidth: this.labelWidth, disabled: isDisabled }
            });
    }
    private getFormData(): DocMoveAssets.IDocMoveAssetsData {

        const result = {
            baId: this.isCreate ? null : this._dataModel.baId,
            baTypeId: this.isCreate ? null : this._dataModel.baTypeId,
            number: this.markersControls["Name"].getValue(this.fieldsInfo).value,
            docDate: this.markersControls["DocDate"].getValue(this.fieldsInfo).value,
            caId: this.markersControls["CompanyArea"].getValue(this.fieldsInfo).value,
            reasonId: this.markersControls["FAMovReasons"].getValue(this.fieldsInfo).value,
            srcLocationId: this.markersControls["SrcLocation"].getValue(this.fieldsInfo).value,
            dstLocationId: this.markersControls["DstLocation"].getValue(this.fieldsInfo).value,
            srcLSId: this.markersControls["SrcLSId"].getValue(this.fieldsInfo).value,
            dstLSId: this.markersControls["DstLSId"].getValue(this.fieldsInfo).value,
            srcMLPId: this.markersControls["SrcMLPId"].getValue(this.fieldsInfo).value,
            dstMLPId: this.markersControls["DstMLPId"].getValue(this.fieldsInfo).value,
            note: this.markersControls["Note"].getValue(this.fieldsInfo).value,
            evidence: this.markersControls["Evidence"].getValue(this.fieldsInfo).value,
        } as DocMoveAssets.IDocMoveAssetsData;


        return result;

    }
    private setFormData() {

        if (!this._dataModel) {
            return;
        }

        let model: { [id: string]: any } = {};

        let info = this.markersControls["Name"].markerInfo;
        this.markersControls["Name"].setInitValue(this.getMarkerValue(info, this._dataModel.number as any, null), model);

        info = this.markersControls["DocDate"].markerInfo;
        this.markersControls["DocDate"].setInitValue(this.getMarkerValue(info, this._dataModel.docDate as any, null), model);

        info = this.markersControls["Evidence"].markerInfo;
        this.markersControls["Evidence"].setInitValue(this.getMarkerValue(info, this._dataModel.evidence as any, null), model);

        info = this.markersControls["Note"].markerInfo;
        this.markersControls["Note"].setInitValue(this.getMarkerValue(info, this._dataModel.note as any, null), model);

        info = this.markersControls["CompanyArea"].markerInfo;
        this.markersControls["CompanyArea"].setInitValue(this.getMarkerValue(info, this._dataModel.caId as any, this._dataModel.caName), model);

        info = this.markersControls["SrcLocation"].markerInfo;
        this.markersControls["SrcLocation"].setInitValue(this.getMarkerValue(info, this._dataModel.srcLocationId as any, null), model);

        info = this.markersControls["DstLocation"].markerInfo;
        this.markersControls["DstLocation"].setInitValue(this.getMarkerValue(info, this._dataModel.dstLocationId as any, null), model);

        info = this.markersControls["FAMovReasons"].markerInfo;
        this.markersControls["FAMovReasons"].setInitValue(this.getMarkerValue(info, this._dataModel.reasonId as any, null), model);

        info = this.markersControls["SrcLSId"].markerInfo;
        this.markersControls["SrcLSId"].setInitValue(this.getMarkerValue(info, this._dataModel.srcLSId as any, this._dataModel.srcLSName), model);

        info = this.markersControls["DstLSId"].markerInfo;
        this.markersControls["DstLSId"].setInitValue(this.getMarkerValue(info, this._dataModel.dstLSId as any, this._dataModel.dstLSName), model);

        info = this.markersControls["SrcMLPId"].markerInfo;
        this.markersControls["SrcMLPId"].setInitValue(this.getMarkerValue(info, this._dataModel.srcMLPId as any, this._dataModel.srcMLPName), model);

        info = this.markersControls["DstMLPId"].markerInfo;
        this.markersControls["DstMLPId"].setInitValue(this.getMarkerValue(info, this._dataModel.dstMLPId as any, this._dataModel.dstMLPName), model);

        //add marker value

        this.formControl.setValues(model);
        this.formControl.clearValidation();
      

    }
    private getMarkerValue(info: Markers.IMarkerInfoModel, value: string, displayValue: string): Markers.IMarkerValueModel {
        return {
            displayValue: displayValue,
            itemId: null,
            endDate: null,
            markerId: info.id,
            markerType: info.markerType,
            MVCAliase: info.name,
            note: null,
            isVisible: true,
            isBlocked: false,
            startDate: null,
            value: value,
            isDeleted: false
        }
    }
 
    private isChangeForm(): boolean {
        for (let key in this.markersControls) {
            if (this.markersControls.hasOwnProperty(key)) {
                if (this.markersControls[key].isChanged === true) {
                    return true;
                }
            }
        }
        return false;
    }
}

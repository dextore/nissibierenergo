﻿import Common = require("../../Common/CommonExporter");
import Mngr = require("../ModuleManager");
import Reg = require("../RegisterModules");
import ListView = require("./BankListView");

export class BanksModuleTest extends Common.MainTabBase {

    protected static _viewName = "banks-module-test";
    protected static _header = "Справочник банков";
    private selectedCompany: Inventory.ILSCompanyAreas;


    private listView: ListView.IBankList;

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
        this.listView = ListView.getBanksList(`${this.viewName}-list`, this);
    }
    systemCompanyHandler(company: Inventory.ILSCompanyAreas) {
        this.listView.changeCompany(company);
    }
    getContent(): object {
        return this.listView.init();
    }
    ready() {
        this.listView.ready();
    }

}

Mngr.ModuleManager.registerModule(BanksModuleTest.viewName, (mainView: Common.IMainTabbarView) => {
    return new BanksModuleTest(mainView);
});

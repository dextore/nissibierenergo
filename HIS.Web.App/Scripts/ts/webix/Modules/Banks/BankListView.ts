﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Components/ComponentBase");
import Tab = require("../../MainView/Tabs/MainTabBase");
import ToolButtons = require("../../Components/Buttons/ToolBarButtons");
import Common = require("../../Common/CommonExporter");
import StateEntity = require("../../Components/Controls/EntityOperationStateControl");
import Departaments = require("./BankDepartamentsListView");
import Dialog = require("./BankDialog");
import DataTable = require("../../Components/QueryBuilderListBase/QueryBuilderListBase");
import IQueryBuilderListSetParameters = DataTable.IQueryBuilderListSetParameters;

export interface IBankList extends Base.IComponent {
    changeCompany(company: Inventory.ILSCompanyAreas);
    ready();
}

export function getBanksList(_viewName: string, module: Common.MainTabBase) {
    return new BanksList(_viewName, module);
}

export class BanksList extends Base.ComponentBase implements IBankList {

    private readonly _entityAlias = "Bank";
    private _selectedCompany: Inventory.ILSCompanyAreas;
    private _entityInfo = Common.entityTypes().getByAliase(this._entityAlias).entityInfo;

    private _operationState: StateEntity.IEntityOperationStateControl;

    private _departaments: Departaments.IDepartamentsList;
    private _departamentsModel = {} as Departaments.IDepartamentsModel;
    private _tableBuilderControl: DataTable.IQueryBuilderList;

    // query builder list parameters 
    private _showBaId: boolean = false;
    private _aliasesWithoutFilter = ["CompanyArea"];
    private _aliasesOrder = [
        "BAId",
        "Name",
        "BIK",
        "CorrAccount",
        "OKPO", 
        "INN",
        "RKC",
        "Address",
        "Accept",
        "State",
        "Creator",
        "CompanyArea"];
    
    private get tableId() { return `${this._viewName}-datatable-id`; }

    get tableControl() {
        return $$(this.tableId) as webix.ui.datatable;
    }
    protected toolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}-tooldar`, "Добавить", this.createEntity.bind(this)),
        edit: ToolButtons.getEditBtn(`${this._viewName}-tooldar`, "Изменить", this.editEntity.bind(this))
    }
    constructor(private readonly _viewName: string, private readonly _module: Common.MainTabBase) {
        super();
        this.initSelectedCompany();
        this.initOperationState();
        this.initDataTableControl();
        this._departaments = Departaments.getDepartamentsList(`${this._viewName}-departaments`,_module);
        //this._module.subscribe(this._specification.controlEvent.subscribe(this.reloadStateBySpecification.bind(this), this._module));
      
    }
    private initDataTableControl() {

        const queryBuilderParam = {
            showBaId: this._showBaId,
            tooltip: true,
            entityInfo: this._entityInfo,
            aliasesOrder: this._aliasesOrder,
            aliasesWithoutFilter: this._aliasesWithoutFilter

        } as IQueryBuilderListSetParameters;
        this._tableBuilderControl = DataTable.getQueryBuilderList(this.tableId, queryBuilderParam);
        const companyField = this._tableBuilderControl.getCompanyAliase();
        const baTypeAliase = this._tableBuilderControl.getBATypeIdAliase();
        const filteringArray = [{
            field: baTypeAliase,
            filterOperator: 0,
            value: this._entityInfo.baTypeId
        }] as Array<object>;
        if (companyField != "") {
            filteringArray.push({
                field: companyField,
                filterOperator: 0,
                value: this._selectedCompany.id
            });
        }
        this._tableBuilderControl.setFilteringFields(filteringArray);

    }
    private initSelectedCompany() {
        this._selectedCompany = this._module.mainTabView.getSelectedCompany();
    }
    private initOperationState(): void {
        this._operationState = StateEntity.getEntityOperationStateControl(`${this._viewName}-operation-state`,
            "Справочник банков",
            () => {
                return true;
            });
        this.subscribe(this._operationState.statusChangedEvent.subscribe((data) => {

            const stateId = this._operationState.state.stateId;
            const baId = this._operationState.state.baId;
            const selectId = this.tableControl.getSelectedId(false, true);
            const selected = this.tableControl.getSelectedItem() as any;
            const innAliase = this._tableBuilderControl.getFieldAlias(100) as string;

            this._departamentsModel.baId = baId;
            this._departamentsModel.stateId = stateId;
            this._departamentsModel.inn = selected[innAliase];


            switch (data) {
                case StateEntity.StateChangeAction.Changed:
                    this.updateTableRecord(baId, selectId);
                    break;
                case StateEntity.StateChangeAction.Canceled:
                    this.updateTableRecord(baId, selectId);
                    break;
                default:
                    this._departaments.reloadByModel(this._departamentsModel);
                    break;
            }
            this.resetButtons();

        }, this));
        this._operationState.addButtons(this.toolBar.add, this.toolBar.edit);
    }
    
    /*
    private reloadStateBySpecification() {
        const selected = this.tableControl.getSelectedItem() as any;
        if (!selected) {
            return;
        }
        this.setOperationState(selected[this._tableBuilderControl.getBAIdAliase()]);
    }
    */
    private setOperationState(baId: number) {
        this._departamentsModel = {} as Departaments.IDepartamentsModel;
        if (baId == null) {
            this._operationState.baId = null;
            return;
        }
        this._operationState.baId = baId;


    }
    private updateTableRecord(baId: number, recId?: number) {

        this._tableBuilderControl.getRowDataByBaId(baId).then(data => {

            const row = data.data[0];
            if (!recId) {
                this.tableControl.add(row, 0);
                this._tableBuilderControl.selectFirstRow();
            } else {
                this.tableControl.updateItem(recId, row);
            }
            // 
            const innAliase = this._tableBuilderControl.getFieldAlias(100) as string;
            this._departamentsModel.baId = baId;
            this._departamentsModel.stateId = this._operationState.state.stateId;
            this._departamentsModel.inn = row[innAliase];
            this._departaments.reloadByModel(this._departamentsModel);

        });
    }

    private createEntity() {
        /*
        const dialog = Dialog.getBanksDialog(`${this._viewName}-entity-dialog`,
            this._module,
            this._entityInfo.markersInfo,
            model);

        dialog.showModalContent(this._module, () => {
            this.updateTableRecord(dialog.resultBaId);
            dialog.close();
        });
        */
        


    }
    private editEntity() {

        console.log("clicked add entity button");

        /*
        const selected = this.tableControl.getSelectedItem();
        const selectId = this.tableControl.getSelectedId(false, true);
        if (!selected) {
            webix.message("Необходимо выбрать элемент для редактирования", "error");
            return;
        }
        const baId = selected[this._tableBuilderControl.getBAIdAliase()] as number;

        Api.getApiDocMoveAssets().getDocMoveAssetsData(baId).then(data => {
            const dialog = Dialog.getDocMoveAssetsDialog(`${this._viewName}-entity-dialog`,
                this._module,
                this._entityInfo.markersInfo,
                data);

            dialog.showModalContent(this._module, () => {
                this.updateTableRecord(dialog.resultBaId, selectId);
                dialog.close();
            });
        });
        */
    }
    private resetButtons() {

        this.toolBar.add.button.enable();
        this.toolBar.edit.button.disable();

        const selected = this.tableControl.getSelectedItem() as any;
        if (!selected || !this._operationState.state) {
            return;
        }
        if (this._operationState.state.stateId === 1) {
            this.toolBar.edit.button.enable();
        }

    }
    changeCompany(company: Inventory.ILSCompanyAreas) {

        this._selectedCompany = company;
        const companyField = this._tableBuilderControl.getCompanyAliase();
        const baTypeAliase = this._tableBuilderControl.getBATypeIdAliase();
        const filteringArray = [{
            field: baTypeAliase,
            filterOperator: 0,
            value: this._entityInfo.baTypeId
        }] as Array<object>;
        if (companyField != "") {
            filteringArray.push({
                field: companyField,
                filterOperator: 0,
                value: this._selectedCompany.id
            });
        }
        this._tableBuilderControl.setFilteringFields(filteringArray);
        this._tableBuilderControl.reload();
    }
    ready() {
        this.resetButtons();
        this._tableBuilderControl.ready();
        this._departaments.ready();
        this.tableControl.attachEvent("onSelectChange", this.onSelectChangeTable.bind(this));
    }

    private onSelectChangeTable() {
        const selected = this.tableControl.getSelectedItem() as any;
        if (!selected) {
            this._departamentsModel = {} as Departaments.IDepartamentsModel;
            this._departaments.reloadByModel(this._departamentsModel);
            return;
        }
        this.setOperationState(selected[this._tableBuilderControl.getBAIdAliase()]);
    }
    init() {
        return {
            rows: [
                this._operationState.init(),
                this._tableBuilderControl.init(),
                { view: "resizer" },
                this._departaments.init()
            ]
        };

    }
} 


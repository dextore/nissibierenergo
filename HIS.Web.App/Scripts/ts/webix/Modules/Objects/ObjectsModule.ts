﻿import Common = require("../../Common/CommonExporter");
import Api = require("../../Api/ApiExporter");
import Mngr = require("../ModuleManager");

export class ObjectsModul extends Common.MainTabBase {

    protected static _viewName = "objects-module";
    protected static _header = "Объекты";

    get dataTableId() {
        return `${ObjectsModul.viewName}-datatable-id`;
    }

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
    }

    getContent() {
        //const wath = this;
        return {
            view: "datatable",
            id: this.dataTableId,
            columns: [
                { id: "code", header: "Код", width: 250 },
                { id: "name", header: "Наименование", width: 200 },
                { id: "fullName", header: "Поное наименование", fillspace: true }
            ],
            url: {
                $proxy: true,
                load: (self) => {
                    Api.getApiObjects().get()
                        .then((data) => {
                            (<webix.ui.list>$$(this.dataTableId)).parse(data, "json");
                        });
                }
            }
        }
    }
}

Mngr.ModuleManager.registerModule(ObjectsModul.viewName, (mainView: Common.IMainTabbarView) => {
    return new ObjectsModul(mainView);
});


﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Common/DialogBase");

export function getDoubleAddressDialog(viewName: string): IDoubleAddressDialog {
    return new DoubleAddressDialog(viewName);
};
export interface IDoubleAddressDialog extends Base.IDialog {
}

class DoubleAddressDialog extends Base.DialogBase {

    private get protoUiSearchName() { return `${this.viewName}-search-protoUI`; }
    private get firstAddressSearchId() { return `${this.viewName}-first-address-search-id`; }
    private get firstAddressSuggestId() { return `${this.viewName}-first-address-suggest-id`; }
    private get secondAddressSearchId() { return `${this.viewName}-second-address-search-id`; }
    private get secondAddressSuggestId() { return `${this.viewName}-second-address-suggest-id`; }
    private get formId() { return `${this.viewName}-double-address-form-id`; }

    private firstAddress = {} as Address.IAddresFullName; 
    private secondAddress = {} as Address.IAddresFullName;

    protected headerLabel(): string { return "Соединение/разъединение двойных адресов"; }
    constructor(viewName: string) {
        super(viewName);
    }
    protected afterShowDialog() {
        this.window.define("width", 900);
        this.window.define("height", 400);
        this.window.resize();
    }



    protected contentConfig() {
        const self = this;

        webix.protoUI({
            name: this.protoUiSearchName,
            $cssName: "search custom",
            $renderIcon: function () {
                const config = this.config;
                config.css = "padding - right: 52px;";

                if (config.icons.length) {
                    const height = config.aheight - 2 * config.inputPadding;
                    const padding = (height - 18) / 2 - 1;
                    let pos = 2;
                    let html = "";


                    for (let i = 0; i < config.icons.length; i++) {
                        html += "<span style='right:" + pos + "px;height:"
                            + (height - padding) + "px;padding-top:" + padding
                            + "px;' class='webix_input_icon fa-" + config.icons[i] + "'></span>";
                        pos += 24;
                    }
                    return html;
                }
                return "";
            },
            on_click: {
                "webix_input_icon": function (e, id, node) {
                    const name = node.className.substr(node.className.indexOf("fa-") + 3);
                    return this.callEvent("on" + name + "IconClick", [e]);
                }
            },
        }, webix.ui.search);


        return {
            rows: [
                self.getForm(self),
                self.getToolbar(self)]
        };
    }

    private getForm(self: any): object {

        const formElements = [
            {
                view: "fieldset",
                label: "Первый адрес",
                body: {
                    rows: [
                        {
                            id: self.firstAddressSearchId,
                            view: self.protoUiSearchName,
                            icons: ["close", "search"],
                            name: "firstAddress",
                            type: "form",
                            label: "Адрес",
                            placeholder: "Введите адрес для поиска...",
                            invalidMessage: "Заполните корректный адрес",
                            on: {
                                onSearchIconClick: () => {
                                    const suggest = $$(self.firstAddressSuggestId) as webix.ui.suggest;
                                    const list = suggest.getList() as any;
                                    const search = $$(this.firstAddressSearchId) as webix.ui.search;
                                    if (list.count() > 0) {
                                        suggest.show(search.getInputNode());
                                        return;
                                    }
                                },
                                onCloseIconClick: () => {
                                    this.firstAddress = {} as Address.IAddresFullName; 
                                    const suggest = $$(self.firstAddressSuggestId) as webix.ui.suggest;
                                    const search = $$(this.firstAddressSearchId) as webix.ui.search;
                                    suggest.setValue("");
                                    search.setValue("");
                                }
                            },
                            suggest: {
                                id: this.firstAddressSuggestId,
                                view: "gridsuggest",
                                width: 800,
                                textValue: "addressText",
                                body: {
                                    yCount: 10,
                                    autoheight: false,
                                    columns: [
                                        { id: "aoId", header: "aoId", hidden: true },
                                        { id: "addressText", header: "Адрес", width: 650 },
                                        { id: "id", header: "id", hidden: true },
                                        { id: "houseId", header: "houseId", hidden: true },
                                        { id: "sourceType", header: "sourceType", hidden: true },
                                        { id: "steadId", header: "steadId", hidden: true }
                                    ],
                                    dataFeed: (filtervalue: string) => {
                                        if (filtervalue.length < 2) return;
                                        Api.getApiAddress().addrSearchFull(filtervalue).then((data) => {
                                            const suggest = $$(this.firstAddressSuggestId) as webix.ui.gridsuggest;
                                            const list: any = suggest.getList();
                                            list.clearAll();
                                            list.parse(data, "json");
                                        });
                                    }
                                },
                                on: {
                                    onValueSuggest: (obj: Address.IAddresFullName) => {
                                        this.firstAddress = obj;
                                    }
                                }
                            }
                        }
                    ]
                }
            },
            {
                view: "fieldset",
                label: "Второй адрес",
                body: {
                    rows: [
                        {
                            id: this.secondAddressSearchId,
                            view: self.protoUiSearchName,
                            icons: ["close", "search"],
                            name: "secondAddress",
                            type: "form",
                            label: "Адрес",
                            placeholder: "Введите адрес для поиска...",
                            invalidMessage: "Заполните корректный адрес",
                            on: {
                                onSearchIconClick: () => {
                                    const suggest = $$(this.secondAddressSuggestId) as webix.ui.suggest;
                                    const list = suggest.getList() as any;
                                    const search = $$(this.secondAddressSearchId) as webix.ui.search;
                                    if (list.count() > 0) {
                                        suggest.show(search.getInputNode());
                                        return;
                                    }
                                },
                                onCloseIconClick: () => {
                                    this.secondAddress = {} as Address.IAddresFullName;
                                    const suggest = $$(this.secondAddressSuggestId) as webix.ui.suggest;
                                    const search = $$(this.secondAddressSearchId) as webix.ui.search;
                                    suggest.setValue("");
                                    search.setValue("");
                                } 
                            },
                            suggest: {
                                id: this.secondAddressSuggestId,
                                view: "gridsuggest",
                                width: 800,
                                textValue: "addressText",
                                body: {
                                    yCount: 10,
                                    autoheight: false,
                                    columns: [
                                        { id: "aoId", header: "aoId", hidden: true },
                                        { id: "addressText", header: "Адрес", width: 650 },
                                        { id: "id", header: "id", hidden: true },
                                        { id: "houseId", header: "houseId", hidden: true },
                                        { id: "sourceType", header: "sourceType", hidden: true },
                                        { id: "steadId", header: "steadId", hidden: true }
                                    ],
                                    dataFeed: (filtervalue: string) => {
                                        if (filtervalue.length < 2) return;
                                        Api.getApiAddress().addrSearchFull(filtervalue).then((data) => {
                                            const suggest = $$(this.secondAddressSuggestId) as webix.ui.gridsuggest;
                                            const list: any = suggest.getList();
                                            list.clearAll();
                                            list.parse(data, "json");
                                        });
                                    }
                                },
                                on: {
                                    onValueSuggest: (obj: Address.IAddresFullName) => {
                                        this.secondAddress = obj;
                                    }
                                }
                            }
                        }
                    ]
                }
            }

        ];

        return {
            id: self.formId,
            view: "form",
            animate: false,
            elements: formElements,
            borderless: true,
            margin: 10,
            rules: {
                "firstAddress": () => {
                    return self.validateFormFields("firstAddress");
                },
                "secondAddress": () => {
                    return self.validateFormFields("secondAddress");
                }
            },
            elementsConfig: {
                labelPosition: "right",
                labelAlign: "left",
                labelWidth: 130
            }
        };
    }

    private validateFormFields(field: string): boolean {

        switch (field) {
            case "firstAddress":
                if (this.isEmptyObject(this.firstAddress)) {
                    return false;
                }
                if (this.firstAddress.houseId == null) {
                    webix.message("Адрес должен заканчиваться домом!", "error");
                    return false;
                }
                if (!this.isEmptyObject(this.secondAddress) &&
                    this.secondAddress.houseId === this.firstAddress.houseId) {
                    webix.message("Адреса совпадают!", "error");
                    return false;

                }
                break;
            case "secondAddress":
                if (this.isEmptyObject(this.secondAddress)) {
                    return false;
                }
                if (this.secondAddress.houseId == null) {
                    webix.message("Адрес должен заканчиваться домом!", "error");
                    return false;
                }
                if (!this.isEmptyObject(this.firstAddress) &&
                    this.secondAddress.houseId === this.firstAddress.houseId) {
                    webix.message("Адреса совпадают!", "error");
                    return false;
                }
            break;
        }
        return true;
    }
    private isEmptyObject(obj: object): boolean {

        if (obj == null || obj == undefined) {
            return true;
        }
        if (Object.keys(obj).length === 0) {
            return true;
        }
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }




    private getToolbar(self: any): object {
        return {
            view: "toolbar",
            paddingY: 10,
            cols: [
                {},
                {
                    view: "button", label: "Объединить", align: "right", width: 100,
                    toolbar: "bottom",
                    click: () => {
                        const form = $$(this.formId) as webix.ui.form;
                        if (!form.validate()) {
                            return;
                        }
                        const model = {
                            firstHouseId: this.firstAddress.houseId,
                            secondHouseId: this.secondAddress.houseId,
                            isBind:true

                        } as Address.IBindHousesModel;

                        Api.getApiAddress().bindHouses(model)
                            .then((data) => {
                                webix.message("Адреса объединены","message");
                                this.okClose();                                                               
                            }).catch(() => {
                                webix.message("Ошибка сопоставления","error");
                            });

                    }
                },
                {
                    view: "button", label: "Разъединить", align: "right", width: 100,
                    toolbar: "bottom",
                    click: () => {
                        const form = $$(this.formId) as webix.ui.form;
                        if (!form.validate()) {
                            return;
                        }
                        const model = {
                            firstHouseId: this.firstAddress.houseId,
                            secondHouseId: this.secondAddress.houseId,
                            isBind: false

                        } as Address.IBindHousesModel;

                        Api.getApiAddress().bindHouses(model)
                            .then((data) => {
                                webix.message("Адреса разъединены", "message");
                                this.okClose();
                            }).catch(() => {
                                webix.message("Ошибка сопоставления", "error");
                            });
                    }
                },
                {
                    view: "button", value: "Закрыть", align: "right", width: 80,
                    click: () => {
                        self.close();
                    }
                },
                { align: "right", width: 5 }
            ]
        }
    }
}
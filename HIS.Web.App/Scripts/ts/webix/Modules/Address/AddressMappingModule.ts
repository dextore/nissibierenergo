﻿import Common = require("../../Common/CommonExporter");
import Api = require("../../Api/ApiExporter");
import Mngr = require("../ModuleManager");
import AddEditAddress = require("../../Components/Address/AddEditAddress");
import AddEditAddressObject = require("../../Components/Address/AddEditAddressObject");
import DoubleAddress = require("./DoubleAddressDialog");

export class AddressMappingModule extends Common.MainTabBase {

    protected static _viewName = "address-mapping-module";
    protected static _header = "Сопоставление адресов";
    private get addEditDialogId() { return `${this.viewName}-add-edit-addressObject-id`; }

    get treeTableId(): string {
        return `${this.viewName}-datatable-id`;
    }


    private updateDataTree(): void
    {
        console.log("Start Update Tree");
        const table = $$(this.treeTableId) as webix.ui.treetable;
        table.clearAll();
        table.showOverlay("Loading....");
        Api.getApiAddress().getAddressMappings()
            .then((data) => {
                const result = this.modifyJsonTree(data);
                table.parse(result,"json");
                table.hideOverlay();
            });

    }

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
    }

    getContent(): object {
        const self = this;
        return {
            rows: [
                {
                    view: "toolbar",
                    css: "toolbar",
                    paddingY: 5,
                    paddingX: 10,
                    cols: [
                       {
                            view: "button",
                            label: "Обновить таблицу",
                            width: 140,
                            click: () => {
                                self.updateDataTree();
                            }
                        },
                        {
                            view: "button",
                            label: "Редактировать адрес",
                            width: 160,
                            click: () => {
                                const addDialog = AddEditAddress.getAddressDialog(`${self.viewName}-add-edit-address`);
                                addDialog.showModal(() => {
                                    self.updateDataTree();
                                    addDialog.close();
                                });

                            }
                        }, {
                            view: "button",
                            label: "Двойные адреса",
                            width: 200,
                            click: () => {
                                const dialog = DoubleAddress.getDoubleAddressDialog(`${self.viewName}-double-address`);
                                dialog.showModal(() => {
                                    dialog.close();
                                });

                            }
                        }
                    ]
                },
                self.getTreeTable(self)
            ]
        };

    }

    private getTreeTable(self: any): object {
        return {
            id: self.treeTableId,
            view: "treetable",
            scrollX: true,
            select: "row",
            fixedRowHeight: false,
            sizeToContent: false,
            columns: self.getColumsTreeTable(),
            scheme: {
                $sort: { by: "fullName", as: "string", dir: "asc" }
            },
            url: {
                $proxy: true,
                load: () => {
                    self.updateDataTree();
                }
            },
            on: {
                "onItemDblClick": () => {
                    self.editAddressObject();
                }
            }

        };
    }


    private editAddressObject(): void {
        const treeTable = $$(this.treeTableId) as webix.ui.treetable;
        const item = treeTable.getSelectedItem() as Address.IAddressComparisonsDpModel;
        if (item.sourceTypeId !== "2") { return; }
        console.log(item.steadId);
        if (item.steadId !== null) {
            Api.getApiAddress().getSteadUpdateInfo(item.steadId).then((data: Address.ISteadUpdateModel) => {
                const dialog = AddEditAddressObject.getAddEditAddressObjectDialog(this.addEditDialogId, null, data, null);
                dialog.showModal(() => {
                    dialog.close();
                    this.updateDataTree();
                });
            });
        } else if (item.houseId !== null) {
            Api.getApiAddress().getHouseUpdateInfo(item.houseId).then((data: Address.IHouseUpdateModel) => {
                data.sourceType = 2;
                const dialog = AddEditAddressObject.getAddEditAddressObjectDialog(this.addEditDialogId, null, null, data);
                dialog.showModal(() => {
                    dialog.close();
                    this.updateDataTree();
                });

            });
        } else {
            Api.getApiAddress().getAddressObject(item.AOid).then((data: Address.IAddressObjectModel) => {
                const aoModel = {
                    AOGuid: item.AOGuid,
                    AOId: item.AOid,
                    AOLevel: item.AOLevel,
                    parentId: item.parentGuid,
                    formalName: data.formalName,
                    shortName: data.shortName,
                    postalCode: data.postCode
                } as Address.IAddressObjectUpdateModel;

                const dialog = AddEditAddressObject.getAddEditAddressObjectDialog(this.addEditDialogId, aoModel, null, null);
                dialog.showModal(() => {
                    dialog.close();
                    this.updateDataTree();
                });

            });
        }


    }


    private getColumsTreeTable(): Array<object> {
        return [
            {
                id: "fullName",
                header: "Адресный справочник системы",
                adjust: true,
                template: "{common.treetable()} #fullName#",
                sort: "string"
            }, {
                id: "fiasFullName",
                adjust: true,
                header: "ФИАС"
            }, {
                id: "Title",
                header: "Статус сопоставления",
                adjust: true,
                template: (obj)=> {
                    if (obj.sourceTypeId === "2") {
                        if (obj.fiasAOId != null ||
                            obj.fiasHouseId != null ||
                            obj.fiasSteadId != null) {
                            return "Сопоставлен";
                        }
                        return "Не сопоставлен";
                    }
                    return "";
                }
            },
            {
                id: "AOid",
                header: "AOid"
            }, {
                id: "parentGuid",
                header: "parentGuid",
            },
            {
                id: "houseId",
                header: "houseId"
            },
            {
                id: "steadId",
                header: "steadId"
            },
            {
                id: "AOGuid",
                header: "AOGuid"
            },
            {
                id: "AOLevel",
                header: "AOLevel"
            },
            {
                id: "fiasAOId",
                header: "fiasAOId"
            },
            {
                id: "fiasHouseId",
                header: "fiasHouseId"               
            },
            {
                id: "fiasSteadId",
                header: "fiasSteadId"
            },
            {
                id: "fiasParentGuid",
                header: "fiasParentGuid"
            },
            {
                id: "sourceTypeId",
                header: "sourceTypeId"
            }
        ];
    }
    // преобразовываем json object для treetable
    private modifyJsonTree(arrayTree: Array<object>): Array<object> {
        const outArray: Array<object> = [];
        for (let counter: number = 0; counter < arrayTree.length; counter++) {
            const currentObject = arrayTree[counter];
            currentObject["open"] = false;
            if (currentObject["items"] !== null) {
                currentObject["open"] = true;
                const dataArray = this.modifyJsonTree(currentObject["items"] as Array<object>);
                currentObject["data"] = dataArray;
            }
            outArray.push(currentObject as any);
        }
        return outArray;
    }
}


Mngr.ModuleManager.registerModule(AddressMappingModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new AddressMappingModule(mainView);
});
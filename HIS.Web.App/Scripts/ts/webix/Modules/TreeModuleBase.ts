﻿import Common = require("../Common/CommonExporter");

export abstract class TreeModuleBase extends Common.MainTabBase {

    protected get treeId(): string {
        return `${this.viewName}-tree-id`;
    }

    protected get leftSideId(): string {
        return `${this.viewName}-left-side-id`;
    }

    // Дерево 
    // Структура
    // Источник данных

    // Получить entity 
    // Вставить в дерево
    // Спозиционироваться


    // Левая часть, способ наполнения?

    protected abstract moduleConfig(): any;

    getContent() {
        return {
            cols: [this.leftSideConfig(),
                { view: "resizer" }, this.treeConfigInternal()],

            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        };
    }

    private leftSideConfig() {
        return {
            gravity: 3,
            id: this.leftSideId,
            cols: [this.moduleConfig()]
        }
    }

    private treeConfigInternal(): any {
        const config = {
            view: "tree", 
            id: this.treeId
        };

        return webix.extend(config, this.treeConfig(), true);
    }

    protected treeConfig() {
        return {};
    }  
} 


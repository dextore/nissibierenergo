﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../../Components/Markers/EntytyRefSelectDlgBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Btn = require("../../../Components/Buttons/ButtonsExporter");
import Pager = require("../../../Components/Pager/Pager");
import Common = require("../../../Common/CommonExporter");

export function getRecipientAccountSelectDlg(viewName: string) {
    return new RecipientAccountSelectDlg(viewName);
}

class RecipientAccountSelectDlg extends Base.EntityRefSelectDlgBase implements Base.IEntityRefSelectDlg {

    headerLabel(): string { return "Cчет получателя денежных средств"; }

    private _parentBaId: number;

    get pagerId(): string {
        return `${this.viewName}_pager_id`;
    }

    get dataTableId(): string {
        return `${this.viewName}_datatable_id`;
    }

    get dataTable(): webix.ui.datatable {
        return $$(this.dataTableId) as webix.ui.datatable;
    }

    private _okBtn = Btn.getOkButton(this.viewName, () => {
        if (!this.selectedValue)
            return; 
        this.okClose(); 
    });
    private _cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });

    constructor(viewName: string) {
        super(viewName, true);
    }

    protected getSelected(): Base.IResultModel {
        const selectedRows = this.dataTable.getSelectedItem(true);
        return (selectedRows.length) ? { baId: selectedRows[0].bankAccountId, name: selectedRows[0].number } : null;
    }

    showSelect(parentBaId: number, module: Tab.IMainTab, callBack?: () => void) {
        this._parentBaId = parentBaId;
        this.showModalContent(module, callBack);
    }

    protected ready() {
        webix.extend($$(this.dataTableId), webix.ProgressBar);
    }

    contentConfig() {
        return {
            rows: [
                this.dataTableConfig(),
                this.footerToolBarConfig()
            ]
        }
    }

    dataTableConfig() {
        return {
            view: "datatable",
            id: this.dataTableId,
            select: "row",
            scroll: "auto",
            height: 200,
            columns: [
                { id: "number", header: "Номер", width: 300 },
                { id: "bankName", header: "Банк", fillspace: true },
                { id: "startDate", header: "Дата открытия", width: 200, format: webix.i18n.dateFormatStr },
                { id: "endDate", header: "Дата закрытия", width: 200, format: webix.i18n.dateFormatStr },
                { id: "stateName", header: "Состояние", width: 200 },
                { id: "isDefault", header: "По умолчанию", width: 150, template: Common.TemplateHelper.DatatableColumnBooleanTemplate() }
            ],
            url: {
                $proxy: true,
                load: (view, callback, params) => {
                    ($$(this.dataTableId) as webix.ui.datatable).clearAll();
                    if (!this._parentBaId) {
                        (webix.ajax as any).$callback(view, callback, []);
                        return;
                    }
                    Api.ApiLSbankAccounts.getLSBankAccounts(this._parentBaId)
                        .then((data) => {
                            (webix.ajax as any).$callback(view, callback, data);
                        });
                }
            }
        }
    }

    footerToolBarConfig() {
        return {
            cols: [
                {},
                this._okBtn.init(),
                this._cancelBtn.init()
            ]
        }
    }
}
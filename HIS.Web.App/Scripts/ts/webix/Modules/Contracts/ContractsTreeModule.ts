﻿import Base = require("../../MainView/Tabs/MainTabBase");
import Tab = require("../../MainView/IMainTabsView")
import Mngr = require("../ModuleManager");
import View = require("./Views/ContractView");
import Common = require("../../Common/CommonExporter");


export interface IContractsTreeModule extends Base.IMainTab {
    systemSearchHandler(entity: Entity.IFullTextSearchEntityInfoModel);
}

export class ContractsTreeModule extends Base.MainTabBase implements IContractsTreeModule {

    protected static _viewName = "contract-tree-module";
    protected static _header = "Договора";

    private _selectedItem: Tree.ISystemTreeItemModel;

    private _view = View.getContractModuleView(`${this.viewName}-contract-view`, this);

    constructor(mainTabView: Tab.IMainTabbarView) {
        super(mainTabView);
    }

    addNew(): any {
        this._view.addNew();
    }

    getIsTreeNeeded(): boolean {
        return true;
    }

    treeSelectedChangeHandler(entity: Tree.ISystemTreeItemModel): void {
        this._selectedItem = entity;

        // Найти чей контракт
        // Добавить в дерево
        // Добавить к владельцу контнракт
        // Загрузить данные в контракт


        if (!this._selectedItem || (entity.baTypeId !== Common.BATypes.Contract)) {
            this._view.contractId = null;
            return;
        }

        this._view.contractId = entity.baId;
    }

    ready() {
        super.ready();
        this._view.ready();
    }

    getContent() {
        return {
            view: "multiview",
            animate: false,
            cells: [
                this._view.init(),
            ],
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        }
    }
}


Mngr.ModuleManager.registerModule(ContractsTreeModule.viewName, (mainView: Tab.IMainTabbarView) => {
    return new ContractsTreeModule(mainView);
});

﻿import Base = require("../../../Components/ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Toolbar = require("./ContractToolbar");
import Preview = require("./ContractInfoView");
import Event = require("../../../Common/Events");
import DP = require("./Dependencies/DependenciesView");

export interface IContractModuleView extends Base.IComponent {
    contractId: number;
    addNew();
    ready();
}

export function getContractModuleView(viewName: string, module: Tab.IMainTab) {
    return new ContractModuleView(viewName, module);
}

class ContractModuleView extends Base.ComponentBase implements IContractModuleView {
    private _contractId: number;
    private _toolbar: Toolbar.IContractModuleToolbar;
    private _view: Preview.IContractModulePreviewView;
    private _contractChangeEvent: Event.IEvent<number> = new Event.Event<number>();
    private _dependenciesView: DP.IDependenciesView;

    get viewName() {
        return `${this._viewName}_contract_module_view`;
    }

    get contractId() {
        return this._contractId;
    }

    get viewId() {
        return `${this.viewName}_view_id`;
    }


    get stubId() {
        return `${this.viewName}_contract_stub`;
    }

    get contractChangeEvent(): Event.IEvent<number> {
        return this._contractChangeEvent;
    }

    // Событи (сигдал контролам на перечитывание данных!!! 1 внешний - дерево, поиск, переход из legal subject, 2 внутрениий - смена состояния - флрма добавить изменить)

    set contractId(value) {
        this._contractId = value;
        this._contractChangeEvent.trigger(this._contractId, this);
    }

    private contractChangeEventHandler(contractId: number) {
        this._toolbar.contractId = contractId;
        this._view.contractId = contractId;
        this._dependenciesView.contractId = contractId;
        (contractId) ? $$(this.stubId).hide() : $$(this.stubId).show();
    }

    constructor(private readonly _viewName: string, private readonly _module: Tab.IMainTab) {
        super();
        this._toolbar = Toolbar.getContractModuleToolbar(this.viewName, this._module, this.contractChangeEvent);
        this._view = Preview.getContractModulePreviewView(this.viewName, this._module);
        this._dependenciesView = DP.getDependenciesView(this.viewName, this._module);

        this.subscribe(this.contractChangeEvent.subscribe(this.contractChangeEventHandler, this));
        this.subscribe(this._view.editetdEvent.subscribe(this._toolbar.edit, this));
    }

    addNew() {
        this._toolbar.addNew();
    }

    ready() {
        this._toolbar.ready();
        this._view.ready();
    }

    init() {
        return {
            rows: [
                {
                    id: this.viewId, 
                    rows: [
                        // toolbar
                        this._toolbar.init(),
                        // форма представление
                        this._view.init(),
                        this._dependenciesView.init()
                    ]
                }, {
                    id: this.stubId,
                    hidden: true,
                    rows: [{}]
                }
            ]
        }
    }
}
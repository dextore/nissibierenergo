﻿import Common = require("../../../Common/CommonExporter");

export interface IContractFormHelper {
    controlNames: Common.IControlInfo[];
    markersInfo: { [id: string]: Markers.IMarkerInfoModel; }
}

export function getContractFormHelper(): IContractFormHelper {
    return new ContractFormHelper();
}

class ContractFormHelper implements IContractFormHelper {

    constructor() {
        if (!markersInfo) {
            markersInfo = {};
            const mInfo = Common.entityTypes().getByAliase("Contract").entityInfo.markersInfo;
            mInfo.forEach(item => {
                markersInfo[item.name] = item;
            });
        }
    }

    get controlNames(): Common.IControlInfo[] {
        return controlNames;
    };

    get markersInfo() {
        return markersInfo;
    };
}

let markersInfo: { [index: string]: Markers.IMarkerInfoModel; };
const controlNames: Common.IControlInfo[] = [{ name: "Name", labelWidth: 130 },
                                             { name: "CompanyArea", labelWidth: 100 },
                                             { name: "UKGroup", labelWidth: 100 },
                                             { name: "LegalSubject", labelWidth: 100 },
                                             { name: "StartDate", labelWidth: 230 },
                                             { name: "EndDate", labelWidth: 230 },
                                             { name: "DocDate", labelWidth: 150 },
                                             { name: "ContractType", labelWidth: 120 },
                                             { name: "Note", labelWidth: 80 }];  

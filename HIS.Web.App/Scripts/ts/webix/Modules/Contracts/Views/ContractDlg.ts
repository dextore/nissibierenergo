﻿import Base = require("../../../Common/DialogBase");
import Btn = require("../../../Components/Buttons/ButtonsExporter");
import Form = require("./ContractForm");
import Tab = require("../../../MainView/Tabs/MainTabBase");

export interface IContractDlg extends Base.IDialog {
    contractId: number;
    getFormData(): Markers.IEntityUpdateModel;
}

export function getContractDlg(viewName: string, module: Tab.IMainTab): IContractDlg {
    return new ContractDlg(viewName, module);
}

class ContractDlg extends Base.DialogBase implements IContractDlg {

    private _contractId: number;
    private _okBtn = Btn.getOkButton(this.viewName, () => { this.okClose(); });
    private _cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });
    private _form: Form.IContractForm;

    get contractId(): number {
        return this._form.contractId;
    }

    set contractId(value: number) {
        this._contractId = value;
        if (this.window) {
            this._form.contractId = value;
        }
    }

    constructor(viewName: string, private readonly _module: Tab.IMainTab) {
        super(viewName);
        this._form = Form.getContractForm(`${this.viewName}-form`, _module);
        this.hPadding = 100;
        this.vPadding = 100;
    }

    bindModel(): void {
        this._form.contractId = this._contractId;
    }

    showModalContent(module: Tab.IMainTab, callBack?: () => void) {
        super.showModalContent(module, callBack);
        this.tabItem.disableCompany();
    } 

    destroy(): void {
        this.tabItem.enableCompany();
        super.destroy();
    }

    getFormData(): Markers.IEntityUpdateModel {
        return this._form.getFormData();
    }

    headerLabel(): string { return "Договор"; }

    okClose(): void {
        if (!this._form.validate())
            return;

        this._form.saveData().then((contractId: number) => {
            this._contractId = contractId;
            super.okClose();
        });
    }

    contentConfig() {
        return {
            rows: [
                this._form.init(),
                this.fotterToolBar()
            ]
        }
    }

    private fotterToolBar() {
        return {
            height: 40,
            cols: [
                {},
                this._okBtn.init(),
                this._cancelBtn.init()
            ]
        }
    }

    protected windowConfig() {
        return {
            autoheight: true,
        };
    }

}
﻿import Base = require("../../../Components/ComponentBase");
import Operation = require("../../../Components/Controls/EntityOperationStateControl");
import ToolButtons = require("../../../Components/Buttons/ToolBarButtons");
import Dlg = require("./ContractDlg");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Event = require("../../../Common/Events");
import Common = require("../../../Common/CommonExporter");

export interface IContractModuleToolbar extends Base.IComponent {
    contractId: number;
    ready();
    addNew();
    edit(value: boolean);
}

export function getContractModuleToolbar(viewName: string, module: Tab.IMainTab, contractChangeEvent: Event.IEvent<number>): IContractModuleToolbar {
    return new ContractModuleToolbar(viewName, module, contractChangeEvent);
}

class ContractModuleToolbar extends Base.ComponentBase implements IContractModuleToolbar {

    private _addNewBtn: ToolButtons.IToolBarButton;
    private _editBtn: ToolButtons.IToolBarButton;

    get viewName(): string {
        return `${this._viewName}-toolbar`;
    }

    get contractId(): number {
        return this._operationState.baId;
    }

    set contractId(value: number) {
        this._operationState.baId = value;

        if (!this._editBtn.button)
            return;

        (!this._operationState.baId) ? this._editBtn.button.disable() : this._editBtn.button.enable();
    }

    private _operationState = Operation.getEntityOperationStateControl(`${this.viewName}-operation-state`,
        (contractId: number) => {
            const promise = webix.promise.defer();

            //Api.ApiLegalPersons.getLegalPerson(personalId, []).then(result => {
            //    (promise as any).resolve(`${result.name} (${result.code})`);
            //}).catch(() => { (promise as any).reject() });

            (promise as any).resolve(`Номер договора и еще чтото!!!`);

            return promise;
        },
        () => {
            return true;
        });
    

    constructor(private readonly _viewName: string,
                private readonly _module: Tab.IMainTab,
                private readonly _contractChangeEvent: Event.IEvent<number>) {
        super();

        this._addNewBtn = ToolButtons.getNewBtn(`${this.viewName}-operation-toolbar`, "Добавить", (id, e) => {
            this.showDlg(null);
        });
        this._editBtn = ToolButtons.getEditBtn(`${this.viewName}-operation-toolbar`, "Изменить", (id, e) => {
            this.showDlg(this.contractId);
        });
        this._operationState.addButtons(this._addNewBtn, this._editBtn);
    }

    edit(value: boolean) {
        if (!this._editBtn)
            return; 
        (value) ? this._editBtn.button.enable() : this._editBtn.button.disable();
    }

    addNew() {
        this.showDlg(null);
    }

    private showDlg(contractId: number) {
        const dlg = Dlg.getContractDlg(`${this._viewName}-dlg-form`, this._module);
        dlg.contractId = contractId;

        dlg.showModalContent(this._module, () => {
            if (!contractId) {
                const data = dlg.getFormData();
                this.sendTreeChange(dlg.contractId, data);
            } else {
                this._contractChangeEvent.trigger(dlg.contractId);
            }
            dlg.close();
        });
    }

    private sendTreeChange(contractId: number, model: Markers.IEntityMarkersModel) {

        const filtered = model.markerValues.filter(item => { return item.MVCAliase === "Name" });
        const entityTypeInfo = Common.entityTypes().getByAliase("Contract").entityInfo;

        const entityInfo = {
            baId: contractId,
            endDate: null,
            name: filtered[0].value,
            startDate: null,
            stateId: null,
            stateName: null,
            typeId: entityTypeInfo.baTypeId,
            typeName: entityTypeInfo.name
        };

        this._module.mainTabView.sendTreeItem(entityInfo as any);
    }

    ready() {
        this._addNewBtn.button.enable();
    }

    init() {
        return this._operationState.init();
    }
}


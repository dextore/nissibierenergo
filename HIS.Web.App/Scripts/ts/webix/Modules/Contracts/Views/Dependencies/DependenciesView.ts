﻿import Base = require("../../../../Components/ComponentBase");
import OP = require("./Markers/ContractMarkerForm");
import Tab = require("../../../../MainView/Tabs/MainTabBase");
import Spec = require("./Specification/SpecificationView")
import Contract = require("../ContractView");


export interface IDependenciesView extends Base.IComponent {
    contractId: number;
    ready();
}

export function getDependenciesView(viewName: string,
    module: Tab.IMainTab): IDependenciesView {
    return new DependenciesView(viewName, module);
}


class DependenciesView extends Base.ComponentBase implements IDependenciesView {

    private _contractId: number = null;
    private _optionalView: OP.IContractMarkerForm;
    private _specification: Spec.ISpecificationView;

    set contractId(value: number) {
        this._contractId = value;
        if (this.tabView) {
            this._contractId ? this.tabView.show() : this.tabView.hide();
            this._optionalView.contractId = this._contractId;
            this._specification.contractId = this._contractId;
        }
    };

    get viewName() {
        return `${this._viewName}_dependencies_view`;
    }

    get tabViewId() {
        return `${this.viewName}_tab_id`;
    }

    get tabView() {
        return $$(this.tabViewId) as webix.ui.tabview;
    }

    constructor(
        private readonly _viewName: string,
        private readonly _module: Tab.IMainTab) {
        super();
        this._optionalView = OP.getContractMarkerForm(`${this.viewName}-optional`, _module);
        this._specification = Spec.getSpecificationView(`${this.viewName}-specification`, _module, this._optionalView);
    }

    ready() {
        this._contractId ? this.tabView.show() : this.tabView.hide();
        this._optionalView.contractId = this._contractId;
    }

    init() {
        return {
            id: this.tabViewId,
            view: "tabview",
            hidden: true,
            tabbar: {
                optionWidth: 200
            },
            cells: [
                {
                    header: "Договорные условия",
                    body: this._optionalView.init()
                },
                {
                    header: "Спецификация",
                    body: this._specification.init()
                }
            ]
        }    
    }

}
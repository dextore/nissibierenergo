﻿import Api = require("../../../../../Api/ApiExporter");
import Base = require("../../../../../Components/ComponentBase");
import Tab = require("../../../../../MainView/Tabs/MainTabBase");
import Common = require("../../../../../Common/CommonExporter");

export interface IContractMarkerInfoForm extends Base.IComponent {
    loadData(markerId: number, contractId: number);
}

export function getContractMarkerInfoForm(viewName: string, module: Tab.IMainTab): IContractMarkerInfoForm {
    return new ContractMarkerInfoForm(viewName, module);
}

class ContractMarkerInfoForm extends Base.ComponentBase implements IContractMarkerInfoForm {

    get viewName() {
        return `${this._viewName}_view`;
    }

    get formId() {
        return `${this.viewName}_form_id`;
    }

    get form() {
        return $$(this.formId) as webix.ui.form;
    }

    constructor(private readonly _viewName: string, private readonly _mainTab: Tab.IMainTab) {
         super();
    }

    loadData(markerId: number, contractId: number) {
        if (!contractId) {
            return;
        }
        Api.getApiMarkers().entityMarkerValues({
            baId: contractId,
            markerIds: [],
            names: []
        }).then(result => {
            const filtered = result.markerValues.filter(item => { return item.markerId === markerId });
            if (filtered.length) {
                const markerInfo = Common.entityTypes().getByAliase("Contract").markersInfoArray
                    .filter(item => { return item.id === markerId });
                var value = "";
                switch (markerInfo[0].markerType) {
                    case HIS.Models.Layer.Models.Markers.MarkerType.MtBoolean:
                    case HIS.Models.Layer.Models.Markers.MarkerType.MtInteger:
                    case HIS.Models.Layer.Models.Markers.MarkerType.MtDecimal:
                    case HIS.Models.Layer.Models.Markers.MarkerType.MtString:
                    case HIS.Models.Layer.Models.Markers.MarkerType.MtMoney:
                    case HIS.Models.Layer.Models.Markers.MarkerType.MtDateTime:
                        value = filtered[0].value;
                        break;
                    case HIS.Models.Layer.Models.Markers.MarkerType.MtList:
                    case HIS.Models.Layer.Models.Markers.MarkerType.MtEntityRef:
                    case HIS.Models.Layer.Models.Markers.MarkerType.MtAddress:
                    case HIS.Models.Layer.Models.Markers.MarkerType.MtReference:
                        value = filtered[0].displayValue;
                        break;
                }
                const dateFormat = webix.Date.dateToStr("%d.%m.%Y", false);
                const model = (!filtered[0].value)
                    ? {}
                    : {
                          name: markerInfo[0].label,
                          value: value,
                          startDate: dateFormat(filtered[0].startDate),
                          endDate: filtered[0].endDate === null ? "31.12.9999" : dateFormat(filtered[0].endDate)
                      };
                this.form.setValues(model);    
            }
        });
    }

    initControls() {
        return  [
                { cols: [{ view: "label", name: "name" }, { view: "label", name: "value", gravity:2 }] },
                { cols: [{ view: "label", name: "startDate" }, { view: "label", name: "endDate" }] }
            ];
    }

    init() {
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: this.initControls(),
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        }
    }

}
﻿import Base = require("../../../../../Components/ComponentBase");
import Tab = require("../../../../../MainView/Tabs/MainTabBase");
import Form = require("./InventMarkersForm");
import Toolbar = require("./InventMarkersToobar");

export interface IInventMarkersView extends Base.IComponent {
    baId: number;
    contractModel: Markers.IEntityMarkersModel;
}

export function getInventMarkersView(viewName: string, module): InventMarkersView {
    return new InventMarkersView(viewName, module);
}

class InventMarkersView extends Base.ComponentBase implements IInventMarkersView {

    private _form: Form.IInventMarkersForm;
    private _toolbar: Toolbar.IInventMarkersToobar;

    get viewName() {
        return `${this._viewName}_view`;
    }

    get viewId() {
        return `${this.viewName}_id`;
    }

    get stubViewId() {
        return `${this.viewName}_stub_id`;
    }

    get baId(): number {
        return this._form.baId;
    }

    set baId(value: number) {
        this._form.baId = value;
        this._toolbar.disabled = !value;
        const view = $$(this.viewId);
        if (!view)
            return;

        if (value) {
            view.show();
            $$(this.stubViewId).hide();
            return;
        }
        view.hide();
        $$(this.stubViewId).show();
    }

    get contractModel(): Markers.IEntityMarkersModel {
        return this._form.contractModel;
    };

    set contractModel(model: Markers.IEntityMarkersModel) {
        this._form.contractModel = model;
    };

    constructor(private readonly _viewName: string, private readonly _module: Tab.IMainTab) {
        super();
        this._form = Form.getInventMarkersForm(_viewName, _module);
        this._toolbar = Toolbar.getInventMarkersToobar(_viewName, _module);
        this.subscribe(this._toolbar.buttonClickEvent.subscribe((arg: Toolbar.ButtonEnum) => {
            switch (arg) {
                case Toolbar.ButtonEnum.Edit:
                    this._form.edit();
                    break;
                case Toolbar.ButtonEnum.Cancel: 
                    this._form.cancel();
                    break;
                case Toolbar.ButtonEnum.Save:
                    this._form.save();
                    break;
            default:
            }
        }, this));
        this.subscribe(this._form.stateChangeEvent.subscribe((state: Form.FormStateEnum) => {
            switch (state) {
                case Form.FormStateEnum.Edit:
                    this._toolbar.disabled = false;
                    this._toolbar.readonly = false;
                    break;
                case Form.FormStateEnum.ReadOnly:
                    this._toolbar.disabled = false;
                    this._toolbar.readonly = true;
                    break;
                case Form.FormStateEnum.Disabled:
                    this._toolbar.disabled = true;
                    break;
            }
        }, this));
    }

    init() {
        return {
            rows: [
                {
                   id: this.viewId,  
                   rows:[ this._toolbar.init(),
                          this._form.init()]
                },
                {
                    id: this.stubViewId,
                    hidden: true
                }
            ],
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        }
    }
}
﻿import Base = require("../../../../../Components/ComponentBase");
import Tab = require("../../../../../MainView/Tabs/MainTabBase");
import Event = require("../../../../../Common/Events");
import ToolBtn = require("../../../../../Components/Buttons/ToolBarButtons");

export enum ButtonEnum {
    Edit,
    Save,
    Cancel
}

export interface IInventMarkersToobar extends Base.IComponent {
    readonly: boolean;
    buttonClickEvent: Event.IEvent<ButtonEnum>;
    disabled: boolean;
}

export function getInventMarkersToobar(viewName: string, module: Tab.IMainTab): IInventMarkersToobar {
    return new InventMarkersToobar(viewName, module);
}

class InventMarkersToobar extends Base.ComponentBase implements IInventMarkersToobar {

    private _buttonClickEvent: Event.IEvent<ButtonEnum> = new Event.Event<ButtonEnum>();

    get viewName(): string {
        return `${this._viewName}_view`;
    }

    private get toolbarId(): string {
        return `${this.viewName}_toolbar_id`;
    }

    get readonly(): boolean {
        return !this._toolbar.edit.button.isEnabled();
    }

    get disabled(): boolean {
        return !this._toolbar.cancel.button.isEnabled() &&
            !this._toolbar.save.button.isEnabled() &&
            !this._toolbar.edit.button.isEnabled();
    };

    set disabled(value: boolean) {
        if (!this._toolbar.cancel.button)
            return;

        if (value) {
            this._toolbar.cancel.button.disable();
            this._toolbar.save.button.disable();
            this._toolbar.edit.button.disable();
        }
        this.readonly = true;
    };

    set readonly(value: boolean) {
        if (!this._toolbar.cancel.button)
            return;

        if (value) {
            this._toolbar.cancel.button.disable();
            this._toolbar.save.button.disable();
            this._toolbar.edit.button.enable();
            return;
        }
        this._toolbar.edit.button.disable();
        this._toolbar.cancel.button.enable();
        this._toolbar.save.button.enable();
    }

    get buttonClickEvent() {
        return this._buttonClickEvent;
    }

    private _toolbar = {
        edit: ToolBtn.getEditBtn(`${this.viewName}-tooldar`, "Изменить", () => {
            this.editClick();
        }),
        save: ToolBtn.getSaveBtn(`${this.viewName}-tooldar`, "Сохранить изменения", () => {
            this.saveClick();
        }),
        cancel: ToolBtn.getCancelBtn(`${this.viewName}-tooldar`, "Отказаться от изменений", () => {
            this.cancelClick();
        })
    }

    constructor(private readonly _viewName: string, private readonly _mainTab: Tab.IMainTab) {
        super(); 
    }

    private cancelClick() {
        this._buttonClickEvent.trigger(ButtonEnum.Cancel);
    }

    private editClick() {
        this._buttonClickEvent.trigger(ButtonEnum.Edit);
    }

    private saveClick() {
        this._buttonClickEvent.trigger(ButtonEnum.Save);
    }

    init() {
        return {
            view: "toolbar",
            id: this.toolbarId,
            height: 30,
            elements: [
                {},
                this._toolbar.save.init(),
                this._toolbar.cancel.init(),
                this._toolbar.edit.init()
            ]
        }
    }

}
﻿import Base = require("../../../../../Components/ComponentBase");
import Tab = require("../../../../../MainView/Tabs/MainTabBase");
import Toolbar = require("./InventToolbar");
import List = require("./InventListView");
import ToolButtons = require("../../../../../Components/Buttons/ToolBarButtons");
import Dlg = require("./InventDlg");
import Api = require("../../../../../Api/ApiExporter");
import Common = require("../../../../../Common/CommonExporter");
import Event = require("../../../../../Common/Events");

export interface IInventsView extends Base.IComponent {
    contractId: number;
    selectionChangedEvent: Event.IEvent<number>;
}

export function getInventsView(viewName: string,
    module: Tab.IMainTab): IInventsView {
    return new InventsView(viewName, module);
}

class InventsView extends Base.ComponentBase implements IInventsView {

    private _toolbar: Toolbar.IInventToolbar;
    private _list: List.IInventListView;
    private _filterControl = new StateFilterControl(`${this.viewName}_statefilter`);
    private _selectionChangedEvent: Event.IEvent<number> = new Event.Event<number>();

    private _addNewBtn: ToolButtons.IToolBarButton;
    private _editBtn: ToolButtons.IToolBarButton;

    private _contractId: number = null;

    get viewName() {
        return this._viewName;
    }

    get contractId() {
        return this._contractId;
    }

    set contractId(value: number) {
        this._contractId = value;
        this._filterControl.updateFilter();
        this._list.contractId ? this._addNewBtn.button.enable() : this._addNewBtn.button.disable();
    }

    get selectionChangedEvent(): Event.IEvent<number> {
        return this._selectionChangedEvent;
    }

    constructor(private readonly _viewName: string,
        private readonly _mainTab: Tab.IMainTab) {
        super();
        this._toolbar = Toolbar.getInventToolbar(`${this.viewName}`, this._mainTab);
        this._list = List.getInventsView(`${this.viewName}`, this._mainTab);

        this._addNewBtn = ToolButtons.getNewBtn(`${this.viewName}-operation-toolbar`, "Добавить", (id, e) => {
            this.showDlg(null);
        });
        this._editBtn = ToolButtons.getEditBtn(`${this.viewName}-operation-toolbar`, "Изменить", (id, e) => {
            this.showDlg(this._toolbar.baId);
        });
        this._toolbar.addButtons(this._addNewBtn, this._editBtn);

        this.subscribe(this._list.selectionChangedEvent.subscribe((baId: number) => {
                this._toolbar.baId = baId;

                if (!this._editBtn.button)
                    return;

                (!this._toolbar.baId) ? this._editBtn.button.disable() : this._editBtn.button.enable();
                this._selectionChangedEvent.trigger(baId);
            },
            this));

        this.subscribe(this._filterControl.filterChanged.subscribe((value: any[]) => {
                this._list.setKeys({
                    contractId: this._contractId,
                    states: this._filterControl.getFilterValues() 
                });
            }, this)
        );

        this.subscribe(this._toolbar.statusChangedEvent.subscribe(
            (args) => {
                this._list.updateSelectedItem();
                //const selectedItem = this._list.selectedItem;
                //Api.ApiContractInvents.getItem(selectedItem.baId).then((item => {
                //    this._list.updateSelectedItem(item);
                //}));
            }, this
        ));
    }

    private showDlg(baId: number) {
        webix.message("Show dialog!!!");
        const dlg = Dlg.getInventDlg(`${this._viewName}-dlg-form`, this._mainTab, { baId: baId, contractId: this._contractId});

        dlg.showModalContent(this._mainTab, () => {
            Api.ApiContractInvents.getItem(dlg.baId).then((item: ContractInvents.IContractInventModel) => {
                if (!baId) {
                    this._list.addItem(item);
                } else {
                    this._list.updateSelectedItem();
                    //this._list.updateSelectedItem(item);
                }
                dlg.close();
            });
        });
    }

    filterInit() {
        return {
            view: "multicombo",
            label: "<b>Фильтр состояний</b>",
            labelWidth: 150,
            value: "8",
            tagMode: false, // Показывать все тэги
            button: true,
            tagTemplate: (values) => {
                return (values.length ? values.length + `выбрано условий ${values.length}` : "");
            },
            suggest: {
                selectAll: true,
                body: {
                    data: [
                        { id: 2, title: "Проект сущности" },
                        { id: 4, title: "Архивный" },
                        { id: 8, title: "Активирован" },
                        { id: 255, title: "Удален" }
                    ],          
                    template: webix.template("#title#")
                }
            }//,
            //on: {
            //    onChange: () => {
            //        webix.message("Data was changed");
            //    }
            //}
        }
    }

    init() {
        return {
            minWidth: 440,
            rows: [
                // toolbar 
                this._toolbar.init(),
                this._filterControl.init(),
                // карточки номенклатуры
                this._list.init()
            ],
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        }
    }
}

class StateFilterControl {

    private _selectedValues = [];
    private _suggestLoaded = false;
    private _updateFilter = false;
    private _filterChanged = new Event.Event<any[]>();

    get viewName() {
        return `${this._viewName}`;
    }

    get multicomboId() {
        return `${this.viewName}_multicombo_id`;
    }

    get filterChanged() {
        return this._filterChanged;
    } 

    get multicombo() {
        return $$(this.multicomboId) as webix.ui.multicombo;
    }

    constructor(private readonly _viewName) {

    }

    getFilterValues(): any[] {
        return this.multicombo.getValue().split(",");
    } 

    updateFilter() {
        this._updateFilter = true;
        if (this._suggestLoaded) {
            this._filterChanged.trigger(this._selectedValues, this);
        }
    }

    init() {
        const self = this;
        return {
            view: "multicombo",
            id: this.multicomboId,
            label: "<b>Фильтр состояний</b>",
            labelWidth: 150,
            //value: "8",
            tagMode: false, // Показывать все тэги
            button: true,
            tagTemplate: (values) => {
                return (values.length ? `Выбрано условий ${values.length}` : "");
            },
            suggest: {
                selectAll: true,
                body: {
                    url: {
                        $proxy: true,
                        load: (view, callback, params) => {
                            const entityInfo = Common.entityTypes().getByAliase("ContractInvent");
                            Api.getApiOperations().getEntityStates(entityInfo.entityInfo.baTypeId).then(items => {

                                let selectedValues = "";
                                const dataItems = items.map(item => {
                                    if (!item.isDelState && !item.isFirstState)
                                        selectedValues += !selectedValues ? `${item.stateId}` : `,${item.stateId}`;
                                    return webix.extend(item, { id: item.stateId });
                                });
                                self.multicombo.setValue(selectedValues);
                                (webix.ajax as any).$callback(view, callback, dataItems);
                                self._suggestLoaded = true;
                                if (self._updateFilter) {
                                    self._filterChanged.trigger(self._selectedValues, self);
                                }
                            });
                        }
                    },
                    template: webix.template("#stateName#")
                },
                on: {
                    onShow: () => {
                        //webix.message("Show!!!");
                        self._selectedValues = self.multicombo.getValue().split(",");
                    },
                    onHide: () => {
                        if (self._selectedValues.join() === self.multicombo.getValue())
                            return;

                        this._selectedValues = self.multicombo.getValue().split(",");
                        if (self._updateFilter) {
                            self._filterChanged.trigger(self._selectedValues, self);
                        }
                    }
                }
            }
        }
    }
}
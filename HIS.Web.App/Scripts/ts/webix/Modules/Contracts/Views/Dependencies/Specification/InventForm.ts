﻿import Api = require("../../../../../Api/ApiExporter");
import Common = require("../../../../../Common/CommonExporter");
import Base = require("../../../../../Components/ComponentBase");
import Tab = require("../../../../../MainView/Tabs/MainTabBase");
import CtrlBase = require("../../../../../Components/Markers/MarkerControlBase");
import Helper = require("./InventFormHelper");
import Ctrl = require("../../../../../Components/Markers/MarkerControlFactory");

export interface IInventForm extends Base.IComponent {
    validate(): boolean;
    baId: number;
    contractId: number;
    saveData(): Promise<number>;
    getFormData(): Markers.IEntityUpdateModel;
}

export function getInventForm(viewName: string, module: Tab.IMainTab): IInventForm {
    return new InventForm(viewName, module);
}

class InventForm extends Base.ComponentBase implements IInventForm {

    private _baId: number;
    private _contractId: number;
    private _helper = Helper.getInventFormHelper();
    private _markerControls: { [id: string]: CtrlBase.IMarkerControl; } = {};

    get viewName(): string {
        return `${this._viewName}_view`;
    }

    get baId() {
        return this._baId;
    }

    set baId(value: number) {
        this._baId = value;
        this.loadData(this._baId);
    }

    get contractId() {
        return this._baId;
    }

    set contractId(value: number) {
        this._contractId = value;
    }

    private get formId(): string {
        return `${this.viewName}_form_id`;
    }

    private get form(): webix.ui.form {
        return $$(this.formId) as webix.ui.form;
    }


    constructor(private readonly _viewName: string, private readonly _mainTab: Tab.IMainTab) {
        super(); 
    }

    getFormData(): Markers.IEntityUpdateModel {
        const formModel = this.form.getValues();

        const entityInfo = Common.entityTypes().getByAliase("ContractInvent").entityInfo;

        const model = {
            baId: this._baId,
            baTypeId: entityInfo.baTypeId,
            markerValues: []
        }

        const value = this.getMarkerValue(
            this._helper.markersInfo["Contract"],
            this._contractId.toString(),
            null);

        model.markerValues.push(value);

        this._helper.controlNames
            //.filter(item => { return item.name !== "StartDate" && item.name !== "EndDate" })
            .forEach(item => {
                const value = this._markerControls[item.name].getValue(formModel);
                if (value.value || value.isDeleted)
                    model.markerValues.push(value);
            });

        return model;
    }

    loadData(contractId: number) {
        if (!contractId) {
            this.bindData(null);
            return;
        }
        Api.getApiMarkers().entityMarkerValues({
            baId: contractId,
            markerIds: [],
            names: Helper.getInventFormHelper().controlNames.map(item => { return item.name })
        }).then(result => {
            this.bindData(result);
        });
    }



    validate(): boolean {
        return this.form.validate();
    }

    saveData(): Promise<number> {
        const model = this.getFormData();
        const promise = Api.ApiEntities.update(model);
        promise.then((baId: number) => {
            this._baId = baId;
        });
        return promise;
    }

    private bindData(model: Markers.IEntityMarkersModel) {
        const formModel = {};

        if (model) {
            model.markerValues.forEach(item => {
                const control = this._markerControls[item.MVCAliase];
                if (control) {
                    control.setInitValue(item, formModel);
                    if ("baId" in control)
                        control["baId"] = this._contractId;
                }
            }, this);
        } else {
            this._helper.controlNames
                //.filter(item => { return item.name !== "StartDate" && item.name !== "EndDate" })
                .forEach(item => {
                    this._markerControls[item.name].setInitValue(null, formModel);
                });
        }

        this.form.setValues(formModel);
    }

    private getMarkerValue(info: Markers.IMarkerInfoModel, value: string, displayValue: string): Markers.IMarkerValueModel {
        return {
            displayValue: displayValue,
            itemId: null,
            endDate: null,
            markerId: info.id,
            markerType: info.markerType,
            MVCAliase: info.name,
            note: null,
            isVisible: true,
            isBlocked: false,
            startDate: null,
            value: value,
            isDeleted: false
        }
    }

    private initMarkerControls() {
        this._helper.controlNames
            //.filter(item => { return item.name !== "StartDate" && item.name !== "EndDate" })
            .forEach(item => {
                let extCfg; 
                switch (item.name) {
                    case "Note":
                        extCfg = { labelWidth: item.labelWidth, labelPosition: "top" };
                        break;
                    case "StartDate":
                    case "EndDate":
                        extCfg = { labelWidth: item.labelWidth, width: 400 };
                        break;
                    default:
                        extCfg = { labelWidth: item.labelWidth };
                }

                this._markerControls[item.name] = Ctrl.MarkerControlfactory.getControl(`${this.viewName}-ctrl-${item.name}`,
                    this._helper.markersInfo[item.name],
                    (control) => { return extCfg });
                if (this._markerControls[item.name]["setModule"])
                    this._markerControls[item.name]["setModule"](this._mainTab);
                switch (item.name) {
                    case "Inventory":
                        this._markerControls[item.name]["isDisabledAddButton"] = true;
                        break;
                }
            });
    }

    initControls() {
        return [
            this._markerControls["Inventory"].init(),
            {
                cols: [
                    this._markerControls["StartDate"].init(),
                    this._markerControls["EndDate"].init(), { gravity: 2 }
                ]
            },
            this._markerControls["Note"].init()
        ];
    }

    init() {
        this.initMarkerControls();
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: this.initControls(),
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        }
    }

}
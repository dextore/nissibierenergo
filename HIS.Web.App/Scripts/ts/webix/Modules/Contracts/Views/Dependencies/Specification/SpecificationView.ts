﻿import Base = require("../../../../../Components/ComponentBase");
import Tab = require("../../../../../MainView/Tabs/MainTabBase");
import Invents = require("./InventsView");
import MarkersView = require("./InventMarkersView");
import CM = require("../Markers/ContractMarkerForm");

export interface ISpecificationView extends Base.IComponent {
    contractId: number;
}

export function getSpecificationView(viewName: string,
    module: Tab.IMainTab,
    contractMarkerForm: CM.IContractMarkerForm): ISpecificationView {
    return new SpecificationView(viewName, module, contractMarkerForm);
}

class SpecificationView extends Base.ComponentBase implements ISpecificationView {

    private _invents: Invents.IInventsView;
    private _markers: MarkersView.IInventMarkersView;

    get viewName() {
        return this._viewName;
    }

    get contractId(): number {
        return this._invents.contractId;
    }

    set contractId(value: number) {
        this._invents.contractId = value;
    }

    constructor(private readonly _viewName: string,
        private readonly _mainTab: Tab.IMainTab,
        private readonly _contractMarkerForm: CM.IContractMarkerForm) {
        super();
        this._invents = Invents.getInventsView(`${this.viewName}-invent`, this._mainTab);
        this._markers = MarkersView.getInventMarkersView(`${this.viewName}-markers`, this._mainTab);
        this.subscribe(this._invents.selectionChangedEvent.subscribe((baId: number) => {
            this._markers.baId = baId;
        }, this));

        this.subscribe(this._contractMarkerForm.contractValuesChanged.subscribe((model: Markers.IEntityMarkersModel) => {
            this._markers.contractModel = model;
        }, this));

    }

    init() {

        return {
            cols: [
                // Список номенклатуры
                this._invents.init(),
                { view: "resizer" },
                {
                    view: "tabview",
                    gravity: 3,
                    tabbar: {
                        optionWidth: 250
                    },
                    cells: [
                        {
                            header: "Условия по номенклатуре",
                            body: this._markers.init()
                        }, {
                            header: "Объекты",
                            body: { template: "Объекты" }
                        }
                    ]

                }
            ]
        }
    }

}
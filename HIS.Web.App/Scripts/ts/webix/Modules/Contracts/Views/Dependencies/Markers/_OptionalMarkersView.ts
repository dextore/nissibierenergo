﻿import Api =require("../../../../../Api/ApiExporter");
import Base = require("../../../../../Components/Collections/CollectionBase");
import Common = require("../../../../../Common/CommonExporter");
import Tab = require("../../../../../MainView/Tabs/MainTabBase");
import ToolBtn = require("../../../../../Components/Buttons/ToolBarButtons");

export interface IOptionalMarkersView extends Base.ICollectionBase {
    contractId: number;
}

export function getOptionalMarkersView(viewName: string, module: Tab.IMainTab): IOptionalMarkersView {
    return new OptionalMarkersView(viewName, module);
}

class OptionalMarkersView extends Base.CollectionBase implements IOptionalMarkersView {

    private _contractId: number;


    set contractId(value: number) {
        this._contractId = value;
        this.loadData();
    }

    private _toolbar = {
        edit: ToolBtn.getEditBtn(`${this.viewName}-tooldar`, "Изменить", () => {
            this.editClick();
        }),
        save: ToolBtn.getSaveBtn(`${this.viewName}-tooldar`, "Сохранить изменения", () => {
            this.saveClick();
        }),
        cancel: ToolBtn.getCancelBtn(`${this.viewName}-tooldar`, "Отказаться от изменений", () => {
            this.cancelClick();
        })
    }

    constructor(viewName: string, module: Tab.IMainTab) {
        super(`${viewName}_optional_markers`, module);
    }

    private cancelClick() {
        this.changeState(false);
        this.loadData();
    }

    private editClick() {
        this.changeState(true);
    }

    private saveClick() {
        if (!this.dataTable.validate())
            return;
        this.changeState(false);
        this.saveChanges();
    }

    private saveChanges() {
        console.log(this.dataTable.serialize());
    }

    private changeState(edit: boolean) {

        this.dataTable.define("editable", edit ? true: "");

        edit ? this._toolbar.edit.button.disable() : this._toolbar.edit.button.enable();
        edit ? this._toolbar.cancel.button.enable() : this._toolbar.cancel.button.disable();
        edit ? this._toolbar.save.button.enable() : this._toolbar.save.button.disable();
    }

    disableAll() {
        this._toolbar.edit.button.disable();
        this._toolbar.cancel.button.disable();
        this._toolbar.save.button.disable();
    }

    getData(): Promise<Markers.IMarkerValueModel[]> {
        const promise = webix.promise.defer();

        Api.getApiMarkers().entityMarkerValues({
            baId: this._contractId,
            markerIds: [],
            names: []
        }).then(data => {
            const markersInfo = Common.entityTypes().getByAliase("Contract").entityInfo.markersInfo;

            const markerValues = data.markerValues;
            const result: IMarkerValueModelExt[] = [];

            for (let i = 0; i < markerValues.length; ++i) {
                const markerValue = markerValues[i];
                const markerInfo = markersInfo.filter(itemInfo => {
                    return itemInfo.id === markerValue.markerId && itemInfo.isOptional;
                });

                if (markerInfo.length && markerInfo[0].isPeriodic && markerValue.isVisible) {
                    result.push(webix.extend(markerValue, { label: markerInfo[0].label }));
                }
            }

            result.length > 0 ? this.changeState(false) : this.disableAll();

            (promise as any).resolve(result);
        });

        return promise;
    }

    isParamUndefined(): boolean {
        return !this._contractId;
    }

    private valueColumnTemplate(data: Markers.IMarkerValueModel) {
        switch (data.markerType) {
            case HIS.Models.Layer.Models.Markers.MarkerType.MtBoolean:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtInteger:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtDecimal:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtString:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtMoney:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtDateTime:
                return `<div style="overflow:hidden">${
                    (!data.value) ? "" : data.value}</div>`;
            case HIS.Models.Layer.Models.Markers.MarkerType.MtList:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtEntityRef:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtAddress:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtReference:
                return `<div style="overflow:hidden">${
                    (!data.displayValue) ? "" : data.displayValue}</div>`;
        }
    }

    initToolbarElements(): any[] {
        return [
            {},
            this._toolbar.save.init(),
            this._toolbar.cancel.init(), 
            this._toolbar.edit.init()
        ];
    }

    private onBeforeEditStart(id) {
        const columnConfig = this.dataTable.getColumnConfig(id);
        columnConfig.editor = this.getColumnConfig(id.row);
    }

    getColumnConfig(row: Markers.IMarkerValueModel) {
        switch (row.markerType) {
            case HIS.Models.Layer.Models.Markers.MarkerType.MtBoolean:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtInteger:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtDecimal:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtString:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtMoney:
                return "text"; 
            case HIS.Models.Layer.Models.Markers.MarkerType.MtDateTime:
                return "datepicker"; 
            case HIS.Models.Layer.Models.Markers.MarkerType.MtList:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtEntityRef:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtAddress:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtReference:
                return "select"; 
        }
    }

    initColumns(): any[] {
        return [
            {
                id: "label",
                header: "Наименование",
                fillspace: true 
            },
            {
                id: "startDate",
                header: "Дата с...",
                width: 120,
                editor: "dateExtended",
                format: webix.Date.dateToStr("%d.%m.%Y", false)
            },
            {
                id: "endDate",
                header: "Дата по...",
                width: 120,
                format: webix.Date.dateToStr("%d.%m.%Y", false)
            },
            {
                id: "value",
                header: "Значение",
                template: (data: Markers.IMarkerValueModel) => { return this.valueColumnTemplate(data) },
                fillspace: true
            },
            {
                id: `note`,
                header: "Примечание",
                sort: "string",
                editor: "popup",
                fillspace: true
            }
        ]; 
    }

    protected internalInitDataTable() {
        return {
            on: {
                onBeforeEditStart: (id) => {
                    webix.message(id.row);
                }
            }
        };
    }
}

interface IMarkerValueModelExt extends Markers.IMarkerValueModel {
    label: string
}

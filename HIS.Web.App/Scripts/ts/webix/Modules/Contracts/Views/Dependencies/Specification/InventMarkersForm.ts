﻿import Api = require("../../../../../Api/ApiExporter");
import Base = require("../../../../../Components/ComponentBase");
import Tab = require("../../../../../MainView/Tabs/MainTabBase");
import CtrlBase = require("../../../../../Components/Markers/MarkerControlBase");
import Ctrl = require("../../../../../Components/Markers/MarkerControlFactory");
import Common = require("../../../../../Common/CommonExporter");
import Event = require("../../../../../Common/Events");
import MI = require("./ContractMarkerInfoDlg");

export enum FormStateEnum {
    ReadOnly,
    Edit,
    Disabled
}

export interface IInventMarkersForm extends Base.IComponent {
    baId: number;
    stateChangeEvent: Event.IEvent<FormStateEnum>;
    edit();
    save();
    cancel();
    contractModel: Markers.IEntityMarkersModel;
}

export function getInventMarkersForm(viewName: string, module: Tab.IMainTab): IInventMarkersForm {
    return new InventMarkersForm(viewName, module);
}

class InventMarkersForm extends Base.ComponentBase implements IInventMarkersForm {

    private _baId: number;
    private _contractId: number;
    private _markersInfo = Common.entityTypes().getByAliase("ContractInvent").entityInfo.markersInfo;
    private _contractMarkersInfo = Common.entityTypes().getByAliase("Contract").entityInfo.markersInfo;
    private _stateChangeEvent: Event.IEvent<FormStateEnum> = new Event.Event<FormStateEnum>();
    private _readonly: boolean = true;
    private _contractModel: Markers.IEntityMarkersModel;

    private _markerControls: { [id: string]: CtrlBase.IMarkerControl; } = {};

    get baId(): number {
        return this._baId;
    }

    set baId(value: number) {
        this._baId = value;
        this.loadData(this._baId);
    }

    get viewName() {
        return `${this._viewName}_markers`;
    }

    private get formViewId(): string {
        return `${this.viewName}_view_id`;
    }

    get formId() {
        return `${this._viewName}_form_id`;
    }

    get form() {
        return $$(this.formId) as webix.ui.form;
    }

    get stateChangeEvent(): Event.IEvent<FormStateEnum> {
        return this._stateChangeEvent;
    }

    private get stubViewId(): string {
        return `${this.viewName}_stub_id`;
    }

    get contractModel(): Markers.IEntityMarkersModel {
        return this._contractModel;
    };

    set contractModel(model: Markers.IEntityMarkersModel) {
        this._contractModel = model;
        this.bindContractInfo();
    };

    constructor(private readonly _viewName: string, private readonly _mainTab: Tab.IMainTab) {
        super(); 
    }

    private bindData(model: Markers.IMarkerValueModel[]) {
        if (!this.form)
            return;

        const formModel = {};

        const markerControls = this._markerControls;
        for (let prop in markerControls) {
            if (!markerControls.hasOwnProperty(prop)) continue;
            const markerControl = markerControls[prop];

            let filtered = (!model) ? [] : model.filter(item => item.markerId === markerControl.markerInfo.id);

            (!filtered.length) ? markerControl.setInitValue(null, formModel) : markerControl.setInitValue(filtered[0], formModel);
            if ("baId" in markerControl)
                markerControl["baId"] = this._baId;

        }

        this.form.setValues(formModel);

        this.bindContractInfo();
    }

    private bindContractInfo() {

        const classRedefained = " contract_marker_redefained";

        if (!this._contractModel)
            return; 

        this._contractModel.markerValues.forEach((item: Markers.IMarkerValueModel) => {

            const markerControls = this._markerControls;
            //const fromModel = this.form.getValues();

            for (let prop in markerControls) {
                if (!markerControls.hasOwnProperty(prop)) continue;
                const control = markerControls[prop] as CtrlBase.IMarkerControl;
                if (item.markerId === control.markerInfo.id) {
                    //const markerValue = control.getValue(fromModel);
                    let classNames = control.control.$view.className;
                    if (item.value) {
                        if (classNames.indexOf(classRedefained) < 0)
                            classNames += " contract_marker_redefained";
                    } else {
                        if (classNames.indexOf(classRedefained) >= 0)
                            classNames = classNames.replace(" contract_marker_redefained", "");
                    }
                    control.control.$view.className = classNames;
                }
            }

            //this._markerControls.filter((ctrl: CtrlBase.IMarkerControl) => {
            //});
        });

        //let cFiltered = this._contractMarkersInfo.filter((item: Markers.IMarkerInfoModel) => {
        //    return item.id === markerControl.markerInfo.id;
        //});

        //if (cFiltered.length) {
        //    markerControl.control.define("css", "contract_marker_redefained");
        //    (markerControl.control as any).refresh();
        //}

    }

    private loadData(baId: number) {
        this.setReadOnly(true);
        this._stateChangeEvent.trigger(FormStateEnum.Disabled);
        if (!baId) {
            this.bindData(null);
            return;
        }
        Api.getApiMarkers()
            .entityMarkerValues({
                baId: this.baId,
                markerIds: [],
                names: []
            }).then(data => {

                const markerValues = data.markerValues;
                const result: Markers.IMarkerValueModel[] = [];

                for (let i = 0; i < markerValues.length; ++i) {
                    const markerValue = markerValues[i];
                    const markerInfo = this._markersInfo.filter(itemInfo => {
                        return itemInfo.id === markerValue.markerId;
                    });

                    if (markerInfo.length && markerValue.isVisible) {
                        result.push(markerValue);
                    }

                    if (markerInfo.length && markerInfo[0].name === "Contract") {
                        this._contractId = Number(markerValue.value);
                    }
                }
                this.bindData(result);
                this._readonly = true;
                this._stateChangeEvent.trigger(FormStateEnum.ReadOnly);
            });
    }

    getFormData(): Markers.IEntityUpdateModel {
        const formModel = this.form.getValues();

        const model = {
            baId: this._baId,
            baTypeId: Common.entityTypes().getByAliase("ContractInvent").entityInfo.baTypeId,
            markerValues: []
        }

        const markerControls = this._markerControls;
        for (let prop in markerControls) {
            if (!markerControls.hasOwnProperty(prop)) continue;
            const markerControl = markerControls[prop];
            const value = markerControl.getValue(formModel);
            if (value.value || value.isDeleted) {
                model.markerValues.push(value);
            }
        }

        return model;
    }

    private setReadOnly(readOnly: boolean) {
        if (!this.form)
            return;

        this._readonly = readOnly;

        const markerControls = this._markerControls;
        for (let key in markerControls) {
            if (markerControls.hasOwnProperty(key)) {
                this._markerControls[key].readOnly = readOnly;
            }
        }

        readOnly
            ? this._stateChangeEvent.trigger(FormStateEnum.ReadOnly)
            : this._stateChangeEvent.trigger(FormStateEnum.Edit);
    }

    edit() {
        if (!this._readonly)
            return;

        this.setReadOnly(false);
    }

    save() {
        if (!this.form.validate())
            return;

        const data = this.getFormData();

        Api.ApiEntities.update(data)
            .then().then(result => {
                this.loadData(this._baId);
            }).catch(() => {
                return;
            });;
    }

    cancel() {
        if (!this._baId)
            return; 

        if (this._readonly)
            return;

        this.loadData(this._baId);
    }


    private initMarkerControls() {

        this._markersInfo
            .filter(item => { return !item.isImplemented })
            .forEach(item => {
                const markerControel = Ctrl.MarkerControlfactory.getControl(
                    `${this.viewName}-ctrl-${item.name}`,
                    item,
                    (control) => { return { labelWidth: 250 } });

                this._markerControls[item.name] = markerControel;
                if ("setModule" in markerControel)
                    markerControel["setModule"](this._mainTab);
            }, this);
    }

    private markerControlInit(markerControl: CtrlBase.IMarkerControl) {
        const filtered = this._contractMarkersInfo.filter((item: Markers.IMarkerInfoModel) => {
            return item.id === markerControl.markerInfo.id;
        });

        if (!filtered.length)
            return markerControl.init();

        return {
            cols: [
                markerControl.init(),
                {
                    view: "button",
                    type: "iconButton",
                    icon: "info-circle",
                    width: 28,
                    align: "left",
                    tooltip: "Переопределние маркера",
                    on: {
                        onItemClick: (id, e) => {
                            //webix.message(`Переопределние маркера: ${markerControl.markerInfo.name}`);
                            const dlg = MI.getContractMarkerInfoDlg(this.viewName,
                                this._mainTab,
                                {
                                    markerId: markerControl.markerInfo.id,
                                    contractId: this._contractId
                                });
                            dlg.showModalContent(this._mainTab);
                        }
                    }
                }
            ]
        }
    }

    initControls() {
        const controls: any[] = [];

        const markerControls = this._markerControls;
        for (let prop in markerControls) {
            if (!markerControls.hasOwnProperty(prop)) continue;
            const control = this.markerControlInit(markerControls[prop]);
            controls.push(control);
        }
        return controls;
    }

    init() {
        this.initMarkerControls();
        return {
            rows: [{
                id: this.formViewId,
                rows: [
                    //this.initToolbar(),
                    {
                        view: "form",
                        id: this.formId,
                        autoheight: false,
                        scroll: "y",
                        elements: this.initControls(),
                        on: {
                            onDestruct: () => {
                                this.destroy();
                            }
                        }
                    },
                    {}
                ]
            }, {
                id: this.stubViewId,
                hidden: true
            }]
        }
    }
}
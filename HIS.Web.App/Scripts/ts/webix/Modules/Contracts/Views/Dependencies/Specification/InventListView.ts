﻿import Base = require("../../../../../Components/ComponentBase");
import Tab = require("../../../../../MainView/Tabs/MainTabBase");
import Api = require("../../../../../Api/ApiExporter");
import Event = require("../../../../../Common/Events");
import Contract = require("../../ContractView");

export interface IInventListKeys {
    contractId: number;
    states: any[];
}

export interface IInventListView extends Base.IComponent {
    contractId: number;
    setKeys(keys: IInventListKeys);
    selectionChangedEvent: Event.IEvent<number>;
    updateSelectedItem();
    addItem(item: ContractInvents.IContractInventModel);
    selectedItem: ContractInvents.IContractInventModel;
}

export function getInventsView(viewName: string,
    module: Tab.IMainTab): IInventListView {
    return new InventListView(viewName, module);
}

class InventListView extends Base.ComponentBase implements IInventListView {

    private _contractId: number = null;
    private _states: any[];
    private _selectionChangedEvent: Event.IEvent<number> = new Event.Event<number>();

    get contractId(): number {
        return this._contractId;
    }

    get selectionChangedEvent(): Event.IEvent<number> {
        return this._selectionChangedEvent;
    }

    //set contractId(value: number) {
    //    this._contractId = value;
    //    this.dataView.load(this.dataView.config.url);
    //}

    get viewName() {
        return `${this._viewName}_invents_list`;
    }

    get dataViewId() {
        return `${this.viewName}_dataview_id`;
    } 

    get dataView(): webix.ui.dataview {
        return $$(this.dataViewId) as webix.ui.dataview;
    } 

    get selectedItem(): ContractInvents.IContractInventModel {
        const selected = this.dataView.getSelectedItem(true);
        if (!selected || selected.length === 0)
            return null;

        return selected[0];
    } 

    constructor(private readonly _viewName: string,
        private readonly _mainTab: Tab.IMainTab) {
        super(); 
    }

    setKeys(keys: IInventListKeys) {
        this._contractId = keys.contractId;
        this._states = keys.states;
        this.dataView.load(this.dataView.config.url);
        //webix.message((keys.states as any).join());
    }

    updateSelectedItem() {
        const selected = this.dataView.getSelectedId(true);
        if (!selected || selected.length === 0)
            return;

        const selectedItem = (this.dataView.getSelectedItem(true) as ContractInvents.IContractInventModel[])[0];

        Api.ApiContractInvents.getItem(selectedItem.baId).then((item => {
            this.dataView.updateItem(selected[0], item);
        }));
    }

    addItem(item: ContractInvents.IContractInventModel) {
        const idx = this.dataView.add(item);
        this.dataView.select(idx.toString(), false);
    }

    init() {
        const dateFormat = webix.Date.dateToStr("%d.%m.%Y", false);
        return {
            view: "dataview",
            id: this.dataViewId,
            select: true,
            xCount: 1,
            //yCount: 0,
            //width: 350,
            //autoheight: true,
            type: {
                width: "auto",
                height: 120
            },
            template: (item: ContractInvents.IContractInventModel) => {
                return `<div class='webix_strong'><b>${item.orderNum}</b>&nbsp;&nbsp; ${item.inventName}</div>
                        <div><b>c:</b> ${dateFormat(item.startDate)}</div>
                        <div><b>по:</b> ${dateFormat(item.endDate)}</div>
                        <div><b>состояние:</b> ${item.stateName}</div>
                        <div><b>создал:</b> ${item.userId}</div>`;
            },
            url: {
                $proxy: true,
                load: (view, callback, params) => {

                    if (!(this.dataView as any).showProgress) {
                        webix.extend(this.dataView, webix.ProgressBar);
                    }

                    this.dataView.clearAll();
                    if (!this._contractId) {
                        this._selectionChangedEvent.trigger(null, this);
                        return;
                    }

                    (this.dataView as any).showProgress();
                    Api.ApiContractInvents.get({
                        contractId: this.contractId,
                        states: this._states
                    }).then(items => {
                        (webix.ajax as any).$callback(view, callback, items);
                        (this.dataView as any).hideProgress();
                        if (items.length) {
                            this.dataView.select(this.dataView.getFirstId().toString(), true);
                            return;
                        }
                        this._selectionChangedEvent.trigger(null, this);
                    }).catch(error => {
                        (this.dataView as any).hideProgress();
                    });
                }

            },
            on: {
                onSelectChange: () => {
                    const selected = this.dataView.getSelectedItem(true) as ContractInvents.IContractInventModel[];
                    const baId = !selected || selected.length === 0 ? null : selected[0].baId;
                    this._selectionChangedEvent.trigger(baId, this);
                },
                onDestruct: () => {
                    this.destroy();
                }
            }
        }
    }
}

﻿import Base = require("../../../../../Common/DialogBase");
import Tab = require("../../../../../MainView/Tabs/MainTabBase");
import Btn = require("../../../../../Components/Buttons/ButtonsExporter");
import Form = require("./ContractMarkerInfoForm");

export interface IMarkerKeys {
    markerId: number;
    contractId: number;
}

interface IContractMarkerInfoDlg extends Base.IDialog {
}

export function getContractMarkerInfoDlg(viewName: string, module: Tab.IMainTab, keys: IMarkerKeys): IContractMarkerInfoDlg {
    return new ContractMarkerInfoDlg(viewName, module, keys);
}

class ContractMarkerInfoDlg extends Base.DialogBase implements IContractMarkerInfoDlg {

    private _form: Form.IContractMarkerInfoForm;
    private _okBtn = Btn.getOkButton(this.viewName, () => { this.okClose(); });

    constructor(viewName: string, private readonly _module: Tab.IMainTab, private readonly _keys: IMarkerKeys) {
        super(viewName);
        this._form = Form.getContractMarkerInfoForm(`${this.viewName}-form`, _module);
    }

    bindModel(): void {
        this._form.loadData(
            this._keys.markerId,
            this._keys.contractId);
    }

    protected headerLabel(): string {
        return "Значение из договора";
    }

    private fotterToolBar() {
        return {
            height: 40,
            cols: [
                {},
                this._okBtn.init()
            ]
        }
    }

    protected contentConfig() {
        return {
            rows: [
                this._form.init(),
                this.fotterToolBar()
            ]
        }
    }



}
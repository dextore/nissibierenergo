﻿import Base = require("../../../../../Common/DialogBase");
import Btn = require("../../../../../Components/Buttons/ButtonsExporter");
import Form = require("./InventForm");
import Tab = require("../../../../../MainView/Tabs/MainTabBase");

export interface IInventKeys {
    baId: number;
    contractId: number;
}

export interface IInventDlg extends Base.IDialog {
    baId: number;
    getFormData(): Markers.IEntityUpdateModel;
}

export function getInventDlg(viewName: string, module: Tab.IMainTab, keys: IInventKeys): IInventDlg {
    return new InventDlg(viewName, module, keys);
}

class InventDlg extends Base.DialogBase implements IInventDlg {


    private _form: Form.IInventForm;
    private _okBtn = Btn.getOkButton(this.viewName, () => { this.okClose(); });
    private _cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });

    get baId(): number {
        return this._form.baId;
    }

    constructor(viewName: string, private readonly _module: Tab.IMainTab, private readonly _keys: IInventKeys) {
        super(viewName);
        this._form = Form.getInventForm(`${this.viewName}-form`, _module);
    } 

    bindModel(): void {
        this._form.contractId = this._keys.contractId;
        this._form.baId = this._keys.baId;
    }

    headerLabel(): string {
        return "Позиция спецификации";; 
    }

    getFormData(): Markers.IEntityUpdateModel {
        return this._form.getFormData();
    }

    okClose(): void {
        if (!this._form.validate())
            return;

        this._form.saveData().then((baId: number) => {
            if (!this._keys.baId)
                this._keys.baId = baId;
            super.okClose();
        });
    }

    private fotterToolBar() {
        return {
            height: 40,
            cols: [
                {},
                this._okBtn.init(),
                this._cancelBtn.init()
            ]
        }
    }

    protected windowConfig() {
        return {
            autoheight: true,
            width: 1000,
        };
    }

    contentConfig() {
        return {
            rows: [
                this._form.init(),
                this.fotterToolBar()
            ]
        }
    }
}

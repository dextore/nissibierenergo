﻿import Base = require("../../../../../Components/ComponentBase");
import Operation = require("../../../../../Components/Controls/EntityOperationStateControl");
import Tab = require("../../../../../MainView/Tabs/MainTabBase");
import ToolButtons = require("../../../../../Components/Buttons/ToolBarButtons");
import Event = require("../../../../../Common/Events");

export interface IInventToolbar extends Base.IComponent {
    baId: number;
    addButtons(...args: ToolButtons.IToolBarButton[]): void;
    statusChangedEvent: Event.IEvent<Operation.StateChangeAction>;
}

export function getInventToolbar(viewName: string, module: Tab.IMainTab): IInventToolbar {
    return new InventToolbar(viewName, module);
}

class InventToolbar extends Base.ComponentBase implements IInventToolbar {

    private _operationState = Operation.getEntityOperationStateControl(`${this.viewName}-operation-state`,
        (contractInventId: number) => {
            const promise = webix.promise.defer();

            //Api.ApiLegalPersons.getLegalPerson(personalId, []).then(result => {
            //    (promise as any).resolve(`${result.name} (${result.code})`);
            //}).catch(() => { (promise as any).reject() });

            this._operationState.hideHeade();
            (promise as any).resolve(`Номер договора и еще чтото!!!`);

            return promise;
        },
        () => {
            return true;
        });

    get viewName(): string {
        return `${this._viewName}-toolbar`;
    }

    get baId(): number {
        return this._operationState.baId;
    }

    set baId(value: number) {
        this._operationState.baId = value;
    }

    get statusChangedEvent(): Event.IEvent<Operation.StateChangeAction> {
        return this._operationState.statusChangedEvent;
    }


    constructor(private readonly _viewName: string, private readonly _mainTab: Tab.IMainTab) {
        super(); 
    }

    addButtons(...args: ToolButtons.IToolBarButton[]): void {
        this._operationState.addButtons(...args);
    }

    init() {
        return this._operationState.init();
    }
}

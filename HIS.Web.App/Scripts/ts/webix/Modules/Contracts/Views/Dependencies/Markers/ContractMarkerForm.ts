﻿import Api = require("../../../../../Api/ApiExporter");
import Base = require("../../../../../Components/ComponentBase");
import CtrlBase = require("../../../../../Components/Markers/MarkerControlBase");
import Ctrl = require("../../../../../Components/Markers/MarkerControlFactory");
import Common = require("../../../../../Common/CommonExporter");
import Tab = require("../../../../../MainView/Tabs/MainTabBase");
import ToolBtn = require("../../../../../Components/Buttons/ToolBarButtons");
import ERMC = require("../../../../../Components/Markers/EntityRefMarkerControl");
import Dlg = require("../../../Dlg/RecipientAccountSelectDlg");
import Event = require("../../../../../Common/Events");


export interface IContractMarkerForm extends Base.IComponent {
    contractId: number;
    contractValuesChanged: Event.IEvent<Markers.IEntityMarkersModel>;
}

export function getContractMarkerForm(viewName: string, module: Tab.IMainTab): IContractMarkerForm {
    return new ContractMarkerForm(viewName, module);
}

class ContractMarkerForm extends Base.ComponentBase implements IContractMarkerForm {

    private _contractId: number;
    private _contractValuesChanged: Event.IEvent<Markers.IEntityMarkersModel> = new Event.Event<Markers.IEntityMarkersModel>();

    get viewName(): string {
        return `${this._viewName}_view`;
    }

    set contractId(value: number) {
        this._contractId = value;
        this.loadData(this._contractId);
    }

    private get formId(): string {
        return `${this.viewName}_form_id`;
    }

    get contractValuesChanged() {
        return this._contractValuesChanged;
    }

    private get toolbarId(): string {
        return `${this.viewName}_toolbar_id`;
    }

    private get formViewId(): string {
        return `${this.viewName}form_view`;
    }

    private get stubViewId(): string {
        return `${this.viewName}_stub_id`;
    }

    private get form(): webix.ui.form {
        return $$(this.formId) as webix.ui.form;
    }

    private _markerControls: { [id: string]: CtrlBase.IMarkerControl; } = {};

    private _toolbar = {
        edit: ToolBtn.getEditBtn(`${this.viewName}-tooldar`, "Изменить", () => {
            this.editClick();
        }),
        save: ToolBtn.getSaveBtn(`${this.viewName}-tooldar`, "Сохранить изменения", () => {
            this.saveClick();
        }),
        cancel: ToolBtn.getCancelBtn(`${this.viewName}-tooldar`, "Отказаться от изменений", () => {
            this.cancelClick();
        })
    }

    constructor(private readonly _viewName: string, private readonly _module: Tab.IMainTab) {
        super();
    }

    private cancelClick() {
        this.changeState(false);
        this.loadData(this._contractId);
    }

    private editClick() {
        this.changeState(true);
    }

    private saveClick() {
        if (!this.form.validate())
            return;
        this.changeState(false);
        this.saveChanges();
    }

    private saveChanges() {
        this.saveData().then(result => {
            this.loadData(this._contractId);
        }).catch(() => {
            return;
        });
    }

    private changeState(edit: boolean) {
        edit ? this._toolbar.edit.button.disable() : this._toolbar.edit.button.enable();
        edit ? this._toolbar.cancel.button.enable() : this._toolbar.cancel.button.disable();
        edit ? this._toolbar.save.button.enable() : this._toolbar.save.button.disable();

        const markerControls = this._markerControls;
        for (let key in markerControls) {
            if (markerControls.hasOwnProperty(key)) {
                this._markerControls[key].readOnly = !edit;
            }
        }
        ($$(this.formId) as webix.ui.form).clearValidation();
    }

    private disableAll() {
        this._toolbar.edit.button.disable();
        this._toolbar.cancel.button.disable();
        this._toolbar.save.button.disable();
    }

    loadData(contractId: number) {

        this.disableAll();
        if (!contractId) {
            this.bindData(null);
            return;
        }
        Api.getApiMarkers().entityMarkerValues({
            baId: contractId,
            markerIds: [],
            names: [] //TODO что делать с управляющим маркером опционных??? //Common.entityTypes().getByAliase("Contract").entityInfo.markersInfo 
                //.filter(item => { return item.isOptional })
                //.map(item => item.name)
        }).then(data => {
            const markersInfo = Common.entityTypes().getByAliase("Contract").entityInfo.markersInfo;

            const markerValues = data.markerValues;
            const result: Markers.IMarkerValueModel[] = [];

            for (let i = 0; i < markerValues.length; ++i) {
                const markerValue = markerValues[i];
                const markerInfo = markersInfo.filter(itemInfo => {
                    return itemInfo.id === markerValue.markerId && markerValue.isVisible && !itemInfo.isImplemented;
                });

                if (markerInfo.length && markerValue.isVisible) {
                    result.push(markerValue);
                }
            }
            this.bindData(result);
            this.changeState(false);
            this._contractValuesChanged.trigger(data);
        });
    }

    private bindData(model: Markers.IMarkerValueModel[]) {
        const formModel = {};

        const markerControls = this._markerControls;
        for (let prop in markerControls) {
            if (!markerControls.hasOwnProperty(prop)) continue;
            const markerControl = markerControls[prop];

            let filtered = (!model) ? [] : model.filter(item => item.markerId === markerControl.markerInfo.id);
            if (filtered.length) {
                markerControl.setInitValue(filtered[0], formModel);
                markerControl.visible = true;
            } else {
                markerControl.setInitValue(null, formModel);
                markerControl.visible = false;
            }
            if ("baId" in markerControl)
                markerControl["baId"] = this._contractId;
        }

        if (!model || !model.length) {
            $$(this.formViewId).hide();
            $$(this.stubViewId).show();
        } else {
            $$(this.formViewId).show();
            this.form.setValues(formModel);
            $$(this.stubViewId).hide();
        }
    }

    saveData(): Promise<number> {
        const data = this.getFormData();
        return Api.ApiEntities.update(data);
    }

    getFormData(): Markers.IEntityUpdateModel {
        const formModel = this.form.getValues();

        const model = {
            baId: this._contractId,
            baTypeId: Common.entityTypes().getByAliase("Contract").entityInfo.baTypeId,
            markerValues: []
        }

        const markerControls = this._markerControls;
        for (let prop in markerControls) {
            if (!markerControls.hasOwnProperty(prop)) continue;
            const markerControl = markerControls[prop];
            const value = markerControl.getValue(formModel);
            if (value.value || value.isDeleted) {
                model.markerValues.push(value);
            }
        }

        return model;
    }

    private initMarkerControls() {

        Common.entityTypes().getByAliase("Contract").entityInfo.markersInfo
            .filter(item => { return !item.isImplemented && item.name !== "UKGroup" })
            .forEach(item => {
                const markerControel = Ctrl.MarkerControlfactory.getControl(
                    `${this.viewName}-ctrl-${item.name}`,
                    item,
                    (control) => { return { labelWidth: 250 } });

                this._markerControls[item.name] = markerControel;
                if ("setModule" in markerControel)
                    markerControel["setModule"](this._module);
            });

        this.subscribe((this._markerControls["Recipient"] as ERMC.IEntityRefMarkerControllMaster)
            .selectedChangeEvent.subscribe((baId: number) => {
                (this._markerControls["RecipientAccount"] as ERMC.IEntityRefMarkerControllSlave)
                    .masterBaId = baId;
            }, this));

        (this._markerControls["RecipientAccount"] as ERMC.IEntityRefMarkerControllSlave).selectDlg = () => {
            return Dlg.getRecipientAccountSelectDlg(`${this.viewName}_select_dialog`);
        }
    }


    // Опционные 
    
    // Обычные

    init() {
        this.initMarkerControls();
        return {
            rows: [{
                    id: this.formViewId,
                    rows: [
                        this.initToolbar(),
                        {
                            view: "form",
                            id: this.formId,
                            autoheight: true,
                            elements: this.initControls(),
                            on: {
                                onDestruct: () => {
                                    this.destroy();
                                }
                            }
                        },
                        {} 
                    ]
                }, {
                    id: this.stubViewId,
                    hidden: true
            }]
        }
    }

    initControls() {
        const controls: any[] = [];

        let markerControls: CtrlBase.IMarkerControl[] = [];

        for (let prop in this._markerControls) {
            if (!this._markerControls.hasOwnProperty(prop)) continue;
            markerControls.push(this._markerControls[prop]);
        }

        markerControls = markerControls.sort((a, b) => {
            if (!a.markerInfo.isOptional && b.markerInfo.isOptional)
                return -1;
            if (a.markerInfo.isOptional && !b.markerInfo.isOptional)
                return 1;
            return 0;
        });

        markerControls.forEach(item => {
            controls.push({ cols: [item.init(), {gravity: 0.01}]});
        });
        return controls;
    }

    private initToolbar() {
        return {
            view: "toolbar",
            id: this.toolbarId,
            height: 30,
            elements: [
                {},
                this._toolbar.save.init(),
                this._toolbar.cancel.init(),
                this._toolbar.edit.init()
            ]
        }
    };

}
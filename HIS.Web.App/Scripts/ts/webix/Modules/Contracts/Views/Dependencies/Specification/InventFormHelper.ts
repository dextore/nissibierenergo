﻿import Common = require("../../../../../Common/CommonExporter");

export interface IInventFormHelper {
    controlNames: Common.IControlInfo[];
    markersInfo: { [id: string]: Markers.IMarkerInfoModel; }
}

export function getInventFormHelper(): IInventFormHelper {
    return new InventFormHelper();
}

class InventFormHelper implements IInventFormHelper {

    constructor() {
        if (!markersInfo) {
            markersInfo = {};
            const mInfo = Common.entityTypes().getByAliase("ContractInvent").entityInfo.markersInfo;
            mInfo.forEach(item => {
                markersInfo[item.name] = item;
            });
        }
    }

    get controlNames(): Common.IControlInfo[] {
        return controlNames;
    };

    get markersInfo() {
        return markersInfo;
    };
}

let markersInfo: { [index: string]: Markers.IMarkerInfoModel; };
const controlNames: Common.IControlInfo[] = [{ name: "Inventory", labelWidth: 200 },
                                             { name: "StartDate", labelWidth: 200 },
                                             { name: "EndDate", labelWidth: 200 },
                                             { name: "Note", labelWidth: 100 }];  

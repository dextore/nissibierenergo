﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../../Components/ComponentBase");
import Helper = require("./ContractFormHelper");
import CtrlBase = require("../../../Components/Markers/MarkerControlBase");
import Ctrl = require("../../../Components/Markers/MarkerControlFactory");
import Common = require("../../../Common/CommonExporter");
import Tab = require("../../../MainView/Tabs/MainTabBase");

export interface IContractForm extends Base.IComponent {
    validate(): boolean;
    contractId: number;
    saveData(): Promise<number>;
    getFormData(): Markers.IEntityUpdateModel;
}

export function getContractForm(viewName: string, module: Tab.IMainTab): IContractForm {
    return new ContractForm(viewName, module);
}

class ContractForm extends Base.ComponentBase implements IContractForm {

    private _contractId: number;

    private _helper = Helper.getContractFormHelper();
    private _markerControls: { [id: string]: CtrlBase.IMarkerControl; } = {};


    get viewName(): string {
        return `${this._viewName}_view`;
    }

    get contractId() {
        return this._contractId;
    }

    set contractId(value: number) {
        this._contractId = value;
        this.loadData(this._contractId);
    }

    private get formId(): string {
        return `${this.viewName}_form_id`;
    }

    private get optionsViewId(): string {
        return `${this.viewName}_options_view_id`;
    } 

    private get form(): webix.ui.form {
        return $$(this.formId) as webix.ui.form;
    }

    constructor(private readonly _viewName: string, private readonly _module: Tab.IMainTab) {
        super();
    }

    validate(): boolean {
        return this.form.validate(); 
    }

    loadData(contractId: number) {
        if (!contractId) {
            this.bindData(null);
            return;
        }
        Api.getApiMarkers().entityMarkerValues({
            baId: contractId,
            markerIds: [],
            names: Helper.getContractFormHelper().controlNames.map(item => { return item.name })
        }).then(result => {
            this.bindData(result);
        });
    }

    getFormData(): Markers.IEntityUpdateModel {
        const formModel = this.form.getValues();

        const model = {
            baId: this._contractId,
            baTypeId: Common.entityTypes().getByAliase("Contract").entityInfo.baTypeId,
            markerValues: []
        }

        this._helper.controlNames
            .filter(item => { return item.name !== "StartDate" && item.name !== "EndDate" })
            .forEach(item => {
            const value = this._markerControls[item.name].getValue(formModel);
            if (value.value || value.isDeleted)
                model.markerValues.push(value);
        });

        return model;
    }

    saveData(): Promise<number> {
        const model = this.getFormData();
        const promise = Api.ApiEntities.update(model);
        promise.then((contractId: number) => {
            if (!this._contractId)
                this._contractId = contractId;
        });
        return promise;
    }

    private bindData(model: Markers.IEntityMarkersModel) {
        const formModel = {};

        if (model) {
            model.markerValues.forEach(item => {
                const control = this._markerControls[item.MVCAliase];
                if (control) {
                    control.setInitValue(item, formModel);
                    if ("baId" in control)
                        control["baId"] = this._contractId;
                }
            },this);
        } else {
            this._helper.controlNames
                .filter(item => { return item.name !== "StartDate" && item.name !== "EndDate" })
                .forEach(item => {
                    this._markerControls[item.name].setInitValue(null, formModel);
            });
        }

        const companyArea = "CompanyArea";
        const curArea = this._module.mainTabView.getSelectedCompany();
        const value = getMarkerValue(
            this._helper.markersInfo[companyArea],
            curArea.id.toString(),
            curArea.name);
        this._markerControls[companyArea].setInitValue(value, formModel);

        this.form.setValues(formModel);

        function getMarkerValue(info: Markers.IMarkerInfoModel, value: string, displayValue: string): Markers.IMarkerValueModel {
            return {
                displayValue: displayValue,
                itemId: null,
                endDate: null,
                markerId: info.id,
                markerType: info.markerType,
                MVCAliase: info.name,
                note: null,
                isVisible: true,
                isBlocked: false,
                startDate: null,
                value: value,
                isDeleted: false
            }
        }
    }

    private initMarkerControls() {
        this._helper.controlNames
            .filter(item => { return item.name !== "StartDate" && item.name !== "EndDate" })
            .forEach(item => {
            let extCfg = (item.name === "Note") ? { labelWidth: item.labelWidth, labelPosition: "top" } : { labelWidth: item.labelWidth };
            this._markerControls[item.name] = Ctrl.MarkerControlfactory.getControl(`${this.viewName}-ctrl-${item.name}`,
                this._helper.markersInfo[item.name],
                (control) => { return extCfg });
            if (this._markerControls[item.name]["setModule"])
                this._markerControls[item.name]["setModule"](this._module);
            switch (item.name) {
                case "CompanyArea":
                    this._markerControls[item.name]["isDisabledAddButton"] = true;
                    break;
            }
        });
    }

    init() {
        this.initMarkerControls();
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: this.initControls(),
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        }
    }

    initControls() {
        return [
            {
                cols: [this._markerControls["ContractType"].init(), this._markerControls["UKGroup"].init()]
            }, {
                rows: [
                    {
                        cols: [this._markerControls["Name"].init(),
                               this._markerControls["DocDate"].init(), { gravity: 2 }
                               //this._markerControls["StartDate"].init(),
                               //this._markerControls["EndDate"].init()
                              ]
                    },
                    this._markerControls["CompanyArea"].init(),
                    this._markerControls["LegalSubject"].init(),
                    this._markerControls["Note"].init()
                ]
            }
        ];
    }
}
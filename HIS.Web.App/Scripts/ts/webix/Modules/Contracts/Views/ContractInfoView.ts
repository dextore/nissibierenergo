﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../../Components/ComponentBase");
import Common = require("../../../Common/CommonExporter");
import MV = require("../../../Components/Markers/MarkerViewControl");
import Helper = require("./ContractFormHelper");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import SM = require("../../../Common/SystemMessages");
import Events = require("../../../Common/Events");

export interface IContractModulePreviewView extends Base.IComponent {
    contractId: number;
    ready();
    editetdEvent: Events.IEvent<boolean>;
}

export function getContractModulePreviewView(viewName: string, module: Tab.IMainTab) {
    return new ContractModulePreviewView(viewName, module);
}

interface IControlInfo {
    name: string;
    labelWidth: number;
}

class ContractModulePreviewView extends Base.ComponentBase implements IContractModulePreviewView {

    // Загрузка данных
    // Позиционирование контролов
    // Адализ - если неб ключа или данных скрыть форму.
    private _contractId: number;
    private _helper = Helper.getContractFormHelper();
    private _markerControls: { [id: string]: MV.IMarkerViewControl; } = {};
    private _editetdEvent: Events.IEvent<boolean> = new Events.Event<boolean>();

    get viewName() {
        return `${this._viewName}_preview_form`;
    }

    get viewId() {
        return `${this.viewName}_view_id`;
    }

    get optionalsViewId() {
        return `${this.viewName}_optionals_view_id`;
    }

    get formId() {
        return `${this.viewName}_id`;
    }

    get form() {
        return $$(this.formId) as webix.ui.form;
    }

    get contractId() {
        return this._contractId;
    }

    set contractId(value) {
        this._contractId = value;
        this.load(this._contractId);
    }

    get editetdEvent() {
        return this._editetdEvent;
    } 

    constructor(private readonly _viewName: string, private readonly _module: Tab.IMainTab) {
        super();
        SM.SystemMessages.subscribe(SM.ESystemEventType.TREE_SELECTED_CHANGE, this.treeSelectedChange, this);
    }

    private treeSelectedChange(eventArgs: any) {
        webix.message("tree selected change");
    }

    ready() {
        webix.extend($$(this.formId), webix.ProgressBar);
    }

    private load(contractId: number) {
        if (!contractId) {
            this.bindData(null);
            this.form.hide();
            return;
        }

        !this.form.isVisible() && this.form.show();
        this.showProgress();
        Api.getApiMarkers().entityMarkerValues({
            baId: contractId,
            markerIds: [],
            names: Helper.getContractFormHelper().controlNames.map(item => {return item.name})
        })
            .then(result => {
                try {
                    this.hideProgress();
                    this.bindData(result);
                    this.checkEditState(result.markerValues);
                } catch (e) {
                    console.error(e);
                } finally {
                    this.hideProgress();
                }
            }).catch(() => {
                this.hideProgress();
            });;
    }

    private checkEditState(markers: Markers.IMarkerValueModel[]) {
        const filtered = markers.filter(item => { return !item.isBlocked });
        this.editetdEvent.trigger(filtered.length > 0, this);
    }

    private bindData(model: Markers.IEntityMarkersModel ) {
        const formModel = {};

        if (!model) {
            this.form.clear();
            this.form.setValues(formModel);
            return;
        }

        model.markerValues.forEach(item => {
                const control = this._markerControls[item.MVCAliase];
                if (control)
                    control.setModelValue(item, model.baId, formModel);
            },
            this);

        this.form.setValues(formModel);
    }

    private initMarkerControls() {
        this._helper.controlNames.forEach(item => {
            this._markerControls[item.name] = MV.getMarkerViewControl(`${this.viewName}-ctrl-${item.name}`,
                this._helper.markersInfo[item.name],
                item.labelWidth, item.name === "Note", this._module );
        });
    }

    private showProgress() {
        const form = $$(this.formId) as any;
        if (typeof form.showProgress === "function") {
            form.showProgress();
        }
    }

    private hideProgress() {
        const form = $$(this.formId) as any;
        if (typeof form.hideProgress === "function") {
            form.hideProgress();
        }
    }

    // разметка
    init() {
        return {
            id: this.viewId,
            padding: 15,
            rows: [
                {
                    view: "form",
                    id: this.formId,
                    elements: this.getFormElements()
                }
            ]
        };
    }

    // Представления маркеров (простые переодические!!!)
    getFormElements() {
        this.initMarkerControls();

        return [
            {
                cols: [this._markerControls["ContractType"].init(), this._markerControls["UKGroup"].init()]
            }, {
                rows:[
                    {
                        cols: [this._markerControls["Name"].init(),
                               this._markerControls["DocDate"].init(),
                               this._markerControls["StartDate"].init(),
                               this._markerControls["EndDate"].init()]
                    },
                    this._markerControls["CompanyArea"].init(),
                    this._markerControls["LegalSubject"].init(),
                    this._markerControls["Note"].init()
                ] 
            }
        ];
    }

}


﻿import Api = require("../../../Api/ApiExporter");
import Common = require("../../../Common/CommonExporter");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Base = require("../../../Components/Collections/CollectionBase");
import ToolBtn = require("../../../Components/Buttons/ToolBarButtons");
import History = require("../../../Components/History/HistoryEntityDialog");

export interface ILSContractView extends Base.ICollectionBase {
    lsId: number;
}

export function getLSContractView(module: Tab.IMainTab, viewName: string): ILSContractView {
    return new LSContractView(module, viewName);
}

class LSContractView extends Base.CollectionBase implements ILSContractView {
    private _lsId: number = null;

    get lsId(): number {
        return this._lsId;
    }

    set lsId(value: number) {
        this._lsId = value;
        this.loadData();
    }

    constructor(module: Tab.IMainTab, viewName: string) {
        super(viewName, module);
        this.toolbarElements["addbtn"] = ToolBtn.getNewBtn(`${this.viewName}-newcontract`, "Добавить договор", (id, e) => { this.addNewContract(); });
        this.toolbarElements["historybtn"] = ToolBtn.getHistoryBtn(`${this.viewName}-operation-toolbar`, "История", () => { this.showHistory(); });
    }


    showHistory() {
        const datatable = $$(`${this.dataTableId}`) as webix.ui.datatable;
        if (!datatable.getSelectedItem())
            return;

        let contractId = datatable.getSelectedItem().baId;

        const dialog = History.getHistoryEntityDialog(`${this.viewName}-history-dialog`, null, contractId);
        dialog.showModal(() => {
            dialog.close();
        });
    }

    private addNewContract() {

        const module = this.module.mainTabView.showModule("contract-tree-module");
        module.addNew();

        //const entityTypeInfo = Common.entityTypes().getByAliase("Contract").entityInfo;

        //const entityInfo = {
        //    baId: null,
        //    endDate: null,
        //    name: null,
        //    startDate: null,
        //    stateId: null,
        //    stateName: null,
        //    typeId: entityTypeInfo.baTypeId,
        //    typeName: entityTypeInfo.name
        //};

        //this.module.mainTabView.sendTreeItem(entityInfo as any);
    }

    private showContract(rowItem: Contracts.ILSContractModel) {

        this.module.mainTabView.showModule("contract-tree-module");

        const entityInfo = {
            baId: rowItem.baId,
            endDate: null,
            name: rowItem.number,
            startDate: null,
            stateId: null,
            stateName: null,
            typeId: rowItem.baTypeId,
            typeName: rowItem.typeName
        };

        this.module.mainTabView.sendTreeItem(entityInfo as any);
    }

    isParamUndefined(): boolean {
        return !this._lsId;
    }

    getData(): Promise<Contracts.ILSContractModel[]> {
        this.dataTable.clearAll();
        return Api.getApiContrcats().getByLegalSubject(this.lsId);
    }

    internalInitDataTable() {
        return {
            height: 250,
            scroll: "y",
            tooltip: true,
            onClick: {
                contract_number_ref: (ev, id) => {
                    //webix.message(id.row, id.column);
                    const rowItem = this.dataTable.getItem(id);
                    if (!rowItem)
                        return;

                    this.showContract(rowItem);
                }
            }
        };
    }

    initColumns(): any[] {
        return [
            { id: "typeName", header: "Тип", width: 150 },
            { id: "ukGroup", header: "Группа УК", width: 100 },
            { id: "companyAreaName", header: ["Поставщик", { content: "textFilter" }], width: 200 },
            { id: "number", header: "Номер договора", width: 150, template: "<a href='#' class='contract_number_ref'>#number#</a>" },
            { id: "docDate", header: "Дата договора", width: 120 },
            { id: "startDate", header: "Дата начала действия договора", width: 120 },
            { id: "endDate", header: "Дата окончания действия договора", width: 120 },
            { id: "state", header: ["Состояние", { content: "selectFilter" }], width: 100 },
            {
                id: "note", header: "Примечание", fillspace: true, tooltip: (obj, common) => {
                    const column = common.column.id;
                    return obj[column];
                }
            }
        ]; 
    }

    initToolbarElements(): any[] {
        return [{}, this.toolbarElements["addbtn"].init(), this.toolbarElements["historybtn"].init()];
    }
}
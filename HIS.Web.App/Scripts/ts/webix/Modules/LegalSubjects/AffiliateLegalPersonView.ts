﻿import Base = require("../../Components/ComponentBase");
import Api = require("../../Api/ApiExporter");
import Common = require("../../Common/CommonExporter");

export interface IAffiliateLegalPersonView extends Base.IComponent {
    baId: number;
}

export function getAffiliateLegalPersonView(viewName: string): IAffiliateLegalPersonView {
    return new AffiliateLegalPersonView(viewName);
}

class AffiliateLegalPersonView extends Base.ComponentBase implements IAffiliateLegalPersonView {

    private _baId: number;

    get formId() {
        return `${this._viewName}-form-id`;
    }

    get baId() {
        return this._baId;
    };

    set baId(value: number) {
        this._baId = value;
        this.load();
    };

    constructor(private readonly _viewName) {
        super();
    }

    private load() {

        if (!this._baId) {
            const model = {
                name: "",
                fullName: "",
                opf: ""
            };
            ($$(this.formId) as webix.ui.form).setValues(model);
            return;
        }

        (webix.promise.all([Api.ApiLegalPersons.getLegalPerson(this._baId, [131]),
            Api.getApiMarkers().getCatalogMarkerValueItems(
                Common.entityTypes().getByAliase("AffiliateOrganisation").markersInfo["OrganisationLegalForm"].id)] as any) as any)
            .then(results => {
            const data = results[0] as LegalPersons.ILegalPersonModel;
            const values = JSON.parse(results[1]) as Markers.IReferenceMarkerValueItemModel[];

            let opfValue = "";
            if (data.Markers.length) {
                const item = values.filter(item => item.ItemId === Number(data.Markers[0].value))[0];
                opfValue = `${item.ItemName}`;
            }

            const model = {
                name: data.name,
                fullName: data.fullName,
                opf: opfValue
            };
            ($$(this.formId) as webix.ui.form).setValues(model);

        });
    }

    init() {
        return {
                view: "form",
                id: this.formId,
                elements: [
                    { template: "Юридическое лицо", type: "header" },
                    { 
                        cols: [
                            { view: "label", label: "Организационно-правовая форма:", width: 200 },
                            { view: "label", name: "opf" }
                        ]
                },
                    {
                        cols: [
                            { view: "label", label: "Наименование:", width: 200  },
                            { view: "label", name: "name" }
                ] },
                    {
                        cols: [
                            { view: "label", label: "Полное наименование:", width: 200 },
                            { view: "label", name: "fullName" }
                ] }
            ]
        }
    }
}

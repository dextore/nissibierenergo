﻿import Common = require("../../Common/CommonExporter");
import Api = require("../../Api/ApiExporter");
import Mngr = require("../ModuleManager");
import Reg = require("../RegisterModules");
import Form = require("../../Components/Forms/LegalPerson/LegalPersonList");

export class LegalPersonsModule extends Common.MainTabBase {

    protected static _viewName = Form.modulName;
    protected static _header = Reg.Register.modules[Form.modulName].header;

    private _form: Form.ILegalPersonList;

    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
        this._form = Form.getLegalPersonList(this);
    }

    getContent() {
        return this._form.init();
    }

    ready(): void {       
        this._form.ready();
    }
}

Mngr.ModuleManager.registerModule(LegalPersonsModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new LegalPersonsModule(mainView);
});


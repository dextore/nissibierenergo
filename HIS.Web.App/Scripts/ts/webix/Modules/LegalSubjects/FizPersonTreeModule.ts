﻿import Base = require("../../MainView/Tabs/MainTabBase");
import Tab = require("../../MainView/IMainTabsView")
import Mngr = require("../ModuleManager");
import Api = require("../../Api/ApiExporter");
import PersonForm = require("../LegalSubjects/PersonMainForm");
import AddressForm = require("../LegalSubjects/PersonAddressInformation");
import Common = require("../../Common/CommonExporter");
import Passport = require("../LegalSubjects/PersonPassport");
import Operation = require("../../Components/Controls/EntityOperationStateControl");
import AddNew = require("../LegalSubjects/AddNewPersonDlg");
import ToolButtons = require("../../Components/Buttons/ToolBarButtons");
import CommerceActivity = require("../LegalSubjects/CommerceActivity/MainCommerceActivityView");
import Contract = require("../Contracts/LSContracts/LSContractView");

//stub на будущее
export class FizPersonTreeModule extends Base.MainTabBase {

    private _personMarkersInfo: Markers.IMarkerInfoModel[];

    protected static _viewName = "fiz-person-tree-module";
    protected static _header = "Физические лица";

    protected get scrollId() {
        return `${this.viewName}-scrollpanel-id`;
    }

    private _selectedItem: Tree.ISystemTreeItemModel;
    //private _model: { [id: string]: any } = {};

    // Заголовочный контрол статуса
    private _operationState = Operation.getEntityOperationStateControl(`${this.viewName}-operation-state`,
        (personalId: number) => {
            const promise = webix.promise.defer();

            Api.ApiPersons.getPersonHeader(personalId).then(result => {
                (promise as any).resolve(`${result.fullName}`);
            }).catch(() => { (promise as any).reject() });

            return promise;
        },
        () => {
            if (this._form.isEditState || this._addressForm.isEditState || this._passportForm.isEditState) {
                webix.confirm({
                    title: "Предупреждение",
                    text: "Нужно завершить или отменить редактирование компонентов.",
                    ok: "Закрыть"
                });
                return false;
            }
            return true;
        });
    private _form = PersonForm.getPersonMainForm(FizPersonTreeModule._viewName);
    private _addressForm = AddressForm.getPersonAddressInformation(`${FizPersonTreeModule._viewName}-addressControl`);
    private _passportForm = Passport.getPersonAddressInformation(`${FizPersonTreeModule._viewName}-passportControl`);

    private _commerceActivityView = CommerceActivity.getMainCommerceActivityView(this, `${this.viewName}-commerce-ctivity-view`);
    private _personContracts = Contract.getLSContractView(this, `${this.viewName}-person-ls-contracts`);

    private _addNewBtn: ToolButtons.IToolBarButton;

    getIsTreeNeeded(): boolean {
        return true; 
    }

    constructor(mainTabView: Tab.IMainTabbarView) {
        super(mainTabView);

        this.subscribe(this._form.afterChanged.subscribe((model: Persons.IPersonModel) => {
            this._operationState.baId = this._operationState.baId;

            const entityInfo = {
                baId: model.personId,
                endDate: null,
                name: model.fullName,
                startDate: null,
                stateId: null,
                stateName: null,
                typeId: Common.BATypes.Person,
                typeName: "Физическое лицо"
            };

            this.systemSearchHandler(entityInfo);
        }, this));

        this._addNewBtn = ToolButtons.getNewBtn(`${this.viewName}-operation-toolbar`, "Добавить", (id, e) => {
            const dlg = AddNew.getAddNewPersonDlg(`${this.viewName}-addnew-form`, this._form.markersInfo);
            dlg.showModalContent(this,
                () => {
                    const data = dlg.formData;

                    Api.ApiPersons.update(data).then(item => {

                        const filtered = item.markerValues.filter(mv => { return mv.MVCAliase === "Name" });
                        const fullName = filtered.length ? filtered[0].value : "";

                        const entityInfo = {
                            baId: item.baId,
                            endDate: null,
                            name: fullName,
                            startDate: null,
                            stateId: null,
                            stateName: null,
                            typeId: Common.BATypes.Person,
                            typeName: "Физическое лицо"
                        };

                        this.mainTabView.sendTreeItem(entityInfo as any);
                        //this.systemSearchHandler(entityInfo);

                        dlg.close();
                    });
                });
        });

        this._operationState.addButtons(this._addNewBtn);
    }

    treeSelectedChangeHandler(entity: Tree.ISystemTreeItemModel): void {
        this._selectedItem = entity;

        if (!this._selectedItem || entity.baTypeId !== Common.BATypes.Person) {
            this._operationState.baId = null;
            this._form.personId = null;
            this._addressForm.personId = null;
            this._passportForm.personId = null;
            this._commerceActivityView.personId = null;
            $$(this.scrollId).disable();
            return;
        }

        $$(this.scrollId).enable();

        this._selectedItem = entity;

        Api.ApiPersons.getPerson(this._selectedItem.baId).then(data => {
            this._operationState.baId = data.personId;
            this._form.personId = data.personId;
            this._addressForm.personId = data.personId;
            this._passportForm.personId = data.personId;
            this._commerceActivityView.personId = data.personId;
            this._personContracts.lsId = data.personId;
        });
    }

    ready() {
        super.ready();

        $$(this.scrollId).hide();

        //return (webix.promise.all([Api.getApiMarkers().getEntityMarkersInfo(Common.BATypes.Person),
        //    Api.getApiMarkers().getEntityMarkersInfo(Common.BATypes.CommercePerson)] as any) as any).then(result => {

                this._personMarkersInfo = Common.entityTypes().getByAliase("Person").markersInfoArray;
                //this._commerceMarkersInfo = result[1];
                this._form.setMarkersInfo(this._personMarkersInfo);
                this._addressForm.setMarkersInfo(this._personMarkersInfo);
                this._passportForm.setMarkersInfo(this._personMarkersInfo);
                //this._staticCodesForm.setMarkersInfo(this._commerceMarkersInfo);

                this._form.ready();
                this._addressForm.ready();
                this._passportForm.ready();
                this._commerceActivityView.ready();
                //this._collectionForm.ready();

                this._addNewBtn.button.enable();

                $$(this.scrollId).show();
                $$(this.scrollId).enable();
            //}
        //);
    }

    //initData(): Promise<any> {
    //    return (webix.promise.all([Api.getApiMarkers().getEntityMarkersInfo(Common.BATypes.person),
    //        Api.getApiMarkers().getEntityMarkersInfo(Common.BATypes.personIP),
    //        Api.getApiMarkers().getCatalogMarkerValueList(130)] as any) as any).then(result => {
    //            this._personMarkersInfo = result[0];
    //            this._commerceMarkersInfo = result[1];
    //            this._form.setMarkersInfo(this._personMarkersInfo);
    //            this._addressForm.setMarkersInfo(this._personMarkersInfo);
    //            this._passportForm.setMarkersInfo(this._personMarkersInfo);
    //            this._staticCodesForm.setMarkersInfo(this._commerceMarkersInfo);
    //            this._commerceActivityView.setMarkerInfo(this._commerceMarkersInfo.filter(m => m.id === 130)[0]);
    //            this._commerceActivityView.setMarkerValueList(result[2]);
    //        }
    //    );
    //}

    //systemSearchHandler(entity: Entity.IFullTextSearchEntityInfoModel): void {
    //    if (entity.typeId !== Common.BATypes.Person)
    //        return; 

    //    const item = ($$(this.treeId) as webix.ui.tree).find((item) => {
    //            return item.baId === entity.baId;
    //        },
    //        true);
    //    if (item.hasOwnProperty("baId")) {
    //        ($$(this.treeId) as webix.ui.tree).select(item.id, false);

    //        //($$(this.treeId) as webix.ui.tree).updateItem(item.id, entity);
    //        return;
    //    }

    //    ($$(this.treeId) as webix.ui.tree).add(entity);
    //    const nodeId = (entity as any).id * -1;
    //    ($$(this.treeId) as webix.ui.tree).add({ id: nodeId, name: "Договора" }, 0, (entity as any).id);
    //    Api.getApiContrcats().getContractsByContractor(entity.baId).then((result: Contracts.IContractModel[]) => {
    //        result.forEach((item, idx) => {
    //            const contract = webix.extend(item, { id: item.contractId });
    //            ($$(this.treeId) as webix.ui.tree).add(contract, idx, nodeId.toString());
    //        });
    //    });
    //    ($$(this.treeId) as webix.ui.tree).select((entity as any).id, false);
    //}

    getContent() {
        return {
            rows: [
                this._operationState.init(),
                {
                    view: "scrollview",
                    id: this.scrollId,
                    scroll: "Y",
                    body: {
                        type: "space",
                        rows: [
                            this._form.init(),
                            this._addressForm.init(),
                            this._passportForm.init(),
                            {
                                view: "tabview",
                                cells: [ {
                                        header: "Договора",
                                        body: this._personContracts.init()
                                    }
                                ]
                            },
                            this._commerceActivityView.init(),
                            //this._collectionForm.init(),
                            {}
                        ]
                    }
                }
            ],
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        }
    }

}


Mngr.ModuleManager.registerModule(FizPersonTreeModule.viewName, (mainView: Tab.IMainTabbarView) => {
    return new FizPersonTreeModule(mainView);
});

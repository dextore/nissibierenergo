﻿import Base = require("../../Common/DialogBase");
import MF = require("../../Components/Markers/MarkerControlFactory");
import MB = require("../../Components/Markers/MarkerControlBase"); 
import Btn = require("../../Components/Buttons/ButtonsExporter"); 
import MN = require("./MiddleNameControlWrapper");
import Helper = require("../../Components/Markers/MarkerHelper");

export interface IAddNewPersonDlg extends Base.IDialog {
    formData: Persons.IPersonUpdateModel;
}

export function getAddNewPersonDlg(viewName: string, markersInfo: { [id: string]: Markers.IMarkerInfoModel }): IAddNewPersonDlg {
    return new AddNewPersonDlg(viewName, markersInfo);
} 

class AddNewPersonDlg extends Base.DialogBase implements IAddNewPersonDlg {

    protected get labelWidth() { return 110; } 

    private readonly _lastNameCtrl: MB.IMarkerControl;   
    private readonly _firstNameCtrl: MB.IMarkerControl;
    private readonly _middleNameCtrl: MB.IMarkerControl;

    private _middleNameBrapperCtrl: MN.IMiddleNameControlWrapper;

    private _okBtn = Btn.getOkButton(this.viewName, () => { this.okClose(); });
    private _cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });

    get formId() {
        return `${this.viewName}-form-id`;
    }

    headerLabel(): string { return "Создание нового физического лица" }


    get formData(): Persons.IPersonUpdateModel {

        const data = ($$(this.formId) as webix.ui.form).getValues(); 

        const lastNameMarkerInfo = this._markersInfo["LastName"];

        const lastNameMarkerValue = {
            displayValue: null,
            endDate: null,
            markerId: lastNameMarkerInfo.id,
            note: data[`${`${this._markersInfo["LastName"].name}_note`}`],
            startDate: data[`${`${this._markersInfo["LastName"].name}_date`}`],
            value: data[this._markersInfo["LastName"].name],
            isDeleted: false
        };

        return <any>{
            personId: null,
            birthDate: null,
            code: null,
            firstName: data[this._markersInfo["FirstName"].name],
            fullName: null,
            gender: null,
            inn: null,
            lastName: null,
            middleName: data[this._markersInfo["MiddleName"].name],
            markerValues: [lastNameMarkerValue]
        };
    }


    constructor(viewName: string, private readonly _markersInfo: { [id: string]: Markers.IMarkerInfoModel }) {
        super(viewName);

        this._lastNameCtrl = MF.MarkerControlfactory.getControl(this.viewName, this._markersInfo["LastName"], (content) => {return { labelWidth: this.labelWidth };});
        this._firstNameCtrl = MF.MarkerControlfactory.getControl(this.viewName, this._markersInfo["FirstName"], (content) => { return { labelWidth: this.labelWidth }; });
        this._middleNameCtrl = MF.MarkerControlfactory.getControl(this.viewName, this._markersInfo["MiddleName"], 
            (self) => {
                const config = Helper.markerHelper.requiredConfig(self.controlId, webix.rules.isNotEmpty);
                return webix.extend(config, { labelWidth: this.labelWidth });
            }); 
        this._middleNameBrapperCtrl = MN.getMiddleNameControlWrapper(this.viewName, this._middleNameCtrl);
    }

    contentConfig() {
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
                this._lastNameCtrl.init(),
                this._firstNameCtrl.init(),
                this._middleNameBrapperCtrl.init(),
                {
                    cols: [
                        {},
                        this._okBtn.init(),
                        this._cancelBtn.init()
                    ]
                }
            ]
        }
    }

    protected windowConfig() {
        return {
            autoheight: true,
        };
    }

    okClose(): void {
        if (!($$(this.formId) as webix.ui.form).validate())
            return; 

        super.okClose();
    }
}

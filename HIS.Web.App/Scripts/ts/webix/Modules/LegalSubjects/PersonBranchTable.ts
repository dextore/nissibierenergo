﻿import Base = require("../../Components/ComponentBase");
import BranchDlg = require("./BranchesDlg");
import Api = require("../../Api/ApiBranches");
import ToolBtn = require("../../Components/Buttons/ToolBarButtons");
import Pager = require("../../Components/Pager/Pager");
import History = require("../../Components/History/HistoryEntityDialog");

export interface IPersonBranchTable extends Base.IComponent {
    ready();
    baId: number;
}

export function getPersonBranchTable(viewName: string) {
    return new PersonBranchTable(`${viewName}-PersonBranchTable-id`);
}

class PersonBranchTable extends Base.ComponentBase implements IPersonBranchTable {
    private _baId: number;

    public cashModel = {
        deleted: [],
        changed: [],
        added: []
    };

    get baId() {
        return this._baId;
    }

    set baId(value) {
        this._baId = value;
        this.load();
    }

    private get pagerId() { return `${this.viewName}-pager-id`; }

    constructor(private viewName: string) {
        super();
    }

    protected toolBar = {
        edit: ToolBtn.getEditBtn(`${this.viewName}-tooldar`, "Изменить", this.edit.bind(this)),
        save: ToolBtn.getSaveBtn(`${this.viewName}-tooldar`, "Сохранить изменения", this.save.bind(this)),
        cancel: ToolBtn.getCancelBtn(`${this.viewName}-tooldar`, "Отказаться от изменений", this.cancel.bind(this)),
        add: ToolBtn.getNewBtn(`${this.viewName}-tooldar`, "Добавить отрасль", () => this.showPersonBranchDlg()),
        history: ToolBtn.getHistoryBtn(`${this.viewName}-tooldar`, "История", () => {
            this.showHistory();
        })
    }

    
    private _datatableItemId: string | number;

    private cancel() {
        this.load();
    }

    private edit() {
        this.changeState(false);
    }

    private save() {
        const dataTable = $$(this.viewName) as webix.ui.datatable;
        
        if (this.cashModel.deleted.length != 0) {
            Api.ApiBranchs.delete(this.baId, this.cashModel.deleted).then(result => {
                if (result)
                    return;
            });
        }

        if (this.cashModel.changed.length != 0) {
            Api.ApiBranchs.save(this.baId, this.cashModel.changed).then(result => {
                if (result)
                    return;
            });
        }

        if (this.cashModel.added.length != 0) {
            Api.ApiBranchs.save(this.baId, this.cashModel.added).then(result => {
                if (result) {
                    result.forEach(resultItem => {
                        if (resultItem.itemId === -1) {
                            webix.message({
                                type: "error",
                                text: resultItem.name,
                                expire: -1
                            });
                        }
                    });
                    dataTable.clearAll();
                    result.forEach(item => {
                        if (item.itemId != -1)
                            dataTable.add(item);
                    });
                    
                    
                }
            });
        }

        this.changeState(true);
        this.clearCashModel();
    }

    clearCashModel() {
        this.cashModel.changed = [];
        this.cashModel.added = [];
        this.cashModel.deleted = [];
    }

    private changeState(isReadOnly: boolean) {
        const datatable = $$(`${this.viewName}`) as webix.ui.datatable;

        if (isReadOnly) {
            this.toolBar.edit.button.enable();
            if (datatable.count() === 0)
                this.toolBar.history.button.disable();
            else 
                this.toolBar.history.button.enable();

            this.toolBar.cancel.button.disable();
            this.toolBar.save.button.disable();
            this.toolBar.add.button.disable();
            

            datatable.config.editable = false;
            datatable.config.columns[0].template = (obj, common) => obj.name;
            datatable.refresh();

        } else {
            this.toolBar.edit.button.disable();
            this.toolBar.cancel.button.enable();
            this.toolBar.save.button.enable();
            this.toolBar.add.button.enable();
            datatable.config.editable = true;
            datatable.config.columns[0].template = (obj, common) => "<span class='webix_icon fa-trash-o'></span> " + (obj.name) + " <span style=\"height: 22.5px;padding-top:2.5px;\" class='webix_input_icon fa-search-plus'></span>";
            datatable.refresh();
        }
    }

    init() {
        //6/6
        const pager = Pager.getDataTablePager(this.pagerId);
        const datatable = webix.extend(this.datatableInit(), { pager: this.pagerId});
        return {
            rows: [this.toolbarInit(), datatable, pager.init()]
        }
    }

    ready() {
    }

    toolbarInit() {
        return {
            id: `${this.viewName}-personBranchTollBar-id`,
            view: "toolbar",
            paddingY: 3,
            elements: [
                { view: "label", label: "Отрасль" },
                this.toolBar.add.init(),
                this.toolBar.save.init(),
                this.toolBar.cancel.init(),
                this.toolBar.edit.init(),
                this.toolBar.history.init()
            ]
        }
    }

    datatableInit() {
        let self = this;
        return {
            view: "datatable",
            id: this.viewName,
            select: "row",
            multiselect: true,
            editable: true,
            editaction: "click",
            scroll: "y",
            resizeColumn: true,
            datafetch: 5,
            columns: [
                {
                    id: "name",
                    header: "Наименование отрасли",
                    sort: "string",
                    width: 136,
                    fillspace: true
                },
                {
                    id: "brnNum",
                    header: "ОКВЭД",
                    sort: "int",
                    width: 160
                },
                {
                    id: "startDate",
                    header: "Действует с..",
                    sort: "date",
                    width: 160,
                    format: webix.i18n.dateFormatStr,
                    editor: "date"
                },
                {
                    id: "endDate",
                    header: "Действует по..",
                    sort: "date",
                    width: 160,
                    format: webix.i18n.dateFormatStr,
                    editor: "date"
                }
            ],
            height: 180,
            onClick: {
                "fa-trash-o": (ev, id) => {
                    webix.confirm({
                        title: "Подтверждение",
                        type: "confirm-warning",
                        text: `Выполнить операцию удаление отрасли?`,
                        ok: "Да", cancel: "Нет",
                        callback(result) {
                            if (result) {
                                const dataTable = $$(self.viewName) as webix.ui.datatable;
                                let removedItem: Branch.IBranchItemModel = dataTable.getItem(id);
                                self.cashModel.deleted.push(removedItem);
                                dataTable.remove(id);
                            }
                        }
                    });
                },

                "fa-search-plus": (ev, id) => {
                    const datatable = $$(this.viewName) as webix.ui.datatable;
                    this._datatableItemId = datatable.getItem(id);
                    this.showPersonBranchDlg(true);
                }
            },
            on: {
                onAfterEditStop: (state, editor, ignoreUpdate) => {
                    const dataTable = $$(this.viewName) as webix.ui.datatable;
                    let underEditItem: Branch.IBranchItemModel = dataTable.getItem(editor.row);
                    let isStartDateMoreEndDate = underEditItem.startDate.getTime() > underEditItem.endDate.getTime();
                    let isEndDateLessStartDate = underEditItem.endDate.getTime() < underEditItem.startDate.getTime();
                    if (isStartDateMoreEndDate || isEndDateLessStartDate){
                        webix.alert({
                            title: "Невозможное действие",
                            type: "confirm-warning",
                            text: isEndDateLessStartDate ? `Дата окончания не может быть раньше даты начала` : `Дата начала не может быть позже даты окончания`,
                            ok: "Ок"
                        });

                        underEditItem[editor.column] = state.old;
                        dataTable.refresh();
                        return;
                    }
                    
                    this.cashModel.added.forEach(itemInCash => {
                        if (itemInCash.baId === underEditItem.baId) {
                            itemInCash.startDate = underEditItem.startDate;
                            itemInCash.endDate = underEditItem.endDate;
                        }
                    });

                    this.cashModel.changed.forEach(itemInCash => {
                        if (itemInCash.baId === underEditItem.baId) {
                            itemInCash.startDate = underEditItem.startDate;
                            itemInCash.endDate = underEditItem.endDate;
                        }
                    });
                    
                    if (this.cashModel.added.filter(itemInCash => itemInCash.baId === underEditItem.baId).length === 0) {
                        this.cashModel.added.push(underEditItem);
                    } else if (this.cashModel.added.filter(itemInCash => itemInCash.baId === underEditItem.baId).length != 0) {
                        dataTable.refresh();
                        return;
                    } else if (this.cashModel.changed.filter(itemInCash => itemInCash.baId === underEditItem.baId).length === 0) {
                        this.cashModel.changed.push(underEditItem);
                    } else if (this.cashModel.changed.filter(itemInCash => itemInCash.baId === underEditItem.baId).length != 0) {
                        dataTable.refresh();
                        return;
                    }
                }
            }
        }
    }

  

    showPersonBranchDlg(isEditDlg = false) {
        const personBracnhdataTable = ($$(`${this.viewName}`) as webix.ui.datatable);
        const dlg = BranchDlg.getBranchDlg(`${this.viewName}-PersonBranchDlg-id`,
                                            isEditDlg,
                                            personBracnhdataTable,
                                            this._baId,
                                            this._datatableItemId, this); 
        dlg.showModal(() => {
            dlg.close();
        });
    }

    load() {
        const personBracnhdataTable = ($$(`${this.viewName}`) as webix.ui.datatable);
        webix.extend(personBracnhdataTable, webix.ProgressBar);



        personBracnhdataTable.clearAll();
        if (!this.baId)
            return;

        (personBracnhdataTable as any).showProgress();

        Api.ApiBranchs.getPersonBranches(this.baId).then(result => {

            personBracnhdataTable.config.url = <any>{
                $proxy: true,
                load: (view, callback, params) => {
                    (view as webix.ui.datatable).clearAll();
                    let data: Branch.IBranchModel[] = result;
                    (personBracnhdataTable as any).hideProgress();

                    (webix.ajax as any).$callback(view, callback, data);
                    this.resetButtons(true);
                }
            };

            personBracnhdataTable.load(personBracnhdataTable.config.url);

            this.changeState(true);
        });
    }

    protected resetButtons(hasSelecetd: boolean) { }

    showHistory() {
        const datatable = $$(`${this.viewName}`) as webix.ui.datatable;
        if (!datatable.getSelectedItem())
            return;

        let branchBaId = datatable.getSelectedItem().baId;

        
        const dialog = History.getHistoryEntityDialog(`${this.viewName}-history-dialog`, null, branchBaId);
        dialog.showModal(() => {
            dialog.close();
        });
    }
}
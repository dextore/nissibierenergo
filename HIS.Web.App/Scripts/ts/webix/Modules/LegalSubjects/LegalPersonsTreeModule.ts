﻿import Base = require("../../MainView/Tabs/MainTabBase");
import Tab = require("../../MainView/IMainTabsView")
import Mngr = require("../ModuleManager");
import Common = require("../../Common/BATypes");
import LP = require("./LegalPersonView");
import AFF = require("./AffiliateView");

export interface ILegalPersonTreeModule extends Base.IMainTab {
    systemSearchHandler(entity: Entity.IFullTextSearchEntityInfoModel);
}

export class LegalPersonTreeModule extends Base.MainTabBase implements ILegalPersonTreeModule {
    protected static _viewName = "legal-person-tree-module";
    protected static _header = "Юридические лица"; 

    private _selectedItem: Tree.ISystemTreeItemModel;

    private _legalperson = LP.getLegalPersonView(`${this.viewName}-legalperson-view`, this);
    private _affiliete = AFF.getAffiliateView(`${this.viewName}-affiliate-view`, this);

    getIsTreeNeeded(): boolean {
        return true;
    }

    constructor(mainTabView: Tab.IMainTabbarView) {
        super(mainTabView);
    }

    ready() {
        super.ready();

        this._legalperson.ready();
        this._affiliete.ready();
    }

    treeSelectedChangeHandler(entity: Tree.ISystemTreeItemModel): void {
        this._selectedItem = entity;
        if (!this._selectedItem || (entity.baTypeId !== Common.BATypes.Organisation && entity.baTypeId !== Common.BATypes.AffiliateOrganisation)) {
            this._legalperson.baId = null;
            $$(this._legalperson.viewId).show();
            return;
        }

        if (this._selectedItem.baTypeId !== Common.BATypes.AffiliateOrganisation) {
            this._legalperson.baId = entity.baId;
            $$(this._legalperson.viewId).show();
        } else {
            this._affiliete.baId = entity.baId;
            $$(this._affiliete.viewId).show();
        }
    }

    getContent() {
        return {
            view: "multiview",
            animate: false,
            
            cells: [
                this._legalperson.init(),
                this._affiliete.init()
            ],
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        }
    }
} 


Mngr.ModuleManager.registerModule(LegalPersonTreeModule.viewName, (mainView: Tab.IMainTabbarView) => {
    return new LegalPersonTreeModule(mainView);
});

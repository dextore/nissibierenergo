﻿import Base = require("../../Components/Forms/FormBase");
import MF = require("../../Components/Markers/MarkerControlFactory");
import Validators = require("../../Common/Validate");
import Api = require("../../Api/ApiExporter");
import MR = require("../../Components/Markers/MarkersExporter");
import Event = require("../../Common/Events");

export interface ILegalPersonMainForm extends Base.IForm {
    personId: number;
    opfChanged: Event.IEvent<Markers.IMarkerValueModel>;
    isAffiliate: boolean;
}

export function getLegalPersonMainForm(viewName: string): ILegalPersonMainForm {
    return new LegalPersonMainForm(viewName);
}


class LegalPersonMainForm extends Base.FormBase {
    private _personId: number;
    private _code: string;
    private _opfChanged: Event.IEvent<Markers.IMarkerValueModel> = new Event.Event<Markers.IMarkerValueModel>();
    private _isAffiliate: boolean = false;

    get opfChanged(): Event.IEvent<Markers.IMarkerValueModel> {
        return this._opfChanged;
    }

    get personId(): number {
        return this._personId;
    }
    set personId(value: number) {
        this._personId = value;
        this.load();
    }

    get isAffiliate() {
        return this._isAffiliate;
    }

    set isAffiliate(value: boolean) {
        this._isAffiliate = value;
        (this._isAffiliate) ? this.markersControls["INN"].control.disable() : this.markersControls["INN"].control.enable();
    }

    constructor(viewName) {
        super(viewName);
    }

    ready(): void {
        super.ready();
        this.markersControls["OrganisationLegalForm"] = MF.MarkerControlfactory.getControl(`${this.viewName}-opf`,
            this.markersInfo["OrganisationLegalForm"],
            (content) => { return { labelWidth: 270 } });

        this.markersControls["Name"] = MF.MarkerControlfactory.getControl(this.viewName,
            this.markersInfo["Name"],
            self => { return { labelWidth: 270 } });
        this.markersControls["FullName"] = MF.MarkerControlfactory.getControl(this.viewName,
            this.markersInfo["FullName"],
            self => { return { labelWidth: 270 } });
        this.markersControls["INN"] = MF.MarkerControlfactory.getControl(this.viewName,
            this.markersInfo["INN"],
            (self) => {
                return {
                    labelWidth: 270,
                    pattern: { mask: "############", allow: /[0-9]/g },
                    validate: (value) => {
                        if (!(this.markersControls["INN"] as any).customValidate())
                            return false;
                        if (value)
                            return Validators.isInnValid(value);
                        return true;
                    }
                }
            });

        const form = $$(this.formId) as webix.ui.form;
        form.addView(this.markersControls["OrganisationLegalForm"].init());
        form.addView(this.markersControls["Name"].init());
        form.addView(this.markersControls["FullName"].init());
        form.addView(this.markersControls["INN"].init());
    }

    getFormData() {
        const data = ($$(this.formId) as webix.ui.form).getValues() as { [id: string]: any };
        const opfValue = this.markersControls["OrganisationLegalForm"].getValue(data);

        // TODO переделать на единую модель
        if (this.isAffiliate) {

            const model: LegalPersons.IAffiliateOrganisationModel = {
                legalPersonId: this.personId,
                name: data["Name"],
                fullName: data["FullName"],
                inn: data["INN"],
                code: this._code,
                parentId: data["parentId"],
                markerValues: [opfValue]
            } as any;

            return model;
        } 
        const model: LegalPersons.ILegalPersonModel = {
            legalPersonId: this.personId,
            name: data["Name"],
            fullName: data["FullName"],
            inn: data["INN"],
            code: this._code,
            Markers: [opfValue]
        } as any;

        return model;
    }

    setFormData(data: LegalPersons.ILegalPersonModel) {

        if (this.isAffiliate) {
            data = webix.extend(data, { Markers: data["markerValues"] });
            this.model["parentId"] = data["parentId"];
        }

        this.isAffiliate
            ? ($$(this.headerLabelId) as webix.ui.label).setValue(`(Филиал) НСИ - ${data.name} (${data.code})`)
            : ($$(this.headerLabelId) as webix.ui.label).setValue(`НСИ - ${data.name} (${data.code})`);

        if (data.inn === "-1") {
            webix.message({
                type: "error",
                text: "Юридическое лицо с таким инн уже есть в базе",
                expire: -1
            });
            ($$(this.formId) as webix.ui.form).setValues(this.model);
            ($$(this.formId) as webix.ui.form).clearValidation();
            return;
        }

        if (data.Markers) {
            data.Markers.filter(item => {
                    return item.markerId === this.markersInfo["OrganisationLegalForm"].id;
                },
                this).forEach(markerValue => {
                    this.markersControls["OrganisationLegalForm"].setInitValue(markerValue, this.model);
            });

            const markersControls = this.markersControls;

            for (let idx in markersControls) {
                if (markersControls.hasOwnProperty(idx)) {
                    (this.markersControls[idx] as any).baId = data.legalPersonId;
                }
            }
        }

        this.model["Name"] = data.name;
        this.model["FullName"] = data.fullName;
        this.model["INN"] = data.inn;


        this._code = data.code;

        ($$(this.formId) as webix.ui.form).setValues(this.model);

        ($$(this.formId) as webix.ui.form).clearValidation();

    }

    setMarkersInfo(info: Markers.IMarkerInfoModel[]) {
        const filtered = info.filter(m => m.id === 131
                                    || m.id === 1
                                    || m.id === 2
                                    || m.id === 100);
        super.setMarkersInfo(filtered);
    }

    formElementsInit() {
        return [];
    }

    getData(): Promise<any> {
        if (!this.personId) {
            return this.emptyDataSource();
        }

        $$(this.formId).enable();
        return this.isAffiliate
            ? Api.ApiAffiliateOrganisations.getAffiliateOrganisation(this.personId, [131])
            : Api.ApiLegalPersons.getLegalPerson(this.personId, [131]);
    }

    saveData(data: LegalPersons.ILegalPersonModel): Promise<any> {
        if (this.markersControls["OrganisationLegalForm"].isChanged) {
            const opf = this.isAffiliate
                ? (data as any).markerValues.filter(item => item.markerId === 131)[0]
                : data.Markers.filter(item => item.markerId === 131)[0];
            this.opfChanged.trigger(opf, this);
        }

        const promise = webix.promise.defer();

        const updatePromise = this.isAffiliate
            ? Api.ApiAffiliateOrganisations.update(data as any)
            : Api.ApiLegalPersons.update(data);

        (updatePromise as any).then(result => {
            this.getData().then(data => {
                (promise as any).resolve(data);
            });
        });

        return promise;
    }
}

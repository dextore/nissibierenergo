﻿import Base = require("../../../Components/ComponentBase");
import Api = require("../../../Api/ApiExporter");
import Common = require("../../../Common/CommonExporter");
import Create = require("./CreateCommerceActivityView");
import Operation = require("../../../Components/Controls/EntityOperationStateControl");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import opf = require("./OpfForm");
import StaticCodes = require("./PersonStaticCodesBase");
import Acc = require("./BankAccounts/BankAccountsView");
import PersonBranchTable = require("../PersonBranchTable");
import Addr = require("./AddressListView");
import Contract = require("../../Contracts/LSContracts/LSContractView");

export interface IMainCommerceActivityView extends Base.IComponent {
    personId: number;
    ready(): Promise<any>;
}

export function getMainCommerceActivityView(module: Tab.IMainTab, viewName: string): IMainCommerceActivityView {
    return new MainCommerceActivityView(module, viewName);
}

class MainCommerceActivityView extends Base.ComponentBase implements IMainCommerceActivityView {

    private _opfForm: opf.IOPFForm;
    private _staticCodesForm: StaticCodes.IPersonStaticCodes;
    private _accounts: Acc.IBankAccountsView;
    private _personBranchTable: PersonBranchTable.IPersonBranchTable;
    private _addresList: Addr.IAddressListView;

    private get viewId() {
        return `${this._viewName}-commerce-activity-id`;
    } 

    private get createActivityId() {
        return `${this._viewName}-commerce-create-activity-id`;
    } 

    private get showActivityId() {
        return `${this._viewName}-commerce-show-activity-id`;
    } 

    private _markerInfo: Markers.IMarkerInfoModel;
    private _personId: number;
    private _createCommercialActivityView = Create.getCreateCommerceActivityView(this._module, this.viewName);

    // Заголовочный контрол статуса
    private _operationState = Operation.getEntityOperationStateControl(`${this.viewName}-operation-state`,
        "Коммерческая деятельность",
        () => {
            if (this._opfForm.isEditState || this._staticCodesForm.isEditState) {
                webix.confirm({
                    title: "Предупреждение",
                    text: "Нужно завершить или отменить редактирование компонентов.",
                    ok: "Закрыть"
                });
                return false;
            }
            return true;
        });
    private _contracts = Contract.getLSContractView(this._module, `${this.viewName}-ls-contracts`);

    get tabViewId() {
        return `${this.viewName}-tab-view-id`;
    } 

    get markerInfo() { return this._markerInfo };

    get viewName() {
        return this._viewName;
    }

    get personId() {
        return this._personId;
    }

    set personId(value: number) {
        this._personId = value;
        this._createCommercialActivityView.personId = value;
        this.loadData();
    }

    constructor(private readonly _module: Tab.IMainTab, private readonly _viewName: string) {
        super();
        this.subscribe(this._createCommercialActivityView.commerceActivityCreatedEvent.subscribe((data: Commerce.ICommercePersonModel) => {
            this.loadData();
        }, this));

        this.subscribe(this._operationState.statusChangedEvent.subscribe(() => {
            this.loadData();
        }, this));

        this._opfForm = opf.getOPFForm(`${this.viewName}-opf-form`);
        this._staticCodesForm = StaticCodes.getPersonStaticCodes(`${this._viewName}-staticControl`);
        this._accounts = Acc.getBankAccountsView(this._module, `${this.viewName}-bank-accounts`);
        this._personBranchTable = PersonBranchTable.getPersonBranchTable(`${this._viewName}-personBranchId`); // сюда добавить кнопку с историей 
        this._addresList = Addr.getAddressListView(this._module, `${this.viewName}-address-collection`);
    }

    private loadData() {
        if (!this._personId) {
            $$(this.createActivityId).show();
            $$(this.viewId).disable();
            return;
        }

        $$(this.viewId).enable();
        Api.getApiCommercePersons().getOPFByParent(this._personId).then(result => {
            const view = $$(this.viewId) as webix.ui.multiview;
            if (!result) {
                $$(this.createActivityId).show();
                return;
            }
            if (this._operationState.baId !== result.baId) 
                this._operationState.baId = result.baId;
            this._opfForm.baId = result.baId;
            this._staticCodesForm.personId = result.baId;
            this._staticCodesForm.parrentId = result.parentId;
            this._accounts.legaSubjectId = result.baId;
            this._personBranchTable.baId = result.baId;
            this._addresList.legaSubjectId = result.baId;
            this._contracts.lsId = result.baId;
            $$(this.showActivityId).show();
        });
    }

    setMarkerInfo(markersInfo: Markers.IMarkerInfoModel) {
        this._markerInfo = markersInfo;
        
    }

    ready(): Promise<any> {
        return Api.getApiMarkers().getEntityMarkersInfo(Common.BATypes.CommercePerson).then(result => {
            this._createCommercialActivityView.setMarkersInfo(result);

            this._opfForm.setMarkersInfo(result);
            this._opfForm.ready();

            this._staticCodesForm.setMarkersInfo(result);
            this._staticCodesForm.ready();

            this._personBranchTable.ready();

        });
    }

    init() {
        return {
            id: this.viewId,
            view: "multiview",
            animate: false,
            type: "space",
            cells: [
                {
                    id: this.showActivityId,
                    type: "space",
                    rows: [
                        this._operationState.init(),
                        this._opfForm.init(),
                        this._staticCodesForm.init(),
                        {
                            view: "tabview",
                            id: this.tabViewId,
                            cells: [
                                {
                                    header: "Отрасль",
                                    body: this._personBranchTable.init() 
                                },
                                {
                                    header: "Расчетные счета",
                                    body: {
                                        rows:[this._accounts.init()]
                                    }
                                },
                                {
                                    header: "Почтовые адреса",
                                    body: {
                                        rows: [this._addresList.init()]
                                    }

                                }, {
                                    header: "Договора",
                                    body: this._contracts.init()
                                }
                            ]
                        }
                    ]
                },
                {
                    id: this.createActivityId, select: true,
                    cols: [this._createCommercialActivityView.init()]
                }
            ],
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        };
    }

}
﻿import Base = require("../../../../Common/DialogBase");
import Btn = require("../../../../Components/Buttons/ButtonsExporter"); 
import MB = require("../../../../Components/Markers/MarkerControlBase");
import MR = require("../../../../Components/Markers/EntityRefMarkerControl");
import MF = require("../../../../Components/Markers/MarkerControlFactory");
import Tab = require("../../../../MainView/Tabs/MainTabBase");
import Validate = require("../../../../Common/Validate");

export interface IBankAccountDlg extends Base.IDialog {
    setMarkersInfo(fieldsInfo: { [id: string]: Markers.IMarkerInfoModel });
    formDate: BankAccounts.ILSBankAccountModel;
}

export function getBankAccountDlg(viewName: string, model: BankAccounts.ILSBankAccountModel, module: Tab.IMainTab): IBankAccountDlg {
    return new BankAccountDlg(viewName, model, module);
}

class BankAccountDlg extends Base.DialogBase implements IBankAccountDlg {

    private _okBtn = Btn.getOkButton(this.viewName, () => { this.okClose(); });
    private _cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });

    private _markersInfo: { [id: string]: Markers.IMarkerInfoModel };
    private _markersControls: { [id: string]: MB.IMarkerControl } = {};
    private _model: { [id: string]: any } = {};

    get formId() {
        return `${this.viewName}-form-id`;
    }

    headerLabel(): string {
        return `${(this._model.hasOwnProperty("bankAccountId")) ? "(Изменение)" : "(Добавление)"} Расчетный счет`;

    }

    get formDate() {

        let markerValue = this._markersControls["Name"].getValue(this._model);
        this._bankAccountModel.number = markerValue.value;

        markerValue = this._markersControls["Bank"].getValue(this._model);
        this._bankAccountModel.bankName = markerValue.displayValue;
        this._bankAccountModel.bankId = (!markerValue.value) ? null : Number(markerValue.value);

        markerValue = this._markersControls["StartDate"].getValue(this._model);
        this._bankAccountModel.startDate = new Date(markerValue.value);
      
        markerValue = this._markersControls["EndDate"].getValue(this._model);
        this._bankAccountModel.endDate = (markerValue.value == null) ? null: new Date(markerValue.value);

        markerValue = this._markersControls["IsDefault"].getValue(this._model);
        this._bankAccountModel.isDefault = Boolean(markerValue.value);

        return this._bankAccountModel;
    }

    constructor(viewName: string, private _bankAccountModel: BankAccounts.ILSBankAccountModel, private readonly _module: Tab.IMainTab) {
        super(viewName);
    }

    setMarkersInfo(fieldsInfo: { [id: string]: Markers.IMarkerInfoModel }) {
        this._markersInfo = fieldsInfo;
        this.createControls();
    }

    private createControls() {

        // Номер счета
        this._markersControls["Name"] = MF.MarkerControlfactory.getControl(this.viewName, this._markersInfo["Name"],
            (content) => {
                return {
                    labelWidth: 120,
                    gravity: 3,
                    pattern: { mask: "####################", allow: /[0-9]/g },
                    validate: (value) => {
                        if (!(this._markersControls["Name"] as any).customValidate())
                            return false;
                        return Validate.ValidateRules.bankAccount(value);
                    },
                }
            });
        // Банк
        this._markersControls["Bank"] = MF.MarkerControlfactory.getControl(this.viewName, this._markersInfo["Bank"], (content) => { return { labelWidth: 150 } });
        (this._markersControls["Bank"] as MR.IEntityRefMarkerControl).setModule(this._module);
        // Дата открытия
        this._markersControls["StartDate"] = MF.MarkerControlfactory.getControl(this.viewName, this._markersInfo["StartDate"], (content) => { return { labelWidth: 120 } });
        // Дата закрытия
        this._markersControls["EndDate"] = MF.MarkerControlfactory.getControl(this.viewName, this._markersInfo["EndDate"], (content) => { return { labelWidth: 120 } });
        // По умолчанию
        this._markersControls["IsDefault"] = MF.MarkerControlfactory.getControl(this.viewName, this._markersInfo["IsDefault"], (content) => { return { labelWidth: 160 } });
    }

    protected windowConfig() {
        return {
            autoheight: true,
            width: 800
        };
    }

    ready() {
        super.ready();

   
    }

    bindModel(): void {

        let info = this._markersControls["Name"].markerInfo;
        this._markersControls["Name"].setInitValue(getMarkerValue(info, this._bankAccountModel.number, null), this._model);

        info = this._markersControls["Bank"].markerInfo;
        this._markersControls["Bank"].setInitValue(getMarkerValue(info, this._bankAccountModel.bankId as any, this._bankAccountModel.bankName), this._model);

        info = this._markersControls["StartDate"].markerInfo;
        this._markersControls["StartDate"].setInitValue(getMarkerValue(info, this._bankAccountModel.startDate as any, null), this._model);

        info = this._markersControls["EndDate"].markerInfo;
        this._markersControls["EndDate"].setInitValue(getMarkerValue(info, this._bankAccountModel.endDate as any, null), this._model);

        info = this._markersControls["IsDefault"].markerInfo;
        this._markersControls["IsDefault"].setInitValue(getMarkerValue(info, this._bankAccountModel.isDefault as any, null), this._model);



        function getMarkerValue(info: Markers.IMarkerInfoModel, value: string, displayValue: string): Markers.IMarkerValueModel {

            return {
                displayValue: displayValue,
                itemId: null,
                endDate: null,
                markerId: info.id,
                markerType: info.markerType,
                MVCAliase: info.name,
                note: null,
                isVisible: true,
                isBlocked: false,
                startDate: null,
                value: value,
                isDeleted: false
            };
        }

        ($$(this.formId) as webix.ui.form).setValues(this._model);
        ($$(this.formId) as webix.ui.form).clearValidation();



    }

    okClose(): void {
        if (!($$(this.formId) as webix.ui.form).validate()) {
            return;
        }

        super.okClose();
    }

    contentConfig() {
        return {
            rows: [
                {
                    view: "form",
                    id: this.formId,
                    autoheight: true,
                    elements: [
                        { cols: [this._markersControls["Name"].init(), this._markersControls["IsDefault"].init()] },
                        this._markersControls["Bank"].init(),
                        { cols: [this._markersControls["StartDate"].init(), this._markersControls["EndDate"].init()] }
                    ]

                }, {
                    height: 30,
                    cols: [
                        {},
                        this._okBtn.init(),
                        this._cancelBtn.init()
                    ]
                }
            ]
        };
    }

}
﻿/// <reference path="bankaccountdlg.ts" />
import Base = require("../../../../Components/ComponentBase");
import Tab = require("../../../../MainView/Tabs/MainTabBase");
import State = require("../../../../Components/Controls/EntityOperationStateControl");
import List = require("./BankAcccounsListView");
import ToolButtons = require("../../../../Components/Buttons/ToolBarButtons");
import Api = require("../../../../Api/ApiExporter");
import Common = require("../../../../Common/CommonExporter");
import Dlg = require("./BankAccountDlg");

export interface IBankAccountsView extends Base.IComponent {
    legaSubjectId: number;
    ready(): Promise<any>;
}

export function getBankAccountsView(module: Tab.IMainTab, viewName: string): IBankAccountsView {
    return new BankAccountsView(module, viewName);
}

class BankAccountsView extends Base.ComponentBase implements IBankAccountsView {

    private _legaSubjectId: number;

    get viewName() {
        return this._viewName;
    }

    // Виджет статуса
    private _operationState: State.IEntityOperationStateControl;
    // Виджет список счетов
    private _view: List.IBankAccountsListView;

    private _addBtn: ToolButtons.IToolBarButton;  
    private _editBtn: ToolButtons.IToolBarButton; 
    private _deleteBtn: ToolButtons.IToolBarButton;  

    constructor(private readonly _module: Tab.IMainTab, private readonly _viewName: string) {
        super();

        this._view = List.getBankAccountsListView(this._module, `${this.viewName}-accounts-list-view`);
        this.subscribe(this._view.selectedChangeEvent.subscribe((hasSelected: boolean) => {
            this.resetButtons(hasSelected);
            const row = this._view.selectedRow;
            this._operationState.baId = (!row) ? null : row.bankAccountId;
        }, this));

        this._operationState =  State.getEntityOperationStateControl(`${this.viewName}-operation-state`,
            "Расчетные счета",
            () => {
                //if (this._opfForm.isEditState || this._staticCodesForm.isEditState) {
                //    webix.confirm({
                //        title: "Предупреждение",
                //        text: "Нужно завершить или отменить редактирование компонентов.",
                //        ok: "Закрыть"
                //    });
                //    return false;
                //}
                return true;
            });

        this.subscribe(this._operationState.statusChangedEvent.subscribe((value) => {
            this._view.refreshSelectedRow();
            this._operationState.state.stateId === 3
                ? this._deleteBtn.button.disable()
                : this._deleteBtn.button.enable();
        }, this));

        this._addBtn = ToolButtons.getNewBtn(`${this.viewName}-operation-toolbar`, "Добавить", (id, e) => {
            this.showEditForm({} as any);
        });  
        this._editBtn = ToolButtons.getEditBtn(`${this.viewName}-operation-toolbar`, "Изменить", (id, e) => {
            const row = this._view.selectedRow;
            if (!row)
                return; 
            this.showEditForm(row);
        });  
        this._deleteBtn = ToolButtons.getRemoveBtn(`${this.viewName}-operation-toolbar`, "Удалить",
            (id, e) => {
                const row = this._view.selectedRow;
                if (!row)
                    return; 

                webix.confirm({
                    title: "Подтверждение",
                    type: "confirm-warning",
                    text: "Запись будет удалена?",
                    ok: "Да",
                    cancel: "Нет",
                    callback: (result) => {
                        if (result) {
                            Api.ApiLSbankAccounts.remove(row.baId, row.itemId).then(result => {
                                this._view.removeSelectedRow();
                            });
                        }
                    }
            });
        });  

        this._operationState.addButtons( this._addBtn, this._editBtn, this._deleteBtn);
    }

    ready(): Promise<any> {
        return null; 
    }

    get legaSubjectId() {
        return this._legaSubjectId;
    }

    set legaSubjectId(value: number) {
        this._legaSubjectId = value;
        this._view.legalSubjectId = this._legaSubjectId;
    };

    protected resetButtons(hasSelecetd: boolean) {
        if (!this._legaSubjectId) {
            this._addBtn.button.disable();  
            this._editBtn.button.disable();
            this._deleteBtn.button.disable();
            return;
        }
        this._addBtn.button.enable();
        // TODO  Переделать на общий подход с константами.
        hasSelecetd ? this._editBtn.button.enable() : this._editBtn.button.disable();
        hasSelecetd ? this._deleteBtn.button.enable() : this._deleteBtn.button.disable();
        //this._deleteBtn.button.disable()
        //    ? (this._operationState.state.stateId === 3 ? this._deleteBtn.button.disable() : this._deleteBtn.button.enable())
        //    : this._deleteBtn.button.disable();
    }

    private showEditForm(model: BankAccounts.ILSBankAccountModel) {
        const promise = webix.promise.defer();
        if (!fieldsInfo) {
            Api.getApiMarkers().getEntityMarkersInfo(Common.BATypes.BankAccount).then(result => {
                fieldsInfo = {};
                result.forEach(item => {
                    fieldsInfo[item.name] = item;
                });
                (promise as any).resolve(fieldsInfo);
            });
        } else {
            (promise as any).resolve(fieldsInfo);
        }

        promise.then(info => {
            const dlg = Dlg.getBankAccountDlg(`${this.viewName}-edit-bank-account-dlg`, model, this._module);
            dlg.setMarkersInfo(info);
            dlg.showModalContent(this._module,
                () => {
                    const data = dlg.formDate;
                    data.baId = this._legaSubjectId;
                    Api.ApiLSbankAccounts.update(data).then(result => {
                        if (!data.bankAccountId) {
                            this._view.addRow(result);
                        } else {
                            this._view.updateRow(result);
                        }
                        dlg.close();
                    });
                });
        });
    }

    init() {
        return {
            rows: [
                this._operationState.init(),
                this._view.init()
            ],
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        }
    }
}

let fieldsInfo: { [id: string]: Markers.IMarkerInfoModel } = null;
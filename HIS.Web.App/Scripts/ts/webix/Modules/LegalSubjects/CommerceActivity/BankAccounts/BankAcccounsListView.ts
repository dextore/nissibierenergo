﻿import Base = require("../../../../Components/ComponentBase");
import Tab = require("../../../../MainView/Tabs/MainTabBase");
import Api = require("../../../../Api/ApiExporter");
import Common = require("../../../../Common/CommonExporter");

export interface IBankAccountsListView extends Base.IComponent {
    legalSubjectId: number;
    ready(): Promise<any>;
    selectedChangeEvent: Common.IEvent<boolean>;
    addRow(model: BankAccounts.ILSBankAccountModel);
    updateRow(model: BankAccounts.ILSBankAccountModel);
    removeSelectedRow();
    refreshSelectedRow();
    selectedRow: BankAccounts.ILSBankAccountModel;
}

export function getBankAccountsListView(module: Tab.IMainTab, viewName: string): IBankAccountsListView {
    return new BankAccountsListView(module, viewName);
}

class BankAccountsListView extends Base.ComponentBase implements IBankAccountsListView {

    private _legalSubjectId: number;
    private _selectedChangeEvent = new Common.Event<boolean>();

    get viewName() {
        return this._viewName;
    }

    get dataTableId() {
        return `${this.viewName}-datatable-id`;
    }

    get selectedChangeEvent() {
        return this._selectedChangeEvent;
    };

    get selectedRow(): BankAccounts.ILSBankAccountModel {
        const selectedRows = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true);
        return ((selectedRows.length > 0) ? selectedRows[0] : null) as BankAccounts.ILSBankAccountModel;
    }

    constructor(private readonly _module: Tab.IMainTab, private readonly _viewName: string) {
        super();
    }

    addRow(model: BankAccounts.ILSBankAccountModel) {
        const dataTable = $$(this.dataTableId) as webix.ui.datatable;
        const idx = dataTable.add(model, 0);
        dataTable.select(idx, false);
    }

    updateRow(model: BankAccounts.ILSBankAccountModel) {
        const dataTable = $$(this.dataTableId) as webix.ui.datatable;
        const selected = dataTable.getSelectedId(true, false);
        dataTable.updateItem(selected[0], model);
    }

    removeSelectedRow() {
        const dataTable = $$(this.dataTableId) as webix.ui.datatable;
        const selected = dataTable.getSelectedId(true, false);
        if (!selected.length)
            return;
        dataTable.remove(selected[0]);
    }

    refreshSelectedRow() {
        const dataTable = $$(this.dataTableId) as webix.ui.datatable;
        const selected = dataTable.getSelectedItem(true);
        if (!selected.length)
            return;

        const row = selected[0] as BankAccounts.ILSBankAccountModel;
        Api.ApiLSbankAccounts.getLSBankAccount(row.baId, row.itemId)
            .then((data) => {
                this.updateRow(data);
            });
    }

    ready(): Promise<any> {
        return null; 
    }

    get legalSubjectId() {
        return this._legalSubjectId;
    }

    set legalSubjectId(value: number) {
        this._legalSubjectId = value;
        const datatable = $$(this.dataTableId) as webix.ui.datatable;
        datatable.load(datatable.config.url);
    };

    init() {
        return {
            view: "datatable",
            id: this.dataTableId,
            select:"row",
            scroll: "auto",
            height: 200,
            columns: [
                { id: "number", header: "Номер", width: 300 },
                { id: "bankName", header: "Банк", fillspace: true },
                { id: "startDate", header: "Дата открытия", width: 200, format: webix.i18n.dateFormatStr },
                { id: "endDate", header: "Дата закрытия", width: 200, format: webix.i18n.dateFormatStr },
                { id: "stateName", header: "Состояние", width: 200 },
                { id: "isDefault", header: "По умолчанию", width: 150, template: Common.TemplateHelper.DatatableColumnBooleanTemplate()}
            ],
            on: {
                onSelectChange: () => {
                    const hasSelected = (($$(this.dataTableId) as webix.ui.datatable).getSelectedId(true, false) as any[]).length > 0;
                    this.selectedChangeEvent.trigger(hasSelected, this);
                }
            },
            url: {
                $proxy: true,
                load: (view, callback, params) => {
                    ($$(this.dataTableId) as webix.ui.datatable).clearAll();
                    if (!this._legalSubjectId) {
                        (webix.ajax as any).$callback(view, callback, []);
                        this.selectedChangeEvent.trigger(false, this);
                        return;
                    }
                    Api.ApiLSbankAccounts.getAllLSBankAccounts(this._legalSubjectId)
                        .then((data) => {
                            (webix.ajax as any).$callback(view, callback, data);
                            const hasSelected = ((view as webix.ui.datatable).getSelectedId(true, false) as any[]).length > 0;
                            this.selectedChangeEvent.trigger(hasSelected, this);
                        });
                }
            }
        }
    }

}
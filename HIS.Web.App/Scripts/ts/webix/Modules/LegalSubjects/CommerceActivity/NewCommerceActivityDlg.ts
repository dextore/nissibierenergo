﻿import Base = require("../../../Common/DialogBase");
import Marker = require("../../../Components/Markers/MarkersExporter");
import MF = require("../../../Components/Markers/MarkerControlFactory");
import Btn = require("../../../Components/Buttons/ButtonsExporter"); 

export interface INewCommerceActivityDlg extends Base.IDialog {
    formData: Markers.IMarkerValueModel;
}


export function getNewCommerceActivityDlg(viewName: string, markerInfo: Markers.IMarkerInfoModel): INewCommerceActivityDlg {
    return new NewCommerceActivityDlg(viewName, markerInfo);
}


class NewCommerceActivityDlg extends Base.DialogBase implements INewCommerceActivityDlg {

    private _opfMarkerCtrl: Marker.IPeriodMarkerControl; 

    get formId() {
        return `${this.viewName}-form-id`;
    }

    headerLabel(): string {
        return "Выбор ОПФ для коммерческой деятельности";
    }

    private _okBtn: Btn.IButton;
    private _cancelBtn: Btn.IButton;

    constructor(viewName: string, private readonly _markerInfo: Markers.IMarkerInfoModel) {
        super(viewName);
        this._opfMarkerCtrl = MF.MarkerControlfactory.getControl(`${this.viewName}-opf-marker-control-id`, this._markerInfo,
            (content) => {
                return {
                    labelWidth: 250
                }
            }) as Marker.IPeriodMarkerControl;

        this._okBtn = Btn.getOkButton(this.viewName, () => { this.okClose(); });
        this._cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });
    }

    get formData(): Markers.IMarkerValueModel {

        const data = ($$(this.formId) as webix.ui.form).getValues();

        return this._opfMarkerCtrl.getValue(data);
    }

    okClose(): void {
        if (!($$(this.formId) as webix.ui.form).validate())
            return;

        super.okClose();
    }

    contentConfig() {
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
                this._opfMarkerCtrl.init(),
                {
                    cols: [
                        {},
                        this._okBtn.init(),
                        this._cancelBtn.init()
                    ]
                }
            ]
        }
    }
}
﻿import Base = require("../../../Components/ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import ToolBtn = require("../../../Components/Buttons/ToolBarButtons");
import Addr = require("../../../Components/Address/SearchAddress");
import Api = require("../../../Api/ApiExporter");
import Common = require("../../../Common/MAMArkersIdentifiers");
import History = require("../../../Components/History/HistoryEntityDialog");

export interface IAddressListView extends Base.IComponent {
    legaSubjectId: number;
}

export function getAddressListView(module: Tab.IMainTab, viewName: string) {
    return new AddressListView(module, viewName);
}

class AddressListView extends Base.ComponentBase implements IAddressListView {
    

    private _legaSubjectId: number;
    private _readOnly: boolean = true;
    private _removedRows: IMarkerValueModelChanged[] = [];

    protected get dataTableId() { return `${this._viewName}-datatble-id`; }
    protected get toolBarId() { return `${this._viewName}-toolbar-id`; }

    protected toolBar = {
        edit: ToolBtn.getEditBtn(`${this._viewName}-tooldar`, "Изменить", this.edit.bind(this)),
        save: ToolBtn.getSaveBtn(`${this._viewName}-tooldar`, "Сохранить изменения", this.save.bind(this)),
        cancel: ToolBtn.getCancelBtn(`${this._viewName}-tooldar`, "Отказаться от изменений", this.cancel.bind(this)),
        add: ToolBtn.getNewBtn(`${this._viewName}-tooldar`, "Добавить", this.addRow.bind(this)),
        remove: ToolBtn.getRemoveBtn(`${this._viewName}-tooldar`, "Удалить", this.removeRow.bind(this)),
        //history: ToolBtn.getHistoryBtn(`${this._viewName}-tooldar`, "История", () => {
        //    this.showHistory();
        //})
    }

    get viewName() {
        return this._viewName;
    } 

    get legaSubjectId() {
        return this._legaSubjectId;
    }

    set legaSubjectId(value: number) {
        this._legaSubjectId = value;
        ($$(this.dataTableId) as webix.ui.datatable).load(($$(this.dataTableId) as webix.ui.datatable).config.url);
    }

    get hasSelected() {
        const rows = ($$(this.dataTableId) as webix.ui.datatable).getSelectedId(true, false);
        return rows.length > 0;
    }

    constructor(private readonly _module: Tab.IMainTab, private readonly _viewName: string) {
        super();
    } 

    private edit() {
        this.changeState(false);
    }

    private save() {
        //// валидировать форму
        const dataTable = $$(this.dataTableId) as webix.ui.datatable;

        if (!dataTable.validate())
            return;

        const result: IMarkerValueModelChanged[] = [];
        for (let idx in dataTable.data.pull) {
            if (dataTable.data.pull.hasOwnProperty(idx)) {
                const row = dataTable.data.pull[idx] as IMarkerValueModelChanged;
                if (row.isChanged || row.isNew)
                    result.push(row);
            }
        }

        this._removedRows.forEach(item => {
            result.push(item);
        });
                                                                                
        Api.getApiCommercePersons().saveAddresList({ baId: this._legaSubjectId, markerValues: result }).then(result => {
            dataTable.load(dataTable.config.url);
            this.changeState(true);
        });

        //// если ок получить данные формы
        //const data = this.getFormData();
        //// отправить на сервре
        //this.showProgress();
        //this.saveData(data).then(result => {
        //    try {
        //        this.changeState(true);
        //        this.setFormData(result);
        //        this.afterChanged.trigger(result, this);
        //    } finally {
        //        this.hideProgress();
        //    }
        //}).catch(() => {
        //    this.hideProgress();
        //});
        //// если ок
    }

    private cancel() {
        // восстановить данные формы до перехода в редактирование. 
        this.changeState(true);
        ($$(this.dataTableId) as webix.ui.datatable).load(($$(this.dataTableId) as webix.ui.datatable).config.url);
    }

    private changeState(readOnly: boolean) {
        this._readOnly = readOnly;
        this.resetButtorns();
    }

    private resetButtorns() {
        const datatable = $$(`${this.dataTableId}`) as webix.ui.datatable;
        //if (datatable.count() === 0) {
        //    this.toolBar.history.button.disable();
        //}
        //else this.toolBar.history.button.enable();

        (!this._legaSubjectId) ? this.toolBar.edit.button.disable() : this.toolBar.edit.button.enable();

        if (this._readOnly) {
            this.toolBar.add.button.disable();
            this.toolBar.remove.button.disable();
            this.toolBar.save.button.disable();
            this.toolBar.cancel.button.disable();
            return;
        }
        this.toolBar.add.button.enable();
        this.toolBar.save.button.enable();
        this.toolBar.cancel.button.enable();

        (this.hasSelected) ? this.toolBar.remove.button.enable() : this.toolBar.remove.button.disable();
    }

    private afterEditItem(rowId) {

        //webix.message("Запись была изменена");
        if (($$(this.dataTableId) as webix.ui.datatable).validate(rowId)) {
            const row = ($$(this.dataTableId) as webix.ui.datatable).getItem(rowId) as IMarkerValueModelChanged;
            if (row.isChanged)
                return;

            row.isChanged = true;
            ($$(this.dataTableId) as webix.ui.datatable).updateItem(rowId, row);
        }
    }

    private addRow() {
        const idx = ($$(this.dataTableId) as webix.ui.datatable).add({
            isNew: true,
            markerId: Common.MAMArkersIdentifiers.PostAddress, 
            displayValue: "",
            startDate: new Date(),
            endDate: new Date(9999, 11, 31)
        });
        ($$(this.dataTableId) as webix.ui.datatable).select(idx, false);
        ($$(this.dataTableId) as webix.ui.datatable).showItem(idx);
    }

    private removeRow() {
        const selectedRowIds = ($$(this.dataTableId) as webix.ui.datatable).getSelectedId(true, false);

        if (!selectedRowIds.length)
            return;

        webix.alert({
            text: "Маркер будет удален.",
            type: "alert-warning",
            ok: "Да",
            cancel: "Нет",
            callback: (result) => {
                if (!result) {
                    return;
                }
                const row = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true)[0] as IMarkerValueModelChanged;
                if (!row.isNew) {
                    row.isDeleted = true;
                    this._removedRows.push(row);
                }
                ($$(this.dataTableId) as webix.ui.datatable).remove(selectedRowIds[0]);
            }
        });
    }

    init() {
        return {
            rows: [
                this.toolbarInit(),
                this.dataTbaleInit(),
            ]
        }
    }

    // тулбар (редактировать сохранить отказаться)
    private toolbarInit() {
        return {
            view: "toolbar",
            id: this.toolBarId,
            paddingY: 1,
            height: 30,
            elements: [
                {
                    view: "label", label: "Почтовые адреса"
                },
                this.toolBar.add.init(),
                this.toolBar.remove.init(),
                {width: 15},
                this.toolBar.save.init(),
                this.toolBar.cancel.init(),
                this.toolBar.edit.init(),
                //this.toolBar.history.init()
            ]
        }
    } 

    // datatable 
    private dataTbaleInit() {
        return {
            view: "datatable",
            id: this.dataTableId,
            select: "row",
            resizeColumn: true,
            editable: true,
            scroll: "auto",
            height: 200,
            columns: [
                {
                    id: `value`,
                    header: "Значение",
                    sort: "string",
                    minWidth: 200,
                    width: 200,
                    fillspace: true,
                    invalidMessage: "Нужно заполнить аддрес",
                    template: (data: Markers.IMarkerValueModel) => {
                        return `<span style='right:28px; padding-top:5px' class='webix_input_icon fa-search'></span><div style="overflow:hidden">${
                            data.displayValue}</div>`;
                    }
                },
                {
                    id: `startDate`,
                    header: "Дата с..",
                    sort: "date",
                    editor: "dateExtended",
                    //options: { editable: true }, 
                    width: 150,
                    format: webix.Date.dateToStr("%d.%m.%Y", false)
                },
                {
                    id: `endDate`,
                    header: "Дата по..",
                    sort: "date",
                    editor: "dateExtended",
                    width: 150,
                    format: webix.Date.dateToStr("%d.%m.%Y", false)
                }
            ],
            rules: {
                startDate: webix.rules.isNotEmpty,
                value: webix.rules.isNotEmpty
            },
            onClick: {
                "fa-search": (e, id, node) => {
                    if (this._readOnly)
                        return;

                    const dlg = Addr.getSearchAddress(`${this.viewName}-search-address`, null);
                    dlg.showModal(() => {
                        const searchResult: Address.IAddressUpdateResultModel = dlg.selected;

                        const item = ($$(this.dataTableId) as webix.ui.datatable).getItem(id) as Markers.IMarkerValueModel;
                        item.displayValue = searchResult.name;
                        item.value = searchResult.baId.toString();
                        ($$(this.dataTableId) as webix.ui.datatable).updateItem(id, item);
                        this.afterEditItem(id); 
                        dlg.close();
                    });
                }
            },
            on: {
                onSelectChange: () => {
                    this.resetButtorns(); 
                },
                onBeforeEditStart: (id) => {
                    return !this._readOnly;
                },
                onAfterEditStop: (state, editor, ignoreUpdate: boolean) => {
                    if (state.old instanceof Date) {
                        if (state.value != null && ((state.old as Date).getTime() === (state.value as Date).getTime()))
                            return;
                    }
                    if (state.old === state.value)
                        return;
                    this.afterEditItem(editor.row); 
                }
            },
            url: {
                $proxy: true,
                load: (view, callback, params) => {

                    const dataTable = $$(view) as webix.ui.datatable;
                    dataTable.clearAll();
                    this.resetButtorns();
                    this._removedRows = [];
                    if (!this.legaSubjectId) {
                        return;
                    }

                    Api.getApiCommercePersons().getAddresseList(this.legaSubjectId).then(data => {
                        data = data.map(item => {
                            return webix.extend(item, { isShanged: false, isNew: false });
                        });
                        (webix.ajax as any).$callback(view, callback, data);
                        this.resetButtorns();
                    });
                }
            }
        }
    }

    //showHistory() {
    //    const datatable = $$(`${this.dataTableId}`) as webix.ui.datatable;
    //    if (!datatable.getSelectedItem())
    //        return;

    //    const addresBaid = datatable.getSelectedItem().value;
    //    const dialog = History.getHistoryEntityDialog(`${this.viewName}-history-dialog`, null, addresBaid);
    //    dialog.showModal(() => {
    //        dialog.close();
    //    });
    //}

}

interface IMarkerValueModelChanged extends Markers.IMarkerValueModel {
    isChanged: boolean;
    isNew: boolean;
}

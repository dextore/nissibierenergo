﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../../Components/Forms/FormBase");
import MF = require("../../../Components/Markers/MarkerControlFactory");
import MR = require("../../../Components/Markers/MarkersExporter"); 

export interface IOPFForm extends Base.IForm {
    baId: number;
}

export function getOPFForm(viewName: string): IOPFForm {
    return new OPFForm(viewName);
}

class OPFForm extends Base.FormBase  {

    private _baId: number;
    private _opfName: string; 

    get baId() {
        return this._baId;
    }
    set baId(value: number) {
        this._baId = value;
        this.load();
    }

    constructor(viewName) {
        super(viewName);
    }

    getFormData() {
        const formData = ($$(this.formId) as webix.ui.form).getValues();

        const data: Commerce.ICommercePersonUpdateModel = {
            parentId: formData.parentId,
            baId: this._baId,
            markerValues: [this.markersControls[this._opfName].getValue(formData)]
        };

        return data;
    }

    setFormData(model: Commerce.ICommercePersonModel) {
        this.model["parentId"] = model.parentId;
        (this.markersControls[this._opfName] as MR.IPeriodMarkerControl).baId = model.baId;
        model.markerValues.filter(item => item.markerId === 130).forEach(item => {
            this.markersControls[this._opfName].setInitValue(item, this.model);
        }, this);
        ($$(this.formId) as webix.ui.form).setValues(this.model);
    }

    getData(): Promise<any> {
        return Api.getApiCommercePersons().getPerson(this._baId);
    }

    saveData(data): Promise<any> {
        return Api.getApiCommercePersons().update(data);
    }

    ready(): void {
        super.ready();

        this.markersControls[this._opfName] = MF.MarkerControlfactory.getControl(`${this.viewName}-opf`,
            this.markersInfo[this._opfName],
            (content) => { return { labelWidth: 250 } });

        ($$(this.formId) as webix.ui.form).addView(this.markersControls[this._opfName].init());
    }

    setMarkersInfo(info: Markers.IMarkerInfoModel[]): void {
        const filterefd = info.filter(m => m.id === 130);
        this._opfName = filterefd[0].name;
        super.setMarkersInfo(filterefd);
    }

    formElementsInit() {
        return [];
    }

}
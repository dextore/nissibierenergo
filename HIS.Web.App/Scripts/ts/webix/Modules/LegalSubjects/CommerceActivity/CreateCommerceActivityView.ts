﻿import Base = require("../../../Components/ComponentBase");
import ToolBtn = require("../../../Components/Buttons/ToolBarButtons");
import NewDlg = require("./NewCommerceActivityDlg");
import Api = require("../../../Api/ApiExporter");
import Event = require("../../../Common/Events");
import Tab = require("../../../MainView/Tabs/MainTabBase");

export interface ICreateCommerceActivityView extends Base.IComponent {
    personId: number;
    setMarkersInfo(markersInfo: Markers.IMarkerInfoModel[]);
    commerceActivityCreatedEvent: Event.IEvent<Commerce.ICommercePersonModel>;
}

export function getCreateCommerceActivityView(module: Tab.IMainTab, viewName: string): ICreateCommerceActivityView {
    return new CreateCommerceActivityView(module, viewName);
}

class CreateCommerceActivityView extends Base.ComponentBase implements ICreateCommerceActivityView {

    private _personId: number;
    private _markerInfo: Markers.IMarkerInfoModel;
    private _addNewBtn: ToolBtn.IToolBarButton;
    private _commerceActivityCreatedEvent = new Event.Event<Commerce.ICommercePersonModel>();

    get toolBarId() {
        return `${this.viewName}-toolbar-id`;
    }

    get personId() {
        return this._personId;
    };

    set personId(value: number) {
        this._personId = value;
        (!this._personId) ? this._addNewBtn.button.disable() : this._addNewBtn.button.enable();
    };

    get viewName() {
        return this._viewName;
    }

    get commerceActivityCreatedEvent() {
        return this._commerceActivityCreatedEvent;
    };

    constructor(private readonly _module: Tab.IMainTab, private readonly _viewName: string) {
        super();
        this._addNewBtn = ToolBtn.getNewBtn(this.viewName, "Добавить коммерческую деятельность", (id, e) => {this.addNew();});
    }

    setMarkersInfo(markersInfo: Markers.IMarkerInfoModel[]) {
        this._markerInfo = markersInfo.filter(m => m.id === 130)[0];
    }

    private addNew() {
        const dlg = NewDlg.getNewCommerceActivityDlg(`${this.viewName}-new-activity-dlg`, this._markerInfo);
        dlg.showModalContent(this._module ,() => {
            const formData = dlg.formData;
            const data: Commerce.ICommercePersonUpdateModel = {
                    parentId: this.personId,
                    baId: null,
                    markerValues: [formData]
                };

            Api.getApiCommercePersons().update(data).then(result => {
                this._commerceActivityCreatedEvent.trigger(result, this);
                dlg.close();
            });

        });
    }

    init() {
        return {
            view: "toolbar",
            id: this.toolBarId,
            height: 40,
            elements: [
                { view: "label", label: "Добавить коммерческую деятельность" },
                {},
                this._addNewBtn.init()
            ]
        };
    }
}
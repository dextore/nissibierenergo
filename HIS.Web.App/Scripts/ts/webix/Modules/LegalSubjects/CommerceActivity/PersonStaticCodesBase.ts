﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../../Components/Forms/FormBase");
import MF = require("../../../Components/Markers/MarkerControlFactory");

export interface IPersonStaticCodes extends Base.IForm{
    personId: number;
    parrentId: number;
    changeKpp(item: Markers.IMarkerValueModel);
}

export function getPersonStaticCodes(viewName: string): IPersonStaticCodes {
    return new PersonStaticCodes(viewName);
}

export abstract class PersonStaticCodesBase extends Base.FormBase implements IPersonStaticCodes {
    changeKpp(item) {}
    abstract ogrnValidation(value: any);
    abstract okpoValidation(value: any);
   
    private _personId: number;
    private _parrentId: number;

    protected get labelWidth() { return 110; }

    public get personId() {
        return this._personId;
    }

    public set personId(value: number) {
        this._personId = value;
        this.load();
    }

    public get parrentId() {
        return this._parrentId;
    }

    public set parrentId(value: number) {
        this._parrentId = value;
    }

    constructor(viewName) {
        super(viewName);
    }


    getFormData() {
        const data = ($$(this.formId) as webix.ui.form).getValues() as { [id: string]: any };

        const model: Commerce.ICommercePersonModel = {
            baId: this.personId,
            parentId: this.parrentId,
            markerValues: []
        } as Commerce.ICommercePersonModel;

        if (this.markersControls["OKPO"].isChanged) {
            const markerValue = this.markersControls["OKPO"].getValue(data);
            model.markerValues.push(markerValue);
        }

        if (this.markersControls["OGRN"].isChanged) {
            const markerValue = this.markersControls["OGRN"].getValue(data);
            model.markerValues.push(markerValue);
        }

        if (this.markersControls["OKATO"].isChanged) {
            const markerValue = this.markersControls["OKATO"].getValue(data);
            model.markerValues.push(markerValue);
        }

        if (this.markersControls["OKTMO"].isChanged) {
            const markerValue = this.markersControls["OKTMO"].getValue(data);
            model.markerValues.push(markerValue);
        }

        return model;
    }

    setFormData(data: Persons.IPersonInfoResponseModel) {
  
        ($$(this.headerLabelId) as webix.ui.label).setValue("Статистические коды");

        if (!data.personId && (data as any).baId)
            data = webix.extend(data, { personId: (data as any).baId });

        const markersControls = this.markersControls;
        for (let idx in markersControls) {
            if (markersControls.hasOwnProperty(idx)) {
                (this.markersControls[idx] as any).baId = data.personId;
            }
        }
        if (data.hasOwnProperty("markerValues"))
            data = { ...data, markers: (data as any).markerValues }
        data.markers.filter(item => {
            return item.markerId === this.markersInfo["OKPO"].id;
        }).forEach(item => {
            this.markersControls["OKPO"].setInitValue(item, this.model);
        });

        data.markers.filter(item => {
            return item.markerId === this.markersInfo["OGRN"].id;
        }).forEach(item => {
            this.markersControls["OGRN"].setInitValue(item, this.model);
        });

        data.markers.filter(item => {
            return item.markerId === this.markersInfo["OKATO"].id;
        }).forEach(item => {
            this.markersControls["OKATO"].setInitValue(item, this.model);
        });

        data.markers.filter(item => {
            return item.markerId === this.markersInfo["OKTMO"].id;
        }).forEach(item => {
            this.markersControls["OKTMO"].setInitValue(item, this.model);
        });
        ($$(this.formId) as webix.ui.form).setValues(this.model);
        ($$(this.formId) as webix.ui.form).clearValidation();
    }

    ready(): void {
        let self = this;

        this.markersControls["OKPO"] = MF.MarkerControlfactory.getControl(`${this.viewName}-OKPO`,
            this.markersInfo["OKPO"],
            (content) => {
                return {
                    labelWidth: this.labelWidth,
                    pattern: { mask: "##########", allow: /[0-9]/g },
                    validate: (value) => {
                        if (!(this.markersControls["OKPO"] as any).customValidate()) {
                            return false;
                        }
                        return this.okpoValidation(value);
                    }
                }
            });

        this.markersControls["OGRN"] = MF.MarkerControlfactory.getControl(`${this.viewName}-OGRN`,
            this.markersInfo["OGRN"],
            (content) => {
                return {
                    labelWidth: this.labelWidth,
                    validate: (value) => {
                        if (!(this.markersControls["OGRN"] as any).customValidate()) {
                            return false;
                        }
                        return this.ogrnValidation(value);
                    },
                    pattern: { mask: "###############", allow: /[0-9]/g }
                }
            });

        this.markersControls["OKATO"] = MF.MarkerControlfactory.getControl(`${this.viewName}-OKATO`,
            this.markersInfo["OKATO"],
            (content) => {
                return {
                    labelWidth: this.labelWidth,
                    pattern: { mask: "###########", allow: /[0-9]/g },
                    validate: (value) => {
                        if (!(this.markersControls["OKATO"] as any).customValidate()) {
                            return false;
                        }
                        return value.toString().length === 11 || value.toString().length === 0;
                    }
                }
            });

        this.markersControls["OKTMO"] = MF.MarkerControlfactory.getControl(`${this.viewName}-OKTMO`,
            this.markersInfo["OKTMO"],
            (content) => {
                return {
                    labelWidth: this.labelWidth,
                    pattern: { mask: "###########", allow: /[0-9]/g },
                    validate: (value) => {

                        if (!(this.markersControls["OKTMO"] as any).customValidate()) {
                            return false;
                        }
                        return value.toString().length === 11 ||
                            value.toString().length === 8 ||
                            value.toString().length === 0;
                    }
                }
            });

        const form = $$(this.formId) as webix.ui.form;
        form.addView(this.markersControls["OKPO"].init());
        form.addView(this.markersControls["OKATO"].init());
        form.addView(this.markersControls["OGRN"].init());
        form.addView(this.markersControls["OKTMO"].init());
    }
    
    formElementsInit() {
        return [];
    }

    getData(): Promise<any> {
        return Api.ApiPersons.getPersonInformation(<Persons.IPersonInfoModel>{
            personId: this._personId,
            markersId: [102, 103, 104, 106]
        });
    }

    public saveData(data): Promise<any> {
        return Api.getApiCommercePersons().update(data);
    }
}


class PersonStaticCodes extends PersonStaticCodesBase {
    okpoValidation(value) {
        return value.toString().length === 10 || value.toString().length === 0;
    }

    ogrnValidation(value) {
        return value.toString().length === 11 ||
            value.toString().length === 15 ||
            value.toString().length === 0;
    }

   

    constructor(viewName) {
        super(viewName);
    }
}

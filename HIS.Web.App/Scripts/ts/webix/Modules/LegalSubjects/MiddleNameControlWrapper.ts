﻿import MB = require("../../Components/Markers/MarkerControlBase"); 

export interface IMiddleNameControlWrapper {
    init();
    initCheckBox(value: boolean); 
    changeState(readOnly: boolean); 
}

export function getMiddleNameControlWrapper(viewName: string, markerControl: MB.IMarkerControl): IMiddleNameControlWrapper {
    return new MeddleNameControlWrapper(viewName, markerControl);
}

class MeddleNameControlWrapper implements IMiddleNameControlWrapper {

    get checkBoxId() {
        return `${this._viewName}-middle-name-checkbox-id`;
    }

    constructor(private readonly _viewName: string, private readonly _markerControl: MB.IMarkerControl) {
    }

    init() {

        return {
            cols: [
                this._markerControl.init(),
                {
                    view: "checkbox",
                    id: this.checkBoxId,
                    label: "Отсутствует",
                    labelWidth: 100,
                    value: false,
                    width: 130,
                    on: {
                        onChange: (newv, oldv) => {
                            if (newv) {
                                this._markerControl.control.define("required", "");
                                this._markerControl.control.define("validate", "");
                                (this._markerControl.control as any).refresh();
                                (this._markerControl.control as any).setValue("");
                                this._markerControl.control.disable();
                            } else {
                                this._markerControl.control.define("required", "true");
                                this._markerControl.control.define("validate", webix.rules.isNotEmpty);
                                (this._markerControl.control as any).refresh();
                                this._markerControl.control.enable();
                            }
                        }
                    }
                }
            ]
        };
    }

    initCheckBox(value: boolean) {
        ($$(this.checkBoxId) as webix.ui.checkbox).setValue(value as any);
    }

    changeState(readOnly: boolean) {

        const checkBox = $$(this.checkBoxId) as webix.ui.checkbox;
        readOnly ? checkBox.disable() : checkBox.enable();
    }
}
﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Components/Forms/FormBase");
import MF = require("../../Components/Markers/MarkerControlFactory");
import ML = require("../../Components/Markers/MarkerListControl");
import MR = require("../../Components/Markers/MarkersExporter"); 
import Common = require("../../Common/CommonExporter");
import Validators = require("../../Common/Validate");
import Helper = require("../../Components/Markers/MarkerHelper");
import MN = require("./MiddleNameControlWrapper");


export interface IPersonMainForm extends Base.IForm {
    personId: number;
}

export function getPersonMainForm(viewName: string): IPersonMainForm {
    return new PersonMainForm(viewName);
}


class PersonMainForm extends Base.FormBase {

    private _personId: number;

    private _middleNameCtrl: MN.IMiddleNameControlWrapper;

    protected get labelWidth() { return 120; } 

    constructor(viewName: string) {
        super(viewName);
    }

    get personId(): number {
        return this._personId;
    }

    set personId(value: number) {
        this._personId = value;
        this.load();
    }

    getFormData() {
        const data = ($$(this.formId) as webix.ui.form).getValues() as { [id: string]: any };

        const firstNameMarkerValue = this.markersControls["LastName"].getValue(data);

        const model: Persons.IPersonUpdateModel = {
            personId: data["PersonId"], 
            firstName: data["FirstName"],
            //lastName: data["LastName"], 
            middleName: data["MiddleName"],
            birthDate: data["BirthDate"],
            gender: data["Gender"],
            inn: data["INN"],
            markerValues: [firstNameMarkerValue]
        } as any;

        return model;
    }

    //setFormData(model: Persons.IPersonModel) {
    setFormData(values: Markers.IEntityMarkersModel) {
        try {
            
            if (values.markerValues && values.markerValues.length > 0) {

                this._personId = values.baId;

                values.markerValues.forEach(valueItem => {
                    switch (valueItem.MVCAliase) {
                    case "Code":
                        ($$(this.headerLabelId) as webix.ui.label).setValue(`НСИ (${valueItem.value})`);
                        break;
                    default:
                        const ctrl = this.markersControls[valueItem.MVCAliase];
                            ctrl && ctrl.setInitValue(valueItem, this.model);
                    }
                   
                });
                this.model["PersonId"] = this._personId;

                const markersControls = this.markersControls;
                for (let idx in markersControls) {
                    if (markersControls.hasOwnProperty(idx)) {
                        (this.markersControls[idx] as any).baId = this._personId;
                    }
                }
                this._middleNameCtrl.initCheckBox(!this.model["MiddleName"] as any);
            }
            ($$(this.formId) as webix.ui.form).setValues(this.model);
            // add clear form 
            ($$(this.formId) as webix.ui.form).clearValidation();

        } catch (e) {
            console.log(e);
        }
    }

    formElementsInit() {

        return [];
    }

    ready(): void {
        super.ready();
        
        const form = $$(this.formId) as webix.ui.form;
        
        this.markersControls["LastName"] = MF.MarkerControlfactory.getControl(this.viewName,
            this.markersInfo["LastName"],
            (self) => { return { labelWidth: this.labelWidth } });
        (this.markersControls["LastName"]  as any)._baId = this._personId;
     
        this.markersControls["FirstName"] = MF.MarkerControlfactory.getControl(this.viewName,
            this.markersInfo["FirstName"],
            (self) => {
                //const config = Helper.markerHelper.requiredConfig(self.controlId, webix.rules.isNotEmpty);
                //return webix.extend(config, { labelWidth: this.labelWidth });
                return { labelWidth: this.labelWidth };
            });
        this.markersControls["MiddleName"] = MF.MarkerControlfactory.getControl(this.viewName,
            this.markersInfo["MiddleName"],
            (self) => {
                return { labelWidth: this.labelWidth , requied:true };
                //const config = Helper.markerHelper.requiredConfig(self.controlId, webix.rules.isNotEmpty);
                //return webix.extend(config, { labelWidth: this.labelWidth });
            });
        this.markersControls["BirthDate"] = MF.MarkerControlfactory.getControl(this.viewName,
            this.markersInfo["BirthDate"],
            (self) => {
                return { labelWidth: this.labelWidth };
                //const config = Helper.markerHelper.requiredConfig(self.controlId, webix.rules.isNotEmpty);
                //return webix.extend(config, { labelWidth: this.labelWidth, });
            });
        this.markersControls["Gender"] = ML.getMarkerReadioControl(this.viewName,
            this.markersInfo["Gender"],
            (self) => { return { labelWidth: this.labelWidth , disabled:false} });
        
        this.markersControls["INN"] = MF.MarkerControlfactory.getControl(this.viewName,
            this.markersInfo["INN"],
            (self) => {
                return {
                    labelWidth: this.labelWidth,
                    pattern: { mask: "############", allow: /[0-9]/g },
                    validate: (value) => {
                        
                        if (!(this.markersControls["INN"] as any).customValidate())
                            return false;

                        if (value) {
                            return Validators.isInnValid(value);
                        }
                        return true;
                       
                    },
                }
            });
        this._middleNameCtrl = MN.getMiddleNameControlWrapper(this.viewName, this.markersControls["MiddleName"]);

        form.addView(this.markersControls["LastName"].init());
        form.addView(this.markersControls["FirstName"].init());
        form.addView(this._middleNameCtrl.init());
        form.addView(this.markersControls["BirthDate"].init());
        form.addView(this.markersControls["Gender"].init());
        form.addView(this.markersControls["INN"].init());
        //});
    }

    //getData(): Promise<Persons.IPersonModel> {
    getData(): Promise<Markers.IEntityMarkersModel> {
        if (!this._personId) {
            return this.emptyDataSource();
        }
        $$(this.formId).enable();
        //return Api.ApiPersons.getPerson(this._personId);
        return Api.getApiMarkers().entityMarkerValues({
            baId: this._personId,
            names: ["Code", "FirstName", "LastName", "MiddleName", "Name", "BirthDate", "Gender", "INN"]
        } as any);
    }

    saveData(data: Persons.IPersonUpdateModel): Promise<Markers.IEntityMarkersModel> {
        return Api.ApiPersons.update(data);
    }

    protected changeState(readOnly: boolean) {
        super.changeState(readOnly);
        this._middleNameCtrl.changeState(readOnly);

    }
}

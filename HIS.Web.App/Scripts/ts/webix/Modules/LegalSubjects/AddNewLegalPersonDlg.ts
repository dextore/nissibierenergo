﻿import Base = require("../../Common/DialogBase");
import MB = require("../../Components/Markers/MarkerControlBase");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import MF = require("../../Components/Markers/MarkerControlFactory");
import Helper = require("../../Components/Markers/MarkerHelper");
import Validators = require("../../Common/Validate");

//LegalPersons.ILegalPersonModel
// LegalPersons.ILegalPersonImplementedModel = без маркеров

export interface IAddNewLegalPersonDlg extends Base.IDialog {
    formData: LegalPersons.ILegalPersonModel;
}

export function getAddNewLegalPersonDlg(viewName: string, markersInfo: { [id: string]: Markers.IMarkerInfoModel }): IAddNewLegalPersonDlg {
    return new AddNewLegalPersonDlg(viewName, markersInfo);
}


class AddNewLegalPersonDlg extends Base.DialogBase implements IAddNewLegalPersonDlg {
    protected get labelWidth() { return 270; }
    private readonly _markersInfo: { [id: string]: Markers.IMarkerInfoModel }

    private readonly _opfCtrl: MB.IMarkerControl;
    private readonly _nameCtrl: MB.IMarkerControl;
    private readonly _fullNameCtrl: MB.IMarkerControl;
    private readonly _innCtrl: MB.IMarkerControl;
    

    private _okBtn = Btn.getOkButton(this.viewName, () => { this.okClose(); });
    private _cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });

    get formId() {
        return `${this.viewName}-form-id`;
    }

    constructor(viewName, markersInfo) {
        super(viewName);
        this._markersInfo = markersInfo;

        this._opfCtrl = MF.MarkerControlfactory.getControl(`${this.viewName}-opf`,
            this._markersInfo["OrganisationLegalForm"],
            (content) => { return { labelWidth: this.labelWidth } });

        this._nameCtrl = MF.MarkerControlfactory.getControl(this.viewName,
            this._markersInfo["Name"],
            content => { return { labelWidth: this.labelWidth } });

        this._fullNameCtrl = MF.MarkerControlfactory.getControl(this.viewName,
            this._markersInfo["FullName"],
            self => { return { labelWidth: this.labelWidth } });

        this._innCtrl = MF.MarkerControlfactory.getControl(this.viewName,
            this._markersInfo["INN"],
            (self) => {
                return {
                    labelWidth: this.labelWidth,
                    pattern: { mask: "############", allow: /[0-9]/g },
                    validate: (value) => {

                        if (!(this._innCtrl as any).customValidate()) {
                            return false;
                        }
                        if (value)
                            return Validators.isInnValid(value);
                        return true;
                    },
                }
            });
    }

    get formData(): LegalPersons.ILegalPersonModel {
        const data = ($$(this.formId) as webix.ui.form).getValues();
        
        const opf = {
            displayValue: null,
            endDate: null,
            markerId: this._markersInfo["OrganisationLegalForm"].id,
            note: data[`${`${this._markersInfo["OrganisationLegalForm"].name}_note`}`],
            startDate: data[`${`${this._markersInfo["OrganisationLegalForm"].name}_date`}`],
            value: data[this._markersInfo["OrganisationLegalForm"].name],
            isDeleted: false
        };

        return <any>{
            legalPersonId: null,
            code: null,
            fullName: data[this._markersInfo["FullName"].name],
            inn: data[this._markersInfo["INN"].name],
            name: data[this._markersInfo["Name"].name],
            note: "",
            Markers: [opf],
        };
    }

    headerLabel(): string { return "Создание нового юридического лица" }

    contentConfig() {
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
                this._opfCtrl.init(),
                this._nameCtrl.init(),
                this._fullNameCtrl.init(),
                this._innCtrl.init(),
                {
                    cols: [
                        {},
                        this._okBtn.init(),
                        this._cancelBtn.init()
                    ]
                }
            ]
        }
    }

    protected windowConfig() {
        return {
            autoheight: true,
        };
    }

    okClose(): void {
        if (!($$(this.formId) as webix.ui.form).validate())
            return;
        super.okClose();
    }
}
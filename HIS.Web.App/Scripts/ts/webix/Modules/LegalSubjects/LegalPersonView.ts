﻿import Base = require("../../Components/ComponentBase");
import Main = require("./LegalPersonsTreeModule");
import Api = require("../../Api/ApiExporter");
import Common = require("../../Common/CommonExporter");
import AddressForm = require("./LegalPersonAddressInformaton");
import MainForm = require("./LegalPersonMainForm");
import StaticCodes = require("./LegalSubjectsStaticCodes");
import PersonBranchTable = require("./PersonBranchTable");
import Acc = require("./CommerceActivity/BankAccounts/BankAccountsView");
import Affiliate = require("./AffiliateOrganisations/AffiliateOrganisationList");
import Operation = require("../../Components/Controls/EntityOperationStateControl");
import ToolButtons = require("../../Components/Buttons/ToolBarButtons");
import AddNew = require("./AddNewLegalPersonDlg");
import Contract = require("../Contracts/LSContracts/LSContractView");

export interface ILegalPersonView extends Base.IComponent {
    baId: number;
    viewId: string;
    ready();
}

export function getLegalPersonView(viewName: string, module: Main.ILegalPersonTreeModule) {
    return new LegalPersonView(viewName, module);
}

class LegalPersonView extends Base.ComponentBase implements ILegalPersonView {

    private _baId: number;
    private _mainForm = MainForm.getLegalPersonMainForm(`${this.viewName}-mainForm`);
    private _staticCodesForm = StaticCodes.getLegalSubjectsStaticCodes(`${this.viewName}-staticControl`);
    private _addressForm = AddressForm.getLegalPersonAddressInformation(this._module, `${this.viewName}-addressControl`);
    private _personBranchTable = PersonBranchTable.getPersonBranchTable(`${this.viewName}-personBranchId`);
    private _accounts = Acc.getBankAccountsView(this._module, `${this.viewName}-bank-accounts`);
    private _affiliateOrganisations = Affiliate.getAffiliateOrganisationList(this._module, `${this.viewName}-affiliate-organisations`);
    private _operationState = Operation.getEntityOperationStateControl(`${this.viewName}-operation-state`,
        (personalId: number) => {
            const promise = webix.promise.defer();

            Api.ApiLegalPersons.getLegalPerson(personalId, []).then(result => {
                (promise as any).resolve(`${result.name} (${result.code})`);
            }).catch(() => { (promise as any).reject() });

            return promise;
        },
        () => {
            if (this._mainForm.isEditState || this._addressForm.isEditState) {
                webix.confirm({
                    title: "Предупреждение",
                    text: "Нужно завершить или отменить редактирование компонентов.",
                    ok: "Закрыть"
                });
                return false;
            }
            return true;
        });
    private _commerceContracts = Contract.getLSContractView(this._module, `${this.viewName}-commerce-ls-contracts`);

    private _addNewBtn: ToolButtons.IToolBarButton;

    get viewName() {
        return this._viewName;
    }

    get viewId() {
        return `${this._viewName}-view-id`;
    };

    protected get scrollId() {
        return `${this.viewName}-scrollpanel-id`;
    }
    get tabViewId() {
        return `${this.viewName}-tab-view-id`;
    } 

    get baId() {
        return this._baId;
    }

    set baId(value) {
        this._baId = value;

        (!this._baId) ? $$(this.scrollId).disable() : $$(this.scrollId).enable();

        this._operationState.baId = this._baId;
        this._mainForm.personId = this._baId;
        this._staticCodesForm.personId = this._baId;
        this._staticCodesForm.parrentId = this._baId;

        this._addressForm.personId = this._baId;
        this._personBranchTable.baId = this._baId;
        this._accounts.legaSubjectId = this._baId;
        this._affiliateOrganisations.organisationId = this._baId;
        this._commerceContracts.lsId = this._baId;
    }

    constructor(private readonly _viewName: string, private readonly _module: Main.ILegalPersonTreeModule) {
        super();

        this.subscribe(this._mainForm.afterChanged.subscribe((model: LegalPersons.ILegalPersonModel) => {
            this._operationState.baId = this._operationState.baId;
            const entityInfo = webix.extend(model, {
                baId: model.legalPersonId,
                endDate: null,
                name: `<b>Юридическое лицо</b>&nbsp;${model.fullName}`,
                startDate: null,
                stateId: null,
                stateName: null,
                typeId: 12,
                typeName: "Юридическое лицо"
            });

            this._module.systemSearchHandler(entityInfo);
        }, this));

        this._staticCodesForm.subscribe(this._mainForm.opfChanged.subscribe((item) => {
            this._staticCodesForm.changeKpp(item);
        }, this));

        this._addNewBtn = ToolButtons.getNewBtn(`${this.viewName}-operation-toolbar`, "Добавить", (id, e) => {
            const dlg = AddNew.getAddNewLegalPersonDlg(`${this.viewName}-addnew-form`, this._mainForm.markersInfo);
            dlg.showModalContent(this._module,
                () => {
                    const data = dlg.formData;

                    Api.ApiLegalPersons.update(<LegalPersons.ILegalPersonModel>data).then(item => {

                        if (item.inn === "-1") {
                            webix.message({
                                type: "error",
                                text: "Юридическое лицо с таким инн уже есть в базе",
                                expire: -1
                            });
                            return;
                        }

                        const entityInfo = {
                            baId: item.legalPersonId,
                            endDate: null,
                            name: item.name,
                            startDate: null,
                            stateId: null,
                            stateName: null,
                            typeId: Common.BATypes.Organisation,
                            typeName: "Юридическое лицо"
                        };

                        this._module.mainTabView.sendTreeItem(entityInfo as any);

                        dlg.close();
                    });
                });
        });
        this._operationState.addButtons(this._addNewBtn);
    }

    ready() {

        const markersInfo = Common.entityTypes().getByAliase("Organisation").markersInfoArray;

        this._mainForm.setMarkersInfo(markersInfo);
        this._mainForm.ready();

        this._addressForm.setMarkersInfo(markersInfo);
        this._addressForm.ready();

        this._staticCodesForm.setMarkersInfo(markersInfo);
        this._staticCodesForm.ready();

        this._personBranchTable.ready();

        this._addNewBtn.button.enable();

        $$(this.scrollId).show();
        $$(this.scrollId).enable();
    }

    init() {
        return {
            id: this.viewId,
            rows: [
                this._operationState.init(),
                {
                    view: "scrollview",
                    id: this.scrollId,
                    scroll: "Y",
                    body: {
                        type: "space",
                        rows: [
                            this._mainForm.init(),
                            this._addressForm.init(),
                            this._staticCodesForm.init(),
                            {
                                view: "tabview",
                                id: this.tabViewId,
                                cells: [
                                    {
                                        header: "Отрасль",
                                        body: this._personBranchTable.init()
                                    },
                                    {
                                        header: "Расчетные счета",
                                        body: this._accounts.init()
                                    },
                                    {
                                        header: "Филиалы",
                                        body: this._affiliateOrganisations.init()
                                    }, {
                                        header: "Договора",
                                        body: this._commerceContracts.init()
                                    }
                                ]
                            },
                            {}
                        ]
                    }
                }
            ]
        }
    }
}
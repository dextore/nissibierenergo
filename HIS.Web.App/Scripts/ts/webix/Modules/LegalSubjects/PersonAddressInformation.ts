﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Components/Forms/FormBase");
import MF = require("../../Components/Markers/MarkerControlFactory");
import PeriodMarkerControlBase = require("../../Components/Markers/PeriodMarkerControlBase");
import ToolBarBtn = require("../../Components/Buttons/ToolBarButtons");
import Marker = require("../../Components/Markers/MarkerControlBase");
import Select = require("./SelectAddressDlg");


export interface IPersonAddressInformation extends Base.IForm {
    personId: number;
}

export function getPersonAddressInformation(viewName: string): IPersonAddressInformation {
    return new PersonAddressInformation(viewName);
}

class PersonAddressInformation extends Base.FormBase implements IPersonAddressInformation {
    private _personId: number;
    private _regAddrCopyBtn = ToolBarBtn.getCopyBtn(`${this.viewName}-reg`, "Копировать в адрес регистрации",
        (id, e) => { this.copyAddress(this.markersControls["RegAddress"], "В адрес регистрации" ); });
    private _factAddrCopyBtn = ToolBarBtn.getCopyBtn(`${this.viewName}-fact`, "Копировать в адрес фактического проживания",
        (id, e) => { this.copyAddress(this.markersControls["FactAddress"], "В адрес фактического проживания"); });

    protected get labelWidth() { return 210; }

    get personId() {
        return this._personId;
    }
    set personId(value: number) {
        this._personId = value;
        this.load();
    }

    private _fieldsInfo: { [id: string]: Markers.IMarkerInfoModel } = {};

    constructor(viewName) {
        super(viewName);
    }

    private copyAddress(ctrl: Marker.IMarkerControl, header: string) {
        const dlg = Select.getSelectAddressDlg(`${this.viewName}-select-dlg`,
            this.personId,
            [this.markersInfo["RegAddress"].id, this.markersInfo["FactAddress"].id], header);
        dlg.showModal(() => {

            Api.getApiAddress().getAddressEntity(Number(dlg.selected.value)).then(result => {
                result.baId = null;

                const model: Address.IAddressUpdateModel = {
                    aoId: result.AOId,
                    baId: null,
                    flatNum: result.flatNum,
                    flatType: result.flatTypeId,
                    houseId: result.houseId,
                    isActual: result.isActual,
                    isBuild: result.isBuild,
                    location: result.location,
                    name: result.name,
                    POBox: result.POBox,
                    searchType: HIS.Models.Layer.Models.Address.AddressSourceType.Custom,
                    steadId: result.steadId
                };

                Api.getApiAddress().AddressUpdate(model).then(result => {

                    const formData = ($$(this.formId) as webix.ui.form).getValues();

                    ctrl.setValue({
                            displayValue: result.name,
                            markerType: ctrl.markerInfo.markerType,
                            MVCAliase: ctrl.markerInfo.name,
                            itemId: null,
                            endDate: null,
                            markerId: ctrl.markerInfo.id,
                            note: null,
                            isVisible: true,
                            isBlocked: false,
                            startDate: null,
                            value: result.baId.toString(),
                            isDeleted: false
                        },
                        formData);

                    ($$(this.formId) as webix.ui.form).setValues(formData);

                    dlg.close();
                });
            });
        });
    }

    getFormData() {
        const data = ($$(this.formId) as webix.ui.form).getValues() as { [id: string]: any };

        if (data["FactAddress_display"] === data["RegAddress_display"]) {
            if (data["FactAddress"] !== "")
                data["RegAddress"] = data["FactAddress"];
            else data["FactAddress"] = data["RegAddress"];
        }
        const model: Persons.IPersonInfoUpdateModel = {
            personId: this.personId,
            markers: []
        } as Persons.IPersonInfoUpdateModel;

        if (this.markersControls["RegAddress"].isChanged) {
            const markerValue = this.markersControls["RegAddress"].getValue(data);
            model.markers.push(markerValue);
        }

        if (this.markersControls["FactAddress"].isChanged) {
            const markerValue = this.markersControls["FactAddress"].getValue(data);
            model.markers.push(markerValue);
        }

        return model;
    }

    setFormData(model: Persons.IPersonInfoResponseModel) {
        
        ($$(this.headerLabelId) as webix.ui.label).setValue("Адреса");
        (this.markersControls["RegAddress"] as PeriodMarkerControlBase.IPeriodMarkerControl).baId = model.personId;
        (this.markersControls["FactAddress"] as PeriodMarkerControlBase.IPeriodMarkerControl).baId = model.personId;

        if (model.markers) {
            model.markers.filter(item => {
                return item.markerId === this.markersInfo["RegAddress"].id;
            }).forEach(item => {
                this.markersControls["RegAddress"].setInitValue(item, this.model);
                },
                this);

            model.markers.filter(item => {
                return item.markerId === this.markersInfo["FactAddress"].id;
            }).forEach(item => {
                this.markersControls["FactAddress"].setInitValue(item, this.model);
                },
                this);
        }
        ($$(this.formId) as webix.ui.form).setValues(this.model);

    }

    formElementsInit() {
        return [];
    }

    getData(): Promise<any> {
        if (!this._personId) {
            return this.emptyDataSource();
        }

        $$(this.formId).enable();
        return Api.ApiPersons.getPersonInformation(<Persons.IPersonInfoModel>{
            personId: this._personId,
            markersId: [5, 15, 16]
        });
    }

    saveData(data) {
        return Api.ApiPersons.updatePersonInformation(data);
    }

    ready(): void {

        this.markersControls["RegAddress"] = MF.MarkerControlfactory.getControl(`${this.viewName}-reg-address`,
            this.markersInfo["RegAddress"],
            (content) => { return { labelWidth: this.labelWidth }; });
        this.markersControls["FactAddress"] = MF.MarkerControlfactory.getControl(`${this.viewName}-fact-address`,
            this.markersInfo["FactAddress"],
            (content) => { return { labelWidth: this.labelWidth }; });

        const form = $$(this.formId) as webix.ui.form;
        form.addView({ cols:[this.markersControls["RegAddress"].init(), this._regAddrCopyBtn.init()]});
        form.addView({ cols:[this.markersControls["FactAddress"].init(), this._factAddrCopyBtn.init()]});
    }

    changeState(readOnly: boolean) {
        super.changeState(readOnly);
        if (readOnly) {
            this._regAddrCopyBtn.button.disable();
            this._factAddrCopyBtn.button.disable();
            return 
        }
        this._regAddrCopyBtn.button.enable();
        this._factAddrCopyBtn.button.enable();
    }
}

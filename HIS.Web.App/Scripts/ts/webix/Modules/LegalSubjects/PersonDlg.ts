﻿import Base = require("../../Common/DialogBase");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import Api = require("../../Api/ApiExporter");
import Marker = require("../../Components/Markers/MarkersExporter");


export interface IPersonDlg extends Base.IDialog {

}

export function getPersonDlg(viewName: string, personId: number = null): IPersonDlg {
    return new PersonDlg(viewName, personId);
}

class PersonDlg extends Base.DialogBase implements IPersonDlg {

    rendered() {}

    headerLabel(): string { throw new Error("Not implemented"); }

    contentConfig() {}

    constructor(viewName: string, private _legalPersonId: number) {
        super(viewName);
    }
}

﻿import Base = require("../../Components/ComponentBase");
import Main = require("./LegalPersonsTreeModule");
import Api = require("../../Api/ApiExporter");
import Common = require("../../Common/CommonExporter");
import AddressForm = require("./LegalPersonAddressInformaton");
import MainForm = require("./LegalPersonMainForm");
import StaticCodes = require("./LegalSubjectsStaticCodes");
import PersonBranchTable = require("./PersonBranchTable");
import Acc = require("./CommerceActivity/BankAccounts/BankAccountsView");
import Operation = require("../../Components/Controls/EntityOperationStateControl");
import ToolButtons = require("../../Components/Buttons/ToolBarButtons");
import AddNew = require("./AddNewLegalPersonDlg");
import Parent = require("./AffiliateLegalPersonView");
import Contract = require("../Contracts/LSContracts/LSContractView");

export interface IAffiliateView extends Base.IComponent {
    baId: number;
    viewId: string;
    ready();
}

export function getAffiliateView(viewName: string, module: Main.ILegalPersonTreeModule): IAffiliateView {
    return new AffiliateView(viewName, module);
}

class AffiliateView extends Base.ComponentBase implements IAffiliateView {

    private _baId: number;
    private _mainForm = MainForm.getLegalPersonMainForm(`${this.viewName}-mainForm`);
    private _staticCodesForm = StaticCodes.getLegalSubjectsStaticCodes(`${this.viewName}-staticControl`);
    private _addressForm = AddressForm.getLegalPersonAddressInformation(this._module, `${this.viewName}-addressControl`);
    private _personBranchTable = PersonBranchTable.getPersonBranchTable(`${this.viewName}-personBranchId`);
    private _accounts = Acc.getBankAccountsView(this._module, `${this.viewName}-bank-accounts`);
    private _parent = Parent.getAffiliateLegalPersonView(`${this.viewName}-parent-view`);

    private _operationState = Operation.getEntityOperationStateControl(`${this.viewName}-operation-state`,
        (personalId: number) => {
            const promise = webix.promise.defer();

            Api.ApiLegalPersons.getLegalPerson(personalId, []).then(result => {
                (promise as any).resolve(`${result.name} (${result.code})`);
            }).catch(() => { (promise as any).reject() });

            return promise;
        },
        () => {
            if (this._mainForm.isEditState || this._addressForm.isEditState) {
                webix.confirm({
                    title: "Предупреждение",
                    text: "Нужно завершить или отменить редактирование компонентов.",
                    ok: "Закрыть"
                });
                return false;
            }
            return true;
        });
    private _contracts = Contract.getLSContractView(this._module, `${this.viewName}-ls-contracts`);
    //private _addNewBtn: ToolButtons.IToolBarButton;


    get viewName() {
        return this._viewName;
    }

    get viewId() {
        return `${this._viewName}-view-id`;
    };

    protected get scrollId() {
        return `${this.viewName}-scrollpanel-id`;
    }
    get tabViewId() {
        return `${this.viewName}-tab-view-id`;
    }

    get baId() {
        return this._baId;
    }

    set baId(value) {
        this._baId = value;

        Api.ApiAffiliateOrganisations.getAffiliateOrganisation(this._baId, []).then(data => {
            this._parent.baId = data.parentId;
        });

        this._operationState.baId = this._baId;
        this._mainForm.personId = this._baId;
        this._staticCodesForm.personId = this._baId;
        this._staticCodesForm.parrentId = this._baId;

        this._addressForm.personId = this._baId;
        this._personBranchTable.baId = this._baId;
        this._accounts.legaSubjectId = this._baId;
        this._contracts.lsId = this._baId;
    }

    constructor(private readonly _viewName: string, private readonly _module: Main.ILegalPersonTreeModule) {
        super();

        this.subscribe(this._mainForm.afterChanged.subscribe((model: LegalPersons.ILegalPersonModel) => {
            this._operationState.baId = this._operationState.baId;
            const entityInfo = webix.extend(model, {
                baId: model.legalPersonId,
                endDate: null,
                name: model.fullName,
                startDate: null,
                stateId: null,
                stateName: null,
                typeId: Common.BATypes.AffiliateOrganisation,
                typeName: "Филиал обособленное подразделение юридического лица"
            });

            this._module.mainTabView.sendTreeItem(entityInfo as any);
        }, this));

        this._staticCodesForm.subscribe(this._mainForm.opfChanged.subscribe((item) => {
            this._staticCodesForm.changeKpp(item);
        }, this));
    }

    ready() {

        const markersInfo = Common.entityTypes().getByAliase("Organisation").markersInfoArray;

        this._mainForm.setMarkersInfo(markersInfo);
        this._mainForm.ready();
        this._mainForm.isAffiliate = true;

        this._addressForm.setMarkersInfo(markersInfo);
        this._addressForm.ready();

        this._staticCodesForm.setMarkersInfo(markersInfo);
        this._staticCodesForm.ready();

        this._personBranchTable.ready();

        //this._addNewBtn.button.enable();

        $$(this.scrollId).show();
        $$(this.scrollId).enable();
    }

    init() {
        return {
            id: this.viewId,
            rows: [
                this._operationState.init(),
                {
                    view: "scrollview",
                    id: this.scrollId,
                    scroll: "Y",
                    body: {
                        type: "space",
                        rows: [
                            this._parent.init(),
                            this._mainForm.init(),
                            this._addressForm.init(),
                            this._staticCodesForm.init(),
                            {
                                view: "tabview",
                                id: this.tabViewId,
                                cells: [
                                    {
                                        header: "Отрасль",
                                        body: this._personBranchTable.init()
                                    },
                                    {
                                        header: "Расчетные счета",
                                        body: this._accounts.init()
                                    }, {
                                        header: "Договора",
                                        body: this._contracts.init()
                                    }
                                ]
                            },
                            {}
                        ]
                    }
                }
            ]
        }
    }
}
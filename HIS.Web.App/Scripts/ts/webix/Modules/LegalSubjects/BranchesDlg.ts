﻿import Base = require("../../Common/DialogBase");
import Api = require("../../Api/ApiExporter");
import Btn = require("../../Components/Buttons/ButtonsExporter"); 



export interface IBranchDlg extends Base.IDialog {
    
}

export function getBranchDlg(viewName: string, isEditDlg: boolean,
                             datatable: webix.ui.datatable, personId: number,
                             datatableItemId, personBranchTableContext) {
    return new BranchDlg(viewName, isEditDlg, datatable, personId, datatableItemId, personBranchTableContext);
}

class BranchDlg extends Base.DialogBase {
    get formId() {
        return `${this.viewName}-form-id`;
    }

    private _isEditDlg: boolean;
    private _datatable: webix.ui.datatable;
    private _personId: number;
    private _datatableItemId;
    private _personBranchTableContext;

    private _cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });
    private _okBtn = Btn.getOkButton(this.viewName, () => {
        var data: Branch.IBranchItemModel[] = [];

        if (this._isEditDlg) {
            data = this.getUpdatedData();
            data.forEach(checkedItem => {
                    this._personBranchTableContext.cashModel.changed.push(checkedItem);
                    this._datatable.updateItem(this._datatableItemId.id, checkedItem);
                    this.okClose();
                });
        } else {
            data = this.getDataFromDialog();
            let currentDataTableData: Branch.IBranchItemModel[] = [];
            this._datatable.eachRow((rowid) => currentDataTableData.push(this._datatable.getItem(rowid)));
            
            data.forEach(item => {
                item.startDate = new Date();
                item.endDate = new Date(9999, 11, 31);

                this._datatable.add(item);
                this._personBranchTableContext.cashModel.added.push(item);
                this.okClose();
            });
        };
    });

    constructor(viewName: string, isEditDlg: boolean, datatable: webix.ui.datatable, personId: number, datatableItemId: string | number, personBranchTableContext) {
        super(viewName);
        this._isEditDlg = isEditDlg;
        this._datatable = datatable;
        this._personId = personId;
        this._datatableItemId = datatableItemId;
        this._personBranchTableContext = personBranchTableContext;

        webix.DataDriver.branchTree = webix.extend({
            arr2hash: function (data) {
                var hash = {};
                for (var i = 0; i < data.length; i++) {
                    var pid = data[i].parentId;
                    if (!hash[pid]) hash[pid] = [];
                    hash[pid].push(data[i]);
                }
                return hash;
            },
            hash2tree: function (hash, level) {
                var top = hash[level];
                for (var i = 0; i < top.length; i++) {
                    var branch = top[i].baId;
                    if (hash[branch])
                        top[i].data = this.hash2tree(hash, branch);
                }
                return top;
            },
            getRecords: function (data, id) {
                var hash = this.arr2hash(data);
                return this.hash2tree(hash, 0);
            }
        }, webix.DataDriver.json);
    }

    headerLabel(): string { return "Поиск отрасли" }
    
    contentConfig() {
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            autowidth: true,
            elements: [
                {
                    view: "text",
                    id: `${this.formId}-search-id`,
                    label: "Поиск",
                    width: 500,
                    on: {
                        onTimedKeyPress: () => {
                            let searchValue = ($$(`${this.formId}-search-id`) as webix.ui.text).getValue();
                            if (searchValue.match(/[A-Z0-9.]/g))
                                ($$(`${this.formId}-tree-id`) as webix.ui.tree).filter("#brnNum#", searchValue);
                            else
                                ($$(`${this.formId}-tree-id`) as webix.ui.tree).filter("#name#", searchValue);
                        }
                    }
                },
                    {
                        view: "tree",
                        autoheight: true,
                        autowidth: true,
                        id: `${this.formId}-tree-id`,
                        template: function (obj, common) {
                            if (obj.$level > 2) {
                                return common.icon(obj, common) + common.checkbox(obj, common) + " " + obj.brnNum + " " + "<span>" + obj.name + "</span>";
                            } else {
                                return common.icon(obj, common) + " " + obj.brnNum + " " + "<span>" + obj.name + "</span>";
                            }
                        },
                        on: {
                            onItemCheck: (id, state, isCheck) => {
                                if (!state && !isCheck)
                                    return;
                                if (this._isEditDlg) {
                                    const tree = ($$(`${this.formId}-tree-id`) as webix.ui.tree);
                                    tree.select(id, false);
                                    tree.disable();
                                    const button = ($$(`${this.viewName}-enableDisableButton-id`) as webix.ui.button);
                                    if (!button.isVisible()) {
                                        button.show();
                                    }
                                    tree.refresh();
                                }
                            }
                        },
                        datatype: "branchTree",
                        data: Api.ApiBranchs.branchData
                    },
                    {
                        cols: [
                            {},
                            this._okBtn.init(),
                            this._cancelBtn.init()
                        ]
                    },
                    {
                        view: "button",
                        value: "Отменить выбор",
                        hidden: true,
                        id: `${this.viewName}-enableDisableButton-id`,
                        click: () => {
                            const tree = ($$(`${this.formId}-tree-id`) as webix.ui.tree);
                            const button = ($$(`${this.viewName}-enableDisableButton-id`) as webix.ui.button);
                            tree.enable();
                            tree.uncheckAll();
                            tree.unselectAll();
                            if (button.isVisible())
                                button.hide();
                        }
                    }
                ]
            }
    }
    
    protected windowConfig() {
        return {
            autoheight: true,
            autowidth: true
        };
    }

    private clearFiltering() {
        const tree = ($$(`${this.formId}-tree-id`) as webix.ui.tree);
        tree.filter("", "");
        tree.filter("#brnNum#", "");
        tree.filter("#name#", "");
    }

    private getDataFromDialog() {
        this.clearFiltering();
        const tree = ($$(`${this.formId}-tree-id`) as webix.ui.tree);
        let array = tree.getChecked();
        var resultArray: Branch.IBranchItemModel[] = [];
        array.forEach(itemId => resultArray.push(tree.getItem(itemId)));

        return resultArray;
    }
    
    private getUpdatedData() {
        this.clearFiltering();
        var resultArray: Branch.IBranchItemModel[] = [];
        const tree = ($$(`${this.formId}-tree-id`) as webix.ui.tree);

        let underUpdateItem: Branch.IBranchItemModel = this._datatableItemId;
        let id = tree.getChecked()[0];
        let newItem: Branch.IBranchModel = tree.getItem(id);

        underUpdateItem.baId = newItem.baId;
        underUpdateItem.brnNum = newItem.brnNum;
        underUpdateItem.name = newItem.name;
        underUpdateItem.parentId = newItem.parentId;
        resultArray.push(underUpdateItem);

        return resultArray;
    }
}
﻿import Base = require("../../Common/DialogBase");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import Helpers = require("../../Components/Helpers/DataTableHelpers");
import Api = require("../../Api/ApiExporter");

export interface ISelectAddressDlg extends Base.IDialog {
    selected: Markers.IPeriodicMarkerValueModel;
}

export function getSelectAddressDlg(viewName: string, baId: number, markerIds: number[], headre: string): ISelectAddressDlg {
    return new SelectAddressDlg(viewName, baId, markerIds, headre);
}

class SelectAddressDlg extends Base.DialogBase implements ISelectAddressDlg {

    private _okBtn = Btn.getOkButton(`${this.viewName}-ok`, () => { this.okClose(); });
    private _cancelBtn = Btn.getCancelButton(`${this.viewName}-ok`, () => { this.cancelClose(); });

    headerLabel(): string { return this._headre; }

    get dataTableId(): string {
        return `${this.viewName}-datatable-id`;
    }

    get selected(): Markers.IPeriodicMarkerValueModel {
        const selected = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true);
        return (selected.length) ? selected[0] : null;
    }

    constructor(viewName: string,
        private readonly _baId: number,
        private readonly _markerIds: number[],
        private readonly _headre: string) {
        super(`${viewName}-select-address-dlg`);
    }

    private selectionChanges() {
        Helpers.resetButtons(true, ($$(this.dataTableId) as webix.ui.datatable).getSelectedId(true, false).length > 0,
            { ok: this._okBtn });
    }

    private getProxy() {
        return {
            $proxy: true,
            load: (view, callback, params) => {

                const loaders: Promise<Markers.IPeriodicMarkerValueModel[]>[] = [];

                this._markerIds.forEach(id => {
                    loaders.push(Api.getApiMarkers().getPeriodicMarkerHistory(this._baId, id));
                });

                (webix.promise.all(loaders as any) as any).then((results) => {
                    const data: Markers.IPeriodicMarkerValueModel[] = [];

                    (results as any).forEach((item: Markers.IPeriodicMarkerValueModel[]) => {
                        item.forEach(markerValue => {
                            markerValue.endDate = markerValue.endDate < new Date(9999, 1, 31)
                                ? new Date(markerValue.endDate.getFullYear(), markerValue.endDate.getMonth(), markerValue.endDate.getDate() - 1)
                                : markerValue.endDate;
                            data.push(markerValue);
                        });
                    });
                    (webix.ajax as any).$callback(view, callback, data);
                });
            }
        }
    }

    contentConfig() {
        const self = this;

        return {
            rows: [
                toolbarConfig(),
                dataTableConfig(),
                buttonToolBarConfig(),
                { height: 15 }
            ]
        };

        function toolbarConfig() {
            return {
                view: "toolbar",
                paddingY: 1,
                elements: [
                    {
                        view: "label",
                        label: "Выберите нужный адрес",
                        align: "center"}
                ]
            }
        }

        function dataTableConfig() {
            return {
                view: "datatable",
                id: self.dataTableId,
                select: "row",
                resizeColumn: true,
                columns: [
                    {
                        id: `displayValue`,
                        header: "Значение",
                        sort: "string",
                        minWidth: 200,
                        width: 200,
                        fillspace: true,
                    },
                    {
                        id: `startDate`,
                        header: "Дата с..",
                        sort: "Date",
                        editor: "date",
                        format: webix.Date.dateToStr("%d.%m.%Y", false)
                    },
                    {
                        id: `endDate`,
                        header: "Дата по..",
                        sort: "Date",
                        format: webix.Date.dateToStr("%d.%m.%Y", false)
                    }
                ],
                rules: {
                    startDate: webix.rules.isNotEmpty,
                    value: webix.rules.isNotEmpty
                },
                url: self.getProxy(),
                on: {
                    onSelectChange: () => { self.selectionChanges(); },
                }
            }
        }

        function buttonToolBarConfig() {
            return {
                height: 30,
                cols: [
                    {},
                    self._okBtn.init(),
                    self._cancelBtn.init()
                ]
            }
        }
    }
}
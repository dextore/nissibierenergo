﻿import Base = require("../../Components/Forms/FormBase");
import Api = require("../../Api/ApiExporter");
import MF = require("../../Components/Markers/MarkerControlFactory");
import PeriodMarkerControlBase = require("../../Components/Markers/PeriodMarkerControlBase");
import Addr = require("./CommerceActivity/AddressListView");
import Tab = require("../../MainView/Tabs/MainTabBase");

export interface ILegalPersonAddressInformation extends Base.IForm {
    personId: number;
}

export function getLegalPersonAddressInformation(module: Tab.IMainTab, viewName: string): ILegalPersonAddressInformation {
    return new LegalPersonAddressInformation(module, viewName);
}

class LegalPersonAddressInformation extends Base.FormBase implements ILegalPersonAddressInformation {

    private _personId;
    private _module: Tab.IMainTab;
    private _addresList: Addr.IAddressListView;


    get personId(): number {
        return this._personId;
    }
    set personId(value: number) {
        this._personId = value;
        this._addresList.legaSubjectId = value;
        this.load();
    }

    constructor(module: Tab.IMainTab, viewName) {
        super(viewName);
        this._module = module;
        this._addresList = Addr.getAddressListView(this._module, `${this.viewName}-address-collection`);
    }

    getFormData() {
        const data = ($$(this.formId) as webix.ui.form).getValues() as { [id: string]: any };
        
        const model: LegalPersons.ILegalPersonModel = {
            legalPersonId: this.personId,
            Markers: []
        } as LegalPersons.ILegalPersonModel;

        if (this.markersControls["JurAddress"].isChanged) {
            const markerValue = this.markersControls["JurAddress"].getValue(data);
            model.Markers.push(markerValue);
        }

        return model;
    }

    setFormData(data) {
        

        ($$(this.headerLabelId) as webix.ui.label).setValue("Адреса");
        (this.markersControls["JurAddress"] as PeriodMarkerControlBase.IPeriodMarkerControl).baId = data.personId;
        
        data.markers.filter(item => {
            return item.markerId === this.markersInfo["JurAddress"].id;
        }).forEach(item => {
            this.markersControls["JurAddress"].setInitValue(item, this.model);
            }, this);
        ($$(this.formId) as webix.ui.form).setValues(this.model);
    }

    formElementsInit() {
        return [];
    }

    getData(): Promise<any> {
        if (!this._personId) {
            return this.emptyDataSource();
        }

        $$(this.formId).enable();

        return Api.ApiLegalPersons.getPersonInformation(this._personId, [121]);
    }

    saveData(data): Promise<any> {
        return Api.ApiLegalPersons.updatePersonInformation(this._personId, data.Markers);
    }
    
    ready(): void {
        this.markersControls["JurAddress"] = MF.MarkerControlfactory.getControl(`${this.viewName}-jur-address`,
            this.markersInfo["JurAddress"],
            (content) => { return { labelWidth: 210 }; });

        const form = $$(this.formId) as webix.ui.form;
        form.addView({ cols: [this.markersControls["JurAddress"].init()] });
        form.addView(this._addresList.init());
    }

}
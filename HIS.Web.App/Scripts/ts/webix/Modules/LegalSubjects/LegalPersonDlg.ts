﻿import Base = require("../../Common/DialogBase");
import Btn = require("../../Components/Buttons/ButtonsExporter"); 
import Api = require("../../Api/ApiExporter");
import Marker = require("../../Components/Markers/MarkersExporter");
import Component = require("../../Components/ComponentBase");
import Common = require("../../Common/CommonExporter");

export interface ILegalPersonDlg extends Base.IDialog {

}

export function getLegalPersonDlg(viewName: string, legalPersonId: number = null): ILegalPersonDlg {
    return new LegalPersonDlg(viewName, legalPersonId);
}

class LegalPersonDlg extends Base.DialogBase implements ILegalPersonDlg {

    rendered() {}

    private _okBtn = Btn.getOkButton(this.viewName, () => { this.okClose(); });
    private _cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });

    private _fieldsInfo: { [id: string]: Markers.IMarkerInfoModel } = {};
    private _model: { [id: string]: any } = {};

    get formId() {
        return `${this.viewName}-form-id`;
    }

    headerLabel(): string { return "Юридическое лицо"; }

    constructor(viewName: string, private _legalPersonId: number = null) {
        super(viewName);
    }

    bindModel(): void {

        const self = this;
        const promises: Promise<any>[] = [];

        promises.push(Api.getApiMarkers().getEntityMarkersInfo(Common.BATypes.Organisation));

        if (this._legalPersonId) {
            //promises.push(Api.getApiLegalPersons().getLegalPerson(this._legalPersonId));
        }

        (webix.promise.all(promises as any) as any).then((result: any[]) => {
            loadFieldsInfo(result[0]);
            modelBindToFrom(result.length > 1 ? result[1] : null);
        });

        function loadFieldsInfo(result: Markers.IMarkerInfoModel[]) {
            result.forEach(item => {
                const marker = self.createMarkerControl(item);
                ($$(self.formId) as webix.ui.form).addView(marker.init());

                if ("baId" in marker) {
                    (marker as any).baId = self._legalPersonId;
                }

                self._fieldsInfo[item.id] = item;
            }, self);

            ($$(self.formId) as webix.ui.form).addView({
                    cols: [
                        {},
                        self._okBtn.init(),
                        self._cancelBtn.init()
                    ]
                }
            );
            self.checkSize();
        }

        function modelBindToFrom(model: LegalPersons.ILegalPersonModel) {

            self._model["legalPersonId"] = model.legalPersonId;
            self._model["code"] = model.code;
            self._model["name"] = model.name;
            self._model["fullName"] = model.fullName;

            model.Markers.forEach((item: Markers.IMarkerValueModel) => {
                const markerInfo: Markers.IMarkerInfoModel = self._fieldsInfo[item.markerId];
                self._model[markerInfo.name] = item.value;
                if (markerInfo.isPeriodic)
                    self._model[`${markerInfo.name}data`] = item.startDate;
            });

            ($$(self.formId) as webix.ui.form).setValues(self._model);
        }
    }

    contentConfig() {
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
                {
                    view: "text", name: "code", label: "Код", labelWidth: 150,
                    validate: webix.rules.isNotEmpty,
                    on: {
                        onChange: () => {
                            ($$(this.formId) as any).elements["code"].validate();
                            this.checkSize();
                        }
                    }
                },
                {
                    view: "text", name: "name", label: "Наименование", labelWidth: 150,
                    validate: webix.rules.isNotEmpty,
                    on: {
                        onChange: () => {
                            ($$(this.formId) as any).elements["name"].validate();
                            this.checkSize();
                        }
                    }
                },
                {
                    view: "text", name: "fullName", label: "Полное наименование", labelWidth: 150,
                    validate: webix.rules.isNotEmpty,
                    on: {
                        onChange: () => {
                            ($$(this.formId) as any).elements["fullName"].validate();
                            this.checkSize();
                        }
                    }
                }
            ]
        }
    }

    protected windowConfig() {
        return {
            autoheight: true,
        };
    }

    private checkSize() {
        const rect = $$(this.windowId).getNode().getBoundingClientRect();
        const diff = $$(this.formId).getNode().scrollHeight - $$(this.formId).getNode().clientHeight + 10;
        //if (diff > 0)
        $$(this.windowId).$setSize(rect.width, rect.height + diff);
        ($$(this.windowId) as webix.ui.window).define("height", rect.height + diff);
        //($$(this.windowId) as any).refresh();
    }

    private createMarkerControl(model: Markers.IMarkerInfoModel): Component.IComponent {
        const elementId = `${this.viewName}-${model.name}-id`;

        const load = Api.getApiMarkers().getPeriodicMarkerHistory;
        const save = Api.getApiMarkers().saveMarkerHistory;

        if (model.isPeriodic) {
            switch (model.markerType) {
                case HIS.Models.Layer.Models.Markers.MarkerType.MtBoolean:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtInteger:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtDecimal:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtString:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtMoney:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtDateTime:
                    return Marker.getPeriodMarkerControl(elementId, model);
                case HIS.Models.Layer.Models.Markers.MarkerType.MtList:
                    return Marker.getPeriodMarkerListControl(elementId, model);
            }
        }

        switch (model.markerType) {
            case HIS.Models.Layer.Models.Markers.MarkerType.MtBoolean:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtInteger:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtDecimal:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtString:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtMoney:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtDateTime:
                return Marker.getMarkerTextControl(elementId, model);
            case HIS.Models.Layer.Models.Markers.MarkerType.MtList:
                return Marker.getMarkerListControl(elementId, model);
            case HIS.Models.Layer.Models.Markers.MarkerType.MtEntityRef:
                return Marker.getEntityRefMarkerControl(elementId, model);                
            case HIS.Models.Layer.Models.Markers.MarkerType.MtAddress:
                return Marker.getAddressRefMarkerControl(elementId, model);
        }
    }

    private getFormData() {
        const data = ($$(this.formId) as webix.ui.form).getValues() as { [id: string]: any };

        const model: LegalPersons.ILegalPersonModel = {
            legalPersonId: data["legalPersonId"],
            code: data["code"],
            name: data["name"],
            fullName: data["fullName"],
            inn: data["INN"],
            note: data["note"],
            baTypeId: null,
            Markers: []
        };

        const fieldsInfo = this._fieldsInfo;
        for (let id in fieldsInfo) {
            if (fieldsInfo.hasOwnProperty(id)) {
                const markerInfo = fieldsInfo[id];
                const marker: Markers.IMarkerValueModel = {
                    markerId: markerInfo.id,
                    markerType: markerInfo.markerType,
                    MVCAliase: markerInfo.name,
                    itemId: null,
                    startDate: data[`${markerInfo.name}data`],
                    endDate: null,
                    value: data[markerInfo.name],
                    note: "",
                    displayValue: "",
                    isDeleted: false
                } as any;
                model.Markers.push(marker);
            }
        }

        return model;
    }

    okClose(): void {
        if (!($$(this.formId) as webix.ui.form).validate()) {
            this.checkSize();
            return;
        }

        const model = this.getFormData();

        super.okClose();
    }
}
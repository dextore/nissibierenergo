﻿import Base = require("../../../Components/Collections/CollectionBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import ToolButtons = require("../../../Components/Buttons/ToolBarButtons");
import Api = require("../../../Api/ApiExporter");
import Dlg = require("./NewAffiliateOrganisationDlg");
import Common = require("../../../Common/CommonExporter");
import History = require("../../../Components/History/HistoryEntityDialog");

export interface IAffiliateOrganisationList extends Base.ICollectionBase {
    organisationId: number;
}


export function getAffiliateOrganisationList(module: Tab.IMainTab, viewName: string): IAffiliateOrganisationList {
    return new AffiliateOrganisationList(module, viewName);
}

class AffiliateOrganisationList extends Base.CollectionBase implements IAffiliateOrganisationList {
    
    private _loaddata: boolean = false;
    private _organisationId: number = null;
    private _markersInfo: { [id: string]: Markers.IMarkerInfoModel} = null;

    get organisationId(): number {
        return this._organisationId;
    }

    set organisationId(value: number) {
        this._organisationId = value;
        this.loadData();
    }

    constructor(module: Tab.IMainTab, viewName: string) {
        super(viewName, module);
        this.toolbarElements["addbtn"] = ToolButtons.getNewBtn(`${this.viewName}-operation-toolbar`, "Добавить", webix.bind(this.addCmd, this));
        this.toolbarElements["editbtn"] = ToolButtons.getEditBtn(`${this.viewName}-operation-toolbar`, "Изменить", webix.bind(this.editCmd, this));
        this.toolbarElements["historybtn"] = ToolButtons.getHistoryBtn(`${this.viewName}-operation-toolbar`, "История", webix.bind(this.showHistory, this));
        //this.toolbarElements["deletebtn"] = ToolButtons.getRemoveBtn(`${this.viewName}-operation-toolbar`, "Удалить", webix.bind(this.removeCmd, this));
    }

    private showHistory() {
        const datatable = $$(`${this.dataTableId}`) as webix.ui.datatable;
        if (!datatable.getSelectedItem())
            return;

        let affiliateOrganisationId = datatable.getSelectedItem().legalPersonId;

        const dialog = History.getHistoryEntityDialog(`${this.viewName}-history-dialog`, null, affiliateOrganisationId);
        dialog.showModal(() => {
            dialog.close();
        });

    }

    private addCmd() {
        if (this.isParamUndefined())
            return;

        if (this._loaddata)
            return;

        this._loaddata = true;
        Api.ApiLegalPersons.getLegalPerson(this._organisationId, [])
            .then(result => {
                this._loaddata = false;

                result.Markers.push({ markerId: Common.MAMArkersIdentifiers.Name, value: result.name } as any);
                result.Markers.push({ markerId: Common.MAMArkersIdentifiers.FullName, value: result.fullName } as any);

                const dlg = Dlg.getNewAffiliateOrganisationDlg(`${this.viewName}-new-affiliate-dlg`,
                    this._markersInfo,
                    result.Markers);
                dlg.showModalContent(this.module, () => {

                    const formData = dlg.model;

                    const model: LegalPersons.IAffiliateOrganisationModel = {
                        parentId: result.legalPersonId,
                        name: formData.filter(item => item.markerId === Common.MAMArkersIdentifiers.Name)[0].value,
                        fullName: formData.filter(item => item.markerId === Common.MAMArkersIdentifiers.FullName)[0].value,
                        inn: result.inn,
                        markerValues: formData.filter(item => item.markerId === Common.MAMArkersIdentifiers.OrganisationLegalForm)
                    } as any;

                    Api.ApiAffiliateOrganisations.update(model).then(result => {
                        ($$(this.dataTableId) as webix.ui.datatable).add(result);
                        dlg.close();
                    });
                });
            });
    }

    private editCmd() {
        if (!this.hasSelected)
            return;
        const rowItem = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true)[0];
        this.showAffilate(rowItem);
    }

    isParamUndefined(): boolean {
        return !this._organisationId; 
    }

    private showAffilate(rowItem: LegalPersons.IAffiliateOrganisationModel) {
        //webix.message(rowItem.code);           

        const entityInfo = {
            baId: rowItem.legalPersonId,
            endDate: null,
            name: rowItem.fullName,
            startDate: null,
            stateId: null,
            stateName: null,
            typeId: Common.BATypes.AffiliateOrganisation,
            typeName: "Юридическое лицо"
        };

        this.module.mainTabView.sendTreeItem(entityInfo as any);
    }

    getData(): Promise<any> {
        if (this._markersInfo === null) {
            Api.getApiMarkers().getEntityMarkersInfo(Common.BATypes.AffiliateOrganisation)
                .then(items => {
                    this._markersInfo = {};
                    items.forEach(item => {
                        this._markersInfo[item.name] = item;
                    });
                });
        }

        if (!this._organisationId) {
            const promise = webix.promise.defer();
            (promise as any).resolve({});
            $$(this.viewId).disable();
            return promise;
        }

        $$(this.viewId).enable();
        return Api.ApiAffiliateOrganisations.getAffiliateOrganisations(this._organisationId); 
    }

    initColumns(): any[] {
        return [
            {
                id: "code", header: "Код", width: 150,
                template: "<a href='#' class='code_ref'>#code#</a>"
            },
            { id: "name", header: "Наименование", fillspace: true }
        ];
    }

    initToolbarElements(): any[] {
        return [
            {},
            this.toolbarElements["addbtn"].init(),
            this.toolbarElements["editbtn"].init(),
            this.toolbarElements["historybtn"].init()
            //this.toolbarElements["deletebtn"].init()
        ];
    }

    protected internalInitDataTable() {
        return {
            height: 400,
            onClick: {
                code_ref: (ev, id) => {
                    //webix.message(id.row, id.column);
                    const rowItem = ($$(this.dataTableId) as webix.ui.datatable).getItem(id);
                    this.showAffilate(rowItem);
                }
            }
        };
    }
}
﻿import Base = require("../../../Common/DialogFormBase");
import MB = require("../../../Components/Markers/MarkerControlBase");
import MF = require("../../../Components/Markers/MarkerControlFactory");

export interface INewAffiliateOrganisationDlg extends Base.IDialogForm {

}

export function getNewAffiliateOrganisationDlg(viewName: string, fieldsInfo: { [id: string]: Markers.IMarkerInfoModel }, values: Markers.IMarkerValueModel[]): INewAffiliateOrganisationDlg {
    return new NewAffiliateOrganisationDlg(viewName, fieldsInfo, values);
}

class NewAffiliateOrganisationDlg extends Base.DialogFormBase implements INewAffiliateOrganisationDlg {

    headerLabel(): string { return "Филиал юридического лица"; }

    constructor(viewName: string, fieldsInfo: { [id: string]: Markers.IMarkerInfoModel }, values: Markers.IMarkerValueModel[]) {
        super(viewName, fieldsInfo, values);
    }

    getFormElements(markersControls: { [index: string]: MB.IMarkerControl; }, fieldsInfo: { [id: string]: Markers.IMarkerInfoModel }): any[] {

        const labelWidth = 270;

        markersControls["OrganisationLegalForm"] = MF.MarkerControlfactory.getControl(`${this.viewName}-organisationoegalform`, fieldsInfo["OrganisationLegalForm"], (content) => { return { labelWidth } });
        markersControls["Name"] = MF.MarkerControlfactory.getControl(`${this.viewName}-name`, fieldsInfo["Name"], (content) => { return { labelWidth } });
        markersControls["FullName"] = MF.MarkerControlfactory.getControl(`${this.viewName}-fullname`, fieldsInfo["FullName"], (content) => { return { labelWidth } });

        return [markersControls["OrganisationLegalForm"].init(), markersControls["FullName"].init(), markersControls["Name"].init()];
    }

    protected windowConfig() {
        const config = super.windowConfig();
        return webix.extend(config, { width: 800 });
    }

}
﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Components/Forms/FormBase");
import MF = require("../../Components/Markers/MarkerControlFactory");
import Helper = require("../../Components/Markers/MarkerHelper");

export interface IPersonPassport extends Base.IForm {
    personId: number;
}

export function getPersonAddressInformation(viewName: string): IPersonPassport {
    return new PersonPassport(viewName);
}

class PersonPassport extends Base.FormBase {
    private createEmptyModel(markerId) {
        return {
            displayValue: "",
            markerType: null,
            MVCAliase: "",
            itemId: null,
            endDate: null,
            markerId: markerId,
            note: null,
            isVisible: true,
            isBlocked: false,
            startDate: null,
            value: "",
            isDeleted: false
        };
    }
    
    

    private _fieldsInfo: { [id: string]: Markers.IMarkerInfoModel } = {};
    private _isPassport = true;
    private _personId: number;

    get personId() {
        return this._personId;
    }
    set personId(value: number) {
        this._personId = value;
        this.load();
    }
    
    constructor(viewName) {
        super(viewName);
    }

    getFormData() {


        const data = ($$(this.formId) as webix.ui.form).getValues() as { [id: string]: any };

        const model: Persons.IPersonInfoUpdateModel = {
            personId: this.personId,
            markers: []
        } as Persons.IPersonInfoUpdateModel;

        model.markers.push(this.markersControls["IdentityDocumentType"].getValue(data));
        
        if (this.markersControls["IdentityDocumentSeries"].isChanged) {
            const markerValue = this.markersControls["IdentityDocumentSeries"].getValue(data);
            model.markers.push(markerValue);
        }
        if (this.markersControls["IdentityDocumentNumber"].isChanged) {
            const markerValue = this.markersControls["IdentityDocumentNumber"].getValue(data);
            model.markers.push(markerValue);
        }
        if (this.markersControls["IdentityDocumentDate"].isChanged) {
            const markerValue = this.markersControls["IdentityDocumentDate"].getValue(data);
            model.markers.push(markerValue);
        }
        if (this.markersControls["IdentityDocumentDepartment"].isChanged) {
            const markerValue = this.markersControls["IdentityDocumentDepartment"].getValue(data);
            model.markers.push(markerValue);
        }
        if (this.markersControls["IdentityDocumentCode"].isChanged) {
            const markerValue = this.markersControls["IdentityDocumentCode"].getValue(data);
            model.markers.push(markerValue);
        }

        return model;
    }

    setFormData(model: Persons.IPersonInfoResponseModel) {
        ($$(this.headerLabelId) as webix.ui.label).setValue("Документ удостоверяющий личность");

        let isModelHaveIdentityDocumentType: boolean = false;

        
        if (model.markers) {
            model.markers.forEach(item => {
                if (item.markerId === 90) {
                    isModelHaveIdentityDocumentType = true;
                    if (item.value === "1")
                        this._isPassport = true;
                    else
                        this._isPassport = false;
                }
            });

            if (isModelHaveIdentityDocumentType) {
                model.markers.filter(item => {
                    return item.markerId === this.markersInfo["IdentityDocumentType"].id;
                }).forEach(item => {
                    this.markersControls["IdentityDocumentType"].setInitValue(item, this.model);
                    (this.markersControls["IdentityDocumentType"] as any).baId = model.personId;
                });
            } else {
                this.markersControls["IdentityDocumentType"].setInitValue({
                        displayValue: "1",
                        markerType: null,
                        MVCAliase: "",
                        itemId: null,
                        endDate: null,
                        markerId: 90,
                        note: null,
                        isVisible: true,
                        isBlocked: false,
                        startDate: null,
                        value: "1",
                        isDeleted: false
                    },
                    this.model);
                (this.markersControls["IdentityDocumentType"] as any).baId = model.personId;
            }

            // если пасспорт
            if (this._isPassport) {
                //задать серию, код.
                model.markers.filter(item => {
                    return item.markerId === this.markersInfo["IdentityDocumentSeries"].id;
                }).forEach(item => {
                    this.markersControls["IdentityDocumentSeries"].setInitValue(item, this.model);
                    (this.markersControls["IdentityDocumentSeries"] as any).baId = model.personId;
                });

                model.markers.filter(item => {
                    return item.markerId === this.markersInfo["IdentityDocumentCode"].id;
                }).forEach(item => {
                    this.markersControls["IdentityDocumentCode"].setInitValue(item, this.model);
                    (this.markersControls["IdentityDocumentCode"] as any).baId = model.personId;
                });
            }

            // задать остальное
            model.markers.filter(item => {
                return item.markerId === this.markersInfo["IdentityDocumentNumber"].id;
            }).forEach(item => {
                this.markersControls["IdentityDocumentNumber"].setInitValue(item, this.model);
                (this.markersControls["IdentityDocumentNumber"] as any).baId = model.personId;
            });

            model.markers.filter(item => {
                return item.markerId === this.markersInfo["IdentityDocumentDate"].id;
            }).forEach(item => {
                this.markersControls["IdentityDocumentDate"].setInitValue(item, this.model);
                (this.markersControls["IdentityDocumentDate"] as any).baId = model.personId;
            });

            model.markers.filter(item => {
                return item.markerId === this.markersInfo["IdentityDocumentDepartment"].id;
            }).forEach(item => {
                this.markersControls["IdentityDocumentDepartment"].setInitValue(item, this.model);
                (this.markersControls["IdentityDocumentDepartment"] as any).baId = model.personId;
            });
        }
        ($$(this.formId) as webix.ui.form).setValues(this.model);
        ($$(this.formId) as webix.ui.form).clearValidation();
    }
    formElementsInit() {
        return [];
    }
    
    getData(): Promise<any> {
        if (!this._personId) {
            return this.emptyDataSource();
        }

        $$(this.formId).enable();


        return Api.ApiPersons.getPersonInformation(<Persons.IPersonInfoModel>{
                personId: this._personId,
                markersId: [90, 91, 92, 93, 94, 95]
            });
    }

    ready(): void {
        let self = this;
        
        this.markersControls["IdentityDocumentType"] = MF.MarkerControlfactory.getControl(`${this.viewName}-identityDocumentType`,
            this.markersInfo["IdentityDocumentType"],
            (content) => {
                return {
                    labelWidth: 50,
                    label: "Тип",
                    options: Helper.markerHelper.convertListForOptions(this.markersInfo["IdentityDocumentType"].list, false),
                    on: {
                        onChange: (newv, oldv) => {
                            if (newv === oldv) {
                                return;
                            }
                            if (newv === "1")
                                self._isPassport = true;
                            else self._isPassport = false;
                            self.markersControls["IdentityDocumentType"].isChanged = true;
                            self.changeConfig();
                        }
                    }
                };
            });

        this.markersControls["IdentityDocumentSeries"] = MF.MarkerControlfactory.getControl(`${this.viewName}-identityDocumentSeries`,
            this.markersInfo["IdentityDocumentSeries"],
            (content) => {
                return self._isPassport
                    ? {
                        labelWidth: 300,
                        pattern: { mask: "####", allow: /[0-9]/g },
                        validate: (value) => {
                            if (!(this.markersControls["IdentityDocumentSeries"] as any).customValidate()) {
                                return false;
                            }
                            const lenghtValue = value.toString().length;
                            if (lenghtValue === 0) {
                                return true;
                            } else if (lenghtValue === 4) {
                                return true;
                            }
                            return false;
                        },
                        //required: true
                    }
                    : {
                        disabled: true,
                        validate: (value) => {
                            if (!(this.markersControls["IdentityDocumentSeries"] as any).customValidate()) {
                                return false;
                            }
                            return value.toString().length === 0;
                        }
                    };
            });

        this.markersControls["IdentityDocumentNumber"] = MF.MarkerControlfactory.getControl(`${this.viewName}-identityDocumentNumber`,
            this.markersInfo["IdentityDocumentNumber"],
            (content) => {
                return self._isPassport? {
                    labelWidth: 300,
                    pattern: { mask: "######", allow: /[0-9]/g },
                    validate: (value) => {
                        if (!(this.markersControls["IdentityDocumentNumber"] as any).customValidate()) {
                            return false;
                        }
                        const lenghtValue = value.toString().length;
                        if (lenghtValue == 0) {
                            return true;
                        } else if (lenghtValue == 6) {
                            return true;
                        }
                        return false;
                    },
                    //required: true
                }: {
                        validate: (value) => {
                            if (!(this.markersControls["IdentityDocumentNumber"] as any).customValidate()) {
                                return false;
                            }
                            return value.toString().length !== 0;
                        }
                        //required: true
                };
            });

   
        this.markersControls["IdentityDocumentDate"] = MF.MarkerControlfactory.getControl(`${this.viewName}-identityDocumentDate`,
            this.markersInfo["IdentityDocumentDate"],
            (content) => {
                return {
                    labelWidth: 350,
                    //required: true,
                    //format: webix.i18n.dateFormatStr
                };
            });

        this.markersControls["IdentityDocumentDepartment"] = MF.MarkerControlfactory.getControl(`${this.viewName}-identityDocumentDepartment`,
            this.markersInfo["IdentityDocumentDepartment"],
            (content) => {
                 return {
                     labelWidth: 320,
                     //required: true
                 };
            });

        this.markersControls["IdentityDocumentCode"] = MF.MarkerControlfactory.getControl(`${this.viewName}-identityDocumentCode`,
            this.markersInfo["IdentityDocumentCode"],
            (content) => {
                return self._isPassport? {
                        labelWidth: 150,
                        pattern: { mask: "###-###", allow: /[0-9]/g },
                        //required: true
                } :
                    {
                        disabled: true,
                        //required: false
                    }
            });

        const form = $$(this.formId) as webix.ui.form;
        
        form.addView({
            cols: [this.markersControls["IdentityDocumentType"].init(),
                this.markersControls["IdentityDocumentSeries"].init(),
                this.markersControls["IdentityDocumentNumber"].init()]
        });
        form.addView({ cols: [this.markersControls["IdentityDocumentDepartment"].init()] });
        form.addView({
            cols: [this.markersControls["IdentityDocumentCode"].init(),
                this.markersControls["IdentityDocumentDate"].init()]
        });
    }

    private changeConfig() {
        const form = $$(this.formId) as webix.ui.form;
        if (!this._isPassport) {
            webix.extend(this.markersControls["IdentityDocumentSeries"].control.config,
                {
                    labelWidth: 300,
                    disabled: true,
                    //required: false,
                },
                true);

            webix.extend(this.markersControls["IdentityDocumentNumber"].control.config,
                {
                    labelWidth: 300,
                    //validate: (value) => value.toString().length !== 0,
                    //required: true
                },
                true);
            $$(`${this.markersControls["IdentityDocumentNumber"].control.config.id}`)
                .define("pattern",
                {
                     mask: "##################################################################", allow: /[A-Za-z0-9]/g
                });

            webix.extend(this.markersControls["IdentityDocumentCode"].control.config,
                {
                    labelWidth: 150,
                    disabled: true,
                 //   required: false
                },
                true);
            form.elements["IdentityDocumentSeries"].disable();
            form.elements["IdentityDocumentCode"].disable();
        }
        // пасспрот
        else {       
            webix.extend(this.markersControls["IdentityDocumentSeries"].control.config,
                {
                    labelWidth: 300,
                    disabled: false,
                    pattern: { mask: "####", allow: /[0-9]/g },
                    validate: (value) => {
                        if (!(this.markersControls["IdentityDocumentSeries"] as any).customValidate()) {
                            return false;
                        }
                        const lenghtValue = value.toString().length;
                        if (lenghtValue === 0) {
                            return true;
                        } else if (lenghtValue === 4) {
                            return true;
                        }
                        return false;
                      
                    },
                    //required: true
                },
                true);

            webix.extend(this.markersControls["IdentityDocumentNumber"].control.config,
                {
                    labelWidth: 300,
                    disabled: false,
                 //   required: true,
                    validate: (value) => {
                        if (!(this.markersControls["IdentityDocumentNumber"] as any).customValidate()) {
                            return false;
                        }
                        const lenghtValue = value.toString().length;
                        if (lenghtValue === 0) {
                            return true;
                        } else if (lenghtValue === 6) {
                            return true;
                        }
                        return false;
                    }
                },
                true);
            $$(`${this.markersControls["IdentityDocumentNumber"].control.config.id}`)
                .define("pattern",
                    {
                        mask: "######", allow: /[0-9]/g
                    });
            

            webix.extend(this.markersControls["IdentityDocumentCode"].control.config,
                {
                    labelWidth: 150,
                    disabled: false,
                    pattern: { mask: "###-###", allow: /[0-9]/g },
                //    required: true
                },
                true);
            form.elements["IdentityDocumentSeries"].enable();
            form.elements["IdentityDocumentCode"].enable();
        }

        form.elements["IdentityDocumentSeries"].refresh();
        form.elements["IdentityDocumentNumber"].refresh();
        form.elements["IdentityDocumentCode"].refresh();
        this._clear(form);
    }

    private _clear(form) {

        if ((this.markersControls["IdentityDocumentSeries"].control as webix.ui.text).getValue() !== "") {
            (this.markersControls["IdentityDocumentSeries"].control as webix.ui.text).$setValue("");
            this.markersControls["IdentityDocumentSeries"].setValue(this.createEmptyModel(91), this.model);
           
        }

        if ((this.markersControls["IdentityDocumentNumber"].control as webix.ui.text).getValue() !== "") {
            (this.markersControls["IdentityDocumentNumber"].control as webix.ui.text).$setValue("");
            this.markersControls["IdentityDocumentNumber"].setValue(this.createEmptyModel(92), this.model);
        }

        if ((this.markersControls["IdentityDocumentCode"].control as webix.ui.text).getValue() !== "") {
            (this.markersControls["IdentityDocumentCode"].control as webix.ui.text).$setValue("");
            this.markersControls["IdentityDocumentCode"].setValue(this.createEmptyModel(93), this.model);
        }

        if ((this.markersControls["IdentityDocumentDepartment"].control as webix.ui.text).getValue() !== "") {
            (this.markersControls["IdentityDocumentDepartment"].control as webix.ui.text).$setValue("");
            this.markersControls["IdentityDocumentDepartment"].setValue(this.createEmptyModel(94), this.model);
        }
        if ((this.markersControls["IdentityDocumentDate"].control as webix.ui.datepicker).getValue() !== null) {
            (this.markersControls["IdentityDocumentDate"].control as webix.ui.datepicker).$setValue(null);
        }

        const data = ($$(this.formId) as webix.ui.form).getValues() as { [id: string]: any };
        ($$(this.formId) as webix.ui.form).setValues(data);
        ($$(this.formId) as webix.ui.form).clearValidation();
    }

    saveData(data): Promise<any> {
        return Api.ApiPersons.updatePersonInformation(data);
    }

}

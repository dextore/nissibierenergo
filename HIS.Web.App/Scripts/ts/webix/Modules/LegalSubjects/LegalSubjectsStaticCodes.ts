﻿import Base = require("./CommerceActivity/PersonStaticCodesBase");
import PeriodMarkerControlBase = require("../../Components/Markers/PeriodMarkerControlBase");
import MF = require("../../Components/Markers/MarkerControlFactory");
import Api = require("../../Api/ApiExporter");
import viewConfig = webix.ui.viewConfig;

export function getLegalSubjectsStaticCodes(viewName: string): Base.IPersonStaticCodes {
    return new LegalSubjectsStaticCodes(viewName);
}

class LegalSubjectsStaticCodes extends  Base.PersonStaticCodesBase {
    constructor(viewName) {
        super(viewName);
    }

    getFormData() {
        let baseData = super.getFormData();

        const data = ($$(this.formId) as webix.ui.form).getValues() as { [id: string]: any };

        if (this.markersControls["KPP"].isChanged) {
            const markerValue = this.markersControls["KPP"].getValue(data);
                baseData.markerValues.push(markerValue);
        }

        return baseData;
    }

    setFormData(data: Persons.IPersonInfoResponseModel) {
        
        super.setFormData(data);
        const form = ($$(this.formId) as webix.ui.form);

        data.markers.filter(item => item.markerId === 131).forEach(item => {
            this.changeKpp(item);
        });

        data.markers.filter(item => {
            return item.markerId === this.markersInfo["KPP"].id;
        }).forEach(item => {
            this.markersControls["KPP"].setInitValue(item, this.model);
            (this.markersControls["KPP"] as PeriodMarkerControlBase.IPeriodMarkerControl).baId = data.personId;
            });
        
        form.setValues(this.model);
        form.clearValidation();
    }

    ready() {
        super.ready();

        this.markersControls["KPP"] = MF.MarkerControlfactory.getControl(`${this.viewName}-KPP`,
            this.markersInfo["KPP"],
            (content) => {
                return {
                    pattern: { mask: "#########", allow: /[0-9]/g },
                    labelWidth: this.labelWidth,
                    validate: (value) => {
                        if (!(this.markersControls["KPP"] as any).customValidate())
                            return false;
                        return value.toString().length === 9 || value.toString().length === 0;
                    }
            }
            });

        const form = $$(this.formId) as webix.ui.form;
        form.addView(this.markersControls["KPP"].init());
    }

    ogrnValidation(value) {
        return value.toString().length === 13 ||
            value.toString().length === 15 ||
            value.toString().length === 0;
    }

    okpoValidation(value) {
        return value.toString().length === 10
            || value.toString().length === 8
            || value.toString().length === 0;
    }

    getData(): Promise<any> {
        if (!this.personId) {
            return this.emptyDataSource();
        }

        $$(this.formId).enable();
        return Api.ApiLegalPersons.getPersonInformation(this.personId, [101, 102, 103, 104, 106, 131]);
    }

    saveData(data): Promise<Persons.IPersonInfoResponseModel> {
        return Api.ApiLegalPersons.updatePersonInformation(this.personId, data.markerValues);
    }


    //TODO: заглушка на будущее, связанная с неточностью ТЗ по юр лицам. На случай когда ОПф = ИП или может отсутствовать
    changeKpp(item: Markers.IMarkerValueModel): void {
        return;
        //const form = ($$(this.formId) as webix.ui.form);

        //if (item.value.toString() === "5") {
        //    //disable controlls
        //    this.markersControls["KPP"].disable();

        //    this.markersControls["KPP"].control.config = <viewConfig>webix.extend(this.markersControls["KPP"].control.config,
        //        {
        //            disabled: true,
        //        }, true);

        //    //clear kpp
        //    (this.markersControls["KPP"].control as webix.ui.text).setValue("000000000");
        //    this.markersControls["KPP"].isChanged = true;

        //    //save new kpp
        //    let staticdata = this.getFormData();
        //    this.saveData(staticdata).then(result => {
        //        this.changeState(true);
        //        this.setFormData(result);
        //        this.afterChanged.trigger(result, this);
        //    });
            
        //} else if (item.value.toString() === "1") { // ОПФ = ИП
        //    //disable
        //    this.markersControls["KPP"].enable();

        //    this.markersControls["KPP"].control.config = <viewConfig>webix.extend(this.markersControls["KPP"].control.config,
        //        {
        //            disabled: true,
        //            validate: (value) => value.toString().length === 9
        //        },
        //        true);

        //    //clear
        //    (this.markersControls["KPP"].control as webix.ui.text).setValue("000000000");
        //    this.markersControls["KPP"].isChanged = true;

        //    //save
        //    let staticdata = this.getFormData();
        //    this.saveData(staticdata).then(result => {
        //        this.changeState(true);
        //        this.setFormData(result);
        //        this.afterChanged.trigger(result, this);
        //    });
            
        //} 
        //else { 
            //enable
        //    this.markersControls["KPP"].enable();
            
            //this.resetControlsPernament();
            //////clear
            //if ((this.markersControls["KPP"].control as any).getValue() === "000000000") {
            //    (this.markersControls["KPP"].control as webix.ui.text).setValue("000000000");
            //    this.model["KPP"] = "000000000";
            //}
                
            
            //this.markersControls["KPP"].control.config = <viewConfig>webix.extend(this.markersControls["KPP"].control.config,
            //    {
            //        disabled: false,
            //        validate: (value) => {
            //            return value.toString().length === 9 || value.toString().length === 0;
            //        }
            //    }, true);

            ////save
            //let staticdata = this.getFormData();
            //this.saveData(staticdata).then(result => {
            //    this.changeState(true);
            //    this.setFormData(result);
            //    this.afterChanged.trigger(result, this);
            //});
        //}

        //(this.markersControls["KPP"].control as webix.ui.text).refresh();
    }
}
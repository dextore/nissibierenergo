﻿import Base = require("../../../Common/DialogBase");
import Common = require("../../../Common/CommonExporter");
import Btn = require("../../../Components/Buttons/ButtonsExporter");
import Api = require("../../../Api/ApiExporter");
import Component = require("../../../Components/ComponentBase");
import Pager = require("../../../Components/Pager/Pager");


export function getObjectSearchAddressDialog(viewName: string,
    module: Common.MainTabBase): IObjectSearchAddressDialog {
    return new ObjectSearchAddressDialog(viewName, module);
};
export interface IObjectSearchAddressDialog extends Base.IDialog {
    resultModel: Address.IAOAddressesModel;
}

class ObjectSearchAddressDialog extends Base.DialogBase implements IObjectSearchAddressDialog {

    private _resultModel:Address.IAOAddressesModel = null;
    get resultModel() { return this._resultModel; }

    get tableId() { return `${this.viewName}-table-id`; }
    get pagerId() { return `${this.viewName}-pager-id`; }
    get controlId() { return `${this.viewName}-control-id`; }
    get control() { return $$(this.controlId) as webix.ui.text; };
    get tableControl() { return $$(this.tableId) as webix.ui.datatable; }

    constructor(viewName: string,
        private readonly _module: Common.MainTabBase) {
        super(viewName);      
    }
    protected headerLabel(): string {
        return "Поиск адреса";
    }

    protected okBtn = Btn.getOkButton(this.viewName,
        () => {
            this.okBtn.button.disable();
            /// add code
            const selected = this.tableControl.getSelectedItem() as Address.IAOAddressesModel;
            if (!selected) {
                webix.message("Необходимо выбрать адрес из таблицы","error");
                this.okBtn.button.enable();
                return;
            }
            this.okBtn.button.enable();
            this.okClose();
    
        });
    protected cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });

    protected contentConfig() {
        const pager = Pager.getDataTablePager(this.pagerId);
        return {
            rows: [
                this.initSearchControl(),
                {
                    rows: [
                        this.initTable(),
                        pager.init()
                    ]
                },
                {
                    cols: [
                        {},
                        this.okBtn.init(),
                        this.cancelBtn.init()
                    ]
                }
            ]
        };
    }
    protected windowConfig() {
        return {
            autoheight: true,
        };
    }

    ready() {
        webix.extend(this.tableControl, webix.ProgressBar);
    }

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        //this._module.disableCompany();
    }

    destroy(): void {
        super.destroy();
        //this._module.enableCompany();
    }

    private initSearchControl() {

        return {
            view: "extsearch",
            id: this.controlId,
            label: "Строка поиска адреса",
            icons: ["search", "close"],
            css: "protoUI-with-icons-2",
            labelWidth: 150,
            on: {
                onSearchIconClick: () => {
                    this.tableControl.load(this.tableControl.config.url);
                },
                onCloseIconClick: () => {
                    console.log("icon close");
                    this.control.setValue(null);
                    this.tableControl.load(this.tableControl.config.url);
                }
            }
        }
    }

    private initTable() {
        const self = this;
        return {
            view: "datatable",
            id: this.tableId,
            select: "row",
            scroll: "y",
            resizeColumn: true,
            pager: this.pagerId,
            on: {
                onSelectChange: () => {
                    const selected = this.tableControl.getSelectedItem() as Address.IAOAddressesModel;
                    if (selected) {
                        this._resultModel = selected;
                    }
                }
            },
            columns: [
                { id: "name", header: "Адрес", fillspace: true },
                { id: "baId", header: "baId", hidden:true },
                { id: "aoGuid", header: "aoGuid", hidden: true },
                { id: "houseGuid", header: "houseGuid", hidden: true },
                { id: "steadGuid", header: "steadGuid", hidden: true },
                { id: "flatNum", header: "flatNum", hidden: true },
                { id: "flatTypeId", header: "flatTypeId", hidden: true },
                { id: "poBox", header: "poBox", hidden: true },
                { id: "location", header: "location", hidden: true },
                { id: "isBuild", header: "isBuild", hidden: true },
                { id: "isActual", header: "isActual", hidden: true }
            ],
            url: {
                $proxy: true,
                load: (view, callback, params) => {

                    const data = [] as Address.IAOAddressesModel[];
                    self.tableControl.clearAll();
                    if ((self.tableControl as any).showProgress)
                        (self.tableControl as any).showProgress();
                    
                    const textAddress = self.control.getValue();
                    if (!textAddress) {
                        (webix.ajax as any).$callback(view, callback, data);
                        if ((self.tableControl as any).hideProgress)
                            (self.tableControl as any).hideProgress();
                        return;
                    }
                    /*
                    Code.DocumentsListViewCode.getData(view, params,
                            self._columnsInfo,
                            (self._filterData) ? self._filterData.companyId : null,
                            (self._filterData) ? self._filterData.startDate : null,
                            (self._filterData) ? self._filterData.endDate : null)
                        .then(data => {
                            (webix.ajax as any).$callback(view, callback, data);
                            if ((self.dataTableControl as any).hideProgress)
                                (self.dataTableControl as any).hideProgress();
                        }).catch(() => {
                            if ((self.tableControl as any).hideProgress)
                                (self.tableControl as any).hideProgress();
                        });
                    */
                  
                    data.push({
                        baId: 2545244,
                        name: "630099, Россия, Новосибирская обл, Новосибирск г, Орджоникидзе ул, Дом 32",
                        aoGuid: "8A20D276-829C-44E4-8C9A-8A56149B41A2",
                        houseGuid: "D464C67C-123A-42DF-AFA2-0EABAF33F37F",
                        isBuild: 0,
                        isActual: 1
                    } as any);

                    (webix.ajax as any).$callback(view, callback, data);

                    if ((self.tableControl as any).hideProgress)
                        (self.tableControl as any).hideProgress();
                }
            }
        };
    }

}

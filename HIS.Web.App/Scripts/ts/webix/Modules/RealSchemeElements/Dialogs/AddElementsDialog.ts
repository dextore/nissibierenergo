﻿import Base = require("../../../Common/DialogBase");
import Common = require("../../../Common/CommonExporter");
import Btn = require("../../../Components/Buttons/ButtonsExporter");
import Api = require("../../../Api/ApiExporter");
import MB = require("../../../Components/Markers/MarkerControlBase");
import Component = require("../../../Components/ComponentBase");
import MarkerFactory = require("../../../Components/Markers/MarkerControlFactory");
import EntityRefMarkerControl = require("../../../Components/Markers/EntityRefMarkerControl");

export function getAddElementsDialog(viewName: string,
    module: Common.MainTabBase,
    markers: Markers.IMarkerInfoModel[],
    mvcAlias: string): IAddElementsDialog {
    return new AddElementsDialog(viewName, module, markers, mvcAlias);
};
export interface IAddElementsDialog extends Base.IDialog {
}

class AddElementsDialog extends Base.DialogBase {

    get formId() { return `${this.viewName}-form-id`; }

    constructor(viewName: string,
        private readonly _module: Common.MainTabBase,
        private readonly _entityMarkers: Markers.IMarkerInfoModel[],
        private readonly mvcAlias) {
        super(viewName);      
     
    }

    //private markerOrder = [1, 17, 6, 137, 409, 410, 411, 412, 413, 414, 136, 7];
    //private ignoreMarkers = [45];

    protected headerLabel(): string {

        let header = "Создание ";

        switch (this.mvcAlias) {
            case "RealSchemeObject":
                header += "объекта";
                break;
            case "RealSchemeObjectItem":
                header += "помещения";
                break;
            case "RealSchemeRegPoint":
                header += "точки учета";
                break;

        }
        return header;
    }
    get mainTabView(): Common.IMainTabbarView {
        return this._module.mainTabView;
    }
    get formControl() {
        return $$(this.formId) as webix.ui.form;
    }
    protected _fieldsInfo: { [id: string]: Markers.IMarkerInfoModel } = {};
    private _markersControls: { [id: string]: MB.IMarkerControl } = {};
    protected get labelWidth() { return 280; }

   
    protected okBtn = Btn.getOkButton(this.viewName,
        () => {
            //this.okBtn.button.disable();
            if (this.formControl.validate()) {
                this.okClose();
                /*
                const model = this.getFormData() as DocMoveAssets.IDocMoveAssetsData;
                console.log(model);
             
                Api.getApiDocMoveAssets().updateDocMoveAssets(model).then((data) => {
                    this.baId = data;
                    this.okClose();
                });
                */
            }      
            //this.okBtn.button.enable();
        });
    protected cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });

    protected contentConfig() {
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
            ]
        };
    }
    protected windowConfig() {
        return {
            autoheight: true,
            minWidth: 650,
            width: 720

        };
    }
    private checkSize() {
        const rect = $$(this.windowId).getNode().getBoundingClientRect();
        const diff = $$(this.formId).getNode().scrollHeight - $$(this.formId).getNode().clientHeight + 10;
        $$(this.windowId).$setSize(rect.width, rect.height + diff);
        ($$(this.windowId) as webix.ui.window).define("height", rect.height + diff);
    }

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        //this._module.disableCompany(); 
    }

    destroy(): void {
        //super.destroy();
        //this._module.enableCompany(); 
    }

    bindModel(): void {
        this.loadFieldsInfo();
    }
    private loadFieldsInfo() {


        const entityMarkers = this._entityMarkers;
       
        for (const key in entityMarkers) {
            if (entityMarkers.hasOwnProperty(key)) {
                const item = entityMarkers[key];
                const marker = this.createMarkerControl(item);
                if (marker) {
                    this.formControl.addView(marker.init());
                    if ("baId" in marker) {
                        (marker as any).baId = null;
                    }
                    this._fieldsInfo[item.name] = item;

                    if (item.markerType === 11) {
                        (marker as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
                        (marker as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this._module);
                    }
                    this._markersControls[item.name] = marker as any;
                }

            }
        }
        this.formControl.addView({
            cols: [
                {},
                this.okBtn.init(),
                this.cancelBtn.init()
            ]
        });
        this.checkSize();
    }
    private createMarkerControl(model: Markers.IMarkerInfoModel): Component.IComponent {

        const elementId = `${this.viewName}-form`;
        const self = this;
        let isDisabled: boolean = false;

        return MarkerFactory.MarkerControlfactory.getControl(elementId,
            model,
            (self) => {
                return { labelWidth: this.labelWidth, disabled: isDisabled }
            });
    }
   
}

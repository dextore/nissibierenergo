﻿import Common = require("../../Common/CommonExporter");
import Mngr = require("../ModuleManager");
import SearchPanel = require("./Views/ObjectSearchView");
import ObjectView = require("./Views/ObjectView");
import ObjectItemsView = require("./Views/ObjectItemsView");

export class RealSchemeElementModule extends Common.MainTabBase {

    protected static _header = "Элементы";
    protected static _viewName = "real-scheme-element-module";

    private selectedCompany: Inventory.ILSCompanyAreas;
    private _searchPanel: SearchPanel.IObjectSearchPanel;
    private _objectView: ObjectView.IObjectView;
    private _objectItemsView: ObjectItemsView.IObjectItemsView;

    // entity markers 
    constructor(mainTabView: Common.IMainTabbarView) {
        super(mainTabView);
        this._searchPanel = SearchPanel.getObjectSearchPanel(`${this.viewName}-search-panel`, this);
        this._objectView = ObjectView.getObjectView(`${this.viewName}-object-view`, this);
        this._objectItemsView = ObjectItemsView.getObjectItemsView(`${this.viewName}-object-items-view`, this);
        /* /init breadcrumbs */
    }
    systemCompanyHandler(company: Inventory.ILSCompanyAreas) {
        //this.listView.changeCompany(company);
    }
    getContent(): object {
        return {
            view: "scrollview",
            scroll: "Y",
            body: {
                rows: [
                    this._searchPanel.init(),
                    this._objectView.init(),
                    { view: "resizer" },
                    this._objectItemsView.init(),
                ]
            }
        };
    }
    ready() {
        this._objectView.ready();
        this._objectItemsView.ready();
    }
  
}


Mngr.ModuleManager.registerModule(RealSchemeElementModule.viewName, (mainView: Common.IMainTabbarView) => {
    return new RealSchemeElementModule(mainView);
});
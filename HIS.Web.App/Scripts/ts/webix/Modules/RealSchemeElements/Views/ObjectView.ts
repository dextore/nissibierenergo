﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../../Components/ComponentBase");
import Common = require("../../../Common/CommonExporter");
import ObjectForm = require("../Forms/ObjectForm");

export interface IObjectView extends Base.IComponent {
    ready();
}

export function getObjectView(_viewName: string, module: Common.MainTabBase) {
    return new ObjectView(_viewName, module);
}

export class ObjectView extends Base.ComponentBase implements IObjectView {

    constructor(private readonly _viewName: string, private readonly _module: Common.MainTabBase) {
        super();
        this._form = ObjectForm.getObjectForm(`${this._viewName}-object-form` , this._module)
    }
    private _address: Address.IAOAddressesModel = null;
    private _form:ObjectForm.IObjectForm;

    get filterId() {
        return `${this._viewName}-filter-text`;
    };
    get dataViewId() {
        return `${this._viewName}-dataview`;
    };
    get dataViewControl() {
        return $$(this.dataViewId) as webix.ui.dataview;
    };

    ready() {
        this._form.ready();
    }
    
    init() {
        return {
            cols: [
                this.initObjectsList(),
                { view: "resizer" },
                this._form.init()
            ]
        };
    }
    private initObjectsList() {

        return {
            width: 500,
            rows: [
                {
                   
                    view: "toolbar",
                    elements: [
                        {
                            view: "text",
                            id: this.filterId,
                            label: "Фильтр по наименованию",
                            css: "fltr",
                            labelWidth: 190,
                            on: {
                                onTimedKeyPress: () => {
                                    const value = ($$(this.filterId) as webix.ui.text).getValue().toLowerCase();
                                    this.dataViewControl.filter((obj) => {
                                        return obj.name.toLowerCase().indexOf(value) >= 0;
                                    }, value);
                                    if (value != "") {
                                        this._form.reload(null);
                                    }
                                }
                            }
                        }
                    ]
                },
                {
                    view: "dataview",
                    id: this.dataViewId,
                    xCount: 1,
                    yCount: 7,
                    select: true,
                    on: {
                        onSelectChange: () => {
                            const item = this.dataViewControl.getSelectedItem(false);
                            if (!item) {
                                return;
                            }
                            this._form.reload(item.baId);
                            // add code for reload breadcrumbs 

                        }
                    },
                    type: {
                        height:55,
                        width: "auto"
                    },
                    template: "<div class='webix_strong'>#name#</div><div class='webix_strong'>#type#</div>",
                    data: [
                        { baId: 1, name: "Наименование объекта 1", type: "МКД" },
                        { baId: 2, name: "Наименование объекта 2", type: "ЧЖД" },
                        { baId: 3, name: "Наименование объекта 3", type: "МКД" },
                        { baId: 4, name: "Наименование объекта 4", type: "Производственный корпус" },
                        { baId: 5, name: "Наименование объекта 5", type: "Производственный корпус" },
                        { baId: 6, name: "Наименование объекта 6", type: "ЧЖД" },
                        { baId: 7, name: "Наименование объекта 7", type: "МКД" },
                        { baId: 8, name: "Наименование объекта 8", type: "МКД" },
                    ]

                }

            ]
        };

        /*
        return {
            width:500,
            rows: [
                {
                    height: 35,
                    view: "toolbar",
                    elements: [
                        { view: "text", label: "Фильтр по наименованию", css: "fltr", labelWidth: 190 }
                    ]
                },
                {
                    view: "list",
                    id: this.dataViewId,
                    template: "#name# (#type#)",
                    select: true,
                    on: {
                        onSelectChange: () => {
                            const item = this.dataViewControl.getSelectedItem(false);
                            if (!item) {
                                return;
                            }
                            this._form.reload(item.baId);
                          

                        }
                    },
                    data: [
                        { baId: 1, name: "Наименование объекта 1", type: "МКД" },
                        { baId: 2, name: "Наименование объекта 2", type: "ЧЖД" },
                        { baId: 3, name: "Наименование объекта 3", type: "МКД" },
                        { baId: 4, name: "Наименование объекта 4", type: "Производственный корпус" },
                        { baId: 5, name: "Наименование объекта 5", type: "Производственный корпус" },
                        { baId: 6, name: "Наименование объекта 6", type: "ЧЖД" },
                        { baId: 7, name: "Наименование объекта 7", type: "МКД" },

                        ]
                }
             
                ]
        };
        */
        /*
        return {
            view: "dataview",
            id: this.dataViewId,
            xCount: 1,
            yCount: 4,
            select: true,
            width: 600,
            on: {
                onSelectChange: () => {
                    const item = this.dataViewControl.getSelectedItem(false);
                    if (!item) {
                        return;
                    }
                    this._form.reload(item.baId);
                    // add code for reload breadcrumbs 

                }
            },
            type: {
              
                width: "auto"
            },
            template: "<div class='webix_strong'>#name#</div><div class='webix_strong'>#type#</div>",
            data: [
                { baId: 1, name: "Наименование объекта 1", type: "МКД" },
                { baId: 2, name: "Наименование объекта 2", type: "ЧЖД" },
                { baId: 3, name: "Наименование объекта 3", type: "МКД" },
                { baId: 4, name: "Наименование объекта 4", type: "Производственный корпус" },
                { baId: 5, name: "Наименование объекта 5", type: "Производственный корпус" },
                { baId: 6, name: "Наименование объекта 6", type: "ЧЖД" },
                { baId: 7, name: "Наименование объекта 7", type: "МКД" },
            ]
        };
        */

    }


}
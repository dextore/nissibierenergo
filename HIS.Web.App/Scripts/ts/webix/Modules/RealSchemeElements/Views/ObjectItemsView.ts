﻿import Base = require("../../../Components/ComponentBase");
import Common = require("../../../Common/CommonExporter");
import ObjectItemsForm = require("../Forms/ObjectItemForm");
import RegpointForm = require("../Forms/RegPointForm");
import TreeObjectItems = require("../TreeObjectItems");

export interface IObjectItemsView extends Base.IComponent {
    ready();
}

export function getObjectItemsView(_viewName: string, module: Common.MainTabBase) {
    return new ObjectItemsView(_viewName, module);
}

export class ObjectItemsView extends Base.ComponentBase implements IObjectItemsView {

    constructor(private readonly _viewName: string, private readonly _module: Common.MainTabBase) {
        super();
        //this._form = ObjectItemsForm.getObjectItemsForm(`${this._viewName}-object-form`, this._module);
        this._regPointForm = RegpointForm.getRegPointForm(`${this._viewName}-regpoint-form`, this._module);
        this._objectItemsForm = ObjectItemsForm.getObjectItemsForm(`${this._viewName}-object-items-form`, this._module);
        this._treeObjectItems = TreeObjectItems.getBreadCrumbs(`${this._viewName}-tree-items`);
    }
    private _address: Address.IAOAddressesModel = null;
    //private _form: ObjectItemsForm.IObjectItemsForm;
    private _regPointForm: RegpointForm.IRegPointForm;
    private _objectItemsForm: ObjectItemsForm.IObjectItemsForm;
    private _treeObjectItems: TreeObjectItems.ITreeObjectItems; 
    
    ready() {
        this._regPointForm.ready();
        this._objectItemsForm.ready();
        this._treeObjectItems.ready();
        this.initTreeData();
        this.subscribe(this._treeObjectItems.controlEvent.subscribe(this.changeTreeItem, this));

    }

    private changeTreeItem() {

        const selected = this._treeObjectItems.selectedItem;
        if (!selected) {
            return;
        }

        if (selected.baTypeId == 22) {

            this._regPointForm.hidePanel();
            this._objectItemsForm.showPanel();
            this._objectItemsForm.reload(selected.baId);

        } else if (selected.baTypeId == 27) {

            this._regPointForm.showPanel();
            this._objectItemsForm.hidePanel();
            this._regPointForm.reload(selected.baId);

        }
    }

    private initTreeData() {
        const treeData = [] as TreeObjectItems.ITreeModel[] ;
        treeData.push({
            baId: 1,
            name: "Помещение 1",
            parentId: 777,
            address: "Адрес помещения 1",
            baTypeId: 22,
            hasChild: true,
            isRoot: true
        } as TreeObjectItems.ITreeModel);
        treeData.push({
            baId: 2,
            name: "Помещение 2",
            parentId: 1,
            address: "Адрес помещения 2",
            baTypeId: 22,
            hasChild: true,
            isRoot: false
        } as TreeObjectItems.ITreeModel);
        treeData.push({
            baId: 5,
            name: "Точка учета 1",
            parentId: 1,
            address: "Адрес точки учета 1",
            baTypeId: 27,
            hasChild: false,
            isRoot:false
        } as TreeObjectItems.ITreeModel);
        treeData.push({
            baId: 6,
            name: "Точка учета 2",
            parentId: 1,
            address: "Адрес точки учета 2",
            baTypeId: 27,
            hasChild: false,
            isRoot:false
        } as TreeObjectItems.ITreeModel);

        this._treeObjectItems.treeModel = treeData;
        this._treeObjectItems.reload();


    }

    init() {
        return {
            cols: [
                this._treeObjectItems.init(),
                { view: "resizer" },
                {
                    //minHeight:300,
                    rows: [
                        this._regPointForm.init(),
                        this._objectItemsForm.init()
                    ]
                }
            ]
        };
    }

   

}
﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../../Components/ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Common = require("../../../Common/CommonExporter");
import SearchAddress = require("../../../Components/Address/SearchAddress");
import AddDialog = require("../Dialogs/AddElementsDialog");
import BreadCrumbs = require("../BreadCrumbs");

export interface IObjectSearchPanel extends Base.IComponent {
    ready();
    addressModel:Address.IAOAddressesModel;
}

export function getObjectSearchPanel(_viewName: string, module: Common.MainTabBase) {
    return new ObjectSearchPanel(_viewName, module);
}

export class ObjectSearchPanel extends Base.ComponentBase implements IObjectSearchPanel {

    private _objectInfo = Common.entityTypes().getByAliase("RealSchemeObject");
    private _objectItemsInfo = Common.entityTypes().getByAliase("RealSchemeObjectItem");
    private _regPointInfo = Common.entityTypes().getByAliase("RealSchemeRegPoint");

    constructor(private readonly _viewName: string, private readonly _module: Common.MainTabBase) {
        super();
        this._breadCrumbs = BreadCrumbs.getBreadCrumbs(`${this._viewName}-breadcrumbs`)
        /* init breadcrumbs */
        this._breadCrumbs.addItemCrumb({ id: "1", title: "Объект" } as any);
        this._breadCrumbs.addItemCrumb({ id: "2", title: "Помещение" } as any);
        this._breadCrumbs.addItemCrumb({ id: "3", title: "Точка учета" } as any);
    }
    private _breadCrumbs: BreadCrumbs.IBreadCrumbs;
  
    get controlId() {
        return `${this._viewName}-search-control`;
    };
    get addButtonId() {
        return `${this._viewName}-add-button`;
    }

    get contextMenuId() {
        return `${this._viewName}-content-menu`;
    }

    private menuItems: object[] = [
        { id: 1, value: "Объект", icon: "plus" },
        { id: 2, value: "Помещение", icon: "plus" },
        { id: 3, value: "Точка учета", icon: "plus" }
    ];
 

    get control() {
        return $$(this.controlId) as webix.ui.text;
    };

    get addressModel() {
        return this._address;
    };
    private _address: Address.IAOAddressesModel = null;

    ready() {
       
    }
    init() {
        return {
            rows: [
                {
                    view: "toolbar",
                    label: "Поиск по адресу",
                    height: 66,
                    rows: [
                        {
                            cols: [
                                {},
                                this.initSearchControl(),
                                this.initAddControl()
                            ]

                        },
                        this._breadCrumbs.init()
                    ]
                }
            ]
        };
    }
    private initSearchControl() {

        return {
            view: "extsearch",
            id: this.controlId,
            label: "Строка поиска адреса",
            icons: ["search", "close"],
            css: "protoUI-with-icons-2",
            labelWidth:150,
            readonly: true,
            on: {
                onSearchIconClick: () => {
                    // show modal dialog

                    const dialog = SearchAddress.getSearchAddress(`${this._viewName}-search-dialog`);

                    dialog.showModalContent(this._module,
                        () => {
                            this._address = {
                                baId: 2545244,
                                name: "630099, Россия, Новосибирская обл, Новосибирск г, Орджоникидзе ул, Дом 32",
                                aoGuid: "8A20D276-829C-44E4-8C9A-8A56149B41A2",
                                houseGuid: "D464C67C-123A-42DF-AFA2-0EABAF33F37F",
                                isBuild: 0,
                                isActual: 1
                            } as any;

                            if (this._address) {
                                this.control.setValue(this._address.name);
                            }

                            dialog.close();
                        });
                    /*
                    const dialog =SearchDialog.getObjectSearchAddressDialog(`${this._viewName}-search-dialog`, this._module);

                    dialog.showModalContent(this._module,
                        () => {
                            this._address = dialog.resultModel;
                            if (this._address) {
                                this.control.setValue(this._address.name);
                                // reload model 
                            }
                            dialog.close();

                        });
                    */
                },
                onCloseIconClick: () => {
                    this._address = null;
                    this.control.setValue(null);
                }
            }
        }
    }

    private initAddControl() {
        return {
            id: this.addButtonId,
            view: "button",
            type: "iconButton",
            icon: "plus",
            width: 28,
            on: {
                onItemClick: () => {
                    const menu = new ContextMenu(this.contextMenuId, this.menuItems);
                    const node = ($$(this.addButtonId) as webix.ui.button).getNode();
                    menu.show(node, (menuId: string) => {
                        switch (parseInt(menuId)) {
                            case 1:
                                this.createObject();
                                break; 
                            case 2:
                                this.createObjectItems();
                                break;
                            case 3:
                                this.createRegPoint();
                                break;
                        }
                    });
                }
            }
        };
    }

    private createObject() {
        const dialog = AddDialog.getAddElementsDialog(`${this._viewName}-dialog`,
            this._module,
            this._objectInfo.markersInfo as any,
            this._objectInfo.entityInfo.mvcAlias);

        dialog.showModalContent(this._module,
            () => {
                dialog.close();
                webix.message("Создание сущности объект","message");
            });
    }

    private createObjectItems() {
        const dialog = AddDialog.getAddElementsDialog(`${this._viewName}-dialog`,
            this._module,
            this._objectItemsInfo.markersInfo as any,
            this._objectItemsInfo.entityInfo.mvcAlias);

        dialog.showModalContent(this._module,
            () => {
                dialog.close();
                webix.message("Создание сущности помещение", "message");
            });
    }

    private createRegPoint() {
        const dialog = AddDialog.getAddElementsDialog(`${this._viewName}-dialog`,
            this._module,
            this._regPointInfo.markersInfo as any,
            this._regPointInfo.entityInfo.mvcAlias);

        dialog.showModalContent(this._module,
            () => {
                dialog.close();
                webix.message("Создание сущности точка учета", "message");
            });
    }

}



class ContextMenu {

    get popupId() {
        return `${this._viewName}-popup-id`;
    }
    
    constructor(private readonly _viewName , private readonly _menuItems) {

    }

    show(node: any, callback: (id: string) => void) {

        let popup = $$(this.popupId);
        if (!popup)
            popup = this.createMenu(callback);
        popup.show(node);
    }

    private createMenu(callback: (id: string) => void) {
        const popup = webix.ui({
            view: "contextmenu",
            id: this.popupId,
            data: this._menuItems,
            on: {
                onHide: () => {
                    popup.close();
                },
                onItemClick: (id, e, node) => {
                    popup.close();
                    callback(id);
                }
            }
        }) as webix.ui.contextmenu;
        return popup;
    }

  
}
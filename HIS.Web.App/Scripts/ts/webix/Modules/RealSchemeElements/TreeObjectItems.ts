﻿

import Common = require("../../Common/CommonExporter");

export function getBreadCrumbs(viewName: string): ITreeObjectItems {
    return new TreeObjectItems(viewName);
};
export interface ITreeObjectItems {
    init();
    ready();
    generateHtml();
    reload();
    selectedItem: ITreeModel;
    treeModel:ITreeModel[];
    controlEvent: Common.Event<ITreeModel>;
}
export interface ITreeModel {
    baId: number;
    baTypeId: number;
    name: string;
    address: string;
    parentId?: number;
    hasChild: boolean;
    isRoot: boolean;
    controlEvent: Common.Event<ITreeModel>;
}
class TreeObjectItems implements ITreeObjectItems {

    private viewName: string = "";
    get templateId() { return this.viewName; }

    constructor(viewName: string) {
        this.viewName = viewName;
    }
    
    private _activeItem: ITreeModel;
    private _treeModel: ITreeModel[] = [];

    get selectedItem() {
        return this._activeItem;
    }
    set treeModel(model:ITreeModel[]) {
        this._treeModel = model;
    }

    private _controlEvent = new Common.Event<ITreeModel>();
    get controlEvent(): Common.Event<ITreeModel> {
        return this._controlEvent;
    }


    public ready() {
        console.log("tree is ready");
    }


    private getItemById(baId:number): ITreeModel {
        const result = this._treeModel.filter(item => {
            return item.baId == baId;
        });
        return result[0];
    }

    private getItemByParentId(baId: number): ITreeModel[] {
        const result = this._treeModel.filter(item => {
            return item.parentId == baId;
        });
        return result;
    }

    private getRootItem() {
        const result = this._treeModel.filter(item => {
            return item.isRoot == true;
        });
        return result[0];
    }

    public init(): object {
        const self = this;
        return {
            view: "template",
            type: "section",
            css: "tree-without-padding",
            scroll:"Y",
            id: self.templateId,
            height: 350,
            width: 500,
            template: () => {
                return self.generateHtml();
            }, 
            onClick: {
                "treeItem": (ev, id, el) => {
                    this._activeItem = null;
                    this.removeAllSelected();
                    el.classList.add("selected");
                    const selectedId = el.id.replace("tree-objects-item-id-", "");
                    if (selectedId) {
                        this._activeItem = this.getItemById(selectedId);
                        this._controlEvent.trigger(webix.copy(this._activeItem), this);
                     
                    }
                },
                "show_level_down": (ev, id, el) => {
                    webix.message("Будет происходить перерисовка дерева, обновление формы","message");
                }
            
            }
        };
    }

    private removeAllSelected() {

        this._treeModel.forEach(item => {
            const itemId = "tree-objects-item-id-" + item.baId;
            const element = document.getElementById(itemId);
            element.classList.remove("selected");
        });

    }

    public reload() {
        ($$(this.templateId) as webix.ui.template).refresh();
    }
    public generateHtml(): string {
        const items = [] as ITreeModel[];
        items.push(this.getRootItem());
        return "<div class='webix_view rs_elements_tree' style='width:100%; height:100%'>"+this.getTreeHtml(items,0)+"</div>";
    }

    private getTreeHtml(items: ITreeModel[], level:number) {

        if (!items) {
            return "";
        }
        let outHtml = "";
        let skipHtml = "";
        for (var counter = 0; counter <= level; counter++) {
            skipHtml += "<div class='skipTree'></div>";
        }

        items.forEach(item => {

            let iconHtml = "";

            if (item.isRoot) {
                //iconHtml = "<i class='webix_icon far fa-minus-square'></i>";
            } else if (item.hasChild) {
                const idLevelDown = "show_level_down_id_" + item.baId.toString();
                iconHtml = `<i class='webix_icon far fa-plus-square show_level_down' id=${idLevelDown}></i>`;
            } else {
                iconHtml = "<div class='webix_tree_file'></div>";
            }

            outHtml += `<div class='treeItem' id='tree-objects-item-id-${item.baId}'> 
                            ${skipHtml}
                            <div class='tree_icon'>${iconHtml}</div>
                            <div class='tree_info'>
                            <b>${item.name}</b><br/>
                            <b>${item.address}</b>
                            </div>
                        </div>`;
            if (item.hasChild) {
                const child = this.getItemByParentId(item.baId);
                outHtml += this.getTreeHtml(child, level + 1);
            }
        });
        return outHtml
    }

}

﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../../Components/ComponentBase");
import Common = require("../../../Common/CommonExporter");
import ToolButtons = require("../../../Components/Buttons/ToolBarButtons");
import StateEntity = require("../../../Components/Controls/EntityOperationStateControl");
import MarkerControls = require("../../../Components/Markers/MarkerControlBase");
import MarkerFactory = require("../../../Components/Markers/MarkerControlFactory");
import Component = require("../../../Components/ComponentBase");
import EntityRefMarkerControl = require("../../../Components/Markers/EntityRefMarkerControl");

export interface IRegPointForm{
    ready();
    init();
    hidePanel();
    showPanel();
    reload(baId: number);
  
}

export function getRegPointForm(_viewName: string, module: Common.MainTabBase) {
    return new RegPointForm(_viewName, module);
}

class RegPointForm extends Base.ComponentBase implements RegPointForm {

    private _baId: number;
    private _entityAlias = "RealSchemeRegPoint";
    private _entityInfo = Common.entityTypes().getByAliase(this._entityAlias);


    protected _fieldsInfo: { [name: string]: Markers.IMarkerInfoModel } = {};
    private _markersControls: { [name: string]: MarkerControls.IMarkerControl } = {};
    protected get labelWidth() { return 200; }


    private _isEditState: boolean = false;
    private _operationState: StateEntity.IEntityOperationStateControl;
    private _toolbar = {
        save: ToolButtons.getSaveBtn(`${this._viewName}-tooldar`, "Сохранить", this.saveEntity.bind(this)),
        cancel: ToolButtons.getCancelBtn(`${this._viewName}-tooldar`, "Отменить", this.cancelState.bind(this)),
        edit: ToolButtons.getEditBtn(`${this._viewName}-tooldar`, "Изменить", this.editState.bind(this)),
    }
    get panelId() {
        return `${this._viewName}-panel`;
    }
    get formId() {
        return `${this._viewName}-form`;
    }
    get formControl() {
        return $$(this.formId) as webix.ui.form;
    }

    constructor(private readonly _viewName: string, private readonly _module: Common.MainTabBase) {
        super();
        this.initOperationState();
    }

    public hidePanel() {
        $$(this.panelId).hide();
    }

    public showPanel() {
        $$(this.panelId).show();
    }

    private initOperationState(): void {
        this._operationState = StateEntity.getEntityOperationStateControl(`${this._viewName}-operation-state`,
            "",
            () => {
                return true;
            });
        this.subscribe(this._operationState.statusChangedEvent.subscribe((data) => {
            this.resetButtons();  
        }, this));
        this._operationState.addButtons(this._toolbar.save, this._toolbar.cancel, this._toolbar.edit);
    }

    private resetButtons() {
        this._toolbar.save.button.disable();
        this._toolbar.edit.button.disable();
        this._toolbar.cancel.button.disable();
        if (!this._baId) {
            return;
        }

        if (this._isEditState) {
            this._toolbar.save.button.enable();
            this._toolbar.cancel.button.enable();
        } else 
        {
           this._toolbar.edit.button.enable();
        }
    }

    private editState() {
        this._isEditState = true;
        this.resetButtons();
        this.changeFormState();
    }
    private cancelState() {
        this._isEditState = false;
        this.resetButtons();
        this.changeFormState();
    }
    private saveEntity() {
        this._isEditState = false;
        this.resetButtons();
        this.changeFormState();
        webix.message("будет происходить обновление сущности ", "message");

    }

    ready() {
        webix.extend(this.formControl, webix.ProgressBar);
        this.loadFieldsInfo();
        $$(this.panelId).disable();
        $$(this.panelId).hide();
        this.changeFormState();
    }

    init() {
        return {
            id: this.panelId,
            rows: [
                this._operationState.init(),
                this.formInit()
            ]
        };
    }

    private formInit() {
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
            ]
        };

    }

    reload(baId: number) {
        $$(this.panelId).enable();
        this._baId = baId;
        this.showProgress();
        this.resetButtons();
        this.clearForm();
        // get info 
        // set form info 
        this.changeFormState();
        this.hideProgress();
    }
    private showProgress() {

        const form = $$(this.formId) as any;
        if (typeof form.showProgress === "function") {
            form.showProgress();
        }
    }

    private hideProgress() {
        const form = $$(this.formId) as any;
        if (typeof form.hideProgress === "function") {
            form.hideProgress();
        }
    }

    private changeFormState() {

        const readonly = !this._isEditState as boolean;
        const markersControl = this._markersControls;
        for (let key in markersControl) {
            if (markersControl.hasOwnProperty(key)) {
                this._markersControls[key].readOnly = readonly;
            }
        }
        this.formControl.clearValidation();
    }
    private clearForm() {
        const fakeModel: { [id: string]: any } = {};

        for (let key in this._markersControls) {
            if (this._markersControls.hasOwnProperty(key)) {
                const info = this._markersControls[key].markerInfo;
                this._markersControls[key].setInitValue(this.getMarkerValue(info,"",""), fakeModel);

            }
        }
        this.formControl.clearValidation();
    }
    private initForm() {

    }


    private loadFieldsInfo() {

        const entityMarkers = this._entityInfo.markersInfo;
        for (const key in entityMarkers) {
            if (entityMarkers.hasOwnProperty(key)) {
                const item = entityMarkers[key];
                const marker = this.createMarkerControl(item);
                if (marker) {
                    this.formControl.addView(marker.init());
                    if ("baId" in marker) {
                        (marker as any).baId = this._baId;
                    }
                    this._fieldsInfo[item.name] = item;

                    if (item.markerType === 11) {
                        (marker as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
                        (marker as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this._module);
                    }
                    this._markersControls[item.name] = marker as any;
                }
              
            }
        }
    }
    private createMarkerControl(model: Markers.IMarkerInfoModel): Component.IComponent {

        const self = this;
        const elementId = this.formId;
        let isDisabled: boolean = false;

        if (model.isBlocked) {
            isDisabled = true;
        }
        /*
        if (this.isCreate) {
            isDisabled = false;
        } else if (model.isBlocked) {
            isDisabled = true;
        }*/
        return MarkerFactory.MarkerControlfactory.getControl(elementId,
            model,
            (self) => {
                return { labelWidth: this.labelWidth, disabled: isDisabled }
            });
    }
    private getMarkerValue(info: Markers.IMarkerInfoModel, value: string, displayValue: string): Markers.IMarkerValueModel {
        return {
            displayValue: displayValue,
            itemId: null,
            endDate: null,
            markerId: info.id,
            markerType: info.markerType,
            MVCAliase: info.name,
            note: null,
            isVisible: true,
            isBlocked: false,
            startDate: null,
            value: value,
            isDeleted: false
        }
    }

  





}

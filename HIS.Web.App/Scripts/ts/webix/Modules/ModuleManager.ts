﻿import Common = require("../Common/CommonExporter");
import Main = require("../MainView/MainView")

export class ModuleManager {

    private static modules: { [id: string]: (mainForm: Common.IMainTabbarView) => Common.IMainTab} = {};

    static registerModule(moduleName: string, moduleBuilder: (mainForm: Common.IMainTabbarView) => Common.IMainTab) {
        this.modules[moduleName] = moduleBuilder;
    }

    static getNewInstance(moduleName: string, mainView: Common.IMainTabbarView): Common.IMainTab {
        if (this.modules[moduleName]) {
            const instance = this.modules[moduleName](mainView);
            return instance;
        } else {
            return null;
        }
    }
}


﻿export const viewName = "extsearch";


webix.protoUI({
    name: viewName,
    $cssName: "search",
    $renderIcon: function () {
        const config = this.config;
        config.css = "padding-right: 52px;";

        if (config.icons.length) {
            const height = config.aheight - 2 * config.inputPadding;
            const padding = (height - 18) / 2 - 1;
            let pos = 2;
            let html = "";


            for (let i = 0; i < config.icons.length; i++) {
                html += "<span style='right:" + pos + "px;height:"
                    + (height - padding) + "px;padding-top:" + padding
                    + "px;' class='webix_input_icon fa-" + config.icons[i] + "'></span>";
                pos += 24;
            }
            return html;
        }
        return "";
    },
    on_click: {
        "webix_input_icon": function (e, id, node) {
            const name = node.className.substr(node.className.indexOf("fa-") + 3);
            return this.callEvent("on" + name + "IconClick", [e]);
        }
    },
}, webix.ui.search);

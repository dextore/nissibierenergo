﻿import Common = require("../Common/CommonExporter");
import Args = require("./IFormCallBackArgs");


export interface ITabFromSubscriber {
    mainTabView: Common.IMainTabbarView;
    viewId: string;
    formCallback: (arg: Args.IFormCallBackArgs) => void;
}

﻿webix.i18n.setLocale("ru-RU");

webix.protoUI({
        name: "newlayout",
        $setSize: function(x, y) {
            webix.ui.layout.prototype.$setSize.call(this, x, y);
            if (viewId)
                ($$(viewId) as any).callEvent("onResize", []);
            //webix.message("sizing");
        }
    },
    webix.ui.layout);

let viewId = null;

class EventView {

    get windowId() {
        return "event_view_window_id";
    }

    constructor() {
        this.createWindow();
    }

    createWindow() {
        webix.ui({
            view: "window",
            id: this.windowId,
            width: 200,
            height: 200,
            head: false,
            body: {
                template: "Some text"
            }
        });
    } 

    setPosition(left: number, top: number, width: number, height: number) {
        ($$(this.windowId) as webix.ui.window).setPosition(left, top);
        ($$(this.windowId) as webix.ui.window).$setSize(width, height);
    }

    show() {
        $$(this.windowId).show();
    }

    hide() {
        ($$(this.windowId) as webix.ui.window).hide();
    }

    close() {
        ($$(this.windowId) as webix.ui.window).close();
    }

}

class AppView {

    private _viewName = "app_view";

    private _eventView = new EventView();

    get viewId() {
        return `${this._viewName}-id`;
    }

    config() {
        return {
            cols: [
                { template: "left", width: 150 },
                { view: "resizer" },
                {
                    rows: [
                        { template: "top", height: 150 },
                        { view: "resizer" },
                        {
                            view: "newlayout",
                            id: this.viewId,
                            cols: [{}],
                            on: {
                                onResize: () => {
                                    this.setEventPosition();
                                }
                            }
                        },
                        { view: "resizer" },
                        { template: "bottom", height: 150 },
                    ]
                },
                { view: "resizer" },
                { template: "right", width: 150 }
            ],
        }
    }

    setEventPosition() {
        viewId = this.viewId;
        this._eventView.show();
        const rect: ClientRect = $$(this.viewId).getNode().getBoundingClientRect();
        this._eventView.setPosition(rect.left + 10, rect.top + 10, rect.width - 20, rect.height - 20);    
    }
}

class ProtoApp {

    private _appView = new AppView();

    init() {
        webix.ready(() => {
            webix.ui({ rows: [{ template: "Приложение для прототипирования интерфейсов", height: 50 }, this._appView.config()] });
        });
    }
}

const protoApp = new ProtoApp();
protoApp.init();


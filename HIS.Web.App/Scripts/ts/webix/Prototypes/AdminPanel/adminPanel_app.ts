﻿import View = require("./adminPanel_view");

webix.i18n.setLocale("ru-RU");
class AdminPanelApp {
    private _view = View.getAdminPanelView();

    init() {
        webix.ready(() => {
            webix.ui(this._view.init());
        });
    }
}

const adminPanelApp = new AdminPanelApp();
adminPanelApp.init();

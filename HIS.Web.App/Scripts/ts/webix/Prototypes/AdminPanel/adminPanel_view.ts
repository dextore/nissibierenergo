﻿export interface IAdminPanelView {
    init();
}

const viewName = "AdminPanelView";

export function getAdminPanelView(): IAdminPanelView  {
    return new ContractsView();
}

class ContractsView implements IAdminPanelView  {

    get viewName() { return viewName; }
    get viewId() { return `${this.viewName}-view-id`; }
    get toolBarId() { return `${this.viewName}-toolbar-id`; }
    get stateLabelId() { return `${this.viewName}-statelabel-id`; }
    get selectId() { return `${this.viewName}-selectoperation-id`; }
    get datatbleId() { return `${this.viewName}-datatable-id`; }



    init() {
        return {
            view: "multiview",
            cells: [
                this.tabview()
            ]
        }
    }


    tabview() {
        return {
            id: this.viewId,
            rows: [
                {
                    view: "tabview",
                    tabbar: {
                        optionWidth: 150
                    },
                    cells: [
                        {
                            header: "Административная часть",
                            body: {
                                rows: [
                                    this.toolbar(),
                                    this.datatable(),
                                    {view: "resizer"},
                                    this.markersToolbar(),
                                    this.markersDatatable(),
                                    {}
                                ]
                            }
                        }
                    ]
                },
                {}
            ]
        };
    }

    toolbar() {
        return {
            view: "toolbar",
            id: this.toolBarId,
            height: 40,
            elements: [
                {
                    view: "label",
                    label: "Сущности",
                    inputWidth: 100,
                    align: "left"
                },
                {
                    view: "button",
                    type: "iconButton",
                    icon: "plus",
                    width: 28,
                    align: "left",
                    tooltip: "Добавить сущность",
                    on: {
                        onItemClick: (id, e) => {
                            webix.message("Добавить сущность");
                        }
                    }
                },
                {
                    view: "button",
                    type: "iconButton",
                    icon: "trash",
                    width: 28,
                    align: "left",
                    tooltip: "Удалить сущность",
                    on: {
                        onItemClick: (id, e) => {
                            webix.message("Удалить сущность");
                        }
                    }
                }
            ]
        }
    }

    datatable() {
        let data = [{
            TypeID: 1,
            Name: "Lol",
            isNeedHistory: true,
            isImplamentType: true,
            ImplamentTypeName: "lolTable",
            Parrent: "Parrent",
            SearchType: "Форма поиска"
        }, {
            TypeID: 2,
            Name: "Lol2",
            isNeedHistory: false,
            isImplamentType: true,
            ImplamentTypeName: "lolTable",
            Parrent: "Parrent",
            SearchType: "Форма поиска"
        }, {
            TypeID: 3,
            Name: "Lol3",
            isNeedHistory: true,
            isImplamentType: false,
            ImplamentTypeName: "lolTable",
            Parrent: "Parrent",
            SearchType: "Список"
        }, {
            TypeID: 4,
            Name: "Lol4",
            isNeedHistory: true,
            isImplamentType: false,
            ImplamentTypeName: "lolTable",
            Parrent: "Parrent",
            SearchType: "Список"
        }];

        return {
            view: "datatable",
            id: this.datatbleId,
            select: "row",
            resizeColumn: true,
            autoheight: true,
            tooltip: true,
            columns: [
                { id: "TypeID", header: "ID типа", width: 150 },
                { id: "Name", header: ["Наименование", { content: "textFilter" }], width: 100 },
                { id: "Parrent", header: "Родительский тип", width: 150 },
                {
                    id: "isNeedHistory",
                    header: "Признак журналирования",
                    width: 150,
                    template: (obj, common, value, config) => {
                            return common.checkbox(obj, common, value, config);
                    },
                    fillspace: true

                },
                {
                    id: "isImplamentType",
                    header: "Признак реализованного типа",
                    width: 150,
                    template: (obj, common, value, config) => {
                        return common.checkbox(obj, common, value, config);
                    },
                    fillspace: true
                },
                { id: "ImplamentTypeName", header: "Таблица реализованного типа", fillspace: true },
                { id: "SearchType", header: ["Тип поиска", { content: "selectFilter" }], width: 200 },
            ],
            data: data,
        };
    }

    markersToolbar() {
        return {
            view: "toolbar",
            id: `${this.toolBarId}-markers`,
            height: 40,
            elements: [
                {
                    view: "label",
                    label: "Маркеры",
                    inputWidth: 100,
                    align: "left"
                },
                {
                    view: "button",
                    type: "iconButton",
                    icon: "plus",
                    width: 28,
                    align: "left",
                    tooltip: "Добавить маркер",
                    on: {
                        onItemClick: (id, e) => {
                            webix.message("Добавить маркер");
                        }
                    }
                },
                {
                    view: "button",
                    type: "iconButton",
                    icon: "trash",
                    width: 28,
                    align: "left",
                    tooltip: "Удалить маркер",
                    on: {
                        onItemClick: (id, e) => {
                            webix.message("Удалить маркер");
                        }
                    }
                }
            ]
        }
    }

    markersDatatable() {
        let data = [{
            MarkerID: 1,
            Name: "Lol",
            EntityType: "EntityType",
            TypeId: "TypeId"
        }, {
            MarkerID: 2,
            Name: "Lol2",
            EntityType: "EntityType",
            TypeId: "TypeId"
        }, {
            MarkerID: 3,
            Name: "Lol3",
            EntityType: "EntityType",
            TypeId: "TypeId"
        }, {
            MarkerID: 4,
            Name: "Lol4",
            EntityType: "EntityType",
            TypeId: "TypeId"
        }, {}];

        return {
            view: "datatable",
            id: `${this.datatbleId}-markers`,
            select: "row",
            resizeColumn: true,
            autoheight: true,
            tooltip: true,
            columns: [
                { id: "MarkerID", header: "ID маркера", width: 150 },
                { id: "Name", header: ["Наименование", { content: "textFilter" }], width: 100 },
                { id: "TypeId", header: "Тип маркера", width: 150 },
                { id: "EntityType", header: ["Тип сущности", { content: "textFilter" }], width: 200 },

            ],
            data: data,
        };
    }


}
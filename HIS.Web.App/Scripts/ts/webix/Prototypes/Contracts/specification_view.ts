﻿import Markers = require("./specification_markers");

export class SpecificationView {

    private _markers = new Markers.SpecificationMarkers();

    ready() {
        ($$(this.stateLabelId) as webix.ui.label)
            .setHTML(`<span class='his_state_base his_state_colour_green'>Создан</span>`);
        this._markers.ready();
    }

    get stateLabelId() {
        return `specification_view_state_label_id`;
    }

    init() {


        return {
            cols: [
                {
                    maxWidth: 350,
                    rows: [
                        this.toolbarInit(),
                        this.listInit(),
                        {}
                    ]
                },
                { view: "resizer" },
                {
                    gravity: 3,
                    rows: [
                        {
                            view: "tabview",
                            cells: [
                                {
                                    header: "Условя по номенклатуре",
                                    body: {
                                        rows: [
                                            this._markers.init(),
                                            {}
                                        ]
                                    }
                                }, {
                                    header: "Объекты",
                                    body: {
                                        rows: [
                                            this.objectTreeInit(),
                                            {}
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    }

    private toolbarInit() {
        return {
            view: "toolbar",
            height: 40,
            elements: [
                {},
                {
                    view: "label",
                    id: this.stateLabelId,
                    align: "center",
                    label: "Создан",
                    width: 100
                }, {
                    view: "select", //id: this.selectId,
                    options: ["Создан", "Активирован", "Архив"],
                    width: 100
                }, {
                    view: "button",
                    type: "iconButton",
                    icon: "caret-right",
                    width: 28,
                    align: "left",
                    //disabled: true,
                    tooltip: "Выполнить операцию",
                    on: {
                        onItemClick: (id, e) => {
                            webix.message("Изменить состояние договора");
                        }
                    }
                }, {
                    view: "button",
                    type: "iconButton",
                    icon: "plus",
                    width: 28,
                    align: "left",
                    tooltip: "Добавить позицию спецификации",
                    on: {
                        onItemClick: (id, e) => {
                            webix.message("Добавляем номенклатуру!!!");
                        }
                    }
                }, {
                    view: "button",
                    type: "iconButton",
                    icon: "edit",
                    width: 28,
                    align: "left",
                    tooltip: "Изменить позицию спецификации",
                    on: {
                        onItemClick: (id, e) => {
                            webix.message("Изменить номенклатуру!!!");
                        }
                    }
                }
                , {
                    view: "button",
                    type: "iconButton",
                    icon: "history",
                    width: 28,
                    align: "left",
                    tooltip: "История",
                    on: {
                        onItemClick: (id, e) => {
                            webix.message("Показать историю!!!");
                        }
                    }
                }

            ]
        }
    }

    private listInit() {
        return {

            //view: "form",
            //id: this.formId,
            //elements: this.getElements()

            view: "dataview",
            select: true,
            xCount: 1,
            yCount: 0,
            width: 300,
            autoheight: true,
            type: {
                width: 350,
                height: 120
            } , 
            template: "<div class='webix_strong'>#baTypeId# #name#</div>" +
                      "<div><b>c:</b> #startDate#</div>" +
                      "<div><b>по:</b> #endDate#</div>" +
                      "<div><b>состояние:</b> #status#</div>" +
                      "<div><b>создал:</b> #user#</div>",

            //columns: [
            //    { id: "baId", header: "ид", width: 50 },
            //    { id: "name", header: "Наименование", fillspace: true },
            //    { id: "typeName", header: "Тип", width: 200 },
            //    { id: "buyUnitName", header: "ед.изм", width: 100 },
            //    { id: "startDate", header: "Дата начала действия", width: 180 },
            //    { id: "endDate", header: "Дата окончания действия", width: 180 }
            //],
            //autoheight: true,
            //autowidth: true,
            data: dataSource
        }
    }

    private objectTreeInit() {
        return {
            view: "tree",
            select: true,
            data: [
                {
                    id: "element 1",
                    value: "Объект 1",
                    open: true
                }, {
                    id: "element 2",
                    value: "Объект 2",
                    open: true
                }, {
                    id: "element 3",
                    value: "Объект 3",
                    open: true
                }
            ]
        }
    }

}

const dataSource = [
    {
        "baId": 15421, "baTypeId": 1, "name": "Электрическая энергия", "code": "", "caId": 15410, "companyName": "Новосибирскэнергосбыт", "buyUnitId": null, "buyUnitName": "Gb", "saleUnitId": null, "saleUnitName": "Gb", "stockUnitId": null, "stockUnitName": null, "kindId": 0, "kindName": null, "parentId": null, "parentName": null, "typeId": 1, "typeName": "Энергия", "stateId": 1, "stateName": "Создан", "startDate": "01.01.2015", "endDate": "01.01.2015",  
        "status": "Создан", "user": "1"
    },
    {
        "baId": 15425, "baTypeId": 2, "name": "Тепловая энергии", "code": "", "caId": 15410, "companyName": "Новосибирскэнергосбыт", "buyUnitId": null, "buyUnitName": "Теплота", "saleUnitId": null, "saleUnitName": null, "stockUnitId": null, "stockUnitName": null, "kindId": 0, "kindName": null, "parentId": null, "parentName": null, "typeId": 2, "typeName": "Мощность", "stateId": 1, "stateName": "Создан", "startDate": "01.01.2015", "endDate": "01.01.2015",
        "status": "Создан", "user": "1"
    },
    {
        "baId": 1923718, "baTypeId": 3, "name": "Мощность", "code": "", "caId": 15410, "companyName": "Новосибирскэнергосбыт", "buyUnitId": null, "buyUnitName": "Вт", "saleUnitId": null, "saleUnitName": "Вт", "stockUnitId": null, "stockUnitName": null, "kindId": 1, "kindName": "Товар", "parentId": null, "parentName": null, "typeId": 1, "typeName": "Мощность", "stateId": 1, "stateName": "Создан", "startDate": "01.01.2015", "endDate": "01.01.2015",
        "status": "Создан", "user": "1"
    }
]

////    { id: "baId", header: "ид", width: 50 },
////    { id: "name", header: "Наименование", fillspace: true },
////    { id: "typeName", header: "Тип", width: 200 },
////    { id: "buyUnitName", header: "ед.изм", width: 100 },
////    { id: "startDate", header: "Дата начала действия", width: 180 },
////    { id: "endDate", header: "Дата окончания действия", width: 180 }

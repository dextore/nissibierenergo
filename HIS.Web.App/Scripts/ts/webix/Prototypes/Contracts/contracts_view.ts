﻿import ds = require("./data_sources");
import Module = require("./contract_module");

export interface IContractsView {
    init();
    rendered();
}

const viewName = "contracts_view";

export function getContractsView(): IContractsView {
    return new ContractsView();
}

class ContractsView implements IContractsView {

    get viewName() { return viewName; } 
    get viewId() { return `${this.viewName}-view-id`; }
    get toolBarId() {return `${this.viewName}-toolbar-id`;}
    get stateLabelId() {return `${this.viewName}-statelabel-id`;}
    get selectId() { return `${this.viewName}-selectoperation-id`; }
    get datatbleId() { return `${this.viewName}-datatable-id`; }

    private _module = Module.getContractModule(this.viewName, this.viewId);

    rendered() {
        this._module.rendered();
        //($$(this.stateLabelId) as webix.ui.label)
        //    .setHTML(`<span class='his_state_base his_state_colour_green'>Создан</span>`);
    }

    init() {
        return {
            view: "multiview",
            cells: [
                this.tabview(),
                this._module.init()
            ]
        }
    }

    noteTooltip(obj, common) {
        const column = common.column.id;
        return obj[column];
    };

    tabview() {
        return {
            id: this.viewId,
            rows: [
                {},
                {
                    view: "tabview",
                    tabbar: {
                        optionWidth: 150
                    },
                    cells: [
                        {
                            header: "Договора",
                            body: {
                                rows: [
                                    this.toolbar(),
                                    this.datatable()
                                ]
                            }
                        }
                    ]
                },
                {}
            ]
        };
    }

    toolbar() {
        const self = this;
        return {
            view: "toolbar",
            id: this.toolBarId,
            height: 40,
            elements: [
                { },
                {
                    view: "button",
                    type: "iconButton",
                    icon: "plus",
                    width: 28,
                    align: "left",
                    //disabled: true,
                    tooltip: "Добавить договор",
                    on: {
                        onItemClick: (id, e) => {
                            $$(self._module.viewId).show();
                            this._module.addNew();
                        }
                    }
                }
            ]
        }
    }

    datatable() {
        return {
            view: "datatable",
            id: this.datatbleId,
            select: "row",
            resizeColumn: true,
            autoheight: true,
            tooltip: true,
            //autowidth: true,
            columns: [
                { id: "TypeName", header: "Тип", width: 150 },
                { id: "UKGroup", header: "Группа УК", width: 100 },
                { id: "CompanyAreaName", header: ["Поставщик", { content: "textFilter" }], width: 200 },
                { id: "Number", header: "Номер договора", width: 150, template: "<a href='#' class='contract_number_ref'>#Number#</a>" },
                { id: "DocDate", header: "Дата договора", width: 120 },
                { id: "StartDate", header: "Дата начала действия договора", width: 120 },
                { id: "EndDate", header: "Дата окончания действия договора", width: 120 },
                { id: "ContractState", header: ["Состояние", { content: "selectFilter" }], width: 100 },
                { id: "Note", header: "Примечание", fillspace: true, tooltip: (obj, common) => { return this.noteTooltip(obj, common) } }
            ],
            data: ds.contrsctsDs,
            onClick: {
                contract_number_ref: (ev, id) => {
                    //webix.message(id.row, id.column);
                    const datatable = $$(this.datatbleId) as webix.ui.datatable;
                    const rowItem = datatable.getItem(id);
                    if (!rowItem)
                        return;

                    this._module.show(rowItem);
                }
            }
        }
    }
}
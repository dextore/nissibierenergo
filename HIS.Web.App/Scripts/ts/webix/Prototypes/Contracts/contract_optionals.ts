﻿export class ContractOptionals {

    init() {
        return [
            {
                cols: [
                    {
                        view: "text",
                        name: "CargoSender",
                        label: "Грузоотправитель",
                        labelWidth: 250
                    },
                    this.getPeriodDatePicker("CargoSenderName"),
                    this.getPeriodBtn("CargoSenderBtn")
                ]
            }, {
                cols: [
                    {
                        view: "text",
                        name: "CargoReceiver",
                        label: "Грузополучатель",
                        labelWidth: 250
                    },
                    this.getPeriodDatePicker("CargoReceiverName"),
                    this.getPeriodBtn("CargoReceiverBtn")
                ]
            }, {
                cols: [
                    {
                        view: "text",
                        name: "CargoReceiverAddress",
                        label: "Адрес грузополучателя",
                        labelWidth: 250
                    },
                    this.getPeriodDatePicker("CargoReceiverAddressName"),
                    this.getPeriodBtn("CargoReceiverAddressBtn")
                ]
            }, {
                cols: [
                    {
                        view: "search",
                        name: "Recipient",
                        label: "Получатель денежных средств",
                        labelWidth: 250
                    }
                ]
            }, {
                cols: [
                    {
                        view: "search",
                        name: "RecipientAccount",
                        label: "Cчет получателя денежных средств",
                        labelWidth: 250
                    }
                ]
            }, {}
        ]
    }

    private getPeriodDatePicker(name: string) {

        return {
            view: "label",
            align: "left",
            css: { "font-weight": "bold" },
            name: name,
            width: 100
        }
    }

    private getPeriodBtn(messge: string) {
        return {
            view: "button",
            type: "iconButton",
            icon: "angle-double-down",
            width: 28,
            on: {
                onItemClick: (id, e) => { webix.message(messge); }
            }
        };
    }
}
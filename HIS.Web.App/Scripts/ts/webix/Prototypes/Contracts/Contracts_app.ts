﻿import View = require("./contracts_view");

webix.i18n.setLocale("ru-RU");

class ExtSearchApp {

    private _view = View.getContractsView();

    init() {
        webix.ready(() => {
            webix.ui(this._view.init());
            this._view.rendered();
        });
    }
}

const extSearchApp = new ExtSearchApp();
extSearchApp.init();

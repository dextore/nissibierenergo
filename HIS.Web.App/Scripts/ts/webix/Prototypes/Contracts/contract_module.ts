﻿import Models = require("./contract_models");
import Dlg = require("./contract_dlg");
import Options = require("./contract_optionals");
import Spec = require("./specification_view");

export interface IContractModule {
    init();
    show(model: Models.IContractModel);
    rendered();
    addNew();
}

export function getContractModule(viewName: string, listId: string) {
    return new ContractModule(viewName, listId);
}

class ContractModule implements IContractModule {

    get viewName() {
        return `${this._viewName}_module`;
    } 

    get viewId() {
        return `${this.viewName}_view_id`;
    }

    get optionsViewId() {
        return `${this.viewName}_options_view_id`;
    }

    get stateLabelId() {
        return `${this.viewName}_state_label_id`;
    }

    get formId() {
        return `${this.viewName}_form_id`;
    }

    get form() {
        return $$(this.formId) as webix.ui.form;
    }

    constructor(private readonly _viewName: string,
        private readonly _listId: string) {
    }

    rendered() {
        ($$(this.stateLabelId) as webix.ui.label)
            .setHTML(`<span class='his_state_base his_state_colour_green'>Создан</span>`);
        this._spec.ready();
    }

    show(model: Models.IContractModel) {
        this.form.setValues(model);
        $$(this.viewId).show();
        (model.TypeId === 2) ? $$(this.optionsViewId).show() : $$(this.optionsViewId).hide();
    }

    private dialogresult(model: Models.IContractModel) {
        this.form.setValues(model);
    } 

    addNew() {
        const dlg = Dlg.contractDlg(this._viewName);
        dlg.show(null, webix.bind(this.dialogresult, this));
    }

    private _spec = new Spec.SpecificationView();

    init() {
        const labelWidth = 250;
        const options = new Options.ContractOptionals();
        //const spec = new Spec.SpecificationView();
        return {
            id: this.viewId,
            padding: 20,
            cols: [{
                gravity: 4,
                rows: [{
                    view: "toolbar",
                    height: 40,
                    elements: [
                        {}, {
                            view: "label",
                            id: this.stateLabelId,
                            align: "center",
                            label: "Создан",
                            width: 100
                        },
                        {
                            view: "select", //id: this.selectId,
                            options: ["Создан", "Активирован", "Архив"],
                            width: 120
                        },
                        {
                            view: "button",
                            type: "iconButton",
                            icon: "caret-right",
                            width: 28,
                            align: "left",
                            //disabled: true,
                            tooltip: "Выполнить операцию",
                            on: {
                                onItemClick: (id, e) => {
                                    webix.message("Изменить состояние договора");
                                }
                            }
                        },
                        {
                            view: "button",
                            type: "iconButton",
                            icon: "plus",
                            width: 28,
                            align: "left",
                            //disabled: true,
                            tooltip: "Добавить договор",
                            on: {
                                onItemClick: (id, e) => {
                                    const dlg = Dlg.contractDlg(this._viewName);
                                    dlg.show(null, this.dialogresult);
                                }
                            }
                        },
                        {
                            view: "button",
                            type: "iconButton",
                            icon: "edit",
                            width: 28,
                            align: "left",
                            //disabled: true,
                            tooltip: "Изменить договор",
                            on: {
                                onItemClick: (id, e) => {
                                    this.addNew();
                                    //const dlg = Dlg.contractDlg(this._viewName);
                                    //dlg.show(this.form.getValues(), this.dialogresult);
                                }
                            }
                        }
                    ]
                }, {
                    view: "form",
                    id: this.formId,
                    elements: [
                        {
                            cols: [
                                {
                                    view: "label",
                                    label: "Тип",
                                    width: labelWidth
                                }, {
                                    view: "label",
                                    align: "left",
                                    css: { "font-weight": "bold" },
                                    name: "TypeName"
                                }
                            ]
                        }, {
                            cols: [
                                {
                                    view: "label",
                                    label: "Номер договора",
                                    width: 150
                                }, {
                                    view: "label",
                                    align: "left",
                                    css: { "font-weight": "bold" },
                                    name: "Number"
                                }, {
                                    view: "label",
                                    label: "Дата",
                                    width: 50,
                                }, {
                                    view: "label",
                                    align: "left",
                                    css: { "font-weight": "bold" },
                                    name: "DocDate"
                                }, {
                                    view: "label",
                                    label: "Дата начала действия",
                                    width: 200,
                                }, {
                                    view: "label",
                                    align: "left",
                                    css: { "font-weight": "bold" },
                                    name: "StartDate"
                                }, {
                                    view: "label",
                                    label: "Дата окончания действия",
                                    width: 200,
                                }, {
                                    view: "label",
                                    align: "left",
                                    css: { "font-weight": "bold" },
                                    name: "EndDate"
                                }
                            ]
                        }, {
                            cols: [
                                {
                                    view: "label",
                                    label: "Поставщик",
                                    width: labelWidth
                                }, {
                                    view: "label",
                                    align: "left",
                                    css: { "font-weight": "bold" },
                                    name: "CompanyAreaName"
                                }
                            ]
                        },
                        {
                            cols: [
                                {
                                    view: "label",
                                    label: "Покупатель",
                                    width: labelWidth,
                                }, {
                                    view: "label",
                                    align: "left",
                                    css: { "font-weight": "bold" },
                                    name: "LegalSubjectName"
                                }
                            ]
                        },
                        //{
                        //    cols: [{
                        //        view: "label",
                        //        label: "Дата договора",
                        //        width: labelWidth,
                        //    }, {
                        //        view: "label",
                        //        align: "left",
                        //        css: { "font-weight": "bold" },
                        //        name: "DocDate"
                        //    }]
                        //},
                        //{
                        //    cols: [{
                        //        view: "label",
                        //        label: "Дата начала действия договора",
                        //        width: labelWidth,
                        //    }, {
                        //        view: "label",
                        //        align: "left",
                        //        css: { "font-weight": "bold" },
                        //        name: "StartDate"
                        //    }]
                        //},
                        {
                            id: this.optionsViewId,
                            rows: [
                                {
                                    cols: [
                                        {
                                            view: "label",
                                            label: "Группа УК",
                                            width: labelWidth,
                                        }, {
                                            view: "label",
                                            align: "left",
                                            ////css: { "font-weight": "bold" },
                                            name: "UKGroup"
                                        },
                                        this.getPeriodDatePicker("UKGroupDate"),
                                        this.getPeriodBtn("История грузоотправитель")
                                    ]
                                }, {
                                    cols: [
                                        {
                                            view: "label",
                                            label: "Грузоотправитель",
                                            width: labelWidth,
                                        }, {
                                            view: "label",
                                            align: "left",
                                            css: { "font-weight": "bold" },
                                            name: "CargoSender"
                                        },
                                        this.getPeriodDatePicker("CargoSenderDate"),
                                        this.getPeriodBtn("История грузоотправитель")
                                    ]
                                }, {
                                    cols: [
                                        {
                                            view: "label",
                                            label: "Грузополучатель",
                                            width: labelWidth,
                                        }, {
                                            view: "label",
                                            align: "left",
                                            css: { "font-weight": "bold" },
                                            name: "CargoReceiver"
                                        },
                                        this.getPeriodDatePicker("CargoReceiverDate"),
                                        this.getPeriodBtn("История грузоотправитель")
                                    ]
                                }, {
                                    cols: [
                                        {
                                            view: "label",
                                            label: "Грузополучатель",
                                            width: labelWidth,
                                        }, {
                                            view: "label",
                                            align: "left",
                                            css: { "font-weight": "bold" },
                                            name: "CargoReceiverAddress"
                                        },
                                        this.getPeriodDatePicker("CargoReceiverAddressDate"),
                                        this.getPeriodBtn("История грузополучатель")
                                    ]
                                }, {
                                    cols: [
                                        {
                                            view: "label",
                                            label: "Грузополучатель",
                                            width: labelWidth,
                                        }, {
                                            view: "label",
                                            align: "left",
                                            css: { "font-weight": "bold" },
                                            name: "Recipient"
                                        },
                                        this.getPeriodDatePicker("RecipientDate"),
                                        this.getPeriodBtn("Выбор получатель денежных средств")
                                    ]
                                }, {
                                    cols: [
                                        {
                                            view: "label",
                                            label: "Cчет получателя денежных средств",
                                            width: labelWidth,
                                        }, {
                                            view: "label",
                                            align: "left",
                                            css: { "font-weight": "bold" },
                                            name: "RecipientAccount"
                                        },
                                        this.getPeriodDatePicker("RecipientAccountDate"),
                                        this.getPeriodBtn("Выбор счет получателя денежных средств")
                                    ]
                                }
                            ]
                        },
                        {
                            rows: [
                                {
                                    view: "label",
                                    label: "Примечание",
                                    align: "left"
                                },
                                {
                                    view: "label",
                                    name: "Note",
                                    height: 80
                                }
                            ]
                        }
                    ]
                }, {
                    view: "tabview",
                    cells: [
                        {
                            header: "Договорные условия",
                            body: {
                                rows: options.init()
                            }
                        }, {
                            header: "Спецификация",
                            body: this._spec.init()
                        }
                    ]
                }, {
                        view: "toolbar",
                        height: 40,
                        elements: [
                            {}, {
                                view: "button",
                                width: 100,
                                align: "left",
                                value: "Вернуться",
                                on: {
                                    onItemClick: (id, e) => {
                                        $$(this._listId).show();
                                    }
                                }
                            }
                        ]
                    }]
                },
                { view: "resizer" },
                this.treeInit()
            ]
        }
    }

    private treeInit() {
        return {
            view: "tree",
            select: true,
            data: [
                {
                    id: "root",
                    value: "Последние",
                    open: true,
                    data: [
                        {
                            id: "1", open: true, value: "Иванов Петр Сергеевич", data: [
                                { id: "1.1", value: "договор №-01 Новосибирскэнергосбыт" }
                            ]
                        },
                        {
                            id: "2", value: "ООО СИБМОСТ", open: true, data: [
                                { id: "2.1", value: "договор №-201 Новосибирскэнергосбыт" }
                            ]
                        }
                    ]
                }
            ]
        }
    }

    private getPeriodDatePicker(name: string) {

        return {
            view: "label",
            align: "left",
            css: { "font-weight": "bold" },
            name: name,
            width: 100
        }
    }

    private getPeriodBtn(messge: string) {
        return {
            view: "button",
            type: "iconButton",
            icon: "angle-double-down",
            width: 28,
            on: {
                onItemClick: (id, e) => { webix.message(messge); }
            }
        };
    }

}
﻿export class SpecificationMarkers {

    private formData = {};

    init() {

        return {
            rows: [
                this.initToolBar(),
                this.initList(this.formData)
            ]
        }
    }

    ready() {
        ($$(this.formId) as webix.ui.form).setValues(this.formData);
    }

    private initToolBar() {
        return {
            view: "toolbar",
            height: 40,
            elements: [
                {},
                {
                    view: "button",
                    type: "iconButton",
                    icon: "save",
                    width: 28,
                    align: "left",
                    tooltip: "Сохранить",
                    on: {
                        onItemClick: (id, e) => {
                            webix.message("Добавляем номенклатуру!!!");
                        }
                    }
                }, {
                    view: "button",
                    type: "iconButton",
                    icon: "ban",
                    width: 28,
                    align: "left",
                    tooltip: "Отказаться",
                    on: {
                        onItemClick: (id, e) => {
                            webix.message("Удалить номенклатуру!!!");
                        }
                    }
                }, {
                    view: "button",
                    type: "iconButton",
                    icon: "edit",
                    width: 28,
                    align: "left",
                    tooltip: "Изменить",
                    on: {
                        onItemClick: (id, e) => {
                            webix.message("Изменить!!!");
                        }
                    }
                }
            ]
        }
    }


    get formId() {
        return "contract_spec_condition_form_id";
    }

    private initList(mode: {}) {
        return {
            view: "form",
            id: this.formId,
            elements: this.getElements(mode)


            //view: "datatable",
            //select: "row",
            //resizeColumn: true,
            ////scrollX: true,
            //columns: [
            //    { id: "caption", header: "Наименование", fillspace: true },
            //    { id: "startDate", header: "Дата начала", width: 200 },
            //    { id: "endDate", header: "Дата окончания", width: 200 },
            //    { id: "value", header: "Значение", width: 200 },
            //    { id: "note", header: "Примечания", fillspace: true }
            //],
            //data: dataSource
        }
    }

    private getElements(model: {}) {
        return formData.map(item => {
            return this.initMakrerValue(item, model);
        }, this);
    }

    private initMakrerValue(info: IMarkerInfo, model: {}) {
        model[info.name] = info.value;
        model[`${info.name}_date`] = info.startDate;
        return {
            cols: [{
                    view: "label",
                    id: `${info.name}-id`,
                    align: "left",
                    label: info.title,
                    width: 180
                }, {
                    view: "text",
                    name: info.name//,
                    //width: 120
                }, {
                    view: "datepicker",
                    name: `${info.name}_date`,
                    width: 120
                }, {
                    view: "button",
                    type: "iconButton",
                    icon: "angle-double-down",
                    width: 28,
                    align: "left",
                    tooltip: "История маркера",
                    on: {
                        onItemClick: (id, e) => {
                            webix.message(`История маркера: ${info.title}`);
                        }
                    }
                }, {
                    view: "button",
                    type: "iconButton",
                    icon: "info-circle",
                    width: 28,
                    align: "left",
                    tooltip: "Переопределние маркера",
                    on: {
                        onItemClick: (id, e) => {
                            webix.message(`Переопределние маркера: ${info.title}`);
                        }
                    }
                }
            ]
        }
    }

    //Действие договора	ContractActivity
    //Состояние договора	ContractState
    //Дата вступления в силу договора	ContractValidate
    //Расчет по договору	ContractCalc
    //Дата окончательного расчета по договору	ContractFinalPayment
    //Схема авансов по договору	ContractAdvance
}


interface IMarkerInfo {
    name: string,
    title: string,
    startDate: Date,
    endDate: Date,
    value: string,
    note: string
}

const dataSource = [
    { "name": "ContractActivity", "caption": "Действие договора", "startDate": "01.01.2018", "endDate": "01.01.2019", "value": "---", "note": "--"},
    { "name": "ContractState", "caption": "Состояние договора", "startDate": "01.01.2018", "endDate": "01.01.2019", "value": "---", "note": "--" },
    { "name": "ContractValidate", "caption": "Дата вступления в силу договора", "startDate": "01.01.2018", "endDate": "01.01.2019", "value": "---", "note": "--" },
    { "name": "ContractCalc", "caption": "Расчет по договору", "startDate": "01.01.2018", "endDate": "01.01.2019", "value": "---", "note": "--" },
    { "name": "ContractFinalPayment", "caption": "Дата окончательного расчета по договору", "startDate": "01.01.2018", "endDate": "01.01.2019", "value": "---", "note": "--" },
    { "name": "ContractAdvance", "caption": "Схема авансов по договору", "startDate": "01.01.2018", "endDate": "01.01.2019", "value": "---", "note": "--"}
];   


const formData = [
    {
        name: "ContractActivity",
        title: "Действие договора",
        startDate: new Date("01.01.2015"),
        endDate: new Date("01.01.2018"),
        value: "Значение",
        note: "Примечания"
    },
    {
        name: "ContractState",
        title: "Состояние договора",
        startDate: new Date("01.01.2015"),
        endDate: new Date("01.01.2018"),
        value: "Значение",
        note: "Примечания"
    },
    {
        name: "ContractValidate",
        title: "Дата вступления в силу договора",
        startDate: new Date("01.01.2015"),
        endDate: new Date("01.01.2018"),
        value: "Значение",
        note: "Примечания"
    },
    {
        name: "ContractCalc",
        title: "Расчет по договору",
        startDate: new Date("01.01.2015"),
        endDate: new Date("01.01.2018"),
        value: "Значение",
        note: "Примечания"
    },
    {
        name: "ContractFinalPayment",
        title: "Дата окончательного расчета по договору",
        startDate: new Date("01.01.2015"),
        endDate: new Date("01.01.2018"),
        value: "Значение",
        note: "Примечания"
    },
    {
        name: "ContractAdvance",
        title: "Схема авансов по договору",
        startDate: new Date("01.01.2015"),
        endDate: new Date("01.01.2018"),
        value: "Значение",
        note: "Примечания"
    }
]
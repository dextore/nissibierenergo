﻿import Models = require("./contract_models");
import Form = require("./contract_form");

export interface IContarctDlg {
    show(model: Models.IContractModel, calback: (model: Models.IContractModel) => any);
    close();
}

export function contractDlg(viewName: string): IContarctDlg {
    return new ContractDlg(viewName);
}

class ContractDlg implements IContarctDlg {

    get viewName() { return `${this._viewName}-window` }

    private _form = Form.getContractForm(this.viewName);
    private _calback: (model: Models.IContractModel) => any;

    get windowId() { return `${this.viewName}-id` }

    get window() { return $$(this.windowId) as webix.ui.window; }

    constructor(private readonly _viewName: string) {

    }

    show(model: Models.IContractModel, calback: (model: Models.IContractModel) => any) {
        this._calback = calback;

        const rect =  document.body.getBoundingClientRect();
        const width = rect.width - 100;
        const height = rect.height - 100;

        this.createWindow(width, height);
        this._form.setData(model);
        this.window.show();
    }

    close() {
        if (this.window)
            this.window.close();
    }

    createWindow(width: number, height: number) {
        webix.ui({
            view: "window",
            id: this.windowId,
            height: height,
            width: width,
            position: "center",
            head: {
                view: "toolbar",
                margin: -4,
                visibleBatch: 1,
                cols: this.getHeaderButtons()
            },
            body: {
                rows: [
                    this.windowContent(),
                    this.futerButtons()
                ]
            }
        });
    }

    private getHeaderButtons() {
        return [
                { view: "label", label: "Договор" },
                { view: "icon", icon: "times", click: () => { this.close(); } }
            ];
    }

    windowContent() {
        return this._form.init();
    }

    futerButtons() {
        return {
            view: "toolbar",
            height: 45,
            padding: 5,
            cols: [
                {}, {
                    view: "toolbar",
                    height: 40,
                    elements: [
                        {}, {
                            view: "button",
                            width: 100,
                            align: "left",
                            value: "Принять",
                            on: {
                                onItemClick: (id, e) => {
                                    const model = this._form.getData();
                                    if (this._calback)
                                        this._calback(model);
                                    this.close();
                                }
                            }
                        }, {
                            view: "button",
                            width: 100,
                            align: "left",
                            value: "Отказаться",
                            on: {
                                onItemClick: (id, e) => {
                                    this.close();
                                }
                            }
                        }
                    ]
                }
            ]
        } 
    }

}
﻿import Models = require("./contract_models");

export interface IContractForm {
    setData(model: Models.IContractModel);
    getData(): Models.IContractModel;
}

export function getContractForm(viewName: string) {
    return new ContractForm(viewName);
}

class ContractForm implements IContractForm {

    get viewId() {
        return `${this._viewName}_view_id`;
    }

    get optionsViewId() {
        return `${this._viewName}_options_view_id`;
    }

    get formId() {
        return `${this._viewName}_form_id`;
    }

    get form() {
        return $$(this.formId) as webix.ui.form;
    }

    constructor(private readonly _viewName: string) {
    }

    setData(model: Models.IContractModel) {
        const data = (!model) ? {} : model;
        this.form.setValues(data);
        (model && model.TypeId === 2) ? $$(this.optionsViewId).show() : $$(this.optionsViewId).hide();
    }

    getData(): Models.IContractModel {
        return this.form.getValues();
    }

    init() {

        const labelWidth = 250;

        return {
            id: this.viewId,
            padding: 100,
            rows: [
                {
                    view: "form",
                    id: this.formId,
                    elements: [
                        {
                            cols: [
                                {
                                    view: "select",
                                    label: "Тип",
                                    //labelAlign: "right",
                                    name: "TypeId",
                                    value: 1,
                                    labelWidth: 40,
                                    options: [
                                        { "id": 1, "value": "Договор ФЛ" },
                                        { "id": 2, "value": "Договор ЮЛ" }
                                    ],
                                    on: {
                                        onChange: (newv, oldv) => {
                                            (Number(newv) === 2)
                                                ? $$(this.optionsViewId).show()
                                                : $$(this.optionsViewId).hide();
                                        }
                                    }
                                },
                                {
                                    view: "select",
                                    name: "UKGroup",
                                    label: "Группа УК",
                                    labelAlign: "right",
                                    labelWidth: 150,
                                    options: [
                                        { "id": 1, "value": "Группа УК1" },
                                        { "id": 2, "value": "Группа УК2" }
                                    ]
                                },
                                this.getPeriodDatePicker("UKGroupDate"),
                                this.getPeriodBtn("История Группа УК")
                            ]
                        },
                        {
                            cols:[{ view: "text", label: "Номер договора", name: "Number", labelWidth: 120 },
                                {
                                    view: "datepicker",
                                    label: "Дата договора",
                                    labelAlign: "right",
                                    name: "DocDate",
                                    labelWidth: 110
                                },
                                {
                                    view: "datepicker",
                                    label: "Дата начала действия договора",
                                    labelAlign: "right",
                                    name: "StartDate",
                                    labelWidth: labelWidth
                                },
                                {
                                    view: "datepicker",
                                    label: "Дата окончания действия договора",
                                    labelAlign: "right",
                                    name: "EndDate",
                                    labelWidth: labelWidth
                                },
                            ]
                        },{
                            view: "search",
                            label: "Поставщик",
                            name: "CompanyAreaName",
                            labelWidth: 100,
                            on: {
                                onSearchIconClick: (e) => { webix.message("Выбор поставщика"); }
                            }
                        },{
                            view: "search",
                            label: "Покупатель",
                            name: "LegalSubjectName",
                            labelWidth: 100,
                            on: {
                                onSearchIconClick: (e) => { webix.message("Выбор покупателя"); }
                            }
                        }, {
                            id: this.optionsViewId,
                            rows: [{
                                    cols: [{
                                        view: "search", name: "CargoSender",
                                        label: "Грузоотправитель",
                                        labelWidth: labelWidth,
                                        on: {
                                            onSearchIconClick: (e) => { webix.message("Выбор грузоотправитель"); }
                                        }
                                    },
                                    this.getPeriodDatePicker("CargoSenderDate"),
                                    this.getPeriodBtn("История грузоотправитель")
                                    ]
                                }, {
                                    cols: [{
                                        view: "search", name: "CargoReceiver",
                                        label: "Грузополучатель",
                                        labelWidth: labelWidth,
                                        on: {
                                            onSearchIconClick: (e) => { webix.message("Выбор грузополучатель"); }
                                        }
                                    },
                                    this.getPeriodDatePicker("CargoReceiverDate"),
                                    this.getPeriodBtn("История грузополучатель")]
                                }, {
                                    cols: [
                                        {
                                            view: "search", name: "CargoReceiverAddress",
                                            label: "Адрес грузополучателя",
                                            labelWidth: labelWidth,
                                            on: {
                                                onSearchIconClick: (e) => {
                                                    webix.message("Выбор адрес грузополучателя");
                                                }
                                            }
                                        },
                                        this.getPeriodDatePicker("CargoReceiverAddressDate"),
                                        this.getPeriodBtn("История адрес грузополучателя")]
                                }, {
                                    cols: [
                                        {
                                            view: "search", name: "Recipient",
                                            label: "Получатель денежных средств",
                                            labelWidth: labelWidth,
                                            on: {
                                                onSearchIconClick: (e) => {
                                                    webix.message("Выбор получатель денежных средств");
                                                }
                                            }
                                        },
                                        this.getPeriodDatePicker("RecipientDate"),
                                        this.getPeriodBtn("История получатель денежных средств")]
                                }, {
                                    cols: [{
                                        view: "search", name: "RecipientAccount",
                                        label: "Cчет получателя денежных средств",
                                        labelWidth: labelWidth,
                                        on: {
                                            onSearchIconClick: (e) => {
                                                webix.message("Выбор счет получателя денежных средств");
                                            }
                                        }
                                    },
                                    this.getPeriodDatePicker("RecipientAccountDate"),
                                    this.getPeriodBtn("История счет получателя денежных средств")]
                                }
                            ]
                        }, 
                        { view: "textarea", name: "Note", label: "Примечание", labelPosition: "top", height: 80 },
                        {}
                    ]
                }
            ]
        }
    } 


    private getPeriodDatePicker(name: string) {
        return {
            view: "datepicker",
            name: "UKGroupDate",
            width: 120
        }
    }

    private getPeriodBtn(messge: string) {
        return {
            view: "button",
            type: "iconButton",
            icon: "angle-double-down",
            width: 28,
            on: {
                onItemClick: (id, e) => { webix.message(messge); }
            }
        };
    }

}
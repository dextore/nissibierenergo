﻿export interface IStatndartSearchView {
    show();
    isVisible: boolean;
    init(): any;
}

export function getStatndartSearchView(): IStatndartSearchView {
    return new StatndartSearchView();
}

const viewName = "standart-search-view";

class StatndartSearchView implements IStatndartSearchView {

    get viewId() {
        return `${viewName}-id`;
    }

    show() {
        ($$(this.viewId) as webix.ui.view).show();
    }

    get isVisible(): boolean {
        return ($$(this.viewId) as webix.ui.view).isVisible();
    }

    init() {
        return {
            id: this.viewId,
            padding: 10,
            rows: [
                {
                    height: 30,
                    cols: [
                        {
                            width: 50
                        }, {
                            view: "search",
                            placeholder: "Значение для поиска...",
                            on: {
                                onItemClick: () => {
                                    webix.message("Поиск");
                                } 
                            }
                        }, {

                            view: "button",
                            type: "icon",
                            icon: "search",
                            width: 30
                        }, {
                            width: 50
                        }
                    ]
                }, { height: 15}, {
                    cols: [
                        { width: 10 },
                        {
                            view: "datatable",
                            columns: [
                                { id: "Code", header: "№", width: 50 },
                                { id: "name", header: "Наименование", fillspace: true },
                                { id: "inn", header: "ИНН", width: 200 }
                            ],
                            data: []
                        },
                        { width: 10 }
                    ]
                }
            ]
        }
    }
}
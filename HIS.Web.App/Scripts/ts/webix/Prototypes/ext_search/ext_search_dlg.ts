﻿import View = require("./ext_search_view");

export interface IExtSearchDlg {
    show();
}

export function getExtSearchDlg() {
    return new ExtSearchDlg();
}

class ExtSearchDlg implements IExtSearchDlg {

    private _view = View.getExtSearchView();
    private _dlg: webix.ui.window;

    show() {
        const rect = {
            x: 0, y: 0,
            width: document.body.clientWidth - 70 * 2,
            height: document.body.clientHeight - 70 * 2
        };
        this.createWindow(rect.width, rect.height);
        //this._dlg.define("width", rect.width);
        //this._dlg.define("height", rect.height);
        //(this._dlg as any).refresh();
        (this._dlg as any).show({
            x: rect.x + this._dlg.config.padding,
            y: rect.y + this._dlg.config.padding
        });


        //this._dlg.config
        //this._dlg.$setSize(rect.width - (this._dlg.config.padding * 2), rect.height - (this._dlg.config.padding * 2));
        this._dlg.show();

    }

    private cancel() {
        this._dlg.close();
        this._dlg = null;
    }

    private ok() {
        this._dlg.close();
        this._dlg = null;
    }

    private createWindow(width: number, height: number) {
        try {
            this._dlg = webix.ui(this.init(width, height)) as webix.ui.window;
        } catch (err) {
            console.log(err);
        }
    }

    private init(width: number, height: number) {
        return {
            view: "window",
            //: self.windowId,
            position: "center",
            autoheight: true,
            padding: 80,
            width: width,
            height: height,
            head: {
                view: "toolbar",
                margin: -4,
                cols: [
                    { view: "label", label: "Расширенный поиск" },
                    {
                        view: "icon",
                        icon: "times-circle",
                        click: () => { this.cancel(); }
                    }
                ]
            },
            modal: true,
            //fullscreen: true,
            move: true,
            resize: true,
            body: {
                autoheight: true,
                rows: [
                    this._view.init(),
                    {
                    view: "toolbar",
                    height: 40,
                    cols: [
                        {},{
                            view: "button", label: "Выбрать", align: "center", width: 120,
                            click: () => {
                                this.ok();
                            }
                        },{
                            view: "button", label: "Отказаться", align: "center", width: 120,
                            click: () => {
                                this.cancel();
                            }
                        }
                    ]
                }]
            },
        };
    }
}
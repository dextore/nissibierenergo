﻿export interface ICustomSearchView {
    show();
    isVisible: boolean;
    init(): any;
}

export function getCustomSearchView(): ICustomSearchView {
    return new CustomSearchView();
}

const viewName = "custom-search-view";

class CustomSearchView implements ICustomSearchView {

    get viewId() {
        return `${viewName}-id`;
    }

    show() {
        ($$(this.viewId) as webix.ui.view).show();
    }

    get isVisible(): boolean {
        return ($$(this.viewId) as webix.ui.view).isVisible();
    }

    init() {
        return {
            id: this.viewId, 
            padding: 10,
            rows: [
                {
                    padding: 10,
                    rows: [
                        {
                            view: "toolbar",
                            height: 40,
                            cols: [
                                {
                                    rows: [
                                        {height: 5},
                                        {
                                            view: "radio",
                                            align: "center",
                                            width: 150,
                                            value: 1,
                                            options: [
                                                { "id": 1, "value": "И" },
                                                { "id": 2, "value": "ИЛИ" }
                                            ]
                                        }
                                    ]
                                }, {
                                    width: 20,
                                },{
                                    view: "combo",
                                    id: "field_t",
                                    label: "Искать",
                                    value: "1",
                                    width: 400,
                                    options: [
                                        { id: 1, value: "Физические лица" },
                                        { id: 2, value: "Юридические лица" }
                                    ]
                                }, {
                                    width: 20 
                                }, {
                                    view: "button",
                                    id: "my_button",
                                    label: "Выбрать маркеры",
                                    type: "icon",
                                    icon: "edit",
                                    width: 150 
                                }, {

                                }
                            ]
                        }, {
                            borderless: true,
                            view: "accordion",
                            type: "space",
                            cols: [
                                {
                                    gravity: 2,
                                    body: "Значения маркеров для фильтра" 

                                }, {
                                    view: "resizer"
                                },
                                {
                                    header: "Выбор маркера",
                                    body: "Список маркеров",
                                    collapsed: true
                                    //width: 500
                                }
                            ]
                        }
                    ]
                },{
                    view: "resizer"
                },{
                    view: "datatable",
                    columns: [],
                    data: []
                }
            ]                       
        }
    }
}
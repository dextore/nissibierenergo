﻿export interface ISelectMarkersDlg {
    show();
}

export function getSelectMarkersDlg(): ISelectMarkersDlg {
    return new SelectMarkersDlg();
}

class SelectMarkersDlg implements ISelectMarkersDlg {
    show() {
        return {
            view: "window",
            position: "center",
            autoheight: true,
            padding: 80,
            width: 300,
            height: 600,
            head: {
            view: "toolbar",
                margin: -4,
                cols: [
                { view: "label", label: "Расширенный поиск" },
                {
                    view: "icon",
                    icon: "times-circle",
                    //click: () => { this.cancel(); }
                }
            ]
        },
        }
    }
}
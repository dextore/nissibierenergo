﻿import Dlg = require("./ext_search_dlg");

export interface ICearchView {
    init();
}

export function getCearchView(): ICearchView {
    return new CearchView();
} 

class CearchView implements ICearchView {
    init() {
        return {
            rows: [
                {},
                {  
                    height: 40, 
                    cols: [
                        {},
                        {
                            view: "button", label: "поиск", align: "center", width: 120,
                            click: () => {
                                this.searchClick();    
                            }
                        },
                        {}
                    ]
                },
                {}
            ]
        }
    }

    private searchClick() {
        const dlg = Dlg.getExtSearchDlg();
        dlg.show();
        //webix.message("search click!");
    }
}
﻿import View = require("./search_view");

webix.i18n.setLocale("ru-RU");

class ExtSearchApp {

    private _view = View.getCearchView();

    init() {
        webix.ready(() => {
            webix.ui(this._view.init());
        });
    }
}

const extSearchApp = new ExtSearchApp();
extSearchApp.init();


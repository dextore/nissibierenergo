﻿import Stendart = require("./standart_search_view");
import Custom = require("./custom_search_view");

export interface IExtSearchView {
    init();
}

export function getExtSearchView(): IExtSearchView {
    return new ExtSearchView();
}

const viewName = "ext_search_view";

class ExtSearchView implements IExtSearchView {

    private _customView = Custom.getCustomSearchView();
    private _standartView = Stendart.getStatndartSearchView();

    get radioId() {
        return `${viewName}-radio-id`;
    }

    get searchId() {
        return `${viewName}-search-view-id`;
    }

    get advancedSearchId() {
        return `${viewName}-advanced-search-view-id`;
    }

    init() {
        return {
            rows: [
                {
                    cols: [{},
                        {
                            rows:[
                                {
                                    view: "template",
                                    template: "Поиск по...",
                                    borderless: true,
                                    height: 30
                                }, {
                                    view: "radio",
                                    id: this.radioId,
                                    align: "center",
                                    value: 1,
                                    options: [
                                        { "id": 1, "value": "Наименование" },
                                        { "id": 2, "value": "ИНН" },
                                        { "id": 3, "value": "Другое" }
                                    ],
                                    on: {
                                        onChange: (newv, oldv) => {
                                            if (newv.toString() === "3") {
                                                if (this._customView.isVisible)
                                                    return; 
                                                this._customView.show();
                                                return;
                                            }
                                            if (this._standartView.isVisible)
                                                return; 
                                            this._standartView.show();
                                        }
                                    }
                                }
                            ]
                        },
                        {}
                    ]
                },
                {
                    view: "multiview",
                    animate: false,
                    cells: [
                        this._standartView.init(),
                        this._customView.init()
                    ]
                }
            ]
        }
    }

    initSearch() {
        return {
            height: 30,
            id: this.searchId,
            cols: [
                {
                    width: 50
                }, {
                    view: "search",
                    //id: this.controlId,
                    type: "search", // для иконки очистить
                    align: "right",
                    placeholder: "Поиск..",
                    maxWidth: 300,
                    minWidth: 50,
                }, {
                    view: "button",
                    type: "icon",
                    icon: "search",
                    width: 30 
                }, {}
            ]
        } 
    }
}
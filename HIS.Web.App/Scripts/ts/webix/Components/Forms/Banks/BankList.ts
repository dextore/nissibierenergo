﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../ComponentBase");
import BankDlg = require("../../../Modules/Banks/BankDlg");
import BanksBranchDlg = require("../../../Modules/Banks/BanksBranchDlg");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import BankOperation = require("../../Controls/BanksOperationStateControl");
import Pager = require("../../Pager/Pager");

export const modulName = "banks-module";

export interface IBankList extends Base.IComponent {
    ready();
}

export function getBankList(module: Tab.IMainTab) {
    return new BankList(`${modulName}-tab`, module);
}

class BankList extends Base.ComponentBase implements IBankList {
    
    private _columnInfo: { [id: string]: Querybuilder.ICaptionAndAliaseModel } = {}

    protected get dataTableId() {
        return `${this._viewName}-datatable-id`;
    }

    protected get dataTableBranchesId() {
        return `${this._viewName}-dataTableBranches-id`;
    }

    protected get pagerId() {
        return `${this._viewName}-pager-id`;
    }

    protected get pagerBranchesId() {
        return `${this._viewName}-branches-pager-id`;
    }

    private _operations;
    private _branchesOperations;

    constructor(private readonly _viewName: string, private readonly _module: Tab.IMainTab) {
        super();
        this._operations = BankOperation.getBanksOperationStateControl(this._viewName,
            (bankId: number) => {
                const promise = webix.promise.defer();

                Api.getApiBanks().getBanksHeader(bankId).then(result => {
                    (promise as any).resolve(`${result}`);
                }).catch(() => { (promise as any).reject() });

                return promise;
            },
            () => true);

        this._branchesOperations = BankOperation.getBanksOperationStateControl(`${this._viewName}-branches`,
            (bankId: number) => {

                const promise = webix.promise.defer();

                Api.getApiBanks().getBanksHeader(bankId).then(result => {
                    (promise as any).resolve(`${result}`);
                }).catch(() => { (promise as any).reject() });

                return promise;
            },
            () => true);

        this._operations.stateChangesEvent.subscribe((result) => {
            var current = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true)[0];
            let self = this;
            Api.getApiBanks().getBank(current.baId).then(result => {
                let grid = ($$(self.dataTableId) as webix.ui.datatable);
                let selectedId = grid.getSelectedId(false, true);
                grid.updateItem(selectedId, result);
            });
        }, this);

        this._branchesOperations.stateChangesEvent.subscribe((result) => {
            var current = ($$(this.dataTableBranchesId) as webix.ui.datatable).getSelectedItem(true)[0];
            let self = this;
            Api.getApiBanks().getBank(current.baId).then(result => {
                let grid = ($$(self.dataTableBranchesId) as webix.ui.datatable);
                let selectedId = grid.getSelectedId(false, true);
                grid.updateItem(selectedId, result);
            });
        }, this);
    }

    init() {
        //15/10
        const pager = Pager.getDataTablePager(this.pagerId);
        const pagerBranches = Pager.getDataTablePager(this.pagerBranchesId);
        const dataTable = webix.extend(this.dataTableInit(), { pager: this.pagerId });
        const dataTableBranches = webix.extend(this.dataTableBranchesInit(), { pager: this.pagerBranchesId });
        return {
            rows: [
                {   
                    rows: [
                        this.toolbarInit(),
                        dataTable,
                        pager.init()
                    ]
                },
                { view: "resizer" },
                {
                    height: 350,
                    rows: [
                        this.toolbarInit("Отделения банка", true),
                        dataTableBranches,
                        pagerBranches.init()
                    ]
                }
            ]
        }
    }

    ready() {
        this._operations.ready();
        webix.extend($$(this.dataTableId), webix.ProgressBar);
        const dataTable = $$(this.dataTableId) as webix.ui.datatable;
        const branchTable = $$(this.dataTableBranchesId) as webix.ui.datatable;
        branchTable.hide();
        (dataTable as any).showProgress();

        Api.getApiBanks().get().then((items: Banks.IFrontBanksEntity[]) => {
            dataTable.config.url = <any>{
                $proxy: true,
                load: (view, callback, params) => {
                    let data = items.filter((value, index, array) => value.parentId === null);
                    ($$(this.dataTableId) as any).hideProgress();
                    (webix.ajax as any).$callback(view, callback, data);
                    this.resetButtons(false);
                }
            };

            dataTable.load(dataTable.config.url);
        });


        dataTable.attachEvent("onAfterSelect",
            (selection, preserve) => {

                branchTable.load(branchTable.config.url);
                var select = dataTable.getSelectedItem();
                this._operations.baId = select.baId;
                this._branchesOperations.baId = null;

                return true;
            });

        branchTable.attachEvent("onAfterSelect",
            (selection, preserve) => {
                var select = branchTable.getSelectedItem();
                this._branchesOperations.baId = select.baId;

                return true;
            });
    }

    protected toolbarInit(label = "Справочник банков", isBranches = false) {
        var bank_toolbar = {
            view: "toolbar",
            paddingY: 3,
            height: 40,
            elements: [
                { view: "label", label: label },
                isBranches ? this._branchesOperations.init() : this._operations.init(),
                {
                    view: "icon",
                    icon: "plus",
                    click: () => {
                        if (isBranches) {
                            this.showBranchesDialog();
                        }
                        else this.showDialog();
                    }
                },
                {
                    view: "icon",
                    icon: "edit",
                    click: () => {
                        if (isBranches) {
                            this.showBranchesDialog(true);
                        }
                        else this.showDialog(true);
                    }
                }
            ]
        };
        if (isBranches) {
            bank_toolbar["id"] = "branch-toolbar";
        }
        return bank_toolbar;
    }

    protected dataTableBranchesInit() {
        const config = this.dataTableInit(this.dataTableBranchesId);

        const url = {
            $proxy: true,
            load: (view, callback, params) => {
                const dataTable = $$(this.dataTableId) as webix.ui.datatable;
                const branchTable = $$(this.dataTableBranchesId) as webix.ui.datatable;
                const bank = dataTable.getSelectedItem();

                (view as webix.ui.datatable).clearAll();

                if (!bank)
                    return;

                Api.getApiBanks().getBranches(bank.baId).then((branches: Banks.IFrontBanksEntity[]) => {
                    const data = branches;
                    branchTable.clearAll();

                    if (data.length > 0) {
                        branchTable.show();
                    } else {
                        branchTable.hide();
                    }

                    (webix.ajax as any).$callback(view, callback, data);
                    this.resetButtons(false);
                });
            }
        };
        let tmpConfig = webix.extend(config, { url: url });
        return tmpConfig;// webix.extend(tmpConfig, { autoheight: true });
    }

    protected dataTableInit(id = this.dataTableId) {

        if (id == this.dataTableBranchesId) {
            return {
                view: "datatable",
                id: id,
                select: "row",
                scroll: "y",
                datafetch: 15,
                resizeColumn: true,
                columns: [
                    {
                        id: "name",
                        header: ["Наименование отделения банка", { content: "textFilter" }],
                        sort: "string",
                        width: 100,
                        fillspace: true
                    },
                    { id: "bik", header: ["БИК", { content: "textFilter" }], sort: "int", width: 85 },
                    {
                        id: "corespondetsAccount",
                        header: ["Корреспондентский счет", { content: "textFilter" }],
                        sort: "int",
                        width: 160
                    },
                    { id: "inn", header: ["ИНН отделения", { content: "textFilter" }], sort: "int" },
                    { id: "rkc", header: ["РКЦ отделения", { content: "textFilter" }], sort: "int" },
                    { id: "okpo", header: ["ОКПО отделения", { content: "textFilter" }], sort: "int" },
                    {
                        id: "displayAddr",
                        header: ["Местонахождение отделения", { content: "textFilter" }],
                        sort: "string",
                        fillspace: true
                    },
                    {
                        id: "acceptancePeriod",
                        header: ["Срок акцепта отделения (кол-во дней)", { content: "textFilter" }],
                        sort: "int"
                    },
                    {
                        id: "bankStatus",
                        header: ["Статус", { content: "selectFilter" }],
                        sort: "string"
                    }
                ],

            }

        }
        return {
            view: "datatable",
            id: id,
            select: "row",
            scroll: "y",
            datafetch: 15,
            resizeColumn: true,
            columns: [
                {
                    id: "name",
                    header: ["Наименование банка", { content: "textFilter" }],
                    sort: "string",
                    width: 100,
                    fillspace: true
                },
                { id: "bik", header: ["БИК", { content: "textFilter" }], sort: "int", width: 85 },
                {
                    id: "corespondetsAccount",
                    header: ["Корреспондентский счет", { content: "textFilter" }],
                    sort: "int",
                    width: 160
                },
                { id: "inn", header: ["ИНН", { content: "textFilter" }], sort: "int" },
                { id: "rkc", header: ["РКЦ", { content: "textFilter" }], sort: "int" },
                { id: "okpo", header: ["ОКПО", { content: "textFilter" }], sort: "int" },
                {
                    id: "displayAddr",
                    header: ["Местонахождение", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                },
                {
                    id: "acceptancePeriod",
                    header: ["Срок акцепта (кол-во дней)", { content: "textFilter" }],
                    sort: "int"
                },
                {
                    id: "bankStatus",
                    header: ["Статус", { content: "selectFilter" }],
                    sort: "string"
                }
            ],
        
        }
    };


    getBankData(): Banks.IFrontBanksEntity[] {
        var res = [];
        const result = Api.getApiBanks().get();
        return res;
    }

    private showDialog(isEdit = false) {
        const dataTable = ($$(this.dataTableId) as webix.ui.datatable);
        
        if (isEdit) {
            var items = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true);
            if (!items || items.length == 0) {
                return;
            }
            var bankId = items.length ? <any>items[0].baId : null;    
            var dataDataTableId = dataTable.getSelectedId(true, false)[0];
        }

        const dlg = BankDlg.getBankDlg(`${this._viewName}-form`,
                                        bankId,
                                        null,
                                        dataTable,
                                        isEdit,
                                        dataDataTableId === undefined ? 0 : dataDataTableId.row,
                                        this._module);

        dlg.showModalContent(this._module,
            () => {
                dlg.close();
            });
    }

    protected resetButtons(hasSelecetd: boolean) {}

    private showBranchesDialog(isEdit = false) {

        /// проверка выбран ли банк 
        var items = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true);
        if (!items || items.length == 0) {
            return;
        }

        const branchTable = $$(this.dataTableBranchesId) as webix.ui.datatable;

        let bank: Banks.IFrontBanksEntity = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true);
        if (bank) {
            var parrentId = bank[0] ? bank[0].baId : null;
            BanksBranchDlg.inn = bank[0] ? bank[0].inn : null;
        }

        if (isEdit) {
            var items = ($$(this.dataTableBranchesId) as webix.ui.datatable).getSelectedItem(true);
            if (!items || items.length == 0) {
                return;
            }
            var baId = items.length ? <any>items[0].baId : null;
            var branchDataTableId = ($$(this.dataTableBranchesId) as webix.ui.datatable).getSelectedId(true, false)[0];
        }
       
        
        const dlg = BanksBranchDlg.getBankDlg(`${this._viewName}-branches-form`,
                                              baId,
                                              parrentId,
                                              branchTable,
                                              isEdit,
                                              branchDataTableId === undefined ? 0 : branchDataTableId.row,
                                              this._module);

        dlg.showModalContent(this._module,
            () => {
                dlg.close();
            });
    }
}
﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Btn = require("../../Buttons/ButtonsExporter");
import ToolButtons = require("../../Buttons/ToolBarButtons");
import History = require("../../History/HistoryEntityDialog");
import InventoryObjectsMovementHistoryDlg = require("../../../Modules/InventoryObjectsMovementHistory/InventoryObjectsMovementHistoryDlg");
import Pager = require("../../Pager/Pager");

export const modulesName = "fAssets-module";

export interface IInventoryObjectsMovementHistoryList extends Base.IComponent {
    ready();
    selectedCompany: Inventory.ILSCompanyAreas;
}

export function getInventoryObjectsMovementHistoryList(module: Tab.IMainTab): IInventoryObjectsMovementHistoryList {
    return new InventoryObjectsMovementHistoryList(`${modulesName}-tab`, module);
}

class InventoryObjectsMovementHistoryList extends Base.ComponentBase implements IInventoryObjectsMovementHistoryList {

    private _selectedCompany: Inventory.ILSCompanyAreas;
    get selectedCompany(): Inventory.ILSCompanyAreas {
        return this._selectedCompany;
    }

    set selectedCompany(value: Inventory.ILSCompanyAreas) {
        this._selectedCompany = value;
        this.load();
    };

    get datatableId() {
        return `${this._viewName}-datatable-id`;
    }
    get pagerId() {
        return `${this._viewName}-pager`;
    };

    get checkBoxId() {
        return `${this._viewName}-showDeleted`;
    }

    constructor(private readonly _viewName: string,
                private readonly _mainTab: Tab.IMainTab) {
        super();
    }

    init() {
        //27/10
        const pager = Pager.getDataTablePager(this.pagerId);
        const dataTable = webix.extend(this.datatableInit(), { pager: this.pagerId });
        return {
            rows: [this.dataTableToolBarInit(), dataTable, pager.init()]
        };
    }

    dataTableToolBar = {
        history: ToolButtons.getHistoryBtn(`${this._viewName}-operation-toolbar`, "История", () => this.showHistory()),
        inventoryAssetssMovevmentHistory: ToolButtons.getInventoryTransactionBtn(`${this._viewName}-operation-toolbar`, "История движения", () => this.showDialogHist())
    };

    dataTableToolBarInit() {
        return {
            id: `${modulesName}-toolbar`,
            view: "toolbar",
            paddingY: 3,
            elements: [
                { view: "label", label: "Инвентарные объекты" },
                {
                    view: "checkbox",
                    id: this.checkBoxId,
                    label: "Показать удалённые",
                    labelWidth: 140,
                    value: 0
                },
                this.dataTableToolBar.history.init(),
                this.dataTableToolBar.inventoryAssetssMovevmentHistory.init()
            ]
        }
    }

    datatableInit() {
        return {
            view: "datatable",
            id: this.datatableId,
            select: "row",
            resizeColumn: true,
            datafetch: 30,
            columns: [
                {
                    id: "inventoryNumber",
                    header: ["Инвентарный номер", { content: "textFilter" }],
                    sort: "string",
                    adjust: true
                },
                {
                    id: "inventoryName",
                    header: ["Номенклатурная единица", { content: "textFilter" }],
                    sort: "string",
                    adjust: true
                },
                {
                    id: "releaseDate",
                    header: ["Дата выпуска", { content: "dateFilter" }],
                    format: webix.i18n.dateFormatStr,
                    sort: "date",
                    adjust: true
                },
                {
                    id: "docName",
                    header: ["Документ", { content: "textFilter" }],
                    sort: "string",
                    adjust: true
                },
                {
                    id: "serialNumber",
                    header: ["Заводской (серийный) номер", { content: "textFilter" }],
                    sort: "string",
                    adjust: true
                },
                {
                    id: "mRPName",
                    header: ["МОЛ (текущий)", { content: "textFilter" }],
                    sort: "string",
                    adjust: true
                },
                {
                    id: "lSName",
                    header: ["Владелец ТМЦ", { content: "textFilter" }],
                    sort: "string",
                    adjust: true
                },
                {
                    id: "barCode",
                    header: ["Штрих-код", { content: "textFilter" }],
                    sort: "string",
                    adjust: true
                },
                {
                    id: "employeeId",
                    header: ["Сотрудник организации", { content: "textFilter" }],
                    sort: "string",
                    adjust: true
                },
                {
                    id: "location",
                    header: ["Местоположение", { content: "textFilter" }],
                    sort: "string",
                    adjust: true
                },
                {
                    id: "checkDate",
                    header: ["Дата последней гос. проверки", { content: "dateFilter" }],
                    format: webix.i18n.dateFormatStr,
                    sort: "date",
                    adjust: true
                },
                {
                    id: "state",
                    header: ["Состояние", { content: "selectFilter" }],
                    sort: "string",
                    adjust: true
                },
                {
                    id: "companyAreaName",
                    header: ["Организация"],
                    sort: "string",
                    adjust: true
                }
            ]
        };
    }

    showDialogHist() {
        const datatable = $$(this.datatableId) as webix.ui.datatable;
        let selected = (datatable.getSelectedItem() as InventoryObjectsMovementHistory.IInventoryAssetsModel);
        if (!selected) {
            datatable.unselectAll();
            return;
        }
        const dlg = InventoryObjectsMovementHistoryDlg.getInventoryObjectsMovementHistoryDlg(`${this._viewName}-movement-history-dialog`,
            selected.bAId,
            `<b>№ ${selected.inventoryNumber} ${selected.inventoryName} </b>`
        );

        dlg.showModalContent(this._mainTab, () => {
            dlg.close();
        });
    }

    load() {
        let datatable = $$(this.datatableId) as webix.ui.datatable;
        let checkBox = $$(this.checkBoxId) as webix.ui.checkbox;

        datatable.clearAll();

        webix.extend(datatable, webix.ProgressBar);
        (datatable as any).showProgress();

        Api.ApiInventoryObjectsMovementHistory.getFaAssetesList().then(result => {
            datatable.config.url = <any>{
                $proxy: true,
                load: (view, callback, params) => {
                    let data = result;
                    let currentValue = checkBox.getValue();

                    if (Number(currentValue) !== 1) {
                        data = data.filter(item => item.stateId !== 255);
                    }

                    data = data.filter(item => item.companyAreaName === this._selectedCompany.name).reverse();
                    datatable.clearAll();
                    (datatable as any).hideProgress();
                    (webix.ajax as any).$callback(view, callback, data);
                }
            };

            datatable.load(datatable.config.url);
        });

        checkBox.attachEvent("onChange", () => {
            datatable.load(datatable.config.url);
            datatable.refresh();
        });

        datatable.attachEvent("onAfterSelect",
            () => {
                let current: InventoryObjectsMovementHistory.IInventoryAssetsModel = datatable.getSelectedItem();
                this.dataTableToolBar.history.button.enable();
                if (current.stateId === 1 || current.stateId === 7) {
                    this.dataTableToolBar.inventoryAssetssMovevmentHistory.button.enable();
                } else {
                    this.dataTableToolBar.inventoryAssetssMovevmentHistory.button.disable();
                }
                
            });

        datatable.attachEvent("onAfterFilter",
            () => {
                datatable.unselectAll();
                this.dataTableToolBar.inventoryAssetssMovevmentHistory.button.disable();
            });
    }

    ready() { }


    showHistory() {
        const datatable = $$(this.datatableId) as webix.ui.datatable;
        let selectBaId = (datatable.getSelectedItem() as InventoryObjectsMovementHistory.IInventoryAssetsModel).bAId;
        if (!selectBaId) {
            return;
        }
        const dialog = History.getHistoryEntityDialog(`${this._viewName}-dialog`, null, selectBaId);
        dialog.showModal(() => {
            dialog.close();
        });
    }
}

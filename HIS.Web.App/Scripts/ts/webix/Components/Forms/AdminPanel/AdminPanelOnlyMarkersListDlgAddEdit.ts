﻿import Base = require("../../../Common/DialogBase");
import Common = require("../../../Common/CommonExporter");
import Btn = require("../../Buttons/ButtonsExporter");
import adminPanelListDlgView = require("./AdminPanelListDlgView");
import Api = require("../../../Api/ApiExporter");

export function getAdminPanelOnlyMarkersListDlgAddEdit(viewName: string, isEdit: boolean, datatable: webix.ui.datatable, selectedCompany: Inventory.ILSCompanyAreas) {
    return new AdminPanelOnlyMarkersListDlgAddEdit(viewName, isEdit, datatable, selectedCompany);
}

class AdminPanelOnlyMarkersListDlgAddEdit extends Base.DialogBase {
    private _item: AdminPanel.IOnlyMarkersDataModel;
    protected get formId() {
        return `${this.viewName}-AdminPanelOnlyMarkersListDlgAddEdit-Dlg-id`;
    };
    protected get labelWidth() {
        return 150 + 140;
    };

    constructor(viewName: string,
        private isEdit: boolean,
        private datatable: webix.ui.datatable,
        private selectedCompany: Inventory.ILSCompanyAreas) {
        super(viewName);
        if (isEdit)
            this._item = datatable.getSelectedItem() as AdminPanel.IOnlyMarkersDataModel;
    }

    private _tabModule: Common.IMainTab;

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._tabModule = module;
        this._tabModule.disableCompany();

        const markerTypeField = $$(`${this.formId}-markerType-field-id`) as webix.ui.select;

        const baTypeIdField = $$(`${this.formId}-baTypeLink-superField`) as any;
        const viewName = $$(`${this.formId}-implementTypeName-field-id`) as webix.ui.text;

        if (this.isEdit) {
            if (! (this._item.markerTypeId === 11 || this._item.markerTypeId === 12)) {
                baTypeIdField.disable();
                viewName.disable();
            };

            let batypelinkId = this._item.markerTypeId === 11 || this._item.markerTypeId === 12
                ? null
                : this._item.baTypeLinkId;

            baTypeIdField.setValue(this._item.baTypeLink);
            baTypeIdField.define("markerId", batypelinkId);
            if (this._item.markerTypeId === 11 || this._item.markerTypeId === 12) {
                viewName.disable();
                Api.ApiAdminPanel.canEditLinkOnEntityMarker(this._item.id).then(canEdit => {
                    if (canEdit) {
                        baTypeIdField.enable();
                        markerTypeField.enable();
                    } else {
                        baTypeIdField.disable();
                        markerTypeField.disable();
                    }
                });
            }
        } else {
            baTypeIdField.disable();
            viewName.disable();
        }

        markerTypeField.attachEvent("onChange",
            (newv, oldv) => {
                if (newv === "13") {
                    baTypeIdField.disable();
                    baTypeIdField.setValue("");
                    baTypeIdField.markerId = null;
                    viewName.enable();
                }
                else if ((newv === "11") || (newv === "12")) {
                    viewName.disable();
                    viewName.setValue("");
                    baTypeIdField.enable();
                }
                else {
                    baTypeIdField.disable();
                    baTypeIdField.setValue("");
                    baTypeIdField.markerId = null;

                    viewName.setValue("");
                    viewName.disable();
                }
            });

    }

    headerLabel(): string {
        return `${this.isEdit ? "Редактирование" : "Добавление"} маркера`;
    }

    contentConfig() {
        let field = `field-id`;
        let optionsData = [
            { "id": 0, "value": "" },
            { "id": 1, "value": "Логический" },
            { "id": 2, "value": "Целочисленный" },
            { "id": 3, "value": "Вещественный" },
            { "id": 4, "value": "Символьный" },
            { "id": 5, "value": "Перечень" },
            { "id": 6, "value": "Денежный" },
            { "id": 10, "value": "Дата и Время" },
            { "id": 11, "value": "Ссылка на сущность" },
            { "id": 12, "value": "Адрес" },
            { "id": 13, "value": "Справочник" }
        ];
        return {
            view: "form",
            id: this.formId,
            minheight: 600,
            autoheight: true,
            elements: [
                {
                    view: "text",
                    id: `${this.formId}-name-${field}`,
                    label: "Наименование",
                    name: "name",
                    required: true,
                    labelWidth: this.labelWidth,
                    value: this.isEdit ? this._item.name : "",
                },
                {
                    id: `${this.formId}-mvcAlias-${field}`,
                    label: "Уникальное неизменное обозначение",
                    labelWidth: this.labelWidth,
                    name: "mvcAlias",
                    required: true,
                    view: "text",
                    value: this.isEdit ? this._item.mvcAlias : "",
                    validate: (value) => {
                        return value.length > 0 && !(/[а-яё]/.test(value));
                    }
                },
                {
                    id: `${this.formId}-markerType-${field}`,
                    label: "Тип маркера",
                    labelWidth: this.labelWidth,
                    view: "select",
                    options: optionsData,
                    value: this.isEdit
                        ? optionsData.filter(x => x.value === this._item.markerType)[0].id
                        : 0,
                },
                {
                    view: "extsearch",
                    id: `${this.formId}-baTypeLink-superField`,
                    css: "protoUI-with-icons-2",
                    icons: ["search", "close"],
                    label: "Ссылка на тип сущности",
                    labelWidth: this.labelWidth,
                    readonly: true,
                    name: `${this.formId}-baTypeLink-superFieldName`,
                    validate: (value) => {
                        return true;
                    },
                    on: {
                        onSearchIconClick: () => this.showBaTypesChooseDlg(),
                        onCloseIconClick: () => this.clearParentBaType()
                    }
                },
                {
                    id: `${this.formId}-implementTypeName-${field}`,
                    label: "Наименование представления для значений",
                    labelWidth: this.labelWidth,
                    view: "text",
                    value: this.isEdit ? this._item.implementTypeName : ""
                },
                {
                    cols: [
                        {},
                        this.okBtn.init(),
                        this.cancelBtn.init()
                    ]
                }
            ],
            rules: {
                "name": webix.rules.isNotEmpty
            }
        }
    }

    protected okBtn = Btn.getOkButton(this.viewName, () => {
        if (($$(this.formId) as webix.ui.form).validate()) {
            const data = this.getDataFromDialog();

            Api.ApiAdminPanel.createUpdateMarker(data).then(result => {
                if (result) {
                    if (!this.isEdit) {
                        this.datatable.add(result, 0);
                    }
                    else {
                        const { id } = this.datatable.getSelectedItem();
                        this.datatable.updateItem(id, result);
                    }
                    this.okClose();
                }
            });
        }
    });

    protected cancelBtn = Btn.getCancelButton(this.viewName, () => {
        this.okClose();
    });

    getDataFromDialog() {
        const form = $$(this.formId) as any;

        let data = [];
        let self = this;

        form._collection.forEach(
            (val) => {
                let id = ($$(val.id).config.id as string).substring(0, ($$(val.id).config.id as string).length - "-field-id".length);
                id = id.substring(self.formId.length + 1);
                data[id] = ($$(val.id).config as any).value;
            }
        );
        let baTypeLinkId = Number(form._cells[3].config.markerId);
        let result: AdminPanel.IOnlyMarkersDataModel = {
            id: this.isEdit? this._item.id : null,
            baTypeLink: data["baTypeLink-s"],
            baTypeLinkId: baTypeLinkId === 0 ? null : baTypeLinkId,
            implementTypeName: data["implementTypeName"] === "" ? null : data["implementTypeName"],
            markerTypeId: data["markerType"],
            mvcAlias: data["mvcAlias"],
            name: data["name"]
        } as any;

        return result;
    }

    showBaTypesChooseDlg() {
        const dlg = adminPanelListDlgView.getAdminPanelListDlgView(
            `${this.viewName}-dlg`,
            `${this.formId}-baTypeLink-superField`);
        dlg.showModalContent(this._tabModule, () => {
            dlg.close();
        });
    }

    clearParentBaType() {
        ($$(`${this.formId}-baTypeLink-superField`) as any).setValue("");
        ($$(`${this.formId}-baTypeLink-superField`) as any).markerId = null;
    }
}
﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Pager = require("../../Pager/Pager");
import ToolButtons = require("../../Buttons/ToolBarButtons");
import AddEditMarkerDlg = require("./AdminPanelOnlyMarkersListDlgAddEdit");
import AdminAvailableValuesDlg = require("./AdminAvailableValuesDlg");

export const modulesName = "adminPanelOnlyMarkersList-panel-module";

export interface IAdminPanelOnlyMarkersList extends Base.IComponent {
    ready();
    load();
    selectedCompany: Inventory.ILSCompanyAreas;
}

export function getAdminPanelOnlyMarkersList(module: Tab.IMainTab, view:string = ""): IAdminPanelOnlyMarkersList {
    return new AdminPanelOnlyMarkersList(`${modulesName}${view}-tab`, module);
}

class AdminPanelOnlyMarkersList extends Base.ComponentBase implements IAdminPanelOnlyMarkersList {
    selectedCompany: Inventory.ILSCompanyAreas;
    get datatableId() {
        return `${this._viewName}-AdminPanelOnlyMarkersList-datatable-id`;
    }
    get pagerId() {
        return `${this._viewName}-AdminPanelOnlyMarkersList-pager`;
    };

    constructor(private readonly _viewName: string,
        private readonly _mainTab: Tab.IMainTab) {
        super();
    }

    init() {
        const pager = Pager.getDataTablePager(this.pagerId);
        const dataTable = webix.extend(this.datatableInit(), { pager: this.pagerId });
        return {
            rows: [this.dataTableToolBarInit(), dataTable, pager.init()]
        };
    }

    dataTableToolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}-markers-operation-toolbar`, "Добавить маркер", () => this.showAddDlg()),
        edit: ToolButtons.getEditBtn(`${this._viewName}-markers-operation-toolbar`, "Редактировать маркер", () => this.showAddDlg(true)),
        del: ToolButtons.getRemoveBtn(`${this._viewName}-markers-operation-toolbar`, "Удалить маркер", () => this.deleteMarker())
    };

    dataTableToolBarInit() {
        return {
            id: `${this._viewName}-AdminPanelOnlyMarkersList-toolbar`,
            view: "toolbar",
            paddingY: 3,
            elements: [
                { view: "label", label: "Список доступных маркеров" },
                this.dataTableToolBar.add.init(),
                this.dataTableToolBar.edit.init(),
                this.dataTableToolBar.del.init()
            ]
        }
    }

    datatableInit() {
        return {
            view: "datatable",
            id: this.datatableId,
            select: "row",
            resizeColumn: true,
            datafetch: 30,
            columns: [
                {
                    id: "id",
                    header: "ID",
                    sort: "int",
                    fillspace: true
                },
                {
                    id: "name",
                    header: ["Наименование", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                },
                {
                    id: "mvcAlias",
                    header: ["Уникальное неизменное обозначение", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                },
                {
                    id: "baTypeLink",
                    header: ["Ссылка на тип сущности", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                },
                {
                    id: "markerType",
                    header: ["Тип маркера", { content: "textFilter" }],
                    template: (row: AdminPanel.IOnlyMarkersDataModel) => {
                        if (row.markerTypeId === 5) {
                            return `<span style="float:left">${row.markerType}</span> <span class='webix_icon  fa-list-ul' style="float:right; line-height: 26px;"></span>`;
                        }
                        //TODO: потом сделаем для справочника
                        //else if (row.baTypeLinkId === referenceMarkerType) {
                        //    return `<span style="float:left">${row.markerType}</span> <span class='webix_icon  fa-list-ul' style="float:right; line-height: 26px;"></span>`;
                        //}
                        else {
                            return `${row.markerType}`;
                        }
                    },
                    sort: "string",
                    fillspace: true
                },
                {
                    id: "implementTypeName",
                    header: ["Наименование представления для значений", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                },
                
            ],
            onClick: {
                "fa-list-ul": (ev, id) => {
                    const datatable = $$(`${this.datatableId}`) as webix.ui.datatable;
                    const clickedItem: AdminPanel.IOnlyMarkersDataModel = datatable.getItem(id.row);
                    const dlg: AdminAvailableValuesDlg.IAdminAvailableValuesDlg =
                        AdminAvailableValuesDlg.getAdminAvailableValuesDlg(this._viewName);
                    switch (clickedItem.markerTypeId) {
                    case 5: {
                        Api.ApiAdminPanel.getAvailableValuesForList(id.row).then(result => {
                            dlg.dialogData = result;
                            dlg.header = `Доступные значения для перечня <b>${clickedItem.name}</b>`;
                            dlg.markerId = clickedItem.id;
                            dlg.showModal(() => {
                                dlg.close();
                            });
                        });
                        break;
                    }
                    //TODO: потом сделаем для справочника
                    //case 13:
                    //{
                    //    Api.ApiAdminPanel.getAvailableValuesForReference(id.row).then(result => {
                    //        console.log("resultFromServer: ", result);
                    //        dlg.dialogData = result;
                    //        dlg.header = `Доступные значения для справочника <b>${clickedItem.name}</b>`;
                    //        dlg.showModal(() => {
                    //            dlg.close();
                    //        });
                    //    });
                    //    break;
                    //}
                    default: break;
                    }
                }
            }
        };
    }

    private getDisabledCheckBox(obj, common, value, config) {
        const checked = (value == config.checkValue) ? 'checked="true"' : "";
        return `<input disabled class='webix_table_checkbox' type='checkbox' ${checked}>`;
    }

    load() {
            let datatable = $$(this.datatableId) as webix.ui.datatable;
            datatable.clearAll();

            webix.extend(datatable, webix.ProgressBar);
            (datatable as any).showProgress();

            Api.ApiAdminPanel.getAllMarkers().then(result => {
                datatable.config.url = <any>{
                    $proxy: true,
                    load: (view, callback, params) => {

                        (datatable as any).hideProgress();
                        (webix.ajax as any).$callback(view, callback, result);
                    }
                };

                datatable.load(datatable.config.url);
            });
    }

    ready() {
        this.dataTableToolBar.add.button.enable();
        this.dataTableToolBar.edit.button.disable();
        this.dataTableToolBar.del.button.disable();

        const datatable = $$(this.datatableId) as webix.ui.datatable;
        datatable.attachEvent("onAfterSelect",
            () => {
                this.dataTableToolBar.edit.button.enable();
                this.dataTableToolBar.del.button.enable();
            });
    }

    showAddDlg(isEdit: boolean = false) {
        const datatable = $$(this.datatableId) as webix.ui.datatable;
        const dlg = AddEditMarkerDlg.getAdminPanelOnlyMarkersListDlgAddEdit(this._viewName,
            isEdit,
            datatable,
            this.selectedCompany);
        let self = this;

        dlg.showModalContent(self._mainTab, () => {
            dlg.close();
        });
    }

    deleteMarker() {
        const datatable = $$(this.datatableId) as webix.ui.datatable;
        const { id } = datatable.getSelectedItem() as AdminPanel.IOnlyMarkersDataModel;

        webix.confirm({
            title: "Подтверждение",
            type: "confirm-warning",
            text: `Выполнить операцию удаления маркера?`,
            ok: "Да", cancel: "Нет",
            callback(result) {
                if (result) {
                    webix.extend(datatable, webix.ProgressBar);
                    (datatable as any).showProgress();

                    Api.ApiAdminPanel.deleteMarker(id).then(result => {
                        (datatable as any).hideProgress();
                        if (result) {
                            datatable.remove(id);
                            this.dataTableToolBar.edit.button.disable();
                            this.dataTableToolBar.del.button.disable();
                        }
                    }).catch((reason) => {
                        (datatable as any).hideProgress();
                    });
                }
            }
        });
    }
}

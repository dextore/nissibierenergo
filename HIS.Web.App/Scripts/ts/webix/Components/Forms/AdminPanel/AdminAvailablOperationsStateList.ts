﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Pager = require("../../Pager/Pager");
import ToolButtons = require("../../Buttons/ToolBarButtons");
import operationsStateDlg = require("../../../Modules/AdminPanel/OperationsStateDlg");

export const modulesName = "admin-panel-module";

export interface IAdminAvailablOperationsStateList  extends Base.IComponent {
    ready();
    load();
}

export function getAdminAvailablOperationsStateList(module: Tab.IMainTab, adminPanelEntityList): IAdminAvailablOperationsStateList {
    return new AdminAvailablOperationsStateList(`${modulesName}-tab`, module, adminPanelEntityList);
}

class AdminAvailablOperationsStateList extends Base.ComponentBase implements IAdminAvailablOperationsStateList {
    baTypeId: number;
    baId: number;

    get datatableId() {
        return `${this._viewName}-AdminAvailablOperationsStateList-datatable-id`;
    }
    get pagerId() {
        return `${this._viewName}-AdminAvailablOperationsStateList-pager`;
    };

    constructor(private readonly _viewName: string,
        private readonly _mainTab: Tab.IMainTab,
        private readonly adminPanelEntityList) {
        super();
    }

    init() {
        const pager = Pager.getDataTablePager(this.pagerId);
        const dataTable = webix.extend(this.datatableInit(), { pager: this.pagerId });
        return {
            rows: [this.dataTableToolBarInit(), dataTable, pager.init()]

        };
    }

    dataTableToolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}-add-operationsState-dlg`, "Добавить связь оперции с состоянием", () => this.showAddEditDlg(false)),
        edit: ToolButtons.getEditBtn(`${this._viewName}-edit-operationsState-dlg`, "Редактировать связь оперции с состоянием", () => this.showAddEditDlg(true)),
        del: ToolButtons.getRemoveBtn(`${this._viewName}-del-operationsState-dlg`, "Удалить связь оперции с состоянием", () => this.deleteOperationStateFromEntitysType())
    };

    showAddEditDlg(isEdit: boolean) {
        const datatable = $$(this.datatableId) as webix.ui.datatable;
        const dlg = operationsStateDlg.getOperationsStateDlg(this._viewName, isEdit, datatable, null, this.baId);
        dlg.showModalContent(this._mainTab, () => dlg.close());
    }

    deleteOperationStateFromEntitysType() {
        let self = this;
        webix.confirm({
            title: "Подтверждение",
            type: "confirm-warning",
            text: `Отвязать оперцию и состояние?`,
            ok: "Да", cancel: "Нет",
            callback(result) {
                if (result) {
                    const datatable = $$(self.datatableId) as webix.ui.datatable;
                    const { operationId, id, sourceStateId } = datatable.getSelectedItem() as any;
                    webix.extend(datatable, webix.ProgressBar);
                    (datatable as any).showProgress();

                    Api.ApiAdminPanel.deleteOperationStateToBaType(self.baId, operationId, sourceStateId).then(result => {
                        if (result) {
                            (datatable as any).hideProgress();
                            datatable.remove(id);
                            self.dataTableToolBar.edit.button.disable();
                            self.dataTableToolBar.del.button.disable();
                        }
                    }).catch(() => {
                        (datatable as any).hideProgress();
                    });

                    (datatable as any).hideProgress();
                    self.dataTableToolBar.edit.button.disable();
                    self.dataTableToolBar.del.button.disable();
                }
            }
        });
    }

    dataTableToolBarInit() {
        return {
            id: `${modulesName}-AdminAvailablOperationsStateList-toolbar`,
            view: "toolbar",
            paddingY: 3,
            elements: [
                { view: "label", label: "Состояния операций типа сущности" },
                this.dataTableToolBar.add.init(),
                this.dataTableToolBar.edit.init(),
                this.dataTableToolBar.del.init()
            ]
        }
    }

    datatableInit() {
        return {
            view: "datatable",
            id: this.datatableId,
            select: "row",
            resizeColumn: true,
            datafetch: 30,
            columns: [
                {
                    id: "operationId",
                    header: "ID",
                    sort: "int",
                    fillspace: true
                },
                {
                    id: "operationName",
                    header: ["Операция", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                },
                {
                    id: "sourceStateName",
                    header: ["Начальное состояние", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                },
                {
                    id: "destinationStateName",
                    header: ["Конечное состояние", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                },
                
                {
                    id: "isBlocked",
                    header: { text: "Признак блокирования перехода" },
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                    fillspace: true
                }
            ],
        };
    }

    private getDisabledCheckBox(obj, common, value, config) {
        const checked = (value == config.checkValue) ? 'checked="true"' : "";
        return `<input disabled class='webix_table_checkbox' type='checkbox' ${checked}>`;
    }

    load() {

    }

    ready() {
        this.dataTableToolBar.add.button.enable();
        this.dataTableToolBar.del.button.disable();
        this.dataTableToolBar.edit.button.disable();

        const entityDataTable = $$(`${this.adminPanelEntityList.datatableId}`) as webix.ui.datatable;
        entityDataTable.attachEvent("onAfterSelect", (selection, preserve) => {
            var selectedEntity = entityDataTable.getSelectedItem() as AdminPanel.IBaTypesModel;

            let datatable = $$(this.datatableId) as webix.ui.datatable;
            datatable.clearAll();

            webix.extend(datatable, webix.ProgressBar);
            (datatable as any).showProgress();

            Api.ApiAdminPanel.getAvailablOperationsStateForEntiti(selectedEntity.typeId).then(result => {
                datatable.config.url = <any>{
                    $proxy: true,
                    load: (view, callback, params) => {

                        (datatable as any).hideProgress();
                        (webix.ajax as any).$callback(view, callback, result);
                    }
                };
                this.baId = selectedEntity.typeId;
                datatable.load(datatable.config.url);
            });

            datatable.attachEvent("onAfterSelect", () => {
                this.dataTableToolBar.del.button.enable();
                this.dataTableToolBar.edit.button.enable();
            });
        });
    }

}

﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Pager = require("../../Pager/Pager");
import ToolButtons = require("../../Buttons/ToolBarButtons");
import AddEditStateDlg = require("./AdminPanelOnlyStateListDlgAddEdit"); // поменять
export const modulesName = "adminPanelOnlyStateList-panel-module";

export interface IAdminPanelOnlyStateList extends Base.IComponent {
    ready();
    load();
    selectedCompany: Inventory.ILSCompanyAreas;
}

export function getAdminPanelOnlyStateList(module: Tab.IMainTab, view: string = ""): IAdminPanelOnlyStateList {
    return new AdminPanelOnlyStateList(`${modulesName}${view}-tab`, module);
}

class AdminPanelOnlyStateList extends Base.ComponentBase implements IAdminPanelOnlyStateList {
    selectedCompany: Inventory.ILSCompanyAreas;
    get datatableId() {
        return `${this._viewName}-AdminPanelOnlyStateList-datatable-id`;
    }
    get pagerId() {
        return `${this._viewName}-AdminPanelOnlyStateList-pager`;
    };

    constructor(private readonly _viewName: string,
        private readonly _mainTab: Tab.IMainTab) {
        super();
    }

    init() {
        const pager = Pager.getDataTablePager(this.pagerId);
        const dataTable = webix.extend(this.datatableInit(), { pager: this.pagerId });
        return {
            rows: [this.dataTableToolBarInit(), dataTable, pager.init()]
        };
    }

    dataTableToolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}-states-operation-toolbar`, "Добавить состояние", () => this.showAddDlg()),
        edit: ToolButtons.getEditBtn(`${this._viewName}-states-operation-toolbar`, "Редактировать состояние", () => this.showAddDlg(true)),
        del: ToolButtons.getRemoveBtn(`${this._viewName}-states-operation-toolbar`, "Удалить состояние", () => this.deleteState())
    };

    dataTableToolBarInit() {
        return {
            id: `${this._viewName}-AdminPanelOnlyStateList-toolbar`,
            view: "toolbar",
            paddingY: 3,
            elements: [
                { view: "label", label: "Список доступных состояний" },
                this.dataTableToolBar.add.init(),
                this.dataTableToolBar.edit.init(),
                this.dataTableToolBar.del.init()
            ]
        }
    }

    datatableInit() {
        return {
            view: "datatable",
            id: this.datatableId,
            select: "row",
            resizeColumn: true,
            datafetch: 30,
            columns: [
                {
                    id: "stateId",
                    header: "ID",
                    sort: "int",
                    fillspace: true
                },
                {
                    id: "stateName",
                    header: ["Наименование", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                }
            ],
        };
    }

    load() {
            let datatable = $$(this.datatableId) as webix.ui.datatable;
            datatable.clearAll();

            webix.extend(datatable, webix.ProgressBar);
            (datatable as any).showProgress();

            Api.ApiAdminPanel.getAllStates().then(result => {
                datatable.config.url = <any>{
                    $proxy: true,
                    load: (view, callback, params) => {

                        (datatable as any).hideProgress();
                        (webix.ajax as any).$callback(view, callback, result);
                    }
                };

                datatable.load(datatable.config.url);
            });
    }

    ready() {
        this.dataTableToolBar.add.button.enable();
        this.dataTableToolBar.edit.button.disable();
        this.dataTableToolBar.del.button.disable();

        const datatable = $$(this.datatableId) as webix.ui.datatable;
        datatable.attachEvent("onAfterSelect",
            () => {
                this.dataTableToolBar.edit.button.enable();
                this.dataTableToolBar.del.button.enable();
            });
    }

    showAddDlg(isEdit: boolean = false) {
        const datatable = $$(this.datatableId) as webix.ui.datatable;
        const dlg = AddEditStateDlg.getAdminPanelOnlyMarkersListDlgAddEdit(this._viewName,
            isEdit,
            datatable,
            this.selectedCompany);
        let self = this;
        dlg.showModalContent(self._mainTab, () => {
            dlg.close();
        });
    }

    deleteState() {
        let self = this;
        webix.confirm({
            title: "Подтверждение",
            type: "confirm-warning",
            text: `Выполнить операцию удаления состояния?`,
            ok: "Да", cancel: "Нет",
            callback(result) {
                if (result) {
                    const datatable = $$(self.datatableId) as webix.ui.datatable;
                    const { stateId, id } = datatable.getSelectedItem() as any;
                    webix.extend(datatable, webix.ProgressBar);
                    (datatable as any).showProgress();

                    Api.ApiAdminPanel.deleteState(stateId).then(result => {
                        if (result) {
                            (datatable as any).hideProgress();
                            datatable.remove(id);
                            self.dataTableToolBar.edit.button.disable();
                            self.dataTableToolBar.del.button.disable();
                        }
                    }).catch(() => {
                        (datatable as any).hideProgress();
                    });
                }
            }
        });
    }
}

﻿import Base = require("../../../Common/DialogBase");
import Pager = require("../../Pager/Pager");
import ToolbarButtons = require("../../Buttons/ToolBarButtons");
import adminAvailableValuesAddEditDlg = require("./AdminAvailableValuesAddEditDlg");
import Api = require("../../../Api/ApiExporter");

export interface IAdminAvailableValuesDlg extends Base.IDialog {
    dialogData: AdminPanel.IItemTableViewModel[];
    markerId: number;
    rendered();
    header: string;
}

export function getAdminAvailableValuesDlg(viewName: string): IAdminAvailableValuesDlg {
    return new AdminAvailableValuesDlg(viewName);
}

class AdminAvailableValuesDlg extends Base.DialogBase implements IAdminAvailableValuesDlg {
    dialogData: AdminPanel.IItemTableViewModel[];

    markerId: number;

    header: string;

    headerLabel(): string { return this.header }

    get datatableId() {
        return `${this.viewName}-markers-available-values-datatable`;
    };

    get pagerId() {
        return `${this.viewName}-markers-available-values-datatable-pager`;
    };

    rendered() {
        
    }

    dataTableToolBar = {
        add: ToolbarButtons.getNewBtn(`${this.viewName}-markers-available-values-add`, "Добавить значение", () => this.showEditAddDlg(false)),
        edit: ToolbarButtons.getEditBtn(`${this.viewName}-markers-available-values-edit`, "Редактировать значение", () => this.showEditAddDlg(true)),
        del: ToolbarButtons.getRemoveBtn(`${this.viewName}-markers-available-values-del`, "Удалить значение", () => this.deleteValue()),
    }
    
    contentConfig() {
        const pager = Pager.getDataTablePager(this.pagerId);
        const dataTable = webix.extend(this.datatableInit(), { pager: this.pagerId });
        const toolBar = this.dataTableToolBarInit();

        return {
            rows: [toolBar, dataTable, pager.init()]
        };
    }

    dataTableToolBarInit() {
        return {
            id: `${this.viewName}-markers-available-values-dlg-toolbar`,
            view: "toolbar",
            paddingY: 3,
            elements: [
                { view: "label", label: "Маркеры сущности" },
                this.dataTableToolBar.add.init(),
                this.dataTableToolBar.edit.init(),
                this.dataTableToolBar.del.init()
            ]
        }
    }
    
    datatableInit() {
        let size = 65;
        return {
            view: "datatable",
            data: this.dialogData,
            id: this.datatableId,
            select: "row",
            resizeColumn: true,
            datafetch: 30,
            columns: [
                {
                    id: "itemId",
                    header: "ID",
                    sort: "int",
                    width: 43
                },
                {
                    id: "orderNumber",
                    header: { text: "Порядковый номер", css: "multiline2-1" },
                    sort: "int",
                    width: 80
                },
                {
                    id: "isActive",
                    header: { text: "Активный" },
                    width: size,
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                },
                {
                    id: "itemName",
                    header: ["Значение", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                }
            ]
        };
    }

    showModal(callBack: () => void): any {
        super.showModal(callBack);

        this.dataTableToolBar.add.button.enable();
        this.dataTableToolBar.del.button.disable();
        this.dataTableToolBar.edit.button.disable();

        const datatable = $$(this.datatableId) as webix.ui.datatable;
        datatable.attachEvent("onAfterSelect", () => {
            this.dataTableToolBar.edit.button.enable();
            this.dataTableToolBar.del.button.enable();
        });
    }

    constructor(viewName: string) {
        super(viewName);
    }

   
    private getDisabledCheckBox(obj, common, value, config) {
        const checked = (value == config.checkValue) ? 'checked="true"' : "";
        return `<input disabled class='webix_table_checkbox' type='checkbox' ${checked}>`;
    }

    showEditAddDlg(isEdit: boolean ) {
        const datatable = $$(this.datatableId) as webix.ui.datatable;
        const dlg = adminAvailableValuesAddEditDlg.getAdminAvailableValuesAddEditDlg(`${this.viewName}-AdminAvailableValuesAddEditDlg`, isEdit, datatable, this.markerId);
        dlg.showModal(() => dlg.close());
    }

    deleteValue() {
        let self = this;
        webix.confirm({
            title: "Подтверждение",
            type: "confirm-warning",
            text: `Выполнить операцию удаления значения?`,
            ok: "Да", cancel: "Нет",
            callback(result) {
                if (result) {
                    const datatable = $$(self.datatableId) as webix.ui.datatable;
                    const { itemId, id } = datatable.getSelectedItem() as any;

                    webix.extend(datatable, webix.ProgressBar);
                    (datatable as any).showProgress();

                    Api.ApiAdminPanel.deleteListItem(self.markerId, itemId).then(result => {
                        if (result) {
                            (datatable as any).hideProgress();
                            datatable.remove(id);
                            self.dataTableToolBar.edit.button.disable();
                            self.dataTableToolBar.del.button.disable();
                        }
                    }).catch(() => {
                        (datatable as any).hideProgress();
                    });
                }
            }
        });


        
    }
}

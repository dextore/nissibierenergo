﻿import Base = require("../../../Common/DialogBase");
import Common = require("../../../Common/CommonExporter");
import Btn = require("../../Buttons/ButtonsExporter");
import adminAvailableOperationsList = require("./AdminAvailableOperationsList");
//import Api = require("../../Api/ApiExporter");
//import ToolButtons = require("../../Components/Buttons/ToolBarButtons");

export function getAdminAvailableOperationsListDlgView(viewName: string, isEdit: boolean, control: any, entityBaId: number, tabModule: Common.IMainTab ){
    return new AdminAvailableOperationsListDlgView(viewName, isEdit, control, entityBaId, tabModule);
}
//AdminAvailableOperationsListDlgView

class AdminAvailableOperationsListDlgView extends Base.DialogBase {
    protected get formId() {
        return `${this.viewName}-AdminAvailableOperationsListDlgView-Dlg-id`;
    };
    protected get labelWidth() {
        return 150 + 140;
    };
    private _list: any;

    constructor(viewName: string,
        private isEdit: boolean,
        private control: any,
        private entityBaId: number,
        private _tabModule: Common.IMainTab) {
        super(viewName);
        this._list = adminAvailableOperationsList.getAdminAvailableOperationsListForDlg(
            this._tabModule,
            $$("admin-panel-module-tab-baTypes-dataTable-id") as webix.ui.datatable,
            "FromSearchDlg");
    }

    

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._tabModule = module;
        this._tabModule.disableCompany();
        this._list.load();
        this._list.ready();
    }

    headerLabel(): string {
        return `${this.isEdit ? "Редактирование" : "Добавление"} операции`;
    }

    contentConfig() {
        return {
            resizeColumn: true,
            autoheight: true,
            autowidth: true,
            rows: [
                this._list.init(),
                {
                    cols: [
                        {},
                        this.okBtn.init(),
                        this.cancelBtn.init()
                    ]
                }
            
            ],
        }
    }

    protected okBtn = Btn.getOkButton(this.viewName, () => {
        let id = this._list.datatableId;
        let dataitem: AdminPanel.IBaTypesOperationsModel = ($$(id) as webix.ui.datatable).getSelectedItem();

        this.control.setValue(dataitem.operationName);
        this.control.define("operationId", `${dataitem.operationId}`); //Number(control.config.markerId)
        this.control.refresh();

        this.okClose();
    });

    protected cancelBtn = Btn.getCancelButton(this.viewName, () => {
        this.okClose();
    });

    getDataFromDialog() {
        const form = $$(this.formId) as webix.ui.form;
        let someValues = form.getValues();

        return someValues;
    }

    //showSearchMarkerDlg() { throw new Error("Not implemented"); }
}
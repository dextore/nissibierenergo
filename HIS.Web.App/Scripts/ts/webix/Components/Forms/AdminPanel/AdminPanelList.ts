﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Pager = require("../../Pager/Pager");
import ToolButtons = require("../../Buttons/ToolBarButtons");
import EntityAdminDlg = require("../../../Modules/AdminPanel/EntityAdminDlg");
import Batable = require("./AdminPanelBaTypesDataTable");
export const modulesName = "admin-panel-module";

export interface IAdminPanelList extends Base.IComponent {
    ready();
    load();
    //initWithoutToolbar();
    datatableId;
    pagerId;
    selectedCompany: Inventory.ILSCompanyAreas;
}

export function getAdminPanelList(module: Tab.IMainTab): IAdminPanelList {
    return new AdminPanelList(`${modulesName}-tab`, module);
}

class AdminPanelList extends Base.ComponentBase implements IAdminPanelList {
    selectedCompany: Inventory.ILSCompanyAreas;

    get pagerId() {
        return `${this._viewName}-pager`;
    };
    private datatable: Batable.IAdminPanelBaTypesDataTable;
    datatableId;

    constructor(private readonly _viewName: string,
        private readonly _mainTab: Tab.IMainTab) {
        super();
        this.datatable = Batable.getAdminPanelBaTypesDataTable(_viewName, _mainTab);
        this.datatableId = this.datatable.datatableId;
    }

    init() {
        const pager = Pager.getDataTablePager(this.pagerId);
        const dataTable = webix.extend(this.datatable.init(), { pager: this.pagerId });

        return {
            rows: [this.dataTableToolBarInit(), dataTable, pager.init()]
            
        };
    }


    dataTableToolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}`, "Добавить сущность", () => this.showAddDlg()),
        edit: ToolButtons.getEditBtn(`${this._viewName}`, "Редактировать сущность", () => this.showAddDlg(true))
    };

    showAddDlg(isEditDlg: boolean = false) {
        const datatable: webix.ui.datatable = $$(this.datatable.datatableId) as webix.ui.datatable;
        const dlg = EntityAdminDlg.getEntityAdmiDialog(this._viewName, isEditDlg, datatable, this.selectedCompany);
        dlg.showModalContent(this._mainTab, () => dlg.close()); 
    }

    dataTableToolBarInit() {
        return {
            id: `${modulesName}-toolbar`,
            view: "toolbar",
            paddingY: 3,
            elements: [
                { view: "label", label: "Типы сущностей" },
                this.dataTableToolBar.add.init(),
                this.dataTableToolBar.edit.init(),
            ]
        }
    }

    private getDisabledCheckBox(obj, common, value, config) {
        const checked = (value == config.checkValue) ? 'checked="true"' : "";
        return `<input disabled class='webix_table_checkbox' type='checkbox' ${checked}>`;
        //disabled 
    }

    load() {
        this.datatable.load();
        let datatable = $$(`${this.datatable.datatableId}`) as webix.ui.datatable;


        datatable.attachEvent("onAfterSelect",
            (x, y) => {
                this.dataTableToolBar.edit.button.enable();
            });
    }

    ready() {
        this.dataTableToolBar.add.button.enable();
    }

}

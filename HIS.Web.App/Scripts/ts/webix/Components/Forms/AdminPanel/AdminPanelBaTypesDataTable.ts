﻿import Base = require("../../ComponentBase");
import Api = require("../../../Api/ApiExporter");
import Tab = require("../../../MainView/Tabs/MainTabBase");

export function getAdminPanelBaTypesDataTable(viewName: string, module: Tab.IMainTab) {
    return new AdminPanelBaTypesDataTable(viewName, module);
}

export interface IAdminPanelBaTypesDataTable {
    ready();
    load();
    init();
    datatableId;
}

class AdminPanelBaTypesDataTable extends Base.ComponentBase implements IAdminPanelBaTypesDataTable {
    constructor(private readonly _viewName: string,
        private module: Tab.IMainTab) {
        super();
    }

    get datatableId() {
        return `${this._viewName}-baTypes-dataTable-id`;
    }
    
    init() {
        let size = 115;
        return {
            view: "datatable",
            id: `${this.datatableId}`,
            select: "row",
            resizeColumn: true,
            datafetch: 30,
            columns: [
                {
                    id: "typeId",
                    header: "ID", 
                    sort: "int",
                    adjust: "data"
                },
                {
                    id: "name",
                    header: ["Наименование", { content: "textFilter" }],
                    sort: "string",
                    adjust: "data",
                    minWidth: 453
                },
                {
                    id: "mvcAlias",
                    header: [
                        { text: "Уникальное неизменное обозначение", css: "multiline", height: 35 },
                        { content: "textFilter" }
                    ],
                    sort: "string",
                    adjust: "data",
                    minWidth: 172
                },
                {
                    id: "parent",
                    header: [{ text: "Ссылка на тип сущности-родителя", css: "multiline2" }, { content: "textFilter" }],
                    sort: "string",
                    adjust: "data",
                    minWidth: 114
                },
                {
                    id: "fixOperationHistory",
                    header: { text: "Признак журналирования", css: "multiline3" },
                    width: size,
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                },
                {
                    id: "isImplementType",
                    header: { text: "Признак реализованного типа", css: "multiline3" },
                    width: size,
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                },
                {
                    id: "isGroupType",
                    header: { text: "Признак технологической сущности", css: "multiline3" },
                    width: size,
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                },
                {
                    id: "isImplementTypeName",
                    header: [
                        {
                            text: "Наименование таблицы для хранения экземпляров",
                            css: "multiline"
                        }, { content: "textFilter" }
                    ],
                    sort: "string",
                    adjust: "data",
                    minWidth: 244
                },
                {
                    id: "searchType",
                    header: ["Форма поиска/список", { content: "selectFilter" }],
                    sort: "string",
                    adjust: true,
                    minWidth: 158
                },
                {
                    id: "shortName",
                    header: ["Краткое наименование", { content: "textFilter" }],
                    sort: "string",
                    adjust: "data",
                    minWidth: 158
                },
                {
                    id: "fullName",
                    header: [
                        {
                            text: "Дополнительное полное наименование",
                            css: "multiline",
                            height: 35
                        },
                        {
                            content: "textFilter"
                        }
                    ],
                    sort: "string",
                    fillspace: true,
                    minWidth: 223
                }
            ]
        };
    }

    private getDisabledCheckBox(obj, common, value, config) {
        const checked = (value == config.checkValue) ? 'checked="true"' : "";
        return `<input disabled class='webix_table_checkbox' type='checkbox' ${checked}>`;
    }

    load() {
        let datatable = $$(`${this.datatableId}`) as webix.ui.datatable;
        datatable.clearAll();

        webix.extend(datatable, webix.ProgressBar);
        (datatable as any).showProgress();

        Api.ApiAdminPanel.getEntities().then(result => {
            datatable.config.url = <any>{
                $proxy: true,
                load: (view, callback, params) => {

                    (datatable as any).hideProgress();
                    (webix.ajax as any).$callback(view, callback, result);
                }
            };
            datatable.load(datatable.config.url);
        });
    }

    ready() {}
}
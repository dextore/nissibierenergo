﻿import Base = require("../../../Common/DialogBase");
import Common = require("../../../Common/CommonExporter");
import Btn = require("../../Buttons/ButtonsExporter");
import adminPanelList = require("./AdminPanelBaTypesDataTable");

export function getAdminPanelListDlgView(viewName: string, controlId:string = "") {
    return new AdminPanelListDlgView(viewName, controlId);
}

class AdminPanelListDlgView extends Base.DialogBase {
    protected get formId() {
        return `${this.viewName}-AdminPanelListDlgView-Dlg-id`;
    };
    protected get labelWidth() {
        return 150 + 140;
    };
    private _list: adminPanelList.IAdminPanelBaTypesDataTable;

    constructor(viewName: string,
        private _controlId: string) {
        super(viewName);
        this._list = adminPanelList.getAdminPanelBaTypesDataTable(this.viewName, this._tabModule);
    }

    private _tabModule: Common.IMainTab;
    

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._tabModule = module;
        this._tabModule.disableCompany();
        this._list.load();
        this._list.ready();
    }

    headerLabel(): string {
        return "";
    }

    contentConfig() {
        let field = `field-id`;
        return {
            resizeColumn: true,
            autoheight: true,
            autowidth: true,
            rows: [
                this._list.init(),
                {
                    cols: [
                        {},
                        this.okBtn.init(),
                        this.cancelBtn.init()
                    ]
                }
            ],
        }
    }

    protected okBtn = Btn.getOkButton(this.viewName, () => {
        let dataitem: AdminPanel.IBaTypesModel = ($$(`${this._list.datatableId}`) as webix.ui.datatable).getSelectedItem();
        let control = ($$(this._controlId) as any);

        control.setValue(dataitem.name);
        control.define("markerId", `${dataitem.typeId}`);

        this.okClose();
    });

    protected cancelBtn = Btn.getCancelButton(this.viewName, () => {
        this.okClose();
    });

    getDataFromDialog() {
        const form = $$(this.formId) as webix.ui.form;
        let someValues = form.getValues();

        return someValues;
    }
}
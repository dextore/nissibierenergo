﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Pager = require("../../Pager/Pager");
import ToolButtons = require("../../Buttons/ToolBarButtons");
import StateLinkDlg = require("../../../Modules/AdminPanel/StatesAdminDlg");
export const modulesName = "admin-panel-module";

export interface IAvailableStateList extends Base.IComponent {
    ready();
    load();
}

export function getAdminAvailableStateList(module: Tab.IMainTab, adminPanelEntityList): IAvailableStateList {
    return new AdminAvailableStateList(`${modulesName}-tab`, module, adminPanelEntityList);
}

export function getAdminAvailableStateListForDlg(module: Tab.IMainTab, adminPanelEntityList, callFromDlg): IAvailableStateList {
    return new AdminAvailableStateList(`${modulesName}-tab-AdminAvailableStateListForDlg`, module, adminPanelEntityList, callFromDlg);
}

class AdminAvailableStateList extends Base.ComponentBase implements IAvailableStateList {
    baTypeId: number;
    baId: number;

    get datatableId() {
        return `${this._viewName}-AvailableState-datatable-id${this.callFromDlg}`;
    }
    get pagerId() {
        return `${this._viewName}-AvailableState-pager${this.callFromDlg}`;
    };

    constructor(private readonly _viewName: string,
        private readonly _mainTab: Tab.IMainTab,
        private readonly adminPanelEntityList,
        private readonly callFromDlg: string = "") {
        super();
    }

    init() {
        const pager = Pager.getDataTablePager(this.pagerId);
        const dataTable = webix.extend(this.datatableInit(), { pager: this.pagerId });
        return {
            rows: [this.dataTableToolBarInit(), dataTable, pager.init()]

        };
    }

    dataTableToolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}-add-dlg${this.callFromDlg}`, "Добавить состояние", () => this.showAddEditDlg(false)),
        edit: ToolButtons.getEditBtn(`${this._viewName}-edit-dlg${this.callFromDlg}`, "Редактировать состояние", () => this.showAddEditDlg(true)),
        del: ToolButtons.getRemoveBtn(`${this._viewName}-del-dlg${this.callFromDlg}`, "Удалить состояние", () => this.deleteStateFromEntitysType())
    };

    dataTableToolBarInit() {
        return {
            id: `${modulesName}-AvailableState-toolbar${this.callFromDlg}`,
            view: "toolbar",
            paddingY: 3,
            elements: [
                { view: "label", label: "Состояния типа сущности" },
                this.dataTableToolBar.add.init(),
                this.dataTableToolBar.edit.init(),
                this.dataTableToolBar.del.init()
            ]
        }
    }

    datatableInit() {
        return {
            view: "datatable",
            id: this.datatableId,
            select: "row",
            resizeColumn: true,
            datafetch: 30,
            columns: [
                {
                    id: "stateId",
                    header: "ID",
                    sort: "int",
                    fillspace: true
                },
                {
                    id: "stateName",
                    header: ["Состояние", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                },
                {
                    id: "isFirstState",
                    header: { text: "Признак начального состояния" },
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                    fillspace: true,
                },
                {
                    id: "isDelState",
                    header: { text: "Признак удаленного (архивного) состояния" },
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                    fillspace: true
                }
            ],
        };
    }

    private getDisabledCheckBox(obj, common, value, config) {
        const checked = (value == config.checkValue) ? 'checked="true"' : "";
        return `<input disabled class='webix_table_checkbox' type='checkbox' ${checked}>`;
    }

    load() {
        
    }

    ready() {
        this.dataTableToolBar.add.button.enable();
        this.dataTableToolBar.del.button.disable();
        this.dataTableToolBar.edit.button.disable();

        if (!this.adminPanelEntityList.datatableId) {
            let selectedBaId = (this.adminPanelEntityList.getSelectedItem() as AdminPanel.IBaTypesModel).typeId;
            let datatable = $$(this.datatableId) as webix.ui.datatable;
            datatable.clearAll();

            webix.extend(datatable, webix.ProgressBar);
            (datatable as any).showProgress();

            Api.ApiAdminPanel.getAvailableSteteForEntiti(selectedBaId).then(result => {
                datatable.config.url = <any>{
                    $proxy: true,
                    load: (view, callback, params) => {

                        (datatable as any).hideProgress();
                        (webix.ajax as any).$callback(view, callback, result);
                    }
                };

                datatable.load(datatable.config.url);
            });

            datatable.attachEvent("onAfterSelect", () => {
                this.dataTableToolBar.del.button.enable();
                this.dataTableToolBar.edit.button.enable();
            });
        } else {
            const entityDataTable = $$(`${this.adminPanelEntityList.datatableId}`) as webix.ui.datatable;

            entityDataTable.attachEvent("onAfterSelect", (selection, preserve) => {

                const selectedBaId = (entityDataTable.getSelectedItem() as AdminPanel.IBaTypesModel).typeId;
                let datatable = $$(this.datatableId) as webix.ui.datatable;
                datatable.clearAll();

                webix.extend(datatable, webix.ProgressBar);
                (datatable as any).showProgress();

                Api.ApiAdminPanel.getAvailableSteteForEntiti(selectedBaId).then(result => {
                    datatable.config.url = <any>{
                        $proxy: true,
                        load: (view, callback, params) => {

                            (datatable as any).hideProgress();
                            (webix.ajax as any).$callback(view, callback, result);
                        }
                    };

                    datatable.load(datatable.config.url);
                });
                this.baId = entityDataTable.getSelectedItem().typeId;

                datatable.attachEvent("onAfterSelect", () => {
                    this.dataTableToolBar.del.button.enable();
                    this.dataTableToolBar.edit.button.enable();
                });
            });
        }
    }

    showAddEditDlg(isEdit: boolean) {
        const datatable = $$(this.datatableId) as webix.ui.datatable;
        const dlg = StateLinkDlg.getStatesAdminDlg(this._viewName, isEdit, datatable, null, this.baId);
        dlg.showModalContent(this._mainTab, () => dlg.close());
    }

    deleteStateFromEntitysType() {
        let self = this;
        webix.confirm({
            title: "Подтверждение",
            type: "confirm-warning",
            text: `Отвязать состояние?`,
            ok: "Да", cancel: "Нет",
            callback(result) {
                if (result) {
                    const datatable = $$(self.datatableId) as webix.ui.datatable;
                    const { stateId, id } = datatable.getSelectedItem() as any;
                    webix.extend(datatable, webix.ProgressBar);
                    (datatable as any).showProgress();

                    Api.ApiAdminPanel.deleteStateFromBaType(self.baId, stateId).then(result => {
                        if (result) {
                            (datatable as any).hideProgress();
                            datatable.remove(id);
                            self.dataTableToolBar.edit.button.disable();
                            self.dataTableToolBar.del.button.disable();
                        }
                    }).catch(() => {
                        (datatable as any).hideProgress();
                    });

                    (datatable as any).hideProgress();
                    self.dataTableToolBar.edit.button.disable();
                    self.dataTableToolBar.del.button.disable();
                }
            }
        });
    }
}

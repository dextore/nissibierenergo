﻿import Base = require("../../../Common/DialogBase");
import Common = require("../../../Common/CommonExporter");
import Btn = require("../../Buttons/ButtonsExporter");
import Api = require("../../../Api/ApiExporter");

export function getAdminPanelOnlyOperationsListDlgAddEdit(viewName: string, isEdit: boolean, datatable: webix.ui.datatable, selectedCompany: Inventory.ILSCompanyAreas) {
    return new AdminPanelOnlyOperationListDlgAddEdit(viewName, isEdit, datatable, selectedCompany);
}

class AdminPanelOnlyOperationListDlgAddEdit extends Base.DialogBase {
    private _item: AdminPanel.IBaTypesOperationsCreateModel;
    protected get formId() {
        return `${this.viewName}-AdminPanelOnlyOperationListDlgAddEdit-Dlg-id`;
    };
    protected get labelWidth() {
        return 150 + 140;
    };

    constructor(viewName: string,
        private isEdit: boolean,
        private datatable: webix.ui.datatable,
        private selectedCompany: Inventory.ILSCompanyAreas) {
        super(viewName);
        if (isEdit)
            this._item = datatable.getSelectedItem() as AdminPanel.IBaTypesOperationsCreateModel;
    }

    private _tabModule: Common.IMainTab;

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._tabModule = module;
        this._tabModule.disableCompany();

        if (this.isEdit) {
            
        } else {
            
        }
    }

    headerLabel(): string {
        return `${this.isEdit ? "Редактирование" : "Добавление"} операции`;
    }

    contentConfig() {
        return {
            view: "form",
            id: this.formId,
            minheight: 600,
            autoheight: true,
            elements: [
                {
                    view: "text",
                    id: `${this.formId}-operationName-field-id`,
                    label: "Наименование",
                    name: "name",
                    required: true,
                    labelWidth: this.labelWidth,
                    value: this.isEdit ? this._item.operationName : "",
                },
                {
                    cols: [
                        {},
                        this.okBtn.init(),
                        this.cancelBtn.init()
                    ]
                }
            ],
            rules: {
                "name": webix.rules.isNotEmpty
            }
        }
    }

    protected okBtn = Btn.getOkButton(this.viewName, () => {
        if (($$(this.formId) as webix.ui.form).validate()) {
            const data = this.getDataFromDialog();

            Api.ApiAdminPanel.createUpdateOperations(data).then(result => {
                if (result) {
                    if (!this.isEdit) {
                        this.datatable.add(result, 0);
                    }
                    else {
                        const { id } = this.datatable.getSelectedItem();
                        this.datatable.updateItem(id, result);
                    }
                    this.okClose();
                }
            });
        }
    });

    protected cancelBtn = Btn.getCancelButton(this.viewName, () => {
        this.okClose();
    });

    getDataFromDialog() {
        const form = $$(this.formId) as any;

        let data = [];
        let self = this;

        form._collection.forEach(
            (val) => {
                let id = ($$(val.id).config.id as string).substring(0, ($$(val.id).config.id as string).length - "-field-id".length);
                id = id.substring(self.formId.length + 1);
                data[id] = ($$(val.id).config as any).value;
            }
        );

        let result: AdminPanel.IBaTypesOperationsCreateModel = {
            operationId: this.isEdit? this._item.operationId : null,
            operationName: data["operationName"]
        };

        return result;
    }
}
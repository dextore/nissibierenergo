﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Pager = require("../../Pager/Pager");
import ToolButtons = require("../../Buttons/ToolBarButtons");
import AddEditOperationDlg = require("./AdminPanelOnlyOperationListDlgAddEdit");
export const modulesName = "adminPanelOnlyOperationList-panel-module";

export interface IAdminPanelOnlyOperationList extends Base.IComponent {
    ready();
    load();
    selectedCompany: Inventory.ILSCompanyAreas;
}

export function getAdminPanelOnlyOperationList(module: Tab.IMainTab, view: string = ""): IAdminPanelOnlyOperationList {
    return new AdminPanelOnlyOperationList(`${modulesName}${view}-tab`, module);
}

class AdminPanelOnlyOperationList extends Base.ComponentBase implements IAdminPanelOnlyOperationList {
    selectedCompany: Inventory.ILSCompanyAreas;
    get datatableId() {
        return `${this._viewName}-AdminPanelOnlyOperationList-datatable-id`;
    }
    get pagerId() {
        return `${this._viewName}-AdminPanelOnlyOperationList-pager`;
    };

    constructor(private readonly _viewName: string,
        private readonly _mainTab: Tab.IMainTab) {
        super();
    }

    init() {
        const pager = Pager.getDataTablePager(this.pagerId);
        const dataTable = webix.extend(this.datatableInit(), { pager: this.pagerId });
        return {
            rows: [this.dataTableToolBarInit(), dataTable, pager.init()]
        };
    }

    dataTableToolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}-operation-operation-toolbar`, "Добавить операцию", () => this.showAddDlg()),
        edit: ToolButtons.getEditBtn(`${this._viewName}-operation-operation-toolbar`, "Редактировать операцию", () => this.showAddDlg(true)),
        del: ToolButtons.getRemoveBtn(`${this._viewName}-operation-operation-toolbar`, "Удалить операцию", () => this.deleteOperations())
    };

    dataTableToolBarInit() {
        return {
            id: `${this._viewName}-AdminPanelOnlyOperationList-toolbar`,
            view: "toolbar",
            paddingY: 3,
            elements: [
                { view: "label", label: "Список доступных операций" },
                this.dataTableToolBar.add.init(),
                this.dataTableToolBar.edit.init(),
                this.dataTableToolBar.del.init()
            ]
        }
    }

    datatableInit() {
        return {
            view: "datatable",
            id: this.datatableId,
            select: "row",
            resizeColumn: true,
            datafetch: 30,
            columns: [
                {
                    id: "operationId",
                    header: "ID",
                    sort: "int",
                    fillspace: true
                },
                {
                    id: "operationName",
                    header: ["Наименование", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                }
            ],
        };
    }

    load() {
            let datatable = $$(this.datatableId) as webix.ui.datatable;
            datatable.clearAll();

            webix.extend(datatable, webix.ProgressBar);
            (datatable as any).showProgress();

            Api.ApiAdminPanel.getAllOperations().then(result => {
                datatable.config.url = <any>{
                    $proxy: true,
                    load: (view, callback, params) => {

                        (datatable as any).hideProgress();
                        (webix.ajax as any).$callback(view, callback, result);
                    }
                };

                datatable.load(datatable.config.url);
            });
    }

    ready() {
        this.dataTableToolBar.add.button.enable();
        this.dataTableToolBar.edit.button.disable();
        this.dataTableToolBar.del.button.disable();

        const datatable = $$(this.datatableId) as webix.ui.datatable;
        datatable.attachEvent("onAfterSelect",
            () => {
                this.dataTableToolBar.edit.button.enable();
                this.dataTableToolBar.del.button.enable();
            });
    }

    showAddDlg(isEdit: boolean = false) {
        const datatable = $$(this.datatableId) as webix.ui.datatable;
        const dlg = AddEditOperationDlg.getAdminPanelOnlyOperationsListDlgAddEdit(this._viewName,
            isEdit,
            datatable,
            this.selectedCompany);
        let self = this;
        dlg.showModalContent(self._mainTab, () => {
            dlg.close();
        });
    }

    deleteOperations() {
        let self = this;
        webix.confirm({
            title: "Подтверждение",
            type: "confirm-warning",
            text: `Выполнить операцию удаления операции?`,
            ok: "Да", cancel: "Нет",
            callback(result) {
                if (result) {
                    const datatable = $$(self.datatableId) as webix.ui.datatable;
                    const { operationId, id } = datatable.getSelectedItem() as any;

                    webix.extend(datatable, webix.ProgressBar);
                    (datatable as any).showProgress();

                    Api.ApiAdminPanel.deleteOperations(operationId).then(result => {
                        if (result) {
                            (datatable as any).hideProgress();
                            datatable.remove(id);
                            self.dataTableToolBar.edit.button.disable();
                            self.dataTableToolBar.del.button.disable();
                        }
                    }).catch(() => {
                        (datatable as any).hideProgress();
                    });
                }
            }
        });


        
    }
}

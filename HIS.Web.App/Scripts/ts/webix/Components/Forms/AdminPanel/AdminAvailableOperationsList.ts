﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Pager = require("../../Pager/Pager");
import ToolButtons = require("../../Buttons/ToolBarButtons");
import OperationLinkDlg = require("../../../Modules/AdminPanel/OperationsAdminDlg");
export const modulesName = "admin-panel-module";

export interface IAdminAvailableOperationsList extends Base.IComponent {
    ready();
    load();
}

export function getAdminAvailableOperationsList(module: Tab.IMainTab, adminPanelEntityList): IAdminAvailableOperationsList {
    return new AdminAvailableOperationsList(`${modulesName}-tab`, module, adminPanelEntityList);
}

export function getAdminAvailableOperationsListForDlg(module: Tab.IMainTab, adminPanelEntityList, callFromDlg): IAdminAvailableOperationsList {
    return new AdminAvailableOperationsList(`${modulesName}-tab-AdminAvailableOperationsListForDlg`, module, adminPanelEntityList, callFromDlg);
}

class AdminAvailableOperationsList extends Base.ComponentBase implements IAdminAvailableOperationsList {
    baTypeId: number;
    baId: number;

    get datatableId() {
        return `${this._viewName}-AdminAvailableOperationsList-datatable-id${this.callFromDlg}`;
    }
    get pagerId() {
        return `${this._viewName}-AdminAvailableOperationsList-pager${this.callFromDlg}`;
    };

    constructor(private readonly _viewName: string,
        private readonly _mainTab: Tab.IMainTab,
        private readonly adminPanelEntityList,
        private readonly callFromDlg: string = "") {
        super();
    }

    init() {
        const pager = Pager.getDataTablePager(this.pagerId);
        const dataTable = webix.extend(this.datatableInit(), { pager: this.pagerId });
        return {
            rows: [this.dataTableToolBarInit(), dataTable, pager.init()]

        };
    }

    dataTableToolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}-add-operation-dlg${this.callFromDlg}`, "Добавить операцию", () => this.showAddEditDlg(false)),
        edit: ToolButtons.getEditBtn(`${this._viewName}-edit-operation-dlg${this.callFromDlg}`, "Редактировать операцию", () => this.showAddEditDlg(true)),
        del: ToolButtons.getRemoveBtn(`${this._viewName}-del-operation-dlg${this.callFromDlg}`, "Удалить операцию", () => this.deleteStateFromEntitysType())
    };

    dataTableToolBarInit() {
        return {
            id: `${modulesName}-AdminAvailableOperationsList-toolbar${this.callFromDlg}`,
            view: "toolbar",
            paddingY: 3,
            elements: [
                { view: "label", label: "Операции типа сущности" },
                this.dataTableToolBar.add.init(),
                this.dataTableToolBar.edit.init(),
                this.dataTableToolBar.del.init()
            ]
        }
    }

    datatableInit() {
        return {
            view: "datatable",
            id: this.datatableId,
            select: "row",
            resizeColumn: true,
            datafetch: 30,
            columns: [
                {
                    id: "operationId",
                    header: "ID",
                    sort: "int",
                    fillspace: true
                },
                {
                    id: "operationName",
                    header: ["Операция", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                },
                {
                    id: "isCreateOperation",
                    header: { text: "Операция создания" },
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                    fillspace: true,
                },
                {
                    id: "isDeleteOperation",
                    header: { text: "Операция удаления (архивирования)" },
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                    fillspace: true
                }
            ],
        };
    }

    private getDisabledCheckBox(obj, common, value, config) {
        const checked = (value == config.checkValue) ? 'checked="true"' : "";
        return `<input disabled class='webix_table_checkbox' type='checkbox' ${checked}>`;
    }

    load() {

    }

    ready() {
        this.dataTableToolBar.add.button.enable();
        this.dataTableToolBar.del.button.disable();
        this.dataTableToolBar.edit.button.disable();

        if (!this.adminPanelEntityList.datatableId) {
            let selectedBaId = (this.adminPanelEntityList.getSelectedItem() as AdminPanel.IBaTypesModel).typeId;
            let datatable = $$(this.datatableId) as webix.ui.datatable;
            datatable.clearAll();

            webix.extend(datatable, webix.ProgressBar);
            (datatable as any).showProgress();

            Api.ApiAdminPanel.getAvailableOperationsForEntiti(selectedBaId).then(result => {
                datatable.config.url = <any>{
                    $proxy: true,
                    load: (view, callback, params) => {

                        (datatable as any).hideProgress();
                        (webix.ajax as any).$callback(view, callback, result);
                    }
                };

                datatable.load(datatable.config.url);
            });

            datatable.attachEvent("onAfterSelect", () => {
                this.dataTableToolBar.del.button.enable();
                this.dataTableToolBar.edit.button.enable();
            });
        } else {
            const entityDataTable = $$(`${this.adminPanelEntityList.datatableId}`) as webix.ui.datatable;
            entityDataTable.attachEvent("onAfterSelect", (selection, preserve) => {
                var selectedEntity = entityDataTable.getSelectedItem() as AdminPanel.IBaTypesModel;

                let datatable = $$(this.datatableId) as webix.ui.datatable;
                datatable.clearAll();

                webix.extend(datatable, webix.ProgressBar);
                (datatable as any).showProgress();

                Api.ApiAdminPanel.getAvailableOperationsForEntiti(selectedEntity.typeId).then(result => {
                    datatable.config.url = <any>{
                        $proxy: true,
                        load: (view, callback, params) => {

                            (datatable as any).hideProgress();
                            (webix.ajax as any).$callback(view, callback, result);
                        }
                    };

                    datatable.load(datatable.config.url);
                });
                this.baId = entityDataTable.getSelectedItem().typeId;

                datatable.attachEvent("onAfterSelect", () => {
                    this.dataTableToolBar.del.button.enable();
                    this.dataTableToolBar.edit.button.enable();
                });

            });
        }
    }

    showAddEditDlg(isEdit: boolean) {
        const datatable = $$(this.datatableId) as webix.ui.datatable;
        const dlg = OperationLinkDlg.getOperationsAdminDlg(this._viewName, isEdit, datatable, null, this.baId);
        dlg.showModalContent(this._mainTab, () => dlg.close());
    }

    deleteStateFromEntitysType() {
        let self = this;
        webix.confirm({
            title: "Подтверждение",
            type: "confirm-warning",
            text: `Отвязать операцию?`,
            ok: "Да", cancel: "Нет",
            callback(result) {
                if (result) {
                    const datatable = $$(self.datatableId) as webix.ui.datatable;
                    const { operationId , id } = datatable.getSelectedItem() as any;
                    webix.extend(datatable, webix.ProgressBar);
                    (datatable as any).showProgress();

                    Api.ApiAdminPanel.deleteOperationFromBaType(self.baId, operationId).then(result => {
                        if (result) {
                            (datatable as any).hideProgress();
                            datatable.remove(id);
                            self.dataTableToolBar.edit.button.disable();
                            self.dataTableToolBar.del.button.disable();
                        }
                    }).catch(() => {
                        (datatable as any).hideProgress();
                    });

                    (datatable as any).hideProgress();
                    self.dataTableToolBar.edit.button.disable();
                    self.dataTableToolBar.del.button.disable();
                }
            }
        });
    }
}

﻿import Base = require("../../../Common/DialogBase");
import Common = require("../../../Common/CommonExporter");
import Btn = require("../../Buttons/ButtonsExporter");
import Api = require("../../../Api/ApiExporter");

export function getAdminAvailableValuesAddEditDlg(viewName: string, isEdit: boolean, datatable: webix.ui.datatable, markerId) {
    return new AdminAvailableValuesAddEditDlg(viewName, isEdit, datatable, markerId);
}

class AdminAvailableValuesAddEditDlg extends Base.DialogBase {
    private _item: AdminPanel.IItemTableViewModel;
    private _markerId: number;


    protected get formId() {
        return `${this.viewName}-AdminAvailableValuesAddEditDlg-AdminAvailableValuesAddEditDlg-Dlg-id`;
    };
    protected get labelWidth() {
        return 150 + 140;
    };

    constructor(viewName: string,
        private isEdit: boolean,
        private datatable: webix.ui.datatable,
        markerId: number) {
        super(viewName);
        this._markerId = markerId;
        if (isEdit)
            this._item = datatable.getSelectedItem() as AdminPanel.IItemTableViewModel;
    }

    private _tabModule: Common.IMainTab;

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._tabModule = module;
        this._tabModule.disableCompany();
    }

    headerLabel(): string {
        return `${this.isEdit ? "Редактирование" : "Добавление"} значения для маркера`;
    }

    contentConfig() {
        let field = `field-id`;
        return {
            view: "form",
            id: this.formId,
            minheight: 600,
            autoheight: true,
            elements: [
                {
                    id: `${this.formId}-itemName-${field}`,
                    label: "Значение",
                    labelWidth: this.labelWidth,
                    view: "text",
                    required: true,
                    name: "value",
                    value: this.isEdit ? this._item.itemName : ""
                },
                {
                    id: `${this.formId}-orderNumber-${field}`,
                    label: "Порядковый номер",
                    labelWidth: this.labelWidth,
                    view: "text",
                    value: this.isEdit ? this._item.orderNumber : ""
                },
                {
                    id: `${this.formId}-isActive-${field}`,
                    label: "Признак aктивности",
                    labelWidth: this.labelWidth,
                    view: "checkbox",
                    value: this.isEdit ? this._item.isActive : 0
                },
                {
                    cols: [
                        {},
                        this.okBtn.init(),
                        this.cancelBtn.init()
                    ]
                }
            ],
            rules: {
                "value": webix.rules.isNotEmpty
            }
        }
    }

    protected okBtn = Btn.getOkButton(`${this.viewName}-AdminAvailableValuesAddEditDlg`, () => {
        if (($$(this.formId) as webix.ui.form).validate()) {
            const data = this.getDataFromDialog();
            
            Api.ApiAdminPanel.createUpdateListItem(this._markerId, data).then(result => {
                if (!this.isEdit) {
                    this.datatable.add(result, 0);
                } else {
                    let { id } = this.datatable.getSelectedItem();
                    this.datatable.updateItem(id, result);
                }

                this.okClose();
            });

        }
    });

    protected cancelBtn = Btn.getCancelButton(`${this.viewName}-AdminAvailableValuesAddEditDlg`, () => {
        this.okClose();
    });

    getDataFromDialog() {
        const form = $$(this.formId) as any;
        let data = [];
        let self = this;

        form._collection.forEach(
            (val) => {
                let id = ($$(val.id).config.id as string).substring(0, ($$(val.id).config.id as string).length - "-field-id".length);
                id = id.substring(self.formId.length + 1);
                data[id] = ($$(val.id).config as any).value;
            }
        );
        data["itemId"] = this.isEdit ? this._item.itemId : null;

        const result: AdminPanel.IItemTableViewModel = {
            itemId: Number(data["itemId"]) === 0 ? null : Number(data["itemId"]),
            isActive: Boolean(data["isActive"]),
            itemName: data["itemName"],
            orderNumber: data["orderNumber"] === ""
                ? null
                : Number(data["orderNumber"])
        };

        return result;
    }
}
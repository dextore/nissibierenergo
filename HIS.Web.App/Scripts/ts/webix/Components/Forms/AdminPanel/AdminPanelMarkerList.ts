﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Pager = require("../../Pager/Pager");
import AdminAvailableValuesDlg = require("./AdminAvailableValuesDlg");
import ToolButtons = require("../../Buttons/ToolBarButtons");
import MarkersAdminDlg = require("../../../Modules/AdminPanel/MarkersAdminDlg");


export const modulesName = "admin-panel-module";

export interface IAdminPanelMarkerList extends Base.IComponent {
    selectedCompany: Inventory.ILSCompanyAreas;
    ready();
    load();
}

export function getAdminPanelMarkerList(module: Tab.IMainTab, adminPanelEntityList): IAdminPanelMarkerList {
    return new AdminPanelMarkerList(`${modulesName}-tab`, module, adminPanelEntityList);
}

class AdminPanelMarkerList extends Base.ComponentBase implements IAdminPanelMarkerList {
    baTypeId: number;
    selectedCompany: Inventory.ILSCompanyAreas;

    get datatableId() {
        return `${this._viewName}-markers-datatable-id`;
    }
    get pagerId() {
        return `${this._viewName}-markers-pager`;
    };

    constructor(private readonly _viewName: string,
        private readonly _mainTab: Tab.IMainTab,
        private readonly adminPanelEntityList) {
        super(); 
    }

    init() {
        const pager = Pager.getDataTablePager(this.pagerId);
        const dataTable = webix.extend(this.datatableInit(), { pager: this.pagerId });
        return {
            rows: [this.dataTableToolBarInit(), dataTable, pager.init()]

        };
    }

    dataTableToolBar = {
        add: ToolButtons.getNewBtn(`${this._viewName}-markers-toolbar-addbutton`, "Добавить маркер", () => this.showAddDlg() ),
        edit: ToolButtons.getEditBtn(`${this._viewName}-markers-toolbar-editbutton`, "Изменить маркер", () => this.showAddDlg(true)),
        del: ToolButtons.getRemoveBtn(`${this._viewName}-markers-operation-toolbar`, "Удалить маркер (coming soon in december)", () => this.deleteMarkerFromBaType())
    };

    dataTableToolBarInit() {
        return {
            id: `${modulesName}-markers-toolbar`,
            view: "toolbar",
            paddingY: 3,
            elements: [
                { view: "label", label: "Маркеры типа сущности" },
                this.dataTableToolBar.add.init(),
                this.dataTableToolBar.edit.init(),
                this.dataTableToolBar.del.init()
            ]
        }
    }

    showAddDlg(isEdit: boolean = false) {
        const datatable = $$(this.datatableId) as webix.ui.datatable;
        const dlg = MarkersAdminDlg.getMarkersAdminDlg(this._viewName, isEdit, datatable, this.selectedCompany, this.baTypeId);
        dlg.showModalContent(this._mainTab, () => dlg.close());
    }

    datatableInit() {
        let size = 205;
        const listMarkerType = 5;
        const referenceMarkerType = 13;
        return {
            view: "datatable",
            id: this.datatableId,
            select: "row",
            resizeColumn: true,
            datafetch: 30,
            columns: [
                {
                    id: "id",
                    header: "ID",
                    sort: "int",
                    adjust: "data"
                },
                {
                    id: "name",
                    header: ["Наименование", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                },
                {   
                    id: "mvcAlias",
                    header: [
                        { text: "Уникальное неизменное обозначение" }, { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                },
                {
                    id: "baTypeLink",
                    header: ["Ссылка на тип сущности", { content: "textFilter" }],
                    sort: "string",
                    adjust: true
                },
                {
                    id: "markerType",
                    header: ["Тип маркера", { content: "selectFilter" }],
                    template: (row: AdminPanel.IMarkersAdminModel) => {
                        if (row.baTypeLinkId === listMarkerType) {
                            return `<span style="float:left">${row.markerType}</span> <span class='webix_icon  fa-list-ul' style="float:right; line-height: 26px;"></span>`;
                        }
                        else if (row.baTypeLinkId === referenceMarkerType) {
                            return `<span style="float:left">${row.markerType}</span> <span class='webix_icon  fa-list-ul' style="float:right; line-height: 26px;"></span>`;
                        } else {
                            return `${row.markerType}`;
                        }
                    },
                    //sort: "string",
                    adjust: true
                },
                {
                    id: "isPeriodic",
                    header: { text: "Признак периодичности", width: size, css: "multiline3" },
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                },
                {
                    id: "isCollectible",
                    header: { text: "Признак коллекции", width: size, css: "multiline3" },
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                },
                {
                    id: "isBlocked",
                    header: { text: "Признак блокировки заполнения", width: size, css: "multiline3" },
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                },
                {
                    id: "isFixMarkerHistory",
                    header: { text: "Признак фиксирования истории", width: size, css: "multiline3" },
                    template: (obj, common, value, config) => {     
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                },
                {
                    id: "isImplementTypeField",
                    header: { text: "Признак реализованного типа ", width: size, css: "multiline3" },
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                },
                {
                    id: "isRequired",
                    header: { text: "Признак обязательности заполнения", width: size, css: "multiline3" },
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                },
                {
                    id: "isOptional",
                    header: { text: "Признак опционального маркера", width: size, css: "multiline3" },
                    template: (obj, common, value, config) => {
                        return this.getDisabledCheckBox(obj, common, value, config);
                    },
                },
                {
                    id: "overrideName",
                    header: ["Переопределенное наименование", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true
                },
                {
                    id: "implementTypeName",
                    header: [{ text: "Таблица хранения значений", css: "multiline4" } , { content: "textFilter" }],
                    sort: "string",
                    fillspace: true,
                    adjust: true
                },
                {
                    id: "implementTypeField",
                    header: ["Поле хранения значений", { content: "textFilter" }],
                    sort: "string",
                    fillspace: true,
                    adjust: true
                }
            ],
            onClick: {
                "fa-list-ul": (ev, id) => {
                    const datatable = $$(`${this.datatableId}`) as webix.ui.datatable;
                    const clickedItem: AdminPanel.IMarkersAdminModel = datatable.getItem(id.row);
                    const dlg: AdminAvailableValuesDlg.IAdminAvailableValuesDlg =
                        AdminAvailableValuesDlg.getAdminAvailableValuesDlg(this._viewName);
                    switch (clickedItem.baTypeLinkId) {
                    case 5: {
                            Api.ApiAdminPanel.getAvailableValuesForList(id.row).then(result => {
                                console.log("resultFromServer: ", result);
                                console.log("markerid: ", clickedItem.id);
                                dlg.dialogData = result;
                                dlg.markerId = clickedItem.id;
                                dlg.header = `Доступные значения для перечня <b>${clickedItem.name}</b>`;
                                dlg.showModal(() => {
                                    dlg.close();
                                });
                            });
                            break;
                        }
                    case 13:
                    {
                        Api.ApiAdminPanel.getAvailableValuesForReference(id.row).then(result => {
                            console.log("resultFromServer: ", result);
                            dlg.dialogData = result;
                            dlg.header = `Доступные значения для справочника <b>${clickedItem.name}</b>`;
                            dlg.showModal(() => {
                                dlg.close(); 
                            });
                        });
                        break;
                    }
                    default: break;
                    }
                }
            }
        };
    }

    private getDisabledCheckBox(obj, common, value, config) {
        const checked = (value == config.checkValue) ? 'checked="true"' : "";
        return `<input disabled class='webix_table_checkbox' type='checkbox' ${checked}>`;
    }

    load() {

    }

    ready() {
        const entityDataTable = $$(`${this.adminPanelEntityList.datatableId}`) as webix.ui.datatable;
        entityDataTable.attachEvent("onAfterSelect", (selection, preserve) => {
            var selectedEntity = entityDataTable.getSelectedItem() as AdminPanel.IBaTypesModel;

            let datatable = $$(this.datatableId) as webix.ui.datatable;
            datatable.clearAll();
            this.dataTableToolBar.edit.button.disable();
            this.dataTableToolBar.del.button.disable();
            webix.extend(datatable, webix.ProgressBar);
            (datatable as any).showProgress();
            this.baTypeId = selectedEntity.typeId;
            Api.ApiAdminPanel.getMarkers(selectedEntity.typeId).then(result => {
                datatable.config.url = <any>{
                    $proxy: true,
                    load: (view, callback, params) => {

                        (datatable as any).hideProgress();
                        datatable.attachEvent("onAfterSelect", (selection, preserve) => {
                            this.dataTableToolBar.edit.button.enable();
                            this.dataTableToolBar.del.button.enable();
                        });
                        (webix.ajax as any).$callback(view, callback, result);
                    }
                };

                datatable.load(datatable.config.url);
            });
        });
        this.dataTableToolBar.add.button.enable();
    }

    deleteMarkerFromBaType() {
        webix.confirm({
            title: "Подтверждение",
            type: "confirm-warning",
            text: `Выполнить операцию "отвязки" маркера?`,
            ok: "Да", cancel: "Нет",
            callback(result) {
                if (result) {
                    const entityDataTable = $$(`${this.adminPanelEntityList.datatableId}`) as webix.ui.datatable;
                    const datatable = $$(this.datatableId) as webix.ui.datatable;
                    const { typeId } = entityDataTable.getSelectedItem() as AdminPanel.IBaTypesModel;
                    const underDelItem = datatable.getSelectedItem() as AdminPanel.IMarkersAdminModel;

                    Api.ApiAdminPanel.deleteMarkerFromBaType(typeId, underDelItem).then(result => {
                        if (result) {
                            datatable.remove(underDelItem.id);
                            this.dataTableToolBar.del.button.disable();
                            this.dataTableToolBar.edit.button.disable();
                        }
                    });
                }
            }
        });
    }
}
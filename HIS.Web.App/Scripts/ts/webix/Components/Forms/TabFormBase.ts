﻿import Common = require("../../Common/CommonExporter");
import Itnerfaces = require("../../Abstractions/InterfacesExporter");

export interface ITabForm {
    tabViewId: string;
    tabHeader: string;
    init(): any;
    destroy();
    subscribeCallback(callback: (arg: Itnerfaces.IFormCallBackArgs) => void, subscriber: any, subscriberId: string): Common.ITypedSubscription<Itnerfaces.IFormCallBackArgs>;
}


export abstract class TabFormBase implements ITabForm  {

    protected static _viewName = "module-base";
    protected static _header = "";

    private readonly _parentTab: Common.IMainFormTab; 

    private _entityName?: string;
    private _callbacksEvent = new Common.Event<Itnerfaces.IFormCallBackArgs>();
    private _subscribers: { [id: string]: Common.ITypedSubscription<Itnerfaces.IFormCallBackArgs> } = {};

    static getTabId(baId?: number): string {
        return (!!baId) ? `${this._viewName}-${baId}-id` : `${this._viewName}-new-id`;
    }

    static showForm(formSubscriber: Itnerfaces.ITabFromSubscriber,
        formBuilder: () => ITabForm,
        tabId: string) {

        const formTab = formSubscriber.mainTabView.showForm(tabId, () => {
            const form = formBuilder();
            return new Common.MainFormTab(formSubscriber.mainTabView, form);
        });

        return formTab.form.subscribeCallback(formSubscriber.formCallback, formSubscriber, formSubscriber.viewId);
    }

    get tabViewId(): string {
        return (this.constructor as typeof TabFormBase).getTabId(this._baId);
    }  

    get viewName(): string {
        return (!!this._baId) ? `${(this.constructor as typeof TabFormBase)._viewName}-${this._baId}` : `${(this.constructor as typeof TabFormBase)._viewName}-new`;
    }  

    get tabHeader(): string {
        return `${(this.constructor as typeof TabFormBase)._header} ${(this._entityName) ? this._entityName : "(новый)"}`;
    }  

    get parentTab(): Common.IMainFormTab {
        return this._parentTab;
    }

    constructor(private _mainTabView: Common.IMainTabbarView, private _baId?: number){}

    abstract init();

    load(baId?: number, entityName?: string) {
        this._baId = baId;
        this._entityName = entityName;
    }

    subscribeCallback(callback: (arg: Itnerfaces.IFormCallBackArgs) => void, subscriber: any, subscriberId: string): Common.ITypedSubscription<Itnerfaces.IFormCallBackArgs> {
        if (!this._subscribers[subscriberId]) {
            this._subscribers[subscriberId] = this._callbacksEvent.subscribe(callback, subscriber);
        }
        return this._subscribers[subscriberId];
    }

    triggerCallbacks(baId: number, isNew: boolean) {
        this._callbacksEvent.trigger({ baId: baId, isNew: isNew });
        this.clearCallbackSubscriber();
    }

    close() {
        this._mainTabView.closeForm(this.tabViewId);
    }

    destroy() {
        this.clearCallbackSubscriber();
    }

    private clearCallbackSubscriber() {
        const subscribers = this._subscribers;
        for (let id in subscribers) {
            if (subscribers.hasOwnProperty(id)) {
                this._subscribers[id].unsubscribe();
            }
        }
    }
}
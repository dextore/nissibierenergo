﻿import Base = require("../../ComponentBase");

export interface ILegalPersonForm extends Base.IComponent {

}

export function getLegalPersonForm(): ILegalPersonForm {
    return new LegalPersonForm();
} 

class LegalPersonForm extends Base.ComponentBase implements ILegalPersonForm {

    init() {

    }

}
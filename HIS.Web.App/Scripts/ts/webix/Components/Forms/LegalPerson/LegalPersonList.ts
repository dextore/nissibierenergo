﻿import Common = require("../../../Common/CommonExporter");
import Api = require("../../../Api/ApiExporter");
import Base = require("../../ComponentBase");
import ToolBtn = require("../../Buttons/ToolBarButtons");
import Dlg = require("../../../Modules/LegalSubjects/LegalPersonDlg");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import Helpers = require("../../Helpers/HelpersExporter");
import Pager = require("../../Pager/Pager");

export const modulName = "legal-person-module";

export interface ILegalPersonList extends Base.IComponent {
    ready();
}

export function getLegalPersonList(module: Tab.IMainTab) {
    return new LegalPersonlist(`${modulName}-tab`, module);
}

class LegalPersonlist extends Base.ComponentBase implements ILegalPersonList {

    private _toolBar = {
        new: ToolBtn.getNewBtn(`${this._viewName}-new`, "Добавить юридическое лицо", () => {
            ($$(this.dataTableId) as webix.ui.datatable).clearSelection();
            this.showDialog();
        }), 
        edit: ToolBtn.getEditBtn(`${this._viewName}-edit`, "Изменить юридическое лицо", () => { this.showDialog(); }),
        remove: ToolBtn.getRemoveBtn(`${this._viewName}-remove`, "Удалить юридическое лицо", () => { webix.message("remove lick"); }), 
        refresh: ToolBtn.getRefreshBtn(`${this._viewName}-refresh`, "Добавить юридическое лицо", () => {
            const datatable = $$(this.dataTableId) as webix.ui.datatable;
            datatable.load(datatable.config.url);
        }) 
    }

    private _columnInfo: { [id: string]: Querybuilder.ICaptionAndAliaseModel } = {}
    private _baIdColumnInfo: Querybuilder.ICaptionAndAliaseModel; 

    protected get dataTableId() {
        return `${this._viewName}-datatable-id`;
    }

    protected get pagerId() {
        return `${this._viewName}-pager-id`;
    }

    constructor(private readonly _viewName: string, private readonly _module: Tab.IMainTab) {
        super();
    } 

    init() {
        const pager = Pager.getDataTablePager(this.pagerId);
        return { rows: [this.toolbarInit(), this.dataTableInit(), pager.init()]}
    }

    ready() {

        webix.extend($$(this.dataTableId), webix.ProgressBar);

        Api.getApiQueryBuilder().getFields(11).then((data: Querybuilder.ICaptionAndAliaseModel[]) => {
            const dataTable = $$(this.dataTableId) as webix.ui.datatable;
            var columns = webix.toArray(dataTable.config.columns);

            let baIdAlias = ""; 

            data.forEach(item => {
                if (item.aliase.indexOf("BAId")) {
                    baIdAlias = item.aliase;
                    this._baIdColumnInfo = item;
                }
                switch (item.type) {
                    case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Boolean:
                    case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Number:
                    case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.String:
                        columns.push({ id: item.aliase, header: [item.caption, { content: "serverFilter" }], sort: "server" });
                        break;
                    case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Date:
                        columns.push({
                            id: item.aliase, header: [item.caption, { content: "datepickerFilter" }],
                            template: (row, common) => {
                                if (row[item.aliase])
                                    return webix.i18n.dateFormatStr(new Date(row[item.aliase]));
                                return "";
                            },
                            sort: "server"
                        });
                        break;
                }
                this._columnInfo[item.aliase] = item;
            });

            dataTable.define("columns", columns);
            dataTable.refreshColumns();

            dataTable.config.url = <any>{
                    $proxy: true,
                    load: (view, callback, params) => {
                        ($$(this.dataTableId) as any).showProgress();
                        const pager = view.getPager();

                        const from = (!params) ? 0 : params.start;
                        const to = (!params) ? pager.data.size : params.start + params.count;

                        const queryParams = {
                            baTypeId: 11, from: from, to: to,
                            filteringsFields: [],
                            orderingsFields: [(params && params.sort)
                                ? {
                                    orderingField: params.sort.id, sortingOrder: params.sort.dir === "asc"
                                        ? HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Asc
                                        : HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Desc
                                }
                                : {
                                    orderingField: baIdAlias, sortingOrder: HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Asc 

                                }]
                        };

                        if (params && params.filter) {
                            for (let value in params.filter) {
                                if (params.filter.hasOwnProperty(value)) {
                                    if (!params.filter[value])
                                        continue;

                                    queryParams.filteringsFields.push({
                                        field: value,
                                        filterOperator: filterOperatorByFieldType(this._columnInfo[value].type),
                                        value: this._columnInfo[value].type === HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Date
                                            ? filterDateTimeConverter(params.filter[value] as Date)
                                            : params.filter[value]
                                    });
                                }
                            }
                        }

                        Api.getApiQueryBuilder().getData(queryParams as any).then((items) => {

                            const result = JSON.parse(items as any);

                            const data = queryParams.from === 0
                                ? {
                                    data: result.items,
                                    pos: 0,
                                    total_count: result.rowCount
                                }
                                : {
                                    data: result.items,
                                    pos: queryParams.from
                                };
                            ($$(this.dataTableId) as any).hideProgress();
                            (webix.ajax as any).$callback(view, callback, data);
                            Helpers.resetButtons(true, false, this._toolBar);
                        }).catch(() => {
                            ($$(this.dataTableId) as any).hideProgress();
                            (webix.ajax as any).$callback(view, callback, null);
                        });
                    }
            };
            dataTable.load(dataTable.config.url);
        });

        function filterOperatorByFieldType(fieldType: HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType) {
            switch (fieldType) {
                case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Boolean:
                case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Date:
                case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Number:   
                    return HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal;
                case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.String:
                    return HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Like;
                default:
                    return HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal;
            }
        }

        function filterDateTimeConverter(date: Date) {
            const tzoffset = date.getTimezoneOffset() * 60000; //offset in milliseconds
            return (new Date((date as any) - tzoffset)).toISOString().slice(0, -1);
        }
    }

    protected toolbarInit() {
        return {
            view: "toolbar",
            elements: [this._toolBar.new.init(), this._toolBar.edit.init(), this._toolBar.remove.init(), this._toolBar.refresh.init() ]
        }
    }

    protected dataTableInit() {
        return {
            view: "datatable",
            id: this.dataTableId,
            select: "row",
            scroll: "y",
            resizeColumn: true,
            pager: this.pagerId,
            datafetch: 15,
            on: {
                onSelectChange: () => {
                    Helpers.resetButtons(true, ($$(this.dataTableId) as webix.ui.datatable).getSelectedId(true, false).length > 0, this._toolBar);
                }//,
            }//,
        }
    }


    //protected resetButtons(hasSelecetd: boolean) {
    //    for (let value in this._toolBar) {
    //        if (!this._toolBar.hasOwnProperty(value))
    //            continue;

    //        const btn = this._toolBar[value] as ToolBtn.IToolBarButton;
    //        if (btn.needSelected) {
    //            hasSelecetd ? btn.button.enable() : btn.button.disable(); 
    //            continue;
    //        }

    //        btn.button.enable();
    //    } 
    //}

    private showDialog() {
        const items = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true);

        const baId = items.length ? (items[0] as any)[this._baIdColumnInfo.aliase] : null;

        const dlg = Dlg.getLegalPersonDlg(`${this._viewName}-form`, baId);
        dlg.showModalContent(this._module,
            () => {
                dlg.close();
            });
    }
}

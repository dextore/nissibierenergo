﻿import Base = require("../../../Common/DialogBase");
import Btn = require("../../../Components/Buttons/ButtonsExporter");
import BA = require("../../../Common/BATypes");
import Api = require("../../../Api/ApiExporter");
import MH = require("../../../Common/ModuleHelper");
import Entities = require("../../../Common/SystemEntitityTypes");
import Pager = require("../../../Components/Pager/Pager");

export interface IEntityData {
    id: number;
    name: string;
}

export interface IEntitySearchDlg extends Base.IDialog {
    setMarkersInfo(fieldsInfo: { [id: string]: Markers.IMarkerInfoModel });
    selectedEntity: IEntityData;
    disabledAddButton: boolean;
}

export function getEntitySearchDlg(viewName: string, baType: BA.BATypes): IEntitySearchDlg {
    return new EntitySearchDlg(viewName, baType);
}

class EntitySearchDlg extends Base.DialogBase implements IEntitySearchDlg {
    //TODO: подумать EntitySearchDlg
    disabledAddButton: boolean = false;

    private _searchBtn = Btn.getSelectButton(this.viewName, () => {
        this.search();
    });

    private _shoModule = Btn.getAddFromModuleButton(this.viewName, () => { this.showModule(); });
    private _okBtn = Btn.getOkButton(this.viewName, () => { this.okClose(); });
    private _cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });
    private _displayValueAliase: string;
    private _baIdAliase: string;

    get searchCtrlId() { return `${this.viewName}-search-control-id`; }
    get dataTableId() { return `${this.viewName}-result-datatble-id`; }
    get pagerId() { return `${this.viewName}-datatble-pafer-id`; }

    get selectedEntity(): IEntityData {
        const selected = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true);
        if (!selected.length)
            return null;

        const row = selected[0];

        return {
            id: row[this._baIdAliase],
            name: row[this._displayValueAliase]
        };
    }

    constructor(viewName: string, private readonly _baType: BA.BATypes) {
        super(viewName);
    }

    setMarkersInfo(fieldsInfo: { [index: string]: Markers.IMarkerInfoModel; }) { }

    headerLabel(): string { return "Выбор"; }

    ready(): void {

        Api.getApiQueryBuilder().getFields(this._baType).then((data: Querybuilder.ICaptionAndAliaseModel[]) => {
           
            const dataTable = $$(this.dataTableId) as webix.ui.datatable;
            var columns = webix.toArray(dataTable.config.columns);

            data.forEach(item => {
                if (item.markerId === 1) {
                    columns.push({ id: item.aliase, header: item.caption, fillspace: true });
                    this._displayValueAliase = item.aliase;
                }
                if (item.aliase.indexOf("//BAId]") >= 0) {
                    this._baIdAliase = item.aliase;
                }
            });

            dataTable.define("columns", columns);
            dataTable.refreshColumns();
        });
    }

    bindModel(): void {
        super.bindModel();
        ($$(this.searchCtrlId) as webix.ui.text).focus(); 
        webix.UIManager.setFocus($$(this.searchCtrlId) as any);
    }

    private search() {
        const searchValue = ($$(this.searchCtrlId) as webix.ui.text).getValue();

        if (!searchValue || searchValue.length < 2)
            return;

        const dataTable = $$(this.dataTableId) as webix.ui.datatable;
        (dataTable).load(dataTable.config.url);
    }

    okClose(): void {
        if (!($$(this.dataTableId) as webix.ui.datatable).getSelectedId(true, false).length)
            return;

        super.okClose(); 
    }

    showModule() {
        const entity = Entities.entityTypes().getByBATypeId(this._baType);
        MH.ModuleHelper.showModuleByAlias(entity.entityInfo.mvcAlias, this.tabItem.mainTabView);
    } 

    contentConfig() {
        //15/10
        const pager = Pager.getDataTablePager(this.pagerId);
        return {
            rows: [
                {
                    height: 30,
                    cols: [
                        { width: 50 },
                        {
                            view: "text",
                            id: this.searchCtrlId,
                            placeholder: "Поиск по наименованию",
                            on: {
                                onKeyPress: (code, e) => {
                                    if (code === 13)
                                        this.search();
                                }
                            }
                        },
                        this._searchBtn.init()
                    ]
                },
                {
                    rows:[
                        {
                            view: "datatable",
                            id: this.dataTableId,
                            select: "row",
                            scroll: "y",
                            resizeColumn: true,
                            pager: this.pagerId,
                            datafetch: 15,
                            on: {
                                onSelectChange: () => {
                                    //Helpers.resetButtons(($$(this.dataTableId) as webix.ui.datatable).getSelectedId(true, false).length > 0, this._toolBar);
                                }
                            },
                            url: {
                                $proxy: true,
                                load: (view, callback, params) => {

                                    if (!($$(this.searchCtrlId) as webix.ui.text).getValue())
                                        return;

                                    if (!$$(this.dataTableId).hasOwnProperty("ProgressBar"))
                                        webix.extend($$(this.dataTableId), webix.ProgressBar);

                                    const dataTable = view as webix.ui.datatable;
                                    (dataTable as any).showProgress();

                                    const pager = dataTable.getPager();
                                    const from = (!params) ? 0 : params.start;
                                    const to = (!params) ? pager.data.size : params.start + params.count;

                                    const queryParams = {
                                        baTypeId: this._baType,
                                        from: from,
                                        to: to,
                                        filteringsFields: [{
                                            field: this._displayValueAliase,
                                            filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator
                                                .Like,
                                            value: ($$(this.searchCtrlId) as webix.ui.text).getValue()
                                        }],
                                        orderingsFields: [
                                            {
                                                orderingField: this._displayValueAliase,
                                                sortingOrder: HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Asc 
                                            }
                                        ]
                                    };  
                                    ($$(this.dataTableId) as webix.ui.datatable).clearAll();
                                    Api.getApiQueryBuilder().getData(queryParams as any).then((items) => {

                                        const result = JSON.parse(items as any);

                                        const data = queryParams.from === 0
                                            ? {
                                                data: result.items,
                                                pos: 0,
                                                total_count: result.rowCount
                                            }
                                            : {
                                                data: result.items,
                                                pos: queryParams.from
                                            };
                                        ($$(this.dataTableId) as any).hideProgress();
                                        (webix.ajax as any).$callback(view, callback, data);
                                        //Helpers.resetButtons(false, this._toolBar);
                                    }).catch(() => {
                                        ($$(this.dataTableId) as any).hideProgress();
                                        (webix.ajax as any).$callback(view, callback, null);
                                    });

                                }
                            }
                        }, 
                        pager.init()
                    ]
                }, {
                    height: 30,
                    cols: [
                        webix.extend(this._shoModule.init(), { disabled: this.disabledAddButton }),
                        {},
                        this._okBtn.init(),
                        this._cancelBtn.init()
                    ]
                }
            ]
        }
    }
}

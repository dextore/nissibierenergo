﻿import Base = require("../ComponentBase");
import ToolBtn = require("../Buttons/ToolBarButtons");
import MB = require("../../Components/Markers/MarkerControlBase");
import Common = require("../../Common/CommonExporter");

export interface IForm extends Base.IComponent {
    ready();
    isEditState: boolean;
    markersInfo: { [id: string]: Markers.IMarkerInfoModel };
    setMarkersInfo(markersInfo: Markers.IMarkerInfoModel[]);
    afterChanged: Common.IEvent<any>;
  
}

export abstract class FormBase extends Base.ComponentBase implements IForm {
    protected get formId() { return `${this._viewName}-form-id`; }

    protected get toolBarId() { return `${this._viewName}-toolbar-id`; }

    protected get headerLabelId() { return `${this._viewName}-header-label-id`; }

    private _afterChanged = new Common.Event<any>();

    private _markersInfo: { [id: string]: Markers.IMarkerInfoModel } = {};
    private _markersControls: { [id: string]: MB.IMarkerControl } = {};
    private _model: { [id: string]: any } = {};

    get markersInfo() { return this._markersInfo };

    protected get markersControls() { return this._markersControls };

    protected get model() { return this._model };

    protected toolBar = {
        edit: ToolBtn.getEditBtn(`${this._viewName}-tooldar`, "Изменить", this.edit.bind(this)),
        save: ToolBtn.getSaveBtn(`${this._viewName}-tooldar`, "Сохранить изменения", this.save.bind(this)),
        cancel: ToolBtn.getCancelBtn(`${this._viewName}-tooldar`, "Отказаться от изменений", this.cancel.bind(this))
    }

    get viewName() {
        return this._viewName;
    }

    get isEditState(): any {
        return this.toolBar.save.button.isEnabled();
    }

    get afterChanged(): Common.IEvent<any> {
        return this._afterChanged;
    }

    setMarkersInfo(info: Markers.IMarkerInfoModel[]) {
        info.forEach(item => {
                (item.implementTypeField)
                    ? this.markersInfo[item.implementTypeField] = item
                    : this.markersInfo[item.name] = item;
            },
            this);
    }

    protected constructor(private readonly _viewName: string) {
        super();
    }

    private edit() {
        this.changeState(false);
    }
    private hasControlChanges(): boolean {

        let isChange: boolean = false;
        for (let key in this.markersControls) {
            if (this.markersControls.hasOwnProperty(key)
                && this.markersControls[key].isChanged) {
                isChange = true;
                break;
            }
        }
        return isChange;
    }
    private save() {
        // валидировать форму
        if (!($$(this.formId) as webix.ui.form).validate())
            return; 
        // проверяем контролы на изменения 
        if (!this.hasControlChanges()) {
            this.load();
            return;
        }
        // если ок получить данные формы
        const data = this.getFormData();
        // отправить на сервре
        this.showProgress();
        this.saveData(data).then(result => {
            try {
                /* временно убрал так как некорректно работает сохранение паспортных данных 
                this.setFormData(result);
                this.changeState(true);
                this.afterChanged.trigger(result, this);
                //this.load();
                */
                this.changeState(true);
                this.afterChanged.trigger(result, this);
                this.load();
            } catch (e) {
                console.log(e);
            }
            finally {
                this.hideProgress();
            }
        }).catch(() => {
            this.hideProgress();
        });
        // если ок
    }

    private cancel() {
        // восстановить данные формы до перехода в редактирование. 
        this.load();
    }

    protected changeState(readOnly: boolean) {

        if (readOnly) {
            this.toolBar.edit.button.enable();
            this.toolBar.cancel.button.disable();
            this.toolBar.save.button.disable();
        } else {
            this.toolBar.edit.button.disable();
            this.toolBar.cancel.button.enable();
            this.toolBar.save.button.enable();
        }
 
        const markersControl = this._markersControls;
        for (let key in markersControl) {
            if (markersControl.hasOwnProperty(key)) {
                this._markersControls[key].readOnly = readOnly;
            }
        }
        ($$(this.formId) as webix.ui.form).clearValidation();
    }

    // преобразование данных формы в модель
    protected abstract getFormData(): any;
    // преобразование модели для показа на форме
    protected abstract setFormData(data: any);
    // элементы формы
    protected abstract formElementsInit();

    protected emptyDataSource(): Promise<any> {
        $$(this.formId).disable();
        const promise = webix.promise.defer();
        (promise as any).resolve({});
        return promise;
    }

    // получить данные с сервера
    load() {
        this.showProgress();
        this.getData().then(result => {
            try {               
                this._model = {};
                this.clearFormData();
                this.setFormData(result);
                this.changeState(true);
            } finally {
               this.hideProgress();
            }
        }).catch(() => {
            this.hideProgress();
        });;
    } 

    // получить данные с сервера
    abstract getData(): Promise<any>; 
    // отправить данные на сервер
    abstract saveData(data: any): Promise<any>; 

    init() {
        return {
            rows: [
                this.toolbarInit(),
                webix.extend(this.formInit(), this.extendFormInit())
            ]
        }
    }

    private formInit() {
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: this.formElementsInit(),
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        }
    } 

    protected  extendFormInit() {
        return {};
    } 

    // тулбар (редактировать сохранить отказаться)
    protected toolbarInit() {
        return {
            view: "toolbar",
            id: this.toolBarId,
            paddingY: 1,
            height: 30,
            elements: [
                { id: this.headerLabelId, view: "label", label: "" },
                this.toolBar.save.init(),
                this.toolBar.cancel.init(),
                this.toolBar.edit.init()
            ]
        }
    } 

    private showProgress() {
        const form = $$(this.formId) as any;
        if (typeof form.showProgress === "function") {
            form.showProgress();
        }
    }

    private hideProgress() {
        const form = $$(this.formId) as any;
        if (typeof form.hideProgress === "function") {
            form.hideProgress();
        }
    }

    public clearFormData() {
        const values = ($$(this.formId) as webix.ui.form).getValues();
        for (var key in values) {
            values[key] = null;
        }
        ($$(this.formId) as webix.ui.form).setValues(values);
    }

    ready() {
        webix.extend($$(this.formId), webix.ProgressBar);
    }
}

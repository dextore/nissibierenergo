﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");

export interface IDocumentsTable extends Base.IComponent {
    ready();
}

export function getDocumentsTable(viewName: string) {
    return new DocumentsTable(`${viewName}-tab`);
}

class DocumentsTable extends Base.ComponentBase implements IDocumentsTable {
    private _viewName: string;

    constructor(viewName: string) {
        super();
        this._viewName = viewName;
    }

    init() {}

    ready() { }


}
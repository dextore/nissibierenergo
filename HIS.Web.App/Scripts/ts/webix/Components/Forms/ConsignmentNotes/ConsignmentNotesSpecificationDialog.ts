﻿import Base = require("../../../Common/DialogBase");
import Btn = require("../../Buttons/ButtonsExporter");
import Api = require("../../../Api/ApiExporter");
import MF = require("../../Markers/MarkerControlFactory");
import EntityRefMarkerControl = require("../../Markers/EntityRefMarkerControl");
import BaseMarker = require("../../Markers/MarkerControlBase");
import MarkerType = HIS.Models.Layer.Models.Markers.MarkerType;
import markerHelper = require("../../Markers/MarkerHelper");
import Operation = require("../../Controls/EntityOperationStateControl");
import ComponentBase = require("../../ComponentBase");

export function getConsignmentNotesSpecificationDialog(viewName: string, isEdit: boolean, documentTable: webix.ui.datatable, specTable:webix.ui.datatable, module: any, documentOperationStateControl: any) {
    return new ConsignmentNotesSpecificationDialog(viewName, isEdit, documentTable, specTable, module, documentOperationStateControl);
}

class ConsignmentNotesSpecificationDialog extends Base.DialogBase {
    constructor(viewName: string,
        private isEdit: boolean,
        private documentTable: webix.ui.datatable,
        private specTable: webix.ui.datatable,
        private module: any,
        private documentOperationStateControl: any) {
        super(viewName);
    }

    protected _markersControls: { [id: string]: BaseMarker.IMarkerControl } = {};
    protected _fieldsInfo: { [id: string]: Markers.IMarkerInfoModel } = {};
    protected _model: { [id: string]: any } = {};

    protected get formId() {
        return `${this.viewName}-specification-Dlg`;
    };

    private okBtn = Btn.getOkButton(`${this.viewName}`, () => {
        if (($$(this.formId) as webix.ui.form).validate()) {
            let data: ConsignmentNotes.IConsignmentNotesSpecificationsUpdateModel = this.getFormData();
            Api.ApiConsignmentNotes.saveConsignmentNotesSpecification(data).then(result => {
                let curentDocument = this.documentTable.getSelectedItem();
                
                if (!this.isEdit) {
                    if (!this.specTable.isVisible()) this.specTable.show();
                    this.specTable.add(result);
                } else {
                    let { id } = this.specTable.getSelectedItem();
                    this.specTable.updateItem(id, result);
                }

                Api.ApiConsignmentNotes.getConsignmentNotesDocument(curentDocument.baId).then(result => {
                    let finishResult = { ...result };
                    finishResult.docDate = new Date(result.docDate.toString());
                    this.documentTable.updateItem(curentDocument.id, finishResult);
                });
                (this.documentOperationStateControl as Operation.IEntityOperationStateControlFilterOperation).filterAllowOperation = (item) => item.id !== 255;
                (this.documentOperationStateControl as  any).load();
                this.okClose();
            });
        }
    });
    private cancelBtn = Btn.getCancelButton(`${this.viewName}`, () => {
        this.cancelClose();
    });

    headerLabel(): string {
        return `${this.isEdit ? "Редактировать" : "Добавить"} спецификацию`;
    }
    protected get labelWidth() { return 170; }

    contentConfig() {
        let config = {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
                this.getInventoryNameControl().init(),
                this.getUnitControl().init(),
                this.getQuantityControl(),
                this.getPriceControl(),
                this.getPriceVatControl(),
                this.getPVATControl(),
                {
                    cols: [
                        this.getVATRateControl(),
                        {
                            id: "isVATIncluded",
                            name: "isVATIncluded",
                            view: "checkbox",
                            label: "Включен НДС",
                            labelWidth: this.labelWidth + 20
                        }
                    ]
                },
                this.getCostControl(),
                this.getVatControl(),
                this.getCostVatControl(),
                {
                    cols: [
                        {},
                        this.okBtn.init(),
                        this.cancelBtn.init()
                    ]
                }
            ]
        }

        return config;
    }

    private getFormData(): ConsignmentNotes.IConsignmentNotesSpecificationsUpdateModel {
        const form = $$(this.formId) as webix.ui.form;

        let inventoryNameValue = this._markersControls["inventoryNameControl"].getValue(this._fieldsInfo).value;

        return {
            recId: !this.isEdit? null : this.specTable.getSelectedItem().recId,
            unitId: Number(form.elements["unitName"].getValue()),
            cost: Number(form.elements["cost"].getValue()),
            costVAT: Number(form.elements["costVAT"].getValue()),
            docId: (this.documentTable.getSelectedItem() as ConsignmentNotes.IConsignmentNotesDocumentModel).baId,
            inventoryId: Number(inventoryNameValue),
            isVATIncluded: form.elements["isVATIncluded"].getValue(),
            price: Number(form.elements["price"].getValue()),
            priceVAT: Number(form.elements["priceVAT"].getValue()),
            pVAT: Number(form.elements["pVAT"].getValue()),
            quantity: Number(form.elements["quantity"].getValue()),
            vAT: Number(form.elements["vAT"].getValue()),
            vATRate: Number(form.elements["vATRate"].getValue())
        } as ConsignmentNotes.IConsignmentNotesSpecificationsUpdateModel;
    }

    private setFormData() {
        const dataItem: ConsignmentNotes.IConsignmentNotesSpecificationsModel = this.specTable.getSelectedItem();
        const docId = (this.documentTable.getSelectedItem() as ConsignmentNotes.IConsignmentNotesDocumentModel).baId;
        const form = $$(this.formId) as webix.ui.form;
        Api.ApiConsignmentNotes.getConsignmentNotesSpecificationFullModel(dataItem.recId, docId).then(result => {
            if (result) {
                let info = this._markersControls["inventoryNameControl"].markerInfo;
                this._markersControls["inventoryNameControl"].setInitValue(
                    getMarkerValue(info, result.inventoryId as any, result.inventoryName),
                    this._model);

                let form = ($$(this.formId) as webix.ui.form);
                form.setValues(this._model);

                form.elements["unitName"].setValue(result.unitId);
                form.elements["cost"].setValue(result.cost);
                form.elements["costVAT"].setValue(result.costVAT);
                form.elements["isVATIncluded"].setValue(result.isVATIncluded);
                form.elements["price"].setValue(result.price);
                form.elements["priceVAT"].setValue(result.priceVAT);
                form.elements["pVAT"].setValue(dataItem.pVAT);
                form.elements["quantity"].setValue(result.quantity);
                form.elements["vAT"].setValue(result.vAT);
                form.elements["vATRate"].setValue(result.vATRate);

                form.clearValidation();
            }
        });

        function getMarkerValue(info: Markers.IMarkerInfoModel, value: string, displayValue: string): Markers.IMarkerValueModel {
            return {
                displayValue: displayValue,
                itemId: null,
                endDate: null,
                markerId: info.id,
                markerType: info.markerType,
                MVCAliase: info.name,
                note: null,
                isVisible: true,
                isBlocked: false,
                startDate: null,
                value: value,
                isDeleted: false
            }
        }
    }

    ready(): void {
        Api.getApiNomenclature().getRefSysUnits().then((data) => {
            const resultData = data.map(({ unitId, name }) => {
                return { id: unitId, value: name };
            });

            let select = ($$(this.formId) as webix.ui.form).elements["unitName"] as webix.ui.select;
            select.define("options", resultData);
            select.refresh();
        });

        if (this.isEdit)
            this.setFormData();
    }

    private getCostVatControl(): BaseMarker.IMarkerControl {
        const controlConfig = {
            id: "costVAT",
            name: "costVAT",
            view: "text",
            label: "Сумма склада с НДС, руб.",
            pattern: { mask: "######################################", allow: /[0-9,.]/g },
            format: "1.111,00",
            labelWidth: this.labelWidth + 20
        };

        return webix.extend(controlConfig, markerHelper.markerHelper.requiredConfig(controlConfig.id, null));
    }

    private getVatControl(): BaseMarker.IMarkerControl {
        const controlConfig = {
            id: "vAT",
            view: "text",
            name: "vAT",
            label: "Сумма склада НДС, руб.",
            pattern: { mask: "######################################", allow: /[0-9,.]/g },
            format: "1.111,00",
            labelWidth: this.labelWidth + 20 
        };

        return webix.extend(controlConfig, markerHelper.markerHelper.requiredConfig(controlConfig.id, null));
    }

    private getCostControl(): BaseMarker.IMarkerControl {
        const controlConfig = {
            id: "cost",
            view: "text",
            name: "cost",
            label: "Сумма склада без НДС, руб.",
            pattern: { mask: "######################################", allow: /[0-9,.]/g },
            format: "1.111,00",
            labelWidth: this.labelWidth + 40
        };

        return webix.extend(controlConfig, markerHelper.markerHelper.requiredConfig(controlConfig.id, null));
    }

    private getVATRateControl(): BaseMarker.IMarkerControl {
        const controlConfig = {
            id: "vATRate",
            name: "vATRate",
            view: "text",
            label: "Ставка НДС, %",
            labelWidth: this.labelWidth,
            pattern: { mask: "###", allow: /[0-9]/g },
        };

        return webix.extend(controlConfig, markerHelper.markerHelper.requiredConfig(controlConfig.id, (value) => {
            return value <= 100 && value !== "";
        } ));
    }

    private getPVATControl(): BaseMarker.IMarkerControl {
        const controlConfig = {
            id: "pVAT",
            name: "pVAT",
            view: "text",
            label: "Цена за единицу НДС, руб.",
            pattern: { mask: "######################################", allow: /[0-9,.]/g },
            format: "1.111,00",
            labelWidth: this.labelWidth + 40
        };

        return webix.extend(controlConfig, markerHelper.markerHelper.requiredConfig(controlConfig.id, null));
    }

    private getPriceVatControl(): BaseMarker.IMarkerControl {
        const controlConfig = {
            id: "priceVAT",
            name: "priceVAT",
            view: "text",
            label: "Цена за единицу с НДС, руб.",
            pattern: { mask: "######################################", allow: /[0-9,.]/g },
            format: "1.111,00",
            labelWidth: this.labelWidth + 40 
        };

        return webix.extend(controlConfig, markerHelper.markerHelper.requiredConfig(controlConfig.id, null));
    }

    private getPriceControl(): BaseMarker.IMarkerControl {
        const controlConfig = {
            id: "price",
            view: "text",
            name: "price",
            label: "Цена за единицу без НДС, руб.",
            pattern: { mask: "######################################", allow: /[0-9,.]/g },
            format: "1.111,00",
            labelWidth: this.labelWidth + 40
        };

        return webix.extend(controlConfig, markerHelper.markerHelper.requiredConfig(controlConfig.id, null));
    }

    private getQuantityControl(): BaseMarker.IMarkerControl {
        const controlConfig = {
            id: "quantity",
            name: "quantity",
            view: "text",
            label: "Количество в н.е.",
            labelWidth: this.labelWidth + 80,
            pattern: { mask: "######################################", allow: /[0-9]/g },
        };

        return webix.extend(controlConfig, markerHelper.markerHelper.requiredConfig(controlConfig.id, (value) => {
            return value > 0;
        }));
    }

    private getInventoryNameControl(): BaseMarker.IMarkerControl {
        let inventoryNameInfo: Markers.IMarkerInfoModel = {
            id: 1000,
            catalogImplementTypeName: "",
            implementTypeField: "",
            implementTypeName: "",
            isBlocked: false,
            isCollectible: false,
            isImplemented: true,
            isOptional: false,
            isPeriodic: false,
            isRequired: true,
            label: "Номенклатурная единица",
            markerType: MarkerType.MtEntityRef,
            name: "inventoryName",
            refBaTypeId: 2,
            list: []
        } as Markers.IMarkerInfoModel;
        this._fieldsInfo[inventoryNameInfo.id] = inventoryNameInfo;
        
        let inventoryNameControl: BaseMarker.IMarkerControl = MF.MarkerControlfactory.getControl(this.viewName, inventoryNameInfo,
            (content) => {
                return {
                    label: "Номенклатурная единица",
                    labelWidth: this.labelWidth + 25
                }
            });
        (inventoryNameControl as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
        
        (inventoryNameControl as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this.module);
        this._markersControls["inventoryNameControl"] = inventoryNameControl;
        return inventoryNameControl;
    }

    private getUnitControl(): BaseMarker.IMarkerControl {
        let unitNameInfo: Markers.IMarkerInfoModel = {
            id: 1001,
            catalogImplementTypeName: "",
            implementTypeField: "",
            implementTypeName: "",
            isBlocked: false,
            isCollectible: false,
            isImplemented: true,
            isOptional: false,
            isPeriodic: false,
            isRequired: true,
            label: "Единица измерения",
            markerType: MarkerType.MtList,
            name: "unitName",
            refBaTypeId: 0,
            searchTypeId:null,
            list: []
        };
        const control = MF.MarkerControlfactory.getControl(this.viewName, unitNameInfo,
            (content) => {
                return {
                    label: "Единица измерения",
                    labelWidth: this.labelWidth + 25,
                    value: 3
                }
            });

        return control;
    }
}
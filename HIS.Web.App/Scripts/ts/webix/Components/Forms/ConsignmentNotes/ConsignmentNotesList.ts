﻿import Api = require("../../../Api/ApiExporter");
import Base = require("../../ComponentBase");
import Tab = require("../../../MainView/Tabs/MainTabBase");
import ToolBtn = require("../../Buttons/ToolBarButtons");
import DocumentsDlg = require("./ConsignmentNotesDocumentsDialog");
import SpecificationDlg = require("./ConsignmentNotesSpecificationDialog");
import Operation = require("../../Controls/EntityOperationStateControl");
import Tran = require("../../../Modules/Documents/InventoryTransactions/InventoryTransactionsDlg");
import Warning = require("../../Messages/Warning");
import Pager = require("../../Pager/Pager");

export const modulesName = "consignment-notes-module";

export interface IConsignmentNotesList extends Base.IComponent {
    ready();
    selectedCompany: Inventory.ILSCompanyAreas;
}

export function getConsignmentNotesList(module: Tab.IMainTab) {
    return new ConsignmentNotesList(`${modulesName}-tab`, module);
}

export class ConsignmentNotesList extends Base.ComponentBase implements IConsignmentNotesList {
    private documemntOperationStateControl;
    private _selectedCompany;

    set selectedCompany(value: Inventory.ILSCompanyAreas) {
        this._selectedCompany = value;
        this.load();
    };

    get selectedCompany(): Inventory.ILSCompanyAreas {
        return this._selectedCompany;
    }

    private _historyBtn = ToolBtn.getInventoryTransactionBtn(`${modulesName}-document-tooldar`, "Проводки", () => {
        const datatable = $$(this.datatableId) as webix.ui.datatable;
        const docInfo: ConsignmentNotes.IConsignmentNotesDocumentModel = datatable.getSelectedItem();
        if (!docInfo)
            return;

        const dialog = Tran.getInventoryTransactionsDlg(`${this._viewName}-tran-dialog`,
            docInfo.baId,
            `<b>№ ${docInfo.number} от ${webix.i18n.dateFormatStr(new Date(docInfo.docDate.toString()))}</b>`);
        dialog.showModalContent(this._module, () => {
            dialog.close();
        });
    });


    constructor(private readonly _viewName: string, private readonly _module: Tab.IMainTab) {
        super();

        this.documemntOperationStateControl = Operation.getEntityOperationStateControl(this._viewName,
            "Накладная",
            () => true);
        
        this.documemntOperationStateControl.addButtons(
            ToolBtn.getEditBtn(`${modulesName}-document-tooldar`, "Изменить", this.editDocuments.bind(this)) as ToolBtn.IToolBarButton, 
            ToolBtn.getNewBtn(`${modulesName}-document-tooldar`, "Добавить накладную", () => this.showDocumentsDialog()) as ToolBtn.IToolBarButton,
            this._historyBtn
            );

        this.documemntOperationStateControl.statusChangedEvent.subscribe(result => {
            if (result === Operation.StateChangeAction.Loaded) {
                this._historyBtn.button.enable();
            }
            if (result === Operation.StateChangeAction.Changed || result ===  Operation.StateChangeAction.Canceled) {
                const datatable = $$(this.datatableId) as webix.ui.datatable;
                let current = datatable.getSelectedItem();
                Api.ApiConsignmentNotes.getConsignmentNotesDocument(current.baId).then(answer => {
                    let finishResult = {
                        ...answer,
                        docDate: new Date(answer.docDate.toString())
                    };
                    //finishResult.docDate = new Date(answer.docDate.toString());
                    datatable.updateItem(current.id, finishResult);
                    this.changeState(false, this.documentstoolBar);

                    ($$(this.specificationDatatableId) as webix.ui.datatable).unselectAll();
                    this.specificationtoolBar.add.button.disable();
                    this.specificationtoolBar.add.button.refresh();
                    this.changeSpecToolbarState(true, this.specificationtoolBar);

                    let curentStatus = (this.documemntOperationStateControl as Operation.IEntityOperationStateControl).state;
                    Warning.showForEntity(this._viewName, current.baId);
                });
            }
        }, this);
    }
    protected get datatableId() {
        return `${this._viewName}-documents-table-id`;
    }
    protected get specificationDatatableId() {
        return `${this._viewName}-specification-table-id`;
    }
    protected get pagerId() {
        return `${this._viewName}-pager-id`;
    }
    protected get specificationPagerId() {
        return `${this._viewName}-specification-pager-id`;
    }
    //--------------------------DOCUMENT-------------------------------
    documentsDataTableInit() {
        return {
            view: "datatable",
            id: this.datatableId,
            select: "row",
            resizeColumn: true,
            columns: [
                {
                    id: "number",
                    header: ["Номер документа", { content: "textFilter" }],
                    sort: "string",
                    adjust: true
                },
                {
                    id: "docDate",
                    header: [
                        "Дата документа",
                        { content: "dateFilter" }
                    ],
                    sort: "date",
                    format: webix.i18n.dateFormatStr,
                    adjust: true
                },
                {
                    id: "legalSubjectsName",
                    header: ["Контрагент", { content: "textFilter" }],
                    adjust: true
                },
                {
                    id: "contractName",
                    header: ["Договор", { content: "textFilter" }],
                    adjust: true
                },
                {
                    id: "note",
                    header: ["Примечание", { content: "textFilter" }],
                    adjust: true
                },
                {
                    id: "userName",
                    header: ["Создал", { content: "textFilter" }],
                    adjust: true
                },
                {
                    id: "warehouseName",
                    header: ["Склад", { content: "textFilter" }],
                    adjust: true
                },
                {
                    id: "cost",
                    header: ["Сумма/склада без НДС, руб.", { content: "numberFilter" }],
                    format: webix.i18n.priceFormat,
                    adjust: true
                },
                {
                    id: "vAT",
                    header: ["Сумма/склада НДС, руб.", { content: "numberFilter" }],
                    format: webix.i18n.priceFormat,
                    adjust: true
                },
                {
                    id: "costVAT",
                    header: ["Сумма/склада с НДС, руб.", { content: "numberFilter" }],
                    format: webix.i18n.priceFormat,
                    adjust: true
                },
                {
                    id: "stateDisplay",
                    header: ["Состояние", { content: "selectFilter" }],
                    weight : 99
                },
                {
                    id: "companyAreaName",
                    header: ["Организациия"],
                    adjust: true
                }
            ],
        }
    }
    
    protected documentstoolBar = {
        edit: ToolBtn.getEditBtn(`${modulesName}-document-tooldar`, "Изменить", this.editDocuments.bind(this)),
        add: ToolBtn.getNewBtn(`${modulesName}-document-tooldar`, "Добавить накладную", () => this.showDocumentsDialog())
    }

    editDocuments() {
        this.showDocumentsDialog(true);
    }

    showDocumentsDialog(isEdit: boolean = false) {
        const documentTable: webix.ui.datatable = $$(this.datatableId) as webix.ui.datatable;
        
        const dlg = DocumentsDlg.getConsignmentNotesDocumentsDialog(this._viewName, isEdit, documentTable, this._module, this._selectedCompany);
        dlg.showModalContent(this._module, () => dlg.close()); 
    }
    //-----------------------------------------------------------------


    //--------------------------Specifications-------------------------
    specificationDataTableInit() {
        return {
            view: "datatable",
            id: `${this.specificationDatatableId}`,
            select: "row",
            resizeColumn: true,
            columns: [
                {
                    id: "recId",
                    header: ["Id", { content: "textFilter" }],
                    sort: "string",
                    adjust: true
                },
                {
                    id: "inventoryName",
                    header: ["Номенклатурная единица", { content: "textFilter" }],
                    adjust: true
                },
                {
                    id: "consignmentNotesDocumentName",
                    header: ["Приходная накладная", { content: "textFilter" }],
                    adjust: true
                },
                {
                    id: "unitName",
                    header: ["Единица измерения", { content: "selectFilter" }],
                    adjust: true
                },
                {
                    id: "quantity",
                    header: ["Количество в н.е.", { content: "textFilter" }],
                    adjust: true
                },
                {
                    id: "price",
                    header: ["Цена за единицу без НДС", { content: "numberFilter" }],
                    format: webix.i18n.priceFormat,
                    adjust: true
                },
                {
                    id: "priceVAT",
                    header: ["Цена за единицу с НДС", { content: "numberFilter" }],
                    format: webix.i18n.priceFormat,
                    adjust: true
                },
                {
                    id: "pVAT",
                    header: ["Цена за единицу НДС", { content: "numberFilter" }],
                    format: webix.i18n.priceFormat,
                    adjust: true
                },
                {
                    id: "isVATIncluded",
                    header: ["Включен НДС", { content: "selectFilter" }],
                    adjust: true,
                    template: (obj) => (obj.isVATIncluded) ? "Да" : "Нет"
                },
                {
                    id: "vATRate",
                    header: ["Ставка НДС, %", { content: "selectFilter" }],
                    adjust: true
                },
                {
                    id: "cost",
                    header: ["Сумма/склада без НДС, руб.", { content: "numberFilter" }],
                    format: webix.i18n.priceFormat,
                    adjust: true
                },
                {
                    id: "vAT",
                    header: ["Сумма/склада НДС, руб.", { content: "numberFilter" }],
                    format: webix.i18n.priceFormat,
                    adjust: true
                },
                {
                    id: "costVAT",
                    header: ["Сумма/склада с НДС, руб.", { content: "numberFilter" }],
                    format: webix.i18n.priceFormat,
                    adjust: true
                }
            ],
        };
    }

    protected specificationtoolBar = {
        edit: ToolBtn.getEditBtn(`${modulesName}-specification-tooldar`, "Изменить", this.editSpecifications.bind(this)),
        add: ToolBtn.getNewBtn(`${modulesName}-specification-tooldar`, "Добавить накладную", () => this.showSpecificationsDialog()),
        del: ToolBtn.getRemoveBtn(`${modulesName}-specification-tooldar`, "Удалить спецификацию", () => this.removeSpecification())
    }
    
    
    specificationtoolBarInit() {
        return {
            id: `${modulesName}-specification-toolbar`,
            view: "toolbar",
            paddingY: 3,
            elements: [
                { view: "label", label: "Спецификация" },
                this.specificationtoolBar.add.init(),
                this.specificationtoolBar.edit.init(),
                this.specificationtoolBar.del.init()
            ]
        }
    }

    editSpecifications() {
        this.showSpecificationsDialog(true);
    }

    removeSpecification() {
        let self = this;
        webix.confirm({
            title: "Подтверждение",
            type: "confirm-warning",
            text: `Выполнить операцию удаления строки спецификации?`,
            ok: "Да", cancel: "Нет",
            callback(result) {
                if (result) {
                    const datatable = $$(self.specificationDatatableId) as webix.ui.datatable;
                    const documentTable = $$(self.datatableId) as webix.ui.datatable;

                    const currentDocument = documentTable.getSelectedItem();
                    const itemUnderDel = datatable.getSelectedItem();
                    let item = { recId: itemUnderDel.recId, docId: currentDocument.baId };

                    Api.ApiConsignmentNotes.deleteConsignmentNotesSpecification(item.recId, item.docId).then(result => {
                        if (result) {
                            datatable.remove(itemUnderDel.id);
                            if (datatable.data.order.length === 0) {
                                (self.documemntOperationStateControl as Operation.
                                        IEntityOperationStateControlFilterOperation).filterAllowOperation =
                                    (item) => item.id !== 5;
                                (self.documemntOperationStateControl as any).load();
                            } else {

                                datatable.load(datatable.config.url);
                            }
                            self.changeSpecToolbarState(true, self.specificationtoolBar);
                        }
                    });
                }
            }
        });
    }

    showSpecificationsDialog(isEdit: boolean = false) {
        const documentTable: webix.ui.datatable = $$(this.datatableId) as webix.ui.datatable;
        const specTable: webix.ui.datatable = $$(this.specificationDatatableId) as webix.ui.datatable;
        const dlg = SpecificationDlg.getConsignmentNotesSpecificationDialog(this._viewName, isEdit, documentTable, specTable, this._module, this.documemntOperationStateControl);
        dlg.showModal(() => {
            dlg.close();
        });
    }
    //-----------------------------------------------------------------

    protected resetButtons(hasSelecetd: boolean) {

    }

    init() {
        //15/10
        const pager = Pager.getDataTablePager(this.pagerId);
        const specificationPager = Pager.getDataTablePager(this.specificationPagerId);

        const documentsDataTable = webix.extend(this.documentsDataTableInit(), { pager: this.pagerId });
        const specificationDataTable = webix.extend(this.specificationDataTableInit(), { pager: this.specificationPagerId });
        return {
            rows: [
                {
                    rows: [
                        this.documemntOperationStateControl.init(),
                        documentsDataTable,
                        pager.init()
                    ]
                },
                { view: "resizer" },
                {
                    height: 350,
                    rows: [
                        this.specificationtoolBarInit(),
                        specificationDataTable,
                        specificationPager.init()
                    ]
                }
            ]
        }
    }

    ready() {
    }

 
    private changeState(isReadOnly: boolean, toolbar: any) {
        const documentTable = $$(this.datatableId) as webix.ui.datatable;
        var document = documentTable.getSelectedItem() as ConsignmentNotes.IConsignmentNotesDocumentModel;
        if (document.stateId !== 1)
            isReadOnly = true;
        if (isReadOnly) {
            toolbar.edit.button.disable();
        } else {
            toolbar.edit.button.enable();
        }
    }

    private changeSpecToolbarState(isReadOnly: boolean, toolbar: any) {
        const documentTable = $$(this.datatableId) as webix.ui.datatable;
        var document = documentTable.getSelectedItem() as ConsignmentNotes.IConsignmentNotesDocumentModel;
        if (document.stateId !== 1) {
            toolbar.add.button.disable();
            isReadOnly = true;
        }
        else toolbar.add.button.enable();

        if (isReadOnly) {
            toolbar.edit.button.disable();
            toolbar.del.button.disable();
        } else {
            toolbar.edit.button.enable();
            toolbar.del.button.enable();
        }
    }

    load() {
        const documentsDataTable = $$(this.datatableId) as webix.ui.datatable;
        const specificationDataTable = $$(this.specificationDatatableId) as webix.ui.datatable;
        documentsDataTable.clearAll();
        this.documentstoolBar.add.button.enable();

        webix.extend(documentsDataTable, webix.ProgressBar);
        (documentsDataTable as any).showProgress();

        specificationDataTable.hide();

        Api.ApiConsignmentNotes.getConsignmentNotesDocuments().then((result: ConsignmentNotes.
            IConsignmentNotesDocumentModel[]) => {
            documentsDataTable.config.url = <any>{
                $proxy: true,
                load: (view, callback, params) => {
                    let data = result;
                    data = data.filter(item => item.companyAreaName === this._selectedCompany.name).reverse();

                    (documentsDataTable as any).hideProgress();
                    (webix.ajax as any).$callback(view, callback, data);
                    this.resetButtons(false);
                }
            };

            documentsDataTable.load(documentsDataTable.config.url);
        });

        documentsDataTable.attachEvent("onAfterSelect", (selection, preserve) => {
            var selectedDocument =
                documentsDataTable.getSelectedItem() as ConsignmentNotes.IConsignmentNotesDocumentModel;
            this.documemntOperationStateControl.baId = selectedDocument.baId;

            const url = {
                $proxy: true,
                load: (view, callback, params) => {
                    selectedDocument =
                        documentsDataTable.getSelectedItem() as ConsignmentNotes.IConsignmentNotesDocumentModel;
                    Api.ApiConsignmentNotes.getConsignmentNotesSpecificationByDocument(selectedDocument.baId).then(
                        (result:
                            ConsignmentNotes.IConsignmentNotesSpecificationsModel[]) => {
                            const data = result;
                            specificationDataTable.clearAll();

                            if (data.length > 0) {
                                specificationDataTable.show();
                                (this.documemntOperationStateControl as Operation.
                                        IEntityOperationStateControlFilterOperation).filterAllowOperation = (item) => item.id !== 255;
                            } else {
                                specificationDataTable.hide();
                                (this.documemntOperationStateControl as Operation.IEntityOperationStateControlFilterOperation).filterAllowOperation = (item) => item.id !== 5;
                            }

                            this.changeSpecToolbarState(true, this.specificationtoolBar);
                            (webix.ajax as any).$callback(view, callback, data);
                            this.resetButtons(false);
                        });
                }
            };

            webix.extend(specificationDataTable.config, { url: url });
            specificationDataTable.load(specificationDataTable.config.url);
            this.changeState(false, this.documentstoolBar);

            return true;
        });

        specificationDataTable.attachEvent("onAfterSelect", (selection, preserve) => {
            this.changeSpecToolbarState(false, this.specificationtoolBar);
        });
    }
}
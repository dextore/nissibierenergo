﻿import Base = require("../../../Common/DialogBase");
import Btn = require("../../Buttons/ButtonsExporter");
import Api = require("../../../Api/ApiExporter");
import MF = require("../../Markers/MarkerControlFactory");
import EntityRefMarkerControl = require("../../Markers/EntityRefMarkerControl");
import BaseMarker = require("../../Markers/MarkerControlBase");
import Common = require("../../../Common/CommonExporter");

export function getConsignmentNotesDocumentsDialog(viewName: string, isEdit: boolean, documentTable: webix.ui.datatable, module: any, selectedCompany: Inventory.ILSCompanyAreas) {
    return new ConsignmentNotesDocumentsDialog(viewName, isEdit, documentTable, module, selectedCompany);
}

class ConsignmentNotesDocumentsDialog extends Base.DialogBase {
    private _item: any;
    protected get formId() {
        return `${this.viewName}-document-Dlg`;
    };
    protected get labelWidth() {
        return 150;
    };

    protected _fieldsInfo: { [id: string]: Markers.IMarkerInfoModel } = {};
    protected _markersControls: {[id: string]:BaseMarker.IMarkerControl} = {};
    protected _model: { [id: string]: any } = {};

    constructor(viewName: string,
                private isEdit: boolean,
                private datatable: webix.ui.datatable,
                private _module,
                private _selectedCompany: Inventory.ILSCompanyAreas) {
        super(viewName);
        if (isEdit)
            this._item = datatable.getSelectedItem() as ConsignmentNotes.IConsignmentNotesDocumentModel;
    }

    protected okBtn = Btn.getOkButton(this.viewName, () => {
        this.okBtn.button.disable();
        if (($$(this.formId) as webix.ui.form).validate()) {

            if (!this.isChangeForm()) {
                this.okClose();
                return;
            }
            const data = this.getDataFromDialog();
            Api.ApiConsignmentNotes.saveConsignmentNotesDocument(data).then(result => {
                if (!this.isEdit) {
                    this.datatable.add(result, 0);
                } else {
                    let { id } = this.datatable.getSelectedItem();
                    let finishResult = { ...result, docDate: new Date(result.docDate.toString()) };
                    //finishResult.docDate = new Date(result.docDate.toString());
                    this.datatable.updateItem(id, finishResult);
                }
                this.okClose();
            });
            this.okBtn.button.enable();
        }
    });

    private isChangeForm(): boolean {
        let result: boolean = false;
        for (let key in this._markersControls) {

            if (this._markersControls.hasOwnProperty(key)) {
                if (this._markersControls[key].isChanged === true) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    protected cancelBtn = Btn.getCancelButton(this.viewName, () => {
        this.okClose();
    });

    private _tabModule: Common.IMainTab;

    showModalContent(module: Common.IMainTab, callBack?: () => void): void {
        super.showModalContent(module, callBack);
        this._tabModule = module;
        this._tabModule.disableCompany();
    }

    destroy(): void {
        super.destroy();
        this._tabModule.enableCompany();
    }

    headerLabel() {
        return `${this.isEdit ? "Редактировать " : "Добавить "}приходную накладную`;
    }

    contentConfig() {   
        return {
            view: "form",
            id: this.formId,
            autoheight: true,
            elements: [
            ]
        }
    }

    bindModel(): void {
        const self = this;
        const promise: Promise<any>[] = [];
        promise.push(Api.getApiMarkers().getEntityMarkersInfo(80));
        if (this._item) {
            promise.push(Api.ApiConsignmentNotes.getConsignmentNotesFullDocument(this._item.baId));
        }

        (webix.promise.all(promise as any) as any).then((result: any[]) => {
            loadFildsInfo(result[0]);

            ($$(self.formId) as webix.ui.form).addView({
                cols: [
                    {},
                    self.okBtn.init(),
                    self.cancelBtn.init()
                ]
            });

            if (result.length > 1) {
                modelBindToForm(result.length > 1 ? result[1] : null);  
            }

            const info = this._markersControls["CompanyArea"].markerInfo;
            this._markersControls["CompanyArea"].setInitValue(getMarkerValue(info, this._selectedCompany.id as any, this._selectedCompany.name), this._model);
            ($$(self.formId) as webix.ui.form).setValues(this._model);
            ($$(self.formId) as webix.ui.form).clearValidation();
            (this._markersControls["CompanyArea"] as any)._selectEntityBtn.button.disable();

        });

        function loadFildsInfo(markers: Markers.IMarkerInfoModel[]) {
            markers.forEach(item => {
                const marker = self.createMarkerControl(item);
                if (marker) {
                    ($$(self.formId) as webix.ui.form).addView(marker.init());
                    self._fieldsInfo[item.id] = item;

                    if ("baId" in marker) {
                        (marker as any).baId = self._item.baId;
                    }
                }
            });
        }

        function modelBindToForm(model: ConsignmentNotes.IConsignmentNotesDocumentFullModel) {
            if (self.isEdit) {
                let info = self._markersControls["Name"].markerInfo;
                self._markersControls["Name"].setInitValue(getMarkerValue(info, model.number, null), self._model);
                
                info = self._markersControls["Note"].markerInfo;
                self._markersControls["Note"].setInitValue(getMarkerValue(info, model.note as any, null), self._model);

                info = self._markersControls["DocDate"].markerInfo;
                self._markersControls["DocDate"].setInitValue(getMarkerValue(info, new Date(model.docDate.toString()) as any, null), self._model);

                info = self._markersControls["LegalSubject"].markerInfo;
                self._markersControls["LegalSubject"].setInitValue(getMarkerValue(info, model.legalSubjectId as any, model.legalSubjectsName), self._model);

                info = self._markersControls["Contract"].markerInfo;
                self._markersControls["Contract"].setInitValue(getMarkerValue(info, model.contractId as any, model.contractName), self._model);

                info = self._markersControls["WHWarehouses"].markerInfo;
                self._markersControls["WHWarehouses"].setInitValue(getMarkerValue(info, model.whWarehousesId as any, model.warehouseName), self._model);

                ($$(self.formId) as webix.ui.form).setValues(self._model);
            }
        }

        function getMarkerValue(info: Markers.IMarkerInfoModel, value: string, displayValue: string): Markers.IMarkerValueModel {
            return {
                displayValue: displayValue,
                itemId: null,
                endDate: null,
                markerId: info.id,
                markerType: info.markerType,
                MVCAliase: info.name,
                note: null,
                isVisible: true,
                isBlocked: false,
                startDate: null,
                value: value,
                isDeleted: false
            }
        }
    }

    getDataFromDialog(): ConsignmentNotes.IConsignmentNotesDocumentUpdateModel {
        return {
            baId: (!this._item)? null : this._item.baId,
            baTypeId: 80,
            cAId: Number(this._markersControls["CompanyArea"].getValue(this._fieldsInfo).value), 
            contractId: Number(this._markersControls["Contract"].getValue(this._fieldsInfo).value) === 0
                ? null
                : Number(this._markersControls["Contract"].getValue(this._fieldsInfo).value),
            lSId: Number(this._markersControls["LegalSubject"].getValue(this._fieldsInfo).value),
            docDate: new Date(this._markersControls["DocDate"].getValue(this._fieldsInfo).value),
            number: this._markersControls["Name"].getValue(this._fieldsInfo).value,
            note: this._markersControls["Note"].getValue(this._fieldsInfo).value,
            wHId: Number(this._markersControls["WHWarehouses"].getValue(this._fieldsInfo).value),
        } as ConsignmentNotes.IConsignmentNotesDocumentUpdateModel;
    }

    createMarkerControl(markerInfo: Markers.IMarkerInfoModel) {
        if (markerInfo.id === 1) {
            let marker = MF.MarkerControlfactory.getControl(this.viewName,
                    markerInfo, 
                    (content) => {
                        return {    
                            labelWidth: this.labelWidth
                        }
                    });
            this._markersControls["Name"] = marker;
            return marker;
        }
        if (markerInfo.id === 6) {
            let marker = MF.MarkerControlfactory.getControl(`${this.viewName}-organisation`,
                markerInfo,
                (content) => {
                    return {
                        labelWidth: this.labelWidth,
                        disabled: true
                    }
                });
            (marker as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
            (marker as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this._module);
            //marker.control.config
            this._markersControls["CompanyArea"] = marker;
            return marker;  
        }

        if (markerInfo.id === 7) {
            let marker = MF.MarkerControlfactory.getControl(`${this.viewName}-note`, markerInfo, (content) => {
                return {
                    labelWidth: this.labelWidth
                }
            });
            this._markersControls["Note"] = marker;
            return marker;
        }

        if (markerInfo.id === 17) {
            let marker = MF.MarkerControlfactory.getControl(`${this.viewName}-docDate`, markerInfo, (content) => {
                return {
                    labelWidth: this.labelWidth
                }
            });
            this._markersControls["DocDate"] = marker;
            return marker;
        }

        if (markerInfo.id === 40) {
            let marker = MF.MarkerControlfactory.getControl(`${this.viewName}-contract`, markerInfo, (content) => {
                return {
                    labelWidth: this.labelWidth
                }
            });
            (marker as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
            (marker as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this._module);
            this._markersControls["Contract"] = marker;
            return marker;
        }

        if (markerInfo.id === 19) {
            let marker = MF.MarkerControlfactory.getControl(`${this.viewName}-legalSubject`, markerInfo, (content) => {
                return {
                    labelWidth: this.labelWidth
                };
            });
            (marker as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
            (marker as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this._module);
            this._markersControls["LegalSubject"] = marker;
            return marker;
        }

        if (markerInfo.id === 134) {
            let marker = MF.MarkerControlfactory.getControl(`${this.viewName}-warehouse`, markerInfo, (content) => {
                return {
                    labelWidth: this.labelWidth,
                }
            });
            (marker as EntityRefMarkerControl.IEntityRefMarkerControllAddButtonDisabler).isDisabledAddButton = true;
            (marker as EntityRefMarkerControl.IEntityRefMarkerControl).setModule(this._module);
            this._markersControls["WHWarehouses"] = marker;
            return marker;
        }
    }
}

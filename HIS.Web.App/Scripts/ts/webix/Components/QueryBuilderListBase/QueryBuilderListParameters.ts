﻿import Common = require("../../Common/CommonExporter");
import { IQueryBuilderListSetParameters } from "./QueryBuilderListBase";

export interface IColumnsInfo {
    baTypeId: number,
    baIdAlias: string,
    stateAlias: string,
    baIdColumnInfo: Querybuilder.ICaptionAndAliaseModel,
    columns: any[],
    columnInfo: { [id: string]: Querybuilder.ICaptionAndAliaseModel }

}

export class QueryBuilderListParameters {

    public showBaId: boolean = false;
    public tooltip: boolean = false;
    public allWithoutFilters: boolean = false;
    public entityInfo: Entities.IEntityInfoModel;
    public viewItems: Entities.IEntityViewItemModel[];
    public hiddenMarkerTypes = [5, 11, 12, 13] as number[];
    public aliasesOrder = [] as string[];
    public aliasesWithoutFilter = [] as string[];
    public aliasesSkip = [] as string[];
    public filteringsFields = [] as object[];


    constructor(parameters: IQueryBuilderListSetParameters) {

        if (!isEmptyObject(parameters)) {

            this.showBaId = !(!parameters.showBaId);
            this.tooltip = !(!parameters.tooltip);
            this.entityInfo = parameters.entityInfo;

            if (parameters.hiddenMarkerTypes) {
                this.hiddenMarkerTypes = parameters.hiddenMarkerTypes;
            }

            if (parameters.aliasesOrder) {
                this.fiilAliasesOrder(parameters.aliasesOrder);
            }

            if (parameters.aliasesWithoutFilter) {
                this.fillAliasesWithoutFilter(parameters.aliasesWithoutFilter);
            }
            if (parameters.aliasesSkip) {
                this.fillAliasesSkip(parameters.aliasesSkip);
            }
            if (parameters.allWithoutFilters) {
                this.allWithoutFilters = true;
            }
        }
    }

    private fillAliasesSkip(aliases: string[]) {
        if (!aliases || aliases.length === 0) {
            return;
        }
        const tableName = this.entityInfo.mvcAlias;
        aliases.forEach(item => {
            this.aliasesSkip.push(`[${tableName}//${item}]`);
            this.getAliasesMarkerName(item).forEach(setItem => {
                this.aliasesSkip.push(setItem);
            });
        });
    }

    private fillAliasesWithoutFilter(aliases: string[]) {
        if (!aliases || aliases.length === 0) {
            return;
        }
        const tableName = this.entityInfo.mvcAlias;
        aliases.forEach(item => {
            this.aliasesWithoutFilter.push(`[${tableName}//${item}]`);
            this.getAliasesMarkerName(item).forEach(setItem => {
                this.aliasesWithoutFilter.push(setItem);
            });
        });

    }

    private fiilAliasesOrder(aliases: string[]) {
        if (!aliases || aliases.length === 0) {
            return;
        }
        const tableName = this.entityInfo.mvcAlias;
        aliases.forEach(item => {
            this.aliasesOrder.push(`[${tableName}//${item}]`);
            this.getAliasesMarkerName(item).forEach(setItem => {
                this.aliasesOrder.push(setItem);
            });
        });
    }

    private getAliasesMarkerName(aliase: string): string[] {
        const outArray = [] as string[];
        const tableName = this.entityInfo.mvcAlias;
        const markerTypesWitnName = [5, 11, 12, 13];
        this.entityInfo.markersInfo.forEach(
            item => {
                if (item.name === aliase && markerTypesWitnName.indexOf(item.markerType) >= 0) {
                    outArray.push(`[${tableName}//${item.implementTypeField}//Name]`);
                    outArray.push(`[${tableName}//${item.implementTypeField}//ItemName]`);
                    outArray.push(`[${tableName}//${aliase}//Name]`);
                    outArray.push(`[${tableName}//${aliase}//ItemName]`);
                    return;
                }
            });
        return outArray;
    }

    public getOptionsByAliase(aliase:string):object[] {
        if (aliase === this.getStateAliase()) {
           return this.getStateOptions();
        }
        const marker= this.getMarkerByAliase(aliase) as any;
        return this.getOprionsByMarker(marker);
    }

    private getOprionsByMarker(marker: Markers.IMarkerInfoModel): object[] {
        if (isEmptyObject(marker)) {
            return [];
        }
        const options = [];
        switch (marker.markerType) {
            case HIS.Models.Layer.Models.Markers.MarkerType.MtList:
                if (marker.list.length > 0) {
                   options.push({id: "",value:""} as any);

                    marker.list.forEach(item => {
                        options.push({
                            id: item.value,
                            value:item.value
                        } as any);
                    });
                }
            break;
            case HIS.Models.Layer.Models.Markers.MarkerType.MtReference:
                const views = this.viewItems.filter(x => x.markerId === marker.id);
                if (views.length > 0) {
                    options.push({ id: "", value: "" } as any);
                    views.forEach(item => {
                        options.push({
                            id: item.itemName,
                            value: item.itemName
                        } as any);
                    });

                }
            break;
        }
        return options;
    }

    private getMarkerByAliase(aliase: string): Markers.IMarkerInfoModel {
        let marker:Markers.IMarkerInfoModel = null;
        const tableName = this.entityInfo.mvcAlias;
        this.entityInfo.markersInfo.forEach(
            item => {
                if (aliase === `[${tableName}//${item.implementTypeField}//Name]` ||
                    aliase === `[${tableName}//${item.implementTypeField}//ItemName]` ||
                    aliase === `[${tableName}//${aliase}//Name]` ||
                    aliase === `[${tableName}//${aliase}//ItemName]`) {
                    marker = item;
                    return item;
                }
            });
        return marker;
    }


    public getCompanyAliase(): string {
        let result = "";
        this.entityInfo.markersInfo.forEach(item => {
            if (item.name === "CompanyArea") {
                result = `[${this.entityInfo.mvcAlias}//CompanyArea]`;
            }

        });
        return result;
    }
    public getBaTypeIdAliase(): string {
        return `[${this.entityInfo.mvcAlias}//BATypeId]`;
    }
    public getBaIdAliase(): string {
        return `[${this.entityInfo.mvcAlias}//BAId]`;
    }

    public getStateAliase(): string
    {
        return `[${this.entityInfo.mvcAlias}//State]`;
    }
    private getStateOptions(): any {
        const options = [];
        if (this.entityInfo.states.length > 0) {
            options.push({ id: "", value: "" } as any);
            this.entityInfo.states.forEach(item => {
                options.push({
                    id: item.name,
                    value: item.name
                } as any);
            });
        }
        return options;
     
    }

    public sortFields(fields: Querybuilder.ICaptionAndAliaseModel[]): Querybuilder.ICaptionAndAliaseModel[] {
        const aliases = this.aliasesOrder;
        return fields.sort((first: Querybuilder.ICaptionAndAliaseModel, second: Querybuilder.ICaptionAndAliaseModel) => {
            const toEndIndex = 999999;
            let firstIndex = aliases.indexOf(first.aliase);
            let secondIndex = aliases.indexOf(second.aliase);

            if (firstIndex < 0) { firstIndex = toEndIndex; }
            if (secondIndex < 0) { secondIndex = toEndIndex; }

            if (firstIndex === secondIndex) { return 0; }
            if (firstIndex < secondIndex) { return -1 }

            return 1;

        });
    }
    public filterOperatorByFieldType(fieldType: HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType) {
        switch (fieldType) {
        case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Boolean:
        case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Date:
        case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Number:
            return HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal;
        case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.String:
            return HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Like;
        default:
            return HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal;
        }
    }

}
function isEmptyObject(obj: object): boolean {

    if (obj == null || obj == undefined) {
        return true;
    }
    if (Object.keys(obj).length === 0) {
        return true;
    }
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
}




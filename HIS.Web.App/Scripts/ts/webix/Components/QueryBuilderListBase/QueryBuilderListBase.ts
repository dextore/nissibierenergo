﻿import Pager = require("../Pager/Pager");
import Api = require("../../Api/ApiExporter");
import Parameters = require("./QueryBuilderListParameters");
import IColumnsInfo = Parameters.IColumnsInfo;


export interface IQueryBuilderListSetParameters {
    showBaId: boolean;
    tooltip: boolean;
    allWithoutFilters:boolean;
    entityInfo: Entities.IEntityInfoModel;
    hiddenMarkerTypes: number[];
    aliasesOrder: string[];
    aliasesSkip: string[];
    aliasesWithoutFilter: string[];
    showOptionalMarkers: boolean; ///модель с маркерами , что бы можно было определить какие опциональные и заблокировать их

 }
export interface IQueryBuilderList {
    init();
    reload();
    ready();
    updateRow(baId: number, recordId?: number);
    getRowDataByBaId(baId: number): Promise<any>;
    selectFirstRow();
    setFilteringFields(filters: object[]);
    getFilteringFields(): object[];
    getFieldAlias(markerId: number);
    getBAIdAliase();
    getBATypeIdAliase();
    getCompanyAliase();
    getStateAliase();
    
}

export function getQueryBuilderList(viewName: string, queryBuilderParam: IQueryBuilderListSetParameters) {
    return new QueryBuilderList(viewName, queryBuilderParam);
}
class QueryBuilderList implements IQueryBuilderList {
    
    private _hiddenColumnCaption = { "BAId": 1, "BATypeId": 1, "IsFirstState": 1, "IsDelState": 1 }
    private get tableId() { return this._viewName; }
    private get pagerId() { return `${this._viewName}-pager-id`; }
    private _columnsInfo = null as IColumnsInfo;
    private _qbParameters = null as Parameters.QueryBuilderListParameters;

    private get tableControl() {
        return $$(this.tableId) as webix.ui.datatable;
    }
    constructor(private readonly _viewName: string, setParameters: IQueryBuilderListSetParameters) {
        this._qbParameters = new Parameters.QueryBuilderListParameters(setParameters);
    }

    public selectFirstRow():void {
        const firstId = this.tableControl.getFirstId();
        this.tableControl.select(firstId, false);
        this.tableControl.setPage(0);
    }
    public updateRow(baId: number, recordId?: number) {

        this.getRowDataByBaId(baId).then(data => {
            const row = data.data[0];
            if (!recordId) {
                this.tableControl.add(row, 0);
                this.selectFirstRow();
            } else {
                this.tableControl.updateItem(recordId, row);
            }

        });
    }
    public getRowDataByBaId(baId: number): Promise<any> {
        const p = webix.promise.defer();

        const startFiltering = this.getFilteringFields() as object[];

        if (baId) {
            this.setFilteringFields([{
                field: this.getBAIdAliase(),
                filterOperator: 0,
                value: baId
            }]);
        }

        this.getData(this.tableControl, null).then(data => {
            this.setFilteringFields(startFiltering);
            return (p as any).resolve(data);
        }).catch(error => {
            this.setFilteringFields(startFiltering);
            return (p as any).reject();
        });

        return p;

    }

    setFilteringFields(filters: object[]) {
        this._qbParameters.filteringsFields = filters;
    }
    getFilteringFields() {
        return this._qbParameters.filteringsFields;
    }

    ready() {
        webix.extend(this.tableControl, webix.ProgressBar);
        this.initTableColumns();
    }
    init() {
        const pager = Pager.getDataTablePager(this.pagerId);
        return {
            rows: [
                this.initDataTable(),
                pager.init()
            ]
        };
    };
    reload() {
        this.tableControl.load(this.tableControl.config.url);
    }

    private initTableColumns() {
      
        const promise: Promise<any>[] = [];
        promise.push(Api.getApiCommons().getEntityViewsItems(this._qbParameters.entityInfo.baTypeId));
        promise.push(Api.getApiQueryBuilder().getFields(this._qbParameters.entityInfo.baTypeId));

        this.tableControl.clearAll();
        this.tableControl.define("columns", []);
        this.tableControl.refreshColumns();

        (webix.promise.all(promise as any) as any).then((data: any[]) => {
            this._qbParameters.viewItems = data[0];
            this._columnsInfo = this.getFields(data[1]);
            this.tableControl.define("columns", this._columnsInfo.columns);
            this.tableControl.refreshColumns();
            this.tableControl.refresh();
            this.reload();
          
        }).catch(error => {
            webix.message("Ошибка формирования таблицы", "error");
        });

    }
    private initDataTable() {
        const self = this;

        return {
            view: "datatable",
            id: this.tableId,
            select: "row",
            scroll: true,
            resizeColumn: true,
            columns: [],
            pager: this.pagerId,
            tooltip: this._qbParameters.tooltip,
            url: {
                $proxy: true,
                load: (view, callback, params) => {

                    self.tableControl.clearAll();
                    if ((self.tableControl as any).showProgress)
                        (self.tableControl as any).showProgress();
                    self.getData(view, params)
                        .then(data => {
                            (webix.ajax as any).$callback(view, callback, data);
                            if ((self.tableControl as any).hideProgress)
                                (self.tableControl as any).hideProgress();
                        }).catch(() => {
                            (webix.ajax as any).$callback(view, callback, []);
                            if ((self.tableControl as any).hideProgress)
                                (self.tableControl as any).hideProgress();
                        });
                }
            }
        };


     

    }
    private getFields(data: Querybuilder.ICaptionAndAliaseModel[]): IColumnsInfo {
        const fields = this._qbParameters.sortFields(data);
        return this.fillFields(this._qbParameters.entityInfo.baTypeId, fields);
    }
    private fillFields(baTypeId: number, data: Querybuilder.ICaptionAndAliaseModel[]): IColumnsInfo {

        let columsWidth: number = 0;
        const tableWidth = this.tableControl.$width as number;

        const result = {
            baTypeId: baTypeId,
            baIdAlias: "",
            baIdColumnInfo: null,
            columns: [],
            columnInfo: {}
        } as IColumnsInfo;

        data.forEach(item => {
            
            let hidden = false as boolean;
            if (this._hiddenColumnCaption[item.caption]) {
                hidden = true;
            }
            if (item.aliase.indexOf("BAId") >= 0) {
                result.baIdAlias = item.aliase;
                result.baIdColumnInfo = item;
                hidden = !this._qbParameters.showBaId;
            }
            if (this._qbParameters.aliasesSkip &&
                this._qbParameters.aliasesSkip.indexOf(item.aliase) >= 0) {
                return;
            }

            if (this._qbParameters.hiddenMarkerTypes &&
                this._qbParameters.hiddenMarkerTypes.indexOf(item.markerTypeId) >= 0) {
                hidden = true;
            }

            result.columnInfo[item.aliase] = item;

            let columnWidth = webix.html.getTextSize(item.caption, "webix_hcell").width;
            columnWidth = (columnWidth < 100) ? 100 : columnWidth;
            if (!hidden) {
                columsWidth += columnWidth;
            }

            const header = this.getColumnHeader(item);

            switch (item.type) {
                case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Boolean:
                case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Number:
                case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.String:
                    result.columns.push({
                        id: item.aliase,
                        header: header,
                        width: columnWidth,
                        hidden: hidden,
                        sort: "server"
                    });
                    break;
                case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Date:
                    result.columns.push({
                        id: item.aliase,
                        header: header,
                        width: columnWidth,
                        hidden: hidden,
                        template: (row, common) => {
                            if (row[item.aliase])
                                return webix.i18n.dateFormatStr(new Date(row[item.aliase]));
                            return "";
                        },
                        sort: "server"
                    });
                    break;
            }
        });

        if (columsWidth < tableWidth) {
            result.columns.forEach(item => {
                item.fillspace = true;
            });
        }

        return result;
    }
    private getColumnHeader(item: Querybuilder.ICaptionAndAliaseModel): any {

        let header = item.caption as any;

        
        if ((this._qbParameters.aliasesWithoutFilter && 
            this._qbParameters.aliasesWithoutFilter.indexOf(item.aliase) >= 0) || 
            this._qbParameters.allWithoutFilters) {
         
            return header;
        }
        switch (item.type) {
            case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Boolean:
            case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Number:
            case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.String:
                header = [item.caption, { content: "serverFilter" }];
                const options = this._qbParameters.getOptionsByAliase(item.aliase);
                if (options.length > 0) {
            
                    header = [
                        item.caption, {
                            content: "serverSelectFilter",
                            options: options
                        }
                    ];
                }


                break;
            case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Date:
                header = [item.caption, { content: "datepickerFilter" }];
                break;
        }
        return header;
    }
    private getData(view: webix.ui.datatable, params:any): Promise<any> {

        const p = webix.promise.defer();
        const columnsInfo = this._columnsInfo;
       
        if (!columnsInfo) {
            (p as any).resolve([]);
            return p;
        }
        const pager = view.getPager();
        const from = (!params) ? 0 : params.start;
        const to = (!params) ? pager.data.size : params.start + params.count;
        const filteringsFields = (!this._qbParameters || !this._qbParameters.filteringsFields)
            ? []
            : this._qbParameters.filteringsFields;

     
        const queryParams = {
            baTypeId: columnsInfo.baTypeId,
            from: from,
            to: to,
            filteringsFields: [],
            orderingsFields: [(params && params.sort)
                ? {
                    orderingField: params.sort.id, sortingOrder: params.sort.dir === "asc"
                        ? HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Asc
                        : HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Desc
                }
                : {
                    orderingField: columnsInfo.baIdAlias, sortingOrder: HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Desc

                }]
        };

        if (params && params.filter) {
            for (let value in params.filter) {
                if (params.filter.hasOwnProperty(value)) {
                    if (!params.filter[value])
                        continue;
                    queryParams.filteringsFields.push({
                        field: value,
                        filterOperator: this._qbParameters.filterOperatorByFieldType(columnsInfo.columnInfo[value].type),
                        value: columnsInfo.columnInfo[value].type === HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Date
                            ? this.filterDateTimeConverter(params.filter[value] as Date)
                            : params.filter[value]
                    });

                }
            }
        }
        this._qbParameters.filteringsFields.forEach(item => {
            queryParams.filteringsFields.push({
                field: item["field"],
                filterOperator: item["filterOperator"],
                value: item["value"]
            });
        });
        Api.getApiQueryBuilder().getData(queryParams as any).then((items) => {

            const result = JSON.parse(items as any);

            const data = queryParams.from === 0
                ? {
                    data: result.items,
                    pos: 0,
                    total_count: result.rowCount
                }
                : {
                    data: result.items,
                    pos: queryParams.from
                };

            (p as any).resolve(data);
        }).catch(() => {
            (p as any).reject();
        });

        return p;
    }
    public getFieldAlias(markerId: number): string {

        if (!this._columnsInfo)
            return "";
        const columnInfo = this._columnsInfo.columnInfo;
        for (let index in columnInfo) {
            if (columnInfo.hasOwnProperty(index)) {
                if (columnInfo[index].markerId === markerId)
                    return columnInfo[index].aliase;
            }
        }
    }

    public getCompanyAliase(): string {
        return this._qbParameters.getCompanyAliase();
    }

    public getBAIdAliase(): string {
        return this._qbParameters.getBaIdAliase();
    }
    public getBATypeIdAliase(): string {
        return this._qbParameters.getBaTypeIdAliase();
    }
    

    public getStateAliase(): string {
        return this._qbParameters.getStateAliase();
    }
    private filterDateTimeConverter(date: Date) {
        const tzoffset = date.getTimezoneOffset() * 60000; //offset in milliseconds
        return (new Date((date as any) - tzoffset)).toISOString().slice(0, -1);
    }

}
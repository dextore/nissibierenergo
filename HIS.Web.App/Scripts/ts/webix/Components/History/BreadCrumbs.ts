﻿

import Common = require("../../Common/CommonExporter");

export function getBreadCrumbs(viewName: string): IBreadCrumbs {
    return new BreadCrumbs(viewName);
};
export interface IBreadCrumbs {
    setActiveItemById(itemId: number);
    addItemCrumb(item: ICrumb);
    init();
    generateHtml();
    updateHtml();
    selectedItem: number;
    controlEvent: Common.Event<number>;
}
interface ICrumb {
    id: number;
    title: string;
    url: string;
}
class BreadCrumbs implements IBreadCrumbs {

    private viewName: string = "";
    get templateId() { return this.viewName; }

    constructor(viewName: string) {
        this.viewName = viewName;
    }
    private _controlEvent = new Common.Event<number>();

    get controlEvent(): Common.Event<number> {
        return this._controlEvent;
    }

    get selectedItem() {
        return this.activeItem.id;
    }

    public init(): object {
        const self = this;
        return {
            view: "template",
            type: "header",
            id: self.templateId,
            padding:0,
            margin: 0,   
            height:30,
            template: () => {
                return self.generateHtml();
            }, 
            onClick: {
                "crumbItem": (ev, id, el) => {
                    const activeItem = this.selectedItem;
                    const itemId  =el.id.replace(this.viewName + "-item-", "") as number;
                    if (activeItem != itemId) {
                        this.setActiveItemById(itemId as number);
                        this._controlEvent.trigger(webix.copy(itemId), this);
                    }
                  
                }
            
            }
        };
    }
    private crumbs: ICrumb[] = [];
    private activeItem: ICrumb;
    public addItemCrumb(item: ICrumb) {
        this.crumbs.push(item);
        this.setActiveItem();      
    }
    public setActiveItemById(itemId: number) {
        const newCrumbs: ICrumb[] = [];
        let isAddItem: boolean = true;
        this.crumbs.forEach(item => {
            if (isAddItem == true) {
                newCrumbs.push(item);
            }
            if (item.id == itemId) {
                isAddItem = false;
            }
        });
        this.crumbs = newCrumbs;
        this.setActiveItem();
        this.updateHtml();
    }
    public updateHtml() {
        ($$(this.templateId) as webix.ui.template).refresh();
    }
    private getLastItem(): ICrumb {
        return this.crumbs[this.crumbs.length - 1];
    }
    private setActiveItem(): void {
        this.activeItem = this.getLastItem(); 
    }
    private getFirstItem(): ICrumb {
        return this.crumbs[0];
    }
    public generateHtml(): string {

        let maxCrumbs = 3; 
        let isShowHomeButton = false;
        const countCrumbs = this.crumbs.length;
     
        if (maxCrumbs < countCrumbs) {
            maxCrumbs--;
            isShowHomeButton = true;
        }

        let html: string = "";
        let counter = 0;
        const itemsHtml: string[] = [];

        const crumbs = this.crumbs as ICrumb[];
        this.crumbs.forEach(item => {
            if (counter < 5) {
                counter++;
            }
            let title = item.title;
            if (!title || title == "") {
                title = "Экземпляр сущности";
            }
            html = "<a id='" + this.viewName + "-item-" + item.id.toString() + "' class='n" + counter.toString() + " crumbItem'>" + title + "</a>";
            itemsHtml.push(html);
        });
    
        html = "<div class='webix_layout_toolbar panel_breadcrumb'><div class='breadcrumb'>";
        if (isShowHomeButton == true) {
            const firstItem = this.getFirstItem() as ICrumb;
            let title = firstItem.title;
            if (!title || title == "") {
                title = "Экземпляр сущности";
            }
            html += "<a id='" + this.viewName + "-item-" + firstItem.id.toString() + "' class='nHomeCrumbItem crumbItem'>" + title + "....</a>";
        }

        const start = countCrumbs - maxCrumbs;  
        counter = 1;
        itemsHtml.forEach(item => {
            if (counter > start) {
                html += item;
            }
            counter++;
        });
        html += "</div></div>";

        return html;
    }
}

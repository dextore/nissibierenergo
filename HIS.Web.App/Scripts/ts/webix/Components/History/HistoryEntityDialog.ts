﻿import Base = require("../../Common/DialogBase");
import Common = require("../../Common/CommonExporter");
import Pager = require("../Pager/Pager");
import Api = require("../../Api/ApiExporter");
import BreadCrumbs = require("./BreadCrumbs");

export function getHistoryEntityDialog(viewName: string, mainTabView: Common.IMainTabbarView, baId: number): IHistoryEntityDialog {
    return new HistoryEntityDialog(viewName, mainTabView, baId);
};
export interface IHistoryEntityDialog extends Base.IDialog {
}

class HistoryEntityDialog extends Base.DialogBase {

    get stateTableId() { return `${this.viewName}-state-table-id`; }
    get markersTableId() { return `${this.viewName}-markers-table-id`; }
    get stateTabId() { return `${this.viewName}-state-tab-id`; }
    get markersTabId() { return `${this.viewName}-markers-tab-id`; }
    get statePagerId() { return `${this.viewName}-state-pager-id`; }
    get markersPagerId() { return `${this.viewName}-markers-pager-id`; }

    private breadCrumbs: BreadCrumbs.IBreadCrumbs;

    constructor(viewName: string, private _mainTabView: Common.IMainTabbarView, private _baId: number = 0) {
        super(viewName);
        this.breadCrumbs = BreadCrumbs.getBreadCrumbs(`${this.viewName}-breadcrumbs-id`);

        Api.getApiHistory().getEntityData(this._baId).then((data) => {

            const entityName = this.getEntityProperty(data, "Name");
            this.breadCrumbs.addItemCrumb({ id: this._baId, title: entityName } as any);
            this.subscribe(this.breadCrumbs.controlEvent.subscribe(this.selectBreadcrumbsItem, this));
            this.breadCrumbs.updateHtml();

        }).catch(() => {

            this.breadCrumbs.addItemCrumb({ id: this._baId, title: "Экземпляр сущности" } as any);
            this.subscribe(this.breadCrumbs.controlEvent.subscribe(this.selectBreadcrumbsItem, this));
            this.breadCrumbs.updateHtml();

        });
    }

    private getEntityProperty(data: object, property: string): string
    {
        let result = "";
        let item: object = {};
        const search = "//" + property + "]";

        (data["items"] as object[]).forEach(x => {
            item = x;         
        });

        for (const key in item) {
            if (item.hasOwnProperty(key)) {
                if (key.indexOf(search) >= 0) {
                    result = item[key] as string;
                    break;
                }
            }
        }
        return result;

    }


    protected headerLabel(): string {
        return "История изменения экземпляра сущности";
    }
    get mainTabView(): Common.IMainTabbarView {
        return this._mainTabView;


    }
    protected windowConfig() {
        return {
            autoheight: true,
            width: 900
        };
    }
    afterShowDialog()
    {
        this.switchTabs();                                                                  
    }

    selectBreadcrumbsItem() {
        const itemId = this.breadCrumbs.selectedItem;
        this._baId = itemId;
        this.reloadMarkersHistory();
    }

    protected contentConfig() {
        const self = this;
        return {
            rows: [
                this.breadCrumbs.init(),
                {
                    id: self.stateTabId, 
                    hide:true,
                    rows: [
                        self.getStateTable(),
                        self.getPager(self.statePagerId)
                    ]
                }, 
                {
                    id: self.markersTabId,    
                    hide: true,
                    rows: [
                        self.getMarkersHistoryTable(),
                        self.getPager(self.markersPagerId)
                    ]
                },
                this.getTabBar()
            ]
        };
    }

    private switchTabs(tabId: string = "") {
        if (!tabId) {
            tabId = this.stateTabId;
        }    
        $$(this.stateTabId).hide();
        $$(this.markersTabId).hide();
        $$(tabId).show();
    }
    private getTabBar()
    {
        const self = this;
        return {
            view: "tabbar",
            type: "bottom",
            multiview: true,
            height: 50,
            on: {
                onChange: (newv, oldv) => {
                    self.switchTabs(newv);
                }
            },
            options: [
                { value: "Изменение состояний", id: self.stateTabId },
                { value: "Изменение маркеров", id: self.markersTabId }
            ]
        };
    }
    private reloadMarkersHistory() {

        const tableMarkers = $$(this.markersTableId) as webix.ui.datatable;
        const tableState = $$(this.stateTableId) as webix.ui.datatable;

        this.clearTableFilters(this.markersTableId);
        this.clearTableFilters(this.stateTableId);
        this._baId = this.breadCrumbs.selectedItem;
        this.breadCrumbs.updateHtml();

        tableMarkers.load(tableMarkers.config.url);
        tableState.load(tableState.config.url);
    }
    private clearTableFilters(tableId: string) {

        const tablesFilters: string[] = [
            "operationDate",
            "operationName",
            "markerName",
            "markerValue",
            "itemId",
            "srcStateName",
            "dstStateName"
        ];
        const dataTable = $$(tableId) as webix.ui.datatable;     
        dataTable.eachColumn((columnId) => {
            if (tablesFilters.indexOf(columnId) >= 0) {
                let filter = dataTable.getFilter(columnId);
                if (filter) {
                    if (filter.setValue) filter.setValue("");
                    else filter.value = "";					
                }
            }
        });
        dataTable.filterByAll();
     
    }
    private getMarkersHistoryTable() {

        const self = this;

        return {
            view: "datatable",
            id: self.markersTableId,
            select: "row",
            scroll: true,
            resizeColumn: true,
            pager: self.markersPagerId,
            tooltip: true,
            on: {
                onBeforeFilter: () => {
                    /*
                    self.isFilterUpdate = true;
                    const table = $$(this.stateTableId) as webix.ui.datatable;
                    table.load(table.config.url);
                    */
                }, 
                onAfterLoad: () => {
                    const dataTable = $$(self.markersTableId) as webix.ui.datatable;
                    dataTable.eachRow((rowid) => {
                        let currentItem = dataTable.getItem(rowid);
                        if (currentItem.color % 2 === 0)
                            dataTable.addRowCss(rowid, "webix_cell_newis"); //even
                        else dataTable.addRowCss(rowid, "special_webix_grid_color");
                    });
                },
                onItemClick: (e, id, node) => {               
                    
                    if (e.column !== "markerValueBAId") {
                        return;
                    }                
                    const tableMarkers = $$(self.markersTableId) as webix.ui.datatable;
                    const marker = tableMarkers.getSelectedItem() as History.IEntityHistoryModel;                
                    if (!marker.markerValueBAId) {
                        return;
                    }
                    this.breadCrumbs.addItemCrumb({
                        id: marker.markerValueBAId,
                        title:marker.markerValue
                    } as any);
                    self.reloadMarkersHistory();
                }
            },
            columns: [
                { id: "baId", header: "baId", hidden: true },
                { id: "userId", header: "ID", hidden: true },
                { id: "userName", header: "Пользователь", width: 200 },
                { id: "operationId", header: "operationId", hidden: true },
                { id: "operationDate", header: ["Дата операции", { content: "dateRangeFilter", timepicker: true, }], width: 200, sort: "date", format: webix.i18n.fullDateFormatStr },
                { id: "operationName", header: ["Операция", { content: "selectFilter" }], width: 120 },
                { id: "markerId", header: "markerId", hidden: true },
                { id: "markerName", header: ["Маркер", { content: "selectFilter" }], width: 150 },
                { id: "markerValue", width: 150, header: ["Значение", { content: "textFilter" }] },
                {
                    id: "markerValueBAId", width: 30,
                    header: [""],
                    tooltip: false,
                    template: (row: History.IEntityHistoryModel) => {
                        if (row.markerValueBAId) {
                            return "<span class='webix_icon fa-ellipsis-h'></span>";
                        }
                        return "";
                    }
                },  
                {
                    id: "startDate", width: 90, format: webix.i18n.dateFormatStr,
                    header: [{ content: "columnGroup",  closed: false, batch:"period",  groupText: "Период действия", colspan: 2 },"C"]
                },
                    
                { id: "endDate", header: [null, "ПО"], batch: "period", width: 90, format: webix.i18n.dateFormatStr },
                { id: "itemId", header: ["Порядковый №", { content: "textFilter" }], width: 100, },
                {
                    id: "oldStartDate", width: 90, format: webix.i18n.dateFormatStr,
                    header: [{ content: "columnGroup", closed: false, batch: "oldperiod", groupText: "Старый период действия", colspan: 2 }, "C"]
                },

                { id: "oldEndDate", header: [null, "ПО"], batch: "oldperiod", width: 90, format: webix.i18n.dateFormatStr },
                { id: "oldMarkerValue", width: 150, header: ["Старое значение", { content: "textFilter" }] },
             
            ],
            url: {
                $proxy: true,
                load: function (view, callback, params) {
                    const dataTable = $$(self.markersTableId) as webix.ui.datatable;
                    dataTable.clearAll();
                    dataTable.showOverlay("Загрузка....");
                    Api.getApiHistory().getEntityMarkersHistory(self._baId).then((data: History.IEntityHistoryModel[]) => {
                        dataTable.clearAll();
                        (webix.ajax as any).$callback(view, callback, data);
                        dataTable.hideOverlay();
                    }).catch(() => {
                        dataTable.hideOverlay();
                        (webix.ajax as any).$callback(view, callback, []);
                    });
                }
            }
        };
    }

    private getStateTable() {
        const self = this;
        return {
            view: "datatable",
            id: self.stateTableId,
            select: "row",
            scroll: "y",
            resizeColumn: true,
            scrollY: true,
            pager: self.statePagerId,
            on: {
                onBeforeFilter: () => {
                    /*                              
                    self.isFilterUpdate = true;
                    const table = $$(this.stateTableId) as webix.ui.datatable;
                    table.load(table.config.url);
                    */
                },
            },
            columns: [
                { id: "baId", header: "baId", hidden: true },
                { id: "userId", header: "ID", hidden: true },
                { id: "userName", header: "Пользователь", width: 220 },
                { id: "operationId", header: "operationId", hidden: true },
                { id: "operationDate", header: ["Дата операции", { content: "dateRangeFilter" }], sort: "date",  format: webix.i18n.fullDateFormatStr, width: 250 },
                // format: webix.i18n.fullDateFormatStr
                { id: "operationName", header: ["Операция", { content: "selectFilter" }], fillspace: true, width: 200 },
                { id: "srcStateId", header: "srcStateId", hidden: true },
                { id: "srcStateName", header: ["Начальное состояние", { content: "selectFilter" }], fillspace: true, width: 200 },
                { id: "dstStateId", header: "dstStateId", hidden: true },
                { id: "dstStateName", header: ["Конечное состояние", { content: "selectFilter" }], fillspace: true, width: 200 }
            ],
            url: {
                $proxy: true,
                load: function (view, callback, params) {
                    const dataTable = $$(self.stateTableId) as webix.ui.datatable;
                    dataTable.clearAll();
                    dataTable.showOverlay("Загрузка....");
                    Api.getApiHistory().getEntityStateHistory(self._baId).then((data: History.IEntityHistoryModel[]) => {

                        dataTable.clearAll();
                        (webix.ajax as any).$callback(view, callback, data);
                        dataTable.hideOverlay();
                    }).catch(() => {
                        dataTable.hideOverlay();
                        (webix.ajax as any).$callback(view, callback, []);
                    });
                }
            }
        };  
    }

    private getPager(pagerId:string): object {

        const pager = Pager.getDataTablePager(pagerId);
        return pager.init();
    }
}

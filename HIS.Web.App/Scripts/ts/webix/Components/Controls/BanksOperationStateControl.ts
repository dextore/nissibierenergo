﻿import Base = require("../ComponentBase");
import ToolButtons = require("../Buttons/ToolBarButtons");
import Api = require("../../Api/ApiExporter");
import Common = require("../../Common/CommonExporter");
import History = require("../../Components/History/HistoryEntityDialog");


export interface IEntityOperationStateControl extends Base.IComponent  {
    baId: number;
    ready();
    stateChangesEvent: Common.Event<any>;
}

export function getBanksOperationStateControl(viewName: string,
    getHeaderInfo: (baId: number) => Promise<string>,
    isPossibilityStatusChanging: () => boolean): IEntityOperationStateControl {
    return new BanksOperationStateControl(viewName, getHeaderInfo, isPossibilityStatusChanging);
}

class BanksOperationStateControl extends Base.ComponentBase implements IEntityOperationStateControl {

    private _stateChangesEvent: Common.Event<any> = new Common.Event<any>();

    private _baId = null;
    private _model: Operations.IEntityStateModel;
    private _header: string;

    private _runBtn = ToolButtons.getRunBtn(`${this._viewName}-toolbar-operations`, "Выполнить операцию", (id, e) => { this.changeStatus() });
    private _historyBtn = ToolButtons.getHistoryBtn(`${this._viewName}-operation-toolbar`, "История", (id, e) => {
        this.showHistory();
    });

    get toolBarId() { return `${this._viewName}-toolbar-operations-id` }
    get headreLabelId() { return `${this._viewName}-header-label-id` }
    get stateLabelId() { return `${this._viewName}-state-label-id` }
    get selectId() { return `${this._viewName}-operation-select-id` }

    get baId(): number {
        return this._baId;
    }

    get stateChangesEvent() {
        return this._stateChangesEvent;
    }

    set baId(value: number) {
        this._baId = value;
        this.load();
    }

    load() {
        if (!this._baId) {
            this._model = null;
            this.bindData();
            return;
        }
        (webix.promise.all([Api.getApiOperations().getEntityState(this._baId), this._getHeaderInfo(this._baId)] as any) as any).then(result => {
            this._model = result[0];
            this._header = result[1];
            this.bindData();
        });

        //Api.getApiOperations().getEntityState(this._baId).then(result => {
        //    this._model = result;
        //    this.bindData();
        //});
    }

    constructor(private readonly _viewName: string,
        private readonly _getHeaderInfo: (baId: number) => Promise<string>,
        private readonly _isPossibilityStatusChanging: () => boolean) {
        super();
    }

    // baTypeName
    // Name + code?
    // label current state
    // select for allowed operations
    // run selected operation

    init() {
        return {
            view: "toolbar",
            id: this.toolBarId,
            height: 40,
            elements: [
                { view: "label", id: this.headreLabelId, inputWidth: 100 },
                { view: "label", id: this.stateLabelId, align: "center" },
                { view: "select", id: this.selectId ,options: [], width: 120 },
                this._runBtn.init(),
                this._historyBtn.init(), 
            ]
        }
    }

    ready() {
    }

    refresh() {
        ($$(this.headreLabelId) as webix.ui.label).setValue("");
        ($$(this.stateLabelId) as webix.ui.label).setValue("");
        ($$(this.selectId) as webix.ui.select).define("options", []);
        ($$(this.selectId) as webix.ui.select).refresh();
        this._runBtn.button.disable();
        this._historyBtn.button.disable();
    }

    private bindData() {
        

        if (!this._model) {
            ($$(this.headreLabelId) as webix.ui.label).setValue("");
            ($$(this.stateLabelId) as webix.ui.label).setValue("");
            ($$(this.selectId) as webix.ui.select).define("options", []);
            ($$(this.selectId) as webix.ui.select).refresh();
            this._runBtn.button.disable();    
            this._historyBtn.button.disable();
            return;
        }

        if (this.baId > 0) {
            this._historyBtn.button.enable();
        }

        ($$(this.headreLabelId) as webix.ui.label).setValue(`${this._header}`);

        if (this._model.isFirstState)
            ($$(this.stateLabelId) as webix.ui.label).setHTML(`<span class='his_state_base his_state_colour_blue'>${this._model.stateName}</span>`);
        else if (this._model.isDelState)
            ($$(this.stateLabelId) as webix.ui.label).setHTML(`<span class='his_state_base his_state_colour_red'>${this._model.stateName}</span>`);
        else 
            ($$(this.stateLabelId) as webix.ui.label).setHTML(`<span class='his_state_base his_state_colour_green'>${this._model.stateName}</span>`);

        const operationsList = (this._model.allowedOperations && this._model.allowedOperations.length > 0)
            ? this._model.allowedOperations.map(item => {
                return {
                    id: item.operationId,
                    value: item.operationName
                };
            })
            : [];

        ($$(this.selectId) as webix.ui.select).define("options", operationsList);
        ($$(this.selectId) as webix.ui.select).refresh();
        if (operationsList.length) {
            ($$(this.selectId) as webix.ui.select).setValue(operationsList[0].id.toString());
            this._runBtn.button.enable();
        }
    }

    private showHistory() {
        if (!this.baId) {
            return;
        }
        const dialog = History.getHistoryEntityDialog(`${this._viewName}-dialog`, null, this.baId);
        dialog.showModal(() => {
            dialog.close();
        });
    }

    private changeStatus() {
        if (!this._model)
            return;

        const operationId = ($$(this.selectId) as webix.ui.select).getValue();
        if (!operationId)
            return;

        if (!this._isPossibilityStatusChanging()) {
            return;
        }

        const operationName = this._model.allowedOperations.filter(item => {
            return item.operationId.toString() === operationId.toString();
        })[0].operationName;

        const self = this;
        webix.confirm({
            title: "Подтверждение",
            type: "confirm-warning",
            text: `Выполнить операцию "${operationName}"?`,
            ok:"Да", cancel: "Нет",
            callback(result) {
                if (result)
                    Api.getApiOperations().stateChange({ baId: self._baId, operationId: Number(operationId)}).then(data => {
                        self._model = data;
                        self.bindData();
                        self._stateChangesEvent.trigger(null);
                    })
            }
        });
    }
}
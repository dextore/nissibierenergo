﻿import Base = require("../ComponentBase");
import ToolButtons = require("../Buttons/ToolBarButtons");
import Api = require("../../Api/ApiExporter");
import Event = require("../../Common/Events");
import History = require("../../Components/History/HistoryEntityDialog");

export enum StateChangeAction {
    Changed,
    Loaded,
    Canceled
}

export interface IEntityOperationStateControl extends Base.IComponent  {
    baId: number;
    ready();
    addButtons(...args: ToolButtons.IToolBarButton[]): void;
    statusChangedEvent: Event.IEvent<StateChangeAction>;
    state: Operations.IEntityStateModel;
    hideHeade();
}

export interface IEntityOperationStateControlFilterOperation extends IEntityOperationStateControl {
    filterAllowOperation;
}

export function getEntityOperationStateControl(viewName: string,
    getHeaderInfo: ((baId: number) => Promise<string>) | string,
    isPossibilityStatusChanging: () => boolean): IEntityOperationStateControl {
    return new EntityOperationStateControl(viewName, getHeaderInfo, isPossibilityStatusChanging);
}

class EntityOperationStateControl extends Base.ComponentBase implements IEntityOperationStateControl, IEntityOperationStateControlFilterOperation {
    
    filterAllowOperation = (value) => true;

    private _baId = null;
    private _model: Operations.IEntityStateModel;
    private _header: string;

    //private _addNewBtn = ToolButtons.getNewBtn(`${this._viewName}-toolbar`, "Добавить", (id, e) => { this._addNew() });
    private _runBtn = ToolButtons.getRunBtn(`${this._viewName}-toolbar`, "Выполнить операцию", (id, e) => { this.changeStatus() });
    private _historyBtn = ToolButtons.getHistoryBtn(`${this._viewName}-operation-toolbar`, "История", (id, e) => {
        this.showHistory();
    });
    private _extButtons: ToolButtons.IToolBarButton[] = [];

    private _statusChangedEvent = new Event.Event<StateChangeAction>();

    get toolBarId() { return `${this._viewName}-toolbar-id` }
    get headreLabelId() { return `${this._viewName}-header-label-id` }
    get stateLabelId() { return `${this._viewName}-state-label-id` }
    get selectId() { return `${this._viewName}-operation-select-id` }

    get statusChangedEvent() {
        return this._statusChangedEvent;
    }

    get baId(): number {
        return this._baId;
    }

    set baId(value: number) {
        this._baId = value;
        this.load(this._baId);
    }

    get state() {
        return this._model;
    }

    load(baId: number) {
        const self = this;

        if (!baId) {
            this._model = null;
            this.bindData(this._model);
            checkStringHeader();
            return;
        }

        Api.getApiOperations().getEntityState(baId).then(result => {
            this._model = result;
            this.bindData(this._model);
            checkStringHeader();
            self.statusChangedEvent.trigger(StateChangeAction.Loaded, this);
        });

        function checkStringHeader() {
            if (self._getHeaderInfo && typeof self._getHeaderInfo === "string") {
                self._header = self._getHeaderInfo as any;
                ($$(self.headreLabelId) as webix.ui.label).setValue(self._header);
            }
        }
    }

    constructor(private readonly _viewName: string,
        private readonly _getHeaderInfo: ((baId: number) => Promise<string>) | string,
        private readonly _isPossibilityStatusChanging: () => boolean) {
        super();
    }

    hideHeade() {
        const label = $$(this.headreLabelId) as webix.ui.label;
        label.define(" width", 5);
        label.refresh();
    }

    addButtons(...args: ToolButtons.IToolBarButton[]): void {
        args.forEach(item => {
            this._extButtons.push(item);
        });
    }
        
    ready() {
        this._extButtons.forEach(item => {
            item.button.enable();
        });
    }

    private bindData(model: Operations.IEntityStateModel) {
        if (!model) {
            ($$(this.headreLabelId) as webix.ui.label).setValue("");
            ($$(this.stateLabelId) as webix.ui.label).setValue("");
            ($$(this.selectId) as webix.ui.select).define("options", []);
            ($$(this.selectId) as webix.ui.select).disable();
            ($$(this.selectId) as webix.ui.select).refresh();
            this._runBtn.button.disable();
            this._historyBtn.button.disable();

            return;
        }
        
        if (model.isFirstState)
            ($$(this.stateLabelId) as webix.ui.label).setHTML(`<span class='his_state_base his_state_colour_blue'>${model.stateName}</span>`);
        else if (model.isDelState)
            ($$(this.stateLabelId) as webix.ui.label).setHTML(`<span class='his_state_base his_state_colour_red'>${model.stateName}</span>`);
        else 
            ($$(this.stateLabelId) as webix.ui.label).setHTML(`<span class='his_state_base his_state_colour_green'>${model.stateName}</span>`);

        const operationsList = (model.allowedOperations && model.allowedOperations.length > 0)
            ? model.allowedOperations.map(item => {
                return {
                    id: item.operationId,
                    value: item.operationName
                };
            }).filter(this.filterAllowOperation)
            : [];

        ($$(this.selectId) as webix.ui.select).define("options", operationsList);
        ($$(this.selectId) as webix.ui.select).refresh();
        if (operationsList.length) {
            ($$(this.selectId) as webix.ui.select).setValue(operationsList[0].id.toString());
            this._runBtn.button.enable();
            ($$(this.selectId) as webix.ui.select).enable();
        }
        if (this.baId) {
            this._historyBtn.button.enable();
        }
    }

    private showHistory() {
        if (!this.baId) {
            return;
        }
        const dialog = History.getHistoryEntityDialog(`${this._viewName}-dialog`, null, this.baId);
        dialog.showModal(() => {
            dialog.close();
        });
    }
        
    private changeStatus() {
        if (!this._model)
            return;

        const operationId = ($$(this.selectId) as webix.ui.select).getValue();
        if (!operationId)
            return;

        if (!this._isPossibilityStatusChanging()) {
            return;
        }

        const operationName = this._model.allowedOperations.filter(item => {
            return item.operationId.toString() === operationId.toString();
        })[0].operationName;

        const self = this;
        webix.confirm({
            title: "Подтверждение",
            type: "confirm-warning",
            text: `Выполнить операцию "${operationName}"?`,
            ok:"Да", cancel: "Нет",
            callback(result) {
                if (result) {
                  
                    Api.getApiOperations().stateChange({ baId: self._baId, operationId: Number(operationId) }).then(
                        data => {
                            self._model = data;
                            self.bindData(self._model);
                            Number(operationId) === 7
                                ? self.statusChangedEvent.trigger(StateChangeAction.Canceled, this)
                                : self.statusChangedEvent.trigger(StateChangeAction.Changed, this);
                        });
                    
                }
            }
        });
    }

    // baTypeName
    // Name + code?
    // label current state
    // select for allowed operations
    // run selected operation

    init() {
        const config = {
            view: "toolbar",
            id: this.toolBarId,
            height: 40,
            elements: [
                { view: "label", id: this.headreLabelId },
                {},
                { view: "label", id: this.stateLabelId, width: 150, align: "center" },
                { view: "select", id: this.selectId, options: [], width: 120 },
                this._runBtn.init(),
                this._historyBtn.init(),
                //,
                //this._addNewBtn.init()
            ]
        };

        this._extButtons.forEach(item => {
            config.elements.push(item.init());
        });

        return config;
    }
}
﻿import Buttons = require("../Buttons/ToolBarButtons");

export function resetButtons(hasData: boolean, hasSelected: boolean, buttons: any): void {
    for (let value in buttons) {
        if (!buttons.hasOwnProperty(value))
            continue;

        const btn = buttons[value] as Buttons.IToolBarButton;
        if (!hasData) {
            btn.button.disable();
            continue;
        }

        if (btn.needSelected) {
            hasSelected ? btn.button.enable() : btn.button.disable();
            continue;
        }

        btn.button.enable();
    }
}

export function dataTableRows(dataTable: webix.ui.datatable): any[] {
    const result = []; 
    for (let value in dataTable.data.pull) {
        if (dataTable.data.pull.hasOwnProperty(value)) {
            result.push(dataTable.data.pull[value]);
        }
    }

    return result;
}
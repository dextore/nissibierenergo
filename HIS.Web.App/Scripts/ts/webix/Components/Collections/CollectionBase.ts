﻿import Base = require("../ComponentBase");
import Tab = require("../../MainView/Tabs/MainTabBase");
import Helper = require("../Helpers/DataTableHelpers");

export interface ICollectionBase extends Base.IComponent {
    viewId: string;
}

export abstract class CollectionBase extends Base.ComponentBase implements ICollectionBase {

    private _toolbarElements: {[id: string]: any} = {};

    protected get toolbarElements() {
        return this._toolbarElements;
    }

    protected get module(): Tab.IMainTab {
        return this._module;
    }

    protected get viewName(): string {
        return this._viewName;
    }

    get viewId(): string {
        return `${this.viewName}-view-id`;
    }

    protected get dataTableId(): string {
        return `${this.viewName}-datatable-id`;
    }

    protected get toolbarId(): string {
        return `${this.viewName}-toolbar-id`;
    }

    protected get dataTable(): webix.ui.datatable {
        return $$(this.dataTableId) as webix.ui.datatable;
    }


    protected constructor(private readonly _viewName: string, private readonly _module: Tab.IMainTab) {
        super();
    }

    init() {
        return {
            id: this.viewId,
            height: 300,
            rows: [
                this.initToolbar(),
                this.initDataTable()
            ],
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        }
    }

    protected get hasSelected() {
        return (($$(this.dataTableId) as webix.ui.datatable).getSelectedId(true, false) as any[]).length > 0;
    }

    protected selectionChanged() {
        Helper.resetButtons(true, this.hasSelected, this.toolbarElements);
    }

    protected isParamUndefined() {
        return true;
    }

    protected loadData() {
        this.dataTable.load(this.dataTable.config.url);
    }

    protected abstract getData(): Promise<any>;

    private initDataTable() {
        const config = {
            view: "datatable",
            id: this.dataTableId,
            select: "row",
            //scroll: "auto",
            columns: this.initColumns(),
            on: {
                onSelectChange: () => {
                    this.selectionChanged();
                }
            },
            url: {
                $proxy: true,
                load: (view, callback, params) => {
                    this.dataTable.clearAll();
                    if (this.isParamUndefined()) {
                        //(webix.ajax as any).$callback(view, callback, []);
                        Helper.resetButtons(false, false, this.toolbarElements);
                        return;
                    }
                    this.getData().then((data) => {
                        (webix.ajax as any).$callback(view, callback, data);
                        this.selectionChanged();
                    });
                }
            }
        }
        return webix.extend(config, this.internalInitDataTable());
    }

    protected internalInitDataTable() {
        return {};
    }

    private initToolbar() {
        const config = {
            view: "toolbar",
            id: this.toolbarId,
            height: 30,
            elements: this.initToolbarElements()
        }

        return webix.extend(config, this.internalInitToolbar());
    };

    protected internalInitToolbar() {
        return {};
    } 

    protected abstract initColumns(): any[];

    protected abstract initToolbarElements(): any[];
}
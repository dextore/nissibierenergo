﻿
export interface IDataTablePager {
    init();
}
export function getDataTablePager(viewName: string, size: number = 100, group: number = 5) {
    return new DataTablePager(viewName,size,group);
}
class DataTablePager implements IDataTablePager
{
    constructor(private readonly _viewName: string, private readonly _size:number , private readonly _group:number) {
    }
    init():object {
        return {
            view: "pager",
            id: this._viewName,
            template: "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            size: this._size,
            group: this._group
        };
    }

}
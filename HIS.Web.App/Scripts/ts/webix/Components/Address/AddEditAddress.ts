﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Common/DialogBase");
//import Common = require("../../Common/CommonExporter");
//import Interfaces = require("../../Abstractions/InterfacesExporter");
import AddEditAddressObject = require("./AddEditAddressObject");

export interface IAddEditAddress extends Base.IDialog {
    selected: ISearchOptionsModel;
}
export function getAddressDialog(viewName: string, iSearchOptionsModel?: object): IAddEditAddress {
    return new AddEditAddress(viewName, iSearchOptionsModel as ISearchOptionsModel);
};

enum AOLevel {
    RfSubject = 1,
    Region = 3,
    City = 4,
    Settlement = 6,
    ElmStructure = 6.5,
    Street = 7,
}
interface IAddrObjIdentifier {
    AOGUID: System.IGuid;
    AOID: System.IGuid;
    AOLevel?: AOLevel;
    sourceType?: number;
}
interface ISearchOptionsModel {
    isActual?: boolean;
    postCode?: string;
    cadNum?: string;
    searchValue?: any;
    skip?: number;
    take?: number;
    subjectRf?: IAddrObjIdentifier;
    raion?: IAddrObjIdentifier;
    city?: IAddrObjIdentifier;
    settlement?: IAddrObjIdentifier;
    elementStructure?: IAddrObjIdentifier;
    street?: IAddrObjIdentifier;
    houses?: IAddrObjIdentifier;
    stead?: IAddrObjIdentifier;
}

class AddEditAddress extends Base.DialogBase {
    private searchOptionsModel: ISearchOptionsModel;
    private addressUpdateResult: Address.IAddressUpdateResultModel;
    private isInitialize: boolean = false;
    public get selected(): ISearchOptionsModel { return this.searchOptionsModel; }
    ////
    private get formId() { return `${this.viewName}-form-id`; }
    protected get windowId() { return `${this.viewName}-window-id`; }
    protected get window() { return $$(this.windowId) as webix.ui.window; }
    private get subjectRfSuggestId() { return `${this.viewName}-suggest-subjectRf-id`; }
    private get searchsubjectRfId() { return `${this.viewName}-search-subjectRf-id`; }
    private get regionSuggestId() { return `${this.viewName}-suggest-region-id`; }
    private get searchRegionId() { return `${this.viewName}-search-region-id`; }
    private get citySuggestId() { return `${this.viewName}-suggest-city-id`; }
    private get searchCityId() { return `${this.viewName}-search-city-id`; }
    private get settlementSuggestId() { return `${this.viewName}-suggest-settlement-id`; }
    private get searchSettlementId() { return `${this.viewName}-search-settlement-id`; }
    private get elementStructureSuggestId() { return `${this.viewName}-suggest-element-structure-id`; }
    private get searchElementStructureId() { return `${this.viewName}-search-element-structure-id`; }
    private get streetSuggestId() { return `${this.viewName}-suggest-street-id`; }
    private get searchStreetId() { return `${this.viewName}-search-street-id`; }
    private get houseesSuggestId() { return `${this.viewName}-suggest-houses-id`; }
    private get searchHousesId() { return `${this.viewName}-search-houses-id`; }
    private get steadSuggestId() { return `${this.viewName}-suggest-stead-id`; }
    private get searchSteadId() { return `${this.viewName}-search-stead-id`; }
    private get protoUiSearchName() { return `${this.viewName}-search-protoUI`; }
    private get addEditDialogId() { return `${this.viewName}-add-edit-addressObject-id`; }
    private get cadNumerId() { return `${this.viewName}-cadnum-id`; }
    private get postCodeId() { return `${this.viewName}-postcode-id`; }
    private get isBuildId() { return `${this.viewName}-isbuild-id`; }
    private get addressNameId() { return `${this.viewName}-address-name-id`; }
    private get addressSuggestId() { return `${this.viewName}-address-suggest-id`; }
    private get protoUiTextName() { return `${this.viewName}-text-protoUI`; }


    constructor(viewName: string, iSearchOptionsModel: ISearchOptionsModel) {
        super(viewName);
        this.searchOptionsModel = {
            isActual: false,
            postCode: "",
            cadNum: "",
            searchValue: null,
            skip: 0,
            take: 100,
            subjectRf: null,
            raion: null,
            city: null,
            settlement: null,
            elementStructure: null,
            street: null,
            houses: null,
            stead: null
        }
        if (!this.isEmptyObject(iSearchOptionsModel)) {
            this.searchOptionsModel = {
                isActual: false,
                postCode: "",
                cadNum: "",
                searchValue: iSearchOptionsModel.searchValue,
                skip: iSearchOptionsModel.skip,
                take: iSearchOptionsModel.take,
                subjectRf: iSearchOptionsModel.subjectRf,
                raion: iSearchOptionsModel.raion,
                city: iSearchOptionsModel.city,
                settlement: iSearchOptionsModel.settlement,
                elementStructure: iSearchOptionsModel.elementStructure,
                street: iSearchOptionsModel.street,
                houses: iSearchOptionsModel.houses,
                stead: iSearchOptionsModel.stead
            }
            this.isInitialize = true;
        }
        
    }

    private initializeSearchModel()
    {

        const model = this.searchOptionsModel;
        let stateUpdate = 7;

        ($$(this.cadNumerId) as webix.ui.text).setValue(model.cadNum as string);
        ($$(this.postCodeId) as webix.ui.text).setValue(model.postCode as string);

        if (this.isEmptyObject(model.subjectRf)) {
            return;
        }
        this.refreshsubjectRf(model.subjectRf.AOGUID);

        (($$(this.regionSuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchRegionId) as webix.ui.search).setValue("");
        if (!this.isEmptyObject(model.raion)) {
            stateUpdate = 6;
            this.refreshRegion(model.raion.AOGUID);
        }
        (($$(this.citySuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchCityId) as webix.ui.search).setValue("");
        if (!this.isEmptyObject(model.city)) {
            stateUpdate = 5;
            this.refreshCity(model.city.AOGUID);
        }
        (($$(this.settlementSuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchSettlementId) as webix.ui.search).setValue("");
        if (!this.isEmptyObject(model.settlement)) {
            stateUpdate = 4;
            this.refreshSettlement(model.settlement.AOGUID);
        }
        (($$(this.elementStructureSuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchElementStructureId) as webix.ui.search).setValue("");
        if (!this.isEmptyObject(model.elementStructure)) {
            stateUpdate = 3;
            this.refreshElementStructure(model.elementStructure.AOGUID);
        }
        (($$(this.streetSuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchStreetId) as webix.ui.search).setValue("");
        if (!this.isEmptyObject(model.street)) {
            stateUpdate = 2;
            this.refreshStreet(model.street.AOGUID);
        }
        (($$(this.houseesSuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchHousesId) as webix.ui.search).setValue("");
        if (!this.isEmptyObject(model.houses)) {
            stateUpdate = 1;
            this.refreshHouse({
                AOGuid: model.houses.AOGUID,
                houseId: model.houses.AOID
            } as Address.IHouseRequestModel);
        }
        (($$(this.steadSuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchSteadId) as webix.ui.search).setValue("");
        if (!this.isEmptyObject(model.stead)) {
            stateUpdate = 0;
            this.refreshStead({
                AOGuid: model.stead.AOGUID,
                steadId: model.stead.AOID
            } as Address.ISteadRequestModel);

        }
        this.updateFieldsByState(stateUpdate);
    }
    private refreshFormByField(field, refreshModel: IAddrObjIdentifier): void {
        console.log("field for update", field);
        console.log("refresh model is .. ", refreshModel);
        let isUpdate: boolean = false;
        let stateUpdate:number = 0;
        let currentModel: IAddrObjIdentifier = this.getObjectByPropertyName(field);
        if (this.isEmptyObject(currentModel)) {
            isUpdate = true;
        } else if (currentModel.AOGUID !== refreshModel.AOGUID || currentModel.AOID !== refreshModel.AOID) {
            isUpdate = true;
        }
        let refreshPromise = webix.promise.defer();
        switch (field) {
            case "subjectRf":
                if (isUpdate) { stateUpdate = 7; }
                refreshPromise =  this.refreshsubjectRf(refreshModel.AOGUID);
                break;
            case "raion":
                if (isUpdate) { stateUpdate = 6; }
                refreshPromise = this.refreshRegion(refreshModel.AOGUID);
                break;
            case "city":
                if (isUpdate) { stateUpdate = 5; }
                refreshPromise = this.refreshCity(refreshModel.AOGUID);
                break;
            case "settlement":
                if (isUpdate) { stateUpdate = 4; }
                refreshPromise = this.refreshSettlement(refreshModel.AOGUID);
                break;
            case "elementStructure":
                if (isUpdate) { stateUpdate = 3; }
                refreshPromise = this.refreshElementStructure(refreshModel.AOGUID);
                break;
            case "street":
                if (isUpdate) { stateUpdate = 2; }
                refreshPromise = this.refreshStreet(refreshModel.AOGUID);
                break;
            case "houses":
                if (isUpdate) { stateUpdate = 1; }

                this.clearGuid("houses");
                this.updateSteadComponent(true);

                refreshPromise = this.refreshHouse({
                    AOGuid: refreshModel.AOGUID,
                    houseId: refreshModel.AOID
                } as Address.IHouseRequestModel);

                break;
            case "stead":
                this.clearGuid("houses");
                this.updateHousesComponent(true);

                refreshPromise = this.refreshStead({
                    AOGuid: refreshModel.AOGUID,
                    steadId: refreshModel.AOID
                } as Address.ISteadRequestModel);
                break;
        }

        refreshPromise.then((data) => {
            this.updateFieldsByState(stateUpdate);   
        }).catch((error) => {
            console.log(error);
        });

      
    }


    protected refreshsubjectRf(iAoGuid: System.IGuid): Promise<any> {

        const promise = webix.promise.defer();

        Api.getApiAddress().getAddressObjectCatalog({ AOGuid: iAoGuid }).then((data: Address.IAddressCatalog) => {
            this.searchOptionsModel.subjectRf = {
                AOGUID : data.AOGuid as System.IGuid,
                AOID : data.AOId as System.IGuid,
                AOLevel : AOLevel.RfSubject,
                sourceType : data.sourceType
            } as IAddrObjIdentifier;
            const suggest = $$(this.subjectRfSuggestId) as webix.ui.suggest;
            const search = $$(this.searchsubjectRfId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);

            (promise as any).resolve(true);

        }).catch(() => {
            (promise as any).reject();
            });
        return promise;
    }
    protected refreshRegion(iAoGuid: System.IGuid): Promise<any> {

        const promise = webix.promise.defer();

        Api.getApiAddress().getAddressObjectCatalog({ AOGuid: iAoGuid }).then((data: Address.IAddressCatalog) => {
            this.searchOptionsModel.raion = {
                AOGUID: data.AOGuid as System.IGuid,
                AOID: data.AOId as System.IGuid,
                AOLevel: AOLevel.Region,
                sourceType: data.sourceType
            } as IAddrObjIdentifier;
            const suggest = $$(this.regionSuggestId) as webix.ui.suggest;
            const search = $$(this.searchRegionId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);

            (promise as any).resolve(true);

        }).catch(() => {
            (promise as any).reject();
        });
        return promise;
    }
    protected refreshCity(iAoGuid: System.IGuid): Promise<any> {

        const promise = webix.promise.defer();
        Api.getApiAddress().getAddressObjectCatalog({ AOGuid: iAoGuid }).then((data: Address.IAddressCatalog) => {
            this.searchOptionsModel.city = {
                AOGUID: data.AOGuid as System.IGuid,
                AOID: data.AOId as System.IGuid,
                AOLevel: AOLevel.City,
                sourceType: data.sourceType
            } as IAddrObjIdentifier;

            const suggest = $$(this.citySuggestId) as webix.ui.suggest;
            const search = $$(this.searchCityId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);

            (promise as any).resolve(true);

        }).catch(() => {
            (promise as any).reject();
        });
        return promise;
    }
    protected refreshSettlement(iAoGuid: System.IGuid): Promise<any> {

        const promise = webix.promise.defer();
        Api.getApiAddress().getAddressObjectCatalog({ AOGuid: iAoGuid }).then((data: Address.IAddressCatalog) => {
            this.searchOptionsModel.settlement = {
                AOGUID: data.AOGuid as System.IGuid,
                AOID: data.AOId as System.IGuid,
                AOLevel: AOLevel.Settlement,
                sourceType: data.sourceType
            } as IAddrObjIdentifier;

            const suggest = $$(this.settlementSuggestId) as webix.ui.suggest;
            const search = $$(this.searchSettlementId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);
            (promise as any).resolve(true);

        }).catch(() => {
            (promise as any).reject();
        });
        return promise;
    }
    protected refreshElementStructure(iAoGuid: System.IGuid): Promise<any> {

        const promise = webix.promise.defer();
        Api.getApiAddress().getAddressObjectCatalog({ AOGuid: iAoGuid }).then((data: Address.IAddressCatalog) => {
            this.searchOptionsModel.elementStructure = {
                AOGUID: data.AOGuid as System.IGuid,
                AOID: data.AOId as System.IGuid,
                AOLevel: AOLevel.ElmStructure,
                sourceType: data.sourceType
            } as IAddrObjIdentifier;

            const suggest = $$(this.elementStructureSuggestId) as webix.ui.suggest;
            const search = $$(this.searchElementStructureId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);
            (promise as any).resolve(true);

        }).catch(() => {
            (promise as any).reject();
        });
        return promise;
    }
    protected refreshStreet(iAoGuid: System.IGuid): Promise<any> {
        const promise = webix.promise.defer();

        Api.getApiAddress().getAddressObjectCatalog({ AOGuid: iAoGuid }).then((data: Address.IAddressCatalog) => {
            this.searchOptionsModel.street = {
                AOGUID: data.AOGuid as System.IGuid,
                AOID: data.AOId as System.IGuid,
                AOLevel: AOLevel.Street,
                sourceType: data.sourceType
            } as IAddrObjIdentifier;

            const suggest = $$(this.streetSuggestId) as webix.ui.suggest;
            const search = $$(this.searchStreetId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);

            (promise as any).resolve(true);

        }).catch(() => {
            (promise as any).reject();
        });
        return promise;
    }
    protected refreshHouse(model: Address.IHouseRequestModel): Promise<any> {

        const promise = webix.promise.defer();
        let object = this.getNearObject();
        model.AOGuid = object.AOGUID;

        Api.getApiAddress().getHouse(model).then((data: Address.IAddressHouse) => {
            this.searchOptionsModel.houses = {
                AOGUID: data.houseGuid as System.IGuid,
                AOID: data.houseId as System.IGuid,
                sourceType: data.sourceType
            } as IAddrObjIdentifier;
            const suggest = $$(this.houseesSuggestId) as webix.ui.suggest;
            const search = $$(this.searchHousesId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);
            (promise as any).resolve(true);

        }).catch(() => {
            (promise as any).reject();
        });
        return promise;
    }
    protected refreshStead(model: Address.ISteadRequestModel): Promise<any> {

        const promise = webix.promise.defer();

        let object = this.getNearObject();
        model.AOGuid = object.AOGUID;

        Api.getApiAddress().getStead({ AOGuid: object.AOGUID, steadId: model.steadId } as Address.ISteadRequestModel).then((data: Address.IAddressStead) => {
            this.searchOptionsModel.stead = {
                AOGUID: data.steadGuid as System.IGuid,
                AOID: data.steadId as System.IGuid,
                sourceType: data.sourceType
            } as IAddrObjIdentifier;
            const suggest = $$(this.steadSuggestId) as webix.ui.suggest;
            const search = $$(this.searchSteadId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);
            (promise as any).resolve(true);

        }).catch(() => {
            (promise as any).reject();
        });
        return promise;
    }


    protected headerLabel(): string { return "Добавления/редактирования адреса "; }
    protected afterShowDialog() {

        this.window.define("width", 1100);
        this.window.define("height", 800);
        this.window.resize();
        if (this.isInitialize) {
            this.initializeSearchModel();
        }
    }
    protected contentConfig() {
        const self = this;
        webix.protoUI({
            name: self.protoUiSearchName,
            $cssName: "search protoUI-with-icons-4",
            $renderIcon: function () {
                const config = this.config;
                config.css = "padding - right: 52px;";

                if (config.icons.length) {
                    const height = config.aheight - 2 * config.inputPadding;
                    const padding = (height - 18) / 2 - 1;
                    let pos = 2;
                    let html = "";

                    for (var i = 0; i < config.icons.length; i++) {
                        html += "<span style='right:" + pos + "px;height:"
                            + (height - padding) + "px;padding-top:" + padding
                            + "px;' class='webix_input_icon fa-" + config.icons[i] + "'></span>";
                        pos += 24;
                    }
                    return html;
                }
                return "";
            },
            on_click: {
                "webix_input_icon": function (e, id, node) {
                    let name: string = node.className.substr(node.className.indexOf("fa-") + 3);
                    name = name.replace("-", "");
                    return this.callEvent("on" + name + "IconClick", [e]);
                }
            },
        }, webix.ui.search);

        const formElements: Array<object> = [{
            cols: [{
                rows: [{
                    view: "select", label: "Страна", name: "country",
                    disabled: false, value: 1,
                    options: [{ "id": 1, "value": "Российская Федерация" }]
                },
                self.getRfSubjectsComponent(self),
                self.getCityComponent(self),
                self.getElementStructureComponent(self),
                self.getStreetComponent(self),
                {
                    view: "text",
                    id: self.cadNumerId,
                    label: "Кадастровый номер",
                    type: "search",
                    readonly: true,
                    name: "сadNumber"

                }]
            },
            {
                rows: [{
                    view: "text",
                    id: self.postCodeId,
                    label: "Индекс",
                    name: "postalCode",
                    readonly:true
                },
                self.getRegionComponent(self),
                self.getSettlementComponent(self),
                self.getHousesComponent(self),
                self.getSteadComponent(self)
                ]
            }]
        }, {
                cols: [
                    {
                        template: "right",
                        rows: [
                            {
                                view: "label",
                                gravity: 1,
                                label: "Координаты: "
                            },
                          
                                    {
                                   
                                        view: "text",
                                        name: "Longitude",
                                        label: "долгота",
                                        readonly:true,
                                        width: 300
                                    },
                                    {  
                                        view: "text",
                                        name: "Latitude",
                                        label: "широта",
                                        placeholder: "",
                                        readonly: true,
                                        width: 300
                                    }

                           
                        ]

                    },
                    {
                        template: "right",
                        rows: [
                            {
                                id: self.isBuildId,
                                view: "checkbox",
                                label: "Строительный",
                                value: 0,
                                readonly: true,
                                disabled:true,
                            },
                            {
                                view: "radio",
                                readonly:true,
                                vertical: true,
                                disabled: true,
                                value: 0, options: [
                                    { id: 0, value: "Актуальный" },
                                    { id: 1, value: "Архивный" }
                                ]
                            }
                        ]

                    }
                ]
        }, {
            cols: [
                { width: 200 },
                {
                    rows: [{
                        id: self.addressNameId,
                        view: "text",
                        placeholder: "",
                        labelPosition: "top",
                        readonly: true,
                        suggest: {
                            id: self.addressSuggestId,
                            view: "gridsuggest",
                            width: 375,
                            textValue: "addressText",
                            body: {
                                yCount: 10,
                                scroll: true,
                                autoheight: false,
                                columns: [
                                    { id: "aoId", header: "id", hidden: true },
                                    { id: "addressText", header: "Адрес", width: 500 },
                                    { id: "houseId", header: "houseId", hidden: true },
                                    { id: "sourceType", header: "sourceType", hidden: true },
                                    { id: "steadId", header: "steadId", hidden: true }
                                ]
                            }
                        }
                    }]
                }, { width: 200 }]
        }];

        return {
            rows: [{
                id: self.formId,
                view: "form",
                elements: formElements,
                animate: false,
                borderless: true,
                elementsConfig: {
                    labelPosition: "left",
                    labelAlign: "left",
                    labelWidth: 130,
                    minWidth: 350,
                    animate: false
                }
            },
            self.getToolbar(self)
            ]
        };
    }

    private getNearGuid(aoId: boolean = false): System.IGuid {

        let result: IAddrObjIdentifier = null;
        const prohibitedValues = ["searchValue", "skip", "take", "postCode", "isActual", "cadNum"];

        for (const key in this.searchOptionsModel) {
            if (this.searchOptionsModel.hasOwnProperty(key)) {
                let value = this.searchOptionsModel[key];
                if (value === undefined) { value = null; }
                if (prohibitedValues.indexOf(key) < 0 && value != null) {
                    result = value as IAddrObjIdentifier;
                }
                if (key === "street") { break; }
            }
        }
        if (result === null) {
            return null;
        }
        if (aoId) {
            return result.AOID;
        }
        return result.AOGUID;
    }
    private getNearObject(): IAddrObjIdentifier {
        let result: IAddrObjIdentifier = null;

        // переписать нужно проверить на тип
        const prohibitedValues = ["searchValue", "skip", "take", "postCode", "isActual", "cadNum"];

        for (const key in this.searchOptionsModel) {
            if (this.searchOptionsModel.hasOwnProperty(key)) {
                let value = this.searchOptionsModel[key];
                if (value === undefined) { value = null; }
                if (prohibitedValues.indexOf(key) < 0 && value != null) {
                    result = value as IAddrObjIdentifier;
                }
                if (key === "street") { break; }
            }
        }
        return result;
    }
    private getObjectByPropertyName(selectedProperty: string): IAddrObjIdentifier {
        let result = {} as IAddrObjIdentifier;


        for (const key in this.searchOptionsModel) {
            if (this.searchOptionsModel.hasOwnProperty(key) && selectedProperty === key) {
                result = this.searchOptionsModel[key] as IAddrObjIdentifier;
                break;
            }
        }
        return result;
    }
    private clearGuid(nameGuid: string): void {

        let startDelete: boolean = false;

        for (const key in this.searchOptionsModel) {
            if (this.searchOptionsModel.hasOwnProperty(key)) {
                if (key === nameGuid) {
                    startDelete = true;
                }
                if (startDelete) {
                    this.searchOptionsModel[key] = null;
                }
            }
        }
    }
    private isEmptyObject(obj: object): boolean {

        if (obj == null || obj == undefined) {
            return true;
        }
        if (Object.keys(obj).length === 0) {
            return true;
        }
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }
    private getNearObjectByLevel(maxLevel: number): IAddrObjIdentifier {

        let result = {} as IAddrObjIdentifier;
        // переписать нужно проверить на тип
        const prohibitedValues = ["searchValue", "skip", "take", "postCode", "isActual", "cadNum"];

        for (const key in this.searchOptionsModel) {
            if (this.searchOptionsModel.hasOwnProperty(key)) {
                let value = this.searchOptionsModel[key] as IAddrObjIdentifier;
                if (value === undefined) { value = null; }
                if (prohibitedValues.indexOf(key) < 0 && value != null && value.AOLevel < maxLevel) {
                    result = value as IAddrObjIdentifier;
                }
                if (key === "street") { break; }
            }
        }
        return result;
    }
    private createAddressObject(field: string, aoLevel?: number) {
        const maxLevel = 100;
        let nearObject: IAddrObjIdentifier;
        let aoModel = {} as Address.IAddressObjectUpdateModel;
        let steadModel = {} as Address.ISteadUpdateModel;
        let houseModel = {} as Address.IHouseUpdateModel;

        switch (field) {
            case "stead":
                nearObject = this.getNearObjectByLevel(maxLevel);
                if (this.isEmptyObject(nearObject)) {
                    webix.message("Необходимо выбрать(создать) родительский элемент", "error");
                    return;
                }
                steadModel = {
                    parentId: nearObject.AOID,
                    parentGuid: nearObject.AOGUID,
                    steadId: null,
                    steadGuid: null,
                    number: null,
                    postalCode: "",
                    cadNum: ""
                } as Address.ISteadUpdateModel;

                break;
            case "houses":
                nearObject = this.getNearObjectByLevel(maxLevel);
                if (this.isEmptyObject(nearObject)) {
                    webix.message("Необходимо выбрать(создать) родительский элемент", "error");
                    return;
                }
                houseModel = {
                    AOId: nearObject.AOID,
                    parentGuid: nearObject.AOGUID,
                    houseId: null,
                    houseGuid: null,
                    buildNum: "",
                    estStateId: null,
                    houseNum: "",
                    strStateId: null,
                    structNum: "",
                    sourceType: nearObject.sourceType,
                    postalCode: "",
                    cadNum: "",
                    isBuild: true
                } as Address.IHouseUpdateModel;


                break;
            default:
                nearObject = this.getNearObjectByLevel(aoLevel);
                if (this.isEmptyObject(nearObject)&& field !== "subjectRf") {
                    webix.message("Необходимо выбрать(создать) родительский элемент", "error");
                    return;
                }
                aoModel = {
                    AOGuid: null,
                    AOId: null,
                    AOLevel: aoLevel,
                    parentId: nearObject.AOGUID,
                    postalCode:"",
                    formalName: "",
                    shortName: ""
                } as Address.IAddressObjectUpdateModel;
                break;
        }
        const addDialog = AddEditAddressObject.getAddEditAddressObjectDialog(this.addEditDialogId, aoModel, steadModel, houseModel);
        addDialog.showModal(() => {
            const houseResultModel = addDialog.houseResultModel;
            const steadResultModel = addDialog.steadResultModel;
            const aoResultModel    = addDialog.aoResultModel;
            
            if (!this.isEmptyObject(aoResultModel)) {
                this.refreshFormByField(field, {
                    AOGUID: addDialog.aoResultModel.AOGuid,
                    AOID: addDialog.aoResultModel.AOId
                } as IAddrObjIdentifier);
              
            } else if (!this.isEmptyObject(houseResultModel)) {
                this.refreshFormByField(field, {
                    AOGUID: houseResultModel.houseGuid,
                    AOID: houseResultModel.houseId
                } as IAddrObjIdentifier);
            } else if (!this.isEmptyObject(steadResultModel)) {
                this.refreshFormByField(field, {
                    AOGUID: steadResultModel.steadGuid,
                    AOID: steadResultModel.steadId
                } as IAddrObjIdentifier);
            }
            addDialog.close();
           
        });
    }
    private editAddressObject(field: string, aoLevel?: number) {
      
        const object = this.getObjectByPropertyName(field);
        if (this.isEmptyObject(object)) {
            webix.message("Необходимо выбрать элемент для редактирования", "error");
            return;
        }
        if (object.sourceType !== 2) {
            webix.message("Нельзя редактировать данные из ФИАС!", "error");
            return;
        }
       
        let aoModel = {} as Address.IAddressObjectUpdateModel;
        let steadModel = {} as Address.ISteadUpdateModel;
        let houseModel = {} as Address.IHouseUpdateModel;
        switch (field) {
            case "stead":              
                Api.getApiAddress().getSteadUpdateInfo(object.AOID).then((data: Address.ISteadUpdateModel) => {
                    steadModel = data;
                    const dialog = AddEditAddressObject.getAddEditAddressObjectDialog(this.addEditDialogId, aoModel, steadModel, houseModel);
                    dialog.showModal(() => {
                        this.searchOptionsModel.houses = null;
                        this.refreshFormByField(field, {
                            AOGUID: dialog.steadResultModel.steadGuid,
                            AOID: dialog.steadResultModel.steadId
                        } as IAddrObjIdentifier);
                        dialog.close();
                    }); 
                });
                break;
            case "houses":
                Api.getApiAddress().getHouseUpdateInfo(object.AOID).then((data: Address.IHouseUpdateModel) => {
                    houseModel = data;
                    houseModel.sourceType = object.sourceType;
                    const dialog = AddEditAddressObject.getAddEditAddressObjectDialog(this.addEditDialogId, aoModel, steadModel, houseModel);
                    dialog.showModal(() => {
                        this.searchOptionsModel.stead = null;
                        this.refreshFormByField(field, {
                            AOGUID: dialog.houseResultModel.houseGuid,
                            AOID: dialog.houseResultModel.houseId
                        } as IAddrObjIdentifier);
                        dialog.close();
                    });

                });

                break;
            default:
                const nearObject = this.getNearObjectByLevel(aoLevel);
                if (this.isEmptyObject(nearObject) && field !== "subjectRf") {
                    webix.message("Необходимо выбрать(создать) родительский элемент", "error");
                    return;
                }
                Api.getApiAddress().getAddressObject(object.AOID).then((data: Address.IAddressObjectModel) => {
                    aoModel = {
                        AOGuid: data.AOGuid,
                        AOId: data.AOId,
                        AOLevel: aoLevel,
                        parentId: nearObject.AOGUID,
                        formalName: data.formalName,
                        shortName: data.shortName,
                        postalCode: data.postCode
                    } as Address.IAddressObjectUpdateModel;
                    
                    const dialog = AddEditAddressObject.getAddEditAddressObjectDialog(this.addEditDialogId, aoModel, steadModel, houseModel);
                    dialog.showModal(() => {               
                        this.refreshFormByField(field, {
                            AOGUID: dialog.aoResultModel.AOGuid,
                            AOID: dialog.aoResultModel.AOId
                        } as IAddrObjIdentifier);
                        dialog.close();
                    });

                });
                break;
        }
    }
    private updateFieldsByState(state: number = 7, clearFollowing: boolean = false): void {

        if (state >= 7) {
            this.updateRegionComponent(clearFollowing);
        }
        if (state >= 6) {
            this.updateCityComponent(clearFollowing);
        }
        if (state >= 5) {
            this.updateSettlementComponent(clearFollowing);
        }
        if (state >= 4) {
            this.updateElementStructureComponent(clearFollowing);
        }
        if (state >= 3) {
            this.updateStreetComponent(clearFollowing);
        }
        if (state >= 2) {
            this.updateHousesComponent(clearFollowing);
        }
        if (state >= 1) {
            this.updateSteadComponent(clearFollowing);
        }
        this.showAddressSelected();

    }
    private showAddressSelected(isTableUpdate: boolean = false) {

        ($$(this.postCodeId) as webix.ui.text).setValue("");
        ($$(this.cadNumerId) as webix.ui.text).setValue("");
        ($$(this.isBuildId) as webix.ui.checkbox).setValue("0");

        this.updateSearchAddressField({} as Address.IAddresFullName);
        let setModel: Address.IAddresFullName;
        //очищаем таблицу
        const nearObject = this.getNearObject();
        if (this.isEmptyObject(nearObject)) {
            this.updateSearchAddressField({} as Address.IAddresFullName);
            return;
        }

        if (this.searchOptionsModel.houses != null) {

            setModel = {
                addressText: "",
                aoId: nearObject.AOID,
                houseId: this.searchOptionsModel.houses.AOID,
                sourceType: this.searchOptionsModel.houses.sourceType,
                steadId: null
            }
            Api.getApiAddress().getAddrFullByHouse(nearObject.AOID, this.searchOptionsModel.houses.AOID)
                .then((data: Address.IAddresFullName) => {
                    setModel.addressText = data.addressText;
                    this.updateSearchAddressField(setModel);

                }).catch(() => {
                    this.updateSearchAddressField(setModel);
                });
            if (this.searchOptionsModel.houses.sourceType === 2) {
                Api.getApiAddress().getHouseUpdateInfo(setModel.houseId).then((data: Address.IHouseUpdateModel) => {
                    ($$(this.postCodeId) as webix.ui.text).setValue(data.postalCode);
                    ($$(this.cadNumerId) as webix.ui.text).setValue(data.cadNum);
                    if (data.isBuild) {
                        ($$(this.isBuildId) as webix.ui.checkbox).setValue("1");
                    }
                });
            }
            return;
        }
        if (this.searchOptionsModel.stead != null) {
            setModel = {
                addressText: "",
                aoId: nearObject.AOID,
                houseId: null,
                sourceType: this.searchOptionsModel.stead.sourceType,
                steadId: this.searchOptionsModel.stead.AOID,
            }
            Api.getApiAddress().getAddrFullByStead(nearObject.AOID, this.searchOptionsModel.stead.AOID)
                .then((data: Address.IAddresFullName) => {
                    this.updateSearchAddressField(data);
                }).catch(() => {
                    this.updateSearchAddressField(setModel);
                });
            if (this.searchOptionsModel.stead.sourceType === 2) {
                Api.getApiAddress().getSteadUpdateInfo(setModel.steadId).then((data: Address.ISteadUpdateModel) => {
                    ($$(this.postCodeId) as webix.ui.text).setValue(data.postalCode);
                    ($$(this.cadNumerId) as webix.ui.text).setValue(data.cadNum);
                });
            }
            return;
        }

        setModel = {
            addressText: "",
            aoId: nearObject.AOID,
            houseId: null,
            sourceType: nearObject.sourceType,
            steadId: null
        }
        Api.getApiAddress().getAddrFullByAOID(nearObject.AOID)
            .then((data: Address.IAddresFullName) => {
                setModel.addressText = data.addressText;
                this.updateSearchAddressField(setModel);
            });

        if (nearObject.sourceType === 2) {

            Api.getApiAddress().getAddressObject(setModel.aoId).then((data: Address.IAddressObjectModel) => {
                ($$(this.postCodeId) as webix.ui.text).setValue(data.postCode);
            });
        }

    }
    private updateSearchAddressField(data: Address.IAddresFullName) {

        const suggest = $$(this.addressSuggestId) as webix.ui.gridsuggest;
        const text = $$(this.addressNameId) as webix.ui.text;
        const list = suggest.getList() as any;
        list.clearAll();
        const addressText = data.addressText;
        if (this.isEmptyObject(data)) {
            suggest.setValue("");
            text.setValue("");
            return;
        }
        list.parse(data, "json");
        text.setValue(addressText);
        suggest.setValue(addressText);
    }
    private getAddressRequestModel(aoguid: System.IGuid, iSearchValue?: string): Address.IAddressRequestModel {
        const model: Address.IAddressRequestModel = {
            AOGuid: aoguid,
            searchValue: iSearchValue,
            skip: this.searchOptionsModel.skip,
            take: this.searchOptionsModel.take,
            AOLevel: null,
            liveStatus: null
        };
        if (this.searchOptionsModel.isActual) {
            model.liveStatus = 1;
        }
        return model;
    }
    private updateRegionComponent(clearFollowing: boolean = false) {

        const suggest = $$(this.regionSuggestId) as webix.ui.suggest;
        const search = $$(this.searchRegionId) as webix.ui.search;
        const list = suggest.getList() as any;
         const selectValue = search.getValue();
        list.clearAll();
        if (this.searchOptionsModel.subjectRf == null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(this.searchOptionsModel.subjectRf.AOGUID, selectValue);
        Api.getApiAddress().getRegions(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });

    }
    private updateCityComponent(clearFollowing: boolean = false): void {

        const suggest = $$(this.citySuggestId) as webix.ui.suggest;
        const search = $$(this.searchCityId) as webix.ui.search;
        const list = suggest.getList() as any;
        const aoGuid = this.getNearGuid();
        const selectValue = search.getValue();
        list.clearAll();
        if (aoGuid == null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(aoGuid, selectValue);
        Api.getApiAddress().getCities(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });
    }
    private updateSettlementComponent(clearFollowing: boolean = false): void {

        const suggest = $$(this.settlementSuggestId) as webix.ui.suggest;
        const search = $$(this.searchSettlementId) as webix.ui.search;
        const list = suggest.getList() as any;
         const aoGuid = this.getNearGuid();
         const selectValue = search.getValue();
        list.clearAll();
        if (aoGuid == null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(aoGuid, selectValue);
        Api.getApiAddress().getSettlements(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });
    }
    private updateElementStructureComponent(clearFollowing: boolean = false): void {

        const suggest = $$(this.elementStructureSuggestId) as webix.ui.suggest;
        const search = $$(this.searchElementStructureId) as webix.ui.search;
        const list = suggest.getList() as any;
        const aoGuid = this.getNearGuid();
         const selectValue = search.getValue();
        list.clearAll();
        if (aoGuid == null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(aoGuid, selectValue);
        Api.getApiAddress().getElmStructures(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });
    }
    private updateStreetComponent(clearFollowing: boolean = false): void {

        const suggest = $$(this.streetSuggestId) as webix.ui.suggest;
        const search = $$(this.searchStreetId) as webix.ui.search;
        const list = suggest.getList() as any;
         const aoGuid = this.getNearGuid();
         const selectValue = search.getValue();
        list.clearAll();
        if (aoGuid == null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(aoGuid, selectValue);
        Api.getApiAddress().getStreets(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });
    }
    private updateHousesComponent(clearFollowing: boolean = false): void {
        const suggest = $$(this.houseesSuggestId) as webix.ui.suggest;
        const search = $$(this.searchHousesId) as webix.ui.search;
        const list = suggest.getList() as any;
         const aoGuid = this.getNearGuid();
         const selectValue = search.getValue();
        list.clearAll();
        if (aoGuid == null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(aoGuid, selectValue);
        Api.getApiAddress().getHouses(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });
    }
    private updateSteadComponent(clearFollowing: boolean = false): void {
        const suggest = $$(this.steadSuggestId) as webix.ui.suggest;
        const search = $$(this.searchSteadId) as webix.ui.search;
        const list = suggest.getList() as any;
         const aoGuid = this.getNearGuid();
         const selectValue = search.getValue();
        list.clearAll();
        if (aoGuid == null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(aoGuid, selectValue);
        Api.getApiAddress().getSteads(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });
    }
    ////компоненты формы 
    //// компоненты формы сложного поиска 
    private getRfSubjectsComponent(self): object {
        return {
            view: self.protoUiSearchName,
            icons: ["edit", "plus-circle", "close", "search"],
            name: "subjectRf", label: "Субъект РФ",
            id: self.searchsubjectRfId,
            placeholder: "Выберите субъект РФ...",
            on: {
                onSearchIconClick: () => {
                    const search = $$(self.searchsubjectRfId) as webix.ui.search;
                    const suggest = $$(self.subjectRfSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: () => {
                    self.clearGuid("subjectRf");
                    const suggest = $$(self.subjectRfSuggestId) as webix.ui.suggest;
                    const list = suggest.getList() as any;
                    const search = $$(self.searchsubjectRfId) as webix.ui.search;
                    suggest.setValue("");
                    search.setValue("");
                    Api.getApiAddress().getRFSubjects({
                        searchValue: "",
                        skip: 0,
                        take: 100,
                        AOLevel: null
                    }).then((data) => {
                        list.clearAll();
                        list.parse(data, "json");
                    });
                    self.updateFieldsByState(7);
                },
                onEditIconClick: () => {
                    self.editAddressObject("subjectRf", AOLevel.RfSubject);
                },
                onPlusCircleIconClick: () => {
                    self.createAddressObject("subjectRf", AOLevel.RfSubject);
                }
            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.subjectRfSuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "AOGuid", header: "AOGuid", hidden: true },
                        { id: "AOId", header: "AOId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Субъект РФ", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    url: {
                        $proxy: true,
                        load: () => {
                            Api.getApiAddress().getRFSubjects({
                                searchValue: "",
                                skip: 0,
                                take: 100,
                                AOLevel: null
                            }).then((data) => {
                                const suggest = $$(this.subjectRfSuggestId) as webix.ui.suggest;
                                const list = suggest.getList() as any;
                                list.clearAll();
                                list.parse(data, "json");
                            });
                        }
                    },
                    dataFeed: (filtervalue: string) => {
                        Api.getApiAddress().getRFSubjects({
                            searchValue: filtervalue,
                            skip: 0,
                            take: 100,
                            AOLevel: null
                        }).then((data) => {
                            const suggest = $$(self.subjectRfSuggestId) as webix.ui.suggest;
                            const list = suggest.getList() as any;
                            list.clearAll();
                            list.parse(data, "json");
                        });
                    },
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            ($$(this.searchsubjectRfId) as webix.ui.search).setValue(obj.name);
                            self.clearGuid("subjectRf");
                            self.searchOptionsModel.subjectRf = {
                                AOGUID: obj.AOGuid,
                                AOID: obj.AOId,
                                AOLevel: AOLevel.RfSubject,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.updateFieldsByState(7, true);
                        }
                    }
                }
            }

        };
    }
    private getRegionComponent(self): object {
        return {
            name: "region", label: "Район",
            view: self.protoUiSearchName,
            icons: ["edit", "plus-circle", "close", "search"],
            id: self.searchRegionId,
            placeholder: "Выберите район...",
            on: {
                onSearchIconClick: () => {
                    const search = $$(self.searchRegionId) as webix.ui.search;
                    const suggest = $$(self.regionSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: () => {
                    const search = $$(self.searchRegionId) as webix.ui.search;
                    search.setValue("");
                    self.clearGuid("raion");
                    self.updateFieldsByState(7, true);
                },
                onEditIconClick: () => {
                    self.editAddressObject("raion", AOLevel.Region);
                },
                onPlusCircleIconClick: () => {
                    self.createAddressObject("raion", AOLevel.Region);
                }
            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.regionSuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "AOGuid", header: "AOGuid", hidden: true },
                        { id: "AOId", header: "AOId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Район", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("raion");
                        self.updateFieldsByState(7);
                    },
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            ($$(self.searchRegionId) as webix.ui.search).setValue(obj.name);
                            self.clearGuid("raion");
                            self.searchOptionsModel.raion = {
                                AOGUID: obj.AOGuid,
                                AOID: obj.AOId,
                                AOLevel: AOLevel.Region,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.updateFieldsByState(6, true);
                        }

                    }
                }
            }

        };
    }
    private getCityComponent(self): object {
        return {

            view: self.protoUiSearchName,
            icons: ["edit", "plus-circle", "close", "search"],
            name: "city", label: "Город",
            id: self.searchCityId, placeholder: "Выберите город...",
            on: {
                onSearchIconClick: ()=> {
                    const search = $$(self.searchCityId) as webix.ui.search;
                    const suggest = $$(self.citySuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: () => {
                    const search: webix.ui.text = $$(self.searchCityId) as webix.ui.text;
                    search.setValue("");
                    self.clearGuid("city");
                    self.updateFieldsByState(6, true);
                },
                onEditIconClick: () => {
                    self.editAddressObject("city", AOLevel.City);
                },
                onPlusCircleIconClick: () => {
                    self.createAddressObject("city", AOLevel.City);
                }

            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.citySuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "AOGuid", header: "AOGuid", hidden: true },
                        { id: "AOId", header: "AOId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Город", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("city");
                        self.updateFieldsByState(6);
                    },
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            self.clearGuid("city");
                            ($$(self.searchCityId) as webix.ui.search).setValue(obj.name);
                            self.searchOptionsModel.city = {
                                AOGUID: obj.AOGuid,
                                AOID: obj.AOId,
                                AOLevel: AOLevel.City,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;

                            self.updateFieldsByState(5, true);
                        }
                    }
                }
            }

        };
    }
    private getSettlementComponent(self): object {
        return {

            view: self.protoUiSearchName,
            icons: ["edit", "plus-circle", "close", "search"], name: "settlement", label: "Населенный пункт",
            id: self.searchSettlementId, placeholder: "Выберите населенный пункт...",
            on: {
                onSearchIconClick: () => {
                    const search = $$(self.searchSettlementId) as webix.ui.search;
                    const suggest = $$(self.settlementSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: () => {
                    const search = $$(self.searchSettlementId) as webix.ui.search;
                    search.setValue("");
                    self.clearGuid("settlement");
                    self.updateFieldsByState(5, true);
                },
                onEditIconClick: () => {
                    self.editAddressObject("settlement", AOLevel.Settlement);
                },
                onPlusCircleIconClick: () => {
                    self.createAddressObject("settlement", AOLevel.Settlement);
                }

            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.settlementSuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "AOGuid", header: "AOGuid", hidden: true },
                        { id: "AOId", header: "AOId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Населенный пункт", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("settlement");
                        self.updateFieldsByState(5);
                    }
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            self.clearGuid("settlement");
                            ($$(self.searchSettlementId) as webix.ui.search).setValue(obj.name);
                            self.searchOptionsModel.settlement = {
                                AOGUID: obj.AOGuid,
                                AOID: obj.AOId,
                                AOLevel: AOLevel.Settlement,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.updateFieldsByState(4, true);
                        }
                    }
                }
            }


        };
    }
    private getElementStructureComponent(self): object {
        return {

            view: self.protoUiSearchName,
            icons: ["edit", "plus-circle", "close", "search"], name: "elmStructure", label: "Элемент планировочной структуры",
            placeholder: "Выберите элемент планировочной структуры...",
            id: self.searchElementStructureId,
            on: {
                onSearchIconClick: ()=> {
                    const search = $$(self.searchElementStructureId) as webix.ui.search;
                    const suggest = $$(self.elementStructureSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: () => {
                    const search = $$(self.searchElementStructureId) as webix.ui.search;
                    search.setValue("");
                    self.clearGuid("elementStructure");
                    self.updateFieldsByState(4, true);
                },
                onEditIconClick: () => {
                    self.editAddressObject("elementStructure", AOLevel.ElmStructure);
                },
                onPlusCircleIconClick: () => {
                    self.createAddressObject("elementStructure", AOLevel.ElmStructure);
                }
            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.elementStructureSuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "AOGuid", header: "AOGuid", hidden: true },
                        { id: "AOId", header: "AOId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Элемент планировочной структуры", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("elementStructure");
                        self.updateFieldsByState(4, true);
                    }
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            self.clearGuid("elementStructure");
                            ($$(self.searchElementStructureId) as webix.ui.search).setValue(obj.name);
                            self.searchOptionsModel.elementStructure = {
                                AOGUID: obj.AOGuid,
                                AOID: obj.AOId,
                                AOLevel: AOLevel.ElmStructure,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.updateFieldsByState(3);
                        }
                    }
                }
            }

        };
    }
    private getStreetComponent(self): object {
        return {

            view: self.protoUiSearchName,
            icons: ["edit", "plus-circle", "close", "search"], name: "street", label: "Улица",
            id: self.searchStreetId, placeholder: "Выберите улицу...",
            on: {
                onSearchIconClick: () => {
                    const search = $$(self.searchStreetId) as webix.ui.search;
                    const suggest = $$(self.streetSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: ()=> {
                    const search = $$(self.searchStreetId) as webix.ui.search;
                    search.setValue("");
                    self.clearGuid("street");
                    self.updateFieldsByState(3, true);
                },
                onEditIconClick: () => {
                    self.editAddressObject("street", AOLevel.Street);
                },
                onPlusCircleIconClick: () => {
                    self.createAddressObject("street", AOLevel.Street);
                }
            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.streetSuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "AOGuid", header: "AOGuid", hidden: true },
                        { id: "AOId", header: "AOId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Улица", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("street");
                        self.updateFieldsByState(3);
                    },
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            self.clearGuid("street");
                            ($$(self.searchStreetId) as webix.ui.search).setValue(obj.name);
                            self.searchOptionsModel.street = {
                                AOGUID: obj.AOGuid,
                                AOID: obj.AOId,
                                AOLevel: AOLevel.Street,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.updateFieldsByState(2, true);
                        }
                    }
                }
            }

        };
    }
    private getHousesComponent(self): object {
        return {

            view: self.protoUiSearchName,
            icons: ["edit", "plus-circle", "close", "search"], name: "houses", label: "Номер здания/сооружения/строения",
            placeholder: "Введите здания/сооружения/строения...",
            id: self.searchHousesId,
            on: {
                onSearchIconClick: () => {
                    const search = $$(self.searchHousesId) as webix.ui.search;
                    const suggest = $$(self.houseesSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: () => {
                    const search = $$(self.searchHousesId) as webix.ui.search;
                    search.setValue("");
                    self.clearGuid("houses");
                    self.updateFieldsByState(2, true);
                },
                onEditIconClick: () => {
                    self.editAddressObject("houses");
                },
                onPlusCircleIconClick: () => {
                    self.createAddressObject("houses");
                }
            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.houseesSuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "houseGuid", header: "houseGuid", hidden: true },
                        { id: "houseId", header: "houseId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Номер здания/сооружения/строения", width: 500 },
                        { id: "postalCode", header: "postalCode", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("houses");
                        self.updateFieldsByState(2);
                    }
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            self.clearGuid("houses");
                            ($$(self.searchHousesId) as webix.ui.search).setValue(obj.name);
                            self.searchOptionsModel.houses = {
                                AOGUID: obj.houseGuid,
                                AOID: obj.houseId,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.updateFieldsByState(1, true);
                        }
                    }
                }
            }

        };
    }
    private getSteadComponent(self): object {
        return {
            view: self.protoUiSearchName,
            icons: ["edit", "plus-circle", "close", "search"], name: "stead", label: "Номер земельного участка",
            id: self.searchSteadId, placeholder: "Введите номер земельного участка...",
            on: {
                onSearchIconClick: () => {
                    const search = $$(self.searchSteadId) as webix.ui.search;
                    const suggest = $$(self.steadSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: () => {
                    const search = $$(self.searchSteadId) as webix.ui.search;
                    search.setValue("");
                    self.clearGuid("stead");
                    self.updateFieldsByState(1, true);
                },
                onEditIconClick: () => {
                    self.editAddressObject("stead");
                },
                onPlusCircleIconClick: () => {
                    self.createAddressObject("stead");
                }
            },
            suggest: {
                view: "gridsuggest",
                id: self.steadSuggestId,
                width: 375,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "steadGuid", header: "steadGuid", hidden: true },
                        { id: "steadId", header: "steadId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Номер земельного участка", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("stead");
                        self.updateFieldsByState(1);
                    }
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            self.clearGuid("houses");
                            ($$(self.searchHousesId) as webix.ui.search).setValue("");
                            self.updateHousesComponent(true);
                            ($$(self.searchSteadId) as webix.ui.search).setValue(obj.name);
                            self.searchOptionsModel.stead = {
                                AOGUID: obj.steadGuid,
                                AOID: obj.steadId,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.showAddressSelected();
                        }
                    }
                }
            }

        };
    }
    private getToolbar(self: any): object {
        return {
            view: "toolbar",
            cols: [
                {},
                {
                    view: "button", label: "Принять", align: "right", width: 100,
                    toolbar: "bottom",
                    click: () => {
                        self.searchOptionsModel.cadNum = ($$(self.cadNumerId) as webix.ui.text).getValue();
                        self.searchOptionsModel.postCode = ($$(self.postCodeId) as webix.ui.text).getValue();
                        self.okClose();                     
                    }
                },{

                    view: "button", value: "Закрыть", align: "right", width: 80,
                    click: () => {
                        self.close();
                    }
                },
                { align: "right", width: 5 }

            ]
        }
    }
}
﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Common/DialogBase");
//import Common = require("../../Common/CommonExporter");
//import Interfaces = require("../../Abstractions/InterfacesExporter");


export interface IAddEditAddressObject extends Base.IDialog {
    aoResultModel: Address.IAddressObjectUpdateResultModel;
    steadResultModel: Address.ISteadUpdateResultModel;
    houseResultModel: Address.IHouseUpdateResultModel;
}
export function getAddEditAddressObjectDialog(viewName: string,
    iAoModel?: Address.IAddressObjectUpdateModel,
    iSteadModel?: Address.ISteadUpdateModel,
    iHouseModel?: Address.IHouseUpdateModel): IAddEditAddressObject {
    return new AddEditAddressObject(viewName, iAoModel, iSteadModel, iHouseModel);
};

class AddEditAddressObject extends Base.DialogBase {
    private aoModel = {} as Address.IAddressObjectUpdateModel;
    private steadModel = {} as Address.ISteadUpdateModel;
    private houseModel = {} as Address.IHouseUpdateModel;

    protected callbackAoResult: Address.IAddressObjectUpdateResultModel;
    protected callbackSteadResult: Address.ISteadUpdateResultModel;
    protected callbackHouseResult: Address.IHouseUpdateResultModel;

    public get aoResultModel(): Address.IAddressObjectUpdateResultModel { return this.callbackAoResult; }
    public get steadResultModel(): Address.ISteadUpdateResultModel { return this.callbackSteadResult; }
    public get houseResultModel(): Address.IHouseUpdateResultModel { return this.callbackHouseResult; }

    constructor(viewName: string, iAoModel?: Address.IAddressObjectUpdateModel,
        iSteadModel?: Address.ISteadUpdateModel,
        iHouseModel?: Address.IHouseUpdateModel) {

        super(viewName);
        this.aoModel = iAoModel;
        this.steadModel = iSteadModel;
        this.houseModel = iHouseModel;

    }
    protected afterShowDialog() {
        this.window.define("width",700);
        this.window.define("height", 600);
        this.window.resize();
    }

    private get aoFormId() { return `${this.viewName}-aoform-id`; }
    private get steadFormId() { return `${this.viewName}-steadform-id`; }
    private get houseFormId() { return `${this.viewName}-houseform-id`; }
    private get objectAoTypeId() { return `${this.viewName}-object-type-id`; }
    private get objectAoSuggestId() { return `${this.viewName}-object-suggest-id`; }
    private get comboStrStatId() { return `${this.viewName}-combo-strStat-id`; }
    private get suggeStrStatId() { return `${this.viewName}-suggest-strStat-id`; }
    private get comboEstStateId() { return `${this.viewName}-combo-estState-id`; }
    private get suggestEstStateId() { return `${this.viewName}-suggest-estState-id`; }
    private get protoUiSearchName() { return `${this.viewName}-search-protoUI`; }
    private get aoPostCodeId() { return `${this.viewName}-ao-postcode-id`; }
    private get housePostCodeId() { return `${this.viewName}-house-postcode-id`; }
    private get steadPostCodeId() { return `${this.viewName}-stead-postcode-id`; }
    private get houseCadNumId() { return `${this.viewName}-house-cadnum-id`; }
    private get steadCadNumId() { return `${this.viewName}-stead-cadnum-id`; }
    protected headerLabel(): string { return "Добавлениe/редактированиe адресас"; }
    private get protoUiTextName() { return `${this.viewName}-text-protoUI`; }
    private get houseHouseNumId() { return `${this.viewName}-house-houseNum-id`; }
    private get houseBuildNumId() { return `${this.viewName}-house-buildNum-id`; }
    private get houseStructNumId() { return `${this.viewName}-house-structNum-id`; }
    private get steadNumberId() { return `${this.viewName}-stead-number-id`; }
    private get aoFormalName() { return `${this.viewName}-ao-formalName-id`; }
    



    protected contentConfig() {
        const self = this;

        webix.protoUI({
            name: this.protoUiSearchName,
            $cssName: "search protoUI-with-icons-2",
            $renderIcon: function () {

                const config = this.config;
                config.css = "padding - right: 52px;";

                if (config.icons.length) {
                    const height = config.aheight - 2 * config.inputPadding;
                    const padding = (height - 18) / 2 - 1;
                    let pos = 2;
                    let html = "";
                    for (var i = 0; i < config.icons.length; i++) {
                        html += "<span style='right:" + pos + "px;height:"
                            + (height - padding) + "px;padding-top:" + padding
                            + "px;' class='webix_input_icon fa-" + config.icons[i] + "'></span>";
                        pos += 24;
                    }
                    return html;
                }
                return "";
            },
            on_click: {
                "webix_input_icon": function (e, id, node) {
                    let name: string = node.className.substr(node.className.indexOf("fa-") + 3);
                    name = name.replace("-", "");
                    return this.callEvent("on" + name + "IconClick", [e]);
                }
            },
        }, webix.ui.search);

        webix.protoUI({
            name: this.protoUiTextName,
            $cssName: "search protoUi-with-icons-1",
            $renderIcon: function () {
                const config = this.config;
                if (config.icons.length) {
                    const height = config.aheight - 2 * config.inputPadding;
                    const padding = (height - 18) / 2 - 1;
                    let pos = 2;
                    let html = "";

                    for (let i = 0; i < config.icons.length; i++) {
                        html += "<span style='right:" + pos + "px;height:"
                            + (height - padding) + "px;padding-top:" + padding
                            + "px;' class='webix_input_icon fa-" + config.icons[i] + "'></span>";
                        pos += 24;
                    }
                    return html;
                }
                return "";
            },
            on_click: {
                "webix_input_icon": function (e, id, node) {
                    const name = node.className.substr(node.className.indexOf("fa-") + 3);
                    return this.callEvent("on" + name + "IconClick", [e]);
                }
            },
        }, webix.ui.text);



        if (!this.isEmptyObject(this.aoModel)) {
            return this.getFormAo(self);
        } else if (!this.isEmptyObject(this.houseModel)) {
            return this.getHouseForm(self);
        } else if (!this.isEmptyObject(this.steadModel)) {
            return this.getSteadForm(self);
        }
        return {
            view: "label",
            label: "Ошибка входящих параметров",
            align: "center"
        };
    }


    private getHouseForm(self): object {

        return {

            rows: [{
                id: self.houseFormId,
                view: "form",   
                margin: 10,
                elements: [{
                    view: "fieldset",
                    label: "Здание",
                    body: {
                        rows: [
                            {
                                id: self.comboEstStateId,
                                name: "estStatId", label: "Тип здания", placeholder: "Выберире тип здания ...",
                                invalidMessage: "Необходимо выбрать тип здания",
                                view: self.protoUiSearchName,
                                icons: ["close", "search"],
                                on: {
                                    onSearchIconClick: () => {
                                        const search = $$(self.comboEstStateId) as webix.ui.search;
                                        const suggest = $$(self.suggestEstStateId) as webix.ui.suggest;
                                        suggest.show(search.getInputNode());
                                    },
                                    onCloseIconClick: () => {
                                        const suggest = $$(self.suggestEstStateId) as webix.ui.suggest;
                                        const search = $$(self.comboEstStateId) as webix.ui.search;
                                        suggest.setMasterValue("");
                                        search.setValue("");
                                        self.houseModel.estStateId = null;
                                    }

                                },
                                suggest: {
                                    view: "gridsuggest",
                                    width: 375,
                                    id: self.suggestEstStateId,
                                    body: {
                                        yCount: 10,
                                        scroll: true,
                                        autoheight: false,
                                        columns: [
                                            { id: "id", header: "estStatId", hidden: true },
                                            { id: "estStatId", header: "estStatId", hidden: true },
                                            { id: "dataVersionId", header: "dataVersionId", hidden: true },
                                            { id: "isDelete", header: "isDelete", hidden: true },
                                            { id: "name", header: "Тип сооружения", width: 380 }
                                        ]
                                    },
                                    url: {
                                        $proxy: true,
                                        load: () => {
                                            Api.getApiAddress().getEstateStatuses().then((data) => {
                                                const suggest = $$(self.suggestEstStateId) as webix.ui.suggest;
                                                const search = $$(self.comboEstStateId) as webix.ui.text;
                                                const list = suggest.getList() as any;
                                                list.clearAll();
                                                list.parse(self.modifyEstateStatusJson(data), "json");

                                                for (let counter: number = 0; counter < data.length; counter++) {
                                                    const currentObject: object = data[counter];
                                                    if (currentObject["estStatId"] === self.houseModel.estStateId) {
                                                        search.setValue(currentObject["name"]);
                                                        suggest.setValue(currentObject["name"]);
                                                        break;
                                                    }
                                                }
                                            });
                                        }
                                    },
                                    on: {
                                        onValueSuggest: (obj: Address.IAddressEstateStatus) => {
                                            self.houseModel.estStateId = obj.estStatId;
                                        }
                                    }
                                }
                            },
                            {
                                id: self.houseHouseNumId,
                                view: self.protoUiTextName,
                                icons: ["close"],
                                label: "Номер",
                                name: "houseNum",
                                value: self.houseModel.houseNum,
                                placeholder: "Введите номер ...",
                                invalidMessage: "Необходимо ввести номер",
                                on: {
                                    onCloseIconClick: () => {
                                        const search = $$(self.houseHouseNumId) as webix.ui.search;
                                        search.setValue("");
                                    }

                                }

                            },
                            {
                                id: self.houseBuildNumId,
                                view: self.protoUiTextName,
                                icons: ["close"],
                                label: "Корпус",
                                name: "buildNum",
                                value: self.houseModel.buildNum,
                                placeholder: "Введите корпус...",
                                invalidMessage: "Необходимо ввести корпус",
                                on: {
                                    onCloseIconClick: () => {
                                        const search = $$(self.houseBuildNumId) as webix.ui.search;
                                        search.setValue("");
                                    }

                                }

                            }
                        ]
                    }
                },{
                    view: "fieldset",
                    label: "Строение/сооружение",
                    body: {
                        rows: [
                            {
                                id: self.comboStrStatId,
                                name: "strStatId", label: "Тип сооружения", placeholder: "Выберире тип сооружения...",
                                invalidMessage: "Необходимо выюрать тип сооружения",
                                view: self.protoUiSearchName,
                                icons: ["close", "search"],
                                on: {
                                    onSearchIconClick: () => {
                                        const search = $$(self.comboStrStatId) as webix.ui.search;
                                        const suggest = $$(self.suggeStrStatId) as webix.ui.suggest;
                                        suggest.show(search.getInputNode());
                                    },
                                    onCloseIconClick: () => {
                                        const suggest = $$(self.suggeStrStatId) as webix.ui.suggest;
                                        const search = $$(self.comboStrStatId) as webix.ui.search;
                                        suggest.setMasterValue("");
                                        search.setValue("");
                                        self.houseModel.strStateId = null;
                                    }

                                },
                                suggest: {
                                    view: "gridsuggest",
                                    width: 375,
                                    id: self.suggeStrStatId,
                                    body: {
                                        yCount: 10,
                                        scroll: true,
                                        autoheight: false,
                                        columns: [
                                            { id: "id", header: "strStatId", hidden: true },
                                            { id: "strStatId", header: "strStatId", hidden: true },
                                            { id: "dataVersionId", header: "dataVersionId", hidden: true },
                                            { id: "shortName", header: "shortName", hidden: true },
                                            { id: "isDelete", header: "isDelete", hidden: true },
                                            { id: "name", header: "Тип сооружения", width: 380 }
                                        ]
                                    },
                                    url: {
                                        $proxy: true,
                                        load: () => {
                                            Api.getApiAddress().getStructureStatuses().then((data) => {
                                                const suggest = $$(self.suggeStrStatId) as webix.ui.suggest;
                                                const search = $$(self.comboStrStatId) as webix.ui.text;
                                                const list = suggest.getList() as any;
                                                list.clearAll();
                                                list.parse(self.modifyStructureStatusesJson(data), "json");
                                                for (let counter: number = 0; counter < data.length; counter++) {
                                                    const currentObject: object = data[counter];
                                                    if (currentObject["strStatId"] === self.houseModel.strStateId) {
                                                        search.setValue(currentObject["name"]);
                                                        suggest.setValue(currentObject["name"]);
                                                        break;
                                                    }
                                                }

                                            });
                                        }
                                    },
                                    on: {
                                        onValueSuggest: (obj: Address.IAddressStructureStatus) => {
                                            self.houseModel.strStateId = obj.strStatId;
                                        }
                                    }
                                }
                            }, {
                                id: self.houseStructNumId,
                                view: self.protoUiTextName,
                                icons: ["close"],
                                label: "Номер",
                                name: "structNum",
                                value: self.houseModel.structNum,
                                placeholder: "Введите номер сооружения...",
                                invalidMessage: "Необходимо ввести номер сооружения",
                                on: {
                                    onCloseIconClick: () => {
                                        const search = $$(self.houseStructNumId) as webix.ui.search;
                                        search.setValue("");
                                    }
                                }
                            }]
                    }
                }, {
                    view: "fieldset",
                    label: "Координаты",
                    body: {
                        rows: [{
                            view: "text",
                            name: "Longitude",
                            label: "долгота",
                            readonly: true,
                        }, {
                            view: "text",
                            name: "Latitude",
                            label: "широта",
                            placeholder: "",
                            readonly: true

                        }]
                    }
                }, {
               
                    id: self.housePostCodeId,
                    icons: ["close"],
                    view: self.protoUiTextName,
                    label: "Индекс",
                    name: "postalCode",
                    invalidMessage: "Неверно заполнен индекс",
                    pattern: { mask: "######", allow: /[0-9]/g },
                    value: self.houseModel.postalCode,
                    validate: (value) => {
                        if (value === "") {
                            return true;
                        }
                        return /^[0-9]{6}$/.test(value);
                    },
                    on: {
                        onCloseIconClick: () => {
                            const search = $$(self.housePostCodeId) as webix.ui.search;
                            search.setValue("");
                        },

                    },
                    placeholder: "Введите индекс..."
                }, {
                    view: self.protoUiTextName,
                    icons: ["close"],
                    id: self.houseCadNumId,
                    label: "Кадастровый номер",
                    value: self.houseModel.cadNum,
                    invalidMessage: "Неверно заполнен кадастровый номер",
                    pattern: { mask: "##:##:######:##", allow: /[0-9]/g },
                    validate: (value) => {
                        if (value === "") {
                            return true;
                        }
                        //return /^[0-9]{2}:[0-9]{2}:[0-9]{6}:[0-9]{2}$/.test(value);
                        return /^[0-9]{12}$/.test(value);
                    },
                    on: {
                        onCloseIconClick: () => {
                            const search = $$(self.houseCadNumId) as webix.ui.search;
                            search.setValue("");
                        },

                    },
                    name: "cadNumber",
                    placeholder: "Введите номер..."
                }, {
                    view: "checkbox",
                    label: "Строительный",
                    name: "isBuild",
                    value: self.houseModel.isBuild
                }],
                rules: {
                    "estStatId": () => {
                        return self.validateFormFields("estStatId");
                    },
                    "strStatId": () => {
                        return self.validateFormFields("strStatId");
                    },
                    "houseNum": () => {
                        return self.validateFormFields("houseNum");
                    },
                    "structNum": () => {
                        return self.validateFormFields("structNum");
                    },
                    "cadNumber": () => {
                        return self.validateFormFields("cadNumber");
                    },
                    "postalCode": () => {
                        return self.validateFormFields("postalCode");
                    }
                },
                animate: false,
                borderless: true,
                elementsConfig: {
                    labelPosition: "left",
                    labelAlign: "left",
                    labelWidth: 130,
                    minWidth: 350,
                    animate: false
                }
            }, { height:5},
            self.getToolbar(self)]
        };

    }
    private validateFormFields(field: string): boolean {
        let result: boolean = true;
        const form = $$(this.houseFormId) as webix.ui.form;
        const formValues = form.getValues();


        let estStatId = formValues["estStatId"] as string;
        let strStatId = formValues["strStatId"] as string;
        let houseNum = formValues["houseNum"] as string;
        let buildNum = formValues["buildNum"] as string;
        let structNum = formValues["structNum"] as string;

        estStatId = (estStatId === null || estStatId == undefined) ? "" : estStatId;
        strStatId = (strStatId === null || strStatId == undefined) ? "" : strStatId;
        houseNum = (houseNum === null || houseNum == undefined) ? "" : houseNum;
        buildNum = (buildNum === null || buildNum == undefined) ? "" : buildNum;
        structNum = (structNum === null || structNum == undefined) ? "" : structNum;



        switch (field) {
            case "estStatId":
                if (estStatId === "" && strStatId === "") {
                    result = false;
                } else if ((houseNum !== "" || buildNum !== "") && estStatId === "") {
                    result = false;
                }
                break;
            case "strStatId":
                if (estStatId === "" && strStatId === "") {
                    result = false;
                } else if (structNum !== "" && strStatId === "") {
                    result = false;
                }
                break;
            case "houseNum":
                result = !(houseNum === "" && estStatId !== "");
                break;
            case "structNum":
                result = !(structNum === "" && strStatId !== "");
                break;
            default:
                result = true;
                break;
        }

        return result;
    }
    private getSteadForm(self): object {
        return {
            rows: [{
                id: self.steadFormId,
                view: "form",
                elements: [
                    {
                        id: self.steadNumberId,
                        view: self.protoUiTextName,
                        icons: ["close"],
                        label: "Номер",
                        name: "number",
                        value: self.steadModel.number,
                        placeholder: "Введите номер земельнго участка...",
                        invalidMessage: "Необходимо ввести номер земельного участка",
                        on: {
                            onCloseIconClick: () => {
                                const search = $$(self.steadNumberId) as webix.ui.search;
                                search.setValue("");
                            }

                        }
                    },
                    {
                        view: self.protoUiTextName,
                        icons: ["close"],
                        id: self.steadPostCodeId,
                        label: "Индекс",
                        name: "postalCode",
                        type: "search",
                        pattern: { mask: "######", allow: /[0-9]/g },
                        value: self.steadModel.postalCode,
                        validate: (value) => {
                            if (value === "") {
                                return true;
                            }
                            return /^[0-9]{6}$/.test(value);
                        },
                        on: {
                            onCloseIconClick: () => {
                                const search = $$(self.steadPostCodeId) as webix.ui.search;
                                search.setValue("");
                            },

                        },
                        placeholder: "Введите индекс..."
                    },
                    {
                        view: self.protoUiTextName,
                        icons: ["close"],
                        id: self.steadCadNumId,
                        label: "Кадастровый номер",
                        value: self.steadModel.cadNum,
                        pattern: { mask: "##:##:######:##", allow: /[0-9]/g },
                        name: "cadNumber",
                        placeholder: "Введите номер...",
                        validate: (value) => {
                            if (value === "") {
                                return true;
                            }
                            return /^[0-9]{12}$/.test(value);
                        },
                        on: {
                            onCloseIconClick: () => {
                                const search = $$(self.steadCadNumId) as webix.ui.search;
                                search.setValue("");
                            },

                        }
                    },
                    {
                        view: "fieldset",
                        label: "Координаты",
                        body: {
                            rows: [
                                {

                                    view: "text",
                                    name: "Longitude",
                                    label: "долгота",
                                    readonly: true

                                },
                                {
                                    view: "text",
                                    name: "Latitude",
                                    label: "широта",
                                    placeholder: "",
                                    readonly: true

                                }


                            ]
                        }
                    }
                ],
                rules: {
                    "number": webix.rules.isNotEmpty,
                },
                animate: false,
                borderless: true,
                elementsConfig: {
                    labelPosition: "left",
                    labelAlign: "left",
                    labelWidth: 130,
                    minWidth: 350,
                    animate: false
                }
            },
            self.getToolbar(self)]
        };
    }
    private getFormAo(self): object {
        return {
            rows: [{
                id: self.aoFormId,
                view: "form",
                elements: [
                    {
                        id: self.aoFormalName,
                        view: self.protoUiTextName,
                        icons: ["close"],
                        label: "Наименование",
                        name: "formalName",
                        placeholder: "Введите наименование...",
                        invalidMessage: "Необходимо ввести наименование",
                        value: self.aoModel.formalName,
                        on: {
                            onCloseIconClick: () => {
                                const search = $$(self.aoFormalName) as webix.ui.search;
                                search.setValue("");
                            },

                        }
                    }, {
                        id: self.objectAoTypeId, name: "shortName", label: "Тип", placeholder: "Выберире тип...",
                        view: "combo", invalidMessage: "Необходимо заполнить тип",
                        suggest: {
                            view: "gridsuggest",
                            width: 400,
                            id: self.objectAoSuggestId,
                            body: {
                                yCount: 10,
                                scroll: true,
                                autoheight: false,
                                columns: [
                                    { id: "id", header: "id", hidden: true },
                                    { id: "socrName", header: "Тип", width: 290 },
                                    { id: "SCname", header: "Сокр. наим.", width: 100 }
                                ]
                            },
                            url: {
                                $proxy: true,
                                load: () => {
                                    Api.getApiAddress().getAddressObjectTypes(self.aoModel.AOLevel).then((data) => {
                                        const suggest = $$(self.objectAoSuggestId) as webix.ui.suggest;
                                        const search = $$(self.objectAoTypeId) as webix.ui.text;
                                        const list = suggest.getList() as any;
                                        list.clearAll();
                                        list.parse(self.modifyObjectTypesJson(data), "json");
                                        search.setValue(self.aoModel.shortName);
                                        suggest.setValue(self.aoModel.shortName);
                                    });
                                }
                            },
                            on: {
                                onValueSuggest: (obj: Address.IAddressObjectTypeModel) => {
                                    self.aoModel.shortName = obj.SCname;
                                }
                            }
                        }
                    },
                    {
                        view: self.protoUiTextName,
                        icons: ["close"],
                        id: self.aoPostCodeId,
                        label: "Индекс",
                        type: "search",
                        name: "postalCode",
                        value: self.aoModel.postalCode,
                        validate: (value) => {
                            if (value === "") {
                                return true;
                            }
                            return /^[0-9]{6}$/.test(value);
                        },
                        on: {
                            onCloseIconClick: () => {
                                const search = $$(self.aoPostCodeId) as webix.ui.search;
                                search.setValue("");
                            },

                        },
                        placeholder: "Введите индекс...",
                        pattern: { mask: "######", allow: /[0-9]/g }
                    }
                ],
                rules: {
                    "formalName": webix.rules.isNotEmpty,
                    "shortName": webix.rules.isNotEmpty
                },
                animate: false,
                borderless: true,
                elementsConfig: {
                    labelPosition: "left",
                    labelAlign: "left",
                    labelWidth: 130,
                    minWidth: 350,
                    animate: false
                }
            },
            self.getToolbar(self)]
        };
    }
    private isEmptyObject(obj: object): boolean {

        if (obj == null || obj == undefined) {
            return true;
        }
        if (Object.keys(obj).length === 0) {
            return true;
        }
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }

    private modifyEstateStatusJson(inObject: Array<object>): Array<object> {
        const outArray: Array<object> = [];
        for (let counter: number = 0; counter < inObject.length; counter++) {
            const currentObject = inObject[counter];
            currentObject["id"] = currentObject["estStatId"];
            outArray.push(currentObject as any);
        }
        return outArray;
    }

    private modifyStructureStatusesJson(inObject: Array<object>): Array<object> {
        const outArray: Array<object> = [];
        for (let counter: number = 0; counter < inObject.length; counter++) {
            const currentObject = inObject[counter];
            currentObject["id"] = currentObject["strStatId"];
            outArray.push((currentObject) as any);
        }
        return outArray;
    }

    private modifyObjectTypesJson(inObject: Array<object>): Array<object> {

        const outArray: Array<object> = [];
        for (let counter: number = 0; counter < inObject.length; counter++) {
            const currentObject = inObject[counter];
            currentObject["id"] = currentObject["SCname"];
            currentObject["shortTitle"] = currentObject["SCname"];
            outArray.push(currentObject as any);
        }
        return outArray;
    }


    private getToolbar(self: any): object {
        return {
            view: "toolbar",
            paddingY: 10,
            cols: [
                {},
                {
                    view: "button", label: "Принять", align: "right", width: 100,
                    toolbar: "bottom",
                    click: () => {
                        let form: webix.ui.form = null;
                        if (!self.isEmptyObject(self.aoModel)) {
                            form = $$(self.aoFormId) as webix.ui.form;
                            if (!form.validate()) {
                                return;
                            }
                            let formValues = form.getValues();
                            self.aoModel.formalName = formValues["formalName"];
                            self.aoModel.postalCode = formValues["postalCode"];
                            console.log(self.aoModel);

                            Api.getApiAddress().addressObjectUpdate(self.aoModel).then((data: Address.IAddressObjectUpdateResultModel) => {
                                if (data.isError) {
                                    webix.message(data.errorDescription, "error");
                                } else {
                                    self.callbackAoResult = data;
                                    self.okClose();
                                }

                            });

                        } else if (!this.isEmptyObject(self.houseModel)) {
                            form = $$(self.houseFormId) as webix.ui.form;
                            if (!form.validate()) {
                                return;
                            }
                            let formValues = form.getValues();
                            self.houseModel.houseNum = formValues["houseNum"];
                            self.houseModel.buildNum = formValues["buildNum"];
                            self.houseModel.structNum = formValues["structNum"];
                            self.houseModel.postalCode = formValues["postalCode"];
                            let cadNumber: string = "";
                            if (formValues["cadNumber"] !== "") {
                                cadNumber = formValues["cadNumber"].substring(0, 2) + ":" +
                                    formValues["cadNumber"].substring(2, 4) + ":" +
                                    formValues["cadNumber"].substring(4, 10) + ":" +
                                    formValues["cadNumber"].substring(10, 12);
                            }
                            self.houseModel.cadNum = cadNumber;
                            self.houseModel.isBuild = formValues["isBuild"];

                            Api.getApiAddress().houseUpdate(self.houseModel).then((data: Address.IHouseUpdateResultModel) => {

                                if (data.isError) {
                                    webix.message(data.errorDescription, "error");
                                } else {
                                    self.callbackHouseResult = data;
                                    self.okClose();
                                }
                            });

                        } else if (!this.isEmptyObject(self.steadModel)) {

                            form = $$(self.steadFormId) as webix.ui.form;
                            if (!form.validate()) {
                                return;
                            }
                            let formValues = form.getValues();
                            self.steadModel.number = formValues["number"];
                            self.steadModel.postalCode = formValues["postalCode"];
                            let cadNumber: string = "";
                            if (formValues["cadNumber"] !== "") {
                                cadNumber = formValues["cadNumber"].substring(0, 2) + ":" +
                                    formValues["cadNumber"].substring(2, 4) + ":" +
                                    formValues["cadNumber"].substring(4, 10) + ":" +
                                    formValues["cadNumber"].substring(10, 12);
                            }
                            self.steadModel.cadNum = cadNumber;
                            Api.getApiAddress().steadUpdate(self.steadModel).then((data: Address.ISteadUpdateResultModel) => {

                                if (data.isError) {
                                    webix.message(data.errorDescription, "error");
                                } else {
                                    self.callbackSteadResult = data;
                                    self.okClose();
                                }


                            });
                        }
                    }
                },
                {
                    view: "button", value: "Закрыть", align: "right", width: 80,
                    click: () => {
                        self.close();
                    }
                },
                { align: "right", width: 5 }
            ]
        }
    }
}

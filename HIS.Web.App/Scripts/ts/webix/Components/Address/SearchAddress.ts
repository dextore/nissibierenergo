﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Common/DialogBase");
import AddEditAddress = require("./AddEditAddress");
import Pager = require("../Pager/Pager");

export interface ISearchAddress extends Base.IDialog {
    selected: Address.IAddressUpdateResultModel;
}

export function getSearchAddress(viewName: string, isAdvanced: boolean = false): ISearchAddress {
    return new SearchAddress(viewName, isAdvanced);
};

enum AOLevel {
    RfSubject = 1,
    Region = 3,
    City = 4,
    Settlement = 6,
    ElmStructure = 6.5,
    Street = 7,
}
interface IAddrObjIdentifier {
    AOGUID: System.IGuid;
    AOID: System.IGuid;
    AOLevel?: AOLevel;
    sourceType?: number;
}
interface ISearchOptionsModel {
    isActual?: boolean;
    postCode?: string;
    cadNum?: string;
    searchValue?: any;
    skip?: number;
    take?: number;
    subjectRf?: IAddrObjIdentifier;
    raion?: IAddrObjIdentifier;
    city?: IAddrObjIdentifier;
    settlement?: IAddrObjIdentifier;
    elementStructure?: IAddrObjIdentifier;
    street?: IAddrObjIdentifier;
    houses?: IAddrObjIdentifier;
    stead?: IAddrObjIdentifier;
}

class SearchAddress extends Base.DialogBase {

    private addressUpdate: Address.IAddressUpdateModel;
    private addressFullName = {} as Address.IAddresFullName;
    protected resultAddress: Address.IAddressUpdateResultModel;
    private searchOptionsModel = {} as ISearchOptionsModel;

    private getNearObject(): IAddrObjIdentifier {
        let result: IAddrObjIdentifier = null;

        // переписать нужно проверить на тип
        const prohibitedValues = ["searchValue", "skip", "take", "postCode", "isActual", "cadNum"];

        for (const key in this.searchOptionsModel) {
            if (this.searchOptionsModel.hasOwnProperty(key)) {
                let value = this.searchOptionsModel[key];
                if (value === undefined) { value = null; }
                if (prohibitedValues.indexOf(key) < 0 && value != null) {
                    result = value as IAddrObjIdentifier;
                }
                if (key === "street") { break; }
            }
        }
        return result;
    }


    private getNearGuid(aoId: boolean = false): System.IGuid {
        let result: IAddrObjIdentifier = null;
        // переписать нужно проверить на тип
        const prohibitedValues =  ["searchValue", "skip", "take", "postCode", "isActual", "cadNum"];
        
        for (const key in this.searchOptionsModel) {
             if (this.searchOptionsModel.hasOwnProperty(key)) {
                let value = this.searchOptionsModel[key];
                if (value === undefined) { value = null; }
                if (prohibitedValues.indexOf(key) < 0 && value != null) {
                    result = value as IAddrObjIdentifier;
                }
                if (key === "street") { break; }
            }
        }
        if (result === null) {
            return null;
        }
        if (aoId) {
            return result.AOID;
        }
        return result.AOGUID;
    }
    private clearGuid(nameGuid: string): void {

        let startDelete: boolean = false;

        for (const key in this.searchOptionsModel) {
            if (this.searchOptionsModel.hasOwnProperty(key)) {
                if (key === nameGuid) {
                    startDelete = true;
                }
                if (startDelete) {
                    this.searchOptionsModel[key] = null;
                }
            }
        }
    }
    private isEmptyObject(obj: object): boolean {

        if (obj == null || obj == undefined) {
            return true;
        }
        if (Object.keys(obj).length === 0) {
            return true;
        }
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }
    private initializeAddresUpdate(): void {
        this.addressUpdate = {
            baId: null,
            name: null,
            isBuild: false,
            isActual: true,
        } as Address.IAddressUpdateModel;
    }
    constructor(viewName: string, private isAdvanced: boolean = false) {

        super(viewName);
        if (this.isAdvanced === null || this.isAdvanced === undefined) {
            this.isAdvanced = false;
        }
        this.initializeAddresUpdate();
        this.searchOptionsModel = {
            isActual: false,
            postCode: "",
            searchValue: null,
            skip: 0,
            take: 100,
            subjectRf: null,
            raion: null,
            city: null,
            settlement: null,
            elementStructure: null,
            street: null,
            houses: null,
            stead: null
        } as ISearchOptionsModel;
    }
    protected headerLabel(): string { return "Форма поиска адреса "; }
    public get selected(): Address.IAddressUpdateResultModel { return this.resultAddress; }
    protected get windowId() { return `${this.viewName}-window-id`; }
    protected get window() { return $$(this.windowId) as webix.ui.window; }
    private get simpleFormId() { return `${this.viewName}-simple-form-id`; }
    private get advancedFormId() { return `${this.viewName}-advanced-form-id`; }
    private get addressGridSuggestId() { return `${this.viewName}-suggest-address-id`; }
    private get addressSearchId() { return `${this.viewName}-search-address-id`; }
    ////
    private get searchAddressNameId() { return `${this.viewName}-search-address-name-id`; }
    private get searchAddressSuggestId() { return `${this.viewName}-search-address-suggest-id`; }
    private get selectedAddressNameId() { return `${this.viewName}-selected-address-name-id`; }
    private get selectedAddressSuggestId() { return `${this.viewName}-selected-address-suggest-id`; }
    ////
    private get flatTypeId() { return `${this.viewName}-flatType-id`; }
    private get flatTypeAdvancedId() { return `${this.viewName}-advanced-flatType-id`; }
    private get flatTypeSuggestId() { return `${this.viewName}-suggest-flatType-id`; }
    private get flatNumSimpleId() { return `${this.viewName}-simple-flatNum-id`; }
    private get flatNumAdvancedId() { return `${this.viewName}-advanced-flatNum-id`; }
    private get locationSimpleId() { return `${this.viewName}-simple-location-id`; }
    private get locationAdvancedId() { return `${this.viewName}-advanced-location-id`; }
    private get POBoxSimpleId() { return `${this.viewName}-simple-POBox-id`; }
    private get POBoxAdvancedId() { return `${this.viewName}-advanced-POBox-id`; }
    private get flatTypeAdvanceSuggestId() { return `${this.viewName}-suggest-advance-flatType-id`; }
    private get subjectRfSuggestId() { return `${this.viewName}-suggest-subjectRf-id`; }
    private get searchsubjectRfId() { return `${this.viewName}-search-subjectRf-id`; }
    private get regionSuggestId() { return `${this.viewName}-suggest-region-id`; }
    private get searchRegionId() { return `${this.viewName}-search-region-id`; }
    private get citySuggestId() { return `${this.viewName}-suggest-city-id`; }
    private get searchCityId() { return `${this.viewName}-search-city-id`; }
    private get settlementSuggestId() { return `${this.viewName}-suggest-settlement-id`; }
    private get searchSettlementId() { return `${this.viewName}-search-settlement-id`; }
    private get elementStructureSuggestId() { return `${this.viewName}-suggest-element-structure-id`; }
    private get searchElementStructureId() { return `${this.viewName}-search-element-structure-id`; }
    private get streetSuggestId() { return `${this.viewName}-suggest-street-id`; }
    private get searchStreetId() { return `${this.viewName}-search-street-id`; }
    private get houseesSuggestId() { return `${this.viewName}-suggest-houses-id`; }
    private get searchHousesId() { return `${this.viewName}-search-houses-id`; }
    private get steadSuggestId() { return `${this.viewName}-suggest-stead-id`; }
    private get searchSteadId() { return `${this.viewName}-search-stead-id`; }

    private get resultDataTableId() { return `${this.viewName}-result-datatable-id`; }
    private get postCodeId() { return `${this.viewName}-postcode-id`; }
    private get cadNumerId() { return `${this.viewName}-cadnum-id`; }
    private get pagerId() { return `${this.viewName}-result-pager-id`; }
    private get simpleAoGuidLabelId() { return `${this.viewName}-simple-auguid-simple-id`; }
    private get advancedAoGuidLabelId() { return `${this.viewName}-simple-auguid-advanced-id`; }
    private get searchButtonId() { return `${this.viewName}-search-button-id`; }
    private get protoUiSearchName() { return `${this.viewName}-search-protoUI`; }
    private get protoUiTextName() { return `${this.viewName}-text-protoUI`; }
    private get simpleLabelId() { return `${this.viewName}-simple-label`; }
    private get advancedLabelId() { return `${this.viewName}-advanced-label`; }
    private get buttonAddId() { return `${this.viewName}-add-button`; }
    private get addEditDialogId() { return `${this.viewName}-add-edit-address`; }
    private initializeSearchModel(): void {

        let model = this.searchOptionsModel;
        let stateUpdate = 7;
        
        ($$(this.cadNumerId) as webix.ui.text).setValue(model.cadNum as string);
        ($$(this.postCodeId) as webix.ui.text).setValue(model.postCode as string);

        if (this.isEmptyObject(model.subjectRf)) {
            return;
        }
        this.refreshsubjectRf(model.subjectRf.AOGUID);

        (($$(this.regionSuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchRegionId) as webix.ui.search).setValue("");
        if (this.isEmptyObject(model.raion) === false) {
            stateUpdate = 6;
            this.refreshRegion(model.raion.AOGUID);
        }
        (($$(this.citySuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchCityId) as webix.ui.search).setValue("");
        if (this.isEmptyObject(model.city) === false) {
            stateUpdate = 5;
            this.refreshCity(model.city.AOGUID);
        }
        (($$(this.settlementSuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchSettlementId) as webix.ui.search).setValue("");
        if (this.isEmptyObject(model.settlement) === false) {
            stateUpdate = 4;
            this.refreshSettlement(model.settlement.AOGUID);
        }
        (($$(this.elementStructureSuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchElementStructureId) as webix.ui.search).setValue("");
        if (this.isEmptyObject(model.elementStructure) === false) {
            stateUpdate = 3;
            this.refreshElementStructure(model.elementStructure.AOGUID);
        }
        (($$(this.streetSuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchStreetId) as webix.ui.search).setValue("");
        if (this.isEmptyObject(model.street) === false) {
            stateUpdate = 2;
            this.refreshStreet(model.street.AOGUID);
        }
        (($$(this.houseesSuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchHousesId) as webix.ui.search).setValue("");
        if (this.isEmptyObject(model.houses) === false) {
            stateUpdate = 1;
            this.refreshHouse({
                AOGuid: model.houses.AOGUID,
                houseId: model.houses.AOID
            } as Address.IHouseRequestModel);
        }
        (($$(this.steadSuggestId) as webix.ui.suggest).getList() as any).clearAll();
        ($$(this.searchSteadId) as webix.ui.search).setValue("");
        if (this.isEmptyObject(model.stead) === false) {
            stateUpdate = 0;
            this.refreshStead({
                AOGuid: model.stead.AOGUID,
                steadId: model.stead.AOID
            } as Address.ISteadRequestModel);

        }
        this.updateFieldsByState(stateUpdate, false, true);

    }
    protected refreshsubjectRf(iAoGuid: System.IGuid): void {
        Api.getApiAddress().getAddressObjectCatalog({ AOGuid: iAoGuid }).then((data: Address.IAddressCatalog) => {
            const suggest = $$(this.subjectRfSuggestId) as webix.ui.suggest;
            const search = $$(this.searchsubjectRfId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);
        });
    }
    protected refreshRegion(iAoGuid: System.IGuid): void {
        Api.getApiAddress().getAddressObjectCatalog({ AOGuid: iAoGuid }).then((data: Address.IAddressCatalog) => {
            const suggest = $$(this.regionSuggestId) as webix.ui.suggest;
            const search = $$(this.searchRegionId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);
        });
    }
    protected refreshCity(iAoGuid: System.IGuid): void {
        Api.getApiAddress().getAddressObjectCatalog({ AOGuid: iAoGuid }).then((data: Address.IAddressCatalog) => {
            const suggest = $$(this.citySuggestId) as webix.ui.suggest;
            const search = $$(this.searchCityId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);
        });
    }
    protected refreshSettlement(iAoGuid: System.IGuid): void {
        Api.getApiAddress().getAddressObjectCatalog({ AOGuid: iAoGuid }).then((data: Address.IAddressCatalog) => {
            const suggest = $$(this.settlementSuggestId) as webix.ui.suggest;
            const search = $$(this.searchSettlementId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);
        });
    }
    protected refreshElementStructure(iAoGuid: System.IGuid): void {
        Api.getApiAddress().getAddressObjectCatalog({ AOGuid: iAoGuid }).then((data: Address.IAddressCatalog) => {
            const suggest = $$(this.elementStructureSuggestId) as webix.ui.suggest;
            const search = $$(this.searchElementStructureId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);
        });
    }
    protected refreshStreet(iAoGuid: System.IGuid): void {
        Api.getApiAddress().getAddressObjectCatalog({ AOGuid: iAoGuid }).then((data: Address.IAddressCatalog) => {
            const suggest = $$(this.streetSuggestId) as webix.ui.suggest;
            const search = $$(this.searchStreetId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);
        });
    }
    protected refreshHouse(model: Address.IHouseRequestModel): void {
        const object = this.getNearObject();
        model.AOGuid = object.AOGUID;

        Api.getApiAddress().getHouse(model).then((data: Address.IAddressHouse) => {
            const suggest = $$(this.houseesSuggestId) as webix.ui.suggest;
            const search = $$(this.searchHousesId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);

        });
    }
    protected refreshStead(model: Address.ISteadRequestModel): void {
        const object = this.getNearObject();
        model.AOGuid = object.AOGUID;
        Api.getApiAddress().getStead(model).then((data: Address.IAddressStead) => {
            const suggest = $$(this.steadSuggestId) as webix.ui.suggest;
            const search = $$(this.searchSteadId) as webix.ui.search;
            const list = suggest.getList() as any;
            list.clearAll();
            list.parse(data, "json");
            suggest.setValue(data.name);
            search.setValue(data.name);
        });
    }
    protected afterShowDialog() { this.switchForm(this.isAdvanced); }
    private switchForm(isAdvanced: boolean): void {
        this.isAdvanced = isAdvanced;
        this.initializeAddresUpdate();

        switch (this.isAdvanced) {
            case false:
                $$(this.simpleLabelId).hide();
                $$(this.advancedFormId).hide();
                $$(this.buttonAddId).hide();
                $$(this.simpleFormId).show(false, false);
                $$(this.advancedLabelId).show(false, false);
                this.window.define("width", 860);
                this.window.define("height", 400);
                this.window.resize();
                break;
            case true:
                $$(this.simpleLabelId).show(false, false);
                $$(this.advancedFormId).show(false, false);
                $$(this.buttonAddId).show(false, false);
                $$(this.simpleFormId).hide();
                $$(this.advancedLabelId).hide();
                this.window.define("width", 1100);
                this.window.define("height", 840);
                this.window.resize();
                break;
        }
    }
    private getAddressRequestModel(iAoGuid: System.IGuid, iSearchValue?: string): Address.IAddressRequestModel {
        const model: Address.IAddressRequestModel = {
            AOGuid: iAoGuid,
            searchValue: iSearchValue,
            skip: this.searchOptionsModel.skip,
            take: this.searchOptionsModel.take,
            AOLevel: null,
            liveStatus: null
        };
        if (this.searchOptionsModel.isActual) {
            model.liveStatus = 1;
        }
        return model;
    }
    protected contentConfig() {
        const self = this;

        webix.protoUI({
            name: this.protoUiSearchName,
            $cssName: "search protoUi-with-icons-2",
            $renderIcon: function () {
                const config = this.config;
                config.css = "padding - right: 52px;";

                if (config.icons.length) {
                    const height = config.aheight - 2 * config.inputPadding;
                    const padding = (height - 18) / 2 - 1;
                    let pos = 2;
                    let html = "";


                    for (let i = 0; i < config.icons.length; i++) {
                        html += "<span style='right:" + pos + "px;height:"
                            + (height - padding) + "px;padding-top:" + padding
                            + "px;' class='webix_input_icon fa-" + config.icons[i] + "'></span>";
                        pos += 24;
                    }
                    return html;
                }
                return "";
            },
            on_click: {
                "webix_input_icon": function (e, id, node) {
                    const name = node.className.substr(node.className.indexOf("fa-") + 3);
                    return this.callEvent("on" + name + "IconClick", [e]);
                }
            },
        }, webix.ui.search);

        webix.protoUI({
            name: this.protoUiTextName,
            $cssName: "search protoUi-with-icons-1",
            $renderIcon: function () {
                const config = this.config;
                if (config.icons.length) {
                    const height = config.aheight - 2 * config.inputPadding;
                    const padding = (height - 18) / 2 - 1;
                    let pos = 2;
                    let html = "";

                    for (let i = 0; i < config.icons.length; i++) {
                        html += "<span style='right:" + pos + "px;height:"
                            + (height - padding) + "px;padding-top:" + padding
                            + "px;' class='webix_input_icon fa-" + config.icons[i] + "'></span>";
                        pos += 24;
                    }
                    return html;
                }
                return "";
            },
            on_click: {
                "webix_input_icon": function (e, id, node) {
                    const name = node.className.substr(node.className.indexOf("fa-") + 3);
                    return this.callEvent("on" + name + "IconClick", [e]);
                }
            },
        }, webix.ui.text);

        return {
            scroll: true,
            position: (state) => {
                state.left = state.top = 10;
                state.width = state.maxWidth - 20;
                state.height = state.maxHeight - 80;
            },
            rows: [
                {
                    cols: [
                        self.getSimpleForm(self),
                        self.getAdvancesForm(self)
                    ]
                },
                this.getToolbar(self)
            ]
        };

    }
    private getSelectedForm(): webix.ui.form {
        if (this.isAdvanced) {
            return $$(this.advancedFormId) as webix.ui.form;
        }
        return $$(this.simpleFormId) as webix.ui.form;

    }
    private validateFormFields(field: string): boolean {
        let result: boolean = true;
        const form = this.getSelectedForm();
        const formValues = form.getValues();
        const flatType = formValues["flatType"];
        const flatNumber = formValues["flatNum"];

        switch (field) {
            case "flatType":
                result = !(flatNumber !== "" && flatType === "");
                break;
            case "flatNum":
                result = !(flatNumber === "" && flatType !== "");
                break;
        }
        return result;
    }
    private updateSearchTable() {

        this.updateSelectedAddressField({} as Address.IAddresFullName);
        const dataTable = $$(this.resultDataTableId) as webix.ui.datatable;
        const searchButton = $$(this.searchButtonId) as webix.ui.button;
        const postCode = ($$(this.postCodeId) as webix.ui.text).getValue();
        let cadNumber  = ($$(this.cadNumerId) as webix.ui.text).getValue();
        if (cadNumber !== "") {
            cadNumber = cadNumber.substring(0, 2) + ":" +
                cadNumber.substring(2, 4) + ":" +
                cadNumber.substring(4, 10) + ":" +
                cadNumber.substring(10, 12);
        }
        searchButton.disable();

        (dataTable).showOverlay("Загрузка....");
        (dataTable).clearAll();
        ($$(this.pagerId) as webix.ui.pager).resize();

        if (this.isEmptyObject(this.addressFullName) &&
            postCode === "" && cadNumber === "") {
            dataTable.hideOverlay();
            searchButton.enable();
            return;
        }
        if (this.getNearGuid() == null &&
            postCode === "" && cadNumber === "") {
            dataTable.hideOverlay();
            searchButton.enable();
            return;
        }

        if (this.isEmptyObject(this.addressFullName)) {
            this.addressFullName = {} as Address.IAddresFullName;
            this.addressFullName.aoId = null;
            this.addressFullName.houseId = null;
            this.addressFullName.steadId = null;
        }

       
        Api.getApiAddress().getAddressList({
            AOId: this.addressFullName.aoId,
            houseId: this.addressFullName.houseId,
            steadId: this.addressFullName.steadId,
            onlyActyal: this.searchOptionsModel.isActual,
            postCode: postCode,
            cadNum: cadNumber
        } as any).then((data) => {
       
            (dataTable).parse(data, "json");
            dataTable.hideOverlay();
            searchButton.enable();
            ($$(this.pagerId) as webix.ui.pager).resize();
        }).catch(() => {
            dataTable.hideOverlay();
            searchButton.enable();
            ($$(this.pagerId) as webix.ui.pager).resize();
        });
    }
    private updateSelectedAddressField(data: Address.IAddresFullName) {

        const suggest = $$(this.selectedAddressSuggestId) as webix.ui.gridsuggest;
        const text = $$(this.selectedAddressNameId) as webix.ui.text;
        const list = suggest.getList() as any;
        list.clearAll();
        const addressText = data.addressText as string;
        if (this.isEmptyObject(data)) {
            ($$(this.advancedAoGuidLabelId) as webix.ui.label).setValue("");
            suggest.setValue("");
            text.setValue("");
            return;
        }
        list.parse(data, "json");
        text.setValue(addressText);
        suggest.setValue(addressText);
    }
    private setAddressFullNameModel(isTableUpdate: boolean = false) {

        this.updateSelectedAddressField({} as Address.IAddresFullName);
        /*очищаем таблицу*/
        const dataTable = $$(this.resultDataTableId) as webix.ui.datatable;
        (dataTable).clearAll();

        const nearObject = this.getNearObject() as IAddrObjIdentifier;

        if (this.isEmptyObject(nearObject)) {
            return;
        }

        if (this.searchOptionsModel.houses !== null) {

            this.addressFullName = {
                addressText: "",
                aoId: nearObject.AOID,
                houseId: this.searchOptionsModel.houses.AOID,
                sourceType: this.searchOptionsModel.houses.sourceType,
                steadId: null
            }
            if (isTableUpdate) {
                this.updateSearchTable();
            }
            return;
        }
        if (this.searchOptionsModel.stead !== null) {
            this.addressFullName = {
                addressText: "",
                aoId: nearObject.AOID,
                houseId: null,
                sourceType: this.searchOptionsModel.stead.sourceType,
                steadId: this.searchOptionsModel.stead.AOID
            }
            if (isTableUpdate) {
                this.updateSearchTable();
            }
            return;
        }
        this.addressFullName = {
            addressText: "",
            aoId: nearObject.AOID,
            houseId: null,
            sourceType: nearObject.sourceType,
            steadId: null
        }
        if (isTableUpdate) {
            this.updateSearchTable();
        }

    }
    private updateFieldsByState(state: number = 7, clearFollowing: boolean = false, isUpdateTable: boolean = false): void {

        if (state >= 7) {
            this.updateRegionComponent(clearFollowing);
        }
        if (state >= 6) {
            this.updateCityComponent(clearFollowing);
        }
        if (state >= 5) {
            this.updateSettlementComponent(clearFollowing);
        }
        if (state >= 4) {
            this.updateElementStructureComponent(clearFollowing);
        }
        if (state >= 3) {
            this.updateStreetComponent(clearFollowing);
        }
        if (state >= 2) {
            this.updateHousesComponent(clearFollowing);
        }
        if (state >= 1) {
            this.updateSteadComponent(clearFollowing);
        }
        this.setAddressFullNameModel(isUpdateTable);

    }
    private updateRegionComponent(clearFollowing: boolean = false) {

        const suggest = $$(this.regionSuggestId) as webix.ui.suggest;
        const search = $$(this.searchRegionId) as webix.ui.search;
        const list = suggest.getList() as any;
        const selectValue = search.getValue();
        list.clearAll();
        if (this.searchOptionsModel.subjectRf == null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(this.searchOptionsModel.subjectRf.AOGUID, selectValue);
        Api.getApiAddress().getRegions(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });

    }
    private updateCityComponent(clearFollowing: boolean = false): void {

        const suggest = $$(this.citySuggestId) as webix.ui.suggest;
        const search = $$(this.searchCityId) as webix.ui.search;
        const list = suggest.getList() as any;
        const aoGuid = this.getNearGuid() as System.IGuid;
        const selectValue = search.getValue();
        list.clearAll();
        if (aoGuid === null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(aoGuid, selectValue);
        Api.getApiAddress().getCities(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });
    }
    private updateSettlementComponent(clearFollowing: boolean = false): void {

        const suggest = $$(this.settlementSuggestId) as webix.ui.suggest;
        const search = $$(this.searchSettlementId) as webix.ui.search;
        const list = suggest.getList() as any;
        const aoGuid = this.getNearGuid() as System.IGuid ;
        const selectValue = search.getValue();
        list.clearAll();
        if (aoGuid === null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(aoGuid, selectValue);
        Api.getApiAddress().getSettlements(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });
    }
    private updateElementStructureComponent(clearFollowing: boolean = false): void {

        const suggest = $$(this.elementStructureSuggestId) as webix.ui.suggest;
        const search = $$(this.searchElementStructureId) as webix.ui.search;
        const list = suggest.getList() as any;
        const aoGuid = this.getNearGuid() as System.IGuid;
        const selectValue = search.getValue();
        list.clearAll();
        if (aoGuid === null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(aoGuid, selectValue);
        Api.getApiAddress().getElmStructures(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });
    }
    private updateStreetComponent(clearFollowing: boolean = false): void {

        const suggest = $$(this.streetSuggestId) as webix.ui.suggest;
        const search = $$(this.searchStreetId) as webix.ui.search;
        const list = suggest.getList() as any;
        const aoGuid = this.getNearGuid() as System.IGuid;
        const selectValue = search.getValue();
        list.clearAll();
        if (aoGuid == null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(aoGuid, selectValue);
        Api.getApiAddress().getStreets(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });
    }
    private updateHousesComponent(clearFollowing: boolean = false): void {
        const suggest = $$(this.houseesSuggestId) as webix.ui.suggest;
        const search = $$(this.searchHousesId) as webix.ui.search;
        const list = suggest.getList() as any;
        const aoGuid = this.getNearGuid() as System.IGuid;
        const selectValue = search.getValue();
        list.clearAll();
        if (aoGuid === null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(aoGuid, selectValue);
        Api.getApiAddress().getHouses(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });
    }
    private updateSteadComponent(clearFollowing: boolean = false): void {
        const suggest = $$(this.steadSuggestId) as webix.ui.suggest;
        const search = $$(this.searchSteadId) as webix.ui.search;
        const list = suggest.getList() as any;
        const aoGuid = this.getNearGuid() as System.IGuid ;
        const selectValue = search.getValue();
        list.clearAll();
        if (aoGuid === null) {
            suggest.setValue("");
            search.setValue("");
            return;
        }
        if (clearFollowing) {
            search.setValue("");
        }
        const searchParam = this.getAddressRequestModel(aoGuid, selectValue);
        Api.getApiAddress().getSteads(searchParam).then((data) => {
            list.clearAll();
            list.parse(data, "json");
        });
    }
    private clearSuggestById(suggestId: string): void {
        const suggest = $$(suggestId) as webix.ui.gridsuggest;
        const list: any = suggest.getList();
        list.clearAll();
    }
    private getSelectedAddress(): void {

        const form: webix.ui.form = this.getSelectedForm();
        const formValues = form.getValues();
        let flatSuggest: webix.ui.suggest;
        let flatType: number = null;
        let flatValue: number = 0;
        // simple search
        if (this.isAdvanced === false) {

            if (this.addressUpdate.aoId == null) {
                ($$(this.addressSearchId) as webix.ui.text).setValue("");
            }
            if (!form.validate()) {
                return;
            }
            flatSuggest = $$(this.flatTypeSuggestId) as webix.ui.suggest;
            flatValue = Number(flatSuggest.getValue());
            if (flatValue !== 0 && flatValue != null && flatValue != undefined) {
                flatType = (flatSuggest.getList() as any).getItem(flatSuggest.getValue()).flatTypeId as number;
            }
            this.addressUpdate.flatType = flatType;
            this.addressUpdate.flatNum = formValues["flatNum"];
            this.addressUpdate.location = formValues["location"];
            this.addressUpdate.POBox = formValues["POBox"];


        } else {

            if (!form.validate()) {
                return;
            }
            let dataTable = $$(this.resultDataTableId) as webix.ui.datatable;
            let resultObject = dataTable.getSelectedItem();
            if (resultObject === null) {
                webix.message("Нужно выбрать хотябы один адресный элемент в таблице результатов!", "error");
                return;
            }
            this.addressUpdate.POBox = resultObject.postalCode;
            this.addressUpdate.isActual = true;
            this.addressUpdate.aoId = resultObject.AOId;
            this.addressUpdate.houseId = resultObject.houseId;
            this.addressUpdate.steadId = resultObject.steadId;
            this.addressUpdate.searchType = resultObject.sourceType;
            flatSuggest = $$(this.flatTypeAdvanceSuggestId) as webix.ui.suggest;
            flatValue = Number(flatSuggest.getValue());
            if (flatValue !== 0 && flatValue != null && flatValue != undefined) {
             
                flatType = (flatSuggest.getList() as any).getItem(flatSuggest.getValue()).flatTypeId as number;
            }
            this.addressUpdate.flatType = flatType;
            this.addressUpdate.flatNum = formValues["flatNum"];
            this.addressUpdate.location = formValues["location"];
            this.addressUpdate.POBox = formValues["POBox"];
            
        }
        Api.getApiAddress().AddressUpdate(this.addressUpdate).then((data: Address.IAddressUpdateResultModel) => {
            this.resultAddress = data;
            this.okClose();
        }).catch((data) => {
            console.log(data);
            webix.message(data, "error");
        });


    }
    private getSimpleForm(self: any): object {

        const formElements = [
            {
                id: self.addressSearchId,
                view: self.protoUiSearchName,
                icons: ["close", "search"],
                name: "address",
                type: "form",
                label: "Адрес",
                placeholder: "Введите адрес для поиска...",
                invalidMessage: "Необходимо ввести адрес",
                on: {
                    onSearchIconClick: () => {
                        const suggest = $$(self.addressGridSuggestId) as webix.ui.suggest;
                        const list = suggest.getList() as any;
                        const search = $$(this.addressSearchId) as webix.ui.search;
                        if (list.count() > 0) {
                            suggest.show(search.getInputNode());
                            return;
                        }
                    },
                    onCloseIconClick: () => {
                        ($$(self.addressSearchId) as webix.ui.search).setValue("");
                        ($$(self.simpleAoGuidLabelId) as webix.ui.label).setValue("");
                        self.clearSuggestById(self.addressGridSuggestId);
                        $$(self.addressGridSuggestId).hide();
                        self.initializeAddresUpdate();
                    }
                },
                suggest: {
                    id: self.addressGridSuggestId,
                    view: "gridsuggest",
                    width: 800,
                    textValue: "addressText",
                    body: {
                        yCount: 10,
                        autoheight: false,
                        columns: [
                            { id: "aoId", header: "aoId", hidden: true },
                            { id: "addressText", header: "Адрес", width: 650 },
                            { id: "id", header: "id", hidden: true },
                            { id: "houseId", header: "houseId", hidden: true },
                            { id: "sourceType", header: "sourceType", hidden: true },
                            { id: "steadId", header: "steadId", hidden: true }
                        ],
                        dataFeed: (filtervalue: string) => {

                            if (filtervalue.length === 0) {
                                self.clearSuggestById(self.addressGridSuggestId);
                                $$(self.addressGridSuggestId).hide();
                                ($$(self.simpleAoGuidLabelId) as webix.ui.label).setValue("");
                                return;

                            }
                            if (filtervalue.length < 2) return;
                            Api.getApiAddress().addrSearchFull(filtervalue).then((data) => {
                                const suggest = $$(self.addressGridSuggestId) as webix.ui.gridsuggest;
                                const list: any = suggest.getList();
                                list.clearAll();
                                list.parse(data, "json");
                            });
                            ($$(self.simpleAoGuidLabelId) as webix.ui.label).setValue("");
                            self.initializeAddresUpdate();
                        }
                    },
                    on: {
                        onValueSuggest: (obj: Address.IAddresFullName) => {
                            self.addressUpdate.aoId = obj.aoId;
                            self.addressUpdate.houseId = obj.houseId;
                            self.addressUpdate.steadId = obj.steadId;
                            self.addressUpdate.searchType = obj.sourceType;
                            // гуид только для записей из фиаса
                            if (obj.sourceType !== 2) {
                                Api.getApiAddress().getAddressInfo(obj.aoId).then((data: Address.IAddressAOIdAOGuid) => {
                                    ($$(self.simpleAoGuidLabelId) as webix.ui.label).setValue(data.AOGuid as string);
                                });
                            }
                        },
                        onChange: (newv, oldv) => {
                            if (newv === "") {
                                ($$(self.simpleAoGuidLabelId) as webix.ui.label).setValue("");
                                self.clearSuggestById(self.addressGridSuggestId);
                                $$(self.addressGridSuggestId).hide();
                                self.initializeAddresUpdate();
                            }
                        }
                    }
                }
            },
            {
                cols: [{
                    view: "label",
                    label: "AOGuid:",
                    width: 130
                }, {
                    view: "label",
                    label: "",
                    align: "left",
                    id: self.simpleAoGuidLabelId
                }]
            },
            self.getFlatTypeComponent(),
            {
                id: self.flatNumSimpleId,
                view: self.protoUiTextName,
                icons: ["close"],
                name: "flatNum",
                label: "Номер",
                placeholder: "Введите номер...",
                invalidMessage: "Необходимо заполнить номер помещения",
                on: {
                    onCloseIconClick: () => {
                        const search = $$(self.flatNumSimpleId) as webix.ui.search;
                        search.setValue("");
                    },

                }

            },
            {
                view: self.protoUiTextName,
                icons: ["close"],
                id: self.locationSimpleId,
                name: "location",
                label: "Местоположение",
                placeholder: "Введите местоположение...",
                on: {
                    onCloseIconClick: () => {
                        const search = $$(self.locationSimpleId) as webix.ui.search;
                        search.setValue("");
                    },

                }
            },
            {
                view: self.protoUiTextName,
                icons: ["close"],
                id: self.POBoxSimpleId,
                name: "POBox",
                label: "Абонентский ящик",
                placeholder: "Введите номер...",
                on: {
                    onCloseIconClick: () => {
                        const search = $$(self.POBoxSimpleId) as webix.ui.search;
                        search.setValue("");
                    },

                }
            }
        ];
        return {
            id: self.simpleFormId,
            view: "form",
            animate: false,
            elements: formElements,
            borderless: true,
            margin: 10,
            rules: {
                "address": webix.rules.isNotEmpty,
                "flatType":()=> {
                    return self.validateFormFields("flatType");
                },
                "flatNum": () => {
                    return self.validateFormFields("flatNum");
                }
            },
            elementsConfig: {
                labelPosition: "right",
                labelAlign: "left",
                labelWidth: 130
            }

        };


    }
    private getFlatTypeComponent(formType: boolean = false) {

        let suggestId = this.flatTypeSuggestId;
        let searchId  = this.flatTypeId;
        if (formType) {
            suggestId = this.flatTypeAdvanceSuggestId;
            searchId = this.flatTypeAdvancedId;
        }

        return {
            id: searchId, view: this.protoUiSearchName, name: "flatType", label: "Тип помещения", placeholder: "Выберире тип помещения...",
            icons: ["close", "search"], invalidMessage: "Необходимо заполнить тип помещения",
            on: {
                onCloseIconClick: () => {
                    const suggest = $$(suggestId) as webix.ui.suggest;
                    const search = $$(searchId) as webix.ui.search;
                    suggest.setValue("");
                    search.setValue("");
                },
                onSearchIconClick: () => {
                    const suggest = $$(suggestId) as webix.ui.suggest;
                    const search = $$(searchId) as webix.ui.search;
                    suggest.show(search.getInputNode());
                },
            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: suggestId,
                body: {
                    columns: [
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Тип", width: 380 }
                    ]
                },
                url: {
                    $proxy: true,
                    load: () => {
                        Api.getApiAddress().getFlatTypeList().then((data) => {
                            (($$(suggestId) as webix.ui.gridsuggest).getList() as any).parse(data, "json");
                        }).catch((data) => {
                            console.log(data);
                            webix.message(data, "error");
                        });
                    }
                },
                on: {
                    onValueSuggest: (obj: Address.IFlatTypeModel) => {
                        this.addressUpdate.flatType = obj.flatTypeId;
                    }
                }

            }
        };

    }
    private getToolbar(self: any): object {

        const simpleHide = this.isAdvanced;
        const advancedHide = !simpleHide;

        return {
            view: "toolbar",
            cols: [
                {
                    id: self.advancedLabelId, view: "label", label: "Расширеный поиск",
                    align: "left", width: 150, hidden: simpleHide,
                    click: () => {
                        self.switchForm(true);
                    }
                },
                {
                    id: self.simpleLabelId, view: "label", label: "Простой поиск",
                    align: "left", width: 150, hidden: advancedHide,
                    click: () => {
                        self.switchForm(false);
                    }
                },
                {},
                {
                    id: self.buttonAddId, view: "button",
                    label: "Добавить", align: "right", width: 100,
                    toolbar: "bottom",
                    click: () => {
                        self.searchOptionsModel.cadNum = "";
                        self.searchOptionsModel.postCode = "";
                        const addDialog = AddEditAddress.getAddressDialog(self.addEditDialogId, self.searchOptionsModel);
                        addDialog.showModal(() => {
                            const searchOptionsModel = addDialog.selected;
                            self.searchOptionsModel = {
                                isActual: searchOptionsModel.isActual,
                                postCode: searchOptionsModel.postCode,
                                cadNum: searchOptionsModel.cadNum,
                                searchValue: searchOptionsModel.searchValue,
                                skip: searchOptionsModel.skip,
                                take: searchOptionsModel.take,
                                subjectRf: searchOptionsModel.subjectRf,
                                raion: searchOptionsModel.raion,
                                city: searchOptionsModel.city,
                                settlement: searchOptionsModel.settlement,
                                elementStructure: searchOptionsModel.elementStructure,
                                street: searchOptionsModel.street,
                                houses: searchOptionsModel.houses,
                                stead: searchOptionsModel.stead
                            }
                            self.initializeSearchModel();
                            addDialog.close();
                        });

                    }
                },
                {
                    view: "button", label: "Принять", align: "right", width: 100,
                    toolbar: "bottom",
                    click: () => {
                        self.getSelectedAddress();
                    }
                },
                {

                    view: "button", value: "Закрыть", align: "right", width: 80,
                    click: () => {
                        self.close();
                    }
                },
                { align: "right", width: 5 }

            ]
        }
    }
    private getAdvancesForm(self: any): object {

        const formElements = [{
            cols: [{
                rows: [{
                    view: "select", label: "Страна", name: "country",
                    disabled: false, value: 1,
                    options: [{ "id": 1, "value": "Российская Федерация" }]
                },
                self.getRfSubjectsComponent(self),
                self.getCityComponent(self),
                self.getElementStructureComponent(self),
                self.getStreetComponent(self),
                {
                    view: self.protoUiTextName,
                    icons: ["close"],
                    id: self.cadNumerId,
                    label: "Кадастровый номер",
                    pattern: { mask: "##:##:######:##", allow: /[0-9]/g },
                    name: "CadNumber",
                    validate: (value) => {
                        if (value === "") {
                            return true;
                        }
                        //return /^[0-9]{2}:[0-9]{2}:[0-9]{6}:[0-9]{2}$/.test(value);
                        return /^[0-9]{12}$/.test(value);
                    },
                    on: {
                        onCloseIconClick: () => {
                            const search = $$(self.cadNumerId) as webix.ui.search;
                            search.setValue("");
                        },

                    },
                    placeholder: "Введите номер..."

                }]
            },
            {
                rows: [{
                    view: self.protoUiTextName,
                    icons: ["close"],
                    id: self.postCodeId,
                    label: "Индекс",
                    pattern: { mask: "######", allow: /[0-9]/g },
                    validate: (value) => {
                        if (value === "") {
                            return true;
                        }
                        return /^[0-9]{6}$/.test(value);
                    },
                    name: "postalCode",
                    placeholder: "Введите индекс...",
                    on: {
                        onCloseIconClick: () => {
                            const search = $$(self.postCodeId) as webix.ui.search;
                            search.setValue("");
                        },

                    }

                    },
                self.getRegionComponent(self),
                self.getSettlementComponent(self),
                self.getHousesComponent(self),
                self.getSteadComponent(self),
                {
                    view: "radio",
                    vertical: true,
                    value: 0, options: [
                        { id: 0, value: "Актуальные и исторические" },
                        { id: 1, value: "Только актуальные" }
                    ],
                    on: {
                        onChange: (newv, oldv) => {
                            self.searchOptionsModel.isActual = false;

                            if (newv === 1 || newv === "1" ) {
                                self.searchOptionsModel.isActual = true;
                            }
                            ($$(self.searchHousesId) as webix.ui.search).setValue("");
                            ($$(self.searchSteadId) as webix.ui.search).setValue("");
                            self.clearGuid("houses");
                            self.updateFieldsByState(2, true);
                        }
                    }

                }
                ]
            }]
        },
        {
            cols: [{
            }, {
                template: "right",
                rows: [
                    {
                        view: "button", label: "Найти",
                        id: self.searchButtonId, width: 100,
                        click: () => {
                            self.updateSearchTable();
                        }
                    }
                ]
            }]
        },
        {
            cols: [{
                view: "fieldset",
                gravity: 1,
                label: "Результаты поиска",
                minHeight: 50,
                position: (state) => {
                    state.left = state.top = 10;
                    state.width = state.maxWidth - 20;
                    state.height = state.maxHeight - 20;
                },
                body: self.getSearchTableComponent(self),
            }]
        },
        {
            view: "fieldset",
            label: "Заполняется вручную",
            position: (state) => {
                state.left = state.top = 10;
                state.width = state.maxWidth - 20;
                state.height = state.maxHeight - 20;
            },
            body: {
                cols: [{
                    width: 200,
                },
                {
                    gravity: 1,
                    paddingX: 10,
                    rows: [
                        {
                            id: self.selectedAddressNameId,
                            view: "text",
                            name: "selectedAddressName",
                            placeholder: "",
                            label: "Адрес",
                            invalidMessage: "Нужно выбрать хотябы один адресный элемент в таблице результатов поиска!",
                            readonly: true,
                            suggest: {
                                id: self.selectedAddressSuggestId,
                                view: "gridsuggest",
                                width: 375,
                                textValue: "addressText",
                                body: {
                                    yCount: 10,
                                    scroll: true,
                                    autoheight: false,
                                    columns: [
                                        { id: "aoId", header: "id", hidden: true },
                                        { id: "addressText", header: "Адрес", width: 500 },
                                        { id: "houseId", header: "houseId", hidden: true },
                                        { id: "sourceType", header: "sourceType", hidden: true },
                                        { id: "steadId", header: "steadId", hidden: true }
                                    ]
                                }
                            }
                        },
                        {
                            cols: [{
                                view: "label",
                                label: "AOGuid:",
                                width: 130
                            }, {
                                view: "label",
                                label: "",
                                align: "left",
                                id: self.advancedAoGuidLabelId
                            }]
                        },
                        self.getFlatTypeComponent(true),
                        {
                            id: self.flatNumAdvancedId,
                            view: self.protoUiTextName,
                            icons: ["close"],
                            name: "flatNum",
                            label: "Номер",
                            placeholder: "Введите номер...",
                            invalidMessage: "Необходимо заполнить номер помещения",
                            on: {
                                onCloseIconClick: () => {
                                    const search = $$(self.flatNumAdvancedId) as webix.ui.search;
                                    search.setValue("");
                                },

                            }

                        },
                        {
                            view: self.protoUiTextName,
                            icons: ["close"],
                            id: self.locationAdvancedId,
                            name: "location",
                            label: "Местоположение",
                            placeholder: "Введите местоположение...",
                            on: {
                                onCloseIconClick: () => {
                                    const search = $$(self.locationAdvancedId) as webix.ui.search;
                                    search.setValue("");
                                },

                            }
                        },
                        {
                            view: self.protoUiTextName,
                            icons: ["close"],
                            id: self.POBoxAdvancedId,
                            name: "POBox",
                            label: "Абонентский ящик",
                            placeholder: "Введите номер...",
                            on: {
                                onCloseIconClick: () => {
                                    const search = $$(self.POBoxAdvancedId) as webix.ui.search;
                                    search.setValue("");
                                },

                            }
                        }

                    ]
                }, { width: 200 }]
            }
        },
        { height: 5 }
        ];
        return {
            id: self.advancedFormId,
            view: "form",
            elements: formElements,
            animate: false,
            borderless: true,
            position: (state) => {
                state.left = state.top = 10;
                state.width = state.maxWidth - 20;
                state.height = state.maxHeight - 20;
            },
            elementsConfig: {
                labelPosition: "left",
                labelAlign: "left",
                labelWidth: 130,
                minWidth: 350,
                animate: false
            },
            rules: {
                "selectedAddressName": webix.rules.isNotEmpty,
                "flatType": () => {
                    return self.validateFormFields("flatType");
                },
                "flatNum": () => {
                    return self.validateFormFields("flatNum");
                }
            },
        };
    }
    private getRfSubjectsComponent(self): object {
        return {
            view: self.protoUiSearchName,
            icons: ["close", "search"],
            name: "subjectRf", label: "Субъект РФ",
            id: self.searchsubjectRfId,
            placeholder: "Выберите субъект РФ...",
            on: {

                onSearchIconClick: () => {
                    const search = $$(self.searchsubjectRfId) as webix.ui.search;
                    const suggest = $$(self.subjectRfSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());

                },
                onCloseIconClick: () => {
                    self.clearGuid("subjectRf");
                    const suggest = $$(self.subjectRfSuggestId) as webix.ui.suggest;
                    const list = suggest.getList() as any;
                    suggest.setValue("");
                    const search = $$(self.searchsubjectRfId) as webix.ui.search;
                    search.setValue("");
                    Api.getApiAddress().getRFSubjects({
                        searchValue: "",
                        skip: 0,
                        take: 100,
                        AOLevel: null
                    }).then((data) => {
                        list.clearAll();
                        list.parse(data, "json");
                    });
                    self.updateFieldsByState(7);
                }
            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.subjectRfSuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "AOGuid", header: "AOGuid", hidden: true },
                        { id: "AOId", header: "AOId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Субъект РФ", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    url: {
                        $proxy: true,
                        load: () => {
                            Api.getApiAddress().getRFSubjects({
                                searchValue: "",
                                skip: 0,
                                take: 100,
                                AOLevel: null
                            }).then((data) => {
                                const suggest = $$(this.subjectRfSuggestId) as webix.ui.suggest;
                                const list = suggest.getList() as any;
                                list.clearAll();
                                list.parse(data, "json");
                            });
                        }
                    },
                    dataFeed: (filtervalue: string) => {
                        Api.getApiAddress().getRFSubjects({
                            searchValue: filtervalue,
                            skip: 0,
                            take: 100,
                            AOLevel: null
                        }).then((data) => {
                            const suggest = $$(self.subjectRfSuggestId) as webix.ui.suggest;
                            const list: any = suggest.getList();
                            list.clearAll();
                            list.parse(data, "json");
                        });
                    },
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            ($$(this.searchsubjectRfId) as webix.ui.search).setValue(obj.name);
                            self.clearGuid("subjectRf");
                            self.searchOptionsModel.subjectRf = {
                                AOGUID: obj.AOGuid,
                                AOID: obj.AOId,
                                AOLevel: AOLevel.RfSubject,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.updateFieldsByState(7, true);
                        }
                    }
                }
            }

        };
    }
    private getRegionComponent(self): object {
        return {
            name: "region", label: "Район",
            view: self.protoUiSearchName,
            icons: ["close", "search"],
            id: self.searchRegionId,
            placeholder: "Выберите район...",
            on: {
                onSearchIconClick: () => {
                    const search = $$(self.searchRegionId) as webix.ui.search;
                    const suggest = $$(self.regionSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: () => {
                    const search = $$(self.searchRegionId) as webix.ui.search;
                    search.setValue("");
                    self.clearGuid("raion");
                    self.updateFieldsByState(7, true);
                }
            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.regionSuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "AOGuid", header: "AOGuid", hidden: true },
                        { id: "AOId", header: "AOId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Район", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("raion");
                        self.updateFieldsByState(7);
                    },
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            ($$(self.searchRegionId) as webix.ui.search).setValue(obj.name);
                            self.clearGuid("raion");
                            self.searchOptionsModel.raion = {
                                AOGUID: obj.AOGuid,
                                AOID: obj.AOId,
                                AOLevel: AOLevel.Region,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.updateFieldsByState(6, true);
                        }

                    }
                }
            }

        };
    }
    private getCityComponent(self): object {
        return {

            view: self.protoUiSearchName,
            icons: ["close", "search"],
            name: "city", label: "Город",
            id: self.searchCityId, placeholder: "Выберите город...",
            on: {
                onSearchIconClick: () => {
                    const search = $$(self.searchCityId) as webix.ui.search;
                    const suggest = $$(self.citySuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: () => {
                    const search = $$(self.searchCityId) as webix.ui.search;
                    search.setValue("");
                    self.clearGuid("city");
                    self.updateFieldsByState(6, true);
                }

            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.citySuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "AOGuid", header: "AOGuid", hidden: true },
                        { id: "AOId", header: "AOId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Город", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("city");
                        self.updateFieldsByState(6);
                    },
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            self.clearGuid("city");
                            ($$(self.searchCityId) as webix.ui.search).setValue(obj.name);
                            self.searchOptionsModel.city = {
                                AOGUID: obj.AOGuid,
                                AOID: obj.AOId,
                                AOLevel: AOLevel.City,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;

                            self.updateFieldsByState(5, true);
                        }
                    }
                }
            }

        };
    }
    private getSettlementComponent(self): object {
        return {

            view: self.protoUiSearchName,
            icons: ["close", "search"], name: "settlement", label: "Населенный пункт",
            id: self.searchSettlementId, placeholder: "Выберите населенный пункт...",
            on: {
                onSearchIconClick: ()=> {
                    const search = $$(self.searchSettlementId) as webix.ui.search;
                    const suggest = $$(self.settlementSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: ()=> {
                    const search = $$(self.searchSettlementId) as webix.ui.search;
                    search.setValue("");
                    self.clearGuid("settlement");
                    self.updateFieldsByState(5, true);
                }

            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.settlementSuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "AOGuid", header: "AOGuid", hidden: true },
                        { id: "AOId", header: "AOId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Населенный пункт", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("settlement");
                        self.updateFieldsByState(5);
                    }
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            self.clearGuid("settlement");
                            ($$(self.searchSettlementId) as webix.ui.search).setValue(obj.name);
                            self.searchOptionsModel.settlement = {
                                AOGUID: obj.AOGuid,
                                AOID: obj.AOId,
                                AOLevel: AOLevel.Settlement,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.updateFieldsByState(4, true);
                        }
                    }
                }
            }


        };
    }
    private getElementStructureComponent(self): object {
        return {

            view: self.protoUiSearchName,
            icons: ["close", "search"], name: "elmStructure", label: "Элемент планировочной структуры",
            placeholder: "Выберите элемент планировочной структуры...",
            id: self.searchElementStructureId,
            on: {
                onSearchIconClick: () => {
                    const search = $$(self.searchElementStructureId) as webix.ui.search;
                    const suggest = $$(self.elementStructureSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: () => {
                    const search = $$(self.searchElementStructureId) as webix.ui.search;
                    search.setValue("");
                    self.clearGuid("elementStructure");
                    self.updateFieldsByState(4, true);

                }
            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.elementStructureSuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "AOGuid", header: "AOGuid", hidden: true },
                        { id: "AOId", header: "AOId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Элемент планировочной структуры", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("elementStructure");
                        self.updateFieldsByState(4, true);
                    }
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            self.clearGuid("elementStructure");
                            ($$(self.searchElementStructureId) as webix.ui.search).setValue(obj.name);
                            self.searchOptionsModel.elementStructure = {
                                AOGUID: obj.AOGuid,
                                AOID: obj.AOId,
                                AOLevel: AOLevel.ElmStructure,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.updateFieldsByState(3);
                        }
                    }
                }
            }

        };
    }
    private getStreetComponent(self): object {
        return {

            view: self.protoUiSearchName,
            icons: ["close", "search"], name: "street", label: "Улица",
            id: self.searchStreetId, placeholder: "Выберите улицу...",
            on: {
                onSearchIconClick: () => {
                    const search = $$(self.searchStreetId) as webix.ui.search;
                    const suggest = $$(self.streetSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: () => {
                    const search = $$(self.searchStreetId) as webix.ui.search;
                    search.setValue("");
                    self.clearGuid("street");
                    self.updateFieldsByState(3, true);
                }
            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.streetSuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "AOGuid", header: "AOGuid", hidden: true },
                        { id: "AOId", header: "AOId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Улица", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("street");
                        self.updateFieldsByState(3);
                    },
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            self.clearGuid("street");
                            ($$(self.searchStreetId) as webix.ui.search).setValue(obj.name);
                            self.searchOptionsModel.street = {
                                AOGUID: obj.AOGuid,
                                AOID: obj.AOId,
                                AOLevel: AOLevel.Street,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.updateFieldsByState(2, true);
                        }
                    }
                }
            }

        };
    }
    private getHousesComponent(self): object {
        return {

            view: self.protoUiSearchName,
            icons: ["close", "search"], name: "houses", label: "Номер здания/сооружения/строения",
            placeholder: "Введите здания/сооружения/строения...",
            id: self.searchHousesId,
            on: {
                onSearchIconClick: () => {
                    const search = $$(self.searchHousesId) as webix.ui.search;
                    const suggest = $$(self.houseesSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: () => {
                    const search = $$(self.searchHousesId) as webix.ui.search;
                    search.setValue("");
                    self.clearGuid("houses");
                    self.updateFieldsByState(2, true);
                }
            },
            suggest: {
                view: "gridsuggest",
                width: 375,
                id: self.houseesSuggestId,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "houseGuid", header: "houseGuid", hidden: true },
                        { id: "houseId", header: "houseId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Номер здания/сооружения/строения", width: 500 },
                        { id: "postalCode", header: "postalCode", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("houses");
                        self.updateFieldsByState(2);
                    }
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            self.clearGuid("houses");
                            ($$(self.searchHousesId) as webix.ui.search).setValue(obj.name);
                            self.searchOptionsModel.houses = {
                                AOGUID: obj.houseGuid,
                                AOID: obj.houseId,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.updateFieldsByState(1, true);
                        }
                    }
                }
            }

        };
    }
    private getSteadComponent(self): object {
        return {

            view: self.protoUiSearchName,
            icons: ["close", "search"], name: "stead", label: "Номер земельного участка",
            id: self.searchSteadId, placeholder: "Введите номер земельного участка...",
            on: {
                onSearchIconClick: () => {
                    const search = $$(self.searchSteadId) as webix.ui.search;
                    const suggest = $$(self.steadSuggestId) as webix.ui.suggest;
                    suggest.show(search.getInputNode());
                },
                onCloseIconClick: () => {
                    const search = $$(self.searchSteadId) as webix.ui.search;
                    search.setValue("");
                    self.clearGuid("stead");
                    self.updateFieldsByState(1, true);
                }
            },

            suggest: {
                view: "gridsuggest",
                id: self.steadSuggestId,
                width: 375,
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    columns: [
                        { id: "steadGuid", header: "steadGuid", hidden: true },
                        { id: "steadId", header: "steadId", hidden: true },
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "Номер земельного участка", width: 500 },
                        { id: "sourceType", header: "sourceType", hidden: true }
                    ],
                    dataFeed: () => {
                        self.clearGuid("stead");
                        self.updateFieldsByState(1);
                    }
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            self.clearGuid("houses");
                            ($$(self.searchHousesId) as webix.ui.search).setValue("");
                            self.updateHousesComponent(true);
                            ($$(self.searchSteadId) as webix.ui.search).setValue(obj.name);
                            this.searchOptionsModel.stead = {
                                AOGUID: obj.steadGuid,
                                AOID: obj.steadId,
                                sourceType: obj.sourceType
                            } as IAddrObjIdentifier;
                            self.setAddressFullNameModel();
                        }
                    }
                }
            }

        };
    }
    private getSearchTableComponent(self): object {
        const pager = Pager.getDataTablePager(this.pagerId);
        return {
            rows: [{
                view: "datatable",
                id: self.resultDataTableId,
                select: "row",
                resizeColumn: true,
                navigation: true,
                scrollY: true,
                pager: self.pagerId,
                yCount: 5,
                on: {
                    onAfterSelect: () => {
                        const dataTable = $$(this.resultDataTableId) as webix.ui.datatable;
                        const object = dataTable.getSelectedItem();
                        let setGuid = object.AOGuid;
                        if (object.sourceType === 2) {
                            setGuid = "";
                        }
                        if (object.actualStateName === 'Не актуальный') {
                            //($$(self.resultDataTableId) as webix.ui.datatable).unselect(selection);                        
                            ($$(self.advancedAoGuidLabelId) as webix.ui.label).setValue("");
                            self.updateSelectedAddressField({});
                            return;
                        }
                        ($$(self.advancedAoGuidLabelId) as webix.ui.label).setValue(setGuid);
                        self.updateSelectedAddressField(object);
                    }
                },
                columns: [
                    { id: "addressText", header: "Наименование(текстовая часть выбранного адреса)", minWidth: 420, fillspace: true },
                    { id: "postalCode", header: "Индекс", width: 70 },
                    { id: "houseNum", header: "№ здания", width: 90 },
                    { id: "buildNum", header: "№ сооружения", width: 100 },
                    { id: "strucNum", header: "№ корпуса", width: 90 },
                    { id: "buildStateName", header: "Строительный статус(да/нет)", width: 200 },
                    { id: "actualStateName", header: "Статус(актуальный/архивный)", width: 200 },
                    { id: "startDate", header: "Дата внесения записи", format: webix.i18n.dateFormatStr, width: 80 },
                    { id: "cadNum", header: "Кадастровый номер", width: 150 },
                    { id: "AOId", header: "Уникальный номер", width: 220 },
                    { id: "AOGuid", header: "AOGuid", width: 220, hidden: true }
                ]
            }, pager.init()]
        }

    }
}

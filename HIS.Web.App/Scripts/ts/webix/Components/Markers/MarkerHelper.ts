﻿export class markerHelper {

    public static convertListForOptions(list: Markers.IListItemModel[], epmtyValue: boolean = true) {

        const result = epmtyValue ? [{ id: -1, value: "" }] : [];
        if (!Array.isArray(list))
            return result; 
        list.forEach(item => {
            result.push({
                id: item.itemId,
                value: item.value
            });
        });
        return result;
    }

    public static requiredConfig(controlId: string, rule: (value) => boolean = null) {
        return {
            required: true,
            validate: (rule) ? rule : webix.rules.isNotEmpty,
            on: {
                onChange: () => {
                    ($$(controlId) as webix.ui.text).validate();
                }
            }
        }
    }

}
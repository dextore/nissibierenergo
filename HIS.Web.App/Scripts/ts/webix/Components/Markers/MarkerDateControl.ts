﻿import Base = require("./MarkerControlBase");


export function getMarkerDateControl(viewName: string, markerInfo: Markers.IMarkerInfoModel, getExtConfig: (content: any) => any = null): Base.IMarkerControl {
    return new MarkerDateControl(viewName, markerInfo, getExtConfig);
}

class MarkerDateControl extends Base.MarkerControlBase implements Base.IMarkerControl {

    get control() {
        return $$(this.controlId) as webix.ui.datepickerExtended;
    };

    constructor(id: string, markerInfo: Markers.IMarkerInfoModel, getExtConfig: (content: any) => any = null) {
        super(id, markerInfo, getExtConfig);
    }

    public customValidate(): boolean {

        if (this.readOnly)
            return true;

        const value = this.control.getValue();
      
        if (this.markerInfo.isRequired && !value)
            return false;

        if (!this.control.getValidateTextValue()) 
            return false;

        return true;
    }

    protected config() {

        let config = {
            view: "datepickerExtended",
            id: this.controlId,
            editable: true,
            timepicker: false,
            label: this.markerInfo.label,
            name: this.markerInfo.name,
            readonly: this.readOnly,
            pattern: { mask: "##.##.####", allow: /[0-9]/g }, 
            required: this.markerInfo.isRequired,
            validate: (value) => {
                return this.customValidate();
            },
            on: {
                onChange: (newv, oldv) => {
                    this.control.validate();
                },
                onBlur: () => {
                    this.control.validate();
                }
            }
        };
        return webix.extend(config, this.getExtConfig(this), true);
    }

    setValueInternal(value: Markers.IMarkerValueModel, model: { [index: string]: any; }) {
        if (!value || !value.value) {
            model[this.markerInfo.name] = null;
            return;
        }
        super.setValueInternal(value, model);
    }

}
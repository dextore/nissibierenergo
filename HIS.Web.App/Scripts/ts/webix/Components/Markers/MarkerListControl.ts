﻿import Base = require("./MarkerControlBase");
import Helper = require("./MarkerHelper");

export interface IMarkerListControl extends Base.IMarkerControl {
    control: webix.ui.select;
}

export function getMarkerListControl(viewName: string,
    markerInfo: Markers.IMarkerInfoModel,
    getExtConfig: (content: any) => any = null): IMarkerListControl {
    return new MarkerListControl(viewName, markerInfo, getExtConfig);
}

export function getMarkerReadioControl(viewName: string,
    markerInfo: Markers.IMarkerInfoModel,
    extConfig: any): IMarkerListControl {
    return new MarkerListControl(viewName, markerInfo, extConfig, true);
}

class MarkerListControl extends Base.MarkerControlBase implements IMarkerListControl {

    get control() {
        return $$(this.controlId) as webix.ui.select;
    };

    constructor(id: string,
        markerInfo: Markers.IMarkerInfoModel,
        getExtConfig: (content: any) => any,
        private readonly _asRadio: boolean = false) {
        super(id, markerInfo, getExtConfig);
    }

    public customValidate(): boolean {
        if (this.readOnly)
            return true;
        const value = this.control.getValue();
        if (this.markerInfo.isRequired) {
            if (!value)
                return false;
            if (value === "0") {
                return false;
            }
        }
        return true;
    }


    protected config() {

        let config = {
            view: this._asRadio ? "radio" : "select",
            id: this.controlId,
            label: this.markerInfo.label,
            name: this.markerInfo.name,
            required: this.markerInfo.isRequired,
            readonly: this.readOnly,
            options: Helper.markerHelper.convertListForOptions(this.markerInfo.list, !this._asRadio),
            validate: (value) => {
                return this.customValidate();
            },
            on: {
                onChange: (newv, oldv) => {
                    this.control.validate();
                }
            }
        };
        return webix.extend(config, this.getExtConfig(this), true);
    }

    setReadOnly(value: boolean): void {
        super.setReadOnly(value);
        if ($$(this.controlId)) {
            value ? $$(this.controlId).disable() : $$(this.controlId).enable();
        }
    }

}
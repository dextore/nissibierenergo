﻿import Api = require("../../Api/ApiExporter");

//export interface I

const hidenColumns = {
    ItemId: 1,
    ItemName: 1,
    IsActive: 1,
    OrderNum: 1,
    id: 1,
    nullValue: 1
};

export class CatalogMarkerHelper {
    // current value
    // sugest 

    // получить представление
    // если маркер не обязателен добавить пустую запись
    // сформировать колонки
    // установка значения, проверка если значение не актуально, добавить в список для выбора!!!

    static dataToControl(datatable: webix.ui.datatable, markerInfo: Markers.IMarkerInfoModel) {
        Api.getApiMarkers().getCatalogMarkerValueItems(markerInfo.id).then(items => {

            let resultItems = CatalogMarkerHelper.prepareItems(JSON.parse(items));

            if (!markerInfo.isRequired)
                resultItems = CatalogMarkerHelper.setEmptyItem(resultItems);

            const columns = CatalogMarkerHelper.getColumns(resultItems[0]);
            datatable.define("columns", columns);
            datatable.refreshColumns();

            datatable.define("header", columns.filter(item => !item.hidden).length > 1);
            datatable.refresh();

            if (datatable.config.yCount > resultItems.length) {
                datatable.define("yCount", resultItems.length);
                datatable.refresh();
            }

            datatable.clearAll();
            datatable.parse(resultItems, "json");
        });
    } 

    static setGridsuggestWidtn(ctrl: webix.ui.richselect,
        gridsuggest: webix.ui.gridsuggest,
        datatable: webix.ui.datatable) {
        const width = ctrl.getInputNode().clientWidth;
        const columns = webix.toArray(datatable.config.columns);
        const colWidth = Math.ceil(width / columns.length);
        columns.forEach(item => item.width = colWidth);
        datatable.refreshColumns();
        gridsuggest.define("width", width);
        gridsuggest.resize();
    }

    private static getColumns(dataItem: any): any[] {

        const columns = [];

        for (let key in dataItem) {
            if (dataItem.hasOwnProperty(key)) {
                columns.push({
                    id: key,
                    header: key,
                    //width: 300,
                    //fillspace: true,
                    hidden: !!hidenColumns[key]
                });
            }
        }
        return columns;
    }

    private static prepareItems(items: any[]): any[] {
        return items.map(item => {
            return webix.extend(item, { id: item.ItemId, nullValue: false});
        });
    }

    private static setEmptyItem(items: any[]): any[] {
        const item = items[0];

        const emptyItem = {};

        let firstField = false;
        for (var key in item) {
            if (!hidenColumns[key] && !firstField) {
                emptyItem[key] = "<<пусто>>";
                firstField = true;
                continue;
            }
            emptyItem[key] = null;
        }
        emptyItem["id"] = "empty value";

        return [emptyItem, ...items];
    }
}
﻿import Base = require("./PeriodMarkerControlBase")
import Helper = require("./MarkerHelper");
import Tab = require("../../MainView/Tabs/MainTabBase");
import Api = require("../../Api/ApiExporter");
import SearchDlg = require("../../MainView/UniversalSearch/SearchDlg");

export interface IPeriodEntityRefMarkerControl extends Base.IPeriodMarkerControl {
    setModule(module: Tab.IMainTab);
}

//export interface IEntityRefMarkerControllAddButtonDisabler extends Base.IMarkerControl {
//    isDisabledAddButton;
//}

export function getPeriodEntityRefMarkerControl(viewName: string,
    markerInfo: Markers.IMarkerInfoModel, 
    getExtConfig: (content: any) => any = null): IPeriodEntityRefMarkerControl {
    return new PeriodEntityRefMarkerControl(viewName, markerInfo, getExtConfig);
}

class PeriodEntityRefMarkerControl extends Base.PeriodMarkerControlBase implements IPeriodEntityRefMarkerControl {

    private _disabledAddButton: boolean = false;
    private _entityBaId: number = null;
    private _module: Tab.IMainTab = null;

    get suggestId() { return `suggest-${this.controlId}`; }
    get suggestBodyId() { return `suggest-body-${this.controlId}`; }

    get isDisabledAddButton() {
        return this._disabledAddButton;
    }
    set isDisabledAddButton(value: boolean) {
        this._disabledAddButton = value;
    }

    get control() {
        return $$(this.controlId) as webix.ui.search;
    };

    constructor(id: string, markerInfo: Markers.IMarkerInfoModel, getExtConfig: (content: any) => any) {
        super(id, markerInfo, getExtConfig);
    }


    setModule(module: Tab.IMainTab) {
        if (!module)
            return;
        this._module = module;
        super.setTabModule(this._module);
    }
    
    protected setReadOnly(value: boolean) {
        super.setReadOnly(value);
        this.control.define("readonly", "true");
        this.control.refresh();
        //value ? this.control.disable() : this.control.enable();
    }

    setValueInternal(value: Markers.IMarkerValueModel, model: { [index: string]: any; }) {
        super.setValueInternal(value, model);
        model[this.markerInfo.name] = (!value || !value.displayValue) ? "" : value.displayValue;
        this._entityBaId = (!value || !value.value) ? null : Number(value.value);
    }

    getValue(model: { [index: string]: any; }): Markers.IMarkerValueModel {

        const value = super.getValue(model);
        value.value = (!this._entityBaId) ? null : this._entityBaId.toString();
        value.displayValue = this.control.getValue();
        if (this.isChanged && !value.value) {

            if (!value.startDate) {
                value.startDate = this.initDate;
            }
            value.isDeleted = true;
        }
        return value;
    }

    get isChanged(): boolean {

        let currentValue = this._entityBaId;
        let initValue = this.initValue;

        if (!currentValue) {
            currentValue = null;
        }
        if (!initValue) {
            initValue = null;
        }

        if (this.isChangeDate || (currentValue != initValue)) {
            return true;
        }
        return false;
    };

    public customValidate(): boolean {

        if (this.readOnly)
            return true;

        const value = this.control.getValue();

        if (this.markerInfo.isRequired) {
            if (!value) {
                return false;
            }
            if (!this._entityBaId && !this.forFilter) {
                return false;
            }

        }
        if (this.forFilter) {
            return true;
        }
        const dateValue = this.datePickerControl.getValue();
        if (!value && !!dateValue && this.datePickerControl.validate()) {
            return false;
        }
        if (!!value && !dateValue) {
            this.datePickerControl.validate();
        }
        return true;



    }

    private clearControl() {

        this._entityBaId = null;
        this.control.setValue(null);
        this.control.setValue("");
        this.control.validate();

    }
    private resizeSuggestTable() {

        const table = $$(this.suggestBodyId) as webix.ui.datatable;
        const control = $$(this.controlId) as webix.ui.text;

        if (table.count() < table.config.yCount) {
            table.config.yCount = table.count();
            table.resize();
        }

        const width = control.getInputNode().clientWidth;
        if (table.$width != width) {

            const columns = webix.toArray(table.config.columns);
            columns.forEach(item => item.width = width - 37);
            table.refreshColumns();

        }

    }
    getSelectedCompany() {

        if (!this._module) {
            return null;
        }
        return this._module.mainTabView.getSelectedCompany();
    }
    config() {

        let config: any = null;
        let action = this.markerInfo.searchTypeId;
        if (this.forFilter) {
            action = -1;
        }
        switch (action) {
        // диалоговое окно поиска
        //case -1:
        //    config = this.getConfigForSearch();
        //    break;
        // выпадающий список
        case 1:
            config = this.getConfigForList();
            break;
        // форма поиска 
        default:
            config = this.getConfigForForm();
            break;
        }
        /*
        if (this.markerInfo.isRequired) {
            config = webix.extend(config, Helper.markerHelper.requiredConfig(this.controlId, webix.rules.isNotEmpty));
        }
        */

        return { cols: [config] }
    }
    private getConfigForList() {
        return webix.extend({
            view: "extsearch",
            id: this.controlId,
            css: "protoUI-with-icons-1",
            icons: !this.forFilter ? ["angle-down"] : [],
            readonly: true,
            label: this.markerInfo.label,
            name: this.markerInfo.name,
            validate: (value) => {
                return this.customValidate();
            },
            on: {
                onItemClick: () => {

                    if (this.readOnly)
                        return;

                    if (this.markerInfo.isBlocked) {
                        return;
                    }
                    this.resizeSuggestTable();
                    const control = $$(this.controlId) as webix.ui.text;
                    const suggest = $$(this.suggestId) as webix.ui.suggest;
                    suggest.show(control.getInputNode());
                },
                onBlur: () => {
                    ($$(this.suggestId) as webix.ui.suggest).hide();
                }
            },
            suggest: {
                id: this.suggestId,
                view: "gridsuggest",
                textValue: "name",
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            if (obj["isNull"]) {
                                this.clearControl();
                                if (!this.forFilter) {
                                    this.datePickerControl.validate();
                                }
                                return;
                            }
                            this._entityBaId = obj["id"];
                            //this.isChanged = true;
                            this.control.validate();
                        }
                    }
                },
                body: {
                    id: this.suggestBodyId,
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    autowidth: true,
                    header: false,
                    columns: [
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "name" },
                        { id: "isNull", header: "isNull", hidden: true }
                    ],
                    url: {
                        $proxy: true,
                        load: () => {


                            Api.getApiQueryBuilder().getFields(this.markerInfo.refBaTypeId).then((colums: Querybuilder.ICaptionAndAliaseModel[]) => {

                             

                                let aliaseBaId = "";
                                let aliaseName = "";
                                let aliaseOrganization = "";

                                colums.forEach(item => {

                                    if (item.aliase.indexOf("//Name]") >= 0 && item.markerId == 1) {
                                        aliaseName = item.aliase;
                                    }
                                    if (item.aliase.indexOf("//BAId]") >= 0) {
                                        aliaseBaId = item.aliase;
                                    }
                                    if (item.aliase.indexOf("//CompanyArea]") >= 0) {
                                        aliaseOrganization = item.aliase;
                                    }

                                });
                                const delStateAliase = aliaseName.replace("//Name]", "//IsDelState]")

                                const queryParams = {
                                    baTypeId: this.markerInfo.refBaTypeId,
                                    from: 0,
                                    to: 1000,
                                    filteringsFields: [{
                                        field: delStateAliase,
                                        filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal,
                                        value: "false"
                                    }],
                                    orderingsFields: [{
                                        orderingField: aliaseName,
                                        sortingOrder: HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Asc
                                    }]
                                };

                                const companyId = this.getSelectedCompany().id;

                                if (aliaseOrganization != "" && companyId > 0 && this.markerInfo.id != 6) {
                                    queryParams.filteringsFields.push({
                                        field: aliaseOrganization,
                                        filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal,
                                        value: companyId.toString()
                                    });
                                }

                                Api.getApiQueryBuilder().getData(queryParams as any).then((data) => {

                                    let items = JSON.parse(data as any).items as any[];
                                    const result = [] as any;
                                    if (!this.markerInfo.isRequired) {
                                        result.push({
                                            id: null,
                                            name: "<<пусто>>",
                                            isNull: true
                                        });
                                    }
                                    const list = ($$(this.suggestId) as webix.ui.gridsuggest).getList() as any;
                                    list.clearAll();
                                    items.forEach(item => {
                                        result.push({
                                            id: item[aliaseBaId],
                                            name: item[aliaseName],
                                            isNull: false
                                        });
                                    });
                                    list.parse(result, "json");



                                }).catch((error) => {
                                    console.log(error);
                                });

                            }).catch((error) => {
                                console.log(error);
                            });
                        }
                    }
                }
            }

        }, this.getExtConfig(this), true);
    }
    private getConfigForForm() {

        return webix.extend({
            view: "extsearch",
            id: this.controlId,
            css: "protoUI-with-icons-1",
            icons: !this.forFilter ? ["search", "close"] : [],
            label: this.markerInfo.label,
            readonly: !this.forFilter,
            name: this.markerInfo.name,
            validate: (value) => {
                return this.customValidate();
            },
            on: {
                onSearchIconClick: () => {
                    if (this.readOnly)
                        return;

                    if (!this._module) {
                        webix.message("Tab module not set", "error");
                        return;
                    }
                    if (this.markerInfo.isBlocked) {
                        return;
                    }
                    const dialog = SearchDlg.getSearchDlg({
                        viewName: `${this.viewName}-searc-dlg`,
                        mainTabs: this._module.mainTabView,
                        disableAddButton: this.isDisabledAddButton,
                        baTypId: this.markerInfo.refBaTypeId
                    }, this._module.mainTabView);
                    dialog.showModalContent(this._module,
                        () => {
                            const result = dialog.selectedValue;
                            this._entityBaId = result.baId;
                                                      this.control.setValue(result.name);
                            this.control.validate();
                            dialog.close();

                        });

                },
                onCloseIconClick: () => {
                    if (this.readOnly)
                        return;

                    if (!this._module) {
                        webix.message("Tab module not set", "error");
                        return;
                    }
                    if (this.markerInfo.isBlocked) {
                        return;
                    }
                    //this.isChanged = true;
                    this._entityBaId = null;
                    this.control.setValue("");
                    this.control.validate();
                }
            }
        }, this.getExtConfig(this), true);

    }


}
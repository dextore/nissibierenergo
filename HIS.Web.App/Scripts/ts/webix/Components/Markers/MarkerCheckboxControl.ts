﻿import Base = require("./MarkerControlBase");
import Helper = require("./MarkerHelper");

export function getMarkerCheckboxControl(viewName: string, markerInfo: Markers.IMarkerInfoModel, getExtConfig: (content: any) => any = null): Base.IMarkerControl {
    return new MarkerCheckboxContro(viewName, markerInfo, getExtConfig);
}

class MarkerCheckboxContro extends Base.MarkerControlBase implements Base.IMarkerControl {

    get control() {
        return $$(this.controlId) as webix.ui.checkbox;
    };


    constructor(id: string, markerInfo: Markers.IMarkerInfoModel, getExtConfig: (content: any) => any = null) {
        super(id, markerInfo, getExtConfig);
    }

    public customValidate(): boolean {
        const value = this.control.getValue();
        if (this.readOnly)
            return true;
        if (this.markerInfo.isRequired && !value)
            return false;
        return true;
    }

    protected config() {
        let config = {
            view: "checkbox",
            id: this.controlId,
            label: this.markerInfo.label,
            name: this.markerInfo.name,
            required: this.markerInfo.isRequired,
            readonly: this.readOnly,
            validate: (value) => {
                return this.customValidate();
            },
            on: {
                onChange: (newv, oldv) => {
                    this.control.validate();
                }
            }
        };
        return webix.extend(config, this.getExtConfig(this), true);
    }
}
﻿import Marker = require("./MarkerTextControl");
import MDate = require("./MarkerDateControl");
import MBool = require("./MarkerCheckboxControl");
import Pepiod = require("./PeriodMarkerTextControl");
import PepiodList = require("./PeriodMarkerListControl");
import PepiodAddr = require("./PeriodMarkerAddressControl");
import List = require("./MarkerListControl");
import Addr = require("./AddressRefMarkerControl");
import Entity = require("./EntityRefMarkerControl");
import PepiodEntity = require("./PeriodEntityRefMarkerControl");
import Catalog = require("./MarkerCatalogControl");
import PeriodCatalog = require("./PeriodMarkerCatalogControl");

export class MarkerControlfactory {
    static getControl(viewName: string, markerInfo: Markers.IMarkerInfoModel, getExtConfig: (content: any) => any = null) {
        if (markerInfo.isPeriodic) {
            switch (markerInfo.markerType) {
                case HIS.Models.Layer.Models.Markers.MarkerType.MtBoolean:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtInteger:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtDecimal:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtString:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtMoney:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtDateTime:
                    return Pepiod.getPeriodMarkerControl(viewName, markerInfo, getExtConfig);
                case HIS.Models.Layer.Models.Markers.MarkerType.MtList:
                    return PepiodList.getPeriodMarkerListControl(viewName, markerInfo, getExtConfig);
                case HIS.Models.Layer.Models.Markers.MarkerType.MtEntityRef:
                    return PepiodEntity.getPeriodEntityRefMarkerControl(viewName, markerInfo, getExtConfig);
                case HIS.Models.Layer.Models.Markers.MarkerType.MtAddress:
                    return PepiodAddr.getPeriodMarkerAddressControl(viewName, markerInfo, getExtConfig);
                case HIS.Models.Layer.Models.Markers.MarkerType.MtReference:
                    return PeriodCatalog.getPeriodMarkerCatalogControl(viewName, markerInfo, getExtConfig);
            }
            return null;
        }

        switch (markerInfo.markerType) {
            case HIS.Models.Layer.Models.Markers.MarkerType.MtBoolean:
                return MBool.getMarkerCheckboxControl(viewName, markerInfo, getExtConfig);
            case HIS.Models.Layer.Models.Markers.MarkerType.MtInteger:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtDecimal:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtString:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtMoney:
                return Marker.getMarkerTextControl(viewName, markerInfo, getExtConfig);
            case HIS.Models.Layer.Models.Markers.MarkerType.MtDateTime:
                return MDate.getMarkerDateControl(viewName, markerInfo, getExtConfig);
            case HIS.Models.Layer.Models.Markers.MarkerType.MtList:
                return List.getMarkerListControl(viewName, markerInfo, getExtConfig);
            case HIS.Models.Layer.Models.Markers.MarkerType.MtEntityRef:
                return Entity.getEntityRefMarkerControl(viewName, markerInfo, getExtConfig);
            case HIS.Models.Layer.Models.Markers.MarkerType.MtAddress:
                return Addr.getAddressRefMarkerControl(viewName, markerInfo, getExtConfig);
            case HIS.Models.Layer.Models.Markers.MarkerType.MtReference:
                return Catalog.getMarkerCatalogControl(viewName, markerInfo, getExtConfig);
        }
    }
}
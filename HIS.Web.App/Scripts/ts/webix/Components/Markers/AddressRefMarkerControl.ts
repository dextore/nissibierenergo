﻿import Base = require("./MarkerControlBase");
import Buttons = require("../Buttons/ButtonsExporter");

import SearchAddress = require("../../Components/Address/SearchAddress");

import Helper = require("./MarkerHelper");


export interface IAddressRefMarkerControl extends Base.IMarkerControl {
}

export function getAddressRefMarkerControl(viewName: string, markerInfo: Markers.IMarkerInfoModel, getExtConfig: (content: any) => any = null): IAddressRefMarkerControl {
    return new AddressRefMarkerControl(viewName, markerInfo, getExtConfig);
}

class AddressRefMarkerControl extends Base.MarkerControlBase implements IAddressRefMarkerControl {

    private _addressId: number = null;
    get control() {
        return $$(this.controlId) as webix.ui.text;
    };
    get isChanged(): boolean {

        let currentValue = this._addressId;
        let initValue = this.initValue;

        if (!currentValue) {
            currentValue = null;
        }
        if (!initValue) {
            initValue = null;
        }

        if (currentValue == initValue) {
            return false;
        }

        
        return true;
    };
    
    constructor(id: string, markerInfo: Markers.IMarkerInfoModel, getExtConfig: (content: any) => any) {
        super(id, markerInfo, getExtConfig);
    }

    public customValidate(): boolean {
        const value = this.control.getValue();

        if (this.readOnly)
            return true;

        if (this.markerInfo.isRequired && (!value || !this._addressId))
            return false;

        return true;
    }

    setValueInternal(value: Markers.IMarkerValueModel, model: { [index: string]: any; }) {

        model[this.markerInfo.name] = (!value || !value.displayValue) ? "" : value.displayValue;
        this._addressId = (!value || !value.value) ? null : Number(value.value);

    }

    getValue(model: { [index: string]: any; }): Markers.IMarkerValueModel {

        const value = this.control.getValue();
        const result = {
            displayValue: value,
            endDate: null,
            isBlocked: false,
            isDeleted: false,
            isVisible: true,
            itemId: null,
            markerId: this.markerInfo.id,
            markerType: this.markerInfo.markerType,
            MVCAliase: this.markerInfo.name,
            note: null,
            startDate: null,
            value: (!this._addressId) ? null : this._addressId.toString()
        };
        // проверка на удаление маркера 
        if (!result.value && this.isChanged) {
            result.isDeleted = true;
        }
        return result;
    }

    protected setReadOnly(value: boolean) {
        super.setReadOnly(value);
        this.control.define("readonly", "true");
        this.control.refresh();
    
    }

    config() {
        let config = webix.extend({
            view: this.forFilter ? "text" : "extsearch",
            id: this.controlId,
            label: this.markerInfo.label,
            name: this.markerInfo.name,
            icons: !this.forFilter ? ["search", "close"] : [],
            css: !this.forFilter ? "protoUI-with-icons-2" :  "",
            readonly: !this.forFilter,
            required: this.markerInfo.isRequired,
            validate: (value) => {
                return this.customValidate();
            },
            on: {
                onSearchIconClick: () => {
                    this.selectAddress();
                },
                onCloseIconClick: () => {
                    if (this.readOnly)
                        return;

                    if (this.markerInfo.isBlocked) {
                        return;
                    }
                    this._addressId = null;
                    this.control.setValue("");
                    this.control.validate();
                }
            }
        }, this.getExtConfig(this), true);
        
        return { cols: [config] }
    }
    private selectAddress() {
        if (this.readOnly)
            return;

        const searchDialog = SearchAddress.getSearchAddress(`${this.viewName}-search-address`, null);
        searchDialog.showModal(() => {
            const searchResult: Address.IAddressUpdateResultModel = searchDialog.selected;
            this.control.setValue(searchResult.name);
            this._addressId = searchResult.baId;
            this.control.validate();
            searchDialog.close();
        });
    }
}
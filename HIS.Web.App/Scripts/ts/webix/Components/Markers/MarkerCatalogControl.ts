﻿import Base = require("./MarkerControlBase")
import Helper = require("./MarkerHelper");
import Api = require("../../Api/ApiExporter");
import Loader = require("./CatalogMarkerHelper");

export function getMarkerCatalogControl(viewName: string,
    markerInfo: Markers.IMarkerInfoModel,
    getExtConfig: (content: any) => any = null): Base.IMarkerControl {
    return new MarkerCatalogControl(viewName, markerInfo, getExtConfig);
}

export interface ICatalogModel {
    showHeader: boolean;
    columns:ICatalogColumns[];
}
export interface ICatalogColumns {
    id: string;
    hidden: boolean;
    header:string;
}

class MarkerCatalogControl extends Base.MarkerControlBase implements Base.IMarkerControl {

    get control() {
        return $$(this.controlId) as webix.ui.richselect;
    };

    get bodyId() {
        return `${this.viewName}--${this.markerInfo.name}-gridsuggest-body-id`;
    } 

    get gridsuggestId() {
        return `${this.viewName}--${this.markerInfo.name}-gridsuggest-id`;
    } 

    constructor(viewName: string,
        markerInfo: Markers.IMarkerInfoModel,
        getExtConfig: (content: any) => any, model?:ICatalogModel) {
        super(viewName, markerInfo, getExtConfig);
    }

    public customValidate(): boolean {
    
        const value = this.control.getValue();

        if (this.readOnly)
            return true;

        if (this.markerInfo.isRequired && !value)
            return false;
        if (this.markerInfo.isRequired && (value === "0")) {
            return false;
        }

        return true;
    }


    protected config() {
        const config = webix.extend({
            view: "richselect",
            id: this.controlId,
            label: this.markerInfo.label,
            name: this.markerInfo.name,
            readonly: this.forFilter ? false : this.readOnly,
            required: this.markerInfo.isRequired,
            validate: (value) => {
                return this.customValidate();
            },
            options: {
                view: "gridsuggest",
                id: this.gridsuggestId,
                //textValue: "name",
                textValue: "ItemName",
                resize: true,
                width: 500, // ТО DO не убирать, fix первого показа всплывашки 
                body: {
                    id: this.bodyId,
                    //autoConfig: true,
                    //header: false,
                    scroll: true,
                    //autoheight: false,
                    autowidth: true,
                    autoheight: false,
                    autofocus: true,
                    yCount: 5,
                    borderless: true,
                    columns: [],
                    //columns: [
                    //    { id: "id", header: "id", hidden: true },
                    //    { id: "nullValue", header: "nullValue", hidden: true },
                    //    { id: "name", header: "name", width: 350 },
                    //    { id: "shortName", header: "shortName", width: 150 }
                    //],
                    url: {
                        $proxy: true,
                        load: (view, callback, params) => {
                            Loader.CatalogMarkerHelper.dataToControl(view,
                                this.markerInfo);

                        }
                    }
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj.nullValue) {
                            ($$(this.controlId) as webix.ui.richselect).setValue(null);
                        }
                    },
                    onBeforeShow: () => {
                        Loader.CatalogMarkerHelper.setGridsuggestWidtn(
                            $$(this.controlId) as webix.ui.richselect,
                            $$(this.gridsuggestId) as webix.ui.gridsuggest,
                            $$(this.bodyId) as webix.ui.datatable);
                    },
                    onShow: () => {
                        const width = ($$(this.controlId) as webix.ui.richselect).getInputNode().clientWidth;
                        const ctrl = ($$(this.gridsuggestId) as webix.ui.gridsuggest);
                        ctrl.$setSize(ctrl.$height, width);
                    }
                }
            },
            on: {
                onChange: (newv, oldv) => {
                    this.control.validate();
                }
            }
        }, this.getExtConfig(this), true);

        return config;
    }

    setReadOnly(value: boolean): void {
        super.setReadOnly(value);
        if ($$(this.controlId)) {
            value ? $$(this.controlId).disable() : $$(this.controlId).enable();
        }
    }
}
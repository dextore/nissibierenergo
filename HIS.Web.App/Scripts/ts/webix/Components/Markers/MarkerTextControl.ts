﻿import Base = require("./MarkerControlBase");

export function getMarkerTextControl(viewName: string, markerInfo: Markers.IMarkerInfoModel, getExtConfig: (content: any) => any = null): Base.IMarkerControl {
    return new MarkerTextControl(viewName, markerInfo, getExtConfig);
}

class MarkerTextControl extends Base.MarkerControlBase implements Base.IMarkerControl {

    get control() {
        return $$(this.controlId) as webix.ui.text;
    };

    constructor(id: string, markerInfo: Markers.IMarkerInfoModel, getExtConfig: (content: any) => any = null) {
        super(id, markerInfo, getExtConfig);
    }

    public customValidate(): boolean {
        if (this.readOnly)
            return true;
        const value = this.control.getValue();
        if (this.markerInfo.isRequired && !value)
            return false;
        return true;
    }

    protected config() {

        let config = {
            view: "textExtended",
            id: this.controlId,
            label: this.markerInfo.label,
            name: this.markerInfo.name,
            required: this.markerInfo.isRequired,
            readonly: this.readOnly,
            validate: (value) => {
                return this.customValidate();
            },
            on: {
                onChange: (newv, oldv) => {
                    this.control.validate();
                }
            }
        };
        return webix.extend(config, this.getExtConfig(this), true);
    }
}
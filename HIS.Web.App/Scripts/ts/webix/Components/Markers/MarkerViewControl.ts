﻿import Api = require("../../Api/ApiExporter");
import History = require("./PeriodMarkerHistoryDlg");
import Tab = require("../../MainView/Tabs/MainTabBase");

export interface IMarkerViewControl {
    init(): any;
    setModelValue(markerValue: Markers.IMarkerValueModel, baId: number, model: any);
} 


export function getMarkerViewControl(viewName: string, markerInfo: Markers.IMarkerInfoModel, labelWidth: number = null, multilain: boolean = false, module: Tab.IMainTab) {
    return new MarkerViewControl(viewName, markerInfo, labelWidth, multilain, module);
}


class MarkerViewControl implements IMarkerViewControl {

    private _baId: number = null;
    private _periodDateWidth: 120;
    private _dateFormat = webix.Date.dateToStr("%d.%m.%Y", false);
    get viewName() {
        return `${this._viewName}_marker_view`;
    }

    constructor(private readonly _viewName: string,
        private readonly _markerInfo: Markers.IMarkerInfoModel,
        private readonly _labelWidth: number,
        private readonly _multilain: boolean,
        private readonly _module: Tab.IMainTab) {
    }

    setModelValue(markerValue: Markers.IMarkerValueModel, baId: number, model: any) {
        this._baId = baId;
        switch (this._markerInfo.markerType) {
            case HIS.Models.Layer.Models.Markers.MarkerType.MtBoolean:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtInteger:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtDecimal:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtString:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtMoney:
                model[this._markerInfo.name] = markerValue.value;
                break;
            case HIS.Models.Layer.Models.Markers.MarkerType.MtDateTime:
                model[this._markerInfo.name] = markerValue ? this._dateFormat(markerValue.value) : null;
                break;
            case HIS.Models.Layer.Models.Markers.MarkerType.MtList:
                model[this._markerInfo.name] = markerValue ? markerValue.displayValue : null;                
                break;
            case HIS.Models.Layer.Models.Markers.MarkerType.MtEntityRef:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtAddress:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtReference:
                model[this._markerInfo.name] = markerValue ? markerValue.displayValue : null;                
                break;
        }
        if (this._markerInfo.isPeriodic) {
            model[this.getPeriodDateName(this._markerInfo)] = this._dateFormat(markerValue.startDate);
        }
    }

    init() {
        const cols = this.initLableConfig(this._markerInfo);
        if (this._markerInfo.isPeriodic) {
            (cols as any[]).push(this.initPeriodDate(this._markerInfo), this.initPeriodBtn());
        }

        return {
            cols: cols
        }
    }

    private getPeriodDateName(markerInfo: Markers.IMarkerInfoModel) {
        return `${markerInfo.name}_date`;
    }

    private btnClick() {
        if (!this._baId)
            return;

        const dlg = History.getPeriodMarkerHistoryDlg(this.viewName,
            this._baId,
            this._markerInfo,
            true,
            this._module);

        dlg.showModal(() => { dlg.close(); });
    }

    private initLableConfig(markerInfo: Markers.IMarkerInfoModel) {
        if (!this._multilain) {
            return [
                {
                    view: "label",
                    label: markerInfo.label,
                    width: Boolean(this._labelWidth) ? this._labelWidth : 60
                }, {
                    view: "label",
                    align: "left",
                    css: { "font-weight": "bold" },
                    name: markerInfo.name
                }
            ];
        }
        return [
                {
                    rows: [{
                            view: "label",
                            label: markerInfo.label,
                            width: Boolean(this._labelWidth) ? this._labelWidth : 60
                    }, {height:  20}
                    ]
            }, {
                    view: "textarea",
                    height: 30,
                    css: "textarea_view_control",
                    borderless: true,
                    readonly: true,
                    name: markerInfo.name
            }
        ];
    }

    private initPeriodDate(markerInfo: Markers.IMarkerInfoModel) {
        return {
            view: "label",
            align: "left",
            css: { "font-weight": "bold" },
            name: this.getPeriodDateName(markerInfo),
            width: this._periodDateWidth
        }
    }

    private initPeriodBtn() {
        return {
            view: "button",
            type: "iconButton",
            icon: "angle-double-down",
            width: 28,
            on: {
                onItemClick: (id, e) => { this.btnClick() }
            }
        };
    }

} 
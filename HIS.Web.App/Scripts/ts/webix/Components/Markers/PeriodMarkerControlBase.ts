﻿import Base = require("./MarkerControlBase")
import Buttons = require("../Buttons/ButtonsExporter");
import History = require("./PeriodMarkerHistoryDlg");
import Tab = require("../../MainView/Tabs/MainTabBase");
import Helper = require("./MarkerHelper");
import Api = require("../../Api/ApiExporter");


export interface IPeriodMarkerControl extends Base.IMarkerControl {
    baId: number;
    initDate:Date;
    isChangeDate;
  
}

export abstract class PeriodMarkerControlBase extends Base.MarkerControlBase implements IPeriodMarkerControl {

    private _baId: number;
    private _date: Date = null;
    private _initDate: any = null;

    get initDate(): Date {
        return this._initDate;
    };

    private get datePickerId() {
        return `${this.viewName}-${this.markerInfo.name}-datepicker-id`;
    }

    private readonly _historyBtn: Buttons.IButton;

    get control() {
        return $$(this.controlId);
    };
    get datePickerControl() {
        return $$(this.datePickerId) as webix.ui.datepickerExtended;
    };

    get selectButtonControl() {
        return this._historyBtn.button;
    };
    
    private  _tabModule: Tab.IMainTab = null;
    protected setTabModule(module: Tab.IMainTab) {
        if (!module)
            return;
        this._tabModule = module;
    }

    protected setVisible(value: boolean): void {
        value
            ? $$(this.controlId).getParentView().getParentView().show()
            : $$(this.controlId).getParentView().getParentView().hide();
        //super.setVisible(value);
        //if (value) {
        //    this.datePickerControl.show();
        //    this._historyBtn.button.show();
        //} else {
        //    this.datePickerControl.hide();
        //    this._historyBtn.button.hide();
        //}
    }

    protected setReadOnly(value: boolean) {
        super.setReadOnly(value);
        if (this.datePickerControl) {
            const readonly = value ? "true" : "";
            this.datePickerControl.define("readonly", readonly);
            this.datePickerControl.refresh();
        }
        //(value) ? this._historyBtn.button.disable() : this._historyBtn.button.enable();
    }

    protected constructor(viewName: string,
        markerInfo: Markers.IMarkerInfoModel,
        getExtConfig: (content: any) => any) {
        super(`${viewName}-${markerInfo.name}`, markerInfo, getExtConfig);
        this._historyBtn = Buttons.getPeriodMarkerHistoryButton(this.viewName, false, (id, e) => { this.showHistory() });
    }

    get baId() {
        return this._baId;
    }

    set baId(value: number) {
        this._baId = value;
    }

    setInitValue(value: Markers.IMarkerValueModel, model: { [id: string]: any }) {
        this._initValue = (!value) ? null : value.value;
        this._initDate = (!value) ? null : value.startDate;
        this.setValueInternal(value, model);
    }
    setValue(value: Markers.IMarkerValueModel, model: { [id: string]: any }) {
        this.setValueInternal(value, model);
    }

    get initValue() {
        return this._initValue;
    };

    get isChanged(): boolean {

        if (this.isChangeDate || this.isChangeValue) {
            return true;
        }
        return false;
    };

    get isChangeValue() {

        let currentValue = (this.control as any).getValue() as any;
        let initValue = this._initValue;

        if (!currentValue) {
            currentValue = null;
        }
        if (!initValue) {
            initValue = null;
        }
        if (currentValue == initValue) {
            return false;
        }
        return true;

    }

    get isChangeDate() {
        let currentDate = this.datePickerControl.getValue();
        let initDate = this._initDate;
        if (!currentDate) {
            currentDate = null;
        }
        if (!initDate) {
            initDate = null;
        }
        // если оба нулевые
        if (initDate == currentDate) {
            return false;
        }
        if (Date.parse(currentDate) == Date.parse(initDate)) {
            return false;
        }
        return true;
    }


    setValueInternal(value: Markers.IMarkerValueModel, model: { [index: string]: any; }) {
      
        super.setValueInternal(value, model);
        this._date = (!value) ? null : value.startDate;
        model[`${this.markerInfo.name}_date`] = this._date;
        model[`${this.markerInfo.name}_note`] = (!value) ? null : value.note;
        model[`${this.markerInfo.name}_display`] = (!value) ? null : value.displayValue;
    }

    getValue(model: { [index: string]: any; }): Markers.IMarkerValueModel {

        let value = model[this.markerInfo.name];

        const result = {
            markerType: this.markerInfo.markerType,
            displayValue: model[`${this.markerInfo.name}_display`],
            MVCAliase: this.markerInfo.name,
            itemId: null,
            endDate: null,
            markerId: this.markerInfo.id,
            note: model[`${this.markerInfo.name}_note`],
            isBlocked: false,
            isVisible: true,
            startDate: model[`${this.markerInfo.name}_date`],
            value: value,
            isDeleted: false
        };

        if (this.isChanged && !result.value) {

            if (!result.startDate) {
                result.startDate = this._initDate;
            }
            result.isDeleted = true;
        }
        return result;
    }

    private showHistory() {

        if (!this._baId)
            return;
       
        const dlg = History.getPeriodMarkerHistoryDlg(this.viewName,
            this._baId,
            this.markerInfo,
            this.readOnly,
            this._tabModule);

        dlg.showModal(() => {
            dlg.close();
            (!this.readOnly) && Api.getApiMarkers().getEntityMarkerValues({ baId: this.baId, markerIds: [this.markerInfo.id], names: [] })
                .then(
                result => {
                    this.updateValue(result[0]);
                });
        });
       
    }

    protected updateValue(value: Markers.IMarkerValueModel) {
        const form = (this.control as any).getFormView() as webix.ui.form;
        if (form) {
            const formData = form.getValues();
            this.setValueInternal(value, formData);
            form.setValues(formData);
            return;
        }
        (this.control as any).setValue(value.value);
        (this.datePickerControl as any).setValue(value.startDate);
    }

    disable() {
        const ctrl = this.control;
        if (ctrl)
            ctrl.disable();

        const datePicker = this.datePickerControl;
        if (datePicker) {
            datePicker.disable();
            datePicker.setValue(new Date() as any);
        }

        const btn = this._historyBtn.button;
        if (btn)
            btn.disable();
    }

    enable() {
        const ctrl = this.control;
        if (ctrl)
            ctrl.enable();

        const datePicker = this.datePickerControl;
        if (datePicker) {
            datePicker.enable();
            datePicker.setValue(new Date() as any);
        }

        const btn = this._historyBtn.button;
        if (btn)
            btn.enable();
    }

    init(filter: boolean = false) {
      
        const dataConfig = this.dataConfig();
       
        return {
            cols: this.forFilter
                ? [super.init()]
                : [super.init(), dataConfig, this._historyBtn.init()]
        }
    }

    protected dataConfig() {
        return {
            view: "datepickerExtended",
            id: this.datePickerId,          
            name: `${this.markerInfo.name}_date`,
            width: 120,
            pattern: { mask: "##.##.####", allow: /[0-9]/g },
            readonly: this.readOnly,
            required: this.markerInfo.isRequired,
            validate: (value) => {
                if (this.readOnly)
                    return true;
                const valueControl = ($$(this.controlId) as any).getValue();
                if (this.markerInfo.isRequired) {

                    if (!value) {
                        return false;
                    }
                    if (!this.datePickerControl.getValidateTextValue()) {
                        return false;
                    }
                    if (this.markerInfo.markerType === HIS.Models.Layer.Models.Markers.MarkerType.MtList &&
                        valueControl === -1) {
                        return false;
                    }

                }
                if (valueControl && this.markerInfo.markerType === HIS.Models.Layer.Models.Markers.MarkerType.MtList) {
                    if (valueControl !== -1 && valueControl !== "empty value" && !value) {
                        return false;
                    }

                } else if (valueControl && !value && valueControl !== -1 && valueControl !== "empty value") {
                    return false;
                }
                return true;


            },
            on: {
                onChange: (newv, oldv) => {
                    this.datePickerControl.validate();
                },
                onBlur: () => {
                    this.datePickerControl.validate();
                }
            }
        };
    }
}

﻿import Base = require("../ComponentBase");

export interface IMarkerControl extends Base.IComponent {
    init(): any; 
    control: webix.ui.baseview;
    readOnly: boolean;
    disable();
    enable();
    visible: boolean;
    markerInfo: Markers.IMarkerInfoModel;
    setInitValue(value: Markers.IMarkerValueModel, model: { [id: string]: any });
    setValue(value: Markers.IMarkerValueModel, model: { [id: string]: any });
    getValue(model: { [id: string]: any }): Markers.IMarkerValueModel;
    initValue(): any;
    isChanged: boolean;
    forFilter: boolean;
}

export abstract class MarkerControlBase extends Base.ComponentBase implements IMarkerControl {

    private _readOnly: boolean = false;
    private _isChanged: boolean = false;
    private _forFilter: boolean = false;
    // 
    protected _initValue: any = null;

    protected get controlId(): string {
        return `${this.viewName}-${this.markerInfo.name}-body-id`;
    };

    protected get viewName(): string {
        return this._viewName;
    };

    get markerInfo(): Markers.IMarkerInfoModel {
        return this._markerInfo;
    }

    get control() {
        return $$(this.controlId);
    };

    get forFilter() {
        return this._forFilter;
    }

    set forFilter(value: boolean) {
        this._forFilter = value;
    }

    get visible(): boolean {
        return $$(this.controlId).isVisible();
    }

    set visible(value: boolean) {
        if (value === $$(this.controlId).isVisible())
            return;
        this.setVisible(value);
    }

    protected getExtConfig(content: any): any {
        if (!this._getExtConfig)
            return {};
        const config = this._getExtConfig(content);
        return (!config) ? {} : config;
    };

    protected constructor(private readonly _viewName: string,
        private readonly _markerInfo: Markers.IMarkerInfoModel,
        private readonly _getExtConfig: (content: any) => any) {
        super();
    }

    protected abstract config(): any;

    protected setVisible(value: boolean) {
        value ? $$(this.controlId).show() : $$(this.controlId).hide();
    }

    get readOnly() {
        return this._readOnly;
    }

    set readOnly(value: boolean) {
        this.setReadOnly(value);
    }

    protected setReadOnly(value: boolean) {
        this._readOnly = value;
        if ($$(this.controlId)) {
            const readonly = value ? true : "";
            $$(this.controlId).define("readonly", readonly);
            ($$(this.controlId) as any).refresh();

            //if (value) {
            //    this._tempPattern = ($$(this.controlId).config as any).pattern;
            //    if (this._tempPattern) {
            //        $$(this.controlId).define("pattern", {});
            //        ($$(this.controlId) as any).refresh();
            //    }
            //} else {
            //    if (this._tempPattern) {
            //        $$(this.controlId).define("pattern", this._tempPattern);
            //        ($$(this.controlId) as any).refresh();
            //        this._tempPattern = null;
            //    }
            //}
        }
    }


    init() {
        return this.config();
    }

    setInitValue(value: Markers.IMarkerValueModel, model: { [id: string]: any }) {
        this._initValue = (!value) ? null : value.value;
        this.setValueInternal(value, model);
    }
    setValue(value: Markers.IMarkerValueModel, model: { [id: string]: any }) {
        this.setValueInternal(value, model);
    }

    setValueInternal(value: Markers.IMarkerValueModel, model: { [index: string]: any; }) {
        model[this.markerInfo.name] = (!value) ? null : value.value;
    }

    getValue(model: { [index: string]: any; }): Markers.IMarkerValueModel {

        const value = ($$(this.controlId) as any).getValue();
        return {
            displayValue: null,
            markerType: this.markerInfo.markerType,
            MVCAliase: this.markerInfo.name,
            itemId: null,
            endDate: null,
            markerId: this.markerInfo.id,
            note: null,
            isVisible: true,
            isBlocked: false,
            startDate: null,
            value: value,
            isDeleted: this.isChanged && !value
        };
    }

    get initValue() {
        return this._initValue;
    };

    get isChanged(): boolean {

        let currentValue = (this.control as any).getValue() as any;
        let initValue = this._initValue;
        if (!currentValue || currentValue == "empty value") {
            currentValue = null;
        }
        if (!initValue) {
            initValue = null;
        }
        if (currentValue == initValue) {
            return false;
        }
        /// сравнение двух дат 
        if (Date.parse(currentValue) == Date.parse(initValue)) {
            return false;
        }
        return true;
    };

    set isChanged(value: boolean) {
        this._isChanged = value;
    };

    disable() {
        const ctrl = this.control;
        if (ctrl)
            ctrl.disable();
    }

    enable() {
        const ctrl = this.control;
        if (ctrl)
            ctrl.enable();
    }
}
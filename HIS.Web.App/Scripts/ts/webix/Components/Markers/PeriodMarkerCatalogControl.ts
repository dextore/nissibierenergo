﻿import Base = require("./PeriodMarkerControlBase")
import Helper = require("./MarkerHelper");
import Api = require("../../Api/ApiExporter");
import Loadaer = require("./CatalogMarkerHelper");

export function getPeriodMarkerCatalogControl(viewName: string,
    markerInfo: Markers.IMarkerInfoModel,
    getExtConfig: (content: any) => any = null): Base.IPeriodMarkerControl  {
    return new PeriodMarkerCatalogControl(viewName, markerInfo, getExtConfig);
}

class PeriodMarkerCatalogControl extends Base.PeriodMarkerControlBase implements Base.IPeriodMarkerControl {

    get bodyId() {
        return `${this.viewName}--${this.markerInfo.name}-gridsuggest-body-id`;
    } 

    get gridsuggestId() {
        return `${this.viewName}--${this.markerInfo.name}-gridsuggest-id`;
    } 

    get control() {
        return $$(this.controlId) as webix.ui.richselect;
    };

    constructor(viewName: string,
        markerInfo: Markers.IMarkerInfoModel,
        getExtConfig: (content: any) => any) {
        super(viewName, markerInfo, getExtConfig);
    }

    public customValidate(): boolean {

        if (this.readOnly)
            return true;

        let value = this.control.getValue();
        const dateValue = this.datePickerControl.getValue();

        if (value == "empty value")
            value = null;

        if (this.markerInfo.isRequired && !value)
            return false;

        if (!value && !!dateValue && this.datePickerControl.validate()) {
            return false;
        }
        if (!!value && !dateValue) {
            this.datePickerControl.validate();
        }
        return true;
    }

    protected config() {
        const config = webix.extend({
            view: "richselect",
            id: this.controlId,
            label: this.markerInfo.label,
            name: this.markerInfo.name,
            readonly: this.readOnly,
            required: this.markerInfo.isRequired,
            validate: (value) => {
                return this.customValidate();
            },
            options: {
                view: "gridsuggest",
                id: this.gridsuggestId,
                //textValue: "name",
                //textValue: "itemName",
                textValue: "ItemName",
                resize: true,
                width: 500, // ТО DO не убирать, fix первого показа всплывашки 
                body: {
                    id: this.bodyId,
                    scroll: true,
                    autowidth: true,
                    autoheight: false,
                    autofocus: true,
                    yCount: 5,
                    borderless: true,
                    columns: [],
                    url: {
                        $proxy: true,
                        load: (view, callback, params) => {
                            Loadaer.CatalogMarkerHelper.dataToControl(view, this.markerInfo);
                        }
                    }
                },
                on: {
                    onValueSuggest: (obj) => {
                        if (obj.nullValue) {
                            this.control.setValue(null);
                            this.control.validate();
                            this.datePickerControl.validate();
                        }
                    },
                    onBeforeShow: () => {
                        Loadaer.CatalogMarkerHelper.setGridsuggestWidtn(
                            $$(this.controlId) as webix.ui.richselect,
                            $$(this.gridsuggestId) as webix.ui.gridsuggest,
                            $$(this.bodyId) as webix.ui.datatable);
                    },
                    onShow: () => {
                        const width = ($$(this.controlId) as webix.ui.richselect).getInputNode().clientWidth;
                        const ctrl = ($$(this.gridsuggestId) as webix.ui.gridsuggest);
                        ctrl.$setSize(ctrl.$height, width);
                    }
                }
            },
            on: {
                onChange: (newv, oldv) => {
                    this.control.validate();
                }
            }
        }, this.getExtConfig(this), true);

        return config;
    }
}
﻿import Base = require("../../Common/DialogBase");
import Tab = require("../../MainView/Tabs/MainTabBase");

export interface IResultModel {
    baId: number;
    name: string;
}

export interface IEntityRefSelectDlg extends Base.IDialog {
    showSelect(parentBaId: number, module: Tab.IMainTab, callBack?: () => void);
    selectedValue: IResultModel;
}

export abstract class EntityRefSelectDlgBase extends Base.DialogBase implements IEntityRefSelectDlg {

    protected abstract getSelected(): IResultModel;

    get selectedValue(): IResultModel {
        return this.getSelected();
    }

    abstract showSelect(parentBaId: number, module: Tab.IMainTab, callBack?: () => void);
}
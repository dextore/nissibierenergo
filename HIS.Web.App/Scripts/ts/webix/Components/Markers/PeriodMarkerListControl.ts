﻿import Base = require("./PeriodMarkerControlBase")
import Helper = require("./MarkerHelper");

export function getPeriodMarkerListControl(viewName: string, markerInfo: Markers.IMarkerInfoModel, getExtConfig: (content: any) => any = null): Base.IPeriodMarkerControl {
    return new PeriodMarkerListControl(viewName, markerInfo, getExtConfig);
}

class PeriodMarkerListControl extends Base.PeriodMarkerControlBase implements Base.IPeriodMarkerControl {
    
    get control() {
        return $$(this.controlId) as webix.ui.select;
    };

    constructor(viewName: string,
        markerInfo: Markers.IMarkerInfoModel,
        getExtConfig: (content: any) => any) {
        super(viewName, markerInfo, getExtConfig);
    }

    get isChanged(): boolean {
   
        let currentValue = (this.control as any).getValue() as any;
        let initValue = this._initValue;

        if (!currentValue || currentValue === -1) {
            currentValue = null;
        }
        if (!initValue) {
            initValue = null;
        }
     
        if (this.isChangeDate || (currentValue !== initValue)) {
            return true;
        }
        return false;
    };

    getValue(model: { [index: string]: any; }): Markers.IMarkerValueModel {

        const markerValue = super.getValue(model);

        if (markerValue.value === "-1" || (markerValue.value as any) === -1) {
            markerValue.value = null;
        }

        if (this.isChanged && (!markerValue.value || markerValue.value === "-1" || (markerValue.value as any) === -1)) {

            markerValue.value = null;
            markerValue.isDeleted = true;
            if (!markerValue.startDate) {
                markerValue.startDate = this.initDate;
            }

        }
   
        return markerValue;
    }

    protected setReadOnly(value: boolean) {
        super.setReadOnly(value);
        //(value) ? this.control.disable() : this.control.enable();
    }

    public customValidate(): boolean {

        const value = this.control.getValue() as any;
        const dateValue = this.datePickerControl.getValue();

        if (this.readOnly)
            return true;

        if (this.markerInfo.isRequired && !value)
            return false;

        if (!value && !!dateValue && this.datePickerControl.validate()) {
            return false;
        }
        if (!!value && value !== -1 && !dateValue) {
            this.datePickerControl.validate();
        }
        this.datePickerControl.validate();
        return true;
    }


    protected config() {

        const config = webix.extend({
            view: "combo",
            id: this.controlId,
            label: this.markerInfo.label,
            name: this.markerInfo.name,
            //css: "contract_marker_redefained",
            options: {
                filter: (item, value) => {
                    return true;
                },
                body: {
                    data: Helper.markerHelper.convertListForOptions(this.markerInfo.list)
                }
            },
            readonly: this.readOnly,
            required: this.markerInfo.isRequired,
            validate: (value) => {
                return this.customValidate();
            },
            on: {
                onChange: (newv, oldv) => {
                    this.control.validate();
                },
                onKeyPress: () => {
                    return false;
                }
            }
        }, this.getExtConfig(this), true);

        return config;
    }
}
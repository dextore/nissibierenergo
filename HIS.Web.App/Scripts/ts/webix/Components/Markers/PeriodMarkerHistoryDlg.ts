﻿import Base = require("../../Common/DialogBase");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import ToolBtn = require("../Buttons/ToolBarButtons");
import Api = require("../../Api/ApiExporter");
import Helpers = require("../Helpers/DataTableHelpers");
import Addr = require("../../Components/Address/SearchAddress");
import Tab = require("../../MainView/Tabs/MainTabBase");
import EntitySearch = require("../Forms/EntitySearch/EntitySearchDlg");
import SearchDlg = require("../../MainView/UniversalSearch/SearchDlg");
import BA = require("../../Common/BATypes");
import Loader = require("./CatalogMarkerHelper");
import Common = require("../../Common/CommonExporter");
export interface IPeriodMarkerHistoryDlg extends Base.IDialog {
    current: Markers.IPeriodicMarkerValueModel;
}

export function getPeriodMarkerHistoryDlg(viewName: string, baId: number, markerInfo: Markers.IMarkerInfoModel, readOnly: boolean, module: Tab.IMainTab): IPeriodMarkerHistoryDlg {
    return new PeriodMarkerHistoryDlg(viewName, baId, markerInfo, readOnly , module);
}

class PeriodMarkerHistoryDlg extends Base.DialogBase implements IPeriodMarkerHistoryDlg {
    rendered() {}

    private _removedRows: IMarkerValueModelChanged[] = []; 
  
    private _okBtn = Btn.getOkButton(`${this.viewName}-ok`, () => { this.okClose(); });
    private _cancelBtn = Btn.getCancelButton(`${this.viewName}-cancel`, () => { this.cancelClose(); });
    private _closeButton = Btn.getCloseButton(`${this.viewName}-close`, () => { this.cancelClose(); });
 
    private readonly _load = Api.getApiMarkers().getPeriodicMarkerHistory;
    private readonly _save = Api.getApiMarkers().saveMarkerHistory;

    private _toolBarButtons = {
        add: ToolBtn.getNewBtn(this.viewName, "Добавить маркер", (id, e) => { this.addMarker(); }),
        remove: ToolBtn.getRemoveBtn(this.viewName, "Удалить маркер", (id, e) => { this.removeMarker(); })
    }

    private _current: Markers.IPeriodicMarkerValueModel;

    get current() { return this._current; };

    get dataTableId(): string {
   
        return `${this.viewName}-datatable-id`;
    }
    
    headerLabel(): string { return "Редактирование периодичного праметра"; }

    constructor(viewName: string,
        private readonly _baId: number,
        private readonly _markerInfo: Markers.IMarkerInfoModel,
        private readonly _readOnly: boolean,
        private readonly _module: Tab.IMainTab) {
        super(`${viewName}-history-dlg`);
    }

  
    private addMarker() {
        const idx = ($$(this.dataTableId) as webix.ui.datatable).add({isNew: true});
        ($$(this.dataTableId) as webix.ui.datatable).select(idx, false);
        ($$(this.dataTableId) as webix.ui.datatable).showItem(idx);
    }

    private removeMarker() {
        const selectedRowIds = ($$(this.dataTableId) as webix.ui.datatable).getSelectedId(true, false);

        if (!selectedRowIds.length)
            return;

        webix.alert({
            text: "Маркер будет удален.",
            type: "alert-warning",
            ok: "Да",
            cancel: "Нет",
            callback: (result) => {
                if (!result) {
                    return;
                }
                const row = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true)[0] as IMarkerValueModelChanged;
                if (!row.isNew)
                    this._removedRows.push(row);
                ($$(this.dataTableId) as webix.ui.datatable).remove(selectedRowIds[0]);
            }
        });
    }

    private getProxy() {
        return {
            $proxy: true,
            load: (view, callback, params) => {
                this._load(this._baId, this._markerInfo.id).then(data => {

                    if (this._markerInfo.markerType === HIS.Models.Layer.Models.Markers.MarkerType.MtReference) {



                        Api.getApiMarkers().getCatalogMarkerValueItems(this._markerInfo.id).then(result => {

                            const items = JSON.parse(result);

                            data.forEach(dataItem => {
                                let filtered = items.filter(item => {
                                    return item.ItemId.toString() === dataItem.value;
                                });
                                if (filtered.length)
                                    dataItem.displayValue = filtered[0].ItemName;
                            });

                            (webix.ajax as any).$callback(view, callback, data);
                            Helpers.resetButtons(true, false, this._toolBarButtons);
                            this.checkReadOnly();
                        });
                    } else {
                        (webix.ajax as any).$callback(view, callback, data);
                        Helpers.resetButtons(true, false, this._toolBarButtons);
                        this.checkReadOnly();
                    }
                });
            }
        }
    }

    protected okClose() {
        if (!($$(this.dataTableId) as webix.ui.datatable).validate())
            return;

        const rows = Helpers.dataTableRows($$(this.dataTableId) as webix.ui.datatable) as IMarkerValueModelChanged[]; 

        const changes: Markers.IMarkeHistoryChangesModel = {
            baId: this._baId,
            markerId: this._markerInfo.id,
            changes: rows.filter(item => { return item.changed }).map(item => {
                return {
                    displayValue: item.displayValue,
                    endDate: item.endDate,
                    note: item.note,
                    startDate: item.startDate,
                    value: item.value
                }
            }),
            removed: this._removedRows.map(item => {
                return {
                    displayValue: item.displayValue,
                    endDate: item.endDate,
                    note: item.note,
                    startDate: item.startDate,
                    value: item.value
                };
            }) 
        }

        this._save(changes).then(data => {
            this._current = data;
            super.okClose();
          
        });
    }

    private saveMarkerChanges() {
    }

// START datatable handlers 
    private selectionChanges() {
        Helpers.resetButtons(true, ($$(this.dataTableId) as webix.ui.datatable).getSelectedId(true, false).length > 0, this._toolBarButtons);
        this.checkReadOnly();
    }

    private afterEditItem(state, editor) {
        if (state.old instanceof Date) {
            if (state.value != null && ((state.old as Date).getTime() === (state.value as Date).getTime()))
                return;
        }

        if (state.old === state.value)
            return;

        //webix.message("Запись была изменена");
        if (($$(this.dataTableId) as webix.ui.datatable).validate(editor.row)) {
            const row = ($$(this.dataTableId) as webix.ui.datatable).getItem(editor.row) as IMarkerValueModelChanged;
            if (row.changed)
                return;

            row.changed = true;
            ($$(this.dataTableId) as webix.ui.datatable).updateItem(editor.row, row);
        }
    }
// END datatable handlers 

    contentConfig() {
        const self = this;

        return {
            rows: [
                toolbarConfig(),
                dataTableConfig(),
                buttonToolBarConfig(),
                { height: 15 }
            ]
        };

        function toolbarConfig() {
            return {
                view: "toolbar",
                paddingY: 1,
                elements: [
                    {},
                    self._toolBarButtons.add.init(),
                    self._toolBarButtons.remove.init()
                ]
            }
        }

        function dataTableConfig() {
            
            return {
                view: "datatable",
                id: self.dataTableId,
                select: "row",
                resizeColumn: true,
                editable: !self._readOnly,
                columns: [
                    getValueColum(),
                    {
                        id: `startDate`,
                        header: "Дата с..",
                        sort: "date",
                        editor: "dateExtended",
                        format: webix.Date.dateToStr("%d.%m.%Y", false)
                    },
                    {
                        id: `endDate`,
                        header: "Дата по..",
                        sort: "date",
                        format: webix.Date.dateToStr("%d.%m.%Y", false)
                    },
                    {
                        id: `note`,
                        header: "Примечание",
                        sort: "string",
                        editor: "popup",
                        fillspace: true
                    }
                ],
                rules: {
                    startDate: webix.rules.isNotEmpty,
                    value: webix.rules.isNotEmpty
                },
                url: self.getProxy(),
                on: {
                    onSelectChange: () => { self.selectionChanges(); },
                    onAfterEditStop: (state, editor, ignoreUpdate: boolean) => { self.afterEditItem(state, editor); }
                },
                onClick: {
                    "fa-search": (e, id, node) => {
                        if (self._readOnly)
                            return;
                        switch (self._markerInfo.markerType) {
                            case HIS.Models.Layer.Models.Markers.MarkerType.MtAddress:
                                const dlg = Addr.getSearchAddress(`${self.viewName}-search-address`, null);
                                dlg.showModal(() => {
                                    const searchResult: Address.IAddressUpdateResultModel = dlg.selected;

                                    const item =
                                        ($$(self.dataTableId) as webix.ui.datatable).getItem(id) as Markers.IMarkerValueModel;
                                    item.displayValue = searchResult.name;
                                    item.value = searchResult.baId.toString();
                                    ($$(self.dataTableId) as webix.ui.datatable).updateItem(id, item);
                                    dlg.close();
                                });
                                break;
                            case HIS.Models.Layer.Models.Markers.MarkerType.MtEntityRef:

                                /*
                                const esDialog = SearchDlg.getSearchDlg({
                                    viewName: `${self.viewName}-searc-dlg`,
                                    disableAddButton: true,
                                    baTypId: self._markerInfo.refBaTypeId,
                                    mainTabs: self._module.mainTabView
                                }, self._module.mainTabView);

                                esDialog.showModal(() => {
                                    const dataTable = $$(self.dataTableId) as webix.ui.datatable;
                                    const result = esDialog.selectedValue;
                                    const item = dataTable.getItem(id) as Markers.IMarkerValueModel;
                                    item.displayValue = result.name;
                                    item.value = result.baId.toString();
                                    dataTable.updateItem(id, item);
                                    esDialog.close();
                                });
                                */

                                /* старое диалоговое окно поиска сущности по наименованию */
                              
                                const esDialog = EntitySearch.getEntitySearchDlg(`${self.viewName}-search-dialog`, BA.BATypes.IGroup);
                                esDialog.showModal(
                                    () => {
                                        const dataTable = $$(self.dataTableId) as webix.ui.datatable;
                                        const result = esDialog.selectedEntity;
                                        const item = dataTable.getItem(id) as Markers.IMarkerValueModel;
                                        item.displayValue = result.name;
                                        item.value = result.id.toString();
                                        dataTable.updateItem(id,item);
                                        esDialog.close();
                                    });
                                
                                break;

                        }


                    }
                }
            }
        }

        


        function buttonToolBarConfig() {

            if (self._readOnly) {
                return {
                    height: 30,
                    cols: [
                        {},
                        self._closeButton.init()
                    ]
                }

            }
            return {
                height: 30,
                cols: [
                    {},
                    self._okBtn.init() ,
                    self._cancelBtn.init()
                ]
            }
        }

        function getValueColum() {
            switch (self._markerInfo.markerType) {
            case HIS.Models.Layer.Models.Markers.MarkerType.MtList:
                return {
                    id: `value`,
                    header: "Значение",
                    sort: "string",
                    editor: "select",
                    options: self._markerInfo.list.map(item => {
                        return {
                            id: item.itemId,
                            value: item.value
                        }
                    }),
                    minWidth: 200,
                    width: 200,
                    fillspace: true,
                    invalidMessage: "Нужно заполнить поле"
                }
            case HIS.Models.Layer.Models.Markers.MarkerType.MtAddress:
            case HIS.Models.Layer.Models.Markers.MarkerType.MtEntityRef:
                return {
                    id: `value`,
                    header: "Значение",
                    sort: "string",
                    minWidth: 200,
                    width: 200,
                    fillspace: true,
                    invalidMessage: "Нужно заполнить поле",
                    template: (data: Markers.IMarkerValueModel) => {
                        return `<span style='right:28px; padding-top:5px' class='webix_input_icon fa-search'></span><div style="overflow:hidden">${
                            data.displayValue}</div>`;
                    }
                }
          
            case HIS.Models.Layer.Models.Markers.MarkerType.MtReference:
                return {
                    id: `value`,
                    header: "Значение",
                    sort: "string",
                    minWidth: 200,
                    width: 200,
                    fillspace: true,
                    invalidMessage: "Нужно выбрать ОПФ",
                    editor: "richselect",
                    template: (data: Markers.IMarkerValueModel) => {

                        return `<div style="overflow:hidden">${
                            data.displayValue}</div>`;
                    },
                    popup: {
                        view: "gridsuggest",
                        textValue: "ItemName",
                        resize: true,
                        width: 800,
                        body: {
                            autoConfig: true,
                            header: false,
                            scroll: true,
                            autoheight: false,
                            autofocus: true,
                            yCount: 5,
                            borderless: true,
                            columns: [],
                        },
                        on: {
                            onValueSuggest(item: any) {

                                //webix.message(item.shortName);
                                const id = ($$(self.dataTableId) as webix.ui.datatable).getSelectedId(true, false)[0];
                                const rowItem = ($$(self.dataTableId) as webix.ui.datatable).getItem(id) as Markers.IMarkerValueModel;
                                rowItem.displayValue = item.ItemName;
                                rowItem.value = item.ItemId.toString();
                            }
                        },
                        url: {
                            $proxy: true,
                            load: (view, callback, params) => {
                                Loader.CatalogMarkerHelper.dataToControl(view,
                                    self._markerInfo);
                            }
                        }
                    }
                }
            default:
                return {
                    id: `value`,
                    header: "Значение",
                    sort: "string",
                    editor: "text",
                    minWidth: 200,
                    width: 200,
                    fillspace: true,
                    invalidMessage: "Нужно заполнить поле"
                }
            }
        }
    }

    private checkReadOnly() {
        if (this._readOnly) {
            this._toolBarButtons.add.button.disable();
            this._toolBarButtons.remove.button.disable();
        }
    }
}

interface IMarkerValueModelChanged extends Markers.IPeriodicMarkerValueModel {
    changed: boolean;
    isNew: boolean;
}




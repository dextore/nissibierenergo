﻿import Base = require("./PeriodMarkerControlBase");
import Helper = require("./MarkerHelper");

export function getPeriodMarkerControl(viewName: string, markerInfo: Markers.IMarkerInfoModel, getExtConfig: (content: any) => any = null): Base.IPeriodMarkerControl {
    return new PeriodMarkerTextControl(viewName, markerInfo, getExtConfig);
}

class PeriodMarkerTextControl extends Base.PeriodMarkerControlBase implements Base.IPeriodMarkerControl {

    get control() {
        return $$(this.controlId) as webix.ui.text;
    };

    constructor(viewName: string,
        markerInfo: Markers.IMarkerInfoModel,
        getExtConfig: (content: any) => any) {
        super(viewName, markerInfo, getExtConfig);
    }

    public customValidate(): boolean {

        if (this.readOnly)
            return true;

        const value = this.control.getValue();
        const dateValue = this.datePickerControl.getValue();

        if (this.markerInfo.isRequired && !value)
            return false;

        if (!value && !!dateValue && this.datePickerControl.validate()) {
            return false;
        }
        if (!!value && !dateValue) {
            this.datePickerControl.validate();
        }
        return true;
    }

    protected config() {
        
        const config = webix.extend({
            view: "textExtended",
            id: this.controlId,
            label: this.markerInfo.label,
            name: this.markerInfo.name,
            readonly: this.readOnly,
            required: this.markerInfo.isRequired,
            validate: (value) => {
                return this.customValidate();
            },
            on: {
                onChange: (newv, oldv) => {
                    this.control.validate();
                }
            }
        }, this.getExtConfig(this), true);
        return config;
    }

}
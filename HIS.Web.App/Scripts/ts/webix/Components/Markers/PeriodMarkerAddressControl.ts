﻿import Base = require("./PeriodMarkerControlBase")
import Helper = require("./MarkerHelper");
import SearchAddress = require("../../Components/Address/SearchAddress");

export interface IPeriodMarkerAddressControl extends Base.IPeriodMarkerControl {

}

export function getPeriodMarkerAddressControl(viewName: string,
    markerInfo: Markers.IMarkerInfoModel,
    getExtConfig: (content: any) => any): IPeriodMarkerAddressControl {
    return new PeriodMarkerAddressControl(viewName, markerInfo, getExtConfig);
}

class PeriodMarkerAddressControl extends Base.PeriodMarkerControlBase implements IPeriodMarkerAddressControl {

    private _addressId: number = null;

    get control() {
        return $$(this.controlId) as webix.ui.search;
    };

    get refIdcontrolId(): string {
        return `${this.viewName}-marker-ref-id`;
    }

    constructor(viewName: string,
        markerInfo: Markers.IMarkerInfoModel,
        getExtConfig: (content: any) => any) {
        super(viewName, markerInfo, getExtConfig);
    }

    setValueInternal(value: Markers.IMarkerValueModel, model: { [index: string]: any; }) {
        super.setValueInternal(value, model);
        model[this.markerInfo.name] = (!value || !value.displayValue) ? "" : value.displayValue;
        this._addressId = (!value || !value.value) ? null : Number(value.value);
    }

    get isChanged(): boolean {

        let currentValue = this._addressId;
        let initValue = this.initValue;

        if (!currentValue) {
            currentValue = null;
        }
        if (!initValue) {
            initValue = null;
        }


        if (this.isChangeDate || (currentValue != initValue)) {
            return true;
        }
        return false;
    };

    getValue(model: { [index: string]: any; }): Markers.IMarkerValueModel {
        const value = super.getValue(model);
        value.value = (!this._addressId) ? null : this._addressId.toString();
        value.displayValue = this.control.getValue();

        if (this.isChanged && !value.value) {

            if (!value.startDate) {
                value.startDate = this.initDate;
            }
            value.isDeleted = true;
        }  
        return value;
    }

    protected setReadOnly(value: boolean) {
        super.setReadOnly(value);
        this.control.define("readonly", "true");
        this.control.refresh();
       
    }

    public customValidate(): boolean {
        const value = this.control.getValue();
    
        if (this.readOnly)
            return true;

        if (this.markerInfo.isRequired && !value)
            return false;

        if (this.forFilter)
            return true;

        const dateValue = this.datePickerControl.getValue();
        if (!value && !!dateValue && this.datePickerControl.validate()) {
            return false;
        }
        if (!!value && !dateValue) {
            this.datePickerControl.validate();
        }
        return true;
    }

    config() {
        let config = webix.extend({
            view: this.forFilter ? "text" : "extsearch",
            css: "protoUI-with-icons-2",
            id: this.controlId,
            label: this.markerInfo.label,
            name: `${this.markerInfo.name}_display`,
            icons: !this.forFilter ? ["search", "close"] : [],
            readonly: !this.forFilter,
            required: this.markerInfo.isRequired,
            validate: (value) => {
                return this.customValidate();
            },
            on: {
                onSearchIconClick: () => {
                    this.selectAddress();
                },
                onCloseIconClick: () => {
                    if (this.readOnly)
                        return;

                    if (this.markerInfo.isBlocked) {
                        return;
                    }
                    //this.isChanged = true;
                    this._addressId = null;
                    this.control.setValue("");
                    this.control.validate();
                }
            }
        }, this.getExtConfig(this), true);


        return { cols: [config] }
    }

    private selectAddress() {

        if (this.readOnly)
            return; 

        const searchDialog = SearchAddress.getSearchAddress(`${this.viewName}-search-address`, null);
        searchDialog.showModal(() => {
            const searchResult: Address.IAddressUpdateResultModel = searchDialog.selected;
            this.control.setValue(searchResult.name);
            //this.isChanged = true;
            this._addressId = searchResult.baId;
            this.control.validate();
            searchDialog.close();
        });
    }
}
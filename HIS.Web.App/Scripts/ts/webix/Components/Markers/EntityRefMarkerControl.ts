﻿import Base = require("./MarkerControlBase");
import Helper = require("./MarkerHelper");
import Tab = require("../../MainView/Tabs/MainTabBase");
import SearchDlg = require("../../MainView/UniversalSearch/SearchDlg");
import SelectDlg = require("../../Components/Markers/EntytyRefSelectDlgBase");
import Event = require("../../Common/Events");
import Api = require("../../Api/ApiExporter");

export interface IEntityRefMarkerControl extends Base.IMarkerControl {
    setModule(module: Tab.IMainTab);
}

export interface IEntityRefMarkerControllAddButtonDisabler extends Base.IMarkerControl {
    isDisabledAddButton;

}

export interface IEntityRefMarkerControllMaster extends Base.IMarkerControl {
    selectedChangeEvent: Event.IEvent<number>;
}

export interface IEntityRefMarkerControllSlave extends Base.IMarkerControl {
    masterBaId: number;
    selectDlg: () => SelectDlg.IEntityRefSelectDlg;
}


export function getEntityRefMarkerControl(viewName: string,
    markerInfo: Markers.IMarkerInfoModel,
    getExtConfig: (content: any) => any = null): IEntityRefMarkerControl {
    return new EntityRefMarkerControl(viewName, markerInfo, getExtConfig);
}

class EntityRefMarkerControl extends Base.MarkerControlBase
implements IEntityRefMarkerControl, IEntityRefMarkerControllAddButtonDisabler, IEntityRefMarkerControllSlave,
IEntityRefMarkerControllMaster {
    private _selectedChangeEvent: Event.IEvent<number> = new Event.Event<number>();

    private _masterBaId: number = null;

    get suggestId() { return `suggest-${this.controlId}`; }
    get suggestBodyId() { return `suggest-body-${this.controlId}`;  }

    private _asterChangedHandler: (baId: number) => void = null;

    get masterBaId(): number {
        return this._masterBaId;
    }

    set masterBaId(value: number) {
        this._masterBaId = value;
        if (!this.selectDlg)
            return;

        //(this._masterBaId) ? this.control.enable() : this.control.disable();
        this._entityBaId = null;
        this.control.setValue(null);
    }

    selectDlg: () => SelectDlg.IEntityRefSelectDlg;

    disabledAddButton: boolean = false;

    get isDisabledAddButton() {
        return this.disabledAddButton;
    }

    set isDisabledAddButton(value: boolean) {
        this.disabledAddButton = value;
    }
    get isChanged(): boolean {

        let currentValue = this._entityBaId;
        let initValue = this.initValue;

        if (!currentValue) {
            currentValue = null;
        }
        if (!initValue) {
            initValue = null;
        }

        if (currentValue == initValue) {
            return false;
        }
        return true;
    };

    private _module: Tab.IMainTab = null;
    private _entityBaId: number = null;
    private _currentEntityBaId: number = null;

    get control() {
        if (this.markerInfo.searchTypeId == 1) {
            return $$(this.controlId) as webix.ui.search;
        }
        return $$(this.controlId) as webix.ui.search;
    };

    get selectedChangeEvent(): Event.IEvent<number> {
        return this._selectedChangeEvent;
    }

    constructor(id: string, markerInfo: Markers.IMarkerInfoModel, getExtConfig: (content: any) => any) {
        super(id, markerInfo, getExtConfig);
    }

    setModule(module: Tab.IMainTab) {
        this._module = module;
    }

    getSelectedCompany() {
        if (!this._module) {
            return null;
        }
        return this._module.mainTabView.getSelectedCompany();
    }

    setValueInternal(value: Markers.IMarkerValueModel, model: { [index: string]: any; }) {
        model[this.markerInfo.name] = (!value || !value.displayValue) ? "" : value.displayValue;
        this._entityBaId = (!value || !value.value) ? null : Number(value.value);
        this._currentEntityBaId = this._entityBaId;
        this._selectedChangeEvent.trigger(this._entityBaId, this);
    }

    getValue(model: { [index: string]: any; }): Markers.IMarkerValueModel {

        const value = ($$(this.controlId) as any).getValue();
        const result = {
            displayValue: value,
            endDate: null,
            isBlocked: false,
            isDeleted: false,
            isVisible: true,
            itemId: null,
            markerId: this.markerInfo.id,
            markerType: this.markerInfo.markerType,
            MVCAliase: this.markerInfo.name,
            note: null,
            startDate: null,
            value: (!this._entityBaId) ? null : this._entityBaId.toString()
        };
        // проверка на удаление маркера 
        if (!result.value && this.isChanged) {
               result.isDeleted = true;
        }
        return result;
    }

    protected setReadOnly(value: boolean) {
        super.setReadOnly(value);
        this.control.define("readonly", "true");
        this.control.refresh();
        //value ? this.control.disable() : this.control.enable();
    }

    private clearControl() {

        this._entityBaId = null;
        this.control.setValue(null);
        this.control.setValue("");
        this.control.validate();
        this._selectedChangeEvent.trigger(this._entityBaId, this);

    }
    private resizeSuggestTable() {
      
        const table = $$(this.suggestBodyId) as webix.ui.datatable;
        const control = $$(this.controlId) as webix.ui.text;

        if (table.count() < table.config.yCount) {
            table.config.yCount = table.count();
            table.resize();
        }

        const width = control.getInputNode().clientWidth;
        if (table.$width !== width) {

            const columns = webix.toArray(table.config.columns);
            columns.forEach(item => item.width = width-37);
            table.refreshColumns();

        }
     
    }

    public customValidate(): boolean {
      
        if (this.readOnly)
            return true;
        const value = this.control.getValue();

        if (this.forFilter) {
            if (!value) {
                return false;
            }
        } else if (this.markerInfo.isRequired) {
            if (!value) {
                return false;
            }
            if (!this._entityBaId) {
                return false;
            }
        }
        return true;
    }


    config() {

        let config: any = null;
        let action = this.markerInfo.searchTypeId;
        if (this.forFilter) {
            action = -1;
        }
        switch (action) {
            // диалоговое окно поиска
            case -1:
                config = this.getConfigForSearch();
                break;
            // выпадающий список
            case 1:
                config = this.getConfigForList();
                break;
            // форма поиска 
            default:
                config = this.getConfigForForm();
                break;
        }
        /*
        if (this.markerInfo.isRequired) {
            config = webix.extend(config, Helper.markerHelper.requiredConfig(this.controlId, webix.rules.isNotEmpty));
        }
        */
        return { cols: [config] };
    }

    private getConfigForSearch()
    {
        return webix.extend({
            view: "text",
            id: this.controlId,
            label: this.markerInfo.label,
            name: this.markerInfo.name,
            readonly: !this.forFilter,
            required: true,
            validate: (value) => {
                return this.customValidate();
            }
        }, this.getExtConfig(this), true);
    }
    private getConfigForList() {
        return webix.extend({
            view: "extsearch",
            id: this.controlId,
            icons: !this.forFilter ? ["angle-down"] : [],
            css: !this.forFilter ? "protoUI-with-icons-1" : "",
            readonly: true,
            label: this.markerInfo.label,
            name: this.markerInfo.name,
            required: this.markerInfo.isRequired,
            validate: (value) => {
                return this.customValidate();
            },
            on: {
                onItemClick: () => {

                    if (this.readOnly)
                        return;

                    if (this.markerInfo.isBlocked) {
                        return;
                    }

                    this.resizeSuggestTable();
                    const control = $$(this.controlId) as webix.ui.text;
                    const suggest = $$(this.suggestId) as webix.ui.suggest;
                    suggest.show(control.getInputNode());                   
                },
                onBlur: () => {
                   ($$(this.suggestId) as webix.ui.suggest).hide();
                }
            },
            suggest: {
                id: this.suggestId,
                view: "gridsuggest",
                textValue: "name",
                on: {
                    onValueSuggest: (obj) => {
                        if (obj) {
                            if (obj["isNull"]) {
                                this.clearControl();
                                return;
                            }
                            this._entityBaId = obj["id"];
                            this.control.validate();
                            this._selectedChangeEvent.trigger(this._entityBaId, this);
                        }
                    }
                },
                body: {
                    id: this.suggestBodyId,
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    autowidth: true,
                    header: false,
                    columns: [
                        { id: "id", header: "id" , hidden:true},
                        { id: "name", header: "name" },
                        { id: "isNull", header: "isNull" , hidden: true }
                    ],
                    url: {
                        $proxy: true,
                        load: () => {

                           
                            Api.getApiQueryBuilder().getFields(this.markerInfo.refBaTypeId).then((colums: Querybuilder.ICaptionAndAliaseModel[]) => {


                                let aliaseBaId = "";
                                let aliaseName = "";
                                let aliaseOrganization = "";

                                colums.forEach(item => {

                                    if (item.aliase.indexOf("//Name]") >= 0 && item.markerId == 1) {
                                        aliaseName = item.aliase;
                                    }
                                    if (item.aliase.indexOf("//BAId]") >= 0) {
                                        aliaseBaId = item.aliase;
                                    }
                                    if (item.aliase.indexOf("//CompanyArea]") >= 0) {
                                        aliaseOrganization = item.aliase;
                                    }

                                });
                                const delStateAliase = aliaseName.replace("//Name]","//IsDelState]")
                                
                                const queryParams = {
                                    baTypeId: this.markerInfo.refBaTypeId,
                                    from: 0,
                                    to: 1000,
                                    filteringsFields: [{
                                        field: delStateAliase,
                                        filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal,
                                        value: "false"
                                    }],
                                    orderingsFields: [{
                                        orderingField: aliaseName,
                                        sortingOrder: HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Asc
                                    }]
                                };

                                const companyId = this.getSelectedCompany().id;

                                if (aliaseOrganization != "" && companyId > 0 && this.markerInfo.id != 6) {
                                    queryParams.filteringsFields.push({
                                        field: aliaseOrganization,
                                        filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal,
                                        value: companyId.toString()
                                    });
                                }

                                Api.getApiQueryBuilder().getData(queryParams as any).then((data) => {

                                    let items = JSON.parse(data as any).items as any[];
                                    const result = [] as any;
                                    if (!this.markerInfo.isRequired) {
                                        result.push({
                                            id: null,
                                            name: "<<пусто>>",
                                            isNull:true
                                        });
                                    }
                                    const list = ($$(this.suggestId) as webix.ui.gridsuggest).getList() as any;
                                    list.clearAll();
                                    items.forEach(item => {
                                        result.push({
                                            id: item[aliaseBaId],
                                            name: item[aliaseName],
                                            isNull: false
                                        });
                                    });
                                    list.parse(result, "json");

                                }).catch((error) => {
                                    console.log(error);
                                });

                            }).catch((error) => {
                                console.log(error);
                            });
                        }
                    }
                }
            }

        }, this.getExtConfig(this), true);
    }
    private getConfigForForm() {
        return webix.extend({
            view: "extsearch",
            id: this.controlId,
            css: !this.forFilter ? "protoUI-with-icons-2" : "",
            icons: !this.forFilter ? ["search", "close"] : [],
            label: this.markerInfo.label,
            readonly: !this.forFilter,
            name: this.markerInfo.name,
            required: this.markerInfo.isRequired,
            validate: (value) => {
                return this.customValidate();
            },
            on: {
                onSearchIconClick: () => {
                    if (this.readOnly)
                        return;

                    if (!this._module) {
                        webix.message("Tab module not set", "error");
                        return;
                    }

                    if (this.markerInfo.isBlocked) {
                        return;
                    }

                    if (!this.selectDlg) {
                        const dialog = SearchDlg.getSearchDlg({
                            viewName: `${this.viewName}-searc-dlg`,
                            mainTabs: this._module.mainTabView,
                            disableAddButton: this.isDisabledAddButton,
                            baTypId: this.markerInfo.refBaTypeId
                        },
                            this._module.mainTabView);
                        dialog.showModalContent(this._module,
                            () => {
                                const result = dialog.selectedValue;
                                this._entityBaId = result.baId;
                                this.control.setValue(result.name);
                                this.control.validate();
                                dialog.close();
                                this._selectedChangeEvent.trigger(this._entityBaId, this);
                            });
                    } else {
                        if (!this.masterBaId)
                            return;

                        const dialog = this.selectDlg();
                        dialog.showSelect(this.masterBaId, this._module,
                            () => {
                                const result = dialog.selectedValue;
                                this._entityBaId = result.baId;
                                this.control.setValue(result.name);
                                this.control.validate();
                                dialog.close();
                                this._selectedChangeEvent.trigger(this._entityBaId, this);
                            });
                    }
                },
                onCloseIconClick: () => {
                    if (this.readOnly)
                        return;

                    if (this.selectDlg && !this._entityBaId)
                        return;


                    if (!this._module) {
                        webix.message("Tab module not set", "error");
                        return;
                    }
                    if (this.markerInfo.isBlocked) {
                        return;
                    }
                    this.clearControl();
                }
            }
        }, this.getExtConfig(this), true);

    }

}
﻿import Base = require("../Common/EventSubscriberBase");

export interface IComponent extends Base.IEventSubscriber {
    init(): any;
}

export abstract class ComponentBase extends Base.EventSubscriberBase implements IComponent {
    abstract init(): any;
}
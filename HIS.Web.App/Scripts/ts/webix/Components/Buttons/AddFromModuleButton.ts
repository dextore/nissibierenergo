﻿import Base = require("./ButtonBase");

export function getAddFromModuleButton(viewName: string, click: (id, e) => any) {
    const btn = new OkButton(viewName);
    btn.click = click;
    return btn;
}

class OkButton extends Base.ButtonBase {

    constructor(viewName: string) {
        super(`${viewName}-show-module`);
    }

    config() {
        return {
            type: "iconButton",
            //type: "form",
            icon: "plus",
            value: "Добавить",
            label: "Добавить"
        };
    }
}
﻿import Base = require("./ButtonBase");


export function getPeriodMarkerHistoryButton(viewName: string, disabled: boolean, click: (id, e) => any) {
    const btn = new PeriodMarkerHistoryButton(viewName, disabled);
    btn.click = click;
    return btn;
}

class PeriodMarkerHistoryButton extends Base.ButtonBase {

    constructor(viewName: string, private readonly _disabled: boolean) {
        super(`${viewName}-show-history`);
    }

    config() {
        return {
            type: "iconButton",
            icon: "angle-double-down",
            width: 28,
            disabled: this._disabled
        };
    }
}
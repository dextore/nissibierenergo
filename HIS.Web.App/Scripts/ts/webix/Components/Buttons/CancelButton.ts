﻿import Base = require("./ButtonBase");


export function getCancelButton(viewName: string, click: (id, e) => any) {
    const btn = new CancelButton(viewName);
    btn.click = click;
    return btn;
}

class CancelButton extends Base.ButtonBase {

    constructor(viewName: string) {
        super(`${viewName}-cancel`);
    }

    config() {
        return  {
            type: "iconButton",
            //type: "form",
            icon: "ban",
            value: "Отказаться",
            label: "Отказаться"
        };
    }
}
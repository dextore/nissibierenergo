﻿import Base = require("./ButtonBase");


export function getCloseButton(viewName: string, click: (id, e) => any) {
    const btn = new CloseButton(viewName);
    btn.click = click;
    return btn;
}

class CloseButton extends Base.ButtonBase {

    constructor(viewName: string) {
        super(`${viewName}-close`);
    }

    config() {
        return {
            type: "iconButton",
            icon: "window-close",
            value: "Закрыть",
            label: "Закрыть"
        };
    }
}
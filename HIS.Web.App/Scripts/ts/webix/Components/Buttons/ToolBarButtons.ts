﻿import Base = require("./ButtonBase"); 

export interface IToolBarButton extends Base.IButton {
    needSelected: boolean;
}

export function getArrowLeftBtn(viewName: string, tooltip: string, click: (id, e) => any) {
    const btn = new ToolBarButton(`${viewName}-arrow-left`, "arrow-left", tooltip);
    btn.click = click;
    return btn;
}

export function getNewBtn(viewName: string, tooltip: string, click: (id, e) => any) {
    const btn = new ToolBarButton(`${viewName}-plus`, "plus", tooltip);
    btn.click = click;
    return btn;
}

export function getEditBtn(viewName: string, tooltip: string, click: (id, e) => any) {
    const btn = new ToolBarButton(`${viewName}-adit`, "edit", tooltip, true);
    btn.click = click;
    return btn;
}

export function getRemoveBtn(viewName: string, tooltip: string, click: (id, e) => any) {
    const btn = new ToolBarButton(`${viewName}-remove`, "remove", tooltip, true);
    btn.click = click;
    return btn;
}

export function getRefreshBtn(viewName: string, tooltip: string, click: (id, e) => any) {
    const btn = new ToolBarButton(`${viewName}-refresh`, "retweet", tooltip);
    btn.click = click;
    return btn;
}

export function getSaveBtn(viewName: string, tooltip: string, click: (id, e) => any) {
    const btn = new ToolBarButton(`${viewName}-save`, "save", tooltip);
    btn.click = click;
    return btn;
}

export function getSearchBtn(viewName: string, tooltip: string, click: (id, e) => any) {
    const btn = new ToolBarButton(`${viewName}-search`, "search", tooltip);
    btn.click = click;
    return btn;
}

export function getCancelBtn(viewName: string, tooltip: string, click: (id, e) => any) {
    const btn = new ToolBarButton(`${viewName}-cancel`, "ban", tooltip);
    btn.click = click;
    return btn;
}

export function getRunBtn(viewName: string, tooltip: string, click: (id, e) => any) {
    const btn = new ToolBarButton(`${viewName}-run`, "caret-right", tooltip);
    btn.click = click;
    return btn;
}

export function getCopyBtn(viewName: string, tooltip: string, click: (id, e) => any) {
    const btn = new ToolBarButton(`${viewName}-copy`, "copy", tooltip);
    btn.click = click;
    return btn;
}

export function getHistoryBtn(viewName: string, tooltip: string, click: (id, e) => any) {
    const btn = new ToolBarButton(`${viewName}-history`, "history", tooltip);
    btn.click = click;
    return btn;
}

export function getInventoryTransactionBtn(viewName: string, tooltip: string, click: (id, e) => any) {
    const btn = new ToolBarButton(`${viewName}-inventory-transaction`, "file-alt", tooltip, true);
    btn.click = click;
    return btn;
}

class ToolBarButton extends Base.ButtonBase implements IToolBarButton {

    constructor(viewName: string,
        private readonly _icon: string,
        private readonly _tooltip: string,
        private readonly _needSelected = false) {
        super(viewName);
    }

    get needSelected(): boolean {
        return this._needSelected;
    }

    protected config(): any {
        return {
            type: "iconButton",
            icon: this._icon,
            width: 28,
            align: "left",
            disabled: true,
            tooltip: this._tooltip
        };
    }
}

export function getTestBtn(viewName: string, icon: string, label: string, tooltip: string, needSelected: boolean, click: (id, e) => any) {
    const btn = new TestToolBarButton(`${viewName}-test-${webix.uid()}`, icon, label, tooltip, needSelected);
    btn.click = click;
    return btn;
}

class TestToolBarButton extends Base.ButtonBase implements IToolBarButton {

    constructor(viewName: string,
        private readonly _icon: string,
        private readonly _label: string,
        private readonly _tooltip: string,
        private readonly _needSelected = false) {
        super(viewName);
    }

    get needSelected(): boolean {
        return this._needSelected;
    }

    protected config(): any {
        let config = {
            //type: "iconButton",
            //icon: this._icon,
            //width: 28,
            align: "left",
            disabled: true,
            tooltip: this._tooltip
        };

        if (this._icon) {
            config = webix.extend(config, { type: "iconButton", icon: this._icon });
        }

        if (this._label) {
            config = webix.extend(config, { label: this._label });
        }

        return config;
    }
}


﻿import Base = require("../ComponentBase"); 

export interface IButton extends Base.IComponent {
    viewId: string;
    button: webix.ui.button;
    click: (id, e) => any; 
}

export abstract class ButtonBase extends Base.ComponentBase implements IButton {

    private _click: (id, e) => any = (id, e) => {};

    get viewId(): string {
        return `${this._viewName}-btn-id`;
    }

    get button(): webix.ui.button {
        return $$(this.viewId) as webix.ui.button;
    }

    get click(): (id, e) => any {
        return this._click;
    }

    set click(value: (id, e) => any) {
        this._click = value;
    }

    protected constructor(private readonly _viewName: string) {
        super();
    }

    init(extConfig: any = null) {
        const config = (extConfig) ? webix.extend(this.config(), extConfig, true) : this.config();
        return webix.extend(this.baseConfig(), config, true);
    }

    private baseConfig() {
        return  {
            view: "button",
            id: this.viewId,
            width: 110,
            on: {
                onItemClick: (id, e) => { this._click(id, e); }
            }
        };
    }

    protected config() {}
} 
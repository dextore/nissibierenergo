﻿import Base = require("./ButtonBase");


export function getOkButton(viewName: string, click: (id, e) => any) {
    const btn = new OkButton(viewName);
    btn.click = click;
    return btn;
}

class OkButton extends Base.ButtonBase {

    constructor(viewName: string) {
        super(`${viewName}-ok`);
    }

    config() {
        return  {
            type: "iconButton",
            //type: "form",
            icon: "check-circle",
            value: "Принять",
            label: "Принять"
        };
    }
}
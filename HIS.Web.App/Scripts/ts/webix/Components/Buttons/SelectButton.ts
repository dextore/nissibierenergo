﻿import Base = require("./ButtonBase");


export function getSelectButton(viewName: string, click: (id, e) => any) {
    const btn = new SelectButton(viewName);
    btn.click = click;
    return btn;
}

class SelectButton extends Base.ButtonBase {

    constructor(viewName: string) {
        super(`${viewName}-search`);
    }

    config() {
        return {
            type: "iconButton",
            icon: "search",
            width: 28,
        }
    }
}
﻿import Api = require("../../Api/ApiExporter");
import Base = require("../../Common/DialogBase");
import Btn = require("../Buttons/ButtonsExporter");

export function showWarning(viewName: string, errors: string[]) {
    const warning = new Warning(viewName);
    if (!errors || errors.length === 0)
            return;
    warning.showModal();
    warning.setWarnings(errors);
    
}
export function showForEntity(viewName: string, baId:number) {
    const warning = new Warning(viewName);
     Api.getApiCommons().checkErrors(baId).then(data => {
    if (!data || data.length === 0) 
            return;
         warning.showModal();
         warning.setWarnings(data);
    });
}
class Warning extends Base.DialogBase {

    constructor(viewName: string) {
        super(viewName);      
    }
    get listId() { return `${this.viewName}-list-id`; }
    protected headerLabel(): string { return "Предупреждения"; }
    protected cancelBtn = Btn.getCloseButton(this.viewName, () => { this.cancelClose(); });

    protected contentConfig() {
        const self = this;
        return {
            rows: [
                {
                    view: "list",
                    id: self.listId,
                    css: "warning-message-list",
                    height: 250,
                    borderless: false,
                    select: false,
                    template: (obj) => {
                        return `<div class='warning-message-error-block'>                        
                            <div class='title'>${obj}</div>
                            </div><div style='clear:both'></div>`;
                    }
                },
                {
                    cols: [
                        {},
                        this.cancelBtn.init()
                    ]
                }
              
            ]
        };
    }
    setWarnings(errors: string[]) {
        const list = $$(this.listId) as webix.ui.list;
        list.clearAll();
        errors.forEach(item => {
            list.add(item);
        });
    }
   
}



﻿import Common = require("../Common/CommonExporter");

export interface IMainTabbarView {
    showForm(formId: string, builder: () => Common.IMainTab): Common.IMainFormTab;
    showModule(moduleId: string): Common.IMainFormTab;

    // Возвращает id модуля по MVCAlias baTypeId.
    closeForm(tabId: string);
    getSelectedCompany(): Inventory.ILSCompanyAreas;
    sendTreeItem(entity: Entity.IFullTextSearchEntityInfoModel);
    eventQueue: Common.Event<IEventQueueMessageArg>;
    //disableCompanyChange();
    //enableCompanyChange();
}

export interface IEventQueueMessageArg {
    subscriber: any;
    messageType: QueueMessageType;
    messageArgs: any;
}

export enum QueueMessageType {
    SearchEvent = 1, 
    DisableOrganisation = 10, 
    EnableOrganisation = 11, 
}

﻿import Base = require("../../Common/DialogBase");
import Common = require("../../Common/CommonExporter");
import SET = require("../../Common/SystemEntitityTypes");
import Btn = require("../../Components/Buttons/ButtonsExporter");
import View = require("./SearchView");
import Models = require("./Models");
import Helper = require("../../Common/ModuleHelper");

export interface ISearchDlg extends Base.IDialog {
    selectedValue: Models.ISearchDialogResult;
} 

export interface ISearchParams {
    viewName: string;
    mainTabs: Common.IMainTabbarView;
    disableAddButton: boolean;
    baTypId?: number;
}

// baTypId - настройка для поиска по отдельной сущности
export function getSearchDlg(searchParams: ISearchParams, mainTabbarView: Common.IMainTabbarView): ISearchDlg {
    return new SearchDlg(searchParams, mainTabbarView);
} 

class SearchDlg extends Base.DialogBase implements ISearchDlg {

    private _okBtn = Btn.getOkButton(this.viewName, () => { this.okClose(); });
    private _cancelBtn = Btn.getCancelButton(this.viewName, () => { this.cancelClose(); });
    private _addBtn = Btn.getAddFromModuleButton(this.viewName, () => { this.showModuleAndAdd(); });

    // Представление
    private _view: View.ISearchView;

    get selectedValue() {
        return this._view.selectedValue;
    }

    constructor(private readonly _searchParams: ISearchParams,
        private readonly _mainTabbarView: Common.IMainTabbarView) {
        super(_searchParams.viewName, true);
        this.hPadding = 80;
        this.vPadding = 80;
        this._view = View.getSearchView(`${this.viewName}-view-id`, this._searchParams.baTypId, _mainTabbarView);
    }

    headerLabel(): string {
        return !!this._searchParams.baTypId
            ? `Поиск "${SET.entityTypes().getByBATypeId(this._searchParams.baTypId).entityInfo.name}"`
            : "Поиск...";
    }

    okClose(): void {
        if (!this.selectedValue)
            return;

        if (!this._searchParams.baTypId) {
            this.showModule();
        }

        super.okClose();
    }

    showModule() {
        const selectedValue = this._view.selectedValue;
        if (!selectedValue)
            return;

        const entityInfo = SET.entityTypes().getByBATypeId(selectedValue.baTypeId);

        if (!Helper.ModuleHelper.aliasModuleExists(entityInfo.entityInfo.mvcAlias)) {
            webix.alert({
                title: "Модуль",
                ок: `Закрыть`,
                text: "Модуль не реализован!"
            });

            return;
        }

        Helper.ModuleHelper.showModuleByAlias(entityInfo.entityInfo.mvcAlias, this._mainTabbarView);

        const selectedEntity = {
            baId: selectedValue.baId,
            endDate: null,
            name: selectedValue.name,
            startDate: null,
            stateId: null,
            stateName: null,
            typeId: selectedValue.baTypeId,
            typeName: entityInfo.entityInfo.name
        }

        this._mainTabbarView.eventQueue.trigger({
            messageType: Common.QueueMessageType.SearchEvent,
            messageArgs: selectedEntity,
            subscriber: this
        }, this);
        return;
    }

    showModuleAndAdd() {

        const self = this;

        const result = this._view.getCurrentBaTypes();
        if (result.length === 1) {
            showModule(result[0].mvcAlias);
        } else {
            const menu = new ModulesContentMenu(`${this.viewName}-content-menu`, result);
            menu.show(this._addBtn.button.getNode(), (id: string) => {
                showModule(id);
            });
        }
        
        function showModule(alias: string) {
            if (!Helper.ModuleHelper.aliasModuleExists(alias)) {

                webix.alert({
                    title: "Модуль",
                    ок: `Закрыть`,
                    text: "Модуль не реализован!"
                });

                return;
            }
            self.close();    
            Helper.ModuleHelper.showModuleByAlias(alias, self._searchParams.mainTabs);
        }
    }

    ready(): void {
        this._view.ready();
        this._searchParams.disableAddButton && this._addBtn.button.disable();
    }

    contentConfig() {
        return {
            rows: [
                this._view.init(),
                {
                    view: "toolbar",
                    height: 45,
                    padding: 5,
                    cols: [
                        this._addBtn.init(),
                        {},
                        this._okBtn.init(),
                        this._cancelBtn.init()
                    ]
                }
            ]
        };
    }

    windowConfig() {
        return {minWidth: 800};
    }
}


class ModulesContentMenu {

    get popupId() {
        return `${this._viewName}-popup-id`;
    } 

    constructor(private readonly _viewName,
        private readonly _entities: Entities.IEntityInfoModel[]) {

    }

    show(node: any, callback: (id: string) => void) {

        let popup = $$(this.popupId);
        if (!popup)
            popup = this.createMenu(callback);
        popup.show(node);
    } 

    private createMenu(callback: (id: string) => void) {
        const popup =   webix.ui({
            view: "contextmenu",
            id: this.popupId,
            data: this.getData(this._entities),
            on: {
                onHide: () => {
                    popup.close();
                },
                onItemClick: (id, e, node) => {
                    popup.close();
                    callback(id);
                }
            }
        }) as webix.ui.contextmenu;
        return popup;
    }

    private getData(entities: Entities.IEntityInfoModel[]) {
        return entities.map(item => {
            return {
                id: item.mvcAlias,
                value: item.name
            };
        });
    }
}
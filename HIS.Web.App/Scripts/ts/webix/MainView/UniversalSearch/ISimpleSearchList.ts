﻿import Base = require("../../Components/ComponentBase");
import Models = require("./Models");

export interface ISimpleSearchList extends Base.IComponent {
    showDeleted(value: boolean);
    search(value: string);
    clearSearch();
    ready();
    selectedValue: Models.ISearchDialogResult;
}

export abstract class SimpleSearchListBase extends Base.ComponentBase implements ISimpleSearchList {
    abstract search(value: string);
    abstract showDeleted(value: boolean);
    abstract clearSearch();
    abstract ready();
    selectedValue: Models.ISearchDialogResult;
}
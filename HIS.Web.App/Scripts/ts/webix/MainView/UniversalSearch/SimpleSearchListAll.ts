﻿import Base = require("./ISimpleSearchList");
import Api = require("../../Api/ApiExporter");
import Models = require("./Models");
import Pager = require("../../Components/Pager/Pager");

export interface ISimpleSearchListAll extends Base.ISimpleSearchList {
    setSearchType(byInn: boolean)
}

export function getSimpleSearchListAll(viewName: string, baTypeId: number): ISimpleSearchListAll {
    return new SimpleSearchListAll(viewName, baTypeId);
}


export class SimpleSearchListAll extends Base.SimpleSearchListBase implements ISimpleSearchListAll {

    private _searchValue: string;
    private _showDeleted: boolean;
    private _byInn: boolean = false;

    get dataTableId() {
        return `${this._viewName}-datatable-id`;
    }

    get pagerId() {
        return `${this._viewName}-pager-id`;
    }

    get dataTable() {
        return $$(this.dataTableId) as webix.ui.datatable;
    }

    get selectedBaId(): Models.ISearchDialogResult {
        const rows = this.dataTable.getSelectedItem(true);
        if (!rows.length)
            return null;

        const row = rows[0];

        return {
            baId: row["baId"],
            baTypeId: row["baTypeId"],
            name: row["Name"]
        }
    }

    constructor(private readonly _viewName: string, private readonly _baTypeId: number) {
        super();
    }

    ready() {
        webix.extend(this.dataTable, webix.ProgressBar);
    }

    clearSearch() {
        this.dataTable.clearAll();
    }

    showDeleted(value: boolean) {
        this._showDeleted = value;
    }

    setSearchType(byInn: boolean) {
        this._byInn = byInn;
    } 

    search(value: string) {
        this._searchValue = value;
        (this.dataTable.getPager() as webix.ui.pager).select(0);
        this.dataTable.load(this.dataTable.config.url);
    }

    get selectedValue(): Models.ISearchDialogResult {
        const rows = this.dataTable.getSelectedItem(true);
        if (!rows.length)
            return null;
        const row = rows[0] as UniversalSearch.ISimpleSearchItemModel;


        return {
            baId: row.baId,
            baTypeId: row.baTypeId,
            name: row.name
        }
    }

    init() {
        const pager = Pager.getDataTablePager(this.pagerId);
        return {
            rows: [
                this.dataTableInit(),
                pager.init()
            ]
        };
    }

    dataTableInit() {
        return {
            view: "datatable",
            id: this.dataTableId,
            select: "row",
            //scroll: "y",
            resizeColumn: true,
            pager: this.pagerId,
            datafetch: 20,
            columns: [
                {
                    id: "baId",
                    header: "Ид",
                    width: 50
                }, {
                    id: "typeName",
                    header: "Тип сущности",
                    width: 200
                }, {
                    id: "stateName",
                    header: "Состояние",
                    width: 200
                }, {
                    id: "code",
                    header: "Код",
                    width: 150
                }, {
                    id: "legalFormName",
                    header: "ОПФ",
                    width: 250
                }, {
                    id: "name",
                    header: "Наименование",
                    fillspace: true
                }, {
                    id: "INN",
                    header: "ИНН",
                    width: 200
                }],
            url: {
                $proxy: true,
                load: (view, callback, params) => {
                    this.dataTable.clearAll();

                    if (!this._searchValue)
                        return;

                    //(this.dataTable as any).showProgress();

                    const pager = this.dataTable.getPager();

                    const resquestModel = {
                        from: (!params) ? 0 : params.start,
                        to: (!params) ? pager.data.size : params.start + params.count,
                        value: this._searchValue,
                        showDeleted: this._showDeleted,
                        baTypeId: this._baTypeId
                    }

                    const ds = this._byInn
                        ? Api.ApiUniversalSearch.getByInn(resquestModel)
                        : Api.ApiUniversalSearch.getByName(resquestModel);

                    ds.then(result => {

                        const data = resquestModel.from === 0
                            ? {
                                data: result.items,
                                pos: 0,
                                total_count: result.rowCount
                            }
                            : {
                                data: result.items,
                                pos: resquestModel.from
                            };

                        //($$(this.dataTableId) as any).hideProgress();
                        (webix.ajax as any).$callback(view, callback, data);
                    }).catch(() => {
                        ($$(this.dataTableId) as any).hideProgress();
                        //(webix.ajax as any).$callback(view, callback, null);
                    });
                }
            }

        }
    }

}
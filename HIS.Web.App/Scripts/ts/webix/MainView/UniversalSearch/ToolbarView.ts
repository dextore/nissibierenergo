﻿import Base = require("../../Components/ComponentBase");
import Common = require("../../Common/CommonExporter");
import Models = require("./Models");

/* toolbar верхнил с радиокнопками*/

export interface IToolbarView extends Base.IComponent { 
    searchStateChenge: Common.Event<Models.SelectedSearchState>;
    showDeletedChenge: Common.Event<boolean>;
    searchByLegalSubject: boolean;
}

export function getToolbarView(viewName: string, baTypeId: number): IToolbarView {
    return new ToolbarView(viewName, baTypeId);
} 

class ToolbarView extends Base.ComponentBase implements IToolbarView {

    private _searchStateChenge = new Common.Event<Models.SelectedSearchState>();
    private _showDeletedChenge = new Common.Event<boolean>();
    private _searchByLegalSubject: boolean = false;

    get searchByLegalSubject(): boolean {
        return this._searchByLegalSubject;
    }

    get radioId(): string {
        return `${this._viewName}-radio-id`;
    }

    get searchStateChenge() {
        return this._searchStateChenge;
    }

    get showDeletedChenge() {
        return this._showDeletedChenge;
    }

    constructor(private readonly _viewName: string, private readonly _baTypeId: number) {
        super();
        this._searchByLegalSubject = this.checkSearchByLegalSubject(_baTypeId);
    }

    init() {
        return {
            view: "toolbar",
            borderless: true,
            cols: [
                { width: 15 },
                !this._baTypeId
                    ? {
                        view: "radio",
                        id: this.radioId,
                        align: "center",
                        value: 1,
                        minWidth: 300,
                        maxWidth: 300,
                        options: this.getRadioOptions(),
                        on: {
                            onChange: (newv, oldv) => {
                                this._searchStateChenge.trigger(+newv as Models.SelectedSearchState, this);
                            }
                        }
                    }
                    : { width: 15 },
                this.getCheckBoxOptions(),
                { gravity: 0.01}
            ]
        };
    }

    private getRadioOptions() {
        const self = this;

        //return (!this._searchByLegalSubject)
        //    ? [
        //        { "id": 1, "value": getFirsMarkerCaption(this._baTypeId) },
        //        { "id": 3, "value": "Другое" }
        //    ]
        //    : [
        //        { "id": 1, "value": "Наименование" },
        //        { "id": 2, "value": "ИНН" },
        //        { "id": 3, "value": "Другое" }
        //    ];
        return [
            { "id": 1, "value": "Наименование" },
            { "id": 2, "value": "ИНН" },
            { "id": 3, "value": "Другое" }
        ];

        //function getFirsMarkerCaption(baTypeId: number) {
        //    const markersInfo = Common.entityTypes().getByBATypeId(baTypeId).entityInfo.markersInfo;
        //    for (let idx in markersInfo) {
        //        if (markersInfo.hasOwnProperty(idx)) {
        //            const markerInfo = markersInfo[idx];
        //            // по наименованию
        //            if (markerInfo.id === 1) {
        //                return markerInfo.label;
        //            }
        //        }
        //    }
        //    return "Наименование";
        //}
    }

    private getCheckBoxOptions() {
        return {
            view: "checkbox",
            labelRight: "Показывать удаленные",
            //labelAlign: "right",
            labelWidth: 10,
            value: 0,
            align: "left",
            maxWidth: 300,
            on: {
                onChange: (newv, oldv) => {
                    this._showDeletedChenge.trigger(!!newv, this);
                }
            }
        }
    }

    private checkSearchByLegalSubject(baTypeId: number) {
        const lsAlias = "LegalSubject";

        if (!baTypeId)
            return true;

        const entityInfo = Common.entityTypes().getByBATypeId(baTypeId).entityInfo;
        if (entityInfo.mvcAlias === lsAlias)
            return true;

        const lsEntityInfo = Common.entityTypes().getByAliase(lsAlias);
        if (lsEntityInfo.entityInfo.baTypeId === entityInfo.parentBATypeId)
            return true;

        const child = Common.entityTypes().getByParent(lsEntityInfo.entityInfo.baTypeId);

        const filtered = child.filter(item => {
            return item.entityInfo.baTypeId === entityInfo.baTypeId;
        });

        return filtered.length > 0;

        //return checkAsChildren(lsEntityInfo.entityInfo.baTypeId, entityInfo, Common.entityTypes().entitiesInfo);

        //function checkAsChildren(parentId: number,
        //    cEntityInfo: Entities.IEntityInfoModel,
        //    entitiesInfo: Common.SystemEntityTypeInfo[]): boolean {

        //    for (let i = 0; i < entitiesInfo.length; i++) {
        //        const item = entitiesInfo[i];
        //        if (item.entityInfo.parentBATypeId !== parentId)
        //            continue;

        //        if (item.entityInfo.baTypeId === cEntityInfo.parentBATypeId)
        //            return true;

        //        if (checkAsChildren(item.entityInfo.baTypeId, item.entityInfo, entitiesInfo))
        //            return true;
        //    }
        //    return false;
        //}
    }
}
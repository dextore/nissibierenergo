﻿import Api = require("../../Api/ApiExporter");
import MC = require("../../Components/Markers/MarkerControlBase");
import PM = require("../../Components/Markers/PeriodMarkerControlBase");
import Common = require("../../Common/CommonExporter");

export interface IAdvancedSearchDataSource {
    getColumns(baTypeId: number);
    getDataSource(baTypeId: number, showDeleted: boolean,
        filterGroupOperator: HIS.Models.Layer.Models.QueryBuilder.FilteringGroupOperator,
        markerControls: MC.IMarkerControl[],
        companyAreaId: number,
        params): Promise<any>;
    baIdColumnInfo: Querybuilder.ICaptionAndAliaseModel;
}

export function getAdvancedSearchDataSource(dataTable: webix.ui.datatable): IAdvancedSearchDataSource {
    return new AdvancedSearchDataSource(dataTable);
}

class AdvancedSearchDataSource implements IAdvancedSearchDataSource {

    private _hiddenColumnCaption = { "BATypeId": 1, "IsFirstState": 1, "IsDelState": 1 }
    private _hiddenMarkerTypes = [11, 12, 13];
    private _baIdAlias: string = "";
    private _baIdColumnInfo: Querybuilder.ICaptionAndAliaseModel = null;
    private _columnInfo: { [id: string]: Querybuilder.ICaptionAndAliaseModel; } = {};
    private _columns: any[] = [];

    get baIdColumnInfo() {
        return this._baIdColumnInfo;
    }

    constructor(private readonly _dataTable: webix.ui.datatable) {

    }

    getColumns(baTypeId: number) {
        this._columnInfo = {};
        this._columns = [];
        this._baIdAlias = null;
        this._baIdColumnInfo = null;

        return Api.getApiQueryBuilder().getFields(baTypeId).then((data: Querybuilder.ICaptionAndAliaseModel[]) => {

            data.forEach(item => {
                if (item.aliase.indexOf("BAId") >= 0) {
                    this._baIdAlias = item.aliase;
                    this._baIdColumnInfo = item;
                }
                this._columnInfo[item.aliase] = item;
                if (this._hiddenColumnCaption[item.caption]) {
                    return;
                }
                if (this._hiddenMarkerTypes.indexOf(item.markerTypeId) >= 0) {
                    return;
                }
                let columnWidth = webix.html.getTextSize(item.caption, "webix_hcell").width;
                columnWidth = (columnWidth < 100) ? 100 : columnWidth;

                switch (item.type) {
                    case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Boolean:
                    case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Number:
                    case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.String:
                    case HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType.Date:
                        this._columns.push({
                            id: item.aliase,
                            header: item.caption,
                            width: columnWidth,
                            sort: "server"
                        });
                        break;
                }
            });

            return (webix.promise as any).resolve(this._columns);
        });
    } 

    getDataSource(baTypeId: number, showDeleted: boolean,
        filteringsGroupOperator: HIS.Models.Layer.Models.QueryBuilder.FilteringGroupOperator,
        markerControls: MC.IMarkerControl[], 
        companyAreaId: number,
        params): Promise<any> {

        const entityType = Common.entityTypes().getByBATypeId(baTypeId);

        const pager = this.pager;

        const from = (!params) ? 0 : params.start;
        const to = (!params) ? pager.data.size : params.start + params.count;

        const queryParams = {
            baTypeId: baTypeId,
            from: from,
            to: to,
            filteringsGroupOperator: filteringsGroupOperator,
            filteringsFields: [],
            filteringsTreeFields: {
                OrFields: [], 
                AndFields: []
            },
            orderingsFields: [{
                orderingField: this._baIdAlias, sortingOrder: HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Asc
            }],
            needFirstLevelOfTypesHierarchy: true
        };

        //"filteringsTreeFields": {
        //    "OrFields": [],                        //Массивы по аналогии с filteringsFields
        //        "AndFields": null
        //},

        const filterFilds = filteringsGroupOperator === HIS.Models.Layer.Models.QueryBuilder.FilteringGroupOperator.And
            ? queryParams.filteringsTreeFields.AndFields
            : queryParams.filteringsTreeFields.OrFields;

        markerControls.forEach((item: MC.IMarkerControl) => {
            switch (item.markerInfo.markerType) {
                case HIS.Models.Layer.Models.Markers.MarkerType.MtString:
                    filterFilds.push({
                        field: `[${entityType.entityInfo.mvcAlias}//${item.markerInfo.name}]`,
                        filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Like, // LIKE
                        value: (item.control as any).getValue()
                    });
                    break;
                case HIS.Models.Layer.Models.Markers.MarkerType.MtEntityRef:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtAddress:
                    filterFilds.push({
                        field: `[${entityType.entityInfo.mvcAlias}//${item.markerInfo.name}//Name]`,
                        filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Like, // LIKE
                        value: (item.control as any).getValue()
                    });
                    break;
                case HIS.Models.Layer.Models.Markers.MarkerType.MtDateTime:
                    filterFilds.push({
                        field: `[${entityType.entityInfo.mvcAlias}//${item.markerInfo.name}]`,
                        filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal, // =
                        value: webix.i18n.dateFormatStr((item.control as any).getValue())
                    });
                    break;
                case HIS.Models.Layer.Models.Markers.MarkerType.MtMoney:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtBoolean:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtInteger:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtDecimal:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtList:
                case HIS.Models.Layer.Models.Markers.MarkerType.MtReference:
                    filterFilds.push({
                        field: `[${entityType.entityInfo.mvcAlias}//${item.markerInfo.name}]`,
                        filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal, // =
                        value: (item.control as any).getValue().toString()
                    });
                    break;
            }
        });

        if (companyAreaId) {
            filterFilds.push({
                field: `[${entityType.entityInfo.mvcAlias}//CompanyArea]`,
                filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal, // =
                value: companyAreaId.toString()
            });
        }

        if (!showDeleted) {
            queryParams.filteringsTreeFields.AndFields.push({
                field: `[${entityType.entityInfo.mvcAlias}//IsDelState]`,
                filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal,
                value: 0
            });
        }

        return Api.getApiQueryBuilder().getData(queryParams as any).then((items) => {

            const result = JSON.parse(items as any);

            const data = queryParams.from === 0
                ? {
                    data: result.items,
                    pos: 0,
                    total_count: result.rowCount
                }
                : {
                    data: result.items,
                    pos: queryParams.from
                };

            return (webix.promise as any).resolve(data);
        });
    } 

    private get pager() {
        return this._dataTable.getPager();
    }
}
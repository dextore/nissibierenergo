﻿import Base = require("./ISimpleSearchList");
import Api = require("../../Api/ApiExporter");
import Common = require("../../Common/CommonExporter");
import Models = require("./Models");
import Pager = require("../../Components/Pager/Pager");


export function getSimpleSearchListByType(viewName: string, baTypeId: number): Base.ISimpleSearchList {
    return new SimpleSearchListByType(viewName, baTypeId);
}


export class SimpleSearchListByType extends Base.SimpleSearchListBase {

    private _displayValueAliase: string;
    private _baIdAliase: string;

    private _searchValue: string;
    private _showDeleted: boolean;
    private _entityType: Common.SystemEntityTypeInfo; 

    get dataTableId() { return `${this._viewName}-result-datatble-id`; }
    get pagerId() { return `${this._viewName}-datatble-pafer-id`; }

    get dataTable() {
        return $$(this.dataTableId) as webix.ui.datatable;
    }

    get selectedValue(): Models.ISearchDialogResult {
        const rows = this.dataTable.getSelectedItem(true);
        if (!rows.length)
            return null;
        const row = rows[0];

        const markerInfo = this._entityType.entityInfo.markersInfo.filter(item => {
            return item.id === 1;
        })[0];

        return {
            baId: row[this._baIdAliase],
            baTypeId: this._baTypeId,
            name: row[`[${this._entityType.entityInfo.mvcAlias}//${markerInfo.name}]`]
        }
    }

    constructor(
        private readonly _viewName: string,
        private readonly _baTypeId: number) {
        super();
        this._entityType = Common.entityTypes().getByBATypeId(this._baTypeId);
    }

    ready() {
        webix.extend(this.dataTable, webix.ProgressBar);

        Api.getApiQueryBuilder().getFields(this._baTypeId).then((data: Querybuilder.ICaptionAndAliaseModel[]) => {

            var columns = webix.toArray(this.dataTable.config.columns);

            data.forEach(item => {
                if (item.markerId === 1) {
                    columns.push({ id: item.aliase, header: item.caption, fillspace: true });
                    this._displayValueAliase = item.aliase;
                }
                if (item.aliase.indexOf("//BAId]") >= 0) {
                    this._baIdAliase = item.aliase;
                }
            });

            this.dataTable.define("columns", columns);
            this.dataTable.refreshColumns();
        });

    }

    clearSearch() {
        this.dataTable.clearAll();
    }

    showDeleted(value: boolean) {
        this._showDeleted = value;
    }

    search(value: string) {
        this._searchValue = value;
        this.dataTable.load(this.dataTable.config.url);
    }

    init() {
        //20/10
        const pager = Pager.getDataTablePager(this.pagerId);
        return {
            rows: [
                {
                    view: "datatable",
                    id: this.dataTableId,
                    select: "row",
                    scroll: "y",
                    resizeColumn: true,
                    pager: this.pagerId,
                    datafetch: 20,
                    on: {
                        onSelectChange: () => {
                            //Helpers.resetButtons(($$(this.dataTableId) as webix.ui.datatable).getSelectedId(true, false).length > 0, this._toolBar);
                        }
                    },
                    url: {
                        $proxy: true,
                        load: (view, callback, params) => {

                            this.dataTable.clearAll();

                            if (!this._searchValue)
                                return;

                            (this.dataTable as any).showProgress();

                            const pager = this.dataTable.getPager();
                            const from = (!params) ? 0 : params.start;
                            const to = (!params) ? pager.data.size : params.start + params.count;

                            const queryParams = {
                                baTypeId: this._baTypeId,
                                from: from,
                                to: to,
                                filteringsFields: [{
                                    field: this._displayValueAliase,
                                    filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator
                                        .Like,
                                    value: this._searchValue
                                }],
                                orderingsFields: [
                                    {
                                        orderingField: this._displayValueAliase,
                                        sortingOrder: HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel.Asc
                                    }
                                ]
                            };

                            if (!this._showDeleted) {
                                queryParams.filteringsFields.push({
                                    field: `[${this._entityType.entityInfo.mvcAlias}//IsDelState]`,
                                    filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.Equal,
                                    value: "0"
                                });
                            }

                            ($$(this.dataTableId) as webix.ui.datatable).clearAll();
                            Api.getApiQueryBuilder().getData(queryParams as any).then((items) => {

                                const result = JSON.parse(items as any);

                                const data = queryParams.from === 0
                                    ? {
                                        data: result.items,
                                        pos: 0,
                                        total_count: result.rowCount
                                    }
                                    : {
                                        data: result.items,
                                        pos: queryParams.from
                                    };
                                ($$(this.dataTableId) as any).hideProgress();
                                (webix.ajax as any).$callback(view, callback, data);
                            }).catch(() => {
                                ($$(this.dataTableId) as any).hideProgress();
                                (webix.ajax as any).$callback(view, callback, null);
                            });
                        }
                    }
                }, 
                pager.init()
            ]
        };
    }

}
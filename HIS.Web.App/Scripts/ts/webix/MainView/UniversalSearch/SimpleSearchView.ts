﻿import Base = require("../../Components/ComponentBase");
import Models = require("./Models");
import List = require("./ISimpleSearchList");
import ListAll = require("./SimpleSearchListAll");
import ListByType = require("./SimpleSearchListByType");
import SET = require("../../Common/SystemEntitityTypes");

// seach control
import Ctrl = require("../../Controls/SearchControl");

/* простой поиск */

export interface ISimpleSearchView extends Base.IComponent {
    show();
    isVisible: boolean;
    setShowDeleted(value: boolean);
    setSearchState(state: Models.SelectedSearchState);
    getCurrentBaTypes(): Entities.IEntityInfoModel[]; 
    ready();
    selectedValue: Models.ISearchDialogResult;
}

export function getSimpleSearchView(viewName: string, baTypeId: number, searchByLsSubject: boolean): ISimpleSearchView {
    return new SimpleSearchView(viewName, baTypeId, searchByLsSubject);
}

class SimpleSearchView extends Base.ComponentBase implements ISimpleSearchView {

    private _searchState: Models.SelectedSearchState = Models.SelectedSearchState.Name;
    private _showDeleted: boolean = false; 
    private _list: List.ISimpleSearchList;

    get viewId() {
        return `${this._viewName}-view-id`;
    } 

    get controlId() {
        return `${this._viewName}-search-control-id`;
    } 

    get isVisible() {
        return $$(this.viewId).isVisible();
    }

    get viewName() {
        return this._viewName;
    }

    get selectedValue() {
        return this._list.selectedValue;
    }

    constructor(
        private readonly _viewName: string,
        private readonly _baTypeId: number,
        private readonly _searchByLsSubject: boolean) {
        super();
        this._list = (this._searchByLsSubject)
            ? ListAll.getSimpleSearchListAll(`${this._viewName}-list-all`, this._baTypeId)
            : ListByType.getSimpleSearchListByType(`${this._viewName}-list-bytype`, this._baTypeId);
    }

    ready() {
        this._list.ready();
    }

    setShowDeleted(value: boolean) {
        this._showDeleted = value;
        this._list.showDeleted(this._showDeleted);
    }

    setSearchState(state: Models.SelectedSearchState) {
        this._searchState = state;
        if (this._searchByLsSubject) {
            (this._list as ListAll.ISimpleSearchListAll)
                .setSearchType(this._searchState === Models.SelectedSearchState.Inn);
        }
    }

    getCurrentBaTypes(): Entities.IEntityInfoModel[] {
        const result: Entities.IEntityInfoModel[] = [];
        result.push(SET.entityTypes().getByAliase("Person").entityInfo);
        result.push(SET.entityTypes().getByAliase("Organisation").entityInfo);
        return result;
    } 

    show() {
        $$(this.viewId).show();
        this._list.clearSearch();
    }

    init() {
        return {
            id: this.viewId,
            rows: [
                {
                    //view: "toolbar",
                    //type: "header",
                    padding: 10,
                    //borderless: true,
                    height: 50,
                    cols: [
                        {
                            view: Ctrl.viewName,
                            id: this.controlId,
                            css: "protoUI-with-icons-2",
                            align: "right",
                            placeholder: "Значение для поиска..",
                            icons: ["search", "close"],
                            //maxWidth: 300,
                            minWidth: 50,
                            on: {
                                onSearchIconClick: (e) => {
                                    this.toFind();
                                },
                                onCloseIconClick: (e) => {
                                    ($$(this.controlId) as webix.ui.search).setValue("");
                                },
                                onKeyPress: (code, e) => {
                                    if (code === 13) // Enter code
                                        this.toFind();
                                }
                            },

                        }
                    ]
                },
                this._list.init()
            ]
        }
    }

    private toFind() {
        const searchValue = ($$(this.controlId) as webix.ui.search).getValue();
        if (!searchValue)
            return; 
        this._list.search(searchValue);
    }
}

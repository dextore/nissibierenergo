﻿import Base = require("../../Components/ComponentBase");
import Pager = require("../../Components/Pager/Pager");
import Common = require("../../Common/CommonExporter");
import Marker = require("../../Components/Markers/MarkerControlFactory");
import Btns = require("../../Components/Buttons/ToolBarButtons");
import MC = require("../../Components/Markers/MarkerControlBase");
import DS = require("./AdvancedSearchDataSource");
import Models = require("./Models");

/* поиск по выбранным параметрам */

export interface IAdvancedSearchView extends Base.IComponent {
    show();
    setShowDeleted(value: boolean);
    isVisible: boolean;
    getCurrentBaTypes(): Entities.IEntityInfoModel[];
    selectedValue: Models.ISearchDialogResult;
    ready();
}

export function getAdvancedSearchView(viewName: string, baTypeId: number, mainTabbarView: Common.IMainTabbarView): IAdvancedSearchView {
    return new AdvancedSearchView(viewName, baTypeId, mainTabbarView);
}

class AdvancedSearchView extends Base.ComponentBase implements IAdvancedSearchView {

    private _companyAreaMarkerInfo: Markers.IMarkerInfoModel = null; 
    private _selectedEntity: Entities.IEntityInfoModel = null;
    private _ladelWidth: number = CStartLabelWidth;
    private _markerControls: MC.IMarkerControl[] = [];
    private _dataSource: DS.IAdvancedSearchDataSource = null;
    private _showDeleted: boolean = false; 
    private _filterGroupOperator: HIS.Models.Layer.Models.QueryBuilder.FilteringGroupOperator = HIS.Models.Layer.Models.QueryBuilder.FilteringGroupOperator.And;

    private _arrowLeftBtn = Btns.getArrowLeftBtn(this._viewName, "выбрать маркеры", () => {
        this.selectMarkers();
        ($$(this.markersPlaceViewId) as any).expand();
    });

    get viewId() {
        return `${this._viewName}-view-id`;
    } 

    get filterFormId() {
        return `${this._viewName}-filter-form-id`;
    } 

    get markersListId() {
        return `${this._viewName}-markers-list-id`;
    } 

    get baTypeSelectId() {
        return `${this._viewName}-select-id`;
    } 

    get isVisible(): boolean {
        return $$(this.viewId).isVisible();
    }

    get dataTableId(): string {
        return `${this._viewName}-datatable-id`;
    }

    get pagerId(): string {
        return `${this._viewName}-pager-id`;
    }

    get resultViewId(): string {
        return `${this._viewName}-result-view-id`;
    }

    get markersPlaceViewId(): string {
        return `${this._viewName}-markers-place-view-id`;
    }

    get selectedValue() {
        const rows = ($$(this.dataTableId) as webix.ui.datatable).getSelectedItem(true);
        if (!rows.length)
            return null;
        const row = rows[0];

        const markerInfo = this._selectedEntity.markersInfo.filter(item => {
            return item.id === 1;
        })[0];

        return {
            baId: row[this._dataSource.baIdColumnInfo.aliase],
            baTypeId: this._selectedEntity.baTypeId,
            name: row[`[${this._selectedEntity.mvcAlias}//${markerInfo.name}]`] 
        }
    }

    constructor(private readonly _viewName: string,
        private readonly _baTypeId: number,
        private readonly _mainTabbarView: Common.IMainTabbarView) {
        super();
    }

    getCurrentBaTypes(): Entities.IEntityInfoModel[] {
        return [this._selectedEntity];
    }

    setShowDeleted(value: boolean) {
        this._showDeleted = value;
        //if ($$(this.viewId).isVisible()) {
        //    this.searchClick();
        //}
    }

    show() {
        $$(this.viewId).show();
        if (!this._selectedEntity) {
            this.baTypeChanged(this.currentBaTypeId);
        }
        ($$(this.dataTableId) as webix.ui.datatable).clearAll();
    }

    ready() {
        webix.extend($$(this.dataTableId), webix.ProgressBar);
        //var buttonNode = this._arrowLeftBtn.button.getNode();
        //webix.event(buttonNode, "mousedown", (e) => {
        //    this.selectMarkers();
        //    e.preventDefault();
        //    e.stopPropagation();
        //    return false;
        //}); 
    }

    private searchClick() {
        const form = $$(this.filterFormId) as webix.ui.form;

        const elements = form.getChildViews();
        if (!elements.length)
            return; 

        if (!form.validate())
            return; 

        //webix.message("Search click!!!");

        // На вход MarkersInfo[] и  Values!!!
        // Получить поля - baTypeId Api.getApiQueryBuilder().getFields(baTypeId)
        /* Построить фильтра от выбранных значений формы
           Orders baId!
           filter по типу маркера
           1 Логический = да нет
           2 Целочисленный = 
           3 Вещественный = 
           4 Символьный like
           5 Перечень = itemId
           6 Денежный = 
           10 Дата и время = 
           11 Ссылка на сущьность Name like
           12 Адрес ссылка на сущьность адрес Name like
           13 Справочник Name like ?
         */

        const dataTable = $$(this.dataTableId) as webix.ui.datatable;
        dataTable.clearAll();

        this._dataSource.getColumns(this.currentBaTypeId).then(columns => {
            dataTable.define("columns", columns);
            dataTable.refreshColumns();
            dataTable.load(dataTable.config.url);
            ($$(this.resultViewId) as any).expand();
        });
    }

    private getEntytyTypeSearchList() {
        if(!!this._baTypeId) {
            const entityInfo = Common.entityTypes().getByBATypeId(this._baTypeId).entityInfo;
            const result = [{ id: entityInfo.baTypeId, value: entityInfo.name }];

            const childEntityType = Common.entityTypes().getByParent(entityInfo.baTypeId);
            childEntityType.forEach(item => {
                result.push({ id: item.entityInfo.baTypeId, value: item.entityInfo.name });
            });

            return result;
        }

        const legalSubject = Common.entityTypes().getByAliase(CLegalSubject);
        const document = Common.entityTypes().getByAliase(CDocumentAliase);
        const lsEntities = Common.entityTypes().getByParent(legalSubject.entityInfo.baTypeId);
        const docEntities = Common.entityTypes().getByParent(document.entityInfo.baTypeId);
        return lsEntities.concat(docEntities as any).map(item => {
            return { id: item.entityInfo.baTypeId, value: item.entityInfo.name }
        });;
    }

    private baTypeChanged(baTypeId: number) {
        this._selectedEntity = Common.entityTypes().getByBATypeId(baTypeId).entityInfo;

        this.clearFilterMarkers();
        this.clearEntityMarkers();
        this.loadEntityMarkers();
        this.selectDefaultMarker();

        this._dataSource = DS.getAdvancedSearchDataSource($$(this.dataTableId) as webix.ui.datatable);
    }

    private clearFilterMarkers() {
        const form = $$(this.filterFormId) as webix.ui.form;
        const elements = form.getChildViews();
        const items = elements.map(item => {
            return item;
        });
        items.forEach(item => {
            form.removeView(item);
        });
        this._markerControls = [];
    }

    private clearEntityMarkers() {
        ($$(this.markersListId) as webix.ui.list).clearAll();
    }

    private prepareMarkerInfo(markerInfo: Markers.IMarkerInfoModel): Markers.IMarkerInfoModel {
        const info = webix.copy(markerInfo);
        info.isRequired = true;
        return info;
    }

    private loadEntityMarkers() {
        if (!this._selectedEntity)
            return;

        this._ladelWidth = CStartLabelWidth;
        const list = $$(this.markersListId) as webix.ui.list;

        const merkersInfo = (this._filterGroupOperator ===
                HIS.Models.Layer.Models.QueryBuilder.FilteringGroupOperator.And)
            ? this._selectedEntity.markersInfo.filter(item => {
                return item.id !== 1 && !item.isCollectible && item.name !== "CompanyArea";
            })
            : this._selectedEntity.markersInfo;

        const filtered = this._selectedEntity.markersInfo.filter(item => {
            return item.name === "CompanyArea";
        });

        this._companyAreaMarkerInfo = filtered.length ? filtered[0] : null;

        const data = merkersInfo.map(item => {
            const ladelWidth = webix.html.getTextSize(item.label, "webix_inp_label ").width + 10;
            
            if (ladelWidth > this._ladelWidth)
                this._ladelWidth = ladelWidth;

            return {
                id: item.id,
                value: item.label,
                marker: this.prepareMarkerInfo(item)
            };
        });

        list.parse(data, "json");
    }

    private selectDefaultMarker() {
        const markerInfo = this._selectedEntity.markersInfo.filter(item => {
            return item.id === 1;
        })[0];
        this.selectMarker(this.prepareMarkerInfo(markerInfo));
    }

    private selectMarkers() {
        const list = $$(this.markersListId) as webix.ui.list;
        const items = list.getSelectedItem(true);
        if (!items.length)
            return;

        items.forEach(item => {
            const info = item.marker as Markers.IMarkerInfoModel;
            this.selectMarker(info);
            //if (this.)
            if (this._filterGroupOperator ===
                HIS.Models.Layer.Models.QueryBuilder.FilteringGroupOperator.And) {
                list.remove(item.id);
            }
        });
    }

    private selectMarker(markerInfo: Markers.IMarkerInfoModel) {
        const id = webix.uid().toString();
        const ctrlId = `${this._viewName}-ctrl-${id}-id`;
        const ctrl = Marker.MarkerControlfactory.getControl(`${this._viewName}-${id}`, markerInfo,
            () => { return { labelWidth: this._ladelWidth } });
        ctrl.forFilter = true;
        this._markerControls.push(ctrl);
        const self = this;
        const btn = Btns.getRemoveBtn(`${this._viewName}-${id}`,
            "Удалить из фильтра",
            () => {
                ($$(this.filterFormId) as webix.ui.form).removeView($$(ctrlId));
                if (this._filterGroupOperator ===
                    HIS.Models.Layer.Models.QueryBuilder.FilteringGroupOperator.And) {
                    ($$(this.markersListId) as webix.ui.list).add({
                        id: markerInfo.id,
                        value: markerInfo.label,
                        marker: webix.copy(markerInfo)
                    });
                    self._markerControls = self._markerControls.filter(item => {
                        return item !== ctrl;
                    });
                }
            });
        const element = {
            id: ctrlId, 
            cols: [
                ctrl.init(),
                btn.init()//,
                //{width: 50}
            ]
        };

        ($$(this.filterFormId) as webix.ui.form).addView(element);
        btn.button.enable();
        (ctrl.control as any).attachEvent("onKeyPress",
            (code, e) => {
                if (code === 13) // Enter code
                    this.searchClick();
            });
    }


    init() {
        return {
            id: this.viewId,
            view: "accordion",
            type: "space",
            rows: [
                {
                    header: "Настройка поиска",
                    body: this.initFilterBody()
                }, {
                    header: "Результат поиска",
                    id: this.resultViewId, 
                    collapsed: true,
                    body: this.initResultBody()
                }
            ]
        }
    }

    initFilterBody() {
        return {
            rows: [
                {
                    view: "toolbar",
                    height: 40,
                    cols: [
                        {
                            rows: [
                                { height: 5 },
                                {
                                    view: "radio",
                                    align: "center",
                                    label: "Объединять по...",
                                    labelWidth: 130,
                                    width: 300,
                                    value: 1,
                                    options: [
                                        { "id": 1, "value": "И" },
                                        { "id": 2, "value": "ИЛИ" }
                                    ],
                                    on: {
                                        onChange: (newv, oldv) => {
                                            (+newv === 1)
                                                ? this._filterGroupOperator = HIS.Models.Layer.Models.QueryBuilder
                                                .FilteringGroupOperator.And
                                                : this._filterGroupOperator = HIS.Models.Layer.Models.QueryBuilder
                                                .FilteringGroupOperator.Or;
                                            this.baTypeChanged(this._selectedEntity.baTypeId);
                                        }
                                    }
                                }
                            ]
                        }, {
                            width: 20,
                        }, {
                            view: "combo",
                            id: this.baTypeSelectId,
                            label: "Искать в",
                            value: !!this._baTypeId ? this._baTypeId : 11, //TO DO переделать,
                            width: 400,
                            //disabled: !!this._baTypeId,
                            options: this.getEntytyTypeSearchList(),
                            on: {
                                onChange: (newv, oldv) => {
                                    this.baTypeChanged(+newv);
                                }
                            }
                        }, {
                        }, {
                            view: "button",
                            type: "iconButton",
                            icon: "search",
                            width: 32,
                            align: "left",
                            tooltip: "Искать",
                            on: {
                                onItemClick: (id, e) => {
                                    this.searchClick();
                                }
                            }
                        }
                    ]
                }, {
                    view: "accordion",
                    //type: "space",
                    cols: [
                        {
                            body: {
                                rows: [
                                    {
                                        view: "scrollview",
                                        scroll: "auto",
                                        body: {
                                            view: "form",
                                            id: this.filterFormId,
                                            paddingX: 30,
                                            elements: []
                                        }
                                    }, { gravity: 0.001 }
                                ]
                            }
                        }, {
                            header: "Маркеры",
                            id: this.markersPlaceViewId,
                            //collapsed: true,
                            body: {
                                cols: [
                                    {
                                        width: 30,
                                        css: "webix_accordionitem_header",
                                        //borderless: false,
                                        rows: [
                                            {},
                                            this._arrowLeftBtn.init(),
                                            {}
                                        ]
                                    }, {
                                        view: "list",
                                        id: this.markersListId,
                                        select: true,
                                        multiselect: true,
                                        on: {
                                            onSelectChange: () => {
                                                ($$(this.markersListId) as webix.ui.list).getSelectedId(true).length > 0
                                                    ? this._arrowLeftBtn.button.enable()
                                                    : this._arrowLeftBtn.button.disable();
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    ]
                }
            ]
        };
    }

    initResultBody() {
        //15/3
        const pager = Pager.getDataTablePager(this.pagerId);
        const datatable = {
            view: "datatable",
            id: this.dataTableId,
            select: "row",
            //scroll: "y",
            resizeColumn: true,
            pager: this.pagerId,
            datafetch: 15,
            url: {
                $proxy: true,
                load: (view, callback, params) => {
                    if (!this._dataSource)
                        return;

                    const companyId = this._mainTabbarView.getSelectedCompany().id;

                    this._dataSource.getDataSource(
                        this.currentBaTypeId,
                        this._showDeleted,
                        this._filterGroupOperator,
                        this._markerControls,
                        (this._companyAreaMarkerInfo) ? companyId : null,
                        params).then(result => {
                            (webix.ajax as any).$callback(view, callback, result);
                    });
                }
            }
        };
        return {
            rows: [
                datatable,
                pager.init()
             
            ]
        }
    }

    private get currentBaTypeId() {
        //return (!this._baTypeId) ? +($$(this.baTypeSelectId) as webix.ui.search).getValue() : this._baTypeId;
        return ($$(this.baTypeSelectId) as webix.ui.search).getValue() as any;
    }
}

const CLegalSubject = "LegalSubject";
const CDocumentAliase = "Document";
const CStartLabelWidth = 200;

﻿export enum SelectedSearchState {
    Name = 1,
    Inn = 2,
    Ower = 3
}

export interface ISearchDialogResult {
    baId: number;
    baTypeId: number;
    name: string; 
}

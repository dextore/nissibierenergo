﻿import Base = require("../../Components/ComponentBase"); 
import Toolbar = require("./ToolbarView");
import SimpleView = require("./SimpleSearchView");
import AdvancedView = require("./AdvancedSearchView");
import Models = require("./Models");
import SET = require("../../Common/SystemEntitityTypes");
import Common = require("../../Common/CommonExporter");

export interface ISearchView extends Base.IComponent {
    ready();
    getCurrentBaTypes(): Entities.IEntityInfoModel[];
    selectedValue: Models.ISearchDialogResult;
}

export function getSearchView(viewName: string, baTypeId: number, mainTabbarView: Common.IMainTabbarView): ISearchView {
    return new SearchView(viewName, baTypeId, mainTabbarView);
}

class SearchView extends Base.ComponentBase implements ISearchView {

    // toolbar
    private _toolbar: Toolbar.IToolbarView;
    // simple search view
    private _simpleView: SimpleView.ISimpleSearchView;
    // ext search view
    private _advancedView: AdvancedView.IAdvancedSearchView;

    get selectedValue() {
        if (this._advancedView.isVisible) {
            return this._advancedView.selectedValue;
        }
        return this._simpleView.selectedValue;
    }

    constructor(private readonly _viewName: string,
        private readonly _baTypeId: number,
        private readonly _mainTabbarView: Common.IMainTabbarView) {
        super();
        this._toolbar = Toolbar.getToolbarView(`${this._viewName}-toolbar-id`, this._baTypeId);

        this._simpleView = SimpleView.getSimpleSearchView(`${this._viewName}-simple-search`,
            this.isLegalSubject(this._baTypeId) ? null : this._baTypeId,
            this._toolbar.searchByLegalSubject);
        this._advancedView = AdvancedView.getAdvancedSearchView(`${this._viewName}-advanced-search`, _baTypeId, _mainTabbarView);
        this.subscribe(this._toolbar.searchStateChenge.subscribe((args: Models.SelectedSearchState) => {
            this.searchStateChange(args);
        }, this));

        this.subscribe(this._toolbar.showDeletedChenge.subscribe((args: boolean) => {
            this.showDeletedChange(args);
        }, this));
    }

    ready() {
        this._advancedView.ready();
        this._simpleView.ready();
        if (!!this._baTypeId)
            this._advancedView.show();
    }

    getCurrentBaTypes(): Entities.IEntityInfoModel[] {
        if (this._baTypeId) {
            const entityInfo = SET.entityTypes().getByBATypeId(this._baTypeId).entityInfo;
            return [entityInfo];
        }

        if (this._simpleView.isVisible)
            return this._simpleView.getCurrentBaTypes();
        return this._advancedView.getCurrentBaTypes();
    }

    private searchStateChange(state: Models.SelectedSearchState) {
        this._simpleView.setSearchState(state);
        if (state === Models.SelectedSearchState.Ower) {
            if (!this._advancedView.isVisible)
                this._advancedView.show();
            return;
        }
        if (!this._simpleView.isVisible)
            this._simpleView.show();
    }

    private showDeletedChange(value: boolean) {
        this._simpleView.setShowDeleted(value);
        this._advancedView.setShowDeleted(value);

    }

    private isLegalSubject(baTypeId: number): boolean {
        if (!baTypeId)
            return false;
        return Common.entityTypes().getByBATypeId(baTypeId).entityInfo.mvcAlias === "LegalSubject";
    }

    init() {
        return {
            borderless: true,
            rows: [
                this._toolbar.init(),
                {
                    view: "multiview",
                    animate: false,
                    cells: [
                        this._simpleView.init(),
                        this._advancedView.init()
                    ]
                }
            ],
            on: {
                onDestruct: () => {
                    this.destroy();
                }
            }
        };
    }
}
﻿import Api = require("../Api/ApiExporter");
import Common = require("../Common/CommonExporter")
import Modules = require("../Modules/ModuleExporter");
//import Form = require("../Components/Forms/FormExporter");
import CompanyControl = require("./MainCompanyControl");
import Search = require("./MainSearchControl");
import MH = require("../Common/ModuleHelper");

import TM = require("./TreeView/MainTreeModulesView");
import Tree = require("./TreeView/MainTreeView");
import PA = require("./PersonAreaView");

export interface IMainView {
    init();
    afterRender();
}

export function getMainForm() {
    if (!mainView) {
        mainView = new MainView();
    }
    return mainView;
}

let mainView: MainView;

const viewName = "hisapp-main-view";

class MainView implements IMainView, Common.IMainTabbarView {

    private _subscriberList: Common.ISubscription[] = [];
    private _searchControl: Search.IMainSearchControl;
    private _companyControl: CompanyControl.IMainCompanyControl;
    private _treeModules = TM.getMainTreeModulesView(this.viewName);
    private _eventQueue = new Common.Event<Common.IEventQueueMessageArg>();

    private _modulesInfo: { [id: string]: MainMenu.IMenuItemModel } = {};
    private _modulesInstance: { [id: string]: Common.IMainTab } = {};

    get viewName() { return viewName; }
    get viewId() { return `${this.viewName}-id`; }
    get toolBarId() { return `${this.viewName}-tooldar-id`; }
    get placeHolderId() { return `${this.viewName}-place-holder-id`; }
    get mainMenuId() { return `${this.viewName}-main-menu-id`; }
    get tabViewId() { return `${this.viewName}-tabview-id`; }
    get statusbarId() { return `${this.viewName}-statusbar-id`; }

    get buttonPersonalAreaId() { return `${this.viewName}-button-personal-area-id`; }
    get buttonHelpId() { return `${this.viewName}-button-help-id`; }

    get eventQueue(): Common.Event <Common.IEventQueueMessageArg> {
        return this._eventQueue;
    }

    constructor() {
        this._searchControl = Search.getMainSearchControl(this.viewName, this);
        this._searchControl.searchEvent.subscribe((entity) => {
            const treeItem = {
                baId: entity.baId,
                baTypeId: entity.typeId,
                mvcAlias: "",
                baTypeName: entity.typeName,
                baTypeNameShort: entity.typeName,
                children: [],
                name: entity.name,
                stateId: entity.stateId,
                stateName: entity.stateName
            } as any;    
            this._treeModules.selectTreeItem(treeItem);
            },
            this);

        this._companyControl = CompanyControl.getMainCompanyControl(this.viewName, this);
        // отмена закрития диалога по esc!!!
        webix.UIManager.removeHotKey("esc");
        this.subscribeToEvent(this.eventQueue.subscribe(this.messageQueueEventHandler, this));
    }

    disableCompanyChange() {
        this._companyControl.disable();
    }

    enableCompanyChange() {
        this._companyControl.enable();
    }

    afterRender() {
        const self = this;
        webix.extend($$(this.viewId), webix.ProgressBar);

        (($$(this.viewId) as any) as webix.ProgressBar).showProgress();

        (webix.promise.all([Api.getApiMainMenu().getMainMenu(), Common.initData()] as any) as any)
            .then(results => {
                const items = results[0];

                if (!items || !items.length)
                    return;
                const mainMenu = $$(self.mainMenuId) as webix.ui.menu;

                items.forEach(item => {
                    mainMenu.data.add({
                        id: (item.moduleName) ? item.moduleName : item.id,
                        value: item.text,
                        submenu: getSubmenuItems(item)
                    });
                    if (item.moduleName)
                        self._modulesInfo[item.moduleName] = item;
                });

                MH.ModuleHelper.init(items);
                // remove afrer
                //this.menuItemClick("real-scheme-element-module");
                //this.menuItemClick("banks-module");
                //this.menuItemClick("banks-module-test");
                //this.menuItemClick("document-move-assets-module");
                //this.menuItemClick("references-inventory-module");
                (($$(this.viewId) as any) as webix.ProgressBar).hideProgress();
            });

        /// Load tree
        const tree = Tree.getMainTreeView(`${this.viewName}-main-tree-view`, "Главное дерево");
        this._treeModules.addTree(tree);
        this._treeModules.selectTree(tree.id);
        tree.afterRender();

        function getSubmenuItems(itemModel: MainMenu.IMenuItemModel) {
            if (!itemModel.items || !itemModel.items.length)
                return null;

            return itemModel.items.map(item => {
                let moduleName = (item.moduleName) ? item.moduleName : null;
                let moduleValue = item.text;
                
                if (item.name === "Administration-Entitys-Href") {
                    moduleName = /href=\'(.*?)\'>/g.exec(item.text)[1];
                    moduleValue = /(?!>)([^><]+)(?=<\/a>)/g.exec(item.text)[0];
                }

                if (moduleName)
                    self._modulesInfo[moduleName] = item;

                return {
                    id: (moduleName) ? moduleName : item.id,
                    value: moduleValue,
                    autowidth: true,
                    submenu: getSubmenuItems(item)
                }
            });

            /*
            return itemModel.items.map(item => {
                const moduleName = (item.moduleName) ?
                    item.moduleName :
                    (itemModel.name === "asp_net_hermes") ? /href=\'(.*?)\'>/g.exec(item.text)[1] : null;
                if (moduleName)
                    self._modulesInfo[moduleName] = item;
                return {
                    id: (moduleName) ? moduleName : item.id,
                    value: itemModel.name === "asp_net_hermes" ? /(?!>)([^><]+)(?=<\/a>)/g.exec(item.text)[0] : item.text,
                    autowidth: true,
                    submenu: getSubmenuItems(item)
                }
            });

            */
        }
    }

    sendTreeItem(entity: Entity.IFullTextSearchEntityInfoModel) {
        this._searchControl.searchEvent.trigger(entity);
    }

    // показывает таб
    showModule(moduleId: string): Common.IMainFormTab {

        const moduleItem = this._modulesInfo[moduleId];

        if (!moduleItem) {
            return null;
        }

        if (moduleItem.text.indexOf("href") >= 0) {
            //webix.message(`Переход по ссылке: ${id}`);
            window.open(moduleId, "_blank");
            return null;
        }

        return this.tryLoadModule(moduleItem) as Common.IMainFormTab;
    }

    // показывает форму
    showForm(formId: string, builder: () => Common.IMainTab): Common.IMainFormTab {
        return this.tryCreateTab(formId, builder) as Common.IMainFormTab;
        //return tab as Common.IMainFormTab;
    }

    // закрывает таб
    closeForm(tabId: string) {
        if (!$$(tabId))
            return;

        ($$(this.tabViewId) as webix.ui.tabview).removeView(tabId);
        this._modulesInstance[tabId] = null;
    }

    private menuItemClick(id: any) {
        this.showModule(id);
    }

    private tryLoadModule(moduleItem: MainMenu.IMenuItemModel): Common.IMainTab {

        const moduleId = `${moduleItem.moduleName}-id`;
        return this.tryCreateTab(moduleId, () => { return Modules.ModuleManager.getNewInstance(moduleItem.moduleName, this) });
    }

    getSelectedCompany() {
        return this._companyControl.selectedValue;
    }

    private tryCreateTab(tabId: string, builder: () => Common.IMainTab): Common.IMainTab {
        const self = this;

        if (!$$(tabId)) {
            try {
                const tab = builder();
                if (!tab) {

                    webix.message("Модуль не зарегистрирован в системе");
                    return null;
                }

                initTabView(tabId, tab);

            } catch (e) {
                webix.message(`Ошибка загрузки модуля: ${e.message}`, `error`, 2000);
                console.log(e);
                return null;
            }
        }
        ($$(self.tabbarId) as webix.ui.tabbar).setValue(tabId);
        this.afterTabSelected(tabId);

        function initTabView(tabId: string, tab: Common.IMainTab) {

            self._modulesInstance[tabId] = tab;

            (tab.isTreeNeeded)
                ? self._treeModules.addTabItem(tab)
                : ($$(self.itemsViewId) as webix.ui.multiview).addView(tab.init());

            // Добавить таб - обязательно после модуля
            ($$(self.tabbarId) as any).addOption({ id: tabId, value: tab.header, close: true, width: webix.html.getTextSize(tab.header, "webix_item_tab").width + 50, });

            if (tab.isTreeNeeded) {
                tab.subscribe(self._treeModules.treeSelectedChange.subscribe(tab.treeSelectedChangeHandler, tab));
            } else {
                tab.subscribe(self._searchControl.searchEvent.subscribe(tab.systemSearchHandler, tab));
            }
            tab.subscribe(self._companyControl.controlEvent.subscribe(tab.systemCompanyHandler, tab));
            tab.systemCompanyHandler(self._companyControl.selectedValue);
            tab.ready();
            if (tab.isTreeNeeded) {
                tab.treeSelectedChangeHandler(self._treeModules.currentTree.selectedItem);
            }
        }
    }

    private tryCloseModule(tabId: string) {
        const self = this;
        const module = this._modulesInstance[tabId];

        if (!module)
            return;

        webix.confirm({
            title: "Подтверждение закрытия модуля",
            text: `Закрыть модуль: "${module.header}"?`,
            ok: "Да",
            cancel: "Нет",
            callback: (result) => {
                if (result) {
                    ($$(self.tabbarId) as webix.ui.tabbar).removeOption(tabId);
                    // простые модули
                    $$(tabId).destructor();

                    if (($$(self.tabbarId) as any).data.options.length === 0) {
                        $$(this.itemsViewId).show();
                    }
                }
            }
        });
    }

    private afterTabUnSelected(tabId: string) {
        if (!tabId)
            return; 
        const instance = this._modulesInstance[tabId];
        instance.onHide();
    }

    private afterTabSelected(tabId: string) {
        if (!tabId)
            return; 
        const instance = this._modulesInstance[tabId];
        instance.onShow();
        instance.isTreeNeeded ? $$(this._treeModules.viewId).show() : $$(this.itemsViewId).show();
        $$(tabId).show();
    }

    private showPersonalAreaPopup() {

        const popup = PA.getPersonAreaView(this.viewName,
            () => {
                $$(this.viewId).destructor();
            });

        popup.show($$(this.buttonPersonalAreaId));
    }

    private showHelp() {
        webix.alert({
            title: "Помощь",
            ок: `Закрыть`,
            text: "Здесь вам... помогут!"
        });
    }

    private get tabbarId() { return `${this.viewName}-tabbar-id`; }
    private get mainViewId() { return `${this.viewName}-main-view-id`; }
    private get itemsViewId() { return `${this.viewName}-items-view-id`; }
    //private get treeItemsViewId() { return `${this.viewName}-tree-items-view-id`; }
    private get moduleViewId() { return `${this.viewName}-module-view-id`; }

    private messageQueueEventHandler(args: Common.IEventQueueMessageArg) {
        if (args.subscriber === this)
            return;

        switch (args.messageType) {
            case Common.QueueMessageType.SearchEvent:
                this._searchControl.searchEvent.trigger(args.messageArgs);
                break;
            case Common.QueueMessageType.DisableOrganisation:
                this._companyControl.disable();
                break;
            case Common.QueueMessageType.EnableOrganisation:
                this._companyControl.enable();
                break;
            default:
                break;
        }
    }

    private destroy() {
        this._subscriberList.forEach(subscriber => {
            subscriber.unsubscribe();
        });
    }

    private subscribeToEvent(subscription: Common.ISubscription): Common.ISubscription {
        this._subscriberList.push(subscription);
        return subscription;
    }

    private unsubscribeFromEvent(subscription: Common.ISubscription) {
        const idx = this._subscriberList.indexOf(subscription);
        if (idx >= 0) {
            this._subscriberList.slice(idx, 1);
        }
        subscription.unsubscribe();
    }

    init() {

        // tab tar - только закладки.
        const tabBar = {
            view: "tabbar",
            id: this.tabbarId,
            optionWidth: 100,
            close: true,
            options: [],
            on: {
                //onAfterTabClick: (tabId) => {
                //    this.afterTabSelected(tabId);
                //},
                onBeforeTabClose: (id, e) => {
                    this.tryCloseModule(id);
                    return false; // для отмены действия по умолчанию!
                },
                onChange: (newv, oldv) => {
                    //webix.message(`Value changed from: "${oldv}to: "${newv}`);
                    this.afterTabUnSelected(oldv);
                    this.afterTabSelected(newv);
                }
            }
        };

        const mainMultiView = {
            view: "multiview",
            id: this.mainViewId,
            animate: false, 
            cells: [
                {
                    view: "multiview",
                    id: this.itemsViewId,
                    animate: false, 
                    cells: [{}]
                }, 
                   this._treeModules.init()
                ]
        }

        const mainToolbar = {
            cletype: "clean",
            cols: [{
                view: "toolbar",
                id: this.toolBarId,
                elements: [
                    {
                        view: "menu",
                        id: this.mainMenuId,
                        borderless: true,
                        autowidth: true,
                        minWidth: "500",
                        data: [],
                        on: {
                            onMenuItemClick: (id) => { this.menuItemClick(id); }
                        }
                    },
                    {
                        rows: [this._companyControl.init(), {}]
                    },
                    {
                        rows: [this._searchControl.init(),
                               {
                                   view: "label",
                                   label: "<label class='behavior-as-anchor code_ref'>Расширенный поиск</label>",
                                   inputWidth: 200,
                                   align: "center",
                                   on: {
                                       onItemClick: (ev, id) => {
                                           this._searchControl.advancedSearch();
                                       }
                                   }
                               }]
                    },
                    {
                        view: "button",
                        id: this.buttonPersonalAreaId,
                        type: "icon",
                        icon: "cog",
                        width: 30,
                        align: "right",
                        tooltip: "Личный кабинет",
                        click: (e) => {
                            this.showPersonalAreaPopup();
                        }
                    }, {
                        view: "button",
                        id: this.buttonHelpId,
                        type: "icon",
                        icon: "question",
                        width: 30,
                        align: "right",
                        tooltip: "Помощь",
                        click: (e) => {
                            this.showHelp();
                        }
                    }
                ]
            }
            ]
        }

        const mainStatusbar = {
            view: "template",
            id: this.statusbarId,
            height: 30,
            template: "<div style='background-color: lavender; text-align: center;'><b>Status Bar</b></div>"
        };

        return {
            view: "layout",
            id: this.viewId,
            //rows: [mainToolbar, { view: "template", type: "clean", height: 5 }, mainMultiView, mainStatusbar]
            rows: [mainToolbar, tabBar, { view: "template", type: "clean", height: 5 }, mainMultiView, mainStatusbar],
            on: {
                onDestruct: () => {
                    try {
                        this.destroy();
                        mainView = null;
                    } catch (e) {
                        console.error(e);
                    }
                },
            }

        };
    }
} 

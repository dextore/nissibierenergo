﻿import Api = require("../Api/ApiExporter");

export interface IPersonAreaView {
    show(parentControl: webix.ui.baseview);
}

export function getPersonAreaView(viewName: string, logoffCallback: () => void) {
    return new PersonAreaView(viewName, logoffCallback);
}

class PersonAreaView implements IPersonAreaView {

    get viewName() {
        return `${this._viewName}-personal-area-popup`; 
    }

    get areaId() {
        return `${this.viewName}-id`; 
    }

    get userNameLabelId() {
        return `${this.viewName}-current-user-name-label-id`; 
    }

    get buttonPersonalAreaId() {
        return `${this.viewName}-button-personal-area-id`; 
    }

    constructor(private readonly _viewName: string,
        private readonly _logoffCallback: () => void) {

    }

    show(parentControl: webix.ui.baseview) {
        if ($$(this.areaId))
            return; 
        this.createPopup();
        ($$(this.areaId) as webix.ui.popup).show(parentControl.$view, { pos: "bottom" });
    }

    private logoffBtnClick() {
        Api.getApiAuthentication().logoff().then(() => {
            ($$(this.areaId) as webix.ui.popup).close();
            this._logoffCallback();
        });
    }

    private popupHide() {
        ($$(this.areaId) as webix.ui.popup).close();
    }

    private popupShow() {
        Api.getApiAuthentication().getUserInfo().then(userInfo => {
            if (userInfo !== null) {
                ($$(this.userNameLabelId) as webix.ui.label).setValue(userInfo.userName);
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    private createPopup() {
        webix.ui({
            view: "popup",
            id: this.areaId,
            height: 200,
            width: 250,
            position: "center",
            body: {
                rows: [{ template: "" },
                    { view: "label", id: this.userNameLabelId, value: "" },
                    {
                        view: "button", value: "Выход",
                        click: webix.bind(this.logoffBtnClick, this)
                    }
                ]
            },
            on: {
                onHide: webix.bind(this.popupHide, this),
                onShow: webix.bind(this.popupShow, this)
            }
        });
    }

}
﻿import Main = require("../IMainTabsView");
import Subscriber = require("../../Common/EventSubscriberBase");
import Dialog = require("../../Common/DialogBase");
import Common = require("../../Common/CommonExporter");


//import Common = require("../../Common/CommonExporter");

export interface IMainTab extends Subscriber.IEventSubscriber {
    viewId: string;
    viewName: string;
    header: string;
    mainTabView: Main.IMainTabbarView;
    init();
    ready();
    destruct();
    onHide();
    onShow();
    addContentWindow(windowId: string);
    removeContentWindow(windowId: string);
    closeSlider(dialog: Dialog.IDialog);
    treeSelectedChangeHandler(entity: Tree.ISystemTreeItemModel);
    systemSearchHandler(entity: Entity.IFullTextSearchEntityInfoModel);
    systemCompanyHandler(company: Inventory.ILSCompanyAreas);
    isTreeNeeded: boolean;
    disableCompany();
    enableCompany();
    addNew();
}

export abstract class MainTabBase extends Subscriber.EventSubscriberBase  implements IMainTab {

    protected static _viewName = "module-base";
    protected static _header = "";
    private _modalDispetcher: ModuleModalDispetcher;  


    static get viewId(): string {
        return `${this._viewName}-id`;
    }

    get viewId(): string {
        return `${(this.constructor as typeof MainTabBase)._viewName}-id`;
    }

    static get viewName(): string {
        return this._viewName;
    }

    get viewName(): string {
        return `${(this.constructor as typeof MainTabBase)._viewName}`;
    }

    static get header(): string {
        return this._header;
    }

    get header(): string {
        return `${(this.constructor as typeof MainTabBase)._header}`;
    }

    get mainTabView() {
        return this._mainTabView;
    }

    get isTreeNeeded() {
        return this.getIsTreeNeeded();
    };

    protected constructor(private _mainTabView: Main.IMainTabbarView) {
        super();
        //this._modalDispetcher = new ModuleModalDispetcher(this.viewId);
    }

    addNew() { }

    treeSelectedChangeHandler(entity: Tree.ISystemTreeItemModel) {

    }

    systemSearchHandler(entity: Entity.IFullTextSearchEntityInfoModel) {

    }

    systemCompanyHandler(company: Inventory.ILSCompanyAreas) {
   
    }

    disableCompany() {
        this._mainTabView.eventQueue.trigger({
            subscriber: this,
            messageType: Common.QueueMessageType.DisableOrganisation,
            messageArgs: null
        }, this);
    }

    enableCompany() {
        this._mainTabView.eventQueue.trigger({
            subscriber: this,
            messageType: Common.QueueMessageType.EnableOrganisation,
            messageArgs: null
        }, this);
    }

    init() {
        const self = this;
        this._modalDispetcher = new ModuleModalDispetcher(self.viewId);

        return {
            view: "multiview",
            id: self.viewId,
            animate: false,
            cells: [self.getContent()],
            on: {
                onDestruct: () => {
                    self.destruct();
                }
            }
        }
    }

    abstract getContent(): any;

    protected getIsTreeNeeded() {
        return false;
    }

    ready() {

    }

    destruct() {
        this._modalDispetcher.destruct();
    }

    onHide() {
        this._modalDispetcher.hide();
    };

    onShow() {
        this._modalDispetcher.show();
    };

    addContentWindow(windowId: string) {
        this._modalDispetcher.addModuleModal(windowId);
    }

    removeContentWindow(windowId: string) {
        this._modalDispetcher.removeModuleModal(windowId);
    }

    closeSlider(dialog: Dialog.IDialog) {
        const items = ($$(this.viewId) as webix.ui.multiview).getChildViews() as webix.ui.view[];

        const filtered = items.filter(item => {
            return item.config.id === dialog.viewId;
        });

        if (!filtered.length)
            return; 

        ($$(this.viewId) as webix.ui.multiview).removeView(dialog.viewId);
    }
}

class ModuleModalDispetcher {
    private _contentWindows: string[] = [];  
    constructor(private readonly _parentViewId: string){
    }

    addModuleModal(windowId: string) {
        if (!this._contentWindows.length) {
            $$(this._parentViewId).disable();
        }
        this._contentWindows.forEach(item => {
            ($$(item) as webix.ui.window).disable();
        });
        this._contentWindows.push(windowId);
    }

    removeModuleModal(windowId: string) {
        const index = this._contentWindows.indexOf(windowId);

        if (index !== -1) {
            this._contentWindows.splice(index, 1);
        }

        if (!this._contentWindows.length) {
            $$(this._parentViewId).enable();
            return;
        }

        (($$(this._contentWindows[this._contentWindows.length - 1])) as webix.ui.window).enable();
    }

    destruct() {
        while (this._contentWindows.length) {
            ($$(this._contentWindows[this._contentWindows.length - 1]) as webix.ui.window).close();
        }
    }

    hide() {
        this._contentWindows.forEach(item => {
            ($$(item) as webix.ui.window).hide();
        });
    };

    show() {
        this._contentWindows.forEach(item => {
            ($$(item) as webix.ui.window).show();
        });
    };

} 
﻿import Base = require("./MainTabBase");
import Common = require("../../Common/CommonExporter");
import Form = require("../../Components/Forms/FormExporter");

export interface IMainFormTab extends Base.IMainTab {
    form: Form.ITabForm
}


export class MainFormTab extends Base.MainTabBase {

    get viewId(): string {
        return this._form.tabViewId;
    }

    get header(): string {
        return this._form.tabHeader;
    }

    get form(): Form.ITabForm {
        return this._form;
    }

    constructor(mainTabView: Common.IMainTabbarView, private readonly _form: Form.ITabForm) {
        super(mainTabView);
    }

    getContent() {
        return this._form.init();
    }

    destruct() {
        this._form.destroy();
        super.destruct();
    }
}
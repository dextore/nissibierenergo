﻿import Base = require("../../Components/ComponentBase");
import TreeBase = require("./MainTreeViewBase");
//import Tree = require("./MainTreeView");
import TI = require("../Tabs/MainTabBase");
import Event = require("../../Common/Events");
import SM = require("../../Common/SystemMessages");

export interface IMainTreeModulesView extends Base.IComponent {
    addTree(tree: TreeBase.IMainTreeView);
    addTabItem(tab: TI.IMainTab);
    selectTree(id: string);
    selectTreeItem(item: Tree.ISystemTreeItemModel);
    treeSelectedChange: Event.Event<Tree.ISystemTreeItemModel>;
    currentTree: TreeBase.IMainTreeView; 
}

export function getMainTreeModulesView(viewName: string) {
    return new MainTreeModulesView(viewName);
}

// Контеунер для показа модулей зависящих от дерева
// Загрузить дерево
// Установить текущее дерево
// Загрузить модуль  

/*
  Устанавливается текущее дерево.
  1 После создания модуль позиционируется от данных дерева
  2 При изменении в дереве - модуль перезагружает данные
  3 после изменения в модуле, дерево апдейтит данные.
  4 если запись удалена из модуля - что делать?
  5 Поиск, дерево позиционируется на найденный итем и передает данные в модуль, модуль делает, что сможет (Тип модуля берется из типа сущьности...)).
*/

class MainTreeModulesView extends Base.ComponentBase implements IMainTreeModulesView {

    private _currentTreeId: string;
    private _treeList: { [id: string]: TreeBase.IMainTreeView } = {}
    private _treeSelectedChange: Event.Event<Tree.ISystemTreeItemModel>;

    get viewName() {
        return `${this._viewName}-tree-palce`;
    }

    get viewId() {
        return `${this.viewName}-id`;
    }

    get treesPlaceId() {
        return `${this.viewName}-module-view-id`;
    }  

    get modulesPlaceId() {
        return `${this.viewName}-tree-view-id`;
    }

    get treeSelectedChange() {
        return this._treeSelectedChange;
    }

    get currentTree() {
        return this._treeList[this._currentTreeId];
    }

    constructor(private readonly _viewName) {
        super();
        this._treeSelectedChange = new Event.Event<Tree.ISystemTreeItemModel>();
    } 

    addTree(tree: TreeBase.IMainTreeView) {
        this._treeList[tree.id] = tree;
        ($$(this.treesPlaceId) as webix.ui.multiview).addView(tree.init());
        this.subscribe(tree.selectedChangedEvent.subscribe((entiry) => {
            this.treeSelectedChange.trigger(entiry);
        }, tree));
    }

    addTabItem(tab: TI.IMainTab) {
        ($$(this.modulesPlaceId) as webix.ui.multiview).addView(tab.init());
    }

    selectTree(id: string) {
        ($$(id) as webix.ui.tree).show();
        this._currentTreeId = id;
    }

    selectTreeItem(item: Tree.ISystemTreeItemModel) {
        this._treeList[this._currentTreeId].selectedItem = item;
    }

    init() {
        return {
            //view: "view",
            id: this.viewId,
            cols: [
                { view: "multiview", id: this.modulesPlaceId, gravity: 3, animate: false, cells: [{}] },
                { view: "resizer" },
                { view: "multiview", id: this.treesPlaceId, animate: false, cells: [{}] }
            ]
        };
    }

}
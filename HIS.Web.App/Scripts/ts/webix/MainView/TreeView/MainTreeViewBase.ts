﻿import Base = require("../../Components/ComponentBase");
import Event = require("../../Common/Events");
import SM = require("../../Common/SystemMessages");

export interface IMainTreeView extends Base.IComponent {
    id: string;
    name: string;
    treeControl: webix.ui.tree;
    selectedChangedEvent: Event.Event<Tree.ISystemTreeItemModel>;
    selectedItem: Tree.ISystemTreeItemModel;
    afterRender();
}

//export function getMainTreeView(viewName: string, name: string) {
//    return new MainTreeView(viewName, name);
//}

export abstract class MainTreeViewBase extends Base.ComponentBase implements IMainTreeView {

    private _selectedChangedEvent: Event.Event<Tree.ISystemTreeItemModel>;

    get id() {
        return `${this._viewName}-id`;
    }

    get tree() {
        return $$(this.id) as webix.ui.tree;
    }

    get name() {
        return this._name;
    }

    get treeControl() {
        return $$(this.id) as webix.ui.tree;
    }

    get selectedChangedEvent() {
        return this._selectedChangedEvent;
    }

    get selectedItem() {
        const item = this.getSelectedItem();
        return (item) ? item.entity : null;
    }

    set selectedItem(value: Tree.ISystemTreeItemModel) {
        this.setSelectedItem(value);
    }

    constructor(private readonly _viewName, private readonly _name: string) {
        super();
        this._selectedChangedEvent = new Event.Event<Tree.ISystemTreeItemModel>();
    }

    afterRender() {
        webix.extend(this.treeControl, webix.ProgressBar);
    }

    init() {
        const config = {
            view: "tree",
            id: this.id,
            select: true,
            //template: (obj, common) => {
            //    const entity = obj as Entity.IFullTextSearchEntityInfoModel;
            //    if (entity.baId === 0)
            //        return `<b>${entity.name}</b>`;
            //    //return `${common.icon(obj, common)}<i>${entity.stateName} </i><b>${entity.typeName} </b>${entity.name}`;
            //    return `<i>${entity.stateName}&emsp;</i>&nbsp;<b>${entity.typeName}</b>&nbsp;${entity.name}`;
            //},
            data: [this.getRootItem()],
            on: {
                onSelectChange: () => {
                    this.selectedChange();
                    //const item = this.getSelectedItem();
                    //this._selectedChangedEvent.trigger(item, this);
                },
                onBeforeOpen: (id) => {
                    this.beforeOpen(id);
                }
            }
        };
        return webix.extend(config, this.treeConfig(), true);
    }

    protected abstract setSelectedItem(item: Tree.ISystemTreeItemModel);

    protected selectedChange() {
        const selectedItem = this.getSelectedItem();
        SM.SystemMessages.trigger(SM.ESystemEventType.TREE_SELECTED_CHANGE, selectedItem, this);
    };

    protected beforeOpen(id) {}

    protected getSelectedItem(): any {
        const selected = this.treeControl.getSelectedItem(true);
        return (selected && selected.length > 0) ? selected[0]: null;
    }

    protected treeConfig() {
        return {};
    }  

    private getRootItem() {
        return {
            id: "root",
            value: "Последние",
        }
    }

    protected getRootNode(): any {
        return this.treeControl.find((item) => {
            return item.id === "root";
        }, true);
    }

}
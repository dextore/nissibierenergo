﻿import Base = require("./MainTreeViewBase");
import Common = require("../../Common/CommonExporter");
import Api = require("../../Api/ApiExporter");
import EntityTypes = require("../../Common/SystemEntitityTypes");

export function getMainTreeView(viewName: string, name: string) {
    return new MainTreeView(viewName, name);
}

const cRootNodeId = "root";
const cEmptyNodeValue = "<<empty>>";
const cEmptyPropName = "entity";

export class MainTreeView extends Base.MainTreeViewBase {

    selectedChange() {
        super.selectedChange();
        this.selectedChangedEvent.trigger(this.getSelectedItem(), this);
    }

    protected getSelectedItem(): Tree.ISystemTreeItemModel {
        let item = super.getSelectedItem();
        if (!item)
            return null;

        if (cEmptyPropName in item) {
            const entity = item[cEmptyPropName] as Tree.ISystemTreeItemModel;
            switch (entity.mvcAlias) {
                case "Person":
                case "Organisation":
                case "AffiliateOrganisation":
                case "Contract":
                    return entity;
                default:
                    const parentId = this.treeControl.getParentId(item.id);
                    if (parentId)
                        item = this.treeControl.getItem(parentId);

                    if (cEmptyPropName in item)
                        return item[cEmptyPropName] as Tree.ISystemTreeItemModel;
            }
        }
        return null;

        //if (item.hasOwnProperty(cEmptyPropName) &&
        //(item.entity.typeId === Common.BATypes.Person ||
        //    item.entity.typeId === Common.BATypes.Organisation ||
        //    item.entity.typeId === Common.BATypes.AffiliateOrganisation ||
        //    item.entity.typeId === Common.BATypes.Contract)) {
        //    entity = item.entity as Entity.IFullTextSearchEntityInfoModel;
        //} else {

        //   const parentId = this.treeControl.getParentId(item.id);

        //   if (parentId)
        //       item = this.treeControl.getItem(parentId);

        //    if (item.hasOwnProperty(cEmptyPropName))
        //       entity = item.entity as Entity.IFullTextSearchEntityInfoModel;
        //}
        //return entity;
    }

    setSelectedItem(entity: Tree.ISystemTreeItemModel) {

        const self = this;

        const result = this.findTreeNode(this.treeControl, entity.baId);
        if (result) {
            const selected = this.treeControl.getSelectedItem(false);
            if (selected.id !== result.id) {
                this.treeControl.select(result.id, false); 
            }
            return;
        }

        Api.ApiSystemTree.getTreeItem(entity.baId).then(dataItem => {
            const treeNode = this.findTreeNode(this.treeControl, dataItem.baId);
            if (!treeNode) {
                self.addTreeNode(this.treeControl, cRootNodeId, dataItem);
                this.treeControl.open(cRootNodeId);
            } else {
                self.removeChildItems(this.treeControl, treeNode.id);

                if (dataItem.children) {
                    let idx = 0;
                    dataItem.children.forEach(item => {
                        self.addTreeNode(this.treeControl, treeNode.id, item, idx);
                        ++idx;
                    });
                }
                this.treeControl.open(treeNode.id);
            }
            self.selectTreeNode(this.treeControl, entity.baId);
        });

        //if (entity.typeId !== Common.BATypes.Person &&
        //    entity.typeId !== Common.BATypes.Organisation &&
        //    entity.typeId !== Common.BATypes.AffiliateOrganisation &&
        //    entity.typeId !== Common.BATypes.Contract)
        //    return;

        //const result = this.treeControl.find((item) => {
        //        return (item.entity) ? item.id === entity.baId : false;
        //    },
        //    true);

        //if (result.hasOwnProperty("entity")) {
        //    //if (entity.hasOwnProperty("id"))
        //    //    delete entity["id"];

        //    const selected = this.treeControl.getSelectedItem(false);
        //    if (selected.id !== result.id) {
        //        this.treeControl.select(result.id, false);
        //    }
        //    return;
        //}

        //switch (entity.typeId) {
        //case Common.BATypes.AffiliateOrganisation:
        //    self.loadAffiliateBranch(entity, self.treeControl);
        //    break;
        //case Common.BATypes.Contract:
        //    self.loadContract(entity, self.treeControl);
        //    break;
        //default:
        //    const newItem = {
        //        id: entity.baId,
        //        value: `<b>${entity.typeName}</b>&nbsp;${entity.name}`,
        //        //value: entity.name,
        //        //webix_kids: true, 
        //        entity: entity
        //    };
        //    const id = self.treeControl.add(newItem, 0, "root");
        //    this.treeControl.select(id, false);
        //    this.treeControl.add({ id: (entity.baId * -1).toString(), value: "empty" }, 0, id);
        //    //this.treeControl.open(id);

        //    this.treeControl.open("root");
        //}


    }

    beforeOpen(id): void {

        const self = this;

        const chaildId = this.treeControl.getFirstChildId(id);
        var chiltItem = this.treeControl.getItem(chaildId);
        if (chiltItem.value !== cEmptyNodeValue)
            return; 

        this.treeControl.remove(chaildId);

        const item = this.treeControl.getItem(id);
        if (!item || !(cEmptyPropName in item))
            return;

        Api.ApiSystemTree.getTreeItemChildren((item[cEmptyPropName] as Tree.ISystemTreeItemModel).baId).then(data => {
            let idx = 0;
            data.forEach(dataItem => {
                this.addTreeNode(this.treeControl, id, dataItem, idx);
                ++idx;
            });
        });


        //const item = this.treeControl.getItem(id);
        //const entity = item.entity as Entity.IFullTextSearchEntityInfoModel;

        //switch (entity.typeId) {
        //    case Common.BATypes.Organisation:
        //    case Common.BATypes.CommercePerson:
        //    case Common.BATypes.AffiliateOrganisation:
        //        self.loadOrgainisationBranch(id, entity.baId);
        //        break;
        //    case Common.BATypes.Person:
        //        self.loadPersonBranch(id, entity, self.treeControl);
        //        break;
        //    //case Common.BATypes.AffiliateOrganisation:
        //    //    loadAffiliateBranch(id);
        //    //    break;
        //}
    }

    private loadContract(entity: Entity.IFullTextSearchEntityInfoModel, treeControl: webix.ui.tree) {
        if (!entity.baId)
            return;

        (treeControl as any).showProgress();
        Api.getApiMarkers().entityMarkerValues({
            baId: entity.baId,
            markerIds: [],
            names: []
        }).then(result => {
            const filtered = result.markerValues.filter(item => item.MVCAliase === "LegalSubject");
            if (!filtered.length) {
                webix.message(`У договора ${entity.name} нет поставщика`, `error`);
                (treeControl as any).hideProgress();
                return;
            }

            const lsId = Number(filtered[0].value);
            Api.ApiLegalPersons.getLegalPerson(lsId, []).then(legalPerson => {

                const treeItem = treeControl.find(item => {
                    return item.id === lsId;
                },
                    true);

                const entityType = EntityTypes.entityTypes().getByBATypeId(legalPerson.baTypeId);

                let centity;
                let id = "";
                if (cEmptyPropName in treeItem) {
                    id = treeItem.id;
                    this.removeChildItems(treeControl, id);
                    centity = treeItem[cEmptyPropName];
                } else {
                    const newItem = {
                        id: legalPerson.legalPersonId,
                        value: `<b>${entityType.entityInfo.name}</b>&nbsp;${legalPerson.name}`,
                        entity: {
                            baId: legalPerson.legalPersonId,
                            endDate: new Date(),
                            name: legalPerson.name,
                            startDate: new Date(),
                            stateId: 0,
                            stateName: "",
                            typeId: entityType.entityInfo.baTypeId,
                            typeName: entityType.entityInfo.name
                        }
                    };

                    centity = newItem.entity;

                    if (entityType.entityInfo.mvcAlias === "CommercePerson") {

                        const currentItem = treeControl.getSelectedItem(false);
                        this.removeChildItems(treeControl, currentItem.id);
                        id = this.treeControl.add(newItem, 0, currentItem.id);

                        Api.getApiContrcats().getByLegalSubject(entity.baId).then(contractItems => {
                            const contractType = EntityTypes.entityTypes().getByAliase("Contract");
                            let idx = 0;
                            contractItems.forEach(contractItem => {
                                const treeItem = {
                                    id: contractItem.baId,
                                    value: `<b>${contractType.entityInfo.name}</b>&nbsp;${contractItem.number}`,
                                    entity: {
                                        baId: contractItem.baId,
                                        endDate: new Date(),
                                        name: contractItem.number,
                                        startDate: new Date(),
                                        stateId: 0,
                                        stateName: "",
                                        typeId: contractType.entityInfo.baTypeId,
                                        typeName: contractType.entityInfo.name
                                    }
                                };
                                const treeId = this.treeControl.add(treeItem, idx, id);
                                ++idx;
                                //if (selectedContractId && selectedContractId === contractItem.baId)
                                //    this.treeControl.select(treeId, false);
                            });
                        });

                    } else {
                        id = this.treeControl.add(newItem, 0, cRootNodeId);
                    }
                }

                if (entityType.entityInfo.mvcAlias === "Person") {
                    this.loadPersonBranch(id, centity, this.treeControl);
                } else
                    this.loadOrgainisationBranch(id, legalPerson.legalPersonId, null, entity.baId);
            });
        });
    }


    private loadPersonBranch(id: string, entity: Entity.IFullTextSearchEntityInfoModel, treeControl: webix.ui.tree) {
        (treeControl as any).showProgress();
        (webix.promise.all([
            Api.getApiCommercePersons().getOPFByParent(entity.baId),
            Api.getApiContrcats().getByLegalSubject(entity.baId)
        ] as any) as any).then(results => {
            let idx = 0;

            const contractType = EntityTypes.entityTypes().getByAliase("Contract");

            const contractItems = results[1] as Contracts.ILSContractModel[];
            contractItems.forEach(contractItem => {
                const treeItem = {
                    id: contractItem.baId,
                    value: `<b>${contractType.entityInfo.name}</b>&nbsp;${contractItem.number}`,
                    entity: {
                        baId: contractItem.baId,
                        endDate: new Date(),
                        name: contractItem.number,
                        startDate: new Date(),
                        stateId: 0,
                        stateName: "",
                        typeId: contractType.entityInfo.baTypeId,
                        typeName: contractType.entityInfo.name
                    }
                };
                const treeId = this.treeControl.add(treeItem, idx, id);
                //if (selectedContractId && selectedContractId === contractItem.baId)
                //    this.treeControl.select(treeId, false);
                ++idx;
            });

            const entityType = EntityTypes.entityTypes().getByAliase("CommercePerson");

            const organisation = results[0] as Commerce.ICommercePersonModel;
            if (organisation) {
                const treeItem = {
                    id: organisation.baId,
                    value: `<b>${entityType.entityInfo.name}</b>&nbsp;${entity.name}`,
                    entity: {
                        baId: organisation.baId,
                        endDate: new Date(),
                        name: entity.name,
                        startDate: new Date(),
                        stateId: 0,
                        stateName: "",
                        typeId: entityType.entityInfo.baTypeId,
                        typeName: entityType.entityInfo.name
                    }
                };
                const organisationId = treeControl.add(treeItem, idx, id);
                this.treeControl.add({ id: (entity.baId * -1).toString(), value: cEmptyNodeValue }, 0, organisationId);
                ++idx;
            };

            //self.loadContracts(id, constracts, idx);
            (treeControl as any).hideProgress();
        });
    }

    private loadOrgainisationBranch(id: string, baId: number, selectedAffiliateId: number = null, selectedContractId: number = null) {
        (this.treeControl as any).showProgress();
        (webix.promise.all([
            Api.ApiAffiliateOrganisations.getAffiliateOrganisations(baId),
            Api.getApiContrcats().getByLegalSubject(baId)
        ] as any) as any).then(results => {
            const affiliateItems = results[0] as LegalPersons.IAffiliateOrganisationModel[];
            const contractItems = results[1] as Contracts.ILSContractModel[];

            //const constracts = results[1] as Contracts.IContractModel[];
            const contractType = EntityTypes.entityTypes().getByAliase("Contract");

            let idx = 0;
            contractItems.forEach(contractItem => {
                const treeItem = {
                    id: contractItem.baId,
                    value: `<b>${contractType.entityInfo.name}</b>&nbsp;${contractItem.number}`,
                    entity: {
                        baId: contractItem.baId,
                        endDate: new Date(),
                        name: contractItem.number,
                        startDate: new Date(),
                        stateId: 0,
                        stateName: "",
                        typeId: contractType.entityInfo.baTypeId,
                        typeName: contractType.entityInfo.name
                    }
                };
                const treeId = this.treeControl.add(treeItem, idx, id);
                if (selectedContractId && selectedContractId === contractItem.baId)
                    this.treeControl.select(treeId, false);
                ++idx;
            });
            //this.loadContracts(id, constracts, idx);

            const affiliateType = EntityTypes.entityTypes().getByAliase("AffiliateOrganisation");

            affiliateItems.forEach(affiliateItem => {
                const treeItem = {
                    id: affiliateItem.legalPersonId,
                    value: `<b>${affiliateType.entityInfo.name}</b>&nbsp;${affiliateItem.name}`,
                    entity: {
                        baId: affiliateItem.legalPersonId,
                        endDate: new Date(),
                        name: affiliateItem.name,
                        startDate: new Date(),
                        stateId: 0,
                        stateName: "",
                        typeId: affiliateType.entityInfo.baTypeId,
                        typeName: affiliateType.entityInfo.name
                    }
                };
                const treeId = this.treeControl.add(treeItem, idx, id);
                this.treeControl.add({ id: (affiliateItem.legalPersonId * -1).toString(), value: cEmptyNodeValue }, 0, treeId);
                if (selectedAffiliateId && selectedAffiliateId === affiliateItem.legalPersonId)
                    this.treeControl.select(treeId, false);
                ++idx;
            });

            if (selectedAffiliateId || selectedContractId) {
                //this.treeControl.open("root");
                this.treeControl.open(id, true);
            }

            (this.treeControl as any).hideProgress();
        });
    }

    loadAffiliateBranch(entity: Entity.IFullTextSearchEntityInfoModel, tree: webix.ui.tree) {
        (this.treeControl as any).showProgress();
        Api.ApiLegalPersons.getParentLegalPerson(entity.baId).then(legalPerson => {

            const treeItem = tree.find((item) => {
                    return item.id === legalPerson.legalPersonId;
                },
                true);

            const entityType = EntityTypes.entityTypes().getByBATypeId(legalPerson.baTypeId);

            let id = "";
            if (cEmptyPropName in treeItem) {
                id = treeItem.id;
                this.removeChildItems(this.treeControl, id);
            } else {
                const newItem = {
                    id: legalPerson.legalPersonId,
                    value: `<b>${entityType.entityInfo.name}/b>&nbsp;${legalPerson.name}`,
                    entity: {
                        baId: legalPerson.legalPersonId,
                        endDate: new Date(),
                        name: legalPerson.name,
                        startDate: new Date(),
                        stateId: 0,
                        stateName: "",
                        typeId: entityType.entityInfo.baTypeId,
                        typeName: entityType.entityInfo.name
                    }
                };
                id = tree.add(newItem, 0, cRootNodeId);
            }

            this.loadOrgainisationBranch(id, legalPerson.legalPersonId, entity.baId);
        });
    }

    private removeChildItems(tree: webix.ui.tree, id: any) {
        let itemId = tree.getFirstChildId(id);
        while (itemId) {
            tree.remove(itemId);
            itemId = tree.getFirstChildId(id);
        }
    }

    private findTreeNode(tree: webix.ui.tree, id: number) {
        const item = this.treeControl.find(item => {
            return item.id === id;
        }, true);
        return ("id" in item) ? item : null;
    } 

    private selectTreeNode(tree: webix.ui.tree, id: number) {
        const item = this.findTreeNode(tree, id);
        if (!item)
            return;

        const selected = tree.getSelectedItem(false);
        if (!selected || selected.id !== item.id) {
            this.treeControl.select(item.id, false);
        }
        return;
    } 

    private addTreeNode(tree: webix.ui.tree, parentId: any, systemTreeItem: Tree.ISystemTreeItemModel, idx: number = 0) {
        const treeNode = {
            id: systemTreeItem.baId,
            baId: systemTreeItem.baId,
            value: `<b>${(systemTreeItem.baTypeNameShort) ? systemTreeItem.baTypeNameShort : systemTreeItem.baTypeName}</b>&nbsp;${systemTreeItem.name}`,
            entity: systemTreeItem
        };
        const id = tree.add(treeNode, idx, parentId);

        if (systemTreeItem.mvcAlias !== "Contract" && (!systemTreeItem.children || !systemTreeItem.children.length) ) {
            //tree.select(id, false);
            tree.add({ id: `empty${systemTreeItem.baId.toString()}`, value: cEmptyNodeValue }, 0, id);
            return;
        }

        let cidx = 0;
        systemTreeItem.children.forEach(item => {
            this.addTreeNode(tree, id, item, cidx);
            ++cidx;
        });
        try {
            if (!tree.isBranchOpen(id))
                tree.open(id);
        } catch (e) {
            //console.log(JSON.stringify(systemTreeItem));
            //console.error(e);
        }

    }
}

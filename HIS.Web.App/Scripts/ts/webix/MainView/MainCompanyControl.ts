﻿import Api = require("../Api/ApiExporter");
import Common = require("../Common/CommonExporter");

export interface IMainCompanyControl {
    init(): any;
    controlEvent: Common.Event<Inventory.ILSCompanyAreas>;
    selectedValue: Inventory.ILSCompanyAreas;
    disable();
    enable();
}

export function getMainCompanyControl(viewName: string, mainTabsView: Common.IMainTabbarView): IMainCompanyControl {
    return new MainCompanyControl(viewName, mainTabsView);
}

class MainCompanyControl implements IMainCompanyControl {

    private _controlEvent = new Common.Event<Inventory.ILSCompanyAreas>();
    private _selectedCompany: Inventory.ILSCompanyAreas;
    private _disableCount: number[] = [];

    get controlEvent(): Common.Event<Inventory.ILSCompanyAreas> {
        return this._controlEvent;
    }

    get controlId() {
        return `${this._viewname}-company-control-id`;
    }

    get suggestId() {
        return `${this._viewname}-companysuggest-control-id`;
    }

    get selectedValue() {
        return this._selectedCompany;
    }

    disable() {
        this._disableCount.push(this._disableCount.length);
        $$(this.controlId).disable();
    }

    enable() {
        this._disableCount.pop();
        if (!this._disableCount.length) {
            $$(this.controlId).enable();
        }
    }

    constructor(private readonly _viewname: string, private readonly _mainTabsView: Common.IMainTabbarView) {

    }

    private setSelectedValue() {
        const suggest = $$(this.suggestId) as webix.ui.suggest;
        const combo = $$(this.controlId) as webix.ui.combo;
        combo.setValue(this.selectedValue.id.toString());
        suggest.setValue(this._selectedCompany.id);
    }

    init() {
        return {
            id: this.controlId,
            multiview: true,
            view: "combo",
            label: "Организация",
            align: "right",
            labelWidth: 90,
            maxWidth: 300,
            minWidth: 50,
            validate: webix.rules.isNotEmpty,
            on: {
                onChange: (newv, oldv) => {
                    if (newv === "") {
                        this.setSelectedValue();
                    }
                }
            },
            suggest: {
                view: "gridsuggest",
                width: 300,
                id: this.suggestId,
                on: {
                    onValueSuggest: (obj: Inventory.ILSCompanyAreas) => {
                        this._selectedCompany = obj;
                        this._controlEvent.trigger(webix.copy(this._selectedCompany), this);
                    }
                },
                body: {
                    yCount: 10,
                    scroll: true,
                    autoheight: false,
                    header: false,
                    columns: [
                        { id: "id", header: "id", hidden: true },
                        { id: "name", header: "name", width: 250 }
                    ],
                    url: {
                        $proxy: true,
                        load: () => {
                            Api.getApiNomenclature().getLsCompanyAreas().then((data: Inventory.ILSCompanyAreas[]) => {
                                const list = ($$(this.suggestId) as webix.ui.gridsuggest).getList() as any;
                                list.clearAll();
                                list.parse(data, "json");
                                data.forEach(item => {
                                    if (item.id == 10173) {
                                        this._selectedCompany = item;
                                    }
                                }, this);
                                if (this.isEmptyObject(this._selectedCompany)) {
                                    this._selectedCompany = data[0];
                                }
                                this.setSelectedValue();
                            });
                        }
                    }
                }
            }
        }
    }
    private isEmptyObject(obj: object): boolean {

        if (obj == null || obj == undefined) {
            return true;
        }
        if (Object.keys(obj).length === 0) {
            return true;
        }
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }



}
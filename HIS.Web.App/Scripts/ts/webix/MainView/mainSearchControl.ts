﻿import Api = require("../Api/ApiExporter");
import Common = require("../Common/CommonExporter");
import Search = require("./UniversalSearch/SearchDlg");
import EntityType = require("../Common/SystemEntitityTypes");
import Helper = require("../Common/ModuleHelper");

// seach control
import Ctrl = require("../Controls/SearchControl");

export interface IMainSearchControl {
    init(): any;
    searchEvent: Common.Event<Entity.IFullTextSearchEntityInfoModel>;
    advancedSearch();
}

export function getMainSearchControl(viewName: string, mainTabsView: Common.IMainTabbarView): IMainSearchControl {
    return new MainSearchControl(viewName, mainTabsView);
}

class MainSearchControl implements IMainSearchControl {

    private _selectedEntity: Entity.IFullTextSearchEntityInfoModel;
    private _searchEvent = new Common.Event <Entity.IFullTextSearchEntityInfoModel>();
    get searchEvent(): Common.Event<Entity.IFullTextSearchEntityInfoModel> {
        return this._searchEvent;
    }

    get controlId() {
        return `${this._viewname}-search-control-id`;
    }

    get listId() {
        return `${this._viewname}-search-suggest-list-id`;
    }

    constructor(private readonly _viewname: string, private readonly _mainTabsView: Common.IMainTabbarView) {
    }

    advancedSearch() {
        const dlg = Search.getSearchDlg({
            viewName: `${this._viewname}-search-dlg`,
            mainTabs: this._mainTabsView, 
            disableAddButton: false,
            baTypId: null,
        }, this._mainTabsView);
        dlg.showModal(() => {
            dlg.close();
        });
        //webix.alert({
        //    title: "Расширенный поиск",
        //    ок: `Закрыть`,
        //    text: "Вскоре можно будет что-то найти!"
        //});
    }

    init() {
        return {
            view: Ctrl.viewName,
            id: this.controlId,
            css: "protoUI-with-icons-2",
            align: "right",
            placeholder: "Поиск..",
            icons: ["search", "close"],
            maxWidth: 300,
            minWidth: 50,
            on: {
                onSearchIconClick: (e) => {
                    this.advancedSearch();
                },
                onCloseIconClick: () => {
                    ($$(this.controlId) as webix.ui.search).setValue("");
                }
            },
            suggest: {
                view: "gridsuggest",
                id: this.listId,
                textValue: "name",
                fitMaster: false,
                resize: true,
                width: 800,
                body: {
                    width: 1000,
                    autoConfig: true,
                    header: false,
                    scroll: true,
                    autoheight: false,
                    autofocus: true,
                    yCount: 8,
                    borderless: true,
                        columns: [
                        { id: "baId", header: "Код", width: 50 },
                        { id: "typeName", header: "Тип", width: 150 },
                        { id: "name", header: "Наименование", fillspace: true },
                        { id: "stateName", header: "Состояние", width: 150 }
                    ],
                    dataFeed: (filtervalue: string) => {
                        if (filtervalue.length < 2)
                            return;

                        Api.getApiSystemSearch().getFullTextSearchEntities(filtervalue).then((data) => {
                            (($$(this.listId) as webix.ui.gridsuggest).getList() as any).parse(data.items, "json");
                        });
                    }
                },
                on: {
                    onValueSuggest: (obj: Entity.IFullTextSearchEntityInfoModel) => {
                        this._selectedEntity = obj;
                        this.showModule(this._selectedEntity);
                    }
                }
            }
        }
    }

    private showModule(found: Entity.IFullTextSearchEntityInfoModel) {

        const entityInfo = EntityType.entityTypes().getByBATypeId(found.typeId);

        if (Helper.ModuleHelper.aliasModuleExists(entityInfo.entityInfo.mvcAlias)) {

        //const moduleId = this._mainTabsView.getModuleId(entityInfo.entityInfo.mvcAlias);
        //if (moduleId) {
            Helper.ModuleHelper.showModuleByAlias(entityInfo.entityInfo.mvcAlias, this._mainTabsView);
            //this._mainTabsView.showModule(moduleId);
            this._searchEvent.trigger(webix.copy(this._selectedEntity), this);
            ($$(this.controlId) as webix.ui.search).setValue("");
            return;
        }

        webix.alert({
            title: "Модуль",
            ок: `Закрыть`,
            text: "Модуль не реализован!"
        });

        //// Загрузить модуль
        //switch (entityInfo.entityInfo.mvcAlias) {
            
        //    this._mainTabsView.

        //    case "":
        //        this._mainTabsView.showModule("legal-person-tree-module");
        //        this._searchEvent.trigger(webix.copy(this._selectedEntity), this);
        //        ($$(this.controlId) as webix.ui.search).setValue("");
        //        return;
        //    case Common.BATypes.AffiliateOrganisation:
        //        this._mainTabsView.showModule("legal-person-tree-module");
        //        this._searchEvent.trigger(webix.copy(this._selectedEntity), this);
        //        //($$(this.controlId) as webix.ui.search).setValue("");
        //        return;
        //    case Common.BATypes.Person:
        //        this._mainTabsView.showModule("fiz-person-tree-module");
        //        this._searchEvent.trigger(webix.copy(this._selectedEntity), this);
        //        ($$(this.controlId) as webix.ui.search).setValue("");
        //        return;
        //    default: 
        //        webix.alert({
        //            title: "Модуль",
        //            ок: `Закрыть`,
        //            text: "Модуль не реализован!"
        //        });
        //}
    }

}
﻿import Api = require("../Api/ApiExporter"); 

export interface ILoginForm {
    show(): Promise<any>;
}

export function getLoginForm(): ILoginForm {
    return new LoginForm();
}

const viewName = `his-login-window-view`;

class LoginForm implements ILoginForm {

    private _promise: Promise<any>;

    get windowId() {
        return `${viewName}-id`;
    }  

    get formId() {
        return `${viewName}-form-id`;
    }  

    get areaComboBoxId() {
        return `${viewName}-combo-id`;
    }

    get window() {
        return $$(this.windowId) as webix.ui.window;
    }

    get form() {
        return $$(this.formId) as webix.ui.form;
    }

    show(): Promise<any> {
        this._promise = webix.promise.defer();
        this.createWindow();
        this.window.show();
        return this._promise;
    }


    private createWindow() {
        webix.ui({
            view: "window",
            id: this.windowId,
            head: "Аутентификация",
            position: "center",
            width: 400, 
            body: {
                view: "form",
                id: this.formId,
                borderless: false,
                elements: [
                    { view: "text", label: "Имя пользователя:", labelWidth: 135, name: "login", validate: webix.rules.isNotEmpty() },
                    { view: "text", type: 'password', labelWidth: 135, label: "Пароль:", name: "password", validate: webix.rules.isNotEmpty() },
                    {
                        view: "richselect", id: this.areaComboBoxId, labelWidth: 135, label: "База данных:", name: "dbalias", validate: webix.rules.isNotEmpty(),                        
                        options: {
                            body: {
                                url: {
                                    $proxy: true,
                                    load: (self) => {
                                        Api.getApiAuthentication()
                                            .getDataBaseAreas()
                                            .then((data: Auth.IAreaDataBaseModel[]) => {
                                                if (!data.length) {
                                                    (<webix.ui.list>(<webix.ui.combo>$$(this.areaComboBoxId)).getList())
                                                        .parse([], "json");
                                                    return;
                                                }

                                                const result = data.map(item => {
                                                    return {id: item.alias, value: item.name } 
                                                });                                                
                                                (<webix.ui.list>(<webix.ui.combo>$$(this.areaComboBoxId)).getList())
                                                    .parse(result, "json");

                                                (<webix.ui.combo>$$(this.areaComboBoxId)).setValue(result[0].id);
                                            });
                                    }
                                }
                            }
                        }     
                        //options: [
                        //    { id: "nskes", value: "Основная БД" },
                        //    { id: "nskes_test", value: "Тестовая БД" }
                        //]
                    },
                    {},
                    { view: "button", value: "Подключиться", type: "form", click: () => { this.loginClick(); }},
                    { view: "button", value: "Доменная аутентификация", type: "form", click: () => { this.domainLoginClick();}}
                ]
            }
        });
    }

    private loginClick() {
        if (!this.form.validate())
            return;

        const formData = this.form.getValues();

        const userInfo: Auth.ILoginModel = {
            userName: formData.login,
            password: formData.password,
            dbalias: formData.dbalias
        };

        Api.getApiAuthentication()
            .formLogin(userInfo)
            .then((res: Auth.IAuthResultModel) => {
                this.window.hide();
                this.window.close();
                (this._promise as any).resolve();
            }).catch(error => {
                webix.message(error.error, "error", 10000);
            });
    }

    private domainLoginClick() {
        const formData = this.form.getValues();

        Api.getApiAuthentication()
            .domainLogin(formData.dbalias)
            .then((res: Auth.IAuthResultModel) => {
                this.window.hide();
                this.window.close();
                (this._promise as any).resolve();
            }).catch(error => {
                webix.message(error.error, "error", 10000);
            });
    }
}
﻿
 
 

 

/// <reference path="Enums.ts" />

declare namespace Address {
	interface IAddresFullName {
		addressText: string;
		aoId: System.IGuid;
		houseId: System.IGuid;
		sourceType: HIS.Models.Layer.Models.Address.AddressSourceType;
		steadId: System.IGuid;
	}
	interface IAddressAOIdAOGuid {
		AOGuid: System.IGuid;
		AOId: System.IGuid;
	}
	interface IAddressCatalog extends Address.IAddressCatalogBase {
	}
	interface IAddressCatalogBase {
		AOGuid: System.IGuid;
		AOId: System.IGuid;
		name: string;
		sourceType: HIS.Models.Layer.Models.Address.AddressSourceType;
	}
	interface IAddressComparisons {
		AOGuid: System.IGuid;
		AOid: System.IGuid;
		AOLevel: number;
		fiasAOGuid: System.IGuid;
		fiasAOId: System.IGuid;
		fiasFullName: string;
		fiasHouseId: System.IGuid;
		fiasParentGuid: System.IGuid;
		fiasSteadId: System.IGuid;
		fullName: string;
		houseId: System.IGuid;
		parentGuid: System.IGuid;
		sourceTypeId: string;
		steadId: System.IGuid;
	}
	interface IAddressComparisonsDpModel extends Address.IAddressComparisons {
		items: Address.IAddressComparisonsDpModel[];
	}
	interface IAddressEntityModel {
		AOGuid: System.IGuid;
		AOId: System.IGuid;
		baId: number;
		flatNum: string;
		flatTypeId: number;
		houseGuid: System.IGuid;
		houseId: System.IGuid;
		isActual: boolean;
		isBuild: boolean;
		location: string;
		name: string;
		POBox: string;
		steadGuid: System.IGuid;
		steadId: System.IGuid;
	}
	interface IAddressEstateStatus {
		dataVersionId: number;
		estStatId: number;
		isDelete: boolean;
		name: string;
	}
	interface IAddressFindResult {
		actualStateName: string;
		addressText: string;
		AOGuid: System.IGuid;
		AOId: System.IGuid;
		buildNum: string;
		buildStateName: string;
		cadNum: string;
		houseId: System.IGuid;
		houseNum: string;
		postalCode: string;
		sourceType: HIS.Models.Layer.Models.Address.AddressSourceType;
		startDate: Date;
		steadId: System.IGuid;
		strucNum: string;
	}
	interface IAddressHierarchyResponse {
		aOGuid: System.IGuid;
		aOId: System.IGuid;
		aOLevel: HIS.Models.Layer.Models.Address.AddressObjectLevel;
		houseId: System.IGuid;
		parentGuid: System.IGuid;
		sourceType: HIS.Models.Layer.Models.Address.AddressSourceType;
		steadId: System.IGuid;
	}
	interface IAddressHouse {
		cadNum: string;
		houseGuid: System.IGuid;
		houseId: System.IGuid;
		houseNum: string;
		liveStatus: number;
		name: string;
		postalCode: string;
		sourceType: HIS.Models.Layer.Models.Address.AddressSourceType;
	}
	interface IAddressHouseModel {
		cadNum: string;
		houseGuid: System.IGuid;
		houseId: System.IGuid;
		liveStatus: number;
		name: string;
		postalCode: string;
		sourceType: HIS.Models.Layer.Models.Address.AddressSourceType;
	}
	interface IAddressObjectModel {
		AOGuid: System.IGuid;
		AOId: System.IGuid;
		formalName: string;
		postCode: string;
		shortName: string;
		sourceType: HIS.Models.Layer.Models.Address.AddressSourceType;
	}
	interface IAddressObjectRequestModel {
		AOGuid: System.IGuid;
	}
	interface IAddressObjectTypeModel {
		SCname: string;
		socrName: string;
	}
	interface IAddressObjectUpdateModel {
		AOGuid: System.IGuid;
		AOId: System.IGuid;
		AOLevel: number;
		formalName: string;
		parentId: System.IGuid;
		postalCode: string;
		shortName: string;
	}
	interface IAddressObjectUpdateResultModel {
		AOGuid: System.IGuid;
		AOId: System.IGuid;
		errorDescription: string;
		isError: boolean;
	}
	interface IAddressRequestModel extends Address.IRequestModel {
		AOGuid: System.IGuid;
		liveStatus: number;
	}
	interface IAddressResultRequestModel {
		AOId: System.IGuid;
		cadNum: string;
		houseId: System.IGuid;
		onlyActyal: boolean;
		postCode: string;
		steadId: System.IGuid;
	}
	interface IAddressSettleRequestModel {
		aoGuid: System.IGuid;
		aoId: System.IGuid;
		aoLevelId: number;
		fiasAOId: System.IGuid;
		fiasHouseId: System.IGuid;
		fiasSteadId: System.IGuid;
		houseGuid: System.IGuid;
		houseId: System.IGuid;
		steadGuid: System.IGuid;
		steadId: System.IGuid;
	}
	interface IAddressStead {
		cadNum: string;
		liveStatus: number;
		name: string;
		postalCode: string;
		sourceType: HIS.Models.Layer.Models.Address.AddressSourceType;
		steadGuid: System.IGuid;
		steadId: System.IGuid;
	}
	interface IAddressStructureStatus {
		dataVersionId: number;
		isDelete: boolean;
		name: string;
		shortName: string;
		strStatId: number;
	}
	interface IAddressUpdateModel {
		aoId: System.IGuid;
		baId: number;
		flatNum: string;
		flatType: number;
		houseId: System.IGuid;
		isActual: boolean;
		isBuild: boolean;
		location: string;
		name: string;
		POBox: string;
		searchType: HIS.Models.Layer.Models.Address.AddressSourceType;
		steadId: System.IGuid;
	}
	interface IAddressUpdateResultModel {
		baId: number;
		name: string;
	}
	interface IAOAddressesModel {
		aoGuid: System.IGuid;
		baId: number;
		flatNum: string;
		flatTypeId: number;
		houseGuid: System.IGuid;
		isActual: boolean;
		isBuild: boolean;
		location: string;
		name: string;
		poBox: string;
		steadGuid: System.IGuid;
	}
	interface IBindHousesModel {
		firstHouseId: System.IGuid;
		isBind: boolean;
		secondHouseId: System.IGuid;
	}
	interface IFlatTypeModel {
		flatTypeId: number;
		isVisible: boolean;
		name: string;
		shortName: string;
	}
	interface IHouseRequestModel {
		AOGuid: System.IGuid;
		houseId: System.IGuid;
	}
	interface IHouseUpdateModel {
		AOId: System.IGuid;
		buildNum: string;
		cadNum: string;
		estStateId: number;
		houseGuid: System.IGuid;
		houseId: System.IGuid;
		houseNum: string;
		isBuild: boolean;
		latitude: number;
		longitude: number;
		parentGuid: System.IGuid;
		postalCode: string;
		sourceType: HIS.Models.Layer.Models.Address.AddressSourceType;
		strStateId: number;
		structNum: string;
	}
	interface IHouseUpdateResultModel {
		errorDescription: string;
		houseGuid: System.IGuid;
		houseId: System.IGuid;
		isError: boolean;
	}
	interface IRequestModel {
		AOLevel: HIS.Models.Layer.Models.Address.AddressObjectLevel;
		searchValue: string;
		skip: number;
		take: number;
	}
	interface ISteadRequestModel {
		AOGuid: System.IGuid;
		steadId: System.IGuid;
	}
	interface ISteadUpdateModel {
		cadNum: string;
		latitude: number;
		longitude: number;
		number: string;
		parentGuid: System.IGuid;
		parentId: System.IGuid;
		postalCode: string;
		steadGuid: System.IGuid;
		steadId: System.IGuid;
	}
	interface ISteadUpdateResultModel {
		errorDescription: string;
		isError: boolean;
		steadGuid: System.IGuid;
		steadId: System.IGuid;
	}
}
declare namespace AdminPanel {
	interface IBaTypesModel {
		fixOperationHistory: boolean;
		fullName: string;
		isGroupType: boolean;
		isImplementType: boolean;
		isImplementTypeName: string;
		mvcAlias: string;
		name: string;
		parent: string;
		parentId: number;
		searchType: string;
		searchTypeId: number;
		shortName: string;
		typeId: number;
	}
	interface IBaTypesOperationsCreateModel {
		operationId: number;
		operationName: string;
	}
	interface IBaTypesOperationsModel {
		isCreateOperation: boolean;
		isDeleteOperation: boolean;
		operationId: number;
		operationName: string;
	}
	interface IBaTypesOperationsStateCreateModel {
		endState: AdminPanel.IBaTypesStatesCreateModel;
		isBlocked: boolean;
		operation: AdminPanel.IBaTypesOperationsCreateModel;
		startState: AdminPanel.IBaTypesStatesCreateModel;
	}
	interface IBaTypesOperationsStateModel {
		destinationStateName: string;
		isBlocked: boolean;
		operationId: number;
		operationName: string;
		sourceStateId: number;
		sourceStateName: string;
	}
	interface IBaTypesStatesCreateModel {
		stateId: number;
		stateName: string;
	}
	interface IBaTypesStatesModel {
		isDelState: boolean;
		isFirstState: boolean;
		stateId: number;
		stateName: string;
	}
	interface IItemTableViewModel {
		isActive: boolean;
		itemId: number;
		itemName: string;
		orderNumber: number;
	}
	interface IMarkersAdminModel {
		baTypeLink: string;
		baTypeLinkId: number;
		id: number;
		implementTypeField: string;
		implementTypeName: string;
		isBlocked: boolean;
		isCollectible: boolean;
		isFixMarkerHistory: boolean;
		isImplementTypeField: boolean;
		isInheritToDescendant: boolean;
		isOptional: boolean;
		isPeriodic: boolean;
		isRequired: boolean;
		markerType: string;
		mvcAlias: string;
		name: string;
		overrideName: string;
	}
	interface IOnlyMarkersDataModel {
		baTypeLink: string;
		baTypeLinkId: number;
		id: number;
		implementTypeName: string;
		markerType: string;
		markerTypeId: number;
		mvcAlias: string;
		name: string;
	}
}
declare namespace Auth {
	interface IAreaDataBaseModel {
		alias: string;
		desc: string;
		name: string;
	}
	interface IAuthResultModel {
		error: string;
		result: boolean;
	}
	interface IDomainUserInfoModel {
		description: string;
		displayName: string;
		employeeId: string;
		samAccountName: string;
		voiceTelephoneNumber: string;
	}
	interface ILoginModel {
		dbalias: string;
		password: string;
		userName: string;
	}
	interface IUserInfoModel {
		userName: string;
	}
}
declare namespace BankAccounts {
	interface ILSBankAccountModel extends Entity.IEntityBaseModel {
		bankAccountId: number;
		bankId: number;
		bankName: string;
		endDate: Date;
		isDefault: boolean;
		itemId: number;
		markerId: number;
		note: string;
		number: string;
		startDate: Date;
		stateId: number;
		stateName: string;
	}
}
declare namespace Banks {
	interface IFrontBankMarker {
		bAId: number;
		endDate: Date;
		markerId: number;
		note: string;
		startDate: Date;
		value: string;
	}
	interface IFrontBanksEntity {
		acceptancePeriod: number;
		addrId: number;
		baId: number;
		bankBranchDisplayValue: string;
		bankStatus: string;
		bATypeId: number;
		bik: string;
		corespondetsAccount: string;
		displayAddr: string;
		inn: string;
		markers: Banks.IFrontBankMarker[];
		name: string;
		okpo: string;
		parentId: number;
		rkc: string;
	}
}
declare namespace Branch {
	interface IBranchItemModel extends Branch.IBranchModel {
		endDate: Date;
		itemId: number;
		startDate: Date;
	}
	interface IBranchModel {
		baId: number;
		brnNum: string;
		name: string;
		parentId: number;
		treeLevel: number;
	}
}
declare namespace Catalogs {
	interface IFALocationsFormModel extends Catalogs.ILegalSubjectFormBaseModel {
	}
	interface IFAMovReasonsFormModel extends Catalogs.ILegalSubjectFormBaseModel {
	}
	interface ILegalSubjectFormBaseModel {
		isActive: boolean;
		itemId: number;
		itemName: string;
		name: string;
		orderNum: number;
		shortName: string;
	}
	interface IOrganisationLegalFormModel extends Catalogs.ILegalSubjectFormBaseModel {
	}
	interface IPersonLegalFormModel extends Catalogs.ILegalSubjectFormBaseModel {
	}
}
declare namespace Commerce {
	interface ICommercePersonModel extends Entity.IEntityBaseModel {
		parentId: number;
	}
	interface ICommercePersonUpdateModel extends Entity.IEntityUpdateBaseModel {
		parentId: number;
	}
}
declare namespace ConsignmentNotes {
	interface IConsignmentNotesDocumentFullModel extends ConsignmentNotes.IConsignmentNotesDocumentModel {
		companyAreaId: string;
		contractId: string;
		legalSubjectId: string;
		whWarehousesId: string;
	}
	interface IConsignmentNotesDocumentModel {
		baId: number;
		baTypeId: number;
		companyAreaName: string;
		contractName: string;
		cost: number;
		costVAT: number;
		docDate: Date;
		legalSubjectsName: string;
		note: string;
		number: string;
		stateDisplay: string;
		stateId: number;
		userName: string;
		vAT: number;
		warehouseName: string;
	}
	interface IConsignmentNotesDocumentUpdateModel {
		baId: number;
		baTypeId: number;
		cAId: number;
		contractId: number;
		cost: number;
		costVAT: number;
		docDate: Date;
		lSId: number;
		note: string;
		number: string;
		stateId: number;
		userId: number;
		vAT: number;
		wHId: number;
	}
	interface IConsignmentNotesSpecificationsFullModel extends ConsignmentNotes.IConsignmentNotesSpecificationsModel {
		docId: number;
		inventoryId: number;
		unitId: number;
	}
	interface IConsignmentNotesSpecificationsModel {
		consignmentNotesDocumentName: string;
		cost: number;
		costVAT: number;
		inventoryName: string;
		isVATIncluded: boolean;
		price: number;
		priceVAT: number;
		pVAT: number;
		quantity: number;
		recId: number;
		unitName: string;
		vAT: number;
		vATRate: number;
	}
	interface IConsignmentNotesSpecificationsUpdateModel {
		cost: number;
		costVAT: number;
		docId: number;
		inventoryId: number;
		isVATIncluded: boolean;
		price: number;
		priceVAT: number;
		pVAT: number;
		quantity: number;
		recId: number;
		unitId: number;
		vAT: number;
		vATRate: number;
	}
}
declare namespace ContractInvents {
	interface IContractInventModel {
		baId: number;
		contractId: number;
		endDate: Date;
		inventId: number;
		inventName: string;
		orderNum: number;
		startDate: Date;
		stateId: number;
		stateName: string;
		userId: number;
		userName: string;
	}
	interface IContractInventsRequestModel {
		contractId: number;
		states: number[];
	}
}
declare namespace Contracts {
	interface IContractElementModel extends Elements.IElementExtModel {
		addrId: number;
		contractElementId: number;
		contractId: number;
		elementCode: string;
		endDate: Date;
		hasItems: boolean;
		isProject: boolean;
		nomenclatureId: number;
		note: string;
		orderNum: number;
		parentId: number;
		startDate: Date;
	}
	interface IContractElementTermModel extends Contracts.ITermBaseModel {
		contractElementId: number;
	}
	interface IContractInfoModel extends Contracts.IContractModel {
		consumerName: string;
		supplierName: string;
	}
	interface IContractModel {
		consumerId: number;
		contractId: number;
		endDate: Date;
		number: string;
		startDate: Date;
		supplierId: number;
		terms: Contracts.IContractTermModel[];
	}
	interface IContractNomenclatureModel extends Nomenclatures.INomenclatureModel {
		contractId: number;
		contractNomenclatureId: number;
		endDate: Date;
		isProject: boolean;
		startDate: Date;
	}
	interface IContractNomenclatureSaveModel {
		contractId: number;
		contractNomenclatureId: number;
		endDate: Date;
		nomenclatureId: number;
		startDate: Date;
		supplierId: number;
	}
	interface IContractNomenclatureTermModel extends Contracts.ITermBaseModel {
		contractNomenclatureId: number;
	}
	interface IContractTermModel extends Contracts.ITermBaseModel {
		contractId: number;
	}
	interface ILSContractModel {
		baId: number;
		baTypeId: number;
		caId: number;
		companyAreaName: string;
		creatorId: number;
		docDate: Date;
		endDate: Date;
		isDelState: boolean;
		isFirstState: boolean;
		lsId: number;
		note: string;
		number: string;
		startDate: Date;
		state: string;
		stateId: number;
		typeId: number;
		typeName: string;
		ukGroup: string;
	}
	interface ITermBaseModel extends Markers.IMarkerValueModelBase {
		endDate: Date;
		note: string;
		startDate: Date;
	}
}
declare namespace DocAcceptAssets {
	interface IIDocAcceptAssetsData {
		baId: number;
		baTypeId: number;
		caId: number;
		caName: string;
		docDate: Date;
		evidence: string;
		hasSpecification: boolean;
		lsId: number;
		lsName: string;
		markerValues: Markers.IMarkerValueModel[];
		note: string;
		number: string;
		riId: number;
		riName: string;
		stateId: number;
		whId: number;
		whName: string;
	}
	interface IIDocAcceptAssetsRows {
		barCode: string;
		checkDate: Date;
		docAcceptId: number;
		employeeId: number;
		faId: number;
		inventoryId: number;
		inventoryName: string;
		inventoryNumber: string;
		recId: number;
		releaseDate: Date;
		serialNumber: string;
	}
	interface IIReceiptInvoiceSelectModel {
		id: number;
		value: string;
	}
	interface IReceiptInvoiceInventSelectModel {
		caId: number;
		code: string;
		docId: number;
		id: number;
		quantity: number;
		value: string;
	}
}
declare namespace DocMoveAssets {
	interface IAssetsDataModel {
		baId: number;
		barCode: string;
		baTypeId: number;
		checkDate: Date;
		docAcceptId: number;
		employeeId: number;
		inventoryId: number;
		inventoryNumber: string;
		lsId: number;
		mlpId: number;
		releaseDate: Date;
		serialNumber: string;
		stateId: number;
	}
	interface IDocMoveAssetsData {
		baId: number;
		baTypeId: number;
		caId: number;
		caName: string;
		docDate: Date;
		dstLocationId: number;
		dstLSId: number;
		dstLSName: string;
		dstMLPId: number;
		dstMLPName: string;
		evidence: string;
		hasSpecification: boolean;
		markerValues: Markers.IMarkerValueModel[];
		note: string;
		number: string;
		reasonId: number;
		srcLocationId: number;
		srcLSId: number;
		srcLSName: string;
		srcMLPId: number;
		srcMLPName: string;
		stateId: number;
	}
	interface IDocMoveAssetsRow {
		barCode: string;
		checkDate: Date;
		docMoveId: number;
		evidence: string;
		evidenceId: number;
		evidenceName: string;
		faId: number;
		faName: string;
		recId: number;
		releaseDate: Date;
		serialNumber: string;
	}
}
declare namespace Documents {
	interface IDocumentTreeItemModel {
		baTypeId: number;
		branchId: number;
		name: string;
		parentId: number;
		treeLevel: number;
	}
}
declare namespace Elements {
	interface IElementExtModel extends Elements.IElementModel {
		address: string;
		elementType: string;
		elementTypeCode: string;
	}
	interface IElementModel {
		elementId: number;
		name: string;
	}
	interface IElementRetModel extends Elements.IElementModel {
		markers: Markers.IMarkerValueModelRet[];
		schemas: Markers.IPeriodicMarkerValueModelRet[];
	}
}
declare namespace Entities {
	interface IEntityInfoModel {
		baTypeId: number;
		implementTypeName: string;
		isGroupType: boolean;
		isImplementType : boolean;
		markersInfo: Markers.IMarkerInfoModel[];
		mvcAlias: string;
		name: string;
		parentBATypeId: number;
		searchTypeId: number;
		states: Entities.IEntityStatesModel[];
	}
	interface IEntityStatesModel {
		id: number;
		name: string;
	}
	interface IEntityViewItemModel {
		isActive: boolean;
		itemId: number;
		itemName: string;
		markerId: number;
		orderNum: number;
		viewName: string;
	}
}
declare namespace Entity {
	interface IEntityBaseModel {
		baId: number;
		markerValues: Markers.IMarkerValueModel[];
	}
	interface IEntityInfoCollectionModel {
		items: Entity.IEntityInfoItemModel[];
	}
	interface IEntityInfoItemModel {
		baId: number;
		code: string;
		desc: string;
		fullName: string;
		implementTypeName: string;
		mvcAlias: string;
		name: string;
		parentTypeId: number;
		stateId: number;
		stateName: string;
		treeType: HIS.Models.Layer.Models.Entity.EntityTreeType;
		typeId: number;
		typeName: string;
	}
	interface IEntityModel extends Entity.IEntityBaseModel {
	}
	interface IEntityUpdateBaseModel {
		baId: number;
		markerValues: Markers.IMarkerValueModel[];
	}
	interface IFullTextSearchEntityInfoCollectionModel {
		items: Entity.IFullTextSearchEntityInfoModel[];
	}
	interface IFullTextSearchEntityInfoModel {
		baId: number;
		endDate: Date;
		name: string;
		startDate: Date;
		stateId: number;
		stateName: string;
		typeId: number;
		typeName: string;
	}
}
declare namespace HIS.Models.Layer.Models.QueryBuilder {
	interface IFilteringsTreeField {
		AndFields: Querybuilder.IFilteringModel[];
		OrFields: Querybuilder.IFilteringModel[];
	}
}
declare namespace History {
	interface IEntityHistoryModel {
		baId: number;
		color: number;
		dstStateId: number;
		dstStateName: string;
		endDate: Date;
		gRecId: number;
		itemId: number;
		markerId: number;
		markerName: string;
		markerValue: string;
		markerValueBAId: number;
		oldEndDate: Date;
		oldMarkerValue: string;
		oldStartDate: Date;
		operationDate: Date;
		operationId: number;
		operationName: string;
		recId: number;
		srcStateId: number;
		srcStateName: string;
		startDate: Date;
		userId: number;
		userName: string;
	}
}
declare namespace Inventory {
	interface IIGroupsModel {
		id: number;
		name: string;
		note: string;
		parentId: number;
	}
	interface IIInventoryModel {
		buyUnitId: number;
		caId: number;
		code: string;
		id: number;
		kindId: number;
		name: string;
		parentId: number;
		resourceTypeId: number;
		saleUnitId: number;
		stockUnitId: number;
		typeId: number;
	}
	interface IInventoryDataModel {
		BAId: number;
		BuyNatMeaning: number;
		Code: string;
		CompanyArea: number;
		CompanyName: string;
		Inventory: number;
		InventoryKind: number;
		InventoryName: string;
		InventoryType: number;
		markerValues: Markers.IMarkerValueModel[];
		Name: string;
		ResourceTypeId: number;
		SaleNatMeaning: number;
		soState: number;
		StockNatMeaning: number;
	}
	interface IInventoryListFiltersModel {
		baId: number;
		buyUnitName: string;
		caId: number;
		kindName: string;
		name: string;
		parentName: string;
		saleUnitName: string;
		stateName: string;
		stockUnitName: string;
		typeName: string;
	}
	interface IInventoryListItemModel {
		baId: number;
		baTypeId: number;
		buyUnitId: number;
		buyUnitName: string;
		caId: number;
		code: string;
		companyName: string;
		kindId: number;
		kindName: string;
		name: string;
		parentId: number;
		parentName: string;
		saleUnitId: number;
		saleUnitName: string;
		stateId: number;
		stateName: string;
		stockUnitId: number;
		stockUnitName: string;
		typeId: number;
		typeName: string;
	}
	interface ILSCompanyAreas {
		id: number;
		isActive: boolean;
		lsId: number;
		name: string;
	}
	interface IRefSysUnitsModel {
		code: number;
		id: number;
		name: string;
		symbol: string;
		unitClassId: number;
		unitId: number;
	}
}
declare namespace InventoryObjectsMovementHistory {
	interface IInventoryAssetsModel {
		bAId: number;
		barCode: string;
		checkDate: Date;
		companyAreaName: string;
		createdDate: Date;
		docName: string;
		employeeName: string;
		inventoryName: string;
		inventoryNumber: string;
		location: string;
		lSName: string;
		mRPName: string;
		releaseDate: Date;
		serialNumber: string;
		state: string;
		stateId: number;
	}
	interface IInventoryAssetsMovementHistoryModel {
		docId: number;
		docName: string;
		endDate: Date;
		inventoryAssetId: number;
		inventoryAssetNumber: string;
		location: string;
		mRPId: number;
		mRPName: string;
		recId: number;
		startDate: Date;
	}
}
declare namespace LegalPersons {
	interface IAffiliateOrganisationModel extends LegalPersons.ILegalPersonImplementedModel {
		markerValues: Markers.IMarkerValueModel[];
		parentId: number;
	}
	interface IContactClientModel {
		contactId: number;
		contactTypeId: number;
		deleted: boolean;
		emails: LegalPersons.IEmailClientModel[];
		note: string;
		phones: LegalPersons.IPhoneClientModel[];
		startDate: Date;
	}
	interface IContactModel {
		contactId: number;
		contactTypeId: number;
		contactTypeName: string;
		emails: LegalPersons.IEmailModel[];
		endDate: Date;
		note: string;
		phones: LegalPersons.IPhoneModel[];
		startDate: Date;
	}
	interface IEmailClientModel {
		deleted: boolean;
		email: string;
		emailId: number;
		firstName: string;
		lastName: string;
		mailConfirmTypeId: number;
		mailTypes: LegalPersons.IMailTypeModel[];
		middleName: string;
	}
	interface IEmailModel {
		email: string;
		emailId: number;
		firstName: string;
		lastName: string;
		mailConfirmTypeId: number;
		mailConfirmTypeName: string;
		mailTypes: LegalPersons.IMailTypeModel[];
		middleName: string;
	}
	interface ILegalPersonCollectionModel {
		items: LegalPersons.ILegalPersonItemModel[];
		totalCount: number;
	}
	interface ILegalPersonImplementedModel {
		baTypeId: number;
		code: string;
		fullName: string;
		inn: string;
		legalPersonId: number;
		name: string;
		note: string;
	}
	interface ILegalPersonInfoModel {
		code: string;
		contacts: LegalPersons.IContactModel[];
		fullName: string;
		legalPersonId: number;
		markers: Markers.IMarkerValueModel[];
		name: string;
		schemas: Markers.IMarkerValueModel[];
	}
	interface ILegalPersonItemModel {
		code: string;
		fullName: string;
		legalPersonId: number;
		name: string;
	}
	interface ILegalPersonModel extends LegalPersons.ILegalPersonImplementedModel {
		Markers: Markers.IMarkerValueModel[];
	}
	interface IMailTypeModel {
		itemName: string;
		markerItemId: number;
		value: string;
	}
	interface IPhoneClientModel {
		deleted: boolean;
		firstName: string;
		lastName: string;
		mailTypes: LegalPersons.IMailTypeModel[];
		middleName: string;
		number: string;
		phoneId: number;
		phoneTypeId: number;
		sendConfirmTypeId: number;
	}
	interface IPhoneModel {
		firstName: string;
		lastName: string;
		mailTypes: LegalPersons.IMailTypeModel[];
		middleName: string;
		number: string;
		phoneId: number;
		phoneTypeId: number;
		phoneTypeName: string;
		sendConfirmTypeId: number;
		sendConfirmTypeName: string;
	}
}
declare namespace MainMenu {
	interface IMenuItemModel {
		disabled: boolean;
		icon: string;
		id: number;
		items: MainMenu.IMenuItemModel[];
		moduleName: string;
		mvcAliases: string[];
		name: string;
		text: string;
	}
}
declare namespace Markers {
	interface IEntityCaptionModel {
		baId: number;
		caption: string;
	}
	interface IEntityMarkersModel {
		baId: number;
		markerValues: Markers.IMarkerValueModel[];
	}
	interface IEntityMarkersValueListModel {
		markerId: number;
		values: Markers.IMarkerItemValueModel[];
	}
	interface IEntityUpdateModel extends Markers.IEntityMarkersModel {
		baTypeId: number;
	}
	interface IListItemModel {
		itemId: number;
		value: string;
	}
	interface IMarkeHistoryChangesModel {
		baId: number;
		changes: Markers.IPeriodicMarkerValueModel[];
		markerId: number;
		removed: Markers.IPeriodicMarkerValueModel[];
	}
	interface IMarkerHistoryModel {
		endDate: Date;
		startDate: Date;
		sValue: string;
	}
	interface IMarkerInfoModel {
		catalogImplementTypeName: string;
		id: number;
		implementTypeField: string;
		implementTypeName: string;
		isBlocked: boolean;
		isCollectible: boolean;
		isImplemented: boolean;
		isOptional: boolean;
		isPeriodic: boolean;
		isRequired: boolean;
		label: string;
		list: Markers.IListItemModel[];
		markerType: HIS.Models.Layer.Models.Markers.MarkerType;
		name: string;
		refBaTypeId: number;
		searchTypeId: number;
	}
	interface IMarkerItemValueModel {
		itemId: number;
		markerId: number;
		value: string;
	}
	interface IMarkerValueListItemModel {
		id: number;
		markerId: number;
		name: string;
	}
	interface IMarkerValueModel {
		displayValue: string;
		endDate: Date;
		isBlocked: boolean;
		isDeleted: boolean;
		isVisible: boolean;
		itemId: number;
		markerId: number;
		markerType: HIS.Models.Layer.Models.Markers.MarkerType;
		MVCAliase: string;
		note: string;
		startDate: Date;
		value: string;
	}
	interface IMarkerValueModelBase {
		entityTypeId: number;
		markerId: number;
		markerName: string;
		markerType: string;
		markerTypeId: number;
		range: Markers.IMarkerValueListItemModel[];
		value: string;
	}
	interface IMarkerValueModelRet {
		markerId: number;
		value: string;
	}
	interface IMarkerValueSetModel {
		baId: number;
		markerId: number;
		startDate: Date;
		value: string;
	}
	interface IMarkerValuesRequestModel {
		baId: number;
		markerIds: number[];
		names: string[];
	}
	interface IOptionalMarkersModel {
		baTypeId: number;
		isVisible: boolean;
		markerId: number;
		optionalMarkerId: number;
	}
	interface IPeriodicMarkerValueModel {
		displayValue: string;
		endDate: Date;
		note: string;
		startDate: Date;
		value: string;
	}
	interface IPeriodicMarkerValueModelRet {
		deleted: boolean;
		markerId: number;
		startDate: Date;
		value: string;
	}
	interface IReferenceMarkerValueItemModel {
		ItemId: number;
		ItemName: string;
	}
}
declare namespace Nomenclatures {
	interface INomenclatureModel {
		baTypeId: number;
		buyNatMeaningId: number;
		buyNatMeaningName: string;
		buyNatMeaningNote: string;
		name: string;
		nomenclatureId: number;
		saleNatMeaningId: number;
		saleNatMeaningName: string;
		saleNatMeaningNote: string;
		stockNatMeaningId: number;
		stockNatMeaningName: string;
		stockNatMeaningNote: string;
		taxRateId: number;
		taxRateName: string;
		taxRateNote: string;
	}
	interface INomenclatureSaveModel {
		baTypeId: number;
		buyNatMeaningId: number;
		name: string;
		nomenclatureId: number;
		saleNatMeaningId: number;
		stockNatMeaningId: number;
		taxRateId: number;
	}
}
declare namespace Operations {
	interface IEntityOperationModel {
		baId: number;
		DestStateId: number;
		isBlocked: boolean;
		isDelState: boolean;
		isFirstState: boolean;
		operationId: number;
		operationName: string;
		srcStateId: number;
		typeId: number;
	}
	interface IEntityStateModel {
		allowedOperations: Operations.IEntityOperationModel[];
		baId: number;
		baTypeId: number;
		isDelState: boolean;
		isFirstState: boolean;
		stateId: number;
		stateName: string;
		typeName: string;
	}
	interface IOperationUpdateModel {
		baId: number;
		operationId: number;
	}
}
declare namespace Persons {
	interface IPersonHeaderModel {
		code: string;
		fullName: string;
		personId: number;
	}
	interface IPersonImplementsModel {
		birthDate: Date;
		code: string;
		firstName: string;
		fullName: string;
		gender: number;
		inn: string;
		lastName: string;
		middleName: string;
		personId: number;
	}
	interface IPersonInfoModalBase {
		personId: number;
	}
	interface IPersonInfoModel extends Persons.IPersonInfoModalBase {
		markersId: number[];
	}
	interface IPersonInfoResponseModel extends Persons.IPersonInfoModalBase {
		markers: Markers.IMarkerValueModel[];
	}
	interface IPersonInfoUpdateModel extends Persons.IPersonInfoModalBase {
		markers: Markers.IMarkerValueModel[];
	}
	interface IPersonModel extends Persons.IPersonImplementsModel {
		markerValues: Markers.IMarkerValueModel[];
	}
	interface IPersonUpdateModel extends Persons.IPersonImplementsModel {
		markerValues: Markers.IMarkerValueModel[];
	}
}
declare namespace Querybuilder {
	interface ICaptionAndAliaseModel {
		aliase: string;
		caption: string;
		markerId: number;
		markerTypeId: number;
		type: HIS.Models.Layer.Models.QueryBuilder.FrontEndDataType;
	}
	interface IFilteringModel {
		field: string;
		filterOperator: HIS.Models.Layer.Models.QueryBuilder.FiltersOperator;
		value: any;
	}
	interface IOrderingModel {
		orderingField: string;
		sortingOrder: HIS.Models.Layer.Models.QueryBuilder.SortingOrderModel;
	}
}
declare namespace QueryBuilderModel {
	interface IOrderingModel {
		baTypeId: number;
		filteringsFields: Querybuilder.IFilteringModel[];
		filteringsGroupOperator: HIS.Models.Layer.Models.QueryBuilder.FilteringGroupOperator;
		filteringsTreeFields: HIS.Models.Layer.Models.QueryBuilder.IFilteringsTreeField;
		from: number;
		needFirstLevelOfTypesHierarchy: boolean;
		orderingsFields: Querybuilder.IOrderingModel[];
		selectedPropertyes: string[];
		to: number;
	}
}
declare namespace States {
	interface IEntityTypeStateModel {
		baTypeId: number;
		isDelState: boolean;
		isFirstState: boolean;
		name: string;
		stateId: number;
	}
	interface IStateInfoModel {
		baTypeId: number;
		isDelState: boolean;
		isFirstState: boolean;
		stateId: number;
		stateName: string;
	}
}
declare namespace Suppliers {
	interface ISupplierItemModel extends LegalPersons.ILegalPersonItemModel {
		supplierId: number;
	}
}
declare namespace System {
	interface IGuid {
	}
}
declare namespace Test {
	interface ITestDataRequestModel {
		isMessage: boolean;
		value: string;
	}
}
declare namespace Tree {
	interface ISystemTreeItemModel {
		baId: number;
		baTypeId: number;
		baTypeName: string;
		baTypeNameShort: string;
		children: Tree.ISystemTreeItemModel[];
		mvcAlias: string;
		name: string;
		stateId: number;
		stateName: string;
	}
}
declare namespace UniversalSearch {
	interface ISearchResultModel {
		items: UniversalSearch.ISimpleSearchItemModel[];
		rowCount: number;
	}
	interface ISimpleSearchItemModel {
		baId: number;
		baTypeId: number;
		code: string;
		INN: string;
		legalFormName: string;
		name: string;
		stateName: string;
		typeName: string;
	}
	interface IUniversalSearchRequestModel {
		baTypeId: number;
		from: number;
		showDeleted: boolean;
		to: number;
		value: string;
	}
}
declare namespace Warehouses {
	interface IInventoryTransactionModel {
		accId: number;
		accountName: string;
		accountNum: string;
		caId: number;
		companyName: string;
		docId: number;
		inventoryId: number;
		inventoryName: string;
		isIncome: boolean;
		isVATIncluded: boolean;
		operationDate: Date;
		priceVAT: number;
		quantity: number;
		recId: number;
		recTypeId: number;
		recTypeName: string;
		sellCost: number;
		sellCostVAT: number;
		storeCost: number;
		VAT: number;
		warehouseId: number;
		warehouseName: string;
	}
}



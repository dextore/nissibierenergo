﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using HIS.DAL.Client.Models.ConsignmentNotes;
using HIS.DAL.Client.Services.ConsignmentNotes;
using HIS.Models.Layer.Models.ConsignmentNotes;

namespace HIS.Web.App.Controllers.API
{
    public class ConsignmentNotesController : HISBaseApiController
    {
        private readonly IConsignmentNotesService _consignmentNotesService;
        public ConsignmentNotesController(IConsignmentNotesService consignmentNotesService)
        {
            _consignmentNotesService = consignmentNotesService;
        }

        [HttpGet]
        public IEnumerable<ConsignmentNotesDocumentModel> GetConsignmentNotesDocuments()
        {
            return _consignmentNotesService.GetConsignmentNotesDocuments().Select(x => Mapper.Map<ConsignmentNotesDocument, ConsignmentNotesDocumentModel>(x));
        }

        [HttpGet]
        public ConsignmentNotesDocumentModel GetConsignmentNotesDocument(long baId)
        {
            var model = _consignmentNotesService.GetConsignmentNotesDocument(baId);

            return Mapper.Map<ConsignmentNotesDocument, ConsignmentNotesDocumentModel>(model);
        }

        [HttpGet]
        public ConsignmentNotesDocumentFullModel GetConsignmentNotesFullDocument(long baId)
        {
            var model = _consignmentNotesService.GetConsignmentNotesFullDocument(baId);

            return Mapper.Map<ConsignmentNotesFullDocument, ConsignmentNotesDocumentFullModel>(model);
        }

        [HttpGet]
        public IEnumerable<ConsignmentNotesSpecificationsModel> GetConsignmentNotesSpecificationByDocument(long docId)
        {
            return _consignmentNotesService.GetConsignmentNotesSpecificationByDocument(docId)
                        .Select(x => Mapper.Map<ConsignmentNotesSpecifications, ConsignmentNotesSpecificationsModel>(x));
        }

        [HttpGet]
        public ConsignmentNotesSpecificationsModel GetConsignmentNotesSpecification(long recId, long docId)
        {
            var model = _consignmentNotesService.GetConsignmentNotesSpecification(recId, docId);

            return Mapper.Map<ConsignmentNotesSpecifications, ConsignmentNotesSpecificationsModel>(model);
        }

        [HttpGet]
        public ConsignmentNotesSpecificationsFullModel GetConsignmentNotesSpecificationFullModel(long recId, long docId)
        {
            var model = _consignmentNotesService.GetConsignmentNotesSpecificationFull(recId, docId);

            return Mapper.Map<ConsignmentNotesSpecificationsFull, ConsignmentNotesSpecificationsFullModel>(model);
        }

        [HttpPost]
        public ConsignmentNotesDocumentModel SaveConsignmentNotesDocument(ConsignmentNotesDocumentUpdateModel consignmentNotesDocumentUpdate)
        {
            var model = _consignmentNotesService.SaveConsignmentNotesDocument(
                Mapper.Map<ConsignmentNotesDocumentUpdateModel, ConsignmentNotesDocumentUpdate>(
                    consignmentNotesDocumentUpdate));

            return Mapper.Map<ConsignmentNotesDocument, ConsignmentNotesDocumentModel>(model);
        }

        [HttpPost]
        public ConsignmentNotesSpecificationsModel SaveConsignmentNotesSpecification(ConsignmentNotesSpecificationsUpdateModel consignmentNotesSpecificationsUpdate)
        {
            var model = _consignmentNotesService.SaveConsignmentNotesSpecification(
                Mapper.Map<ConsignmentNotesSpecificationsUpdateModel, ConsignmentNotesSpecificationsUpdate>(
                    consignmentNotesSpecificationsUpdate));

            return Mapper.Map<ConsignmentNotesSpecifications, ConsignmentNotesSpecificationsModel>(model);
        }

        [HttpPost]
        public bool DeleteConsignmentNotesSpecification([FromUri]long recId, long docId)
        {
            return _consignmentNotesService.DeleteConsignmentNotesSpecification(recId, docId);
        }

        //TODO : добавление/изменение значения маркера экземпляра сущности приходная накладная (в бд есть процедура, по факту не используется)
        //TODO : удаление значения маркера экземпляра сущности приходная накладная (в бд есть процедура, по факту не используется)

    }
}
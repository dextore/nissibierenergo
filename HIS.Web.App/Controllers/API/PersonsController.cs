﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.LegalSubjects;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.Client.Services.LegaSubjects.Persons;
using HIS.DAL.Client.Services.Markers;
using HIS.Models.Layer.Models.LegalSubjects;
using HIS.Models.Layer.Models.Markers;

namespace HIS.Web.App.Controllers.API
{
    /// <inheritdoc />
    /// <summary>
    /// Физ лица.
    /// </summary>
    [RoutePrefix("api/persons")]
    public class PersonsController : HISBaseApiController
    {
        private IPersonsService _lsPersonsService;
        private IMarkersService _markersService;

        public PersonsController(IPersonsService lsPersonsService, IMarkersService markersService)
        {
            _markersService = markersService;
            _lsPersonsService = lsPersonsService;
        }

        [ResponseType(typeof(PersonModel))]
        [HttpGet]
        [Route("GetPerson/{personId}")]
        public IHttpActionResult GetPerson(long personId)
        {
            var person = _lsPersonsService.GetPerson(personId);
            var resultModel = Mapper.Map<Person, PersonModel>(person);
            return Ok(resultModel);
        }

        [ResponseType(typeof(PersonHeaderModel))]
        [HttpGet]
        [Route("GetPersonHeader/{personId}")]
        public IHttpActionResult GetPersonHeader(long personId)
        {
            var person = _lsPersonsService.GetPersonHeader(personId);
            if (person == null)
                return Ok();
            var resultModel = Mapper.Map<PersonHeader, PersonHeaderModel>(person);
            return Ok(resultModel);
        }

        [ResponseType(typeof(IEnumerable<EntityMarkersModel>))]
        [HttpPost]
        public IHttpActionResult Update(PersonUpdateModel person)
        {
            var updateModel = Mapper.Map<PersonUpdateModel, PersonUpdate>(person);
            var personId = _lsPersonsService.Update(updateModel);

            var markers = _markersService.EntityMarkerValues(personId, null);
            var result = markers.Select(m => Mapper.Map<MarkerValue, MarkerValueModel>(m));

            //var personModel = _lsPersonsService.GetPerson(personId);
            //var resultModel = Mapper.Map<Person, PersonModel>(personModel);

            return Ok(new EntityMarkersModel
            {
                BAId = personId,
                MarkerValues = result
            });
        }

        [HttpPost]
        public IHttpActionResult GetPersonInformation(PersonInfoModel personInfo)
        {
            var requestModel = Mapper.Map<PersonInfoModel, PersonInfo>(personInfo);
            var result = _lsPersonsService.GetPersonInformation(requestModel);
            var resultModel = Mapper.Map<PersonInfoResponse, PersonInfoResponseModel>(result);

            return Ok(resultModel);
        }

        [HttpPost]
        public IHttpActionResult UpdatePersonInformation(PersonInfoUpdateModel personInfoUpdate)
        {
            var requestModel = Mapper.Map<PersonInfoUpdateModel, PersonInfoUpdate>(personInfoUpdate);
            var result = _lsPersonsService.UpdatePersonInformation(requestModel);
            var resultModel = Mapper.Map<PersonInfoResponse, PersonInfoResponseModel>(result);

            return Ok(resultModel);
        }
        
    }
}
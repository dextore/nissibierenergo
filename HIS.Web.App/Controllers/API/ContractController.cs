﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Contracts;
using HIS.DAL.Client.Services.Contracts;
using HIS.Models.Layer.Models.Contracts;
//using HIS.Web.App.Models.Contracts;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace HIS.Web.App.Controllers.API
{
    [RoutePrefix("api/Contract")]
    public class ContractController : HISBaseApiController
    {
        private readonly IContractsService _contractsService;
        private readonly ILSContractsService _lcContractsService;

        public ContractController(IContractsService contractsService, ILSContractsService lcContractsService)
        {
            _lcContractsService = lcContractsService;
            _contractsService = contractsService;
        }

        [ResponseType(typeof(IEnumerable<LSContractModel>))]
        [Route("ByLegalSubject/{legaSubjectId}")]
        public IHttpActionResult ByLegalSubject(long legaSubjectId)
        {
            var contracts = _lcContractsService.ByLegalSubject(legaSubjectId);
            var result = contracts.Select(m => Mapper.Map<LSContract, LSContractModel>(m)).ToArray();
            return Ok(result);
        }

        [Route("{contractorId}")]
        public IHttpActionResult GetContractsByContractor(long contractorId)
        {
            var contracts = _contractsService.GetContractsByContractor(contractorId);
            var result = contracts.Select(m => Mapper.Map<Contract, ContractModel>(m));
            return Ok(result);
        }
    }
}

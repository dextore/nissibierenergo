﻿using AutoMapper;
using HIS.DAL.Client.Models.DocAcceptAssets;
using HIS.DAL.Client.Services.DocAcceptAssets;
using HIS.DAL.DB.Context;
using HIS.Models.Layer.Models.DocAcceptAssets;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;



namespace HIS.Web.App.Controllers.API
{
    public class DocAcceptAssetsController : HISBaseApiController
    {
        private IMapper _mapper;
        private IDocAcceptAssetsService _service; 

        public DocAcceptAssetsController(IDocAcceptAssetsService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;

        }
        [ResponseType(typeof(List<DocAcceptAssetsRowsModel>))]
        [HttpPost]
        public IHttpActionResult GetDocAcceptAssetsRows(long docAcceptId)
        {
            var list = _service.GetDocAcceptAssetsRows(docAcceptId);
            var result = list.Select(n => _mapper.Map<DocAcceptAssetsRow, DocAcceptAssetsRowsModel>(n));
            return Ok(result);
          
        }

        [ResponseType(typeof(DocAcceptAssetsRowsModel))]
        [HttpPost]
        public IHttpActionResult GetDocAcceptAssetsRow(long docAcceptId, long recId)
        {
            var model = _service.GetDocAcceptAssetsRow(docAcceptId, recId);
            var result = Mapper.Map<DocAcceptAssetsRow, DocAcceptAssetsRowsModel>(model);
            return Ok(result);

        }
        [ResponseType(typeof(DocAcceptAssetsDataModel))]
        [HttpPost]
        public IHttpActionResult GetDocAcceptAssetsData(long baId)
        {           
            var docAcceptAssets = _service.GetDocAcceptAssets(baId);
            var result = Mapper.Map<DocAcceptAssetsData, DocAcceptAssetsDataModel>(docAcceptAssets);
            return Ok(result);

        }
        [ResponseType(typeof(long))]
        [HttpPost]
        public IHttpActionResult UpdateDocAcceptAssets(DocAcceptAssetsDataModel saveData)
        {
            var updateModel = Mapper.Map<DocAcceptAssetsDataModel,DocAcceptAssetsData>(saveData);
            long baId = _service.UpdateDocAcceptAssets(updateModel);
            return Ok(baId);
        }
        [ResponseType(typeof(ReceiptInvoiceSelectModel))]
        [HttpPost]
        [HttpGet]
        public IHttpActionResult GetReceiptInvoiceSelect(long caId)
        {
            var list = _service.GetReceiptInvoiceSelect(caId);
            var result = list.Select(n => _mapper.Map<ReceiptInvoiceSelect, ReceiptInvoiceSelectModel>(n));
            return Ok(result);
        }
        [ResponseType(typeof(ReceiptInvoiceInventSelectModel))]
        [HttpPost]
        [HttpGet]
        public IHttpActionResult GetReceiptInvoiceInventSelect(long docAcceptId)
        {
            var list = _service.GetReceiptInvoiceInventSelect(docAcceptId);
            var result = list.Select(n => _mapper.Map<ReceiptInvoiceInventSelect, ReceiptInvoiceInventSelectModel>(n));
            return Ok(result);
        }

        [ResponseType(typeof(DocAcceptAssetsRowsModel))]
        [HttpPost]
        public IHttpActionResult UpdateDocAcceptAssetRow(DocAcceptAssetsRowsModel row)
        {
            var updateModel = Mapper.Map<DocAcceptAssetsRowsModel, DocAcceptAssetsRow>(row);
            var outModel = _service.UpdateDocAcceptAssetRow(updateModel);
            var result = Mapper.Map<DocAcceptAssetsRow, DocAcceptAssetsRowsModel>(outModel);
            return Ok(result);
        }

        [ResponseType(typeof(bool))]
        [HttpPost]
        public IHttpActionResult DeleteDocAcceptAssetRow(DocAcceptAssetsRowsModel row)
        {
            var deleteModel = Mapper.Map<DocAcceptAssetsRowsModel, DocAcceptAssetsRow>(row);
            bool result = _service.DeleteDocAcceptAssetRow(deleteModel);
            return Ok(result);
        }

        [ResponseType(typeof(bool))]
        [HttpPost]
        public IHttpActionResult IsHasSpecification(long baId)
        {
            bool result = _service.IsHasSpecification(baId);
            return Ok(result);
        }





    }
}
﻿using System;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.Auth.Entities;
using HIS.DAL.Client.Models.Auth;
using HIS.DAL.Client.Services.Auth;
using HIS.Models.Layer.Models.Authentication;
using HIS.Web.App.Models.Authentication;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace HIS.Web.App.Controllers.API
{

/// <summary>
/// Контроллер по аутентификации пользоватлей в системе
/// </summary>
    [Authorize]
    public class AuthController : HISBaseApiController
    {
        private HISSignInManager _signInManager;
        private HISUserManager _userManager;
        private readonly IAuthService _authService;

        /// <summary>
        /// Конструктор
        /// </summary>
        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }
        /// <summary>
        /// Менеджер входа в систему
        /// </summary>
        public HISSignInManager SignInManager
        {
            get { return _signInManager ?? HttpContext.Current.GetOwinContext().Get<HISSignInManager>(); }
            private set { _signInManager = value; }
        }

        /// <summary>
        /// Менеджер пользователей
        /// </summary>
        public HISUserManager UserManager
        {
            get
            { return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<HISUserManager>(); }
            private set { _userManager = value; }
        }

        private IAuthenticationManager AuthenticationManager => HttpContext.Current.GetOwinContext().Authentication;
        private HISUser CurrentUser => HttpContext.Current.GetOwinContext()
            .GetUserManager<HISUserManager>()
            .FindById(User.Identity.GetUserId<int>());

        /// <summary>
        /// Аутентификация пользователя с помощью логина и пароля (метод GET)
        /// </summary>
        /// <param name="username">Имя пользователя</param>
        /// <param name="password">Пароль</param>
        /// <param name="dbalias">Алиас БД к которой необходимо подключиться</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IHttpActionResult> FormLogin(string username, string password, string dbalias = "nskes")
        {
            return await FormLogin(new LoginModel() { UserName = username, Password = password, DBAlias = dbalias });
        }

        /// <summary>
        /// Аутентификация пользователя с помощью логина и пароля (метод POST)
        /// </summary>
        /// <param name="login">Данные для аутентификации</param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<IHttpActionResult> FormLogin(LoginModel login)
        {
            try
            {
                if ((login == null)||(!ModelState.IsValid))
                {
                    return BadRequest("Ошибка валидации параметров аутентификации (не указан логин или пароль).");
                }

                if (string.IsNullOrEmpty(login.DBAlias))
                {
                    login.DBAlias = "nskes";
                }

                var user = await SignInManager.UserManager.FindAsync(login.UserName, login.Password);
                if ((user != null) && (!_authService.CheckUser(user.Id, login.DBAlias)))
                    //if (user?.DataBaseAreas.FirstOrDefault(c => c.Alias.Equals("nskes")) == null)
                    return BadRequest("Задан неверно пользователь или пароль");

                var result =
                    await SignInManager.PasswordSignInAsync(login.UserName, login.Password, login.DBAlias, true, shouldLockout: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        return Ok("Ok.");
                    case SignInStatus.LockedOut:
                        return BadRequest("Пользователь заблокирован, попробуйте авторизоваться позже.");
                    case SignInStatus.RequiresVerification:
                        return BadRequest("Требуется подтверждение регистрации пользователя.");
                    default:                        
                        return BadRequest("Задан неверно пользователь или пароль.");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Расширенная аутентификация (включая доменную аутентификацию)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [HttpGet]
        [AllowAnonymous]
        [HttpOptions]
        [Route("Auth/ExternalLoginCallback")]
        public async Task<IHttpActionResult> ExternalLoginCallback(string dbalias = "nskes")
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return BadRequest("Ошибка получения информации о пользователе расширенной аутентификации.");
            }

            var user = await SignInManager.UserManager.FindAsync(loginInfo.Login);
            if ((user != null) && (!_authService.CheckUser(user.Id, dbalias)))
                return await
                            ExternalLoginConfirmation(
                                new HISExternalLoginConfirmationViewModel()
                                {
                                    Email = loginInfo.Email,
                                    Name = loginInfo.DefaultUserName
                                });

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, dbalias, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return Ok("Ok.");
                case SignInStatus.LockedOut:
                    return BadRequest("Пользователь заблокирован, попробуйте авторизоваться позже.");
                case SignInStatus.RequiresVerification:
                    return BadRequest("Требуется подтверждение регистрации пользователя.");
                default:
                    return
                        await
                            ExternalLoginConfirmation(
                                new HISExternalLoginConfirmationViewModel()
                                {
                                    Email = loginInfo.Email,
                                    Name = loginInfo.DefaultUserName
                                });
            }
        }

        private async Task<IHttpActionResult> ExternalLoginConfirmation(HISExternalLoginConfirmationViewModel model)
        {
            //if ((User.Identity.IsAuthenticated) && (User.Identity.GetType() == typeof(ClaimsIdentity)))
            //{
            //    return Ok("Ok.");
            //}

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return BadRequest("Ошибка валидации параметров аутентификации.");
                }

                string fullName = null;
                UserPrincipal principal = null;
                using (var context = new PrincipalContext(ContextType.Domain))
                {
                    principal = UserPrincipal.FindByIdentity(context, model.Name);

                    if (principal != null)
                    {
                        fullName = $"{principal.DisplayName}";
                    }
                }

                var user = new HISUser { UserName = model.Name, Email = model.Email, Description = fullName, IsDomainUser = true, UserFIO = new HISUserFIO() { FirstName = principal?.GivenName, LastName = principal?.Surname, MiddlName = principal?.MiddleName } };

                var fndResult = await UserManager.FindByNameAsync(user.UserName);
                if (fndResult == null)
                {
                    var result = await UserManager.CreateAsync(user);
                    if (result.Succeeded)
                    {
                        result = await UserManager.AddLoginAsync(user.Id, info.Login);
                        if (result.Succeeded)
                        {
                            if (principal != null)
                            {
                                if (!string.IsNullOrEmpty(principal.Description))
                                    await
                                        UserManager.AddClaimAsync(user.Id,
                                            new Claim("Desription", principal.Description));
                                if (!string.IsNullOrEmpty(principal.EmployeeId))
                                    await
                                        UserManager.AddClaimAsync(user.Id, new Claim("EmployeeId", principal.EmployeeId));
                                if (!string.IsNullOrEmpty(principal.VoiceTelephoneNumber))
                                    await
                                        UserManager.AddClaimAsync(user.Id,
                                            new Claim("VoiceTelephoneNumber", principal.VoiceTelephoneNumber));
                                if (!string.IsNullOrEmpty(principal.SamAccountName))
                                    await
                                        UserManager.AddClaimAsync(user.Id,
                                            new Claim("SamAccountName", principal.SamAccountName));
                            }

                            return BadRequest("Пользователь не найден.");

                            //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                            //return Ok("Ok.");
                        }
                    }

                    string allError = string.Empty;
                    foreach (string err in result.Errors)
                    {
                        allError = allError + "; " + err;
                    }
                    return BadRequest(allError);
                }

                return BadRequest("Пользователь не найден.");
            }


            return BadRequest("Пользователь не найден.");

            //return Ok("Ok.");
        }

        /// <summary>
        /// Выйти из системы
        /// </summary>
        [HttpPost]
        [HttpGet]
        public IHttpActionResult Logoff()
        {
            try
            {
                AuthenticationManager.SignOut();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Получение значения константы БД
        /// </summary>
        /// <param name="domainUser">Имя доменного пользователя</param>
        public IHttpActionResult GetDomainUserInfo(string domainUser)
        {
            DomainUserInfoModel domainUserInfo = new DomainUserInfoModel();
            using (var context = new PrincipalContext(ContextType.Domain))
            {
                var principal = UserPrincipal.FindByIdentity(context, domainUser);

                if (principal != null)
                {
                    domainUserInfo.DisplayName = principal.DisplayName;
                    domainUserInfo.Description = principal.Description;
                    domainUserInfo.EmployeeId = principal.EmployeeId;
                    domainUserInfo.SamAccountName = principal.SamAccountName;
                    domainUserInfo.VoiceTelephoneNumber = principal.VoiceTelephoneNumber;
                }
            }
            return Ok(domainUserInfo);
        }

        [AllowAnonymous]
        public IHttpActionResult GetUserInfo()
        {                      
            UserInfoModel user = null;
            if (!User.Identity.IsAuthenticated)
                return Ok((UserInfoModel) null);

            Claim dbAliasClaim = (User.Identity as ClaimsIdentity)?.Claims.FirstOrDefault(c => c.Type.Equals("DBAlias"));
            if (dbAliasClaim == null) return Ok((UserInfoModel) null);

            HISUser cUser = CurrentUser;
            if (cUser != null)
            {
                user = new UserInfoModel()
                {
                    UserName = cUser.Description
                };
            }
            return Ok(user);
        }

        /// <summary>
        /// Проверка прав пользователя
        /// </summary>
        /// <param name="rights">Права, перечисленные через запятую: "right1,right2"</param>
        /// <returns>true, если у пользователя есть хотя-бы одно из перечисленных прав, иначе - false</returns>
        [HttpGet]
        public bool CheckRights(string rights)
        {
            return _authService.CheckRights(rights);
        }

        /// <summary>
        /// Проверка ролей пользователя
        /// </summary>
        /// <param name="roles">Роли, перечисленные через запятую: "role1,role2"</param>
        /// <returns>true, если у пользователя есть хотя-бы одна из перечисленных ролей, иначе - false</returns>
        [HttpGet]
        public bool CheckRoles(string roles)
        {
            return _authService.CheckRoles(roles);
        }

        /// <summary>
        /// Проверка пользователя
        /// </summary>
        /// <param name="users">Пользователи, перечисленные через запятую: "user1,user2"</param>
        /// <returns>true, если среди указанных пользователей указан текущий пользователь, иначе - false</returns>
        [HttpGet]
        public bool CheckUsers(string users)
        {
            return _authService.CheckUsers(users);
        }

        /// <summary>
        /// Получение списка БД сервера аутентификации
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [ResponseType(typeof(AreaDataBaseModel[]))]
        public IHttpActionResult GetDataBaseAreas()
        {
            var dataBaseAreas = _authService.GetDataBaseAreas();
            var result = dataBaseAreas.Select(c => Mapper.Map<AreaDataBase, AreaDataBaseModel>(c)).ToArray();
            return Ok(result);          
        }
    }
}
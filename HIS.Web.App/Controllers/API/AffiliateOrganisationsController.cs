﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.LegalSubjects.AffiliateOrganisations;
using HIS.DAL.Client.Services.LegaSubjects.AffiliateOrganisations;
using HIS.Models.Layer.Models.LegalPersons;

namespace HIS.Web.App.Controllers.API
{

    [RoutePrefix("api/AffiliateOrganisations")]
    public class AffiliateOrganisationsController :  HISBaseApiController
    {
        private readonly IAffiliateOrganisationsService _affiliateOrganisationsService;

        public AffiliateOrganisationsController(IAffiliateOrganisationsService affiliateOrganisationsService)
        {
            _affiliateOrganisationsService = affiliateOrganisationsService;
        }

        [ResponseType(typeof(AffiliateOrganisationModel[]))]
        [Route("GetOrganisations/{organisactionId}")]
        public IHttpActionResult GetAffiliateOrganisations(long organisactionId)
        {
            var items = _affiliateOrganisationsService.GetAffiliateOrganisations(organisactionId);
            var resultItems = items.Select(m => Mapper.Map<AffiliateOrganisation, AffiliateOrganisationModel>(m));
            return Ok(resultItems);
        }

        [ResponseType(typeof(AffiliateOrganisationModel))]
        [Route("GetOrganisation/{affiliateOrganisactionId}")]
        [HttpPost]
        public IHttpActionResult GetAffiliateOrganisation(long affiliateOrganisactionId, [FromBody]IEnumerable<int> markersIds)
        {
            var items = _affiliateOrganisationsService.GetAffiliateOrganisation(affiliateOrganisactionId, markersIds);
            var resultItems = Mapper.Map<AffiliateOrganisation, AffiliateOrganisationModel>(items);
            return Ok(resultItems);
        }

        [HttpPost]
        [Route("Save")]
        public IHttpActionResult Save(AffiliateOrganisationModel model)
        {
            var requestModel = Mapper.Map<AffiliateOrganisationModel, AffiliateOrganisation>(model);

            var responceModel = _affiliateOrganisationsService.Update(requestModel);
            var result = Mapper.Map<AffiliateOrganisation, AffiliateOrganisationModel>(responceModel);

            return Ok(result);
        }
    }
}

﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.BankAccounts;
using HIS.DAL.Client.Services.LegaSubjects.BankAccounts;
using HIS.Models.Layer.Models.LegalSubjects.BankAccounts;

namespace HIS.Web.App.Controllers.API
{
    [RoutePrefix("api/LSBankAccounts")]
    public class LSBankAccountsController : HISBaseApiController
    {
        private readonly ILSBankAccountService _lsBankAccountService;

        public LSBankAccountsController(ILSBankAccountService lsBankAccountService)
        {
            _lsBankAccountService = lsBankAccountService;
        }

        [ResponseType(typeof(LSBankAccountModel[]))]
        [Route("GetLSBankAccounts/{baId}")]
        public IHttpActionResult GetLSBankAccounts(long baId)
        {
            var bankAccounts = _lsBankAccountService.GetLSBankAccounts(baId);
            var result = bankAccounts.Select(l => Mapper.Map<LSBankAccount, LSBankAccountModel>(l));
            return Ok(result);
        }

        [ResponseType(typeof(LSBankAccountModel[]))]
        [Route("GetAllLSBankAccounts/{baId}")]
        public IHttpActionResult GetAllLSBankAccounts(long baId)
        {
            var bankAccounts = _lsBankAccountService.GetAllLSBankAccounts(baId);
            var result = bankAccounts.Select(l => Mapper.Map<LSBankAccount, LSBankAccountModel>(l));
            return Ok(result);
        }

        [ResponseType(typeof(LSBankAccountModel))]
        [Route("GetLSBankAccount/{baId}/{itemId}")]
        public IHttpActionResult GetLSBankAccounts(long baId, int itemId)
        {
            var bankAccounts = _lsBankAccountService.GetLSBankAccount(baId, itemId);
            var result = Mapper.Map<LSBankAccount, LSBankAccountModel>(bankAccounts);
            return Ok(result);
        }

        [HttpPost]
        [ResponseType(typeof(LSBankAccountModel))]
        public IHttpActionResult Update(LSBankAccountModel model)
        {
            var requestModel = Mapper.Map<LSBankAccountModel, LSBankAccount>(model);
            var bankAccount = _lsBankAccountService.Update(requestModel);
            var result = Mapper.Map<LSBankAccount, LSBankAccountModel>(bankAccount);
            return Ok(result);
        }

        [HttpPost]
        [ResponseType(typeof(bool))]
        [Route("Remove/{baId}/{itemId}")]
        public IHttpActionResult Remove(long baId, int itemId)
        {
            var result = _lsBankAccountService.Remove(baId, itemId);
            return Ok(result);
        }

    }
}

﻿using AutoMapper;
using HIS.DAL.Client.Models.Nomenclatures;
using HIS.DAL.Client.Services.Nomenclatures;
using HIS.Models.Layer.Models.Nomenclatures;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace HIS.Web.App.Controllers.API
{
    public class NomenclatureController : HISBaseApiController
    {
        private INomenclatureService _nomenclatureService;
        private IMapper _mapper;

        public NomenclatureController(INomenclatureService nomenclatureService, IMapper mapper)
        {
            _nomenclatureService = nomenclatureService;
            _mapper = mapper;
        }

        [ResponseType(typeof(IEnumerable<NomenclatureModel>))]
        public IHttpActionResult GetNomenclatures()
        {
            var nomenclatures = _nomenclatureService.GetNomenclatures();
            var models = nomenclatures.Select(n => _mapper.Map<Nomenclature, NomenclatureModel>(n));
            return Ok(models);
        }

        [HttpPost]
        [ResponseType(typeof(NomenclatureModel))]
        public IHttpActionResult Save(NomenclatureSaveModel nomenclature)
        {
            if (ModelState.IsValid)
            {
                var model = _mapper.Map<NomenclatureSaveModel, NomenclatureSave>(nomenclature);
                var rNmk = _nomenclatureService.Save(model);
                var result = _mapper.Map<Nomenclature, NomenclatureModel>(rNmk);
                return Ok(result);
            }
            return BadRequest(ModelState);
        }
    }
}

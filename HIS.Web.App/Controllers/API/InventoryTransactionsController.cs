﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.InventoryTransactions;
using HIS.DAL.Client.Services.InventoryTransactions;
using HIS.Models.Layer.Models.InventoryTransactions;

namespace HIS.Web.App.Controllers.API
{

    /// <summary>
    /// Контроллер для показа складских проводок
    /// </summary>
    [RoutePrefix("api/InventoryTransactions")]
    public class InventoryTransactionsController : HISBaseApiController
    {
        private readonly IInventoryTransactionsService _inventoryTransactionsService;

        public InventoryTransactionsController(IInventoryTransactionsService _inventoryTransactionsService)
        {
            this._inventoryTransactionsService = _inventoryTransactionsService;
        }

        /// <summary>
        /// Возвращает проводки по документу
        /// </summary>
        /// <param name="baId"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<InventoryTransactionModel>))]
        [Route("{baId}")]
        public IHttpActionResult Get(long baId)
        {
            var items = _inventoryTransactionsService.GetByDocument(baId);
            var result = items.Select(m => Mapper.Map<InventoryTransaction, InventoryTransactionModel>(m));
            return Ok(result);
        }

    }
}

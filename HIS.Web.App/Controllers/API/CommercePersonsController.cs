﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.LegalSubjects.CommercePerson;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.Client.Services.LegaSubjects.CommercePersons;
using HIS.Models.Layer.Models;
using HIS.Models.Layer.Models.LegalSubjects.CommercePersons;
using HIS.Models.Layer.Models.Markers;

namespace HIS.Web.App.Controllers.API
{
    [RoutePrefix("api/commercePersons")]
    public class CommercePersonsController : HISBaseApiController
    {
        private readonly ICommercePersonService _commercePersonService;

        public CommercePersonsController(ICommercePersonService CommercePersonService)
        {
            _commercePersonService = CommercePersonService;
        }

        [ResponseType(typeof(CommercePersonModel))]
        [Route("GetPerson/{personId}")]
        public IHttpActionResult GetPerson(long personId)
        {
            var result = _commercePersonService.GetCommercePerson(personId);
            var model = Mapper.Map<CommercePerson, CommercePersonModel>(result);
            return Ok(model);
        }


        [ResponseType(typeof(CommercePersonModel))]
        [Route("GetOPFByParent/{parentId}")]
        public IHttpActionResult GetOPFByParent(long parentId)
        {
            var result = _commercePersonService.GetOPFByParent(parentId);
            var model = Mapper.Map<CommercePerson, CommercePersonModel>(result);
            return Ok(model);
        }

        [ResponseType(typeof(IEnumerable<MarkerValueModel>))]
        [Route("GetAddresseList/{baId}")]
        public IHttpActionResult GetAddresseList(long baId)
        {
            var markers = _commercePersonService.GetAddressList(baId);
            var model = markers.Select(m => Mapper.Map<MarkerValue, MarkerValueModel>(m));
            return Ok(model);
        }

        [ResponseType(typeof(CommercePersonUpdateModel))]
        [HttpPost]
        public IHttpActionResult Update(CommercePersonUpdateModel personInfoUpdate)
        {
            var updateModel = Mapper.Map<CommercePersonUpdateModel, CommercePersonUpdate>(personInfoUpdate);
            var result = _commercePersonService.UpdateCommercePerson(updateModel);
            var model = Mapper.Map<CommercePerson, CommercePersonModel>(result);

            return Ok(model); 
        }

        [ResponseType(typeof(IEnumerable<MarkerValueModel>))]
        [HttpPost]
        public IHttpActionResult SaveAddresList(EntityModel model)
        {
            if (model.MarkerValues == null)
                return Ok();

            var markers = _commercePersonService.SaveAddresList((long)model.BAId, model.MarkerValues.Select(m => Mapper.Map<MarkerValueModel, MarkerValue>(m)));
            var result = markers.Select(m => Mapper.Map<MarkerValue, MarkerValueModel>(m));
            return Ok(result);
        }
    }

    
}
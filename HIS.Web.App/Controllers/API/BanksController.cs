﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.Banks;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.Client.Services.Auth;
using HIS.DAL.Client.Services.Bank;
using HIS.Models.Layer.Models.Banks;
using HIS.Models.Layer.Models.Markers;


namespace HIS.Web.App.Controllers.API
{
    public class BanksController : HISBaseApiController
    {
        private readonly IBanksService _banksService;
        private readonly IAuthService _authService;
        
        public BanksController(IBanksService banksService, IAuthService authService)
        {
            _banksService = banksService;
            _authService = authService;
        }

        [HttpGet]
        public string GetBankHeader(long baId)
        {
            return _banksService.GetBankHeader(baId);
        }

        [HttpGet]
        public IEnumerable<FrontBanksEntity> GetBanks()
        {
            var banks = _banksService.GetBanks();
            var model = banks.Select(bank => Mapper.Map<BanksEntity, FrontBanksEntity>(bank));

            return model;
        }

        [HttpGet]
        public FrontBanksEntity GetBank(long? baId)
        {
            var bank = _banksService.GetBank(baId);
            var model = Mapper.Map<BanksEntity, FrontBanksEntity>(bank);

            return model;
        }

        [HttpGet]
        public IEnumerable<FrontBanksEntity> GetBranchesByBankId(long? baId)
        {
            var branches = _banksService.GetBanksBranches(baId);
            var model = branches.Select(banksBranch => Mapper.Map<BanksEntity, FrontBanksEntity>(banksBranch));
            return model;
        }

        [HttpPost]
        public FrontBanksEntity Save(FrontBanksEntity entity)
        {
            var userId = _authService.GetCurrentUserId();
            var dataAccessModel = Mapper.Map<FrontBanksEntity, BanksEntity>(entity);
            var result =  _banksService.Update(dataAccessModel, userId);
            var model = Mapper.Map<BanksEntity, FrontBanksEntity>(result);

            return model;
        }

        [ResponseType(typeof(PeriodicMarkerValueModel))]
        [Route("api/Banks/GetPeriodicBanksMarkerHistory/{baId}/{markerId}")]
        public IHttpActionResult GetPeriodicBanksMarkerHistory(long baId, int markerId)
        {
            var values = _banksService.GetPeriodicBanksMarkerHistory(baId, markerId);
            var data = values.Select(v => Mapper.Map<PeriodicMarkerValue, PeriodicMarkerValueModel>(v));
            return Ok(data);
        }

        [HttpPost]
        [ResponseType(typeof(PeriodicMarkerValueModel))]
        public IHttpActionResult SavePeriodicMarkerHistory(MarkeHistoryChangesModel model)
        {
            var changes = Mapper.Map<MarkeHistoryChangesModel, MarkeHistoryChanges>(model);
            var current = _banksService.SaveMarkerValue(changes);
            var result = Mapper.Map<PeriodicMarkerValue, PeriodicMarkerValueModel>(current);
            
            return Ok(result);
        }
    }
}
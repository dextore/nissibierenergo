﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using HIS.DAL.Client.Models.Contracts;
using HIS.DAL.Client.Services.Contracts;
using HIS.Models.Layer.Models.ContractInvents;
using HIS.Models.Layer.Models.Contracts;

namespace HIS.Web.App.Controllers.API
{
    [RoutePrefix("api/contractinvents")]
    public class ContractInventsController :  HISBaseApiController
    {
        private readonly IContractInventsService _contractInventsService;
        private readonly IMapper _mapper;

        public ContractInventsController(IContractInventsService contractInventsService, IMapper mapper)
        {
            _mapper = mapper;
            _contractInventsService = contractInventsService;
        }

        [HttpPost]
        [Route("get")]
        [ResponseType(typeof(IEnumerable<ContractInventModel>))]
        public IHttpActionResult Get(ContractInventsRequestModel model)
        {
            var invents = _contractInventsService.GetInvents(model.ContractId, model.States);
            var results = invents.Select(n => _mapper.Map<ContractInvent, ContractInventModel>(n));
            return Ok(results);
        }

        [HttpGet]
        [Route("getitem/{baId}")]
        [ResponseType(typeof(ContractInventModel))]
        public IHttpActionResult GetItem(long baId)
        {
            var item = _contractInventsService.GetInventItem(baId);
            var result = _mapper.Map<ContractInvent, ContractInventModel>(item);
            return Ok(result);
        }

    }
}

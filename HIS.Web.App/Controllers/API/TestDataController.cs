﻿using System.Web.Http;
using HIS.DAL.Client.Services.Tests;
using HIS.Models.Layer.Models.Tests;

namespace HIS.Web.App.Controllers.API
{
    public class TestDataController : HISBaseApiController
    {
        private ITestDataService _testDataService;

        public TestDataController(ITestDataService testDataService)
        {
            _testDataService = testDataService;
        }

        [HttpPost]
        public IHttpActionResult GetData(TestDataRequestModel models)
        {
            _testDataService.UpdateTestData(models.IsMessage, models.Value);
            return Ok();
        }

    }
}
﻿using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;

namespace HIS.Web.App.Controllers.API
{
    [Authorize]
    public class ExAuthController : Controller
    {
        [AllowAnonymous]
        [Route("Auth/ExternalLogin")]
        public ActionResult ExternalLogin(string dbalias = "nskes")
        {
            string redirectUrl = Url.Action("ExternalLoginCallback", "Auth", new {dbalias = dbalias});          
            return new ChallengeResult("Windows", redirectUrl);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        #endregion
    }

}

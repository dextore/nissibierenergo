﻿using HIS.DAL.Client.Services.Suppliers;

namespace HIS.Web.App.Controllers.API
{
    public class SuppliersController : HISBaseApiController
    {
        ISuppliersService _suppliersService;

        public SuppliersController(ISuppliersService suppliersService)
        {
            _suppliersService = suppliersService;
        }

        //[ResponseType(typeof(IEnumerable<SupplierItemModel>))]
        //public IHttpActionResult Get(string name)
        //{
        //    var suppliers = _suppliersService.Get(name);
        //    var models = suppliers.Select(n => Mapper.Map<SupplierItem, SupplierItemModel>(n));
        //    return Ok(models);
        //}

        //[ResponseType(typeof(SupplierItemModel))]
        //public IHttpActionResult GetSupplier(long supplierId)
        //{
        //    var supplier = _suppliersService.GetSupplier(supplierId);
        //    var models = Mapper.Map<SupplierItem, SupplierItemModel>(supplier);
        //    return Ok(models);
        //}
    }
}


﻿using HIS.DAL.Client.Models.Elements;
using HIS.DAL.Client.Models.RegPoints;
using HIS.DAL.Client.Services.ConPoints;
using HIS.Models.Layer.Models.Elements;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace HIS.Web.App.Controllers.API
{
    public class ConPointsController : HISBaseApiController
    {
        private IConPointService _conPointService;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="legalPersonsService">Сервис для работы с точками потребления</param>
        public ConPointsController(IConPointService conPointService)
        {
            _conPointService = conPointService;
        }

        /// <summary>
        /// Получить список всех объектов
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get()
        {
            var conPoints = _conPointService.Get();

            var model = conPoints.Select(r => Mapper.Map<Element, ElementModel>(r));

            return Ok(model);
        }

        /// <summary>
        /// Получить объект
        /// </summary>
        /// <param name="id">Идентификатор объекта</param>
        /// <returns></returns>
        public IHttpActionResult GetItem(long id)
        {
            var regPoint = _conPointService.GetItem(id);

            var model = Mapper.Map<Element, ElementModel>(regPoint);

            return Ok(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(ElementExtModel[]))]
        public IHttpActionResult GetConPoints()
        {
            var conPoints = _conPointService.GetConPoints();
            var model = conPoints.Select(m => Mapper.Map<ElementExt, ElementExtModel>(m));

            return Ok(model);
        }

        [ResponseType(typeof(ElementExtModel))]
        public IHttpActionResult ConPoint(long conPointId)
        {
            var conPoint = _conPointService.GetConPoint(conPointId);
            var model = Mapper.Map<ElementExt, ElementExtModel>(conPoint);

            return Ok(model);
        }

        /// <summary>
        /// Сохранить объект
        /// </summary>
        /// <param name="regPoint">Модель объекта</param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Save(ElementRetModel regPoint)
        {
            var model = Mapper.Map<ElementRetModel, ElementRet>(regPoint);
            var data = _conPointService.Save(model);
            var result = Mapper.Map<Element, ElementModel>(data);
            return Ok(result);
        }
    }
}

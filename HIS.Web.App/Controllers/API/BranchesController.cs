﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using HIS.DAL.Client.Models.Branches;
using HIS.DAL.Client.Services.Branches;
using HIS.Models.Layer.Models.Branches;

namespace HIS.Web.App.Controllers.API
{
    [RoutePrefix("api/Branches")]
    public class BranchesController : HISBaseApiController
    {
        private readonly IBranchService _branchService;

        public BranchesController(IBranchService branchService)
        {
            _branchService = branchService;
        }

        [HttpGet]
        public IEnumerable<BranchModel> GetBranchesTree()
        {
            return _branchService.GetBranches().Select(branch => new BranchModel
            {
                Baid = branch.Baid,
                ParentId = branch.ParentId,
                BrnNum = branch.BrnNum,
                Name = branch.Name,
                TreeLevel = branch.TreeLevel
            });
        }

        [HttpPost]
        [Route("UpdatePersonBranches/{comercialPersonId}")]
        public IEnumerable<BranchItemModel> UpdatePersonBranches(long? comercialPersonId, [FromBody]IEnumerable<BranchItemModel> branches)
        {
            var model = branches.Select(branch => Mapper.Map<BranchModel, BranchItem>(branch));

            return _branchService.UpdatePersonBranches(comercialPersonId, model)
                                 .Select(result => Mapper.Map<BranchItem, BranchItemModel>(result));
        }

        [HttpPost]
        [Route("DeletePersonBranches/{comercialPersonId}")]
        public IEnumerable<BranchItemModel> DeletePersonBranches(long? comercialPersonId, [FromBody]IEnumerable<BranchItemModel> branches)
        {
            var model = branches.Select(branch => Mapper.Map<BranchModel, BranchItem>(branch));

            return _branchService.DeletePersonBranches(comercialPersonId, model)
                                .Select(result => Mapper.Map<BranchItem, BranchItemModel>(result));
        }

        [HttpGet]
        public IEnumerable<BranchItemModel> GetPersonsBranches(long? comercialPersonId)
        {
            return _branchService.GetPersonsBranches(comercialPersonId)
                                 .Select(result => Mapper.Map<BranchItem, BranchItemModel>(result));
        }

        [HttpPost]
        [Route("CheckBranchForCorrect/{comercialPersonId}")]
        public string CheckBranchForCorrect(long? comercialPersonId, [FromBody]BranchItemModel branch)
        {
            return _branchService.CheckBranchForCorrect(comercialPersonId, Mapper.Map<BranchItemModel, BranchItem>(branch));
        }
    }
}
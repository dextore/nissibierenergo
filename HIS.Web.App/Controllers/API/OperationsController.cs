﻿using HIS.DAL.Client.Models.Operations;
using HIS.DAL.Client.Services.Operations;
using HIS.Models.Layer.Models.Operations;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.BaseAncestors;
using HIS.DAL.Client.Services.BaseAncestors;
using HIS.Models.Layer.Models.Entity;

namespace HIS.Web.App.Controllers.API
{
    /// <summary>
    /// Операции с сущностями
    /// </summary>
    [RoutePrefix("api/operations")]
    public class OperationsController : HISBaseApiController
    {
        private readonly IOperationsService _operationsService;
        private readonly IBaseAncestorsService _baseAncestorsService;

        public OperationsController(IOperationsService operationsService, 
            IBaseAncestorsService baseAncestorsService)
        {
            _operationsService = operationsService;
            _baseAncestorsService = baseAncestorsService;
        }

        /// <summary>
        /// Список разрешонных операций сущьности
        /// </summary>
        /// <param name="baId">Идентификатор сущьности</param>
        /// <returns></returns>
        [ResponseType(typeof(EntityOperationModel[]))]
        [Route("GetAllowedEntityOperations/{baId}")]
        public IHttpActionResult GetAllowedEntityOperations(long baId)
        {
            var operations = _operationsService.GetAllowedEntityOperations(baId);
            var result = operations.Select(m => Mapper.Map<EntityOperation, EntityOperationModel>(m));
            return Ok(result);
        }

        /// <summary>
        /// Список разрешонных операций сущьности
        /// </summary>
        /// <param name="baId">Идентификатор сущьности</param>
        /// <returns></returns>
        [ResponseType(typeof(StateInfoModel[]))]
        [Route("GetEntityStates/{baTypeId}")]
        public IHttpActionResult GetEntityStates(long baTypeId)
        {
            var operations = _operationsService.GetEntityStates(baTypeId);
            var result = operations.Select(m => Mapper.Map<StateInfo, StateInfoModel>(m));
            return Ok(result);
        }

        [ResponseType(typeof(EntityStateModel))]
        [Route("GetEntityState/{baId}")]
        public IHttpActionResult GetEntityState(long baId)
        {
            var state = GetEntityStateInternal(baId);
            if (state == null)
                return Ok();
            return Ok(state);
        }

        [HttpPost]
        [ResponseType(typeof(EntityStateModel))]
        //[Route("StateChange")]
        public IHttpActionResult StateChange(OperationUpdateModel model)
        {
            _baseAncestorsService.StateChange(model.BAId, model.OperationId);

            var state = GetEntityStateInternal(model.BAId);
            if (state == null)
                return Ok();
            return Ok(state);
        }

        private EntityStateModel GetEntityStateInternal(long baId)
        {
            var state = _baseAncestorsService.GetEntitytState(baId);
            if (state == null)
                return null;

            var operations = _operationsService.GetAllowedEntityOperations(baId);

            var result = Mapper.Map<EntityState, EntityStateModel>(state);
            result.AllowedOperations = operations.Select(m => Mapper.Map<EntityOperation, EntityOperationModel>(m));

            return result;
        }

    }
}
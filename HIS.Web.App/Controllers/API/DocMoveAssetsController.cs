﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.DocMoveAssets;
using HIS.Models.Layer.Models.DocMoveAssets;
using HIS.DAL.Client.Services.DocMoveAssets;


namespace HIS.Web.App.Controllers.API
{
    public class DocMoveAssetsController : HISBaseApiController
    {
        private IMapper _mapper;
        private IDocMoveAssetsService _service; 

        public DocMoveAssetsController(IDocMoveAssetsService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }
        [ResponseType(typeof(DocMoveAssetsDataModel))]
        [HttpPost]
        public IHttpActionResult GetDocMoveAssetsData(long baId)
        {
            var item = _service.GetDocMoveAssetsData(baId);
            var result = Mapper.Map<DocMoveAssetsData, DocMoveAssetsDataModel>(item);
            return Ok(result);
        }

        [ResponseType(typeof(bool))]
        [HttpPost]
        public IHttpActionResult IsHasAssets(long baId)
        {
            bool result = _service.IsHasAssets(baId);
            return Ok(result);
        }
        [ResponseType(typeof(long))]
        [HttpPost]
        public IHttpActionResult UpdateDocMoveAssets(DocMoveAssetsDataModel saveData)
        {
            var updateModel = Mapper.Map<DocMoveAssetsDataModel, DocMoveAssetsData>(saveData);
            long baId = _service.UpdateDocMoveAssets(updateModel);
            return Ok(baId);
        }

        [ResponseType(typeof(DocMoveAssetsRowModel))]
        [HttpPost]
        public IHttpActionResult GetDocMoveAssetsRow(long docMoveId, long recId)
        {
            var item = _service.GetDocMoveAssetsRow(docMoveId,recId);
            var result = Mapper.Map<DocMoveAssetsRow, DocMoveAssetsRowModel>(item);
            return Ok(result);
        }

        [ResponseType(typeof(List<DocMoveAssetsRowModel>))]
        [HttpPost]
        public IHttpActionResult GetDocMoveAssetsRows(long docMoveId)
        {
            var list = _service.GetDocMoveAssetsRows(docMoveId);
            var result = list.Select(n => _mapper.Map<DocMoveAssetsRow, DocMoveAssetsRowModel>(n));
            return Ok(result);
        }

        [ResponseType(typeof(DocMoveAssetsRowModel))]
        [HttpPost]
        public IHttpActionResult UpdateDocAcceptAssetRow(DocMoveAssetsRowModel row)
        {
            var updateModel = Mapper.Map<DocMoveAssetsRowModel, DocMoveAssetsRow>(row);
            var outModel = _service.UpdateDocMoveAssetRow(updateModel);
            var result = Mapper.Map<DocMoveAssetsRow, DocMoveAssetsRowModel>(outModel);
            return Ok(result);
        }
        [ResponseType(typeof(bool))]
        [HttpPost]
        public IHttpActionResult DeleteDocAcceptAssetRow(DocMoveAssetsRowModel row)
        {
            var deleteModel = Mapper.Map<DocMoveAssetsRowModel, DocMoveAssetsRow>(row);
            bool result = _service.DeleteDocMoveAssetRow(deleteModel);
            return Ok(result);
        }

        [ResponseType(typeof(AssetsDataModel))]
        [HttpPost]
        public IHttpActionResult GetAssetsInfo(long baId)
        {
            var item = _service.GetAssetsInfo(baId);
            var result = Mapper.Map<AssetsData, AssetsDataModel>(item);
            return Ok(result);
        }
    }


}
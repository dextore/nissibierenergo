﻿using AutoMapper;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.History;
using HIS.Models.Layer.Models.History;
using HIS.DAL.Client.Services.History;
using Newtonsoft.Json;

namespace HIS.Web.App.Controllers.API
{
    public class HistoryController : HISBaseApiController
    {
        private IMapper _mapper;
        private IHistoryService _historyService;


        public HistoryController(IHistoryService historyService, IMapper mapper)
        {
            _historyService = historyService;
            _mapper = mapper;
        }

        [ResponseType(typeof(EntityHistoryModel[]))]
        [HttpPost]
        public IHttpActionResult GetEntityStateHistory(long baId)
        {
            var list = _historyService.GetEntityStateHistory(baId);
            var result = list.Select(n => _mapper.Map<EntityHistory, EntityHistoryModel>(n));
            return Ok(result);          
        }

        [ResponseType(typeof(EntityHistoryModel[]))]
        [HttpPost]
        [HttpGet]
        public IHttpActionResult GetEntityMarkersHistory(long baId)
        {
            var list = _historyService.GetEntityMarkersHistory(baId);
            var result = list.Select(n => _mapper.Map<EntityHistory, EntityHistoryModel>(n));
            return Ok(result);
        }
        
        [HttpPost]
        public IHttpActionResult GetEntityData(long baId)
        {
            string json = _historyService.GetEntityData(baId);
            var result = JsonConvert.DeserializeObject<object>(json);
            return Ok(result);
        }
        [HttpGet]
        public IHttpActionResult GetResultQuery(long baId)
        {
            string query = _historyService.GetResultQuery(baId);
            return Ok(query);
        }

        

    }
}
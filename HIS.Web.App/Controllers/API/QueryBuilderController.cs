﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.Models.Layer.Models.QueryBuilder;

namespace HIS.Web.App.Controllers.API
{
    public class QueryBuilderController : HISBaseApiController
    {
        private IQueryBuilderService _queryBuilderService;

        public QueryBuilderController(IQueryBuilderService queryBuilderService)
        {
            _queryBuilderService = queryBuilderService;
        }

        [HttpPost]
        public IEnumerable<CaptionAndAliaseModel> GetFields(int baTypeId)
        {
            return _queryBuilderService.GetCaptionsAndAndAliases(baTypeId)
                                       .Select(Mapper.Map<CaptionAndAliase, CaptionAndAliaseModel>);
        }
        
        [HttpPost]
        public string GetData(QueryBuilderModel queryBuilderModel)
        {
            var data = _queryBuilderService.GetData(queryBuilderModel);
            return data;
        }
    }
}
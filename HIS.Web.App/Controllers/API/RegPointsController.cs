﻿using HIS.DAL.Client.Models.Elements;
using HIS.DAL.Client.Models.RegPoints;
using HIS.DAL.Client.Services.RegPoints;
using HIS.Models.Layer.Models.Elements;
//using HIS.Web.App.Models.Elements;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace HIS.Web.App.Controllers.API
{
    /// <summary>
    /// Контроллер для работы с точками учета
    /// </summary>
    public class RegPointsController : HISBaseApiController
    {
        private IRegPointsService _regPointsService;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="regPointsService">Сервис для работы с точками учета</param>
        public RegPointsController(IRegPointsService regPointsService)
        {
            _regPointsService = regPointsService;
        }

        /// <summary>
        /// Получить список всех точек учета
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get()
        {
            var regPoints = _regPointsService.Get();

            var model = regPoints.Select(r => Mapper.Map<Element, ElementModel>(r));

            return Ok(model);
        }

        /// <summary>
        /// Получить точку учетак
        /// </summary>
        /// <param name="id">идентификатор точки учета</param>
        /// <returns></returns>
        public IHttpActionResult GetItem(long id)
        {
            var regPoint = _regPointsService.GetItem(id);

            var model = Mapper.Map<Element, ElementModel>(regPoint);

            return Ok(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(ElementExtModel[]))]
        public IHttpActionResult GetRegPoints()
        {
            var regPoints = _regPointsService.GetRegPoints();
            var model = regPoints.Select(m => Mapper.Map<ElementExt, ElementExtModel>(m));

            return Ok(model);
        }

        [ResponseType(typeof(ElementExtModel))]
        public IHttpActionResult GetRegPoint(long regPointId)
        {
            var regPoint = _regPointsService.GetRegPoint(regPointId);
            var model = Mapper.Map<ElementExt, ElementExtModel>(regPoint);

            return Ok(model);
        }

        /// <summary>
        /// Сохранить точку учета
        /// </summary>
        /// <param name="regPoint">Модель с данными точки учета</param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Save(ElementRetModel regPoint)
        {
            var model = Mapper.Map<ElementRetModel, ElementRet>(regPoint);
            var data = _regPointsService.Save(model);
            var result = Mapper.Map<Element, ElementModel>(data);
            return Ok(result);
        }

    }
}

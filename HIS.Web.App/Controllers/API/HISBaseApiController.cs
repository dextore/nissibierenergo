﻿using System.Web.Http;
using AutoMapper;
using Unity.Attributes;

namespace HIS.Web.App.Controllers.API
{
    /// <summary>
    /// Базовый класс API контроллера для системы HIS
    /// </summary>
    public class HISBaseApiController : ApiController
    {
        
        /// <summary>
        /// Сопоставитель моделей
        /// </summary>
        [Dependency]
        public IMapper Mapper { get; set; }
    }
}

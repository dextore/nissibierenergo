﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.Address;
using HIS.DAL.Client.Services.Addresses;
using HIS.DAL.Client.Services.Fias;
using HIS.DAL.DB.Context;
using HIS.Models.Layer.Models;
using HIS.Models.Layer.Models.FIAS;
using HIS.Models.Layer.Models.Address;

namespace HIS.Web.App.Controllers.API
{
    /// <inheritdoc />
    /// <summary>
    /// Контроллер для для получения данных из локальных сервисов FIAS
    /// </summary>
    public class AddressController : HISBaseApiController
    {
        private readonly IAddressService _addressService;
        private readonly IComparisonToHierarchicalService _comparisonToHierarchicalService;
        public AddressController(IAddressService addressService, IComparisonToHierarchicalService comparisonToHierarchicalService)
        {
            _addressService = addressService;
            _comparisonToHierarchicalService = comparisonToHierarchicalService;
        }

        [ResponseType(typeof(IEnumerable<AddressFullName>))]
        [HttpPost]
        public async Task<IHttpActionResult> AddrSearchFull(string findStr)
        {
            var result = await _addressService.AddrSearchFull(findStr);
            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<AddressFullName>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetAddrFullByHouse(Guid AOID, Guid HOUSEID)
        {
            var result = await _addressService.GetAddrFullByHouseId(AOID, HOUSEID);
            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<AddressFullName>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetAddrFullByStead(Guid AOID, Guid STEADID)
        {
            var result = await _addressService.GetAddrFullBySteadId(AOID, STEADID);
            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<AddressFullName>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetAddrFullByAOId(Guid AOID)
        {
            var result = await _addressService.GetAddrFullByAOId(AOID);
            return Ok(result);
        }

        //[ResponseType(typeof(IEnumerable<FiasFindedAddress>))]
        //[HttpPost]
        //public async Task<IHttpActionResult> SimpleAddrSearch(string findStr)
        //{
        //    var result = await _fiasService.SimpleAddrSearch(findStr);
        //    return Ok(result);
        //}

        //[ResponseType(typeof(FiasFindedAddress))]
        //[HttpPost]
        //public async Task<IHttpActionResult> SimpleAddrByAOId(Guid AOID)
        //{
        //    var result = await _fiasService.SimpleAddrByAOId(AOID);
        //    return Ok(result);
        //}

        [ResponseType(typeof(IEnumerable<AddressAOIdAOGuid>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetAddressInfo(Guid AOID)
        {
            var result = await _addressService.GetAddressInfo(AOID);
            return Ok(result);
        }

        /// <summary>
        /// Субъекты РФ (Область/Республика/Край/АО...)
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<AddressCatalog>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetRFSubjects(RequestModel model)
        {
            var result = await _addressService.GetRFSubjects(model);
            return Ok(result);
        }

        //[ResponseType(typeof(IEnumerable<FiasDistrict>))]
        //[HttpPost]
        //public async Task<IHttpActionResult> GetDistricts(AddressRequestModel model)
        //{
        //    var result = await _fiasService.GetDistricts(model);
        //    return Ok(result);
        //}

        /// <summary>
        /// Районы (обрасти республики края...)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<AddressCatalog>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetRegions(AddressRequestModel model)
        {
            var result = await _addressService.GetRegions(model);
            return Ok(result);
        }

        /// <summary>
        /// Города
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<AddressCatalog>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetCities(AddressRequestModel model)
        {
            var result = await _addressService.GetCities(model);
            return Ok(result);
        }

        //[ResponseType(typeof(IEnumerable<FiasInRegion>))]
        //[HttpPost]
        //public async Task<IHttpActionResult> GetInRegions(Guid? AOGUID = null)
        //{
        //    var result = await _fiasService.GetInRegions(AOGUID);
        //    return Ok(result);
        //}

        /// <summary>
        /// Населенные пункты
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<AddressCatalog>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetSettlements(AddressRequestModel model)
        {
            var result = await _addressService.GetSettlements(model);
            return Ok(result);
        }

        /// <summary>
        /// Элементы структуры
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<AddressCatalog>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetElmStructures(AddressRequestModel model)
        {
            var result = await _addressService.GetElmStructures(model);
            return Ok(result);
        }

        /// <summary>
        /// Улицы
        /// </summary>
        /// <param name="AOGUID"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<AddressCatalog>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetStreets(AddressRequestModel model)
        {
            var result = await _addressService.GetStreets(model);
            return Ok(result);
        }

        /// <summary>
        /// Адресные объекты фиас
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<AddressCatalog>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetFiasAddressObjetcList(AddressRequestModel model)
        {
            var result = await _addressService.GetFiasAddressObjetcList(model);
            return Ok(result);
        }

        //[ResponseType(typeof(IEnumerable<FiasRFSubject>))]
        //[HttpPost]
        //public async Task<IHttpActionResult> GetAddTerritories(Guid? AOGUID = null)
        //{
        //    var result = await _addressService.GetAddTerritories(AOGUID);
        //    return Ok(result);
        //}

        //[ResponseType(typeof(IEnumerable<FiasAddTerritoryStreet>))]
        //[HttpPost]
        //public async Task<IHttpActionResult> GetAddTerritoryStreets(Guid? AOGUID = null)
        //{
        //    var result = await _addressService.GetAddTerritoryStreets(AOGUID);
        //    return Ok(result);
        //}

        //[ResponseType(typeof(IEnumerable<FiasAddressHouse>))]
        //[HttpPost]
        //public async Task<IHttpActionResult> GetAddressHouses(Guid? AOGUID = null)
        //{
        //    var result = await _addressService.GetAddressHouses(AOGUID);
        //    return Ok(result);
        //}

        [ResponseType(typeof(IEnumerable<AddressHouse>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetHouses(AddressRequestModel model)
        {
            var result = await _addressService.GetHouses(model);
            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<AddressStead>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetSteads(AddressRequestModel model)
        {
            var result = await _addressService.GetSteads(model);
            return Ok(result);
        }

        [ResponseType(typeof(AddressHouse))]
        [HttpPost]
        public async Task<IHttpActionResult> GetHouse(HouseRequestModel model)
        {
            var result = await _addressService.GetHouse(model.AOGuid, model.HouseId);
            return Ok(result);
        }

        [ResponseType(typeof(AddressStead))]
        [HttpPost]
        public async Task<IHttpActionResult> GetStead(SteadRequestModel model)
        {
            var result = await _addressService.GetStead(model.AOGuid, model.SteadId);
            return Ok(result);
        }

        [ResponseType(typeof(HouseUpdateModel))]
        [HttpPost]
        public IHttpActionResult GetHouseUpdateInfo(Guid houseId)
        {
            var house = _addressService.GetHouseUpdateInfo(houseId);
            var result = Mapper.Map<HouseUpdate, HouseUpdateModel>(house);
            return Ok(result);
        }

        [ResponseType(typeof(SteadUpdateModel))]
        [HttpPost]
        public IHttpActionResult GetSteadUpdateInfo(Guid steadId)
        {
            var stead = _addressService.GetSteadUpdateInfo(steadId);
            //var result = Mapper.Map<SteadUpdate, SteadUpdateModel>(stead);
            var result = new SteadUpdateModel {
                SteadId = stead.SteadId,
                SteadGuid = stead.SteadGuid,
                Number = stead.Number,
                ParentId = stead.ParentId,
                PostCode = stead.PostCode,
                CadNum = stead.CadNum,
                Latitude = stead.Latitude,
                Longitude = stead.Longitude
            };
            return Ok(result);

            //var stead = _addressService.Get

        }

        [ResponseType(typeof(AddressCatalog))]
        [HttpPost]
        public async Task<IHttpActionResult> getAddressObjectCatalog(AddressObjectRequestModel model)
        {
            //var result = await _addressService.GetAddressObject(model.AOGuid);
            var result = await _addressService.GetAddressObjectCatalog((Guid)model.AOGuid);
            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<AddressFindResult>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetAddressList(AddressResultRequestModel model)
        {
            var result = await _addressService.GetAddressList(model);
            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<FlatTypeModel>))]
        [HttpPost]
        public IHttpActionResult GetFlatTypeList()
        {
            //var result = await Task.Factory.StartNew<IEnumerable<FlatTypeModel>>(() =>
            //{
            //    var models = _addressService.GetFlatTypeList(true);
            //    return models.Select(m => Mapper.Map<FlatType, FlatTypeModel>(m)).ToArray();
            //});

            var models = _addressService.GetFlatTypeList(true);
            var result = models.Select(m => Mapper.Map<FlatType, FlatTypeModel>(m)).ToArray();

            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<AddressObjectTypeModel>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetAddressObjectTypes(int level)
        {
            var result = await _addressService.GetAddressObjectTypes(level);
            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<AddressEstateStatus>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetEstateStatuses()
        {
            var result = await _addressService.GetEstateStatuses();
            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<AddressStructureStatus>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetStructureStatuses()
        {
            var result = await _addressService.GetStructureStatuses();
            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<AddressObjectModel>))]
        [HttpPost]
        public IHttpActionResult GetAddressObject(Guid aoId)
        {
            var model = _addressService.GetAddressObject(aoId);
            var result = Mapper.Map<AddressObject, AddressObjectModel>(model);

            return Ok(result);
        }

        [ResponseType(typeof(AddressEntityModel))]
        [HttpPost]
        public IHttpActionResult GetAddressEntity(long baId)
        {
            var models = _addressService.GetAddressEntity(baId);
            var result = Mapper.Map<AddressEntity, AddressEntityModel>(models);

            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<AddressComparisonsDpModel>))]
        [HttpPost]
        public async Task<IHttpActionResult> GetAddressComparisons()
        {
            var comparisons = await _addressService.GetAddressUnsettleComparison();
            var result = _comparisonToHierarchicalService.Convert(comparisons);

            return Ok(result);
        }

        [ResponseType(typeof(AddressUpdateResultModel))]
        [HttpPost]
        public async Task<IHttpActionResult> AddressUpdate(AddressUpdateModel model)
        {
            var updateResult = await _addressService.AddressUpdate( model.BaId, 
                                                                    model.Name,
                                                                    model.AoId, 
                                                                    model.HouseId, 
                                                                    model.SteadId, 
                                                                    model.Location,
                                                                    model.FlatNum, 
                                                                    model.FlatType,
                                                                    model.POBox,
                                                                    model.IsBuild, 
                                                                    model.IsActual,
                                                                    model.SearchType);

            var result = Mapper.Map<AddressUpdateResult, AddressUpdateResultModel>(updateResult);

            return Ok(result);
        }

        [ResponseType(typeof(AddressUpdateResultModel))]
        [HttpPost]
        public IHttpActionResult AddresNameUpdate(AddressUpdateResult model)
        {
            var updateResult = _addressService.AddressNameUpdate(model.BaId, model.Name);
            var result = Mapper.Map<AddressUpdateResult, AddressUpdateResultModel>(updateResult);
            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<AddressObjectUpdateResultModel>))]
        [HttpPost]
        public async Task<IHttpActionResult> AddressObjectUpdate(AddressObjectUpdateModel model)
        {
            // валидация в фиасе 
            var internalModel = Mapper.Map<AddressObjectUpdateModel, AddressObjectUpdate>(model);
            var resultModel = await _addressService.AddressObjectUpdate(internalModel);
            var result = Mapper.Map<AddressObjectUpdateResult, AddressObjectUpdateResultModel>(resultModel);
            return Ok(result);
        }

        [ResponseType(typeof(HouseUpdateResultModel))]
        [HttpPost]
        public async Task<IHttpActionResult> HouseUpdate(HouseUpdateModel model)
        {
            var internalModel = Mapper.Map<HouseUpdateModel, HouseUpdate>(model);
            var resultModel = await _addressService.HouseUpdate(internalModel);
            var result = Mapper.Map<HouseUpdateResult, HouseUpdateResultModel>(resultModel);
            return Ok(result);
        }

        [ResponseType(typeof(SteadUpdateResultModel))]
        [HttpPost]
        public async Task<IHttpActionResult> SteadUpdate(SteadUpdateModel model)
        {
            var internalModel = Mapper.Map<SteadUpdateModel, SteadUpdate>(model);
            var resultModel = await _addressService.SteadUpdate(internalModel);
            var result = Mapper.Map<SteadUpdateResult, SteadUpdateResultModel>(resultModel);
            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<AddressHierarchyResponse>))]
        [HttpPost]
        public IHttpActionResult GetAddressHierarchy(long baId)
        {
            var addressHierarchy = _addressService.GetAddressHierarchy(baId);
            return Ok(addressHierarchy);
        }

        [ResponseType(typeof(bool))]
        [HttpPost]
        public async Task<IHttpActionResult> AddressSettle(IEnumerable<AddressSettleRequestModel> models)
        {
            var result = await _addressService.AddressSettle(models);
            return Ok(result);
        }
        [ResponseType(typeof(bool))]
        [HttpPost]
        public IHttpActionResult BindHouses(BindHouseModel model)
        {
            var result = _addressService.BindHouses(model);
            return Ok(true);
        }

        

    }
}

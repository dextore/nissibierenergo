﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.Catalogs;
using HIS.DAL.Client.Services.Catalogs;
using HIS.Models.Layer.Models.Catalogs;

namespace HIS.Web.App.Controllers.API
{
    [RoutePrefix("api/Catalogs")]
    public class CatalogsController : HISBaseApiController
    {
        readonly ICatalogsService _catalogsService;

        public CatalogsController(ICatalogsService catalogsService)
        {
            _catalogsService = catalogsService;
        }

        [ResponseType(typeof(OrganisationLegalFormModel[]))]
        [Route("GetOrganisationLegalForms")]
        public IHttpActionResult GetOrganisationLegalForms()
        {
            var items = _catalogsService.OrganisationLegalForms();
            var responce = items.Select(c => Mapper.Map<OrganisationLegalForm, OrganisationLegalFormModel>(c)).ToArray();
            return Ok(responce);
        }

        [ResponseType(typeof(PersonLegalFormModel[]))]
        [Route("GetPersonLegalForms")]
        public IHttpActionResult GetPersonLegalForms()
        {
            var items = _catalogsService.PersonLegalForms();
            var responce = items.Select(c => Mapper.Map<PersonLegalForm, PersonLegalFormModel>(c)).ToArray();
            return Ok(responce);
        }
        [ResponseType(typeof(FAMovReasonsFormModel[]))]
        [Route("GetMovReasonsForm")]
        public IHttpActionResult GetMovReasonsForm()
        {
            var items = _catalogsService.FAMovReasons();
            var responce = items.Select(c => Mapper.Map<FAMovReasonsForm, FAMovReasonsFormModel>(c)).ToArray();
            return Ok(responce);
        }
        [ResponseType(typeof(FALocationsFormModel[]))]
        [Route("GetLocations")]
        public IHttpActionResult GetLocations()
        {
            var items = _catalogsService.FALocations();
            var responce = items.Select(c => Mapper.Map<FALocationsForm, FALocationsFormModel>(c)).ToArray();
            return Ok(responce);
        }

    }
}
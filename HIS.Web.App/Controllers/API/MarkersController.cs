﻿using System.Collections.Generic;
using AutoMapper;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.Client.Services.Markers;
using HIS.Models.Layer.Models.Markers;
//using HIS.Web.App.Models.Common;
//using HIS.Web.App.Models.Markers;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace HIS.Web.App.Controllers.API
{
    [RoutePrefix("api/markers")]
    public class MarkersController : HISBaseApiController
    {
        private readonly IMarkersService _markersService;
        private readonly ICatalogMarkerValueItemsService _catalogMarkerValueItemsService;
        private readonly IMapper _mapper;

        public MarkersController(IMarkersService markersService, 
            ICatalogMarkerValueItemsService catalogMarkerValueItemsService, 
            IMapper mapper)
        {
            _markersService = markersService;
            _catalogMarkerValueItemsService = catalogMarkerValueItemsService;
            _mapper = mapper;
        }

        [ResponseType(typeof(IEnumerable<MarkerValueModel>))]
        [HttpPost]
        public IHttpActionResult GetEntityMarkerValues(MarkerValuesRequestModel model)
        {
            var values = _markersService.GetEntityMarkerValues(model.BAId, model.MarkerIds);

            var markerValues = values.Select(v => Mapper.Map<MarkerValue, MarkerValueModel>(v));

            return Ok(markerValues);

        }

        /// <summary>
        /// Занчения маркеров сущности полученные процедурой [dbo].[BA_MarkerValueSelect]
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<EntityMarkersModel>))]
        [HttpPost]
        public IHttpActionResult EntityMarkerValues(MarkerValuesRequestModel model)
        {
            var values = _markersService.EntityMarkerValues(model.BAId, model.Names.ToArray());

            var markerValues = values.Select(v => Mapper.Map<MarkerValue, MarkerValueModel>(v));

            return Ok(new EntityMarkersModel
            {
                BAId = model.BAId,
                MarkerValues = markerValues
            });
        }

        [ResponseType(typeof(IEnumerable<MarkerValueModel>))]
        [Route("CollectibleMarkerValues/{baId}/{name}")]
        [HttpPost]
        public IHttpActionResult CollectibleMarkerValues(long baId, string name)
        {
            var values = _markersService.GetCollectibleMarkerValues(baId, name);

            var data = values.Select(v => Mapper.Map<MarkerValue, MarkerValueModel>(v));

            return Ok(data);
        }

        [ResponseType(typeof(PeriodicMarkerValueModel))]
        [Route("PeriodicMarkerHistor/{baId}/{markerId}")]
        public IHttpActionResult GetPeriodicMarkerHistory(long baId, int markerId)
        {
            var values = _markersService.GetPeriodicMarkerHistory(baId, markerId);
            var data = values.Select(v => Mapper.Map<PeriodicMarkerValue, PeriodicMarkerValueModel>(v));
            return Ok(data);
        }

        public IHttpActionResult GetMarkerValueList(int markerId)
        {
            var values = _markersService.GetMarkerValueList(markerId);

            var data = values.Select(v => Mapper.Map<MarkerValueListItem, MarkerValueListItemModel>(v));

            return Ok(data);
        }

        /// <summary>
        /// Список значений перечня маркеров по идентификатору сущности.
        /// </summary>
        /// <param name="baId">идентификатор сущности</param>
        /// <returns></returns>
        [ResponseType(typeof(EntityMarkersValueListModel[]))]
        public IHttpActionResult GetEntityMarkersValue(long baId)
        {
            var values = _markersService.GetEntityMarkersValue(baId);

            var data = values.Select(v => Mapper.Map<EntityMarkersValueList, EntityMarkersValueListModel>(v)).ToArray();

            return Ok(data);
        }

        [ResponseType(typeof(EntityCaption))]
        public IHttpActionResult GetEntityCaption(long baId)
        {
            var resultModel = _markersService.GetEntityCaption(baId);
            var result = Mapper.Map<EntityCaption, EntityCaptionModel>(resultModel);
            return Ok(result);
        }

        /// <summary>
        /// Описание нереализованных маркеров сущности
        /// </summary>
        /// <param name="baTypeId"></param>
        /// <returns></returns>
        [ResponseType(typeof(MarkerInfoModel[]))]
        [Route("EntityMarkersInfo/{baTypeId}")]
        public IHttpActionResult GetEntityMarkersInfo(int baTypeId)
        {
            var resultModels = _markersService.GetEntityMarkersInfo(baTypeId);
            var result = resultModels.Select(m => Mapper.Map<MarkerInfo, MarkerInfoModel>(m));

            return Ok(result);
        }

        [ResponseType(typeof(ReferenceMarkerValueItemModel))]
        [Route("CatalogValueList/{markerId}")]
        public IHttpActionResult GetCatalogValueList(int markerId)
        {
            var resultModels = _markersService.GetCatalogMarkerValueList(markerId);
            var result = resultModels.Select(m => Mapper.Map<ReferenceMarkerValueItem, ReferenceMarkerValueItemModel>(m));
            return Ok(result);
        }

        [HttpPost]
        [ResponseType(typeof(MarkerValueModel))]
        public IHttpActionResult SaveMarkerValue(MarkerValueSetModel model)
        {
            var marker = Mapper.Map<MarkerValueSetModel, MarkerValueSet>(model);
            var resultModel = _markersService.SaveMarkerValue(marker);
            var result = Mapper.Map<MarkerValueInfo, MarkerValueModel>(resultModel);

            return Ok(result);
        }

        [HttpPost]
        [ResponseType(typeof(PeriodicMarkerValueModel))]
        public IHttpActionResult SaveMarkerHistory(MarkeHistoryChangesModel model)
        {
            var changes = Mapper.Map<MarkeHistoryChangesModel, MarkeHistoryChanges>(model);
            var current = _markersService.SaveMarkerHistory(changes);
            var result = Mapper.Map<PeriodicMarkerValue, PeriodicMarkerValueModel>(current);

            return Ok(result);
        }
        [ResponseType(typeof(List<OptionalMarkersModel>))]
        [HttpPost]
        public IHttpActionResult GetOptionalMarkers(int baTypeId, int markerId, int ItemId)
        {
            var list = _markersService.GetOptionalMarkers(baTypeId, markerId, ItemId);
            var result = list.Select(n => _mapper.Map<OptionalMarkers, OptionalMarkersModel>(n));
            return Ok(result);
        }

        [ResponseType(typeof(string))]
        [Route("GetCatalogMarkerValueItems/{markerId}")]
        public IHttpActionResult GetCatalogMarkerValueItems(int markerId)
        {
            return Ok(_catalogMarkerValueItemsService.GetValueItems(markerId));
        }

    }
}

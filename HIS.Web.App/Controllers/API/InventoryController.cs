﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.Inventory;
using HIS.Models.Layer.Models.Inventory;
using HIS.DAL.Client.Services.Inventory;


namespace HIS.Web.App.Controllers.API
{
    public class InventoryController : HISBaseApiController
    {
        private IMapper _mapper;
        private IInventoryService _inventoryService;

        public InventoryController(IInventoryService inventoryService, IMapper mapper)
        {
            _inventoryService = inventoryService;
            _mapper = mapper;
        }


        [ResponseType(typeof(InventoryDataModel))]
        [HttpPost]
        public IHttpActionResult InventoryUpdate(InventoryDataModel inventory)
        {
            var updateModel = Mapper.Map<InventoryDataModel, InventoryData>(inventory);
            var baId = _inventoryService.InventoryUpdate(updateModel);

            var outModel = _inventoryService.GetInventoryDataModel(baId);
            var resultModel = Mapper.Map<InventoryData, InventoryDataModel>(outModel);

            return Ok(outModel);
        }



        [ResponseType(typeof(List<IInventoryModel>))]
        [HttpPost]
        public IHttpActionResult GetInventories()
        {
            var list = _inventoryService.GetInventories();
            var result = list.Select(n => _mapper.Map<IInventory, IInventoryModel>(n));
            return Ok(result);
        }

        [ResponseType(typeof(List<IInventoryModel>))]
        [HttpPost]
        public IHttpActionResult GetInventoriesByCompanyId(long companyId)
        {
            var list = _inventoryService.GetInventoriesByCompanyId(companyId);
            var result = list.Select(n => _mapper.Map<IInventory, IInventoryModel>(n));
            return Ok(result);
        }


        [ResponseType(typeof(InventoryDataModel))]
        [HttpPost]
        public IHttpActionResult GetInventoryDataModel(long BAId)
        {
            var inventoryData = _inventoryService.GetInventoryDataModel(BAId);
            var resultModel = Mapper.Map<InventoryData, InventoryDataModel>(inventoryData);
            return Ok(resultModel);
        }


        [ResponseType(typeof(List<InventoryListItemModel>))]
        [HttpPost]
        public IHttpActionResult GetInventoriesList(InventoryListFiltersModel filtersModel)
        {
            var filters = Mapper.Map<InventoryListFiltersModel, InventoryListFilters>(filtersModel);
            var list = _inventoryService.GetInventoriesList(filters);
            var result = list.Select(n => _mapper.Map<InventoryListItem, InventoryListItemModel>(n));
            return Ok(result);
        }

        [ResponseType(typeof(List<IGroupsModel>))]
        [HttpPost]
        public IHttpActionResult GetInventoriesGroup()
        {
            var list = _inventoryService.GetInventoriesGroup();
            var result = list.Select(n => _mapper.Map<IGroups, IGroupsModel>(n));
            return Ok(result);
        }

        [ResponseType(typeof(List<LSCompanyAreasModel>))]
        [HttpPost]
        public IHttpActionResult GetLsCompanyAreas()
        {
            var list = _inventoryService.GetLSCompanyAreas();
            var result = list.Select(n => _mapper.Map<LSCompanyAreas, LSCompanyAreasModel>(n));
            return Ok(result);
        }
       
        [ResponseType(typeof(List<RefSysUnitsModel>))]
        [HttpPost]
        public IHttpActionResult GetRefSysUnits()
        {
            var list = _inventoryService.GetRefSysUnits();
            var result = list.Select(n => _mapper.Map<RefSysUnits, RefSysUnitsModel>(n));
            return Ok(result);
        }
      

    // GET: Inventory


}
}
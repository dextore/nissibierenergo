﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.Common;
using HIS.DAL.Client.Models.SystemTree;
using HIS.DAL.Client.Services.SystemTree;
using HIS.Models.Layer.Models.Elements;
using HIS.Models.Layer.Models.SystemTree;
using HIS.Web.App.Models;

namespace HIS.Web.App.Controllers.API
{
    public class SystemTreeController : HISBaseApiController
    {
        private readonly ISystemTreeService _systemTreeService;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="systemTreeService">Сервис работы с системным деревом</param>
        public SystemTreeController(ISystemTreeService systemTreeService)
        {
            _systemTreeService = systemTreeService;
        }

        /// <summary>
        /// Получение коллекции элементов дерева
        /// </summary>
        /// <param name="treeName">Наименование типа дерева</param>
        /// <param name="entityId">Идентификатор сущности (если NULL, то возвращать список сущностей корневого уровня)</param>
        /// <param name="entityParentId">Идентификатор родительской сущности</param>
        /// <param name="parentBaId">Родительский идентификатор СУЩНОСТИ для текущего элемента дерева</param>
        /// <param name="isParent">Признак родительского элемента (т.е. разворот дерева необходимо сформировать "вверх" от заданного элемента до корневого элемента)</param>
        /// <returns></returns>
        public IHttpActionResult GetTreeEntities(string treeName, long? entityId, long? entityParentId, long? parentBaId, bool? isParent)
        {
            EntityInfoTreeCollection tree = _systemTreeService.GetTreeEntities(treeName, entityId, entityParentId, parentBaId, isParent);
            var model = Mapper.Map<EntityInfoTreeCollection, EntityInfoTreeCollectionModel>(tree);
            return Ok(model);
        }

        [ResponseType(typeof(SystemTreeItem))]
        public IHttpActionResult GetTreeItem(long baId)
        {
            var model = _systemTreeService.GetTreeItem(baId);
            var result= Mapper.Map<SystemTreeItem, SystemTreeItemModel>(model);
            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<SystemTreeItem>))]
        public IHttpActionResult GetTreeItemChildren(long baId)
        {
            var children = _systemTreeService.GetTreeItemChildren(baId);
            var result = children.Select(m => Mapper.Map<SystemTreeItem, SystemTreeItemModel>(m));
            return Ok(result);
        }

    }
}

﻿using System.Web.Mvc;
using HIS.Web.App.CustomAttributes;

namespace HIS.Web.App.Controllers.API
{
    /// <summary>
    /// Контроллер по работе с аккаунтами пользователей (добавление/изменение и т.п.)
    /// </summary>
    /// [HISAuth(Rights = "test1,test2")]
    public class AccountController : HISBaseApiController
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public AccountController()
        {
            
        }

        [ApiAuth(Roles = "Role1,Role2", Rights = "test1,test2", Users = "User1,User2")]        
        public ActionResult GetTestRights()
        {
            return null;
        }

        public ActionResult GetTestRights2()
        {
            return null;
        }
    }
}

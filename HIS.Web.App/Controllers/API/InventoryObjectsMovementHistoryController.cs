﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using HIS.DAL.Client.Models.InventoryObjectsMovementHistory;
using HIS.DAL.Client.Services.InventoryObjectsMovementHistory;
using HIS.Models.Layer.Models.InventoryObjectsMovementHistory;

namespace HIS.Web.App.Controllers.API
{
    public class InventoryObjectsMovementHistoryController : HISBaseApiController
    {
        private IInventoryObjectsMovementHistoryService _service;

        public InventoryObjectsMovementHistoryController(IInventoryObjectsMovementHistoryService service)
        {
            _service = service;
        }

        [HttpGet]
        public IEnumerable<InventoryAssetsModel> GetFaAssetesList()
        {
            return _service.GetFaAssetesList().Select(x => Mapper.Map<InventoryAssets, InventoryAssetsModel>(x));
        }

        [HttpGet]
        public IEnumerable<InventoryAssetsMovementHistoryModel> GetFaAssetesMovementHistory(long inventoryAssetId)
        {
            return _service.GetInventoryAssetsMovementHistory(inventoryAssetId)
                           .Select(x => Mapper.Map<InventoryAssetsMovementHistory, InventoryAssetsMovementHistoryModel>(x));
        }
    }
}
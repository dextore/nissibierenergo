﻿using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.SystemSearch;
using HIS.DAL.Client.Services.UniversalSearch;
using HIS.Models.Layer.Models.UniversalSearch;

namespace HIS.Web.App.Controllers.API
{
    public class UniversalSearchController : HISBaseApiController
    {
        private readonly IUniversalSearchService _universalSearchService;

        public UniversalSearchController(IUniversalSearchService universalSearchService)
        {
            _universalSearchService = universalSearchService;
        }

        [ResponseType(typeof(SearchResultModel))]
        [HttpPost]
        public IHttpActionResult SearchByName(UniversalSearchRequestModel requestModel)
        {
            var result = _universalSearchService.SearchByName(requestModel.From, 
                requestModel.To, 
                requestModel.Value, 
                requestModel.ShowDeleted,
                requestModel.BATypeId);
            var model = Mapper.Map<SearchResult, SearchResultModel>(result);
            return Ok(model);
        }

        [ResponseType(typeof(SearchResultModel))]
        [HttpPost]
        public IHttpActionResult SearchByInn(UniversalSearchRequestModel requestModel)
        {
            var result = _universalSearchService.SearchByInn(requestModel.From, 
                requestModel.To, 
                requestModel.Value, 
                requestModel.ShowDeleted,
                requestModel.BATypeId);
            var model = Mapper.Map<SearchResult, SearchResultModel>(result);
            return Ok(model);
        }

    }
}

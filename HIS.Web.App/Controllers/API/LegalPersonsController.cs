﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using HIS.DAL.Client.Services.LegalPersons;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.LegalSubjects;
using HIS.DAL.Client.Models.LegalSubjects.Persons;
using HIS.DAL.Client.Models.Markers;
using HIS.Models.Layer.Models.LegalPersons;
using HIS.Models.Layer.Models.LegalSubjects;
using HIS.Models.Layer.Models.Markers;

namespace HIS.Web.App.Controllers.API
{
    /// <summary>
    /// Контроллер для работы с юридическими лицами
    /// </summary>
    [RoutePrefix("api/LegalPersons")]
    public class LegalPersonsController : HISBaseApiController
    {
        private readonly ILegalPersonsService _legalPersonsService;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="legalPersonsService">Сервис работы с юридическими лицами</param>
        public LegalPersonsController(ILegalPersonsService legalPersonsService)
        {
            _legalPersonsService = legalPersonsService;
        }

        [ResponseType(typeof(LegalPersonModel))]
        [Route("{legalPersonId}")]
        [HttpPost]
        public IHttpActionResult GetLegalPerson(long legalPersonId, [FromBody]IEnumerable<int> markersIds)
        {
            var legalPerson = _legalPersonsService.GetLegalPerson(legalPersonId, markersIds);

            var data = Mapper.Map<LegalPerson, LegalPersonModel>(legalPerson);

            return Ok(data);
        }

        [ResponseType(typeof(LegalPersonModel))]
        [Route("GetParentLegalPerson/{affiliateId}")]
        public IHttpActionResult GetParentLegalPerson(long affiliateId)
        {
            var legalPerson = _legalPersonsService.GetParentLegalPerson(affiliateId);
            var data = Mapper.Map<LegalPerson, LegalPersonModel>(legalPerson);
            return Ok(data);
        }

        [HttpPost]
        [Route("Save")]
        public IHttpActionResult Save([FromBody]LegalPersonModel legalPerson)
        {
            var model = Mapper.Map<LegalPersonModel, LegalPerson>(legalPerson);

            var modelRet = _legalPersonsService.Save(model);
            var result = Mapper.Map<LegalPersonImplemented, LegalPersonImplementedModel>(modelRet);

            return Ok(result);
        }


        [HttpPost]
        [Route("UpdatePersonInformation/{legalPersonId}")]
        public IHttpActionResult UpdatePersonInformation(long legalPersonId, [FromBody]IEnumerable<MarkerValueModel> markers)
        {
            var model = markers.Select(x => Mapper.Map<MarkerValueModel, MarkerValue>(x));

            var modelRet = _legalPersonsService.UpdatePersonInformation(legalPersonId, model);
            modelRet = _legalPersonsService.GetPersonInformation(legalPersonId, markers.Select(x => (long) x.MarkerId));

            var result = Mapper.Map<PersonInfoResponse, PersonInfoResponseModel>(modelRet);

            return Ok(result);
        }

        [HttpPost]
        [Route("GetPersonInformation/{legalPersonId}")]
        public IHttpActionResult GetPersonInformation(long legalPersonId, [FromBody]IEnumerable<long> markersIds)
        {
            var legalPersonInformation = _legalPersonsService.GetPersonInformation(legalPersonId, markersIds);
            
            var result = Mapper.Map<PersonInfoResponse, PersonInfoResponseModel>(legalPersonInformation);
            
            return Ok(result);
        }

    }
}

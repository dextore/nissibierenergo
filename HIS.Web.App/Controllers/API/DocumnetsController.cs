﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.Documents;
using HIS.DAL.Client.Services.Documents;
using HIS.Models.Layer.Models.Documents;

namespace HIS.Web.App.Controllers.API
{
    [RoutePrefix("api/Documents")]
    public class DocumnetsController : HISBaseApiController
    {
        private readonly IDocumentsService _documentsService;

        public DocumnetsController(IDocumentsService documentsService)
        {
            _documentsService = documentsService;
        }

        [ResponseType(typeof(IEnumerable<DocumentTreeItemModel>))]
        [Route("GetTreeItem")]
        public IHttpActionResult GetTreeItem()
        {
            var items = _documentsService.GetTreeItem();
            if (items == null)
                return Ok();

            var result = items.Select(m => Mapper.Map<DocumentTreeItem, DocumentTreeItemModel>(m));
            return Ok(result);
        }

        [ResponseType(typeof(IEnumerable<DocumentTreeItemModel>))]
        [Route("GetFilteredTreeItems/{companyAreaId}/{startDate}/{endDate}")]
        public IHttpActionResult GetFilteredTreeItems(long companyAreaId, DateTime startDate, DateTime endDate)
        {
            var items = _documentsService.GetFilteredTreeItems(companyAreaId, startDate, endDate);
            if (items == null)
                return Ok();

            var result = items.Select(m => Mapper.Map<DocumentTreeItem, DocumentTreeItemModel>(m));
            return Ok(result);
        }

    }
}

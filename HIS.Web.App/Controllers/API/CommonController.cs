﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.Common;
using HIS.DAL.Client.Services.Common;
using HIS.Models.Layer.Models.Common;
using HIS.Models.Layer.Models.Entity;

namespace HIS.Web.App.Controllers.API
{
    /// <summary>
    /// Контроллер основного функционала системы
    /// </summary>
    public class CommonController : HISBaseApiController
    {
        private readonly ICommonService _commonService;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="constantsService">Сервис работы с константами БД</param>
        /// <param name="commonService">Общий функционал</param>
        public CommonController(ICommonService commonService)
        {
            _commonService = commonService;
        }

        /// <summary>
        /// Получение информации о сущности
        /// </summary>
        /// <param name="entityId">ИД сущности</param>
        /// <returns></returns>
        public IHttpActionResult GetEntityInfo(long entityId)
        {
            EntityInfoItem entityItem = _commonService.GetEntityInfo(entityId);
            var model = Mapper.Map<EntityInfoItem, EntityInfoItemModel>(entityItem);
            return Ok(model);
        }

        [ResponseType(typeof(EntityInfoModel[]))]
        public IHttpActionResult GetEntitiesInfo()
        {
            IEnumerable<EntityInfo> entities = _commonService.GetEntitiesInfo();
            var responceModel = entities.Select(m => Mapper.Map<EntityInfo, EntityInfoModel>(m));
            return Ok(responceModel);
        }
        [ResponseType(typeof(List<string>))]
        [HttpPost]
        public IHttpActionResult CheckErrors(long baId)
        {
            var errors = _commonService.CheckErrors(baId);
            return Ok(errors);

        }
        [ResponseType(typeof(List<EntityViewItemModel>))]
        [HttpPost]
        public IHttpActionResult GetEntityViewsItems(int baTypeId)
        {
           var list = _commonService.GetEntityViewsItems(baTypeId);
           var result = list.Select(m => Mapper.Map<EntityViewItem, EntityViewItemModel>(m));
           return Ok(result);

        }

       


    }
}

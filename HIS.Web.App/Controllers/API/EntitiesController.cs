﻿using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.Client.Services.Entities;
using HIS.Models.Layer.Models.Markers;

namespace HIS.Web.App.Controllers.API
{
    [RoutePrefix("api/entities")]
    public class EntitiesController : HISBaseApiController
    {
        private IEntitiesService _entitiesService;

        public EntitiesController(IEntitiesService _entitiesService)
        {
            this._entitiesService = _entitiesService;
        }

        [ResponseType(typeof(long))]
        [HttpPost]
        public IHttpActionResult Update(EntityUpdateModel model)
        {
            var updateModel = Mapper.Map<EntityUpdateModel, EntityUpdate>(model);
            var baId = _entitiesService.Update(updateModel);
            return Ok(baId);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using HIS.DAL.Client.Models.AdminPanel;
using HIS.DAL.Client.Services.AdminPanel;
using HIS.Models.Layer.Models.AdminPanel;

namespace HIS.Web.App.Controllers.API
{
    public class AdminPanelController : HISBaseApiController
    {
        private readonly IAdminService _service;

        public AdminPanelController(IAdminService service)
        {
            _service = service;
        }

        public IEnumerable<BaTypesModel> GetEntities()
        {
            var model = _service.GetEntities();
            return model.Select(x => Mapper.Map<BaTypesView, BaTypesModel>(x));
        }

        [HttpPost]
        public BaTypesModel CreateUpdateEntitiType(BaTypesModel createdEntitiType)
        {
            var view = Mapper.Map<BaTypesModel, BaTypesView>(createdEntitiType);
            var model = _service.CreateUpdateEntitiType(view);

            return Mapper.Map<BaTypesView, BaTypesModel>(model);
        }

        public IEnumerable<ItemTableViewModel> GetAvailableValuesForList(int? markerId)
        {
            var model = _service.GetAvailableValuesForList(markerId);
            return model.Select(x => Mapper.Map<ItemTableView, ItemTableViewModel>(x));
        }
        public IEnumerable<ItemTableViewModel> GetAvailableValuesForReference(int? markerId)
        {
            var model = _service.GetAvailableValuesForReference(markerId);
            return model.Select(x => Mapper.Map<ItemTableView, ItemTableViewModel>(x));
        }

        public IEnumerable<MarkersAdminModel> GetMarkers(int? batypeId)
        {
            var model = _service.GetMarkers(batypeId);
            return model.Select(x => Mapper.Map<MarkersAdminView, MarkersAdminModel>(x));
        }
        public IEnumerable<OnlyMarkersDataModel> GetMarkers()
        {
            var model = _service.GetMarkers();
            return model.Select(x => Mapper.Map<OnlyMarkersDataView, OnlyMarkersDataModel>(x));
        }

        public IEnumerable<BaTypesStatesModel> GetAvailableSteteForEntiti(int? baTypeId)
        {
            var model = _service.GetAvailableSteteForEntiti(baTypeId);
            return model.Select(x => Mapper.Map<BaTypesStatesView, BaTypesStatesModel>(x));
        }

        public IEnumerable<BaTypesOperationsModel> GetAvailableOperationsForEntiti(int? baTypeId)
        {
            var model = _service.GetAvailableOperationsForEntiti(baTypeId);
            return model.Select(x => Mapper.Map<BaTypesOperationsView, BaTypesOperationsModel>(x));
        }

        public IEnumerable<BaTypesOperationsStateModel> GetAvailablOperationsStateForEntiti(int? baTypeId)
        {
            var model = _service.GetAvailablOperationsStateForEntiti(baTypeId);
            return model.Select(x => Mapper.Map<BaTypesOperationsStateView, BaTypesOperationsStateModel>(x));
        }

        [HttpPost]
        public OnlyMarkersDataModel CreateUpdateMarker(OnlyMarkersDataModel marker)
        {
            var view = Mapper.Map<OnlyMarkersDataModel, OnlyMarkersDataView>(marker);
            var model = _service.CreateUpdateMarker(view);

            return Mapper.Map<OnlyMarkersDataView, OnlyMarkersDataModel>(model);
        }

        [HttpPost]
        public bool DeleteMarkerFromBaType([FromUri]int baId, [FromBody]MarkersAdminModel marker)
        {
            var view = Mapper.Map<MarkersAdminModel, MarkersAdminView>(marker);
            var model = _service.DeleteMarkerFromBaType(baId, view);

            return model;
        }

        [HttpPost]
        public MarkersAdminModel AddMarkerToBaType([FromUri]int baId, [FromBody]MarkersAdminModel marker)
        {
            var view = Mapper.Map<MarkersAdminModel, MarkersAdminView>(marker);
            var model = _service.AddMarkerToBaType(baId, view);

            return Mapper.Map<MarkersAdminView, MarkersAdminModel>(model);
        }
        
        [HttpPost]
        public BaTypesStatesModel AddStateToBaType([FromUri]int baId, [FromBody]BaTypesStatesModel state)
        {
            var view = Mapper.Map<BaTypesStatesModel, BaTypesStatesView>(state);
            var model = _service.AddStateToBaType(baId, view);

            return Mapper.Map<BaTypesStatesView, BaTypesStatesModel>(model);
        }

        [HttpPost]
        public bool DeleteStateFromBaType([FromUri]int baId, [FromUri]int stateId)
        {
            return _service.DeleteStateFromBaType(baId, stateId);
        }

        [HttpPost]
        public bool DeleteMarker([FromUri]int markerId)
        {
            return _service.DeleteMarker(markerId);
        }

        [HttpGet]
        public bool CanEditLinkOnEntityMarker(int markerId)
        {
            return _service.CanEditLinkOnEntityMarker(markerId);
        }

        public IEnumerable<BaTypesStatesCreateModel> GetAllStates()
        {
            return _service.GetAllStates().Select(x => Mapper.Map<BaTypesStatesCreateView, BaTypesStatesCreateModel>(x));
        }

        [HttpPost]
        public BaTypesStatesCreateModel CreateUpdateState([FromBody]BaTypesStatesCreateModel state)
        {
            var view = Mapper.Map<BaTypesStatesCreateModel, BaTypesStatesCreateView>(state);
            var model = _service.CreateUpdateState(view);

            return Mapper.Map<BaTypesStatesCreateView, BaTypesStatesCreateModel>(model);
        }

        [HttpPost]
        public bool DeleteState([FromUri]int stateId)
        {
            return _service.DeleteState(stateId);
        }

        public IEnumerable<BaTypesOperationsCreateModel> GetAllOperations()
        {
            return _service.GetAllOperations().Select(x => Mapper.Map<BaTypesOperationsCreateView, BaTypesOperationsCreateModel>(x));
        }

        [HttpPost]
        public BaTypesOperationsCreateModel CreateUpdateOperations(BaTypesOperationsCreateModel operations)
        {
            var view = Mapper.Map<BaTypesOperationsCreateModel, BaTypesOperationsCreateView>(operations);
            var model = _service.CreateUpdateOperations(view);

            return Mapper.Map<BaTypesOperationsCreateView, BaTypesOperationsCreateModel>(model);
        }

        [HttpPost]
        public bool DeleteOperations([FromUri]int operationsId)
        {
            return _service.DeleteOperations(operationsId);
        }

        [HttpPost]
        public ItemTableViewModel CreateUpdateListItem([FromUri]int markerId, [FromBody]ItemTableViewModel itemOfList)
        {
            var view = Mapper.Map<ItemTableViewModel, ItemTableView>(itemOfList);
            var model = _service.CreateUpdateListItem(markerId, view);

            return Mapper.Map<ItemTableView, ItemTableViewModel>(model);
        }

        [HttpPost]
        public bool DeleteListItem([FromUri]int markerId, [FromUri]int listItemId)
        {
            return _service.DeleteListItem(markerId, listItemId);
        }

        [HttpPost]
        public BaTypesOperationsStateModel AddOperationStateToBaType([FromUri] int baId, [FromBody] BaTypesOperationsStateCreateModel operationState)
        {
            var view = Mapper.Map<BaTypesOperationsStateCreateModel, BaTypesOperationsStateCreateView>(operationState); 
            var result = _service.AddOperationStateToBaType(baId, view);

            return Mapper.Map<BaTypesOperationsStateView, BaTypesOperationsStateModel>(result);
        }

        [HttpPost]
        public bool DeleteOperationStateToBaType([FromUri]int baId, [FromUri]int operationId, [FromUri]int srcStateId)
        {
            return _service.DeleteOperationStateToBaType(baId, operationId, srcStateId);
        }

        [HttpPost]
        public BaTypesOperationsModel AddOperationToBaType([FromUri]int baId, [FromBody]BaTypesOperationsModel operation)
        {
            var view = Mapper.Map<BaTypesOperationsModel, BaTypesOperationsView>(operation);
            var result = _service.AddOperationToBaType(baId, view);

            return Mapper.Map<BaTypesOperationsView, BaTypesOperationsModel>(result);
        }

        [HttpPost]
        public bool DeleteOperationFromBaType([FromUri]int baId, [FromUri]int operationId)
        {
            return _service.DeleteOperationFromBaType(baId, operationId);
        }
    }
}

﻿using HIS.DAL.Client.Models.Elements;
using HIS.DAL.Client.Models.RegPoints;
using HIS.DAL.Client.Services.Objects;
using HIS.Models.Layer.Models.Elements;
//using HIS.Web.App.Models.Elements;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace HIS.Web.App.Controllers.API
{
    public class ObjectsController : HISBaseApiController
    {
        IObjectsService _objectsService;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="legalPersonsService">Сервис для работы с объектами</param>
        public ObjectsController(IObjectsService objectsService)
        {
            _objectsService = objectsService;
        }

        /// <summary>
        /// Получить список всех объектов
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get()
        {
            var regPoints = _objectsService.Get();

            var model = regPoints.Select(r => Mapper.Map<Element, ElementModel>(r));

            return Ok(model);
        }

        /// <summary>
        /// Получить объект
        /// </summary>
        /// <param name="id">Идентификатор объекта</param>
        /// <returns></returns>
        public IHttpActionResult GetItem(long id)
        {
            var obj = _objectsService.GetItem(id);

            var model = Mapper.Map<Element, ElementModel>(obj);

            return Ok(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(ElementExtModel[]))]
        public IHttpActionResult GetObjects()
        {
            var objects = _objectsService.GetObjects();
            var model = objects.Select(m => Mapper.Map<ElementExt, ElementExtModel>(m));

            return Ok(model);
        }

        [ResponseType(typeof(ElementExtModel))]
        public IHttpActionResult GetObject(long objectId)
        {
            var obj = _objectsService.GetObject(objectId);
            var model = Mapper.Map<ElementExt, ElementExtModel>(obj);

            return Ok(model);
        }

        /// <summary>
        /// Сохранить объект
        /// </summary>
        /// <param name="regPoint">Модель объекта</param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Save(ElementRetModel regPoint)
        {
            var model = Mapper.Map<ElementRetModel, ElementRet>(regPoint);
            var data = _objectsService.Save(model);
            var result = Mapper.Map<Element, ElementModel>(data);
            return Ok(result);
        }

    }
}

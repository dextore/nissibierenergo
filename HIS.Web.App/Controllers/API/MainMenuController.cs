﻿using System.Web.Http;
using HIS.DAL.Client.Services.Common;
using HIS.Web.App.Models.Builders;

namespace HIS.Web.App.Controllers.API
{
    /// <summary>
    /// Контроллер по работе с главным меню
    /// </summary>
    public class MainMenuController : HISBaseApiController
    {
        private readonly IMainMenuService _mainMenuService;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="mainMenuService">Основнон меню системы</param>
        public MainMenuController(IMainMenuService mainMenuService)
        {
            _mainMenuService = mainMenuService;
        }
        
        /// <summary>
        /// Основное меню
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult GetMainMenu()
        {            
            //IEnumerable<MenuItem> menuItems = _mainMenuService.GetMenu();

            var menu = MainMenuBuilder.GenerateMainMenu();
            return Ok(menu);
        }
    }
}

﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using HIS.DAL.Client.Models.States;
using HIS.DAL.Client.Services.States;
using HIS.Models.Layer.Models.States;

namespace HIS.Web.App.Controllers.API
{
    [RoutePrefix("api/entitystates")]
    public class EntityStatesController : HISBaseApiController
    {
        private readonly IEntityStatesService _entityStatesService;

        public EntityStatesController(IEntityStatesService statesService)
        {
            _entityStatesService = statesService;
        }

        [ResponseType(typeof(EntityTypeStateModel))]
        [Route("getStates/{baTypeId}")]
        public IHttpActionResult GetEntityTypeState(int baTypeId)
        {
            var states = _entityStatesService.GetEntityTypeStates(baTypeId);
            var result = states.Select(m => Mapper.Map<EntityTypeState, EntityTypeStateModel>(m));
            return Ok(result);
        }

    }

}

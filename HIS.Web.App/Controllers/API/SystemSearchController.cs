﻿using System.Web.Http;
using HIS.DAL.Client.Models.Common;
using HIS.DAL.Client.Services.SystemSearch;
using HIS.Models.Layer.Models.Entity;

namespace HIS.Web.App.Controllers.API
{
    public class SystemSearchController : HISBaseApiController
    {
        private readonly ISystemSearchService _searchService = null;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="searchService">Инерфейс системного поиска</param>
        public SystemSearchController(ISystemSearchService searchService)
        {
            _searchService = searchService;
        }

        public IHttpActionResult GetSearchEntities(SystemSearchTypes searchType, string searchText)
        {
            EntityInfoCollection searchCollection = _searchService.GetSearchEntities(searchType, searchText);
            var model = Mapper.Map<EntityInfoCollection, EntityInfoCollectionModel>(searchCollection);
            return Ok(model);            
        }

        public IHttpActionResult GetFullTextSearchEntities(string searchText)
        {
            FullTextSearchEntityInfoItemCollection searchCollection  = _searchService.GetFullTextSearchEntities(searchText);
            var model = Mapper.Map<FullTextSearchEntityInfoItemCollection, FullTextSearchEntityInfoCollectionModel>(searchCollection);
            return Ok(model);
        }
    }
}
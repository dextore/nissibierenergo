using System;
using System.Security.Principal;
using System.Web.Mvc;
using HIS.Web.App.CustomAttributes;

namespace HIS.Web.App.Controllers
{
    public class HomeController : Controller {
        public ActionResult Index()
        {                   
            IIdentity identity = User.Identity;
            bool isAuth = identity.IsAuthenticated;

            return View();
        }


        public ActionResult Test()
        {
            return View();
        }

        /// <summary>
        /// �������� �������� ��������� ������
        /// </summary>
        /// <param name="moduleName">��� ������</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetModuleView(string moduleName)
        {
            if (string.IsNullOrEmpty(moduleName)) 
                throw new Exception("������ ��� �������� �� ������.");

            if (moduleName.Equals("MainForm"))
            {
                return PartialView("MainForm");
            }

            if (moduleName.Equals("LoginForm"))
            {
                return PartialView("LoginForm");
            }

            return PartialView(string.Format("~/Views/Modules/{0}/{0}.cshtml", moduleName));
        }

        [MvcAuth(Roles = "Role1,Role2,testrole")]
        //[MvcAuth(Roles = "Role1,Role2,testrole", Rights = "test1,test2", Users = "User1,User2, SIBIRENERGO\\KachevIV")]
        //[MvcAuth(Rights = "test11,test222")]
        public ActionResult GetTestPage()
        {
            return null;
        }       
    }
}
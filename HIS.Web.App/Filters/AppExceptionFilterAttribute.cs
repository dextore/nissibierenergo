﻿using System;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http.Filters;
using HIS.Business.Layer.Utils.QueryBuilder.Exceptions;
using HIS.DAL.Exceptions;

namespace HIS.Web.App.Filters
{
    public class AppExceptionFilterAttribute: ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnException(actionExecutedContext);

            var notImplementedException = actionExecutedContext.Exception as NotImplementedException;
            if (notImplementedException != null)
            {
                actionExecutedContext.Response = new HttpResponseMessage(HttpStatusCode.NotImplemented);
            }

            var dataValidationException = actionExecutedContext.Exception as DataValidationException;
            if (dataValidationException != null)
            {
                var sb = new StringBuilder();
                sb.Append("{");
                sb.Append($"\"Message\": \"Ошибка проверки данных.\",");
                sb.Append($"\"ExceptionMessage\": \"{HttpUtility.JavaScriptStringEncode(actionExecutedContext.Exception.Message)}\",");
                sb.Append($"\"ExceptionType\": \"{actionExecutedContext.Exception.GetType()}\"");
                //sb.Append(",");


                sb.Append("}");
                actionExecutedContext.Response = new HttpResponseMessage()
                {
                    Content = new StringContent(sb.ToString(), Encoding.UTF8),
                    StatusCode = HttpStatusCode.InternalServerError
                };
                return;
            }

            var debugException = actionExecutedContext.Exception as DebugException; 
            if (debugException != null)
            {
                var sb = new StringBuilder();
                sb.Append("{");
                sb.Append($"\"Message\": \"QueryBuilder не болей .\",");
                sb.Append($"\"ExceptionMessage\": \"{HttpUtility.JavaScriptStringEncode(actionExecutedContext.Exception.Message)}\",");
                sb.Append($"\"ExceptionType\": \"{actionExecutedContext.Exception.GetType()}\"");
                //sb.Append(",");


                sb.Append("}");
                actionExecutedContext.Response = new HttpResponseMessage()
                {
                    Content = new StringContent(sb.ToString(), Encoding.UTF8),
                    StatusCode = HttpStatusCode.InternalServerError
                };
                return;
            }

            var sqlException = actionExecutedContext.Exception as SqlException;
            if (sqlException != null)
            {
                var sb = new StringBuilder();
                sb.Append("{");
                sb.Append($"\"Message\": \"An error has occurred.\",");
                sb.Append($"\"ExceptionMessage\": \"{HttpUtility.JavaScriptStringEncode(actionExecutedContext.Exception.Message)}\",");
                sb.Append($"\"ExceptionType\": \"{actionExecutedContext.Exception.GetType()}\"");
                sb.Append(",");
                sb.Append($"\"Class\": \"{sqlException.Class}\",");
                sb.Append($"\"Number\": \"{sqlException.Number}\",");
                sb.Append($"\"State\": \"{sqlException.State}\"");

                sb.Append("}");
                actionExecutedContext.Response = new HttpResponseMessage()
                {
                    Content = new StringContent(sb.ToString(), Encoding.UTF8),
                    StatusCode = HttpStatusCode.InternalServerError
                };
            }

            
            //if (actionExecutedContext.Exception is SqlException sqlException)
            //{
            //    var sb = new StringBuilder();
            //    sb.Append("{");
            //    sb.Append($@"""class"": ""{sqlException.Class}"",");
            //    sb.Append($@"""number"": ""{sqlException.Number}"",");
            //    sb.Append($@"""state"": ""{sqlException.State}"",");
            //    sb.Append($@"""message"": ""{sqlException.Message}""");
            //    sb.Append("}");
            //    actionExecutedContext.Response = new HttpResponseMessage()
            //    {
            //        Content = new StringContent(sb.ToString())
            //    };
            //}
        }
    }
}
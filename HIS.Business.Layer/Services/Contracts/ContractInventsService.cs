﻿using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.Contracts;
using HIS.DAL.Client.Services.Contracts;
using HIS.DAL.DB.Context;
using HIS.DAL.DB.Handlers.Contracts;
using Unity;

namespace HIS.Business.Layer.Services.Contracts
{
    public class ContractInventsService : ServiceBase, IContractInventsService
    {
        public ContractInventsService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<ContractInvent> GetInvents(long contractId, IEnumerable<int> states)
        {
            return Exec(p =>
            {
                return p.GetHandler<IContractInventsHandler>().Get()
                    .Where(m => m.ContractId == contractId && states.Contains(m.SOStates.StateId))
                    .OrderBy(m => m.OrderNum)
                    .Select(ConvertToContractEvent).ToArray();
            });
        }

        public ContractInvent GetInventItem(long baId)
        {
            return Exec(p =>
            {
                return p.GetHandler<IContractInventsHandler>().Get().Where(m => m.BAId == baId).Select(
                    ConvertToContractEvent
                ).FirstOrDefault();
            });
        }

        private ContractInvent ConvertToContractEvent(EFCAContractInvent contractInvent)
        {
            return new ContractInvent
            {
                OrderNum = contractInvent.OrderNum,
                BAId = contractInvent.BAId,
                ContractId = contractInvent.ContractId,
                InventId = contractInvent.InvId,
                StateId = contractInvent.StateId,
                StateName = contractInvent.SOStates.Name,
                StartDate = contractInvent.StartDate,
                EndDate = contractInvent.EndDate,
                InventName = contractInvent.IInventory.Name,
                UserId = contractInvent.CreatorId
            };
        }
    }
}


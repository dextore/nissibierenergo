﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Contracts;
using HIS.DAL.Client.Services.Contracts;
using HIS.DAL.DB.Handlers.Contracts;
using Unity;

namespace HIS.Business.Layer.Services.Contracts
{
    public class LSContractsService : ServiceBase, ILSContractsService
    {
        public LSContractsService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<LSContract> ByLegalSubject(long lsId)
        {
            return Exec(p => p.GetHandler<ILSContractsHandler>().ByLegalSubject(lsId));
        }
    }
}

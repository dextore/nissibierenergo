﻿using HIS.DAL.Client.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Services.Contracts;
using HIS.DAL.DB.Handlers.Contracts;
using System.Data.Entity;
using HIS.DAL.DB;
using Unity;

namespace HIS.Business.Layer.Services.Contracts
{
    public class ContractsService : ServiceBase, IContractsService
    {
        public ContractsService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<Contract> GetContractsByContractor(long contractorId)
        {
            return Exec<IEnumerable<Contract>>((p) =>
            {
                var contracts = ContractsQuery(p).AsNoTracking().Where(m => m.Supplier == contractorId.ToString() ||
                                                                            m.Consumer == contractorId.ToString()).ToArray();
                return contracts.Select(c => new Contract
                {
                    ContractId = c.ContractId,
                    Number = c.Number,
                    StartDate = c.StartDate,
                    EndDate = c.EndDate,
                    //SupplierId = string.IsNullOrEmpty(c.Supplier) ? (long?)null : long.Parse(c.Supplier),
                    //ConsumerId = string.IsNullOrEmpty(c.Consumer) ? (long?)null : long.Parse(c.Consumer)
                });
            });
        }

        private IQueryable<InternalContract> ContractsQuery(IDataContextHandlerProcess process)
        {
            return process.GetHandler<IContractsHandler>().GetContracts()
                    .Select(c => new InternalContract
                    {
                        ContractId = c.BAId,
                        Number = c.Number,
                        StartDate = (DateTime)c.StartDate,
                        EndDate = (DateTime)c.EndDate,
                        //Supplier = c.CAContractTermValues.Where(p => p.MarkerId == MAMarkerIdentifiers.CompanyArea &&
                        //                                        p.StartDate <= (c.StartDate.Value > DateTime.Now ?
                        //                                                       (c.EndDate.Value < DateTime.Now ? c.EndDate.Value : c.StartDate.Value) :
                        //                                                        DateTime.Now) &&
                        //                                        p.EndDate >= (c.StartDate.Value > DateTime.Now ?
                        //                                                     (c.EndDate.Value < DateTime.Now ? c.EndDate.Value : c.StartDate.Value) :
                        //                                                      DateTime.Now)).FirstOrDefault().sValue,
                        //Consumer = c.CAContractTermValues.Where(p => p.MarkerId == MAMarkerIdentifiers.FactAddress &&
                        //                                             p.StartDate <= (c.StartDate.Value > DateTime.Now ?
                        //                                                            (c.EndDate.Value < DateTime.Now ? c.EndDate.Value : c.StartDate.Value) :
                        //                                                             DateTime.Now) &&
                        //                                             p.EndDate >= (c.StartDate.Value > DateTime.Now ?
                        //                                                          (c.EndDate.Value < DateTime.Now ? c.EndDate.Value : c.StartDate.Value) :
                        //                                                           DateTime.Now)).FirstOrDefault().sValue
                    });
        }
    }

    internal class InternalContract
    {
        internal long ContractId { get; set; }
        internal string Number { get; set; }
        internal DateTime StartDate { get; set; }
        internal DateTime EndDate { get; set; }
        internal string Supplier { get; set; }
        internal string Consumer { get; set; }
    }
}

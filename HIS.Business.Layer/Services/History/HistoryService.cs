﻿using System;
using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.History;
using HIS.DAL.Client.Services.History;
using HIS.DAL.DB.Handlers.History;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;
using HIS.Models.Layer.Models.QueryBuilder;
using Unity;


namespace HIS.Business.Layer.Services.History
{
    public class HistoryService: ServiceBase, IHistoryService
    {
        private IQueryBuilderService _queryBuilderService;

        public HistoryService(IUnityContainer resolver, IQueryBuilderService queryBuilderService) : base(resolver)
        {
            _queryBuilderService = queryBuilderService;
        }

        public IEnumerable<EntityHistory> GetEntityStateHistory(long baId)
        {
            return Exec(p => p.GetHandler<IHistoryHandler>().GetEntityStateHistory(baId));
        }

        public IEnumerable<EntityHistory> GetEntityMarkersHistory(long baId)
        {
            return Exec(p => p.GetHandler<IHistoryHandler>().GetEntityMarkersHistory(baId));
        }

        public string GetResultQuery(long baId)
        {
            return Exec(p => p.GetHandler<IHistoryHandler>().GetResultQuery(baId));
        }

        public string GetEntityData(long baId)
        {
            return this.Exec<string>((c) =>
            {
                var baType = c.GetHandler<IHistoryHandler>().GetBaType(baId);
                if (baType == null)
                {
                    return "{}";
                }
                List<FilteringModel> filteringList = new List<FilteringModel>();
                filteringList.Add(new FilteringModel
                {
                    Field = "[" + baType.MVCAlias + "//BAId]",
                    FilterOperator = Models.Layer.Models.QueryBuilder.FiltersOperator.Equal,
                    Value = baId
                });
                List<OrderingModel> orderingList = new List<OrderingModel>();
                orderingList.Add(new OrderingModel
                {
                    OrderingField = "[" + baType.MVCAlias + "//BAId]",
                    SortingOrder = SortingOrderModel.Asc

                });
                QueryBuilderModel queryBuilderModel = new QueryBuilderModel
                {
                    BaTypeId = baType.TypeId,
                    To = 15,
                    From = 0,
                    FilteringsFields = filteringList.ToList(),
                    OrderingsFields = orderingList.ToList()
                };
                return _queryBuilderService.GetData(queryBuilderModel);
                throw new NotImplementedException();
            });
        }






        // public IEnumerable<>

    }
}

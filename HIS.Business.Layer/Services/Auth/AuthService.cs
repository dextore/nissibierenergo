﻿using System.Collections.Generic;
using HIS.DAL.Client.Services.Auth;
using HIS.DAL.DB.Handlers.Auth;
using System.Linq;
using HIS.DAL.Client.Models.Auth;
using HIS.DAL.DB.Context;
using Unity;

namespace HIS.Business.Layer.Services.Auth
{
    /// <summary>
    /// Реализация сервиса авторизации
    /// </summary>
    public class AuthService : ServiceBase, IAuthService
    {
        public AuthService(IUnityContainer resolver) : base(resolver)
        {
        }

        /// <summary>
        /// Проверка прав пользователя
        /// </summary>
        /// <param name="strRights">Права, перечисленные в строке через запятую: "righth1,right2"</param>
        /// <returns>Результат проверки наличия всех заданных прав у пользователя</returns>
        public bool CheckRights(string strRights)
        {
            return Exec(c =>
            {
                if (string.IsNullOrEmpty(strRights)) return false;
                
                var userId = c.GetHandler<IAuthHandler>().GetCurrentUser()?.Id ?? -1;
                if (userId == -1) return false;

                return true;

                //IQueryable<EFRight> userRights = c.GetHandler<IAuthHandler>().GetUserRights(userId);
                //if (userRights == null) return false;

                //string[] sRights = strRights.Split(',');
                //return userRights.Any(r => sRights.Contains(r.Name));               
            });
        }

        /// <summary>
        /// Получение иекущего пользователя.
        /// </summary>
        /// <returns></returns>
        public int GetCurrentUserId()
        {
            return Exec(c => c.GetHandler<IAuthHandler>().GetCurrentUser()?.Id ?? -1);
        }

        /// <summary>
        /// Проверка наличия ролей пользователя
        /// </summary>
        /// <param name="strRoles">Роли, перечисленные в строке через запятую: "role1,role2"</param>
        /// <returns>Результат проверки наличия заданных ролей у пользователя</returns>
        public bool CheckRoles(string strRoles)
        {
            return Exec(c =>
            {
                if (string.IsNullOrEmpty(strRoles)) return false;

                var userId = c.GetHandler<IAuthHandler>().GetCurrentUser()?.Id ?? -1;
                if (userId == -1) return false;

                return true;

                //IQueryable<EFRole> userRoles = c.GetHandler<IAuthHandler>().GetUserRoles(userId);
                //if (userRoles == null) return false;

                //string[] sRoles = strRoles.Split(',');
                //return userRoles.Any(r => sRoles.Contains(r.Name));
            });
        }

        /// <summary>
        /// Проверка наличия пользователей в БД авторизации
        /// </summary>
        /// <param name="strUsers">Пользователи, перечисленные в строке через запятую: "user1,user2"</param>
        /// <returns>Результат проверки наличия пользователей в БД авторизации</returns>
        public bool CheckUsers(string strUsers)
        {
            return Exec(c =>
            {
                if (string.IsNullOrEmpty(strUsers)) return false;

                string[] sUsers = strUsers.Split(',');
                return sUsers.Any(u => u.Contains(c.GetHandler<IAuthHandler>().GetCurrentUserName()));
            });
        }

        /// <summary>
        /// Список БД для авторизации
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AreaDataBase> GetDataBaseAreas()
        {
            return Exec(
                c =>
                {
                    return c.GetHadlerWithoutContext<IAuthHandler>().GetDataBaseAreas().Select(s => new AreaDataBase()
                    {
                        Alias = s.Alias,
                        Desc = s.Desc,
                        Name = s.Name
                    }).ToList();
                }
            );
        }

        /// <summary>
        /// Проверка пользователя на ба авторизации
        /// </summary>
        /// <param name="id">ИД пользователя</param>
        /// <param name="dbAlias">Алиас БД авторизации</param>
        /// <returns></returns>
        public bool CheckUser(int id, string dbAlias)
        {
            return Exec(c => c.GetHadlerWithoutContext<IAuthHandler>().CheckUser(id, dbAlias));
        }          
    }
}

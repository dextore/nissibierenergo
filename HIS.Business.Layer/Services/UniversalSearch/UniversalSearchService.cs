﻿using HIS.DAL.Client.Models.SystemSearch;
using HIS.DAL.Client.Services.UniversalSearch;
using HIS.DAL.DB.Handlers.UniversalSearch;
using Unity;

namespace HIS.Business.Layer.Services.UniversalSearch
{
    public class UniversalSearchService: ServiceBase, IUniversalSearchService
    {
        public UniversalSearchService(IUnityContainer resolver) : base(resolver)
        {
        }

        public SearchResult SearchByName(long from, long to, string value, bool showDeleted, int? baTypeId)
        {
            return Exec(p => p.GetHandler<IUniversalSearchHandler>().SearchByName(from, to, value, showDeleted, baTypeId));
        }

        public SearchResult SearchByInn(long from, long to, string value, bool showDeleted, int? baTypeId)
        {
            return Exec(p => p.GetHandler<IUniversalSearchHandler>().SearchByInn(from, to, value, showDeleted, baTypeId));
        }
    }
}

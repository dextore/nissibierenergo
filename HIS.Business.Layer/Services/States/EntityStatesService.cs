﻿using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.States;
using HIS.DAL.Client.Services.States;
using HIS.DAL.DB.Handlers.States;
using Unity;

namespace HIS.Business.Layer.Services.States
{
    public class EntityStatesService : ServiceBase, IEntityStatesService
    {

        public EntityStatesService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<EntityTypeState> GetEntityTypeStates(int baTypeId)
        {
            return Exec(p =>
            {
                return p.GetHandler<IEntityStatesHandler>().GetEntityTypeStates(baTypeId)
                    .Select(m => new EntityTypeState
                    {
                        BATypeId = m.BATypeId,
                        IsDelState = m.IsDelState,
                        IsFirstState = m.IsFirstState,
                        StateId = m.StateId,
                        Name = m.Name ?? m.SOStates.Name
                    }).ToList();
            });
        }
    }
}

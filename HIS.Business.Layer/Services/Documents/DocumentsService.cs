﻿using System;
using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.Documents;
using HIS.DAL.Client.Services.Documents;
using HIS.DAL.DB.Handlers.Common;
using HIS.DAL.DB.Handlers.Documents;
using Unity;

namespace HIS.Business.Layer.Services.Documents
{
    public class DocumentsService: ServiceBase, IDocumentsService
    {
        public DocumentsService(IUnityContainer resolver) : base(resolver)
        {
        }


        public IEnumerable<DocumentTreeItem> GetTreeItem()
        {
            return Exec(p =>
            {
                var documentType = p.GetHandler<IBATypesHandler>().GetTypes().FirstOrDefault(m => m.MVCAlias == "Document");

                return documentType != null ? p.GetHandler<IDocumentsTreeHandler>().GetTreeItem(documentType.TypeId) : null;
            });
        }

        public IEnumerable<DocumentTreeItem> GetFilteredTreeItems(long companyAreaId, DateTime startDate, DateTime endDate)
        {
            return Exec(p =>
            {
                var documentType = p.GetHandler<IBATypesHandler>().GetTypes().FirstOrDefault(m => m.MVCAlias == "Document");

                return documentType != null ? p.GetHandler<IDocumentsTreeHandler>().GetFilteredTreeItems(documentType.TypeId, companyAreaId, startDate, endDate) : null;
            });
        }
    }
}

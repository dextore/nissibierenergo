﻿using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.Common;
using HIS.DAL.Client.Services.SystemSearch;
using HIS.DAL.DB.Context;
using HIS.DAL.DB.Handlers.Common;
using Unity;

namespace HIS.Business.Layer.Services.SystemSearch
{
    public class SystemSearchService : ServiceBase, ISystemSearchService
    {
        public SystemSearchService(IUnityContainer resolver) : base(resolver)
        {
        }

        public EntityInfoCollection GetSearchEntities(SystemSearchTypes searchType, string searchText)
        {
            return Exec(c =>
            {
                var resultCollection = new EntityInfoCollection
                {
                    Items = new List<EntityInfoItem>()
                };


                var searchItems = c.GetHandler<ISystemSearchHandler>().GetSystemSearchEntities(searchType, searchText);

                var efSystemSearchEntities = searchItems as EFSystemSearchEntity[] ?? searchItems.OrderBy(o => o.orderId).ToArray();
                if (efSystemSearchEntities.Length >= 1)
                {
                    foreach (EFSystemSearchEntity sItem in efSystemSearchEntities)
                    {
                        EntityInfoItem item = new EntityInfoItem()
                        {
                            BaId = sItem.BaId,
                            TreeType = (EntityTreeTypes)sItem.TreeType,
                            TypeId = sItem.TypeId,
                            TypeName = sItem.TypeName,
                            MVCAlias = sItem.MVCAlias,
                            ParentTypeId = sItem.ParentTypeId,
                            ImplementTypeName = sItem.ImplementTypeName,
                            StateId = sItem.StateId,
                            StateName = sItem.StateName,
                            Name = sItem.Name,
                            FullName = sItem.FullName,
                            Desc = sItem.Desc,
                            Code = sItem.Code
                        };             

                        (resultCollection.Items as List<EntityInfoItem>).Add(item);
                    }
                }

                return resultCollection;
            });
        }

        public FullTextSearchEntityInfoItemCollection GetFullTextSearchEntities(string searchText)
        {
            return Exec(c =>
            {
                var resultCollection = new FullTextSearchEntityInfoItemCollection()
                {
                    Items = new List<FullTextSearchEntityInfoItem>()
                };

                var searchItems = c.GetHandler<ISystemSearchHandler>().GetSystemFullTextSearchEntities(searchText);

                var efSystemFullTextSearchEntities = searchItems as BAFullTextSearchItem[] ?? searchItems.OrderBy(o => o.BaId).ToArray();
                if (efSystemFullTextSearchEntities.Length >= 1)
                {
                    foreach (BAFullTextSearchItem sItem in efSystemFullTextSearchEntities)
                    {
                        FullTextSearchEntityInfoItem item = new FullTextSearchEntityInfoItem()
                        {
                            BaId = sItem.BaId,
                            TypeId = sItem.TypeId,
                            TypeName = sItem.TypeName,
                            StateId = sItem.StateId,
                            StateName = sItem.StateName,
                            StartDate = sItem.StartDate,
                            EndDate = sItem.EndDate,
                            Name = sItem.Name
                        };

                        (resultCollection.Items as List<FullTextSearchEntityInfoItem>).Add(item);
                    }
                }
                return resultCollection;
            });
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using HIS.DAL.Client.Services;
using HIS.DAL.DB;
using Unity;

namespace HIS.Business.Layer.Services
{
    /// <summary>
    /// Базовый класс по работе с хранилищем данных
    /// </summary>
    public abstract class ServiceBase: IService
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        protected ServiceBase(IUnityContainer resolver)
        {
            if (resolver == null)
                throw new ArgumentNullException(nameof(resolver), "ServiceBase");

            Resolver = resolver;           
        }

        /// <summary>
        /// IoC-контейнер
        /// </summary>
        protected IUnityContainer Resolver { get; }

        /// <summary>
        /// Выполнить действие обработчика данных хранилища
        /// </summary>
        /// <typeparam name="TResult">Тип возвращаемого результата</typeparam>
        /// <param name="func">Делегат, который выполняется в рамках процесса</param>
        /// <returns>Результат выполнения</returns>
        protected TResult Exec<TResult>(Func<IDataContextHandlerProcess, TResult> func)
        {
            using (DataContextHandlerProcess process = new DataContextHandlerProcess(Resolver))
            {
                return process.Do(func);
            }
        }

        /// <summary>
        /// Асинхронно выполнить действие обработчика данных хранилища
        /// </summary>
        /// <param name="func">Делегат, который выполняется в рамках процесса</param>
        protected async Task<TResult> ExecAsync<TResult>(Func<IDataContextHandlerProcess, Task<TResult>> func)
        {
            using (DataContextHandlerProcess process = new DataContextHandlerProcess(Resolver))
            {
                return await process.DoWithTransaction(func);
            }
        }


        /// <summary>
        /// Выполнть действие обработчика данных хранилища в единой транзакции
        /// </summary>
        /// <param name="func">Делегат, который выполняется в рамках процесса</param>
        protected TResult ExecWithTransaction<TResult>(Func<IDataContextHandlerProcess, TResult> func)
        {
            using (DataContextHandlerProcess process = new DataContextHandlerProcess(Resolver))
            {
                return process.DoWithTransaction(func);
            }
        }
    }
}

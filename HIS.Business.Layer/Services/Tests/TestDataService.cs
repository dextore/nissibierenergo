﻿using HIS.DAL.Client.Services.Tests;
using HIS.DAL.DB.Handlers.Tests;
using Unity;

namespace HIS.Business.Layer.Services.Tests
{
    public class TestDataService : ServiceBase, ITestDataService
    {
        public TestDataService(IUnityContainer resolver) : base(resolver)
        {
        }

        public void UpdateTestData(bool isMessage, string value)
        {
            ExecWithTransaction(p =>
            {
                p.GetHandler<ITestDataHandler>().UpdateTestData(isMessage, value);
                return 0;
            });
        }
    }
}
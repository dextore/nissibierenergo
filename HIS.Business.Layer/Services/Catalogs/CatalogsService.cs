﻿using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.Catalogs;
using HIS.DAL.Client.Services.Catalogs;
using HIS.DAL.DB.Handlers.Catalogs;
using Unity;

namespace HIS.Business.Layer.Services.Catalogs
{
    public class CatalogsService : ServiceBase, ICatalogsService
    {
        public CatalogsService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<OrganisationLegalForm> OrganisationLegalForms()
        {
            return Exec(p =>
            {
                return p.GetHandler<ICatalogsHandler>().LSOrganisationLegalForms().OrderBy(r => r.OrderNum).Select(m => new OrganisationLegalForm
                {
                    ItemId = m.ItemId,
                    ItemName = m.ItemName,
                    Name = m.Name,
                    ShortName = m.ShortName,
                    IsActive = m.IsActive,
                    OrderNum = m.OrderNum
                }).ToArray();
            });
        }

        public IEnumerable<PersonLegalForm> PersonLegalForms()
        {
            return Exec(p =>
            {
                return p.GetHandler<ICatalogsHandler>().LSPersonLegalForms().OrderBy(r => r.OrderNum).Select(m => new PersonLegalForm
                {
                    ItemId = m.ItemId,
                    ItemName = m.ItemName,
                    Name = m.Name,
                    ShortName = m.ShortName,
                    IsActive = m.IsActive,
                    OrderNum = m.OrderNum
                }).ToArray();
            });
        }
        public IEnumerable<FALocationsForm>FALocations()
        {
            return Exec(p =>
            {
                return p.GetHandler<ICatalogsHandler>().FALocations().OrderBy(r => r.OrderNum).Select(m => new FALocationsForm
                {
                    ItemId = m.ItemId,
                    IsActive = m.IsActive,
                    ItemName = m.ItemName,
                    OrderNum = m.OrderNum,
                    Name = m.ItemName,
                    ShortName = m.ItemName
                }).ToArray();
            });
        }
        public IEnumerable<FAMovReasonsForm> FAMovReasons()
        {
            return Exec(p =>
            {
                return p.GetHandler<ICatalogsHandler>().FAMovReasons().OrderBy(r => r.OrderNum).Select(m => new FAMovReasonsForm
                {
                    ItemId = m.ItemId,
                    IsActive = m.IsActive,
                    ItemName = m.ItemName,
                    OrderNum = m.OrderNum,
                    Name = m.ItemName,
                    ShortName = m.ItemName
                }).ToArray();
            });
        }



    }

}

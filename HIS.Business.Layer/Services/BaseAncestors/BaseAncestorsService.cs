﻿using System.Linq;
using HIS.DAL.Client.Models.BaseAncestors;
using HIS.DAL.Client.Services.BaseAncestors;
using HIS.DAL.DB.Handlers.Common;
using Unity;

namespace HIS.Business.Layer.Services.BaseAncestors
{
    public class BaseAncestorsService: ServiceBase, IBaseAncestorsService
    {
        public BaseAncestorsService(IUnityContainer resolver) : base(resolver)
        {
        }

        public EntityState GetEntitytState(long baId)
        {
            return Exec(c =>
                    {
                        var baseAncestor = c.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor(baId);
                        if (baseAncestor == null)
                            return null;

                        var typeState = baseAncestor.SOStates.SOBATypeStates
                            .FirstOrDefault(t => t.StateId == baseAncestor.StateId && t.BATypeId == baseAncestor.BATypes.TypeId);

                        return new EntityState
                        {
                            BAId = baseAncestor.BAId,
                            BATypeId = baseAncestor.BATypes.TypeId,
                            TypeName = baseAncestor.BATypes.Name,
                            StateId = baseAncestor.SOStates.StateId,
                            StateName = baseAncestor.SOStates.Name,
                            IsFirstState = typeState.IsFirstState,
                            IsDelState = typeState.IsDelState,
                            /*
                            IsFirstState = typeState.IsFirstState ?? false,
                            IsDelState = typeState.IsDelState ?? false,
                            */
                        };
                    });
        }

        public int StateChange(long baId, int operationId)
        {
            return ExecWithTransaction<int>(c => c.GetHandler<IBaseAncestorsHandler>().StateChange(baId, operationId));
        }
    }
}

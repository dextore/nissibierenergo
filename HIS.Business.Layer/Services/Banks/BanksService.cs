﻿using System;
using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.Banks;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.Client.Services.Bank;
using HIS.DAL.DB.Handlers.Banks;
using Unity;

namespace HIS.Business.Layer.Services.Banks
{
    public class BanksService : ServiceBase, IBanksService
    {
        public BanksService(IUnityContainer resolver) : base(resolver)
        {
            
        }

        public IEnumerable<BanksEntity> GetBanks()
        {
            return Exec(p => p.GetHandler<IBanksHandler>().GetBanks());
        }

        public BanksEntity GetBank(long? baId)
        {
            return Exec(p => p.GetHandler<IBanksHandler>()
                .GetBanks(bank => bank.BAId == baId).FirstOrDefault());
        }

        public IEnumerable<BanksEntity> GetBanksBranches(long? baId)
        {
            return Exec(p => p.GetHandler<IBanksHandler>().GetBanksBranches(baId));
        }

        public IEnumerable<PeriodicMarkerValue> GetPeriodicBanksMarkerHistory(long baId, int markerId)
        {
            return Exec(p => p.GetHandler<IBanksHandler>().GetPeriodicBanksMarkerHistory(baId, markerId));
        }

        public PeriodicMarkerValue SaveMarkerValue(MarkeHistoryChanges marker)
        {
            return ExecWithTransaction<PeriodicMarkerValue>(p =>
            {
                var banksHandler = p.GetHandler<IBanksHandler>();

                foreach (var markerValue in marker.Removed)
                {

                    banksHandler.RemoveBanksMarker(
                                    marker.BaId,
                                    marker.MarkerId,
                                    markerValue.StartDate);
                }

                foreach (var markerValue in marker.Changes)
                {
                    banksHandler.SaveMarkerParametr(new BankMarker
                    {
                        Baid = (int)marker.BaId,
                        MarkerId = marker.MarkerId,
                        StartDate = (DateTime)markerValue.StartDate,
                        EndDate = markerValue.EndDate,
                        Note = markerValue.Note,
                        Value = markerValue.Value
                    });

                }
                var date = DateTime.Now.Date;
                var current = banksHandler.GetPeriodicBanksMarkerHistory(marker.BaId, marker.MarkerId).FirstOrDefault();

                return current;
            });
        }
        
        public BanksEntity Update(BanksEntity entity, int userId)
        {
            var firstInfoAboutBank = entity.BaId;
            if (!entity.ParentId.HasValue)
            {
                if (!InnOnlyOne(entity.Inn, entity.BaId ?? 0))
                {
                    entity.Inn = "-1";
                    return entity;
                }
            }
            

            var res = ExecWithTransaction(p =>
            {
                var bankHandler = p.GetHandler<IBanksHandler>();
                
                var bank = bankHandler.Update(entity);

                if (firstInfoAboutBank == null) // создание
                {
                    foreach (var marker in entity.Markers)
                    {
                        if (bank.BaId != null)
                            marker.Baid = (int) bank.BaId;
                        bankHandler.SaveMarkerParametr(marker);
                    }
                }
                else
                {
                    foreach (var marker in entity.Markers)
                    {
                        var oldvalue = bankHandler.GetPeriodicBanksMarkerHistory(bank.BaId.Value, marker.MarkerId);

                        var periodicMarkerValue = oldvalue.FirstOrDefault(oldMarker => oldMarker.EndDate >= new DateTime(9999, 01, 01));

                        var isChanged = (periodicMarkerValue != null && periodicMarkerValue.Value != marker.Value) 
                                        || !oldvalue.Any() 
                                        || ((periodicMarkerValue != null && periodicMarkerValue.StartDate != marker.StartDate));

                        if (isChanged)
                        {
                            bankHandler.SaveMarkerParametr(new BankMarker
                            {
                                Baid = (int)bank.BaId,
                                EndDate = marker.EndDate,
                                StartDate = marker.StartDate,
                                Note = marker.Note,
                                MarkerId = marker.MarkerId,
                                Value = marker.Value
                            });
                        }
                    }
                }
                
                return bank;
            });

            return res;
        }

        private bool InnOnlyOne(string inn, long sourceBaid)
        {
            return Exec(p => p.GetHandler<IBanksHandler>().CheckThatInnOnlyOne(inn, sourceBaid));
        }

        public string GetBankHeader(long baId)
        {
            return Exec(b => b.GetHandler<IBanksHandler>().GetBankHeader(baId));
        }
    }

}

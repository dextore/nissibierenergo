﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.DocMoveAssets;
using HIS.DAL.Client.Services.DocMoveAssets;
using HIS.DAL.DB.Handlers.DocMoveAssets;
using HIS.DAL.DB.Handlers.Common;
using HIS.DAL.DB.Handlers.Markers;
using Unity;

namespace HIS.Business.Layer.Services.DocMoveAssets
{
    public class DocMoveAssetsService : ServiceBase, IDocMoveAssetsService
    {
        public DocMoveAssetsService(IUnityContainer resolver) : base(resolver)
        {

        }
        public DocMoveAssetsData GetDocMoveAssetsData(long baId)
        {
            return Exec(
                p =>
                {
                    var result = p.GetHandler<IDocMoveAssetsHandler>().GetDocMoveAssetsData(baId);
                    int baTypeId = 0;
                    if (result.BaTypeId.HasValue)
                    {
                        baTypeId = (int)result.BaTypeId;
                    }
                    else
                    {
                        var baseAncestor = p.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor(baId);
                        baTypeId = baseAncestor.TypeId;
                    }
                    result.MarkerValues = p.GetHandler<IMarkerHandler>().GetMarkerValues(baId, baTypeId, null);
                    return result;
                }
            );
        }
        public bool IsHasAssets(long baId)
        {
            return Exec(p => p.GetHandler<IDocMoveAssetsHandler>().IsHasAssets(baId));
        }
        public long UpdateDocMoveAssets(DocMoveAssetsData saveData)
        {
            return ExecWithTransaction(c =>
            {
                var handler = c.GetHandler<IDocMoveAssetsHandler>();
                var baId = handler.UpdateDocMoveAssets(saveData);
                foreach (var markerValue in saveData.MarkerValues)
                {
                    handler.SaveMarkerValue(baId, markerValue);
                }
                return baId;
            });
        }
        public DocMoveAssetsRow GetDocMoveAssetsRow(long docMoveId, long recId)
        {
            return Exec(p => p.GetHandler<IDocMoveAssetsHandler>().GetDocMoveAssetsRow(docMoveId,recId));
        }
        public IEnumerable<DocMoveAssetsRow> GetDocMoveAssetsRows(long docMoveId)
        {
            return Exec(p => p.GetHandler<IDocMoveAssetsHandler>().GetDocMoveAssetsRows(docMoveId));
        }
        public DocMoveAssetsRow UpdateDocMoveAssetRow(DocMoveAssetsRow row)
        {
            return ExecWithTransaction(
                p =>
                {
                    var handler = p.GetHandler<IDocMoveAssetsHandler>();
                    var recId = handler.UpdateDocMoveAssetRow(row);

                    return handler.GetDocMoveAssetsRow(row.FADocMovId, recId);
                }
            );

        }
        public bool DeleteDocMoveAssetRow(DocMoveAssetsRow model)
        {
            return Exec(
                p =>
                {
                    p.GetHandler<IDocMoveAssetsHandler>().DeleteDocMoveAssetRow(model);
                    return true;

                }
            );
        }
        public AssetsData GetAssetsInfo(long baId)
        {
            return Exec(p => p.GetHandler<IDocMoveAssetsHandler>().GetAssetsInfo(baId));
        }

    }
}

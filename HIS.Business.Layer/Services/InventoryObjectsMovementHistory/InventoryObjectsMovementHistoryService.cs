﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.InventoryObjectsMovementHistory;
using HIS.DAL.Client.Services.InventoryObjectsMovementHistory;
using HIS.DAL.DB.Handlers.InventoryObjectsMovementHistory;
using Unity;

namespace HIS.Business.Layer.Services.InventoryObjectsMovementHistory
{
    public class InventoryObjectsMovementHistoryService : ServiceBase, IInventoryObjectsMovementHistoryService
    {
        public InventoryObjectsMovementHistoryService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<InventoryAssets> GetFaAssetesList()
        {
            return Exec(x => x.GetHandler<IInventoryObjectsMovementHistoryHandler>().GetFaAssetesList());
        }

        public IEnumerable<InventoryAssetsMovementHistory> GetInventoryAssetsMovementHistory(long inventoryAssetsId)
        {
            return Exec(x =>
                x.GetHandler<IInventoryObjectsMovementHistoryHandler>()
                    .GetInventoryAssetsMovementHistory(inventoryAssetsId));
        }
    }
}

﻿using HIS.DAL.Client.Services.Operations;
using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.Operations;
using HIS.DAL.DB.Handlers.Operations;
using System.Data.Entity;
using HIS.DAL.Client.Models.BaseAncestors;
using Unity;
using EntityState = HIS.DAL.Client.Models.BaseAncestors.EntityState;

namespace HIS.Business.Layer.Services.Operations
{
    public class OperationsService : ServiceBase, IOperationsService
    {
        public OperationsService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<EntityOperation> GetAllowedEntityOperations(long baId)
        {
            return Exec<IEnumerable<EntityOperation>>((p) =>
            {

                return p.GetHandler<IOperationsHandler>().GetAllowedEntityOperations(baId).Select(m => new EntityOperation
                {
                    TypeId = m.BATypeId,
                    OperationId = m.OperationId,
                    OperationName = m.SOBATypeOperations.SOOperations.Name,
                    IsFirstState = m.SOBATypeStates.IsFirstState,
                    IsDelState = m.SOBATypeStates.IsDelState,
                    SrcStateId = m.SrcStateId,
                    DestStateId = m.DstStateId,
                    IsBlocked = m.IsBlocked
                }).AsNoTracking().ToArray();
            });
        }

        public IEnumerable<StateInfo> GetEntityStates(long baTypeId)
        {
            return Exec<IEnumerable<StateInfo>>((p) =>
            {
                return p.GetHandler<IOperationsHandler>().GetEntityStates(baTypeId).Select(m => new StateInfo
                {
                    BATypeId = m.BATypeId,
                    IsDelState = m.IsDelState,
                    IsFirstState = m.IsFirstState,
                    StateId = m.StateId,
                    StateName = m.SOStates.Name,
                }).AsNoTracking().ToArray();
            });

        }
    }
}


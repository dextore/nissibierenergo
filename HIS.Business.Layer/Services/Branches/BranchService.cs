﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Branches;
using HIS.DAL.Client.Services.Branches;
using HIS.DAL.DB.Handlers.Branches;
using Unity;

namespace HIS.Business.Layer.Services.Branches
{
    public class BranchService : ServiceBase, IBranchService
    {
        public BranchService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<Branch> GetBranches()
        {
            return Exec(p => p.GetHandler<IBranchHandler>().GetBranches());
        }

        public IEnumerable<BranchItem> UpdatePersonBranches(long? comercialPersonId, IEnumerable<BranchItem> branches)
        {
            var result = ExecWithTransaction(p =>
                p.GetHandler<IBranchHandler>()
                    .UpdatePersonBranches(comercialPersonId, branches));

            return result;
        }

        public IEnumerable<BranchItem> DeletePersonBranches(long? comercialPersonId, IEnumerable<BranchItem> branches)
        {
            return ExecWithTransaction(p =>
                p.GetHandler<IBranchHandler>().DeletePersonBranches(comercialPersonId, branches));
        }

        public IEnumerable<BranchItem> GetPersonsBranches(long? comercialPersonId)
        {
            return Exec(p => p.GetHandler<IBranchHandler>().GetPersonsBranches(comercialPersonId));
        }

        public string CheckBranchForCorrect(long? comercialPersonId, BranchItem branch)
        {
            return Exec(p => p.GetHandler<IBranchHandler>().BranchItemCheck(comercialPersonId, branch));
        }
    }
}

﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using HIS.DAL.Client.Models.InventoryTransactions;
using HIS.DAL.Client.Services.InventoryTransactions;
using HIS.DAL.DB.Handlers.Warehouses.InventoryTransactions;
using Unity;

namespace HIS.Business.Layer.Services.InventoryTransactions
{
    public class InventoryTransactionsService: ServiceBase, IInventoryTransactionsService
    {
        public InventoryTransactionsService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<InventoryTransaction> GetByDocument(long baId)
        {
            return Exec(p =>
            {
                return p.GetHandler<InventoryTransactionsHandler>().Get().AsNoTracking()
                    .Where(m => m.DocId == baId)
                    .Select(m => new InventoryTransaction
                        {
                            RecId = m.RecId,
                            CAId = m.CAId,
                            CompanyName = m.LSCompanyAreas.Name,
                            DocId = m.DocId,
                            InventoryId = m.InventoryId,
                            InventoryName = m.IInventory.Name,
                            AccId = m.AccId,
                            AccountName = m.PoACompanyAreasPlanOfAccounts.Name,
                            AccountNum = m.PoACompanyAreasPlanOfAccounts.Num,
                            WarehouseId = m.WarehouseId,
                            WarehouseName = m.WHWarehouses.Name,
                            RecTypeId = m.RecTypeId,
                            RecTypeName = m.PORecTypes.Name,
                            IsIncome = m.IsIncome,
                            Quantity = m.Quantity,
                            VAT = m.VAT,
                            PriceVAT = m.PriceVAT,
                            IsVATIncluded = m.IsVATIncluded,
                            StoreCost = m.StoreCost,
                            SellCost = m.SellCost,
                            SellCostVAT = m.SellCostVAT,
                            OperationDate = m.OperationDate
                        }).ToArray();
            });
        }
    }
}

﻿using HIS.DAL.Client.Services.Markers;
using HIS.DAL.DB.Handlers.Markers;
using Unity;

namespace HIS.Business.Layer.Services.Markers
{
    public class CatalogMarkerValueItemsService : ServiceBase, ICatalogMarkerValueItemsService
    {
        public CatalogMarkerValueItemsService(IUnityContainer resolver) : base(resolver)
        {
        }

        public string GetValueItems(int markerId)
        {
            return Exec(p => p.GetHandler<ICatalogMarkerValueItemsHandler>().GetValueItems(markerId));
        }
    }
}

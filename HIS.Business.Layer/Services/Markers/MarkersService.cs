﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using AutoMapper;
using HIS.DAL.Client.Models.Banks;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.Client.Services.Markers;
using HIS.DAL.DB;
using HIS.DAL.DB.Common;
using HIS.DAL.DB.Handlers.Banks;
using HIS.DAL.DB.Handlers.Common;
using HIS.DAL.DB.Handlers.LegalSubjects.Persons;
using HIS.DAL.DB.Handlers.Inventory;
using HIS.DAL.DB.Handlers.Markers;
using HIS.Models.Layer.Models.Markers;
using Unity;

namespace HIS.Business.Layer.Services.Markers
{
    public class MarkersService : ServiceBase, IMarkersService
    {
        private IMapper _mapper;

        public MarkersService(IUnityContainer resolver, IMapper mapper) : base(resolver)
        {
            _mapper = mapper;
        }

        public IEnumerable<MarkerValue> GetEntityMarkerValues(long baId, IEnumerable<int> markerIds)
        {
            return Exec(c =>
            {
                var baseAncestor = c.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor(baId);
                return baseAncestor == null
                    ? null
                    : c.GetHandler<IMarkerHandler>().GetMarkerValues(baId, baseAncestor.TypeId, markerIds);
            });
        }

        public IEnumerable<MarkerValue> EntityMarkerValues(long baId, ICollection<string> names)
        {
            return Exec(p =>
            {
                var values = p.GetHandler<IMarkerHandler>().GetMarkerValues(baId).AsQueryable();

                var baTypeId = p.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor(baId).TypeId;

                var markersInfo = p.GetHandler<IMarkerHandler>().GetMarkersInfo(baTypeId);

                if (names != null && names.Any())
                {
                    values = values.AsQueryable()
                        .Where(m => names.Any(n => string.Compare(m.MVCAlias, n, StringComparison.CurrentCultureIgnoreCase) == 0));
                }

                var markerValues = values.Join(markersInfo, v => v.MarkerId, i => i.Id, (v, i) => new {v, i})
                    .Select(m => new MarkerValue
                    {
                        MarkerId = m.v.MarkerId,
                        MarkerType = m.i.MarkerType,
                        MVCAliase = m.v.MVCAlias,
                        ItemId = m.v.ItemId,
                        Value = string.IsNullOrEmpty(m.v.MarkerValue)
                            ? m.v.MarkerValue 
                            : (MarkerType)m.i.MarkerType != MarkerType.MtDateTime 
                                ? m.v.MarkerValue 
                                : DateTime.Parse(m.v.MarkerValue).ToString("s", CultureInfo.InvariantCulture) ,
                        StartDate = m.v.StartDate,
                        EndDate = null,
                        Note = m.v.Note,
                        IsVisible = true,
                        IsBlocked = m.v.IsBlocked, 
                        DisplayValue = m.v.MarkerValueText,
                        IsDeleted = false
                    }).ToArray();

                var optionalMarkers = p.GetHandler<IOptionalMarkerHandler>().GetOptionalMarkers(baTypeId).ToArray();

                foreach (var optionalMarker in optionalMarkers)
                {
                    var markerValue = markerValues.FirstOrDefault(m => m.MarkerId == optionalMarker.MarkerId);

                    if (markerValue == null)
                        continue;

                    foreach (var optional in optionalMarker.Optionals)
                    {
                        var optionalValue = markerValues.FirstOrDefault(m => m.MarkerId == optional.MarkerId);
                        if (optionalValue == null)
                            continue;
                        var item = optional.Items.FirstOrDefault(m => m.ItemId.ToString() == markerValue.Value);
                        optionalValue.IsVisible = item?.IsVisible ?? optionalMarker.IsVisible;
                    }
                }

                return markerValues;
            });
        }

        public IEnumerable<MarkerValue> GetCollectibleMarkerValues(long baId, string name)
        {
            //throw new NotImplementedException();
            return Exec(p =>
            {
                return p.GetHandler<IMarkerHandler>().GetCollectibleMarkerValues(baId)
                    .Where(m => string.Compare(m.MVCAlias, name, StringComparison.CurrentCultureIgnoreCase) == 0)
                    .Select(m => new MarkerValue
                    {
                        MarkerId = m.MarkerId,
                        ItemId = m.ItemId,
                        Value = m.MarkerValue,
                        StartDate = m.StartDate,
                        EndDate = null,
                        Note = m.Note,
                        DisplayValue = m.MarkerValueText,
                        IsDeleted = false
                    }).ToArray();
            });
        }

        public IEnumerable<PeriodicMarkerValue> GetPeriodicMarkerHistory(long baId, int markerId)
        {
            return Exec(p =>
            {
                return p.GetHandler<IMarkerHistoryHandler>().GetPeriodicMarkerHistory(baId, markerId);

                //var marker = c.GetHandler<IMarkerHandler>().GetMAMarkers().FirstOrDefault(m => m.MarkerId == markerId);
                //if (marker == null)
                //    return null;

                //var baTypeId = c.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor(baId).TypeId;

                //if (marker.TypeId == (int) MarkerType.MtAddress)
                //{
                //    return c.GetHandler<IMarkerHandler>().GetAddressMarkerValueHistory(baId, baTypeId, markerId);
                //}

                //switch ((BATypes) baTypeId)
                //{
                //    case BATypes.Person:
                //    case BATypes.Organisation:
                //    case BATypes.CommercePerson:
                //    case BATypes.AffiliateOrganisation:
                //        return GetPersonMarkerHistory(baId, markerId, c.GetHandler<PersonSubjectsHandler>(), c);
                //    case BATypes.BanksBranches:
                //    case BATypes.Banks:
                //        return GetBanksMarkerHistory(baId, markerId, c.GetHandler<IBanksHandler>());
                //    case BATypes.Inventory:
                //        return c.GetHandler<IInventoryHandler>().GetPeriodicMarkerHistory(baId, markerId);
                //    default:
                //        return GetMarkerHistory(baTypeId, baId, markerId, c.GetHandler<IMarkerHandler>());
                //}
            });
        }

        public IEnumerable<MarkerValueListItem> GetMarkerValueList(int markerId)
        {
            return this.ExecWithTransaction<IEnumerable<MarkerValueListItem>>((c) =>
            {
                return c.GetHandler<IMarkerHandler>().GetMAMarkerValueList(markerId)
                    .AsNoTracking()
                    .ToArray()
                    .Select(i => new MarkerValueListItem
                    {
                        MarkerId = i.MarkerId,
                        Id = i.ItemId,
                        Name = i.ItemName
                    }).ToArray();
            });
        }

        public IEnumerable<EntityMarkersValueList> GetEntityMarkersValue(long baId)
        {
            return this.Exec<IEnumerable<EntityMarkersValueList>>((c) =>
            {
                //throw new NotImplementedException();
                var baTypeId = c.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor(baId).TypeId;

                return c.GetHandler<IMarkerHandler>().GetMABATypeMarkers()
                    .Where(tm => tm.BATypeId == baTypeId && tm.MAMarkers.TypeId == 5)
                    //.AsNoTracking()
                    .Select(r => new EntityMarkersValueList
                    {
                        MarkerId = r.MarkerId,
                        Values = r.MAMarkers.MAMarkerValueList.Select(vl => new MarkerItemValue
                        {
                            MarkerId = vl.MarkerId,
                            ItemId = vl.ItemId,
                            Value = vl.ItemName
                        })
                    }).ToArray();
            });
        }

        public EntityCaption GetEntityCaption(long baId)
        {
            return this.Exec<EntityCaption>((c) =>
            {
                var ancestor = c.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor(baId);
                if ((BATypes) ancestor.TypeId == BATypes.Organisation)
                {
                    var name = c.GetHandler<IPersonsHandler>().GetLegalPerson(baId).Name;

                    return new EntityCaption
                    {
                        BAId = baId,
                        Caption = name
                    };
                }

                return new EntityCaption
                {
                    BAId = baId,
                    Caption = ""
                };

                throw new NotImplementedException();
            });
        }

        public IEnumerable<MarkerInfo> GetEntityMarkersInfo(int baTypeId)
        {
            var listMarkers = this.Exec(c => c.GetHandler<IMarkerHandler>().GetMarkersInfo(baTypeId));
            /*
            for (int i = 0; i < listMarkers.Count(); i++)
            {
                var item = listMarkers.ElementAt(i);
                listMarkers.ElementAt(i).IsOptional = IsOptional(item.Id, baTypeId);
            }
            */
            return listMarkers;
        
        }

        public IEnumerable<ReferenceMarkerValueItem> GetCatalogMarkerValueList(int markerId)
        {
            return Exec(p => p.GetHandler<IMarkerHandler>().GetCatalogMarkerValueList(markerId));
        }

        public MarkerValueInfo SaveMarkerValue(MarkerValueSet markerValue)
        {
            return this.ExecWithTransaction<MarkerValueInfo>((c) =>
            {
                var markerHandler = c.GetHandler<IMarkerHandler>();

                markerHandler.SetMarkerValue(markerValue.BAId,
                    new MarkerValueRet
                    {
                        MarkerId = markerValue.MarkerId,
                        Value = markerValue.Value
                    });


                return markerHandler.GetMABaseAncestorMarkerValues().Where(v => v.BAId == markerValue.BAId)
                    .Select(r => new MarkerValueInfo
                    {
                        MarkerId = r.MAMarkers.MarkerId,
                        MarkerName = r.MAMarkers.Name,
                        MarkerMvcAlias = r.MAMarkers.MVCAlias,
                        MarkerTypeId = r.MAMarkers.MAMarkerTypes.TypeId,
                        MarkerType = r.MAMarkers.MAMarkerTypes.Name,
                        EntityTypeId = r.MAMarkers.TypeId,
                        Range = r.MAMarkers.MAMarkerValueList.Select(v => new MarkerValueListItem
                        {
                            Id = v.ItemId,
                            Name = v.ItemName
                        }),
                        Value = r.sValue
                    }).First();
            });
        }

        public PeriodicMarkerValue SaveMarkerHistory(MarkeHistoryChanges markeHistoryChanges)
        {
            return this.ExecWithTransaction<PeriodicMarkerValue>((c) =>
            {
                var baTypeId = c.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor(markeHistoryChanges.BaId).TypeId;

                foreach (var markerValue in markeHistoryChanges.Removed)
                {
                    switch ((BATypes) baTypeId)
                    {
                        case BATypes.Organisation:
                        case BATypes.Person:
                        case BATypes.CommercePerson:
                        case BATypes.AffiliateOrganisation:
                        {
                            c.GetHandler<PersonSubjectsHandler>().SaveMarkerValue(markeHistoryChanges.BaId,
                                new MarkerValue
                                {
                                    MarkerId = markeHistoryChanges.MarkerId,
                                    Value = markerValue.Value,
                                    StartDate = markerValue.StartDate,
                                    Note = markerValue.Note,
                                    IsDeleted = true,
                                    DisplayValue = ""
                                });
                            break;
                        }

                        case BATypes.Banks:
                        case BATypes.BanksBranches:
                        {
                            c.GetHandler<IBanksHandler>().RemoveBanksMarker(
                                markeHistoryChanges.BaId,
                                markeHistoryChanges.MarkerId,
                                markerValue.StartDate
                            );
                            break;
                        }
                        case BATypes.Inventory:
                            c.GetHandler<IInventoryHandler>().SaveMarkerValue(markeHistoryChanges.BaId,
                                new MarkerValue
                                {
                                    MarkerId = markeHistoryChanges.MarkerId,
                                    Value = markerValue.Value,
                                    StartDate = markerValue.StartDate,
                                    Note = markerValue.Note,
                                    IsDeleted = true,
                                    DisplayValue = ""
                                });
                            break;
                        default:
                            c.GetHandler<IMarkerHandler>().RemoveMarker(
                                markeHistoryChanges.BaId,
                                markeHistoryChanges.MarkerId,
                                markerValue.StartDate);
                            break;
                    }
                }

                foreach (var markerValue in markeHistoryChanges.Changes)
                {
                    switch ((BATypes) baTypeId)
                    {
                        case BATypes.Organisation:
                        case BATypes.Person:
                        case BATypes.CommercePerson:
                        case BATypes.AffiliateOrganisation:
                            c.GetHandler<PersonSubjectsHandler>().SaveMarkerValue(markeHistoryChanges.BaId,
                                new MarkerValue
                                {
                                    MarkerId = markeHistoryChanges.MarkerId,
                                    Value = markerValue.Value,
                                    StartDate = markerValue.StartDate,
                                    Note = markerValue.Note,
                                    IsDeleted = false,
                                    DisplayValue = ""
                                });
                            break;
                        case BATypes.Banks:
                        case BATypes.BanksBranches:
                        {
                            c.GetHandler<IBanksHandler>().SaveMarkerParametr(new BankMarker
                            {
                                Baid = (int) markeHistoryChanges.BaId,
                                MarkerId = markeHistoryChanges.MarkerId,
                                StartDate = (DateTime) markerValue.StartDate,
                                Note = markerValue.Note,
                                Value = markerValue.Value,
                                EndDate = markerValue.EndDate
                            });
                            break;
                        }
                        case BATypes.Inventory:
                            c.GetHandler<IInventoryHandler>().SaveMarkerValue(markeHistoryChanges.BaId,
                                new MarkerValue
                                {
                                    MarkerId = markeHistoryChanges.MarkerId,
                                    Value = markerValue.Value,
                                    StartDate = markerValue.StartDate,
                                    Note = markerValue.Note,
                                    IsDeleted = false,
                                    DisplayValue = ""
                                });
                            break;
                        default:
                            c.GetHandler<IMarkerHandler>().SetMarkerValuePeriod(
                                markeHistoryChanges.BaId,
                                new MarkerValuePeriodRet
                                {
                                    MarkerId = markeHistoryChanges.MarkerId,
                                    StartDate = markerValue.StartDate,
                                    Value = markerValue.Value,
                                    Deleted = false
                                });
                            break;
                    }
                }

                var date = DateTime.Now.Date;
                //var baTypeId = c.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor(markeHistoryChanges.BaId).TypeId;
                IEnumerable<PeriodicMarkerValue> markers;
                switch ((BATypes) baTypeId)
                {
                    case BATypes.Person:
                    case BATypes.CommercePerson:
                    {
                        markers = GetPersonMarkerHistory(markeHistoryChanges.BaId, markeHistoryChanges.MarkerId,
                            c.GetHandler<IPersonSubjectsHandler>(), c);
                        break;
                    }
                    case BATypes.BanksBranches:
                    case BATypes.Banks:
                    {
                        markers = GetBanksMarkerHistory(markeHistoryChanges.BaId, markeHistoryChanges.MarkerId,
                            c.GetHandler<IBanksHandler>());
                        break;
                    }
                    default:
                    {
                        markers = GetMarkerHistory(baTypeId, markeHistoryChanges.BaId, markeHistoryChanges.MarkerId,
                            c.GetHandler<IMarkerHandler>());
                        break;
                    }

                }



                var current = markers.Where(v => v.StartDate <= date
                                                 && v.EndDate >= date).Select(v => new PeriodicMarkerValue
                {
                    StartDate = v.StartDate,
                    EndDate = v.EndDate,
                    Value = v.Value,
                    Note = v.Note
                }).FirstOrDefault();

                //var current = c.GetHandler<IMarkerHandler>().GetMABATypeMarkers(baTypeId)
                //    .Where(tm => tm.IsPeriodic &&
                //                 !tm.IsCollectible)
                //    .SelectMany(tm => tm.MAMarkers.MABaseAncestorMarkerValuePeriods
                //        .Where(v => v.MarkerId == markeHistoryChanges.MarkerId
                //                    && v.BAId == markeHistoryChanges.BaId
                //                    && v.StartDate <= date
                //                    && v.EndDate >= date), (m, v) => new PeriodicMarkerValue
                //    {
                //        StartDate = v.StartDate,
                //        EndDate = v.EndDate,
                //        SValue = v.sValue,
                //        Note = v.Note
                //    }).FirstOrDefault();

                return current;
            });
        }

        private IEnumerable<PeriodicMarkerValue> GetMarkerHistory(int baTypeId, long baId, int markerId,
            IMarkerHandler handler)
        {

            var markerPeriods = handler.GetMABATypeMarkers()
                .Where(tm => tm.BATypeId == baTypeId &&
                             tm.IsPeriodic &&
                             !tm.IsCollectible)
                .SelectMany(tm => tm.MAMarkers.MABaseAncestorMarkerValuePeriods
                    .Where(v => v.MarkerId == markerId && v.BAId == baId), (m, v) => new PeriodicMarkerValue
                {
                    StartDate = v.StartDate,
                    EndDate = v.EndDate,
                    Value = v.sValue,
                    Note = v.Note
                }).ToList();

            return markerPeriods;
        }

        private IEnumerable<PeriodicMarkerValue> GetBanksMarkerHistory(long baId, int markerId, IBanksHandler handler)
        {
            return handler.GetPeriodicBanksMarkerHistory(baId, markerId);
        }

        private IEnumerable<PeriodicMarkerValue> GetPersonMarkerHistory(long baId, int markerId,
            IPersonSubjectsHandler handler, IDataContextHandlerProcess process)
        {

            var markerPeriods = handler.GetPeriodMarkerValues(baId)
                .Where(v => v.MarkerId == markerId).Select(v => new PeriodicMarkerValue
                {
                    StartDate = v.StartDate,
                    EndDate = v.EndDate,
                    Value = v.sValue,
                    Note = v.Note
                }).ToList();

            var values = process.GetHandler<IMarkerHandler>().GetCatalogMarkerValueList(markerId);
            if (values != null)
            {

                foreach (var mp in markerPeriods)
                {
                    var value = values.FirstOrDefault(v => v.ItemId.ToString() == mp.Value);
                    if (value != null)
                        mp.DisplayValue = value.ItemName;
                }
            }

            return markerPeriods;
        }

        public IEnumerable<OptionalMarkers> GetOptionalMarkers(int BATypeId, int markerId, int ItemId)
        {
            return Exec(p => p.GetHandler<IMarkerHandler>().GetOptionalMarkers(BATypeId, markerId, ItemId));
        }

        public bool IsOptional (int markerId,int  baTypeId)
        {
            return Exec(p => p.GetHandler<IMarkerHandler>().IsOptional(markerId , baTypeId));
        }
    }
}


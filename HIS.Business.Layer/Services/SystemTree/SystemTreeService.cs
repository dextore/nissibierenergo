﻿using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.Common;
using HIS.DAL.Client.Models.SystemTree;
using HIS.DAL.Client.Services.SystemTree;
using HIS.DAL.DB;
using HIS.DAL.DB.Context;
using HIS.DAL.DB.Handlers.Common;
using HIS.DAL.DB.Handlers.Contracts;
using HIS.DAL.DB.Handlers.LegalSubjects;
using Unity;

namespace HIS.Business.Layer.Services.SystemTree
{
    public class SystemTreeService : ServiceBase, ISystemTreeService
    {
        public SystemTreeService(IUnityContainer resolver) : base(resolver)
        {
        }

        private void AddElementsToItem(long? parentId, EntityInfoTreeItem parentItem,
            IEnumerable<EFSystemTreeEntity> elements)
        {
            var efSystemTreeEntities = elements as EFSystemTreeEntity[] ?? elements.ToArray();
            foreach (EFSystemTreeEntity item in efSystemTreeEntities.Where(st => st.TreeParentId == parentId))
            {
                if (parentItem.Items == null)
                {
                    parentItem.Items = new List<EntityInfoTreeItem>();
                }

                EntityInfoTreeItem treeItem = new EntityInfoTreeItem()
                {
                    BaId = item.BaId,
                    ParentTypeId = item.ParentId,
                    TreeType = (EntityTreeTypes) item.TreeType,
                    Name = item.Name,
                    FullName = item.FullName,
                    Desc = item.Desc,
                    ImplementTypeName = item.ImplementTypeName,
                    IsHasChildrenItems = item.IsHasChildrenItems,
                    MVCAlias = item.MVCAlias,
                    TypeId = item.TypeId,
                    TypeName = item.TypeName,
                    StateId = item.StateId,
                    StateName = item.StateName,
                    TreeId = item.TreeId,
                    ParentTreeId = item.TreeParentId,
                    ParentBaId = item.ParentBaId,
                    Code = item.Code
                };

                (parentItem.Items as List<EntityInfoTreeItem>).Add(treeItem);

                AddElementsToItem(item.TreeId, treeItem, efSystemTreeEntities);
            }
        }

        public EntityInfoTreeCollection GetTreeEntities(string treeName, long? entityId, long? entityParentId,
            long? parentBaId, bool? isParent = false)
        {
            return Exec(c =>
            {
                var resultCollection = new EntityInfoTreeCollection
                {
                    Items = new List<EntityInfoTreeItem>()
                };

                var subTree = c.GetHandler<ICommonHandler>()
                    .GetSystemTreeEntities(treeName, entityId, entityParentId, parentBaId, isParent);

                var efSystemTreeEntities = subTree as EFSystemTreeEntity[] ?? subTree.OrderBy(o => o.orderId).ToArray();
                if (efSystemTreeEntities.Length >= 1)
                {
                    long? firstParentId = efSystemTreeEntities.OrderBy(o => o.orderId).FirstOrDefault().TreeParentId;

                    foreach (
                        EFSystemTreeEntity item in efSystemTreeEntities.Where(st => st.TreeParentId == firstParentId))
                    {
                        EntityInfoTreeItem treeItem = new EntityInfoTreeItem()
                        {
                            BaId = item.BaId,
                            ParentTypeId = item.ParentId,
                            TreeType = (EntityTreeTypes) item.TreeType,
                            Name = item.Name,
                            FullName = item.FullName,
                            Desc = item.Desc,
                            ImplementTypeName = item.ImplementTypeName,
                            IsHasChildrenItems = item.IsHasChildrenItems,
                            MVCAlias = item.MVCAlias,
                            TypeId = item.TypeId,
                            TypeName = item.TypeName,
                            StateId = item.StateId,
                            StateName = item.StateName,
                            TreeId = item.TreeId,
                            ParentTreeId = item.TreeParentId,
                            ParentBaId = item.ParentBaId,
                            Code = item.Code
                        };

                        AddElementsToItem(item.TreeId, treeItem, efSystemTreeEntities);

                        (resultCollection.Items as List<EntityInfoTreeItem>).Add(treeItem);
                    }
                }

                return resultCollection;
            });
        }

        public SystemTreeItem GetTreeItem(long baId)
        {
            return Exec(c =>
            {
                var baType = c.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor(baId).BATypes;
                switch (@baType.MVCAlias)
                {
                    case "Contract":
                        var contract = c.GetHandler<IContractsHandler>().GetContract(baId);
                        if (contract?.LSId == null)
                            return null;

                        var ls = c.GetHandler<ILegalSubjectHandler>().GetTreeItem((long)contract.LSId);
                        LoadChildren(c, ls);

                        var parent = c.GetHandler<ILegalSubjectHandler>().GetParentTreeItem(ls.BAId);
                        if (parent == null)
                            return ls;

                        LoadChildren(c, parent);

                        parent.Children.First(m => m.BAId == ls.BAId).Children = ls.Children;

                        return parent;
                    case "Organisation":
                    case "Person":
                        return c.GetHandler<ILegalSubjectHandler>().GetTreeItem(baId);
                    case "CommercePerson":
                    case "AffiliateOrganisation":
                        // get perent
                        var parentItem = c.GetHandler<ILegalSubjectHandler>().GetParentTreeItem(baId);
                        if (parentItem == null)
                            return null;
                        LoadChildren(c, parentItem);
                        return parentItem;
                    default:
                        return null;
                }
            });
        }

        public IEnumerable<SystemTreeItem> GetTreeItemChildren(long baId)
        {
            return Exec(c =>
            {
                var childrenCa = c.GetHandler<IContractsHandler>().GetLSContracts(baId);
                var childrenLs = c.GetHandler<ILegalSubjectHandler>().GetChildrenTreeItems(baId);
                return childrenCa.Concat(childrenLs).ToList();
            });
        }

        private void LoadChildren(IDataContextHandlerProcess process, SystemTreeItem item)
        {
            var childrenCa = process.GetHandler<IContractsHandler>().GetLSContracts(item.BAId);
            var childrenLs = process.GetHandler<ILegalSubjectHandler>().GetChildrenTreeItems(item.BAId);
            item.Children = childrenCa.Concat(childrenLs).ToList();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HIS.DAL.Client.Models.Address;
using HIS.DAL.Client.Services.Fias;
using HIS.DAL.DB.Context;
using HIS.DAL.DB.Handlers.Addresses;
using HIS.FIAS.Interfaces;
using HIS.Models.Layer.Models;
using HIS.Models.Layer.Models.Address;
using HIS.Models.Layer.Models.FIAS;
using Unity;

namespace HIS.Business.Layer.Services.Addresses
{
    public class AddressService : ServiceBase, IAddressService
    {

        private readonly IFiasHandler _fiasHandler;

        public AddressService(IUnityContainer resolver, IFiasHandler fiasRepository)
            : base(resolver)
        {
            _fiasHandler = fiasRepository;
        }

        /*
         * SourceType 0-неопределн 1-ФИАС 2-ручной ввод 
         */
        /// <summary>
        /// Поиск адресной строки
        /// </summary>
        /// <param name="findStr"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AddressFullName>> AddrSearchFull(string findStr)
        {
            var fiasResult = await _fiasHandler.AddrSearchFull(findStr);

            var addrResult = Exec((p) => p.GetHandler<IAddressHandler>().AddressFind(findStr, (int)AddressSourceType.Custom));


            var result = addrResult.ToList().Where(m => m.SourceType == 2)
                .Select(AOAddressToAddressFullName)
                .Union(fiasResult.Select(m =>
                    FiasFindedAddressesWithHousestToAddresFullName(m, AddressSourceType.Fias)).ToArray())
                .OrderBy(m => m.AddressText)
                .ThenBy(m => m.SourceType);

            return result;
        }

        public async Task<AddressFullName> GetAddrFullByHouseId(Guid AOId, Guid houseId)
        {
            var addrResult = Exec(p => p.GetHandler<IAddressHandler>().AddressByHouseId(AOId, houseId));
            if (addrResult != null)
            {
                return FTSAddressListToAddresFullName(addrResult);
            }

            var result = await _fiasHandler.GetAddrFullByHouse(AOId, houseId);
            return FiasFindedAddressesWithHousestToAddresFullName(result, AddressSourceType.Fias);
        }

        public async Task<AddressFullName> GetAddrFullBySteadId(Guid AOId, Guid steadId)
        {
            var addrResult = Exec(p => p.GetHandler<IAddressHandler>().AddressBySteadId(AOId, steadId));
            if (addrResult != null)
            {
                return FTSAddressListToAddresFullName(addrResult);
            }

            var result = await _fiasHandler.GetAddrFullByStead(AOId, steadId);
            return FiasFindedAddressesWithHousestToAddresFullName(result, AddressSourceType.Fias);
        }

        public async Task<AddressFullName> GetAddrFullByAOId(Guid AOId)
        {
            var addrResult = Exec((p) => p.GetHandler<IAddressHandler>().AddressByAOId(AOId));
            if (addrResult != null)
            {
                return FTSAddressListToAddresFullName(addrResult);
            }

            var result = await _fiasHandler.GetAddrFullByAOID(AOId);
            return FiasFindedAddressesWithHousestToAddresFullName(result, AddressSourceType.Fias);
        }

        //public async Task<IEnumerable<FiasFindedAddress>> SimpleAddrSearch(string findStr = null)
        //{
        //    var result = await _fiasRepository.SimpleAddrSearch(findStr);
        //    return result;
        //}

        //public async Task<FiasFindedAddress> SimpleAddrByAOID(Guid AOID)
        //{
        //    var result = await _fiasRepository.SimpleAddrByAOID(AOID);
        //    return result;
        //}

        //public async Task<FiasAddressInfo> GetAddressInfo(Guid AOID)
        //{
        //    var result = await _fiasRepository.GetAddressInfo(AOID);
        //    return result;
        //}

        #region ADVANCEDSEARCH

        public async Task<IEnumerable<AddressCatalog>> GetRFSubjects(RequestModel model)
        {
            var fiasResult = await _fiasHandler.GetRFSubjects(model);
            var addrResult = Exec(p =>
            {
                var query = p.GetHandler<IAddressHandler>()
                    .GetAddressObjetcList(null, (int)AddressObjectLevel.RFSubjects, (int)AddressSourceType.Custom);
                if (!string.IsNullOrWhiteSpace(model.SearchValue))
                    query = query.Where(m => m.Name.ToUpper().StartsWith(model.SearchValue.ToUpper()));

                return query.ToArray();
            });

            var result = UnionCatalogResults(addrResult, fiasResult);

            return result.ToArray();
        }

        //public async Task<IEnumerable<FiasDistrict>> GetDistricts(AddressRequestModel model)
        //{
        //    return await GetCatalogs<FiasDistrict>("GetDistricts", model);
        //}

        public async Task<IEnumerable<AddressCatalog>> GetRegions(AddressRequestModel model)
        {
            var fiasResult = await _fiasHandler.GetRegions(model);
            var addrResult = Exec((p) =>
            {
                var query = p.GetHandler<IAddressHandler>().GetAddressObjetcList(model.AOGuid, (int)AddressObjectLevel.Region, (int)AddressSourceType.Custom);
                if (!string.IsNullOrWhiteSpace(model.SearchValue))
                    query = query.Where(m => m.Name.ToUpper().StartsWith(model.SearchValue.ToUpper()));
                return query.ToArray();
            });
            var result = UnionCatalogResults(addrResult, fiasResult);

            return result.ToArray();

        }

        public async Task<IEnumerable<AddressCatalog>> GetCities(AddressRequestModel model)
        {
            var fiasResult = await _fiasHandler.GetCities(model);
            var addrResult = Exec((p) =>
            {
                var query = p.GetHandler<IAddressHandler>().GetAddressObjetcList(model.AOGuid, (int)AddressObjectLevel.City, (int)AddressSourceType.Custom);
                if (!string.IsNullOrWhiteSpace(model.SearchValue))
                    query = query.Where(m => m.Name.ToUpper().StartsWith(model.SearchValue.ToUpper()));
                return query.ToArray();
            });
            var result = UnionCatalogResults(addrResult, fiasResult);
            return result;
        }

        //public async Task<IEnumerable<FiasInRegion>> GetInRegions(Guid? AOGUID = null)
        //{
        //    const string methodName = "GetInRegions";
        //    var rqParams = AOGUID != null ? $"AOGUID={AOGUID}" : "";
        //    var result = await GetData<FiasInRegion[]>(methodName, rqParams);
        //    return result;
        //}

        public async Task<IEnumerable<AddressCatalog>> GetSettlements(AddressRequestModel model)
        {
            var fiasResult = await _fiasHandler.GetSettlements(model);
            var addrResult = Exec(p =>
            {
                var query = p.GetHandler<IAddressHandler>().GetAddressObjetcList(model.AOGuid, (int)AddressObjectLevel.Settlement, (int)AddressSourceType.Custom);
                return !string.IsNullOrWhiteSpace(model.SearchValue) ? query.Where(m => m.Name.ToUpper().StartsWith(model.SearchValue.ToUpper())).ToArray() : query.ToArray();
            });
            var result = UnionCatalogResults(addrResult, fiasResult);
            return result;
        }

        public async Task<IEnumerable<AddressCatalog>> GetElmStructures(AddressRequestModel model)
        {
            var fiasResult = await _fiasHandler.GetElmStructures(model);
            var addrResult = Exec(p =>
            {
                var query = p.GetHandler<IAddressHandler>().GetAddressObjetcList(model.AOGuid, (int)AddressObjectLevel.ElmStructure, (int)AddressSourceType.Custom);
                return !string.IsNullOrWhiteSpace(model.SearchValue) ? query.Where(m => m.Name.ToUpper().StartsWith(model.SearchValue.ToUpper())).ToArray() : query.ToArray();
            });
            var result = UnionCatalogResults(addrResult, fiasResult);
            return result;
        }

        public async Task<IEnumerable<AddressCatalog>> GetStreets(AddressRequestModel model)
        {
            var fiasResult = await _fiasHandler.GetStreets(model);
            var addrResult = Exec((p) =>
            {
                var query = p.GetHandler<IAddressHandler>().GetAddressObjetcList(model.AOGuid, (int)AddressObjectLevel.Street, (int)AddressSourceType.Custom);
                return !string.IsNullOrWhiteSpace(model.SearchValue) ? query.Where(m => m.Name.ToUpper().StartsWith(model.SearchValue.ToUpper())).ToArray() : query.ToArray();
            });
            var result = UnionCatalogResults(addrResult, fiasResult);
            return result;
        }

        public async Task<IEnumerable<AddressCatalog>> GetFiasAddressObjetcList(AddressRequestModel model)
        {
            IEnumerable<FiasCatalogBase> result = null;
            switch (model.AOLevel)
            {
                case AddressObjectLevel.RFSubjects:
                    result = await _fiasHandler.GetRFSubjects(model);
                    break;
                case AddressObjectLevel.District:
                    //result = await _fiasHandler.GetRegions(model);
                    break;
                case AddressObjectLevel.Region:
                    result = await _fiasHandler.GetRegions(model);
                    break;
                case AddressObjectLevel.City:
                    result = await _fiasHandler.GetCities(model);
                    break;
                case AddressObjectLevel.Settlement:
                    result = await _fiasHandler.GetSettlements(model);
                    break;
                case AddressObjectLevel.ElmStructure:
                    result = await _fiasHandler.GetElmStructures(model);
                    break;
                case AddressObjectLevel.Street:
                    result = await _fiasHandler.GetStreets(model);
                    break;
            }

            return result?.Select(m => new AddressCatalog
            {
                AOId = m.AOID,
                AOGuid = m.AOGUID,
                NAME = m.NAME,
                SourceType = AddressSourceType.Fias
            }).ToArray();
        }

        public async Task<IEnumerable<AddressFindResult>> GetAddressList(AddressResultRequestModel model)
        {
            int? liveStatus = model.OnlyActyal ? 1 : (int?)null;
            var fiasResulr = await _fiasHandler.GetAddressList(model.AOId, model.HouseId, model.SteadId, liveStatus, model.PostCode, model.CadNum);
            var addrResult = Exec(p => p.GetHandler<IAddressHandler>().GetAdvancedSearchList(model.AOId, model.HouseId, model.SteadId, liveStatus, AddressSourceType.Custom, model.PostCode, model.CadNum).ToArray());
            // var addrResult = Exec(p => p.GetHandler<IAddressHandler>().GetAddressList(model.AOId, model.HouseId, model.SteadId, liveStatus, AddressSourceType.Custom).ToArray());

            var result = fiasResulr.Select(m => new AddressFindResult
            {
                AOId = m.AOID,
                HouseId = m.HOUSEID,
                SteadId = m.STEADID,
                AddressText = m.ADDRESSTEXT,
                PostalCode = m.POSTALCODE,
                HouseNum = m.HOUSENUM,
                BuildNum = m.BUILDNUM,
                StrucNum = m.STRUCTNUM,
                BuildStateName = m.BUILDSTATENAME,
                ActualStateName = m.LIVESTATUSTEXT,
                StartDate = m.STARTDATE,
                CadNum = "",
                AOGuid = m.AOGUID,
                SourceType = AddressSourceType.Fias
            }).Union(addrResult.Select(m => new AddressFindResult
            {
                AOId = m.AOId,
                HouseId = m.HouseId,
                SteadId = m.SteadId,
                AddressText = m.AddressText,
                PostalCode = m.PostalCode,
                HouseNum = m.HouseNum,
                BuildNum = m.BuildNum,
                StrucNum = m.StrucNum,
                BuildStateName = m.BuildStateName,
                ActualStateName = m.ActualStateName,
                StartDate = m.StartDate,
                CadNum = m.CadNum,
                AOGuid = m.AOGuid,
                SourceType = AddressSourceType.Custom
            }));

            result = result.Where(m => m.AOGuid != null);

            result = result.OrderBy(m => m.AddressText);



            return result.ToArray();

        }

        //public async Task<IEnumerable<FiasAddTerritory>> GetAddTerritories(Guid? AOGUID = null)
        //{
        //    var addrResult 
        //    var result = await _fiasRepository.GetAddTerritories(AOGUID);
        //    return result;
        //}

        //public async Task<IEnumerable<FiasAddTerritoryStreet>> GetAddTerritoryStreets(Guid? AOGUID = null)
        //{
        //    var result = await _fiasRepository.GetAddTerritoryStreets(AOGUID);
        //    return result;
        //}

        //public async Task<IEnumerable<AddressHouse>> GetAddressHouses(Guid AOGuid)
        //{
        //    var fiasResult = await _fiasRepository.GetAddressHouses(AOGuid);
        //    return result;
        //}

        #endregion

        public async Task<IEnumerable<AddressHouse>> GetHouses(AddressRequestModel model)
        {
            var addrResult = Exec(p =>
            {
                var query = p.GetHandler<IAddressHandler>().GetHouseList(model.AOGuid, (int)AddressSourceType.Custom);
                //return string.IsNullOrWhiteSpace(model.SearchValue) ? query.ToArray() : query.Where(m => m.Name.ToUpper().StartsWith(model.SearchValue.ToUpper())).ToArray();
                return string.IsNullOrWhiteSpace(model.SearchValue) ? query.ToArray() : query.Where(m => m.Name.ToUpper().Contains(model.SearchValue.ToUpper())).ToArray();
            });
            var fiasResult = await _fiasHandler.GetHouses(model);

            var result = addrResult.Select(m => new AddressHouse
            {
                HouseId = m.HouseId,
                HouseGuid = m.HouseGuid,
                Name = m.Name,
                HouseNum = "",
                PostalCode = m.PostalCode,
                CadNum = m.CadNum,
                LiveStatus = m.LiveStatus,
                SourceType = (AddressSourceType)m.SourceType
            }).Union(fiasResult.Select(m => new AddressHouse
            {
                HouseId = m.HOUSEID,
                HouseGuid = (Guid)m.HOUSEGUID,
                Name = getFiasHouseName(m),
                HouseNum = m.HOUSENUM,
                PostalCode = m.POSTALCODE,
                CadNum = "",
                LiveStatus = (int)m.LIVESTATUS,
                SourceType = AddressSourceType.Fias
            }));

            if (!string.IsNullOrWhiteSpace(model.SearchValue))
                result = result.Where(m => m.Name.ToLower().Contains(model.SearchValue.ToLower()));
            // выводим только актуальные 
            if (model.liveStatus == 1)
            {
                result = result.Where(m => m.LiveStatus.Equals(1));

            }
            result = result.OrderByDescending(m => m.SourceType)
                .ThenBy(m => m.Name)
                .Skip(model.Skip)
                .Take(model.Take);
            return result;
        }

        public async Task<IEnumerable<AddressStead>> GetSteads(AddressRequestModel model)
        {
            var addrResult = Exec(p =>
            {
                var query = p.GetHandler<IAddressHandler>().GetSteadList(model.AOGuid, (int)AddressSourceType.Custom);
                return string.IsNullOrWhiteSpace(model.SearchValue) ? query.ToArray() : query.Where(m => m.Name.ToUpper().StartsWith(model.SearchValue.ToUpper())).ToArray();
            });

            var fiasResult = await _fiasHandler.GetSteads(model);

            var result = addrResult.Select(m => new AddressStead
            {
                SteadId = m.SteadId,
                SteadGuid = m.SteadGuid,
                Name = m.Name,
                PostalCode = m.PostalCode,
                CadNum = m.CadNum,
                LiveStatus = m.LiveStatus,
                SourceType = (AddressSourceType)m.SourceType
            }).Union(fiasResult.Select(m => new AddressStead
            {
                SteadId = m.STEADID,
                SteadGuid = m.STEADGUID,
                Name = m.NUMBER,
                PostalCode = m.POSTALCODE,
                CadNum = "",
                LiveStatus = (int)m.LIVESTATUS,
                SourceType = AddressSourceType.Fias
            }));

            if (!string.IsNullOrWhiteSpace(model.SearchValue))
                result = result.Where(m => m.Name.ToLower().Contains(model.SearchValue.ToLower()));
            /// выводим только актуальные 
            if (model.liveStatus == 1)
            {
                result = result.Where(m => m.LiveStatus.Equals(1));

            }
            result = result.OrderByDescending(m => m.SourceType)
                .ThenBy(m => m.Name)
                .Skip(model.Skip)
                .Take(model.Take);

            return result;
        }

        public async Task<AddressHouse> GetHouse(Guid? AOGuid, Guid? houseId)
        {


            var fiasResult = await _fiasHandler.GetHouse(AOGuid, houseId);
            if (fiasResult != null)
            {
                return new AddressHouse
                {
                    HouseId = fiasResult.HOUSEID,
                    HouseGuid = (Guid)fiasResult.HOUSEGUID,
                    Name = getFiasHouseName(fiasResult),
                    PostalCode = fiasResult.POSTALCODE,
                    CadNum = "",
                    LiveStatus = (int)fiasResult.LIVESTATUS,
                    SourceType = AddressSourceType.Fias
                };
            }

            var addrResult = Exec(p => p.GetHandler<IAddressHandler>().GetHouse(houseId));

            if (addrResult == null)
                return null;

            return new AddressHouse
            {
                HouseId = addrResult.HouseId,
                HouseGuid = addrResult.HouseGuid,
                Name = addrResult.Name,
                PostalCode = addrResult.PostalCode,
                CadNum = addrResult.CadNum,
                LiveStatus = addrResult.LiveStatus,
                SourceType = (AddressSourceType)addrResult.SourceType
            };

        }


        public async Task<AddressStead> GetStead(Guid? AOGuid, Guid? steadId)
        {

            var fiasResult = await _fiasHandler.GetStead(AOGuid, steadId);

            if (fiasResult != null)
            {
                return new AddressStead
                {
                    SteadId = fiasResult.STEADID,
                    SteadGuid = (Guid)fiasResult.STEADGUID,
                    Name = fiasResult.NUMBER,
                    PostalCode = fiasResult.POSTALCODE,
                    CadNum = "",
                    LiveStatus = (int)fiasResult.LIVESTATUS,
                    SourceType = AddressSourceType.Fias
                };
            }
            var addrResult = Exec(p => p.GetHandler<IAddressHandler>().GetStead(steadId));

            if (addrResult == null)
                return null;

            return new AddressStead
            {
                SteadId = addrResult.SteadId,
                SteadGuid = addrResult.SteadGuid,
                Name = addrResult.Name,
                PostalCode = addrResult.PostalCode,
                CadNum = addrResult.CadNum,
                LiveStatus = addrResult.LiveStatus,
                SourceType = (AddressSourceType)addrResult.SourceType
            };
        }

        public SteadUpdate GetSteadUpdateInfo(Guid steadId)
        {
            return Exec(p => p.GetHandler<IAddressHandler>().GetSteadUpdateInfo(steadId));
        }

        public HouseUpdate GetHouseUpdateInfo(Guid houseId)
        {
            return Exec(p => p.GetHandler<IAddressHandler>().GetHouseUpdateInfo(houseId));
        }

        public async Task<AddressCatalog> GetAddressObject(Guid? aoGuid)
        {
            var result = await _fiasHandler.GetAddressObject(aoGuid);

            return new AddressCatalog
            {
                AOId = result.AOID,
                AOGuid = result.AOGUID,
                NAME = result.NAME,
                SourceType = AddressSourceType.Fias
            };
        }

        public async Task<AddressCatalog> GetAddressObjectCatalog(Guid aoGuid)
        {
            var fiasResult = await _fiasHandler.GetAddressObject(aoGuid);
            if (fiasResult != null)
            {
                return new AddressCatalog
                {
                    AOId = fiasResult.AOID,
                    AOGuid = fiasResult.AOGUID,
                    NAME = fiasResult.NAME,
                    SourceType = AddressSourceType.Fias
                };
            }
            var addrResult = Exec(p => p.GetHandler<IAddressHandler>().GetAddressObject(aoGuid));
            //string AddressText = Exec(p=> p.GetHandler)
            return new AddressCatalog
            {
                AOId = addrResult.AOId,
                AOGuid = addrResult.AOGuid,
                NAME = $"{ addrResult.FormalName} { addrResult.ShortName} (ручной)",
                SourceType = (AddressSourceType)addrResult.SourceType
            };
        }



        public async Task<IEnumerable<FiasDetailAddress>> GetDetailAddrSearch(FiasDetailSearch searchModel)
        {
            var result = await _fiasHandler.GetDetailAddrSearch(searchModel);
            return result;
        }

        public async Task<AddressAOIdAOGuid> GetAddressInfo(Guid AOID)
        {
            var result = await _fiasHandler.GetAddressInfo(AOID);
            return new AddressAOIdAOGuid
            {
                AOId = AOID,
                AOGuid = result.AOGUID
            };
        }


        private IEnumerable<AddressCatalog> UnionCatalogResults(IEnumerable<AOAddressObject> addressCatalog,
            IEnumerable<FiasCatalogBase> fiasCatalog)
        {
            var result = addressCatalog.Select(m => new AddressCatalog
            {
                AOId = m.AOId,
                AOGuid = m.AOGuid,
                NAME = $"{m.Name} (ручной)",
                SourceType = (AddressSourceType)m.SourceType
            }).Union(fiasCatalog.Select(m => new AddressCatalog
            {
                AOId = m.AOID,
                AOGuid = m.AOGUID,
                NAME = m.NAME,
                SourceType = AddressSourceType.Fias
            }))
                .OrderByDescending(m => m.SourceType)
                .ThenBy(m => m.NAME);

            return result.ToArray();
        }

        private string getFiasHouseName(FiasAddressHouse house)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append($"{house.ESTSTATUSNAME} {house.HOUSENUM}");
            if (!string.IsNullOrWhiteSpace(house.BUILDNUM))
                sb.Append($" корпус {house.BUILDNUM}");

            if (house.STRSTATUS != null && house.STRSTATUS > 0)
                sb.Append($" {house.STRSTATUSNAME} {house.STRUCNUM}");

            if (house.LIVESTATUS != 1)
                sb.Append($" {house.LIVESTATUSTEXT}");

            return sb.ToString();
        }


        private AddressFullName FiasFindedAddressesWithHousestToAddresFullName(FiasFindedAddressesWithHousest address,
            AddressSourceType sourceType)
        {
            return new AddressFullName
            {
                AOId = address.AOID,
                //AOGuid = address.AOGuid,
                HouseId = address.HOUSEID,
                SteadId = address.STEADID,
                AddressText = address.ADDRESSTEXT,
                SourceType = sourceType
            };
        }

        private AddressFullName AOAddressToAddressFullName(AOAddress address)
        {
            return new AddressFullName
            {
                AOId = address.AOId,
                HouseId = address.HouseId,
                SteadId = address.SteadId,
                AddressText = address.AddressText,
                SourceType = (AddressSourceType)address.SourceType
            };
        }

        private AddressFullName FTSAddressListToAddresFullName(FTSAddressList address)
        {
            return new AddressFullName
            {
                AOId = address.AOId,
                HouseId = address.HouseId,
                SteadId = address.SteadId,
                AddressText = address.AddressText,
                SourceType = (AddressSourceType)address.SourceType
            };
        }
        public IEnumerable<FlatType> GetFlatTypeList(bool isVisible)
        {
            return Exec(p => p.GetHandler<IAddressHandler>().GetFlatTypeList(isVisible).ToArray());
        }

        public AddressObject GetAddressObject(Guid aoId)
        {
            return Exec(p => p.GetHandler<IAddressHandler>().GetAddressObject(aoId));
        }

        public AddressEntity GetAddressEntity(long baId)
        {
            return Exec(p => p.GetHandler<IAddressHandler>().GetAddressEntity(baId));
        }

        public async Task<IEnumerable<AddressComparisonsModel>> GetAddressUnsettleComparison()
        {
            var unsettleList = Exec(p => p.GetHandler<IAddressHandler>().GetAddressUnsettleList());
            var comparisons = await _fiasHandler.GetAddressComparisons(unsettleList.XMLDATA);
            return comparisons.Select(m => new AddressComparisonsModel
            {
                AOid = m.AOid,
                HouseId = m.HouseId,
                SteadId = m.SteadId,
                AOGuid = m.AOGuid,
                ParentGuid = m.ParentGuid,
                AOLevel = m.AOLevel,
                FullName = m.FullName,
                FiasAOId = m.FAOId,
                FiasHouseId = m.FHouseId,
                FiasSteadId = m.FSteadId,
                FiasAOGuid = m.FAOGuid,
                FiasParentGuid = m.FParentGuid,
                FiasFullName = m.FFullName,
                SourceTypeId = m.SourceTypeId
            });
        }

        public async Task<AddressUpdateResult> AddressUpdate(long? baId, string name, Guid aoId, Guid? houseId, Guid? steadId,
            string location, string flatNum, int? flatType, string sPOBox, bool isBuild, bool isActual, AddressSourceType sourceType)
        {
            FiasXMLAddressData xmlDoc = null;
            if (sourceType == AddressSourceType.Fias)
            {
                // Поучить XML 
                xmlDoc = await _fiasHandler.GetXMLAllParts(aoId, houseId, steadId);
            }

            // Выполнить апдейт
            var addrResult = Exec((p) => p.GetHandler<IAddressHandler>().AddressUpdate(
                baId,
                name,
                sourceType == AddressSourceType.Fias ? null : (Guid?)aoId,
                houseId,
                steadId,
                xmlDoc?.XMLDATA,
                location,
                flatNum,
                flatType,
                sPOBox,
                isBuild,
                isActual));
            // Вернуть результат.
            return addrResult;
        }

        public async Task<IEnumerable<AddressObjectTypeModel>> GetAddressObjectTypes(int level)
        {
            var addressObjectTypes = await _fiasHandler.GetAddressObjectTypes(level);
            return addressObjectTypes.Select(m => new AddressObjectTypeModel
            {
                SCname = m.SCNAME,
                SocrName = m.SOCRNAME
            }).OrderBy(m => m.SocrName).Distinct().ToArray();
        }

        public AddressUpdateResult AddressNameUpdate(long? baId, string newName)
        {
            //выполнить апдейт
            var addrResult = Exec((p) => p.GetHandler<IAddressHandler>().AddressUpdateName(
                baId,
                newName
                ));
            return addrResult;
        }

        /// <summary>
        /// Получение типов зданий
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AddressEstateStatus>> GetEstateStatuses()
        {
            var estateStatuses = await _fiasHandler.GetEstateStatuses();
            return estateStatuses.Select(m => new AddressEstateStatus
            {
                EstStatId = m.ESTSTATID,
                Name = m.NAME,
                DataVersionId = m.DATAVERSIONID,
                IsDelete = m.ISDELETE
            }).OrderBy(m => m.Name).Distinct().ToArray();
        }

        /// <summary>
        /// Получение типов строений
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AddressStructureStatus>> GetStructureStatuses()
        {
            var structureStatuses = await _fiasHandler.GetStructureStatuses();
            return structureStatuses.Select(m => new AddressStructureStatus
            {
                StrStatId = m.STRSTATID,
                Name = m.NAME,
                ShortName = m.SHORTNAME,
                DataVersionId = m.DATAVERSIONID,
                IsDelete = m.ISDELETE
            }).OrderBy(m => m.Name).Distinct().ToArray();
        }

        public async Task<AddressObjectUpdateResult> ValidateAddressObject(AddressObjectUpdate model)
        {
            string ErrorDescription = "";
            bool isFind = false;
            /* если создаем объект , то проверяем его корректность в ФИАСЕ */
            if (!model.AOGuid.HasValue)
            {
                // ищем среди созданных объектов 
                isFind = Exec((p) => p.GetHandler<IAddressHandler>().IsFindAddressObject(model));
                ErrorDescription = "Элемент найден в адресном справочнике!";
                if (isFind != true)
                {
                    isFind = await _fiasHandler.IsFindFiasAddressObject(model.FormalName, model.ShortName, model.AOLevel, model.ParentId);
                    ErrorDescription = "Элемент найден в адресном справочнике ФИАС-а!";
                }
            }
            return new AddressObjectUpdateResult
            {
                AOGuid = Guid.Empty,
                AOId = Guid.Empty,
                IsError = isFind,
                ErrorDescription = ErrorDescription
            };
        }
        public async Task<AddressObjectUpdateResult> AddressObjectUpdate(AddressObjectUpdate model)
        {
            // 
            var tempObject = await ValidateAddressObject(model);
            if (tempObject.IsError == true)
            {
                return tempObject;
            }
            FiasXMLAddressData xmlDoc = null;
            if (!model.AOId.HasValue && model.ParentId.HasValue)
            {
                var addressObject = await _fiasHandler.GetAddressObject(model.ParentId);
                if (addressObject != null)
                {
                    model.ParentId = addressObject.AOID;
                    xmlDoc = await _fiasHandler.GetXMLAllParts(addressObject.AOID);
                }
            }
            var addrResult = Exec((p) => p.GetHandler<IAddressHandler>().AddressObjectUpdate(model, xmlDoc?.XMLDATA));
            return addrResult;
        }
        public async Task<HouseUpdateResult> ValidateHouse(HouseUpdate model)
        {
            string ErrorDescription = "";
            bool isFind = false;
            /* если создаем объект , то проверяем его корректность в ФИАСЕ */
            if (!model.HouseGuid.HasValue)
            {
                // ищем среди созданных объектов 
                isFind = Exec((p) => p.GetHandler<IAddressHandler>().IsFindHouse(model));
                ErrorDescription = "Элемент найден в адресном справочнике!";
                if (isFind != true)
                {
                    isFind = await _fiasHandler.IsFindFiasHouse((Guid)model.ParentGuid, model.EstStateId, model.HouseNum, model.BuildNum, model.StrStateId, model.StructNum, model.CadNum, model.PostCode);
                    ErrorDescription = "Элемент найден в адресном справочнике ФИАС-а!";
                }

            }
            return new HouseUpdateResult
            {
                HouseGuid = Guid.Empty,
                HouseId = Guid.Empty,
                IsError = isFind,
                ErrorDescription = ErrorDescription
            };
        }
        public async Task<HouseUpdateResult> HouseUpdate(HouseUpdate model)
        {
            var tempObject = await ValidateHouse(model);
            if (tempObject.IsError == true)
            {
                return tempObject;
            }
            FiasXMLAddressData xmlDoc = null;
            if (!model.HouseId.HasValue) xmlDoc = await _fiasHandler.GetXMLAllParts((Guid)model.AOId);

            var houseResult = Exec((p) => p.GetHandler<IAddressHandler>().HouseUpdate(model, xmlDoc?.XMLDATA));
            return houseResult;
        }
        public async Task<SteadUpdateResult> ValidateStead(SteadUpdate model)
        {

            string ErrorDescription = "";
            bool isFind = false;
            if (!model.SteadGuid.HasValue && model.ParentGuid.HasValue)
            {
                // ищем среди созданных объектов 
                isFind = Exec((p) => p.GetHandler<IAddressHandler>().IsFindStead(model));
                ErrorDescription = "Элемент найден в адресном справочнике!";
                if (isFind != true)
                {
                    // проверяем в фиасе
                    isFind = await _fiasHandler.IsFindFiasStead((Guid)model.ParentGuid, model.Number, model.CadNum);
                    ErrorDescription = "Элемент найден в адресном справочнике ФИАС-а!";
                }
            }
            return new SteadUpdateResult
            {
                SteadGuid = Guid.Empty,
                SteadId = Guid.Empty,
                IsError = isFind,
                ErrorDescription = ErrorDescription
            };
        }
        public async Task<SteadUpdateResult> SteadUpdate(SteadUpdate model)
        {
            var tempObject = await ValidateStead(model);
            if (tempObject.IsError == true)
            {
                return tempObject;
            }
            FiasXMLAddressData xmlDoc = null;
            if (!model.SteadId.HasValue && model.ParentId.HasValue) xmlDoc = await _fiasHandler.GetXMLAllParts((Guid)model.ParentId);

            var houseResult = Exec((p) => p.GetHandler<IAddressHandler>().SteadUpdate(model, xmlDoc?.XMLDATA));
            return houseResult;
        }
        public IEnumerable<AddressHierarchyResponse> GetAddressHierarchy(long baId)
        {
            var addressHierarchy = Exec(p => p.GetHandler<IAddressHandler>().GetAddressHierarchy(baId));
            return addressHierarchy;
        }
        public async Task<bool> AddressSettle(IEnumerable<AddressSettleRequestModel> models)
        {
            var items = models.OrderBy(m => m.AOLevelId).ToArray();
            foreach (var model in items)
            {
                var xmlDoc = await _fiasHandler.GetXMLAllParts(model.FiasAOId, model.FiasHouseId, model.FiasSteadId);

                Exec(p =>
                {
                    p.GetHandler<IAddressHandler>().AddressSettle(model.AOGuid, model.FiasAOId, model.HouseGuid, model.FiasHouseId, model.SteadGuid, model.FiasSteadId, xmlDoc.XMLDATA);
                    return true;
                });

            }

            return true;
        }
        public bool BindHouses(BindHouseModel bindModel)
        {
            try
            {
                Exec(p =>
                {
                    p.GetHandler<IAddressHandler>().BindHouses(bindModel.FirstHouseId, bindModel.SecondHouseId, bindModel.IsBind);
                    return true;
                });


            }
            catch
            {
                return false;
            }
            return true;
        }


    }
}

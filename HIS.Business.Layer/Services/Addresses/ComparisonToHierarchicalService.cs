﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using HIS.DAL.Client.Services.Addresses;
using HIS.Models.Layer.Models.Address;
using Unity.Attributes;

namespace HIS.Business.Layer.Services.Addresses
{
    public class ComparisonToHierarchicalService: IComparisonToHierarchicalService
    {
        [Dependency]
        public IMapper Mapper { get; set; }

        public IEnumerable<AddressComparisonsDpModel> Convert(IEnumerable<AddressComparisonsModel> items)
        {
            Dictionary<Guid, AddressComparisonsDpModel> resultDic = new Dictionary<Guid, AddressComparisonsDpModel>();

            foreach (var sourceItem in items)
            {
                var resultItem = Mapper.Map<AddressComparisonsModel, AddressComparisonsDpModel>(sourceItem);

                if (resultItem.AOid != null)
                {
                    resultDic[(Guid)resultItem.AOGuid] = resultItem;
                }

                if (resultItem.ParentGuid != null)
                {
                    var parent = resultDic[(Guid)resultItem.ParentGuid];
                    if (parent.Items == null)
                        parent.Items = new List<AddressComparisonsDpModel>();
                    parent.Items.Add(resultItem);
                }
            }

            return resultDic.AsQueryable().Where(kvp => kvp.Value.ParentGuid == null).Select(m => m.Value).ToArray();
        }
    }
}

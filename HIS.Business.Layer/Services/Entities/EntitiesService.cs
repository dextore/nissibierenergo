﻿using System.Linq;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.Client.Services.Entities;
using HIS.DAL.DB.Handlers.Common;
using HIS.DAL.DB.Handlers.Entities;
using HIS.DAL.DB.Handlers.Markers;
using HIS.DAL.Exceptions;
using Unity;

namespace HIS.Business.Layer.Services.Entities
{
    public class EntitiesService: ServiceBase, IEntitiesService
    {
        public EntitiesService(IUnityContainer resolver) : base(resolver)
        {
        }

        public long Update(EntityUpdate model)
        {
            return ExecWithTransaction<long>(p =>
            {
                var markersInfo = p.GetHandler<IMarkerHandler>().GetMarkersInfo(model.BATypeId).ToArray();

                var baId = p.GetHandler<IEntitiesHandler>().Update(model, markersInfo) ?? model.BAId;

                if (baId == null)
                {
                    throw new DataValidationException($"Сущьность не создана!");
                }

                var updateMarkersValue = markersInfo.Join(model.MarkerValues, i => (i.Id), m => (m.MarkerId), (i, m) => new { i, m })
                    .Where(n => n.i.IsImplemented == false).Select(m => m.m).ToArray();
                var alias = p.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor((long)baId).BATypes.MVCAlias;

                p.GetHandler<IEntityMarkersHandler>().UpdateMarkerValues(alias, (long)baId, updateMarkersValue);

                return (long)baId;
            });
        }
    }
}

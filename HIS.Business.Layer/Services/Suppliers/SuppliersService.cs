﻿using HIS.DAL.Client.Services.Suppliers;
using Unity;

namespace HIS.Business.Layer.Services.Suppliers
{
    public class SuppliersService : ServiceBase, ISuppliersService
    {
        public SuppliersService(IUnityContainer resolver) : base(resolver)
        {
        }

        //public IEnumerable<SupplierItem> Get(string name)
        //{
        //    return Exec<IEnumerable<SupplierItem>>((p) =>
        //    {
        //        return p.GetHandler<ISupplierHandler>().GetSuppliers()
        //                .Join(p.GetHandler<ILegalPersonsHandler>().GetLegalPersons(),
        //                      s => s.LSId,
        //                      lp => lp.BAId,
        //                      (s, lp) => new { s, lp })
        //                      .Where(m => name == "null" || string.IsNullOrEmpty(name.Trim()) || m.lp.Name.Contains(name))
        //                      .AsNoTracking().ToList().Select(r => new SupplierItem
        //                      {
        //                          SupplierId = r.s.SupplierId,
        //                          LegalPersonId = r.lp.BAId,
        //                          Code = r.lp.Code,
        //                          Name = r.lp.Name,
        //                          FullName = r.lp.FullName
        //                     });
        //    });
        //}

        //public SupplierItem GetSupplier(long supplierId)
        //{
        //    return Exec<SupplierItem>((p) =>
        //    {
        //        return p.GetHandler<ISupplierHandler>().GetSuppliers()
        //                .Join(p.GetHandler<ILegalPersonsHandler>().GetLegalPersons(),
        //                      s => s.LSId,
        //                      lp => lp.BAId,
        //                      (s, lp) => new { s, lp })
        //                      .Where(m => m.s.SupplierId == supplierId)
        //                      .AsNoTracking().ToList().Select(r => new SupplierItem
        //                      {
        //                          SupplierId = r.s.SupplierId,
        //                          LegalPersonId = r.lp.BAId,
        //                          Code = r.lp.Code,
        //                          Name = r.lp.Name,
        //                          FullName = r.lp.FullName
        //                      }).FirstOrDefault();
        //    });
        //}
    }
}

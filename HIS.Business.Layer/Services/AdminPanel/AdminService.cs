﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.AdminPanel;
using HIS.DAL.Client.Services.AdminPanel;
using HIS.DAL.DB.Common;
using HIS.DAL.DB.Handlers.AdminPanel;
using HIS.Models.Layer.Models.AdminPanel;
using Unity;

namespace HIS.Business.Layer.Services.AdminPanel
{
    public class AdminService: ServiceBase, IAdminService
    {
        public AdminService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<BaTypesView> GetEntities()
        {
            return Exec(p => p.GetHandler<IAdminHandler>().GetEntities());
        }

        public IEnumerable<MarkersAdminView> GetMarkers(int? baTypeId)
        {
            return Exec(p => p.GetHandler<IAdminHandler>().GetMarkers(baTypeId));
        }

        public IEnumerable<OnlyMarkersDataView> GetMarkers()
        {
            return Exec(p => p.GetHandler<IAdminHandler>().GetMarkers());
        }

        public IEnumerable<ItemTableView> GetAvailableValuesForList(int? markerId)
        {
            return Exec(p => p.GetHandler<IAdminHandler>().GetAvailableValuesForList(markerId));
        }

        public IEnumerable<ItemTableView> GetAvailableValuesForReference(int? markerId)
        {
            return Exec(p => p.GetHandler<IAdminHandler>().GetAvailableValuesForReference(markerId));
        }

        public IEnumerable<BaTypesStatesView> GetAvailableSteteForEntiti(int? baTypeId)
        {
            return Exec(p => p.GetHandler<IAdminHandler>().GetAvailableSteteForEntiti(baTypeId));
        }

        public IEnumerable<BaTypesOperationsView> GetAvailableOperationsForEntiti(int? baTypeId)
        {
            return Exec(p => p.GetHandler<IAdminHandler>().GetAvailableOperationsForEntiti(baTypeId));
        }

        public IEnumerable<BaTypesOperationsStateView> GetAvailablOperationsStateForEntiti(int? baTypeId)
        {
            return Exec(p => p.GetHandler<IAdminHandler>().GetAvailablOperationsStateForEntiti(baTypeId));
        }

        public BaTypesView CreateUpdateEntitiType(BaTypesView view)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().CreateUpdateEntitiType(view));
        }

        public OnlyMarkersDataView CreateUpdateMarker(OnlyMarkersDataView marker)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().CreateUpdateMarker(marker));
        }

        public bool DeleteMarkerFromBaType(int baId, MarkersAdminView marker)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().DeleteMarkerFromBaType(baId, marker));
        }

        public MarkersAdminView AddMarkerToBaType(int baId, MarkersAdminView view)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().AddMarkerToBaType(baId, view));
        }

        public bool CanEditLinkOnEntityMarker(int markerId)
        {
            return Exec(p => p.GetHandler<IAdminHandler>().CanEditLinkOnEntityMarker(markerId));
        }

        public bool DeleteMarker(int markerId)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().DeleteMarker(markerId));
        }

        public BaTypesStatesCreateView CreateUpdateState(BaTypesStatesCreateView state)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().CreateUpdateState(state));
        }

        public bool DeleteState(int stateId)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().DeleteState(stateId));
        }

        public BaTypesOperationsCreateView CreateUpdateOperations(BaTypesOperationsCreateView view)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().CreateUpdateOperations(view));
        }

        public bool DeleteOperations(int operationsId)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().DeleteOperations(operationsId));
        }

        public IEnumerable<BaTypesStatesCreateView> GetAllStates()
        {
            return Exec(p => p.GetHandler<IAdminHandler>().GetAllStates());
        }

        public IEnumerable<BaTypesOperationsCreateView> GetAllOperations()
        {
            return Exec(p => p.GetHandler<IAdminHandler>().GetAllOperations());
        }

        public ItemTableView CreateUpdateListItem(int markerId, ItemTableView listItem)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().CreateUpdateListItem(markerId, listItem));
        }

        public bool DeleteListItem(int markerId, int listItemId)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().DeleteListItem(markerId, listItemId));
        }

        public BaTypesStatesView AddStateToBaType(int baId, BaTypesStatesView state)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().AddStateToBaType(baId, state));
        }

        public bool DeleteStateFromBaType(int baId, int stateId)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().DeleteStateFromBaType(baId, stateId));
        }

        public BaTypesOperationsView AddOperationToBaType(int baId, BaTypesOperationsView operation)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().AddOperationToBaType(baId, operation));
        }

        public bool DeleteOperationFromBaType(int baId, int operationId)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().DeleteOperationFromBaType(baId, operationId));
        }

        public BaTypesOperationsStateView AddOperationStateToBaType(int baId, BaTypesOperationsStateCreateView operationState)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().AddOperationStateToBaType(baId, operationState));
        }

        public bool DeleteOperationStateToBaType(int baId, int operationStateId, int srcStateId)
        {
            return ExecWithTransaction(p => p.GetHandler<IAdminHandler>().DeleteOperationStateToBaType(baId, operationStateId, srcStateId));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using HIS.DAL.Client.Models.DocAcceptAssets;
using HIS.DAL.Client.Services.DocAcceptAssets;
using HIS.DAL.DB.Handlers.DocAcceptAssets;
using HIS.DAL.DB.Handlers.Common;
using HIS.DAL.DB.Handlers.Markers;
using HIS.DAL.DB.Context;
using Unity;

namespace HIS.Business.Layer.Services.DocAcceptAssets
{
    public class DocAcceptAssetsService : ServiceBase, IDocAcceptAssetsService
    {
        public DocAcceptAssetsService(IUnityContainer resolver) : base(resolver)
        {
        }
        public DocAcceptAssetsData GetDocAcceptAssets(long baId)
        {
            return Exec(
                p =>
                {
                    var result = p.GetHandler<IDocAcceptAssetsHandler>().GetDocAcceptAssets(baId);
                    int baTypeId = 0;
                    if (result.BaTypeId.HasValue)
                    {
                        baTypeId = (int)result.BaTypeId;

                    } else {
                        var baseAncestor = p.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor(baId);
                        baTypeId = baseAncestor.TypeId;
                    }
                    result.MarkerValues = p.GetHandler<IMarkerHandler>().GetMarkerValues(baId, baTypeId, null);
                    return result;
                }
            );
        }
        public IEnumerable<DocAcceptAssetsRow> GetDocAcceptAssetsRows(long docAcceptId)
        {
            return Exec(p => p.GetHandler<IDocAcceptAssetsHandler>().GetDocAcceptAssetsRows(docAcceptId));
        }
        public DocAcceptAssetsRow GetDocAcceptAssetsRow(long docAcceptId, long recId)
        {
            return Exec(p => p.GetHandler<IDocAcceptAssetsHandler>().GetDocAcceptAssetsRow(docAcceptId,recId));
        }
        public IEnumerable<ReceiptInvoiceSelect> GetReceiptInvoiceSelect(long caId)
        {
            return Exec(p=> p.GetHandler<IDocAcceptAssetsHandler>().GetReceiptInvoiceSelect(caId));
        }
        public IEnumerable<ReceiptInvoiceInventSelect> GetReceiptInvoiceInventSelect(long docAcceptId)
        {
            return Exec(p =>
            {
                var handler = p.GetHandler<IDocAcceptAssetsHandler>();
                var docAcceptAssets = handler.GetDocAcceptAssets(docAcceptId);
                return handler.GetReceiptInvoiceInventSelect((long)docAcceptAssets.RIId);

            });
        }
        public long UpdateDocAcceptAssets(DocAcceptAssetsData saveData)
        {
            return ExecWithTransaction(c =>
            {
                var handler = c.GetHandler<IDocAcceptAssetsHandler>();
                var baId = handler.UpdateDocAcceptAssets(saveData);

                foreach (var markerValue in saveData.MarkerValues)
                {
                    handler.SaveMarkerValue(baId, markerValue);
                }           
                return baId;
            });
        }
        public DocAcceptAssetsRow UpdateDocAcceptAssetRow(DocAcceptAssetsRow model)
        {
            return ExecWithTransaction(
                p =>
                {
                    var handler = p.GetHandler<IDocAcceptAssetsHandler>();
                    var recId = handler.UpdateDocAcceptAssetRow(model);

                    return handler.GetDocAcceptAssetsRow(model.FADocAcceptId, recId);
                } 
            );

        }


        public bool DeleteDocAcceptAssetRow(DocAcceptAssetsRow model)
        {
            return Exec(
                p =>
                {
                    try
                    {
                        p.GetHandler<IDocAcceptAssetsHandler>().DeleteDocAcceptAssetRow(model);

                    }
                    catch (Exception)
                    {
                        return false;
                    }
                    return true;

                }
            );
        }

        public bool IsHasSpecification(long baId)
        {
            return Exec(p => p.GetHandler<IDocAcceptAssetsHandler>().IsHasSpecification(baId));
        }

      




    }
}

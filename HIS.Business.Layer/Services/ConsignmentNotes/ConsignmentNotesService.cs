﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.ConsignmentNotes;
using HIS.DAL.Client.Services.ConsignmentNotes;
using HIS.DAL.DB.Handlers.ConsignmentNotes;
using Unity;

namespace HIS.Business.Layer.Services.ConsignmentNotes
{
    public class ConsignmentNotesService : ServiceBase, IConsignmentNotesService
    {
        public ConsignmentNotesService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<ConsignmentNotesDocument> GetConsignmentNotesDocuments()
        {
            return Exec(p => p.GetHandler<IConsignmentNotesHandler>().GetConsignmentNotesDocuments());
        }

        public ConsignmentNotesDocument GetConsignmentNotesDocument(long baId)
        {
            return Exec(p => p.GetHandler<IConsignmentNotesHandler>().GetConsignmentNotesDocument(baId));
        }

        public ConsignmentNotesFullDocument GetConsignmentNotesFullDocument(long baId)
        {
            return Exec(p => p.GetHandler<IConsignmentNotesHandler>().GetConsignmentNotesFullDocument(baId));
        }

        public ConsignmentNotesDocument SaveConsignmentNotesDocument(ConsignmentNotesDocumentUpdate consignmentNotesDocumentUpdate)
        {
            return ExecWithTransaction(p =>
                p.GetHandler<IConsignmentNotesHandler>().SaveConsignmentNotesDocument(consignmentNotesDocumentUpdate));
        }

        public ConsignmentNotesSpecifications GetConsignmentNotesSpecification(long recId, long docId)
        {
            return Exec(p => p.GetHandler<IConsignmentNotesHandler>().GetConsignmentNotesSpecification(recId, docId));
        }
        public ConsignmentNotesSpecificationsFull GetConsignmentNotesSpecificationFull(long recId, long docId)
        {
            return Exec(p => p.GetHandler<IConsignmentNotesHandler>().GetConsignmentNotesSpecificationFull(recId, docId));
        }

        public IEnumerable<ConsignmentNotesSpecifications> GetConsignmentNotesSpecificationByDocument(long docId)
        {
            return Exec(p => p.GetHandler<IConsignmentNotesHandler>().GetConsignmentNotesSpecificationByDocument(docId));
        }

        public ConsignmentNotesSpecifications SaveConsignmentNotesSpecification(
            ConsignmentNotesSpecificationsUpdate consignmentNotesSpecificationsUpdate)
        {
            return ExecWithTransaction(p =>
                p.GetHandler<IConsignmentNotesHandler>()
                    .SaveConsignmentNotesSpecification(consignmentNotesSpecificationsUpdate));
        }

        public bool DeleteConsignmentNotesSpecification(long recId, long docId)
        {
            return ExecWithTransaction(p =>
                p.GetHandler<IConsignmentNotesHandler>().DeleteConsignmentNotesSpecification(recId, docId));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using HIS.DAL.Client.Models.Inventory;
using HIS.DAL.Client.Services.Inventory;
using HIS.DAL.DB.Handlers.Inventory;
using HIS.DAL.DB.Handlers.Common;
using HIS.DAL.DB.Handlers.Markers;
using Unity;
using Unity.Interception.Utilities;

namespace HIS.Business.Layer.Services.Inventory
{
    public class InventoryService : ServiceBase, IInventoryService
    {
        public InventoryService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<IInventory> GetInventories()
        {
            return Exec(p => p.GetHandler<IInventoryHandler>().GetInventorys());
        }

        public IEnumerable<IInventory> GetInventoriesByCompanyId(long companyId)
        {
            return Exec(p => p.GetHandler<IInventoryHandler>().GetInventoriesByCompanyId(companyId));
        }
            


        public long InventoryUpdate(InventoryData inventory)
        {
            return ExecWithTransaction(c =>
            {
                var handler = c.GetHandler<IInventoryHandler>();
                var BAId = handler.InventoryUpdate(inventory);
                

                foreach (var markerValue in inventory.MarkerValues)
                {
                    handler.SaveMarkerValue(BAId, markerValue);
                }
                // update status
                if (inventory.SOState == null || inventory.SOState == 2)
                {
                    int createOperationId = 1;
                    int newOperationId = c.GetHandler<IBaseAncestorsHandler>().StateChange(BAId, createOperationId);
                }
                return BAId;
            });
        }


        public InventoryData GetInventoryDataModel(long BAId)
        {
            return Exec(p =>
            {
                var baseAncestor = p.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor(BAId);
                if (baseAncestor == null)
                    return null;

                var currentInventory = p.GetHandler<IInventoryHandler>().GetInventoryById(BAId);
                var result = new InventoryData
                {
                    BAId = currentInventory.BAId,
                    Name = currentInventory.Name,
                    Code = currentInventory.Code,
                    Inventory = currentInventory.ParentId,
                    InventoryName = currentInventory.ParentName,
                    CompanyArea = currentInventory.CAId,
                    CompanyName =  currentInventory.CAName,
                    BuyNatMeaning = currentInventory.BuyUnitId,
                    SaleNatMeaning = currentInventory.SaleUnitId,
                    StockNatMeaning = currentInventory.StockUnitId,
                    InventoryType = currentInventory.TypeId,
                    InventoryKind = currentInventory.KindId,
                    ResourceTypeId = currentInventory.ResourceTypeId,
                    SOState = baseAncestor.StateId
                };
                result.MarkerValues = p.GetHandler<IMarkerHandler>().GetMarkerValues(BAId, baseAncestor.TypeId, null);

                result.MarkerValues.ForEach(item =>
                {
                    if (item.MarkerId == 55)
                    {
                        long baId = Convert.ToInt64(item.Value);
                        if (baId > 0)
                        {
                            item.DisplayValue = p.GetHandler<IInventoryHandler>().GetInventoryGroupsById(baId).Name;
                        }
                    }

                });

                return result;

            });
        }


        public IEnumerable<InventoryListItem> GetInventoriesList(InventoryListFilters filters)
        {
            return Exec(p => p.GetHandler<IInventoryHandler>().GetInventoryList(filters));
        }

        public IEnumerable<LSCompanyAreas> GetLSCompanyAreas()
        {
            return Exec(p => p.GetHandler<IInventoryHandler>().GetLSCompanyAreasList());
        }

        public IEnumerable<RefSysUnits> GetRefSysUnits()
        {
            return Exec(p => p.GetHandler<IInventoryHandler>().GetRefSysUnits());
        }

        public IEnumerable<IGroups> GetInventoriesGroup()
        {
            return Exec(p => p.GetHandler<IInventoryHandler>().GetInventoryGroup());
        }

    }
}

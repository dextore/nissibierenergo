﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.BankAccounts;
using HIS.DAL.Client.Services.LegaSubjects.BankAccounts;
using HIS.DAL.DB.Handlers.LegalSubjects.LSBankAccounts;
using Unity;

namespace HIS.Business.Layer.Services.BankAccounts
{
    public class LSBankAccountService : ServiceBase, ILSBankAccountService
    {
        public LSBankAccountService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<LSBankAccount> GetLSBankAccounts(long baId)
        {
            return Exec(c => c.GetHandler<ILSBankAccountHandler>().GetLSBankAccounts(baId));
        }

        public IEnumerable<LSBankAccount> GetAllLSBankAccounts(long baId)
        {
            return Exec(c => c.GetHandler<ILSBankAccountHandler>().GetAllLSBankAccounts(baId));
        }

        public LSBankAccount GetLSBankAccount(long baId, int itemId)
        {
            return ExecWithTransaction(c => c.GetHandler<ILSBankAccountHandler>().GetLSBankAccount(baId, itemId));
        }

        public LSBankAccount Update(LSBankAccount model)
        {
            return ExecWithTransaction(c => c.GetHandler<ILSBankAccountHandler>().Update(model));
        }

        public bool Remove(long baId, int itemId)
        {
            return ExecWithTransaction(c => c.GetHandler<ILSBankAccountHandler>().DeleteBankAccount(baId, itemId));
        }
    }
}

﻿using System.Linq;
using HIS.DAL.Client.Models.LegalSubjects;
using HIS.DAL.Client.Services.LegaSubjects.Persons;
using HIS.DAL.DB.Handlers.Common;
using HIS.DAL.DB.Handlers.LegalSubjects.Persons;
using HIS.DAL.DB.Handlers.Markers;
using Unity;

namespace HIS.Business.Layer.Services.LegalSubjects.Persons
{
    /// <summary>
    /// Сервис сущности физические лица
    /// </summary>
    public class PersonsService : ServiceBase, IPersonsService
    {
        public PersonsService(IUnityContainer resolver) : base(resolver)
        {
        }

        /// <summary>
        /// Получить данные физического лица
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        public Person GetPerson(long personId)
        {
            return Exec(c =>
            {
                //var handler = c.GetHandler<ILSPersonsHandler>();

                var baseAncestor = c.GetHandler<IBaseAncestorsHandler>().GetBaseAncestor(personId);
                if (baseAncestor == null)
                    return null;

                var efPerson = c.GetHandler<IPersonSubjectsHandler>().GetPerson(personId);
                var person = new Person
                {
                    Code = efPerson.Code,
                    FullName = efPerson.FullName,
                    PersonId = efPerson.BAId,
                    FirstName = efPerson.FirstName,
                    MiddleName = efPerson.MiddleName,
                    LastName = efPerson.LastName,
                    BirthDate = efPerson.BirthDate,
                    Gender = efPerson.Gender,
                    INN = efPerson.INN
                };

                person.MarkerValues = c.GetHandler<IMarkerHandler>().GetMarkerValues(personId, baseAncestor.TypeId, null);

                return person;
            });
        }

        public PersonHeader GetPersonHeader(long personId)
        {
            return Exec(c =>
            {
                var efPerson = c.GetHandler<IPersonSubjectsHandler>().GetPerson(personId);

                return (efPerson != null)
                    ? new PersonHeader
                    {
                        Code = efPerson.Code,
                        FullName = efPerson.FullName,
                        PersonId = efPerson.BAId,
                    }
                    : null;
            });
        }

        /// <summary>
        /// Добавит изменить данные физического лица
        /// </summary>
        /// <param name="personUpdate"></param>
        /// <returns></returns>
        public long Update(PersonUpdate person)
        {
            return ExecWithTransaction(c =>
            {
                var handler = c.GetHandler<IPersonSubjectsHandler>();
                var personId = handler.Update(person);

                foreach (var markerValue in person.MarkerValues)
                {
                    handler.SaveMarkerValue(personId, markerValue);
                }

                handler.UpdateFullTextSearch(personId);

                return personId;
            });
        }

        /// <summary>
        /// Получить данные физ. лица по переданным маркерам 
        /// </summary>
        /// <param name="personInfo"> Представление физ лица в виде personId и коллекции markersId</param>
        /// <returns>Модель вида PersonId + Значения маркеров</returns>
        public PersonInfoResponse GetPersonInformation(PersonInfo personInfo)
        {
            return Exec(p => p.GetHandler<IPersonSubjectsHandler>().GetPersonInformation(personInfo));
        }

        /// <summary>
        /// Обновить информацию о физ лице
        /// </summary>
        /// <param name="personInfo"></param>
        /// <returns></returns>
        public PersonInfoResponse UpdatePersonInformation(PersonInfoUpdate personInfo)
        {
            return ExecWithTransaction(p =>
            {
                var handler = p.GetHandler<IPersonSubjectsHandler>();

                foreach (var marker in personInfo.Markers)
                {
                    
                    //if (string.IsNullOrWhiteSpace(marker.Value))
                    //    continue;
                    handler.SaveMarkerValue(personInfo.PersonId, marker);
                }

                return new PersonInfoResponse
                {
                    PersonId = personInfo.PersonId,
                    Markers = handler.GetPersonInformation(new PersonInfo
                    {
                        PersonId = personInfo.PersonId,
                        MarkersId = personInfo.Markers.Select(marker => (long)marker.MarkerId)
                    }).Markers
                };


            });
        }

    }
}
﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using HIS.DAL.Client.Models.LegalSubjects;
using HIS.DAL.Client.Models.LegalSubjects.Persons;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.Client.Services.LegalPersons;
using HIS.DAL.DB.Context;
using HIS.DAL.DB.Handlers.LegalSubjects.Persons;
using Unity;

namespace HIS.Business.Layer.Services.LegalSubjects.Persons
{
    /// <summary>
    /// Сервис по работе с юрилическими лицами
    /// </summary>
    public class LegalPersonsService : ServiceBase,  ILegalPersonsService
    {

        public LegalPersonsService(IUnityContainer resolver)
            : base(resolver)
        {
        }

        public LegalPerson GetLegalPerson(long personId, IEnumerable<int> markersIds)
        { 
            return Exec(c =>
            {
                var legalPerson = c.GetHandler<IPersonsHandler>().GetLegalPerson(personId);
                var result = new LegalPerson
                {
                    LegalPersonId = legalPerson.BAId,
                    Name = legalPerson.Name,
                    Code = legalPerson.Code,
                    FullName = legalPerson.FullName,
                    INN = legalPerson.INN,
                    BATypeId = legalPerson.BATypeId
                };

                markersIds = !markersIds.Any() ? new List<int> { 101, 102, 103, 104, 106, 121, 131} : markersIds;

                result.Markers = c.GetHandler<IPersonSubjectsHandler>()
                                  .GetPersonInformation(new PersonInfo() { PersonId = personId, MarkersId = markersIds.Select(x => (long)x) }).Markers;
                
                return result;
            });
        }

        public LegalPerson GetParentLegalPerson(long affiliateId)
        {
            return Exec(c =>
            {
                var legalPerson = c.GetHandler<IPersonsHandler>().GetParentLegalPerson(affiliateId);

                return legalPerson == null 
                    ? null 
                    : new LegalPerson
                        {
                            LegalPersonId = legalPerson.BAId,
                            Name = legalPerson.Name,
                            Code = legalPerson.Code,
                            FullName = legalPerson.FullName,
                            INN = legalPerson.INN,
                            BATypeId = legalPerson.BATypeId
                        };
            });
        }

        public LegalPersonImplemented Save(LegalPerson legalPerson)
        {
            var mapper = Resolver.Resolve<IMapper>();

            var efLegalPerson = ExecWithTransaction<EFLSLegalSubject>(c =>
            {
                var legalPersonHandler = c.GetHandler<IPersonsHandler>();
                //TODO: Check that inn only one
                var isInnOnlyOne = legalPersonHandler.CheckThatInnOnlyOne(legalPerson.INN, legalPerson.LegalPersonId ?? 0);

                if (!isInnOnlyOne)
                {
                    var negativeResult = legalPerson.LegalPersonId == null 
                        ? new EFLSLegalSubject
                        {
                            Name = legalPerson.Name,
                            FullName = legalPerson.FullName,
                            INN = "-1",
                            Code = legalPerson.Code,
                            Note = legalPerson.Note,
                            BATypeId = legalPerson.BATypeId
                        }
                        : legalPersonHandler.GetLegalPerson(legalPerson.LegalPersonId.Value);
                   
                    negativeResult.INN = "-1";
                    return negativeResult;
                }

                var legalPersonId = legalPersonHandler.Update(legalPerson);

                var markerHandler = c.GetHandler<IPersonSubjectsHandler>();
                foreach (var legalPersonMarker in legalPerson.Markers)
                {
                    markerHandler.SaveMarkerValue(legalPersonId, legalPersonMarker);
                }

                markerHandler.UpdateFullTextSearch(legalPersonId);

                return legalPersonHandler.GetLegalPerson(legalPersonId);
            });

            return mapper.Map<EFLSLegalSubject, LegalPersonImplemented>(efLegalPerson);
        }

        public PersonInfoResponse UpdatePersonInformation(long legalPersonId, IEnumerable<MarkerValue> model)
        {
            return ExecWithTransaction(c =>
            {
                var legalPersonMarkers = model.ToList();
                var markerHandler = c.GetHandler<IPersonSubjectsHandler>();
                var personHandler = c.GetHandler<IPersonSubjectsHandler>();

                foreach (var legalPersonMarker in legalPersonMarkers)
                {
                    markerHandler.SaveMarkerValue(legalPersonId, legalPersonMarker);
                }

                return personHandler.GetPersonInformation(new PersonInfo() { PersonId = legalPersonId, MarkersId = legalPersonMarkers.Select(x => (long)x.MarkerId) });
            });
        }

        public PersonInfoResponse GetPersonInformation(long legalPersonId, IEnumerable<long> markersIds)
        {
            return Exec(c =>
                c.GetHandler<IPersonSubjectsHandler>()
                    .GetPersonInformation(new PersonInfo() {PersonId = legalPersonId, MarkersId = markersIds }));
        }
    }
}

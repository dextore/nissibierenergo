﻿using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.LegalSubjects.CommercePerson;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.Client.Services.LegaSubjects.CommercePersons;
using HIS.DAL.DB;
using HIS.DAL.DB.Common;
using HIS.DAL.DB.Handlers.LegalSubjects.CommercePersons;
using HIS.DAL.DB.Handlers.Markers;
using Unity;

namespace HIS.Business.Layer.Services.LegalSubjects.CommercePersons
{
    public class CommercePersonService : ServiceBase, ICommercePersonService
    {
        public CommercePersonService(IUnityContainer resolver) : base(resolver)
        {
        }

        public CommercePerson GetCommercePerson(long personId)
        {
            return Exec(p => GetCommercePersonInfo(p, personId));
        }

        public CommercePerson GetOPFByParent(long parentId)
        {
            // TODO переделать
            const string opfMVCAlias = "PersonLegalForm";

            return Exec(p =>
            {
                var subject = p.GetHandler<ICommercePersonHandler>().GetLegalSubjectByParent(parentId);
                if (subject == null)
                    return null;

                var marker = p.GetHandler<IMarkerHandler>().GetMAMarkers().Where(m => m.MVCAlias == opfMVCAlias).FirstOrDefault();

                subject.MarkerValues = p.GetHandler<IMarkerHandler>().GetMarkerValues(
                    (long)subject.BAId, 
                    (int)BATypes.CommercePerson,
                    new List<int>() {marker.MarkerId});

                return subject;
            });
        }

        public IEnumerable<MarkerValue> GetAddressList(long baId)
        {
            return Exec(c => c.GetHandler<ICommercePersonHandler>().GetAddresseList(baId));
        }

        public CommercePerson UpdateCommercePerson(CommercePersonUpdate requestModel)
        {
            return ExecWithTransaction(p =>
            {
                var handler = p.GetHandler<ICommercePersonHandler>();
                var personId = handler.Update(requestModel);

                foreach (var markerValue in requestModel.MarkerValues)
                {
                    handler.SaveMarkerValue(personId, markerValue);
                }

                return GetCommercePersonInfo(p, personId);
            });
        }

        public IEnumerable<MarkerValue> SaveAddresList(long baId, IEnumerable<MarkerValue> addresses)
        {
            return ExecWithTransaction(p =>
            {
                p.GetHandler<ICommercePersonHandler>().SaveAddresses(baId, addresses);
                return p.GetHandler<ICommercePersonHandler>().GetAddresseList(baId);
            });
        }

        private CommercePerson GetCommercePersonInfo(IDataContextHandlerProcess p, long personId)
        {
            var subject = p.GetHandler<ICommercePersonHandler>().GetCommercePerson(personId);

            if (subject == null)
                return null;

            subject.MarkerValues = p.GetHandler<IMarkerHandler>().GetMarkerValues(
                (long)subject.BAId,
                (int)BATypes.CommercePerson,
                null);

            return subject;
        }

    }
}

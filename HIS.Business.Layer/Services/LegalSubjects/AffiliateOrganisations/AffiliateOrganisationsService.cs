﻿using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.LegalSubjects;
using HIS.DAL.Client.Models.LegalSubjects.AffiliateOrganisations;
using HIS.DAL.Client.Services.LegaSubjects.AffiliateOrganisations;
using HIS.DAL.DB.Handlers.LegalSubjects.AffiliateOrganisations;
using HIS.DAL.DB.Handlers.LegalSubjects.Persons;
using Unity;

namespace HIS.Business.Layer.Services.LegalSubjects.AffiliateOrganisations
{
    public class AffiliateOrganisationsService : ServiceBase, IAffiliateOrganisationsService
    {
        public AffiliateOrganisationsService(IUnityContainer resolver) : base(resolver)
        {
        }

        public IEnumerable<AffiliateOrganisation> GetAffiliateOrganisations(long organisactionId)
        {
            return Exec(p => p.GetHandler<IAffiliateOrganisationsHandler>().GetAffiliateOrganisations(organisactionId));
        }

        public AffiliateOrganisation GetAffiliateOrganisation(long affiliateOrganisactionId, IEnumerable<int> markersIds)
        {
            return Exec(p =>
            {
                var affiliateOrganisation = p.GetHandler<IAffiliateOrganisationsHandler>()
                        .GetAffiliateOrganisation(affiliateOrganisactionId);

                markersIds = !markersIds.Any() ? new List<int> { 101, 102, 103, 104, 106, 121, 131 } : markersIds;

                affiliateOrganisation.MarkerValues = p.GetHandler<IPersonSubjectsHandler>()
                    .GetPersonInformation(new PersonInfo() { PersonId = affiliateOrganisactionId, MarkersId = markersIds.Select(x => (long)x) }).Markers;

                return affiliateOrganisation;
            });


        }

        public AffiliateOrganisation Update(AffiliateOrganisation model)
        {
            return ExecWithTransaction(p =>
            {
                var handler = p.GetHandler<IAffiliateOrganisationsHandler>();
                var organisationId = handler.Update(model);
                p.GetHandler<IPersonSubjectsHandler>().UpdateFullTextSearch(organisationId);
                return handler.GetAffiliateOrganisations(model.ParentId)
                    .FirstOrDefault(m => m.LegalPersonId == organisationId);
            });
        }
    }
}

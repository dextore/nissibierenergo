﻿using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.Common;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.Client.Services.Common;
using HIS.DAL.DB.Handlers.Common;
using Unity;

namespace HIS.Business.Layer.Services.Common
{
    public class CommonService : ServiceBase, ICommonService
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="resolver"></param>
        public CommonService(IUnityContainer resolver) : base(resolver)
        {
        }

        /// <summary>
        /// Получение информации о сущености
        /// </summary>
        /// <param name="entityId">ИД сущности</param>
        /// <returns>Модель сущности, либо null, если информация по сущности не определена</returns>
        public EntityInfoItem GetEntityInfo(long entityId)
        {
            return Exec(c =>
            {
                EntityInfoItem efItemInfo =
                    c.GetHandler<ICommonHandler>()
                        .GetBABaseAncestor().Join(c.GetHandler<ICommonHandler>().GetBATypes().AsEnumerable(), ancestor => ancestor.TypeId, type => type.TypeId,
                            (ancestor, type) =>
                            new {
                                ancestor.BAId,
                                ancestor.TypeId,
                                type.Name,
                                type.ParentId,
                                type.ImplementTypeName,
                                type.MVCAlias,
                                ancestor.StateId                                
                            }).Join(c.GetHandler<ICommonHandler>().GetSOStates().AsEnumerable(), ancestor => ancestor.StateId, state => state.StateId,
                            (ancestor, state) =>
                            new EntityInfoItem() {
                                BaId = ancestor.BAId,                                
                                ImplementTypeName = ancestor.ImplementTypeName,
                                MVCAlias = ancestor.MVCAlias,
                                ParentTypeId = ancestor.ParentId,
                                StateId = ancestor.StateId,
                                StateName = state.Name,
                                TreeType = EntityTreeTypes.SO_ELEMENT,
                                Name = string.Empty,
                                FullName = string.Empty,
                                Desc = string.Empty,    
                                Code = string.Empty                            
                            })
                        .FirstOrDefault(ancestor => ancestor.BaId == entityId);

                if (efItemInfo == null) return null;

                efItemInfo.Name = GetEntityName(entityId);
                efItemInfo.FullName = GetEntityFullName(entityId);
                efItemInfo.Desc = GetEntityDescription(entityId);
                efItemInfo.Code = GetEntityCode(entityId);

                return efItemInfo;
            });
        }

        public IEnumerable<EntityInfo> GetEntitiesInfo()
        {
            return Exec(p =>
            {
                return p.GetHandler<ICommonHandler>().GetBATypes().Select(t =>
                new EntityInfo
                {
                    BATypeId = t.TypeId,
                    Name = t.Name,
                    MVCAlias = t.MVCAlias,
                    ImplementTypeName = t.ImplementTypeName,
                    IsGroupType = t.IsGroupType,
                    IsImplementType = t.IsImplementType,
                    ParentBATypeId = t.ParentId,
                    SearchTypeId = t.SearchTypeId,
                    States = t.SOBATypeStates.Where(w => w.BATypeId == t.TypeId)
                        .Select(st=> new EntityStates
                    {
                        StateId = st.StateId,
                        Name = st.SOStates.Name
                    }),
                    MarkersInfo = t.MABATypeMarkers.Select(tm => new MarkerInfo
                    {
                        Id = tm.MarkerId,
                        MarkerType = tm.MAMarkers.MAMarkerTypes.TypeId,
                        SearchTypeId = tm.MAMarkers.BATypes.SearchTypeId,
                        RefBaTypeId = tm.MAMarkers.BATypeId,
                        Name = tm.MAMarkers.MVCAlias,
                        Label = tm.Name == null || tm.Name.Equals("") ? tm.MAMarkers.Name : tm.Name,
                        IsPeriodic = tm.IsPeriodic,
                        IsCollectible = tm.IsCollectible,
                        IsBlocked = tm.IsBlocked,
                        IsRequired = tm.IsRequired,
                        IsOptional = t.MABATypeOptionalMarkers.Any(x => x.OptionalMarkerId == tm.MarkerId && x.BATypeId == t.TypeId),
                        IsImplemented = !(tm.ImplementTypeField == null || tm.ImplementTypeField.Equals("")),
                        ImplementTypeName = tm.ImplementTypeName,
                        CatalogImplementTypeName = tm.MAMarkers.ImplementTypeName,
                        ImplementTypeField = tm.ImplementTypeField,
                        List = tm.MAMarkers.MAMarkerValueList.Where(wm=>wm.MarkerId == tm.MarkerId).Select(v => new ListItem
                        {
                            ItemId = v.ItemId,
                            Value = v.ItemName
                        })
                    })
                }).ToArray();
                
            });
        }

        public IEnumerable<string> CheckErrors(long baId)
        {
            return ExecWithTransaction(p => p.GetHandler<ICommonHandler>().CheckErrors(baId));
        }

        public IEnumerable<EntityViewItem> GetEntityViewsItems(int baTypeId)
        {
            return ExecWithTransaction(p => p.GetHandler<ICommonHandler>().GetEntityViewsItems(baTypeId));
        }
        
        /// <summary>
        /// Получение кода сущности
        /// </summary>
        /// <param name="entityId">ИД сущности</param>
        /// <returns>Код (если он определен) иначе string.Empty !</returns>
        public string GetEntityCode(long entityId)
        {
            return string.Empty;
        }

        /// <summary>
        /// Получение наименования сущности
        /// </summary>
        /// <param name="entityId">ИД сущности</param>
        /// <returns>Наименование</returns>
        public string GetEntityName(long entityId)
        {
            return string.Empty;
        }

        /// <summary>
        /// Получение полного наименования сущности
        /// </summary>
        /// <param name="entityId">ИД сущности</param>
        /// <returns>Полное наименование</returns>
        public string GetEntityFullName(long entityId)
        {
            return string.Empty;
        }

        /// <summary>
        /// Получение описания имени сущности
        /// </summary>
        /// <param name="entityId">ИД сущности</param>
        /// <returns>Описание</returns>
        public string GetEntityDescription(long entityId)
        {
            return string.Empty;
        }
    }
}

﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Common;
using HIS.DAL.Client.Services.Common;
using Unity;

namespace HIS.Business.Layer.Services.Common
{
    public class MainMenuService : ServiceBase, IMainMenuService
    {
        public IEnumerable<MenuItem> GetMenu()
        {
            List<MenuItem> mainMenu = new List<MenuItem>()
            {
                new MenuItem
                {
                    Name = "General",
                    Desc = "Общее",
                    SubItems = new List<MenuItem>()
                    {
                        new MenuItem
                        {
                            Name = "Create",
                            Desc = "Создать"                            
                        },
                        new MenuItem
                        {
                            Name = "Open",
                            Desc = "Открыть"
                        },
                        new MenuItem
                        {
                            Name = "Save",
                            Desc = "Сохранить"
                        },
                        new MenuItem
                        {
                            Name = "Delete",
                            Desc = "Удалить"
                        },
                        new MenuItem
                        {
                            Name = "Find",
                            Desc = "Поиск"
                        },
                        new MenuItem
                        {
                            Name = "Refresh",
                            Desc = "Обновить"
                        },
                        new MenuItem
                        {
                            Name = "Preview",
                            Desc = "Предварительный просмотр"
                        },
                        new MenuItem
                        {
                            Name = "Print",
                            Desc = "Печать"
                        },
                        new MenuItem
                        {
                            Name = "Divisions",
                            Desc = "Отделения",
                            SubItems = new GroupMenuItems("DIV") 
                            {                                                               
                                new CheckedMenuItem()
                                {
                                    Name = "Novosibirsk",
                                    Desc = "Новосибирск",
                                    IsChecked = true
                                },
                                new CheckedMenuItem()
                                {
                                    Name = "Priobsk",
                                    Desc = "Приобское отделение",
                                    IsChecked = false
                                }
                            } 
                        },
                        new MenuItem
                        {
                            Name = "Exit",
                            Desc = "Выход"
                        }
                    }
                },

                //Модули
                new MenuItem()
                {
                    Name = "Administration",
                    Desc = "Администрирование",
                    SubItems = new List<MenuItem>
                    {
                        new ModuleMenuItem
                        {
                            Name = "ModuleTest",
                            Desc = "Тестовый модуль",
                            ModuleName = "TestModule"
                        },
                        new ModuleMenuItem
                        {
                            Name = "ModuleRights",
                            Desc = "Права (роли, карточки доступа и т.п.)",
                            ModuleName = "RightsModule"
                        },
                        new ModuleMenuItem
                        {
                            Name = "ModuleEntities",
                            Desc = "Сущности системы",
                            ModuleName = "EntitiesModule"
                        }
                    }
                },

                new MenuItem
                {
                    Name = "Contractors",
                    Desc = "Контрагенты",
                    SubItems = new List<MenuItem>
                    {
                        new ModuleMenuItem
                        {
                            Name = "ModulePhisicalPersons",
                            Desc = "Физические лица",
                            ModuleName = "PhysicalPersonsModule"
                        },
                        new ModuleMenuItem
                        {
                            Name = "ModuleLegalPersons",
                            Desc = "Юридические лица",
                            ModuleName = "LegalPersonsModule"
                        }
                    }
                },
                
                new MenuItem
                {
                    Name = "Help",
                    Desc = "Помощь",
                    SubItems = new List<MenuItem>
                    {
                        new MenuItem()
                        {
                            Name = "ModuleHelp",
                            Desc = "Помощь"
                        }
                    }
                }
            };

            return mainMenu;
        }

        public MainMenuService(IUnityContainer resolver) 
            : base(resolver)
        {
        }
    }
}

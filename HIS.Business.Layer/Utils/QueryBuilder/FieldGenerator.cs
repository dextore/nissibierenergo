﻿using System;
using System.Collections.Generic;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator;
using HIS.DAL.DB.Handlers.QueryBuilder;
using Unity;
using Unity.Interception.Utilities;

namespace HIS.Business.Layer.Utils.QueryBuilder
{
    public class FieldGenerator : BaseSelectGenerator, IFieldGenerator
    {
        public Dictionary<string, string> GeneratedFieldDictionary { get; set; } = new Dictionary<string, string>();

        public FieldGenerator(IUnityContainer resolver, int baTypeId, IQueryBuilderHandler queryBuilderHandler, IEnumerable<string> selectedPropertyes)
            : base(resolver, baTypeId, queryBuilderHandler, selectedPropertyes) { }

        public IEnumerable<CaptionAndAliase> FormatMarkersAliases()
        {
            var res = AddBaIdAliases(new List<CaptionAndAliase>());

            foreach (var marker in _markers)
            {
                res.Add(new CaptionAndAliase
                {
                    Caption = marker.Name,
                    Aliase = $"[{_entitiy.MvcAlias}//{marker.MvcAlias}]",
                    Type = GetFrontEndType(marker.SQLServerType),
                    MarkerId = marker.MarkerId,
                    MarkerTypeId = marker.MarkerTypeId
                });

                switch (marker.MarkerTypeId)
                {
                    case 5:
                        res.Add(new CaptionAndAliase
                        {
                            Caption = marker.Name,
                            Aliase = $"[{_entitiy.MvcAlias}//{marker.MvcAlias}//ItemName]",
                            Type = FrontEndDataType.String,
                            MarkerId = null,
                            MarkerTypeId = null
                        });
                        break;
                    case 13:
                        res.Add(new CaptionAndAliase
                        {
                            Caption = marker.Name,
                            Aliase = $"[{_entitiy.MvcAlias}//{marker.ImplementTypeField}//ItemName]",
                            Type = FrontEndDataType.String,
                            MarkerId = null,
                            MarkerTypeId = null
                        });
                        break;
                    case 11:
                    case 12:
                        res.Add(new CaptionAndAliase
                        {
                            Caption = marker.Name,
                            Aliase = $"[{_entitiy.MvcAlias}//{marker.MvcAlias}//{marker.LinkedBaTypeFieldName}]",
                            Type = FrontEndDataType.String,
                            MarkerId = null,
                            MarkerTypeId = null
                        });
                        break;
                    default: continue;
                }
            }

            res = AddStateAliases(res);
            return res;
        }

        //TODO : Переписать на независимое получение полей для словаря.
        public void GenerateFieldDictionary()
        {
            var select = SelectMarker.AddBaidServiceFilds(_entitiy);
            ParseTwoValueMarker(select)
                .ForEach(keyvalue => GeneratedFieldDictionary
                    .Add(keyvalue.Key, keyvalue.Value));

            foreach (var marker in _markers)
            {
                select = SelectMarker.Generate(marker, _entitiy);

                switch ((QueryBuilderMarkerTypeId)marker.MarkerTypeId)
                {
                    case QueryBuilderMarkerTypeId.Address:
                    case QueryBuilderMarkerTypeId.Enumeration:
                    case QueryBuilderMarkerTypeId.Link:
                    case QueryBuilderMarkerTypeId.Reference:
                    {
                        ParseTwoValueMarker(select)
                            .ForEach(keyvalue =>
                            {
                                if (!GeneratedFieldDictionary.ContainsKey(keyvalue.Key))
                                {
                                    GeneratedFieldDictionary
                                        .Add(keyvalue.Key, keyvalue.Value);
                                }

                            });

                        break;
                    }
                    default:
                    {
                        var keyvalue = ParseDefaultMarker(select);
                        GeneratedFieldDictionary.Add(keyvalue.Key, keyvalue.Value);
                        break;
                    }
                }
            }

            select = SelectMarker.AddServiceFilds(_entitiy);
            ParseTwoValueMarker(select)
                .ForEach(keyvalue => GeneratedFieldDictionary
                    .Add(keyvalue.Key, keyvalue.Value));
        }

        private static KeyValuePair<string, string> ParseDefaultMarker(string select)
        {
            string[] realNameAndAliase;
            realNameAndAliase = select.Split(new[] { " AS " }, StringSplitOptions.None);
            var key = realNameAndAliase[1].TrimEnd(new[] { ',', '\n', '\t' });
            var value = realNameAndAliase[0];
            return new KeyValuePair<string, string>(key, value);
        }

        private static IEnumerable<KeyValuePair<string, string>> ParseTwoValueMarker(string select)
        {
            var tmpValues = select.TrimEnd(new[] { ',', '\n', '\t' }).Split(new string[] { ",\n\t" }, StringSplitOptions.None);
            foreach (var tmp in tmpValues)
            {
                var realNameAndAliase = tmp.Split(new string[] { " AS " }, StringSplitOptions.None);
                var key = realNameAndAliase[1].TrimEnd(new[] { ',', '\n', '\t' });
                var value = realNameAndAliase[0];
                yield return new KeyValuePair<string, string>(key, value);
            }
        }

        private List<CaptionAndAliase> AddBaIdAliases(List<CaptionAndAliase> res)
        {
            res.Add(new CaptionAndAliase
            {
                Caption = "BaId",
                Aliase = $"[{_entitiy.MvcAlias}//BAId]",
                Type = FrontEndDataType.Number,
                MarkerId = null,
                MarkerTypeId = null
            });

            res.Add(new CaptionAndAliase
            {
                Caption = "BATypeId",
                Aliase = $"[{_entitiy.MvcAlias}//BATypeId]",
                Type = FrontEndDataType.Number,
                MarkerId = null,
                MarkerTypeId = null
            });

            return res;
        }

        private List<CaptionAndAliase> AddStateAliases(List<CaptionAndAliase> res)
        {
            res.Add(new CaptionAndAliase
            {
                Caption = "Состояние",
                Aliase = $"[{_entitiy.MvcAlias}//State]",
                Type = FrontEndDataType.String,
                MarkerId = null,
                MarkerTypeId = null
            });

            res.Add(new CaptionAndAliase
            {
                Caption = "IsDelState",
                Aliase = $"[{_entitiy.MvcAlias}//IsDelState]",
                Type = FrontEndDataType.Boolean,
                MarkerId = null,
                MarkerTypeId = null
            });

            res.Add(new CaptionAndAliase
            {
                Caption = "IsFirstState",
                Aliase = $"[{_entitiy.MvcAlias}//IsFirstState]",
                Type = FrontEndDataType.Boolean,
                MarkerId = null,
                MarkerTypeId = null
            });

            return res;
        }

        private static FrontEndDataType GetFrontEndType(string sqlType)
        {
            switch (sqlType)
            {
                case "bigint": return FrontEndDataType.Number;
                case "int": return FrontEndDataType.Number;
                case "float": return FrontEndDataType.Number;
                case "money": return FrontEndDataType.Number;
                case "nvarchar(2000)": return FrontEndDataType.String;
                case "bit": return FrontEndDataType.Boolean;
                case "datetime": return FrontEndDataType.Date;
                default: throw new NotImplementedException("Type doesnot frontend implamation");
            }
        }
    }
}

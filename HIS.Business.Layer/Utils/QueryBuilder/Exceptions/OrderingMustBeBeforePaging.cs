﻿using System;

namespace HIS.Business.Layer.Utils.QueryBuilder.Exceptions
{
    public class OrderingMustBeBeforePaging : Exception
    {
        public OrderingMustBeBeforePaging(string message) : base(message)
        {
            
        }
    }
}

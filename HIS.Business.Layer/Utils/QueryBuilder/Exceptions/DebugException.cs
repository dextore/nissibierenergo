﻿using System;
using HIS.Models.Layer.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.Exceptions
{
    public class DebugException : Exception
    {
        public string SqlReq { get; set; }
        public QueryBuilderModel QBModel { get; set; }

        public DebugException(Exception ex) : base(ex.Message, ex)
        {

        }
    }
}

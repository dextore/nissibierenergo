﻿using System;

namespace HIS.Business.Layer.Utils.QueryBuilder.Exceptions
{
    public class SelectMustAddfFirst : Exception
    {
        public SelectMustAddfFirst(string message) : base(message) { }
    }
}

﻿using System;

namespace HIS.Business.Layer.Utils.QueryBuilder.Exceptions
{
    public class PaggingAlreadyAdd : Exception
    {
        public PaggingAlreadyAdd(string message) : base(message) { }
    }
}

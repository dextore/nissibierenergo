﻿using System.Collections.Generic;
using HIS.Business.Layer.Utils.QueryBuilder.Models;

namespace HIS.Business.Layer.Utils.QueryBuilder.Interfaces
{
    public interface IFieldGenerator
    {
        Dictionary<string, string> GeneratedFieldDictionary { get; set; }
        void GenerateFieldDictionary();
        IEnumerable<CaptionAndAliase> FormatMarkersAliases();
    }
}

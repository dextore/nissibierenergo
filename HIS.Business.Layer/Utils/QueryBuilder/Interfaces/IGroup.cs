﻿using System.Collections.Generic;
using HIS.Business.Layer.Utils.QueryBuilder.Models;

namespace HIS.Business.Layer.Utils.QueryBuilder.Interfaces
{
    public interface IGroup<T>
    {
        List<T> GroupList { get; set; }
        string FormatGroupQuery();
    }

    public interface IFilteringGroupOptionsSetter : IGroup<IGroupMember>
    {
        FilteringGroupOperator FilteringGroupOperatorSetter { get; set; }
    }
}
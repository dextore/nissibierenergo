﻿namespace HIS.Business.Layer.Utils.QueryBuilder.Interfaces
{
    public interface IGroupMember
    {
        string Field { get; set; }
        string FormatItemString();
        string FieldAliase { get; set; }
    }
}

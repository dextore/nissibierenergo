﻿using System.Collections.Generic;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.Models.Layer.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.Interfaces
{
    public interface IQueryBuilderService
    {
        IEnumerable<CaptionAndAliase> GetCaptionsAndAndAliases(int baTypeId);
        string GetData(QueryBuilderModel queryBuilderModel);
    }
}
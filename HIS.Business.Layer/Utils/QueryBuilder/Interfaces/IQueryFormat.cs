﻿using System.Collections.Generic;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.Models.Layer.Models.QueryBuilder;
using FilteringGroupOperator = HIS.Business.Layer.Utils.QueryBuilder.Models.FilteringGroupOperator;

namespace HIS.Business.Layer.Utils.QueryBuilder.Interfaces
{
    public interface IQueryFormat
    {
        IFieldGenerator FieldGenerator { get; set; }

        IQueryFormat Select(int baTypeId, IEnumerable<string> selectedPropertyes);
        IQueryFormat From(int baTypeId);
        IQueryFormat Union(bool needFirstLevelOfTypesHierarchy);
        IQueryFormat Where(IEnumerable<Filtering> filters, FilteringGroupOperator filteringGroupOperator = FilteringGroupOperator.And);
        IQueryFormat OrderBy(IEnumerable<Ordering> orderings);
        IQueryFormat OffsetTake(int from, int to);
        string BuildQuery();
        IQueryFormat Where(FilteringsTreeField filteringsTreeFields, FilteringGroupOperator filteringsGroupOperator);
    }
}

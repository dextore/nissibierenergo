﻿namespace HIS.Business.Layer.Utils.QueryBuilder.Interfaces
{
    public interface IPaging
    {
        string FormatPaggingQuery();
    }
}

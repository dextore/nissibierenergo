﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HIS.Business.Layer.Utils.QueryBuilder.Models;

namespace HIS.Business.Layer.Utils.QueryBuilder.Interfaces
{
    public abstract class Group<T> : IGroup<T>, IDisposable
        where T: IGroupMember
    {
        public List<T> GroupList { get; set; }
        protected string _sbResult;
        protected StringBuilder _stringBuilder;
        protected string _separator;
        protected QuerySection _section;
        
        public abstract void CreateQueryForSection();

        protected Group(QuerySection section, string separator)
        {
            GroupList = new List<T>();
            _section = section;
            _sbResult = string.Empty;
            _separator = separator;
        }
        public virtual string FormatGroupQuery()
        {
            _sbResult = string.Empty;
            _stringBuilder = new StringBuilder(QuerySectionEnum.ToFriendlyString(_section));

            if (CreateQueryForSingleMember())
                return _sbResult;
            
            CreateQueryForSection();
            
            _sbResult = TrimLastSeparatorAppend();

            if (_sbResult == "WH\n")
                _sbResult = string.Empty;

            return _sbResult;
        }
        
        protected virtual bool CreateQueryForSingleMember()
        {
            if (GroupList.Count == 1)
            {
                _stringBuilder.Append(GroupList.First().FormatItemString());
                _sbResult = _stringBuilder.ToString();

                return true;
            }

            return false;
        }

        protected virtual void GenerateStringForAllItemsInGroup(string lastString)
        {
            foreach (var member in GroupList)
            {
                var groupString = member.FormatItemString();
                groupString = groupString.Substring(0, groupString.Length - 1);
                _stringBuilder.Append(groupString + lastString);
            }
        }

        protected virtual string TrimLastSeparatorAppend()
        {
            return _stringBuilder.ToString()
                       .Substring(0, _stringBuilder.Length
                                     - "\n".Length
                                     - _separator.Length)
                   + "\n";
        }

        public void Dispose()
        {
            _stringBuilder.Clear();
        }

    }
}

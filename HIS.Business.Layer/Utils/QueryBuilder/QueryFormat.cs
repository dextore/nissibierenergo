﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using HIS.Business.Layer.Services;
using HIS.Business.Layer.Utils.QueryBuilder.Exceptions;
using HIS.Business.Layer.Utils.QueryBuilder.FromFormating;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.Business.Layer.Utils.QueryBuilder.SelectFormating;
using HIS.DAL.DB.Handlers.QueryBuilder;
using HIS.Models.Layer.Models.QueryBuilder;
using Unity;
using FilteringGroupOperator = HIS.Business.Layer.Utils.QueryBuilder.Models.FilteringGroupOperator;
using FiltersOperator = HIS.Business.Layer.Utils.QueryBuilder.Models.FiltersOperator;

namespace HIS.Business.Layer.Utils.QueryBuilder
{
    public class QueryFormat : ServiceBase, IQueryFormat
    {
        private BaseSelectGenerator _select;
        private BaseSelectGenerator _from;
        private IGroup<IGroupMember> _filteringGroup;
        private IGroup<IGroupMember> _orderingGroup;
        private IPaging _paging;
        private StringBuilder _query;
        private readonly IUnityContainer _resolver;
        private readonly IQueryBuilderHandler _queryBuilderHandler;

        private bool NeedFirstLevelOfTypesHierarchy { get; set; }
        private IEnumerable<string> _selectedPropertyes { get; set; }
        public IFieldGenerator FieldGenerator { get; set; }
        private bool _needCreateSelect = true;


        public QueryFormat(IUnityContainer resolver, IQueryBuilderHandler queryBuilderHandler) : base(resolver)
        {
            _query = new StringBuilder();
            _queryBuilderHandler = queryBuilderHandler;
            _resolver = resolver;
        }

        public IQueryFormat Select(int baTypeId, IEnumerable<string> selectedPropertyes)
        {
            _selectedPropertyes = selectedPropertyes;

            _select = _needCreateSelect
                ? new SelectSection(_resolver, baTypeId, _queryBuilderHandler, selectedPropertyes)
                : _select;
            
            FieldGenerator = new FieldGenerator(_resolver, baTypeId, _queryBuilderHandler, selectedPropertyes);
            FieldGenerator.GenerateFieldDictionary();

            _query.Append(_select.GenerateQuerysSection());
            return this;
        }

        public IQueryFormat From(int baTypeId)
        {
            _from = new FromSection(_resolver, baTypeId, _queryBuilderHandler, _selectedPropertyes);

            _query.Append(_from.GenerateQuerysSection());
            return this;
        }

        public IQueryFormat Union(bool needFirstLevelOfTypesHierarchy)
        {
            if (!needFirstLevelOfTypesHierarchy) return this;
            NeedFirstLevelOfTypesHierarchy = true;

            var queryList = new List<string>();

            foreach (var treeType in _from.TreeTypes.Where(x => x.TreeLevel > 0))
            {
                this._select.ReplaceEntityMvcAliase(treeType.MVCAlias);
                var tmpQueryBuilder = new QueryFormat(_resolver, _queryBuilderHandler)
                {
                    _needCreateSelect = false,
                    _select = this._select
                };

                var tmpQuery = tmpQueryBuilder.Select(treeType.BATypeId.Value, _selectedPropertyes)
                                              .From(treeType.BATypeId.Value)
                                              .BuildQuery();
                queryList.Add(tmpQuery);
            }
            var unionBuilder = new StringBuilder($"SELECT * FROM ({_query}");

            foreach (var newquery in queryList)
            {
                unionBuilder.Append("UNION\nSELECT\n")
                    .Append(newquery);
            }

            unionBuilder.Append(") AS t\n");
            _query = new StringBuilder(unionBuilder.ToString()); 
            return this;
        }

        public IQueryFormat Where(IEnumerable<Filtering> filters, FilteringGroupOperator filteringGroupOperator)
        {
            if (!filters.Any()) return this;

            foreach (var filtering in filters)
            {
                var tempField = NeedFirstLevelOfTypesHierarchy ? filtering.Field : FieldGenerator.GeneratedFieldDictionary.First(x => x.Key == filtering.Field).Value;

                var filter = new WhereSection(tempField, filtering.FilterOperator, filtering.Value, filtering.Field);
                if (_filteringGroup == null)
                    CreateFilteringGroup(filteringGroupOperator);
                else
                {
                    var current = ((IFilteringGroupOptionsSetter) _filteringGroup).FilteringGroupOperatorSetter;
                    if (current != filteringGroupOperator)
                        ((IFilteringGroupOptionsSetter) _filteringGroup).FilteringGroupOperatorSetter =
                            filteringGroupOperator;
                }
                _filteringGroup.GroupList.Add(filter);
            }

            _query.Append(_filteringGroup.FormatGroupQuery());
            return this;
        }

        public IQueryFormat Where(FilteringsTreeField filteringsTreeFields, FilteringGroupOperator filteringsGroupOperator)
        {
            var generator = new FilteringsTreeFieldGenerator();

            if (filteringsTreeFields.AndFields != null)
            {
                generator.dataAnd = new List<WhereSection>();

                foreach (var andField in filteringsTreeFields.AndFields)
                {
                    var tempField = NeedFirstLevelOfTypesHierarchy ? andField.Field : FieldGenerator.GeneratedFieldDictionary.First(x => x.Key == andField.Field).Value;
                    FiltersOperator prepareVaue;
                    HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.TryParse(andField.FilterOperator.ToString(), out prepareVaue);
                    var filter = new WhereSection(tempField, prepareVaue, andField.Value, andField.Field);

                    generator.dataAnd.Add(filter);
                }
            }

            if (filteringsTreeFields.OrFields != null)
            {
                generator.dataOr = new List<WhereSection>();

                foreach (var orFields in filteringsTreeFields.OrFields)
                {
                    var tempField = NeedFirstLevelOfTypesHierarchy ? orFields.Field : FieldGenerator.GeneratedFieldDictionary.First(x => x.Key == orFields.Field).Value;
                    FiltersOperator prepareVaue;
                    HIS.Models.Layer.Models.QueryBuilder.FiltersOperator.TryParse(orFields.FilterOperator.ToString(), out prepareVaue);
                    var filter = new WhereSection(tempField, prepareVaue, orFields.Value, orFields.Field);

                    generator.dataOr.Add(filter);
                }
            }


            _query.Append(generator.FormatGroupQuery());

            return this;
            //throw new System.NotImplementedException();
        }

        private void CreateFilteringGroup(FilteringGroupOperator filteringGroupOperator)
        {
            _filteringGroup = new FilteringGroup(filteringGroupOperator, QuerySection.Where);
        }

        public IQueryFormat OrderBy(IEnumerable<Ordering> orderings)
        {
            if (!orderings.Any()) return this;

            foreach (var ordering in orderings)
            {
                if (_orderingGroup == null)
                    CreateOrderingGroup();
                _orderingGroup.GroupList.Add(new OrderBySection(ordering.OrderingField, ordering.SortingOrder));
            }

            _query.Append(_orderingGroup.FormatGroupQuery());
            return this;
        }

        private void CreateOrderingGroup()
        {
            _orderingGroup = new OrderingGroup(QuerySection.OrderBy);
        }

        public IQueryFormat OffsetTake(int from, int to)
        {
            if (from == 0 && to == 0)
                return this;
            if (_orderingGroup == null)
                throw new OrderingMustBeBeforePaging("Paging can't be without Ordering");
            if (_paging != null)
                throw new PaggingAlreadyAdd("Paging can't be add when it is");
            _paging = new Paging(from, to);

            _query.Append(_paging.FormatPaggingQuery());
            return this;
        }

        public string BuildQuery()
        {
            var query = _query.ToString();

            _query.Clear();
            _orderingGroup?.GroupList?.RemoveAll(x => x != null);
            _filteringGroup?.GroupList?.RemoveAll(x => x != null);
            _paging = null;
            return query;
        }

        
    }
}

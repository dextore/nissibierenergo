﻿using System.Collections.Generic;
using System.Linq;
using HIS.Business.Layer.Services;
using HIS.DAL.Client.Models.QueryBuilder;
using HIS.DAL.DB.Handlers.QueryBuilder;
using Unity;

namespace HIS.Business.Layer.Utils.QueryBuilder
{
    public abstract class BaseSelectGenerator : ServiceBase
    {
        protected EntityQueryBuilderInfo _entitiy;
        protected IEnumerable<MarkersQueryBuilder> _markers;
        protected int _baTypeId;
        public IEnumerable<TreeTypes> TreeTypes;

        protected IQueryBuilderHandler _queryBuilderHandler;
        private IEnumerable<string> _selectedPropertyes { get; set; }

        public virtual string GenerateQuerysSection() => "";

        protected BaseSelectGenerator(IUnityContainer resolver, int baTypeId, IQueryBuilderHandler queryBuilderHandler, IEnumerable<string> selectedPropertyes)
            : base(resolver)
        {
            _queryBuilderHandler = queryBuilderHandler;
            _baTypeId = baTypeId;
            _selectedPropertyes = selectedPropertyes;

            TreeTypes = Exec(p => p.GetHandler<IQueryBuilderHandler>().GetTreeTypeses(baTypeId));


            _entitiy = Exec(p => p.GetHandler<IQueryBuilderHandler>().GetEntityQueryBuilderInfo(_baTypeId));
            _markers = IsSelectedPropertyesExist()
                ? Exec(p => p.GetHandler<IQueryBuilderHandler>()
                                .GetMarkersForBaTypeId(_baTypeId)
                                .Where(IsInSelectedPropertyes)
                                .ToList())
                : Exec(p => p.GetHandler<IQueryBuilderHandler>().GetMarkersForBaTypeId(_baTypeId));
        }

        public void ReplaceEntityMvcAliase(string newAliase)
        {
            _entitiy.MvcAlias = newAliase;
        }

        private bool IsSelectedPropertyesExist() => !(_selectedPropertyes == null || !_selectedPropertyes.Any());

        private bool IsInSelectedPropertyes (MarkersQueryBuilder marker)
        {
            return _selectedPropertyes.Contains($"[{_entitiy.MvcAlias}//{marker.MvcAlias}]")
                   || _selectedPropertyes.Contains($"[{_entitiy.MvcAlias}//{marker.MvcAlias}//ItemName]")
                   || _selectedPropertyes.Contains($"[{_entitiy.MvcAlias}//{marker.MvcAlias}//Item]");
        }
    }
}

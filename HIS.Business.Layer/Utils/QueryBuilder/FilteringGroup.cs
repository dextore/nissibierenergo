﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using FilteringGroupOperator = HIS.Business.Layer.Utils.QueryBuilder.Models.FilteringGroupOperator;

namespace HIS.Business.Layer.Utils.QueryBuilder
{
    public class FilteringGroup : Group<IGroupMember>, IFilteringGroupOptionsSetter
    {
        public FilteringGroup(FilteringGroupOperator filteringGroupOperator, QuerySection section)
            : base(section, filteringGroupOperator.ToString()) { }

        public override void CreateQueryForSection()
        {
            GenerateStringForAllItemsInGroup($" {_separator} ");
        }

        public FilteringGroupOperator FilteringGroupOperatorSetter
        {
            get { return this._separator == "And" ? FilteringGroupOperator.And : FilteringGroupOperator.Or; }
            set { this._separator = value.ToString(); }
        }
    }

    public class FilteringsTreeFieldGenerator
    {
        protected StringBuilder _stringBuilder;
        protected string _sbResult;
        protected string _separator;
        public List<WhereSection> dataOr { get; set; }
        public List<WhereSection> dataAnd { get; set; }

        public string FormatGroupQuery()
        {
            _sbResult = string.Empty;
            _stringBuilder = new StringBuilder(QuerySectionEnum.ToFriendlyString(QuerySection.Where));

            var orQuery = string.Empty;
            var andQuery = string.Empty;

            var orFieldsIsNotEmpty = (dataOr != null && dataOr.Any());
            var andFieldsIsNotEmpty = (dataAnd != null && dataAnd.Any());
            var allTypeFields = orFieldsIsNotEmpty && andFieldsIsNotEmpty;

            if (orFieldsIsNotEmpty)
            {
                //работаем с or полями 
                orQuery = FormatQueryForOrGroup();
                _stringBuilder.Append(orQuery);
            }
            if (andFieldsIsNotEmpty)
            {
                //работает с and полями 
                andQuery = FormatQueryForAndGroup();
                _stringBuilder.Append(andQuery);
            }

            if (allTypeFields)
            {
                //сделать or поля 
                //сделать and поля
                // объединить => return "({orFields}) and ({andFields})"
                _sbResult = $"WHERE ({orQuery}) and ({andQuery})\n";
            }
            else _sbResult = _stringBuilder.ToString();

            _stringBuilder.Clear();
            return _sbResult == "WHERE "? "" : _sbResult;
        }

        public string FormatQueryForOrGroup()
        {
            var result = new StringBuilder();
            var tmpRes = string.Empty;
            
            if (dataOr.Count() > 1)
            {
                foreach (var field in dataOr)
                {
                    result.Append(field.FormatItemString())
                        .Append(" OR ");
                }

                tmpRes = result.ToString();
                tmpRes = tmpRes.Substring(0, tmpRes.Length - 1 - 4);
            }
            else
            {
                result.Append(dataOr.First().FormatItemString());
                tmpRes = result.ToString();
            }

            result.Clear();

            return tmpRes;
        }

        public string FormatQueryForAndGroup()
        {
            var result = new StringBuilder();
            var tmpRes = string.Empty;

            if (dataAnd.Count() > 1)
            {
                foreach (var field in dataAnd)
                {
                    result.Append(field.FormatItemString()).Append(" AND ");
                }

                tmpRes = result.ToString();
                tmpRes = tmpRes.Substring(0, tmpRes.Length - 1 - 5);
            }
            else
            {
                result.Append(dataAnd.First().FormatItemString());
                tmpRes = result.ToString();
            }

            result.Clear();

            return tmpRes;
        }

    }
}
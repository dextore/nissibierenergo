﻿using System;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;
using HIS.Business.Layer.Utils.QueryBuilder.Models;

namespace HIS.Business.Layer.Utils.QueryBuilder
{
    public class WhereSection : IGroupMember
    {
        public string Field { get; set; }
        public string FieldAliase { get; set; }
        private object _value;

        private readonly FiltersOperator _filtersOperator;
        public WhereSection(string field, FiltersOperator filtersOperator, object value, string fieldAliase = "")
        {
            Field = field;
            _filtersOperator = filtersOperator;
            _value = value;
            FieldAliase = fieldAliase;
        }

        public string FormatItemString()
        {
            if (_value is string) _value = $"'{_value}'";
            if (_value is DateTime)
            {
                _value = $"{{d'{(DateTime)_value:yyyy-MM-dd}'}}";
                return $"ISNULL({Field}, \'1900-01-01\') {FilteringOperatorEnum.ToFriendlyString(_filtersOperator)} {_value}\n";
            }
            if (_value == null) _value = "''";
            if (_filtersOperator == FiltersOperator.Like)
                return $"{Field} LIKE '{string.Format(FilteringOperatorEnum.ToFriendlyString(_filtersOperator), _value.ToString().Replace("'",""))}' \n";
            return $"{Field} {FilteringOperatorEnum.ToFriendlyString(_filtersOperator)} {_value}\n";
        }
    }
}
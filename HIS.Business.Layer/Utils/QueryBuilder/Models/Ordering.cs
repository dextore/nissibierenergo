﻿namespace HIS.Business.Layer.Utils.QueryBuilder.Models
{
    public class Ordering
    {
        public string OrderingField { get; set; }
        public SortingOrder SortingOrder { get; set; }
    }
}

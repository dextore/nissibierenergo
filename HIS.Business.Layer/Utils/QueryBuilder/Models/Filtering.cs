﻿namespace HIS.Business.Layer.Utils.QueryBuilder.Models
{
    public class Filtering
    {
        public string Field { get; set; }
        public FiltersOperator FilterOperator { get; set; }
        public object Value { get; set; }
    }
}

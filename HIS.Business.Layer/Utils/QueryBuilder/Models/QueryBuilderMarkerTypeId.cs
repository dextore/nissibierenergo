﻿namespace HIS.Business.Layer.Utils.QueryBuilder.Models
{
    public enum QueryBuilderMarkerTypeId
    {
        Reference = 13,
        Address = 12,
        Link = 11,
        Enumeration = 5
    }
}

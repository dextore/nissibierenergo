﻿namespace HIS.Business.Layer.Utils.QueryBuilder.Models
{
    public enum QueryBuilderMarkerImplementation
    {
        /// <summary>
        /// вынесен
        /// </summary>
        Implement,
        /// <summary>
        /// не вынесен
        /// </summary>
        NoImplement,
        /// <summary>
        /// свалка
        /// </summary>
        Base,

        Unknown
    }
}

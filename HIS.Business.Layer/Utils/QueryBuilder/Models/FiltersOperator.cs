﻿using System;

namespace HIS.Business.Layer.Utils.QueryBuilder.Models
{
    public enum FiltersOperator
    {
        Equal,
        NoEqual,
        Less,
        More,
        LessEqual,
        MoreEqual,
        Like
    }

    public static class FilteringOperatorEnum
    {
        public static string ToFriendlyString(FiltersOperator filtersOperator)
        {
            switch (filtersOperator)
            {
                case FiltersOperator.Equal: return "=";
                case FiltersOperator.NoEqual: return "!=";
                case FiltersOperator.Less: return "<";
                case FiltersOperator.More: return ">";
                case FiltersOperator.LessEqual: return "<=";
                case FiltersOperator.MoreEqual: return ">=";
                case FiltersOperator.Like: return "%{0}%";
                default: throw new NotImplementedException("Filter has not implemented");
            }
        }
    }
}
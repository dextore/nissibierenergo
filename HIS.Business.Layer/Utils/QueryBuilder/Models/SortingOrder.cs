﻿namespace HIS.Business.Layer.Utils.QueryBuilder.Models
{ 
    public enum SortingOrder
    {
        Asc,
        Desc
    }
}
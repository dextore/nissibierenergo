﻿namespace HIS.Business.Layer.Utils.QueryBuilder.Models
{
    public class CaptionAndAliase
    {
        public string Aliase { get; set; }
        public string Caption { get; set; }
        public FrontEndDataType Type { get; set; }
        public int? MarkerId { get; set; }
        public int? MarkerTypeId { get; set; }
    }
}

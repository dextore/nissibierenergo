﻿namespace HIS.Business.Layer.Utils.QueryBuilder.Models
{
    public enum FrontEndDataType
    {
        Number,
        String,
        Boolean,
        Date
    }
}

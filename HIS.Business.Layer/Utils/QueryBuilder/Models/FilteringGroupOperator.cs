﻿namespace HIS.Business.Layer.Utils.QueryBuilder.Models
{
    public enum FilteringGroupOperator
    {
        And,
        Or
    }
}

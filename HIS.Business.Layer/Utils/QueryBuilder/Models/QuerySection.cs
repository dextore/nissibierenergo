﻿using System;

namespace HIS.Business.Layer.Utils.QueryBuilder.Models
{
    public enum QuerySection
    {
        OrderBy,
        Where,
        Select
    }

    public static class QuerySectionEnum
    {
        public static string ToFriendlyString(QuerySection querySection)
        {
            switch (querySection)
            {
                case QuerySection.OrderBy: return "ORDER BY ";
                case QuerySection.Where: return "WHERE ";
                case QuerySection.Select: return "SELECT ";
                default: throw new NotImplementedException("This section doesn't implemente");
            }
        }
    }
}

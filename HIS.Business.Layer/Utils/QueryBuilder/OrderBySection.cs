﻿using System;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;
using HIS.Business.Layer.Utils.QueryBuilder.Models;

namespace HIS.Business.Layer.Utils.QueryBuilder
{
    public class OrderBySection : IGroupMember
    {
        public string Field { get; set; }
        private readonly SortingOrder _sortingOrder;

        public OrderBySection(string orderingField, SortingOrder sortingOrder)
        {
            if(orderingField == string.Empty) throw new ArgumentException("Ordering field cant't be Empty");

            Field = orderingField;
            _sortingOrder = sortingOrder;
        }

       
        public string FormatItemString()
        {
            return $"{Field} {_sortingOrder}\n";
        }

        public string FieldAliase { get; set; }
    }
}
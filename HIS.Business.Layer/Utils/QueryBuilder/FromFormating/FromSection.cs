﻿using System.Collections.Generic;
using System.Text;
using HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator;
using HIS.DAL.DB.Handlers.QueryBuilder;
using Unity;
using Unity.Interception.Utilities;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating
{
    public class FromSection : BaseSelectGenerator
    {
        private readonly StringBuilder _fromBuilder;

        public FromSection(IUnityContainer resolver, int baTypeId, IQueryBuilderHandler queryBuilderHandler,
            IEnumerable<string> selectedPropertyes)
            : base(resolver, baTypeId, queryBuilderHandler, selectedPropertyes)
        {
            _fromBuilder = new StringBuilder($"FROM {_entitiy.ImplementTypeName} AS [{_entitiy.MvcAlias}]\n");
        }

        public override string GenerateQuerysSection()
        {
            _markers.ForEach(marker => _fromBuilder.Append(FromMarker.Generate(marker, _entitiy)));
            _fromBuilder.Append(FromMarker.AddServiceFilds(_entitiy, _baTypeId));
            var res = _fromBuilder.ToString();

            _fromBuilder.Clear();
            return res;
        }

    }
}

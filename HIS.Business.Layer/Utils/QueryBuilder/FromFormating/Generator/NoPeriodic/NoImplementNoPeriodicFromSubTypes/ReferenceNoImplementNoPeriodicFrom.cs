﻿using System;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic.NoImplementNoPeriodicFromSubTypes
{
    public class ReferenceNoImplementNoPeriodicFrom : NoImplementNoPeriodicFrom
    {
        public ReferenceNoImplementNoPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            if (string.IsNullOrEmpty(_marker.MarkerView))
                throw new Exception($"marker.MarkerView IsNullOrEmpty value: {_marker.MarkerView}");

            return 
                $"LEFT JOIN [{_marker.MarkerView}] AS [ViewMV{_marker.MarkerId}]\n" +
                $"\tON [{_entity.MvcAlias}].{_marker.ImplementTypeField} = [ViewMV{_marker.MarkerId}].ItemId\n";
        }
    }
}

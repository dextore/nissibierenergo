﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic.NoImplementNoPeriodicFromSubTypes
{
    public class SampleNoImplementNoPeriodicFrom : NoImplementNoPeriodicFrom
    {
        public SampleNoImplementNoPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            return $"LEFT JOIN [{_marker.ImplementTypeName}] AS [MV{_marker.MarkerId}]\n" +
            $"\tON [MV{_marker.MarkerId}].BAId = [{_entity.MvcAlias}].BAId\n";
        }
    }
}

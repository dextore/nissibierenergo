﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic.NoImplementNoPeriodicFromSubTypes
{
    public class LinkNoImplementNoPeriodicFrom : NoImplementNoPeriodicFrom
    {
        public LinkNoImplementNoPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            return
                $"LEFT JOIN [{_marker.ImplementTypeName}] AS [MV{_marker.MarkerId}]\n" +
                $"\tON [MV{_marker.MarkerId}].BAId = [{_entity.MvcAlias}].BAId\n" +

                $"LEFT JOIN [{_marker.LinkedBaTypeImplaiment}] AS [LinkMV{_marker.MarkerId}-{_marker.LinkedBaTypeImplaiment}]\n" +
                $"\tON [MV{_marker.MarkerId}].{_marker.ImplementTypeField} = [LinkMV{_marker.MarkerId}-{_marker.LinkedBaTypeImplaiment}].BAId\n";
        }
    }
}

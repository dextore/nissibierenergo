﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic.ImplaimentNoPeriodicFromSubTypes
{
    public class SampleImplementNoPeriodicFrom : ImplementNoPeriodicFrom
    {
        public SampleImplementNoPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            return 
                $"LEFT JOIN [{_marker.ImplementTypeName}] AS [MV{_marker.MarkerId}]\n" +
                $"\tON [MV{_marker.MarkerId}].BAId = [{_entity.MvcAlias}].BAId\n" +
                $"\tAND [MV{_marker.MarkerId}].MarkerId = {_marker.MarkerId}\n";
        }
    }
}

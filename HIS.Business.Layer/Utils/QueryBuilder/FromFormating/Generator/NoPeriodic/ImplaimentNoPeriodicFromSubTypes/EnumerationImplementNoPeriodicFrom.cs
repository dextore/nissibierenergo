﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic.ImplaimentNoPeriodicFromSubTypes
{
    public class EnumerationImplementNoPeriodicFrom : ImplementNoPeriodicFrom
    {
        public EnumerationImplementNoPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            return            
                $"LEFT JOIN [{_marker.ImplementTypeName}] AS [MV{_marker.MarkerId}]\n" +
                $"\tON [MV{_marker.MarkerId}].BAId = [{_entity.MvcAlias}].BAId\n" +
                $"\tAND [MV{_marker.MarkerId}].MarkerId = {_marker.MarkerId}\n" +

                $"LEFT JOIN [MAMarkerValueList] as [ValuesMV{_marker.MarkerId}]\n" +
                $"\tON [ValuesMV{_marker.MarkerId}].MarkerId = {_marker.MarkerId}\n" +
                $"\tAND [ValuesMV{_marker.MarkerId}].ItemId = [MV{_marker.MarkerId}].sValue\n";
        }
    }
}

﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic.BaseImplaimentNoPeriodicFromSubTypes
{
    public class SampleBaseImplaimentNoPeriodicFrom : BaseImplaimentNoPeriodicFrom
    {
        public SampleBaseImplaimentNoPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            return 
                $"LEFT JOIN [MABaseAncestorMarkerValues] AS [MVB{_marker.MarkerId}]\n" +
                $"\tON [MVB{_marker.MarkerId}].BAId = [{_entity.MvcAlias}].BAId\n" +
                $"\tAND [MVB{_marker.MarkerId}].MarkerId = {_marker.MarkerId}\n";
        }
    }
}

﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic.BaseImplaimentNoPeriodicFromSubTypes
{
    public class AddressBaseImplaimentNoPeriodicFrom : BaseImplaimentNoPeriodicFrom
    {
        public AddressBaseImplaimentNoPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            return 
                $"LEFT JOIN [MABaseAncestorMarkerValuePeriods] AS [MVB{_marker.MarkerId}]\n" +
                $"\tON [MVB{_marker.MarkerId}].BAId = [{_entity.MvcAlias}].BAId\n" +
                $"\tAND [MVB{_marker.MarkerId}].MarkerId = {_marker.MarkerId}" +

                $"LEFT JOIN [{_marker.LinkedBaTypeImplaiment}] AS [LinkMVB{_marker.MarkerId}-{_marker.LinkedBaTypeImplaiment}]\n" +
                $"\tON [MVB{_marker.MarkerId}].{_marker.ImplementTypeField} = [LinkMVB{_marker.MarkerId}-{_marker.LinkedBaTypeImplaiment}].BAId\n";
        }
    }
}

﻿using System;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic.BaseImplaimentNoPeriodicFromSubTypes
{
    public class ReferenceBaseImplaimentNoPeriodicFrom : BaseImplaimentNoPeriodicFrom
    {
        public ReferenceBaseImplaimentNoPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            if (string.IsNullOrEmpty(_marker.MarkerView))
                throw new Exception($"marker.MarkerView IsNullOrEmpty value: {_marker.MarkerView}");

            return 
                $"LEFT JOIN [MABaseAncestorMarkerValues] AS [MVB{_marker.MarkerId}]\n" +
                $"\tON [MVB{_marker.MarkerId}].BAId = [{_entity.MvcAlias}].BAId\n" +
                $"\tAND [MVB{_marker.MarkerId}].MarkerId = {_marker.MarkerId}\n" +

                $"LEFT JOIN [{_marker.MarkerView}] AS [ViewMVB{_marker.MarkerId}]\n" +
                $"\tON [MVB{_marker.MarkerId}].sValue = [ViewMVB{_marker.MarkerId}].ItemId\n";
        }
    }
}

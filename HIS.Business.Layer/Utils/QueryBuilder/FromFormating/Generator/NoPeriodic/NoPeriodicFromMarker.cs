﻿using System;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic
{
    public class NoPeriodicFromMarker : FromMarkerStrategy
    {
        public NoPeriodicFromMarker(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity)
        {
            _marker = marker;
            _entity = entity;
        }

        public override string FormatFromQuery()
        {
            switch (GetMarkerImplementation())
            {
                case QueryBuilderMarkerImplementation.Implement: return new ImplementNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerImplementation.NoImplement: return new NoImplementNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerImplementation.Base: return new BaseImplaimentNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerImplementation.Unknown: throw new NotImplementedException();
                default: throw new NotImplementedException();
            }
        }
    }
}

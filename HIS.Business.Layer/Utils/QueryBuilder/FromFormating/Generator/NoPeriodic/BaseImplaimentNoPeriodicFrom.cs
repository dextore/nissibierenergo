﻿using HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic.BaseImplaimentNoPeriodicFromSubTypes;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic
{
    public class BaseImplaimentNoPeriodicFrom : NoPeriodicFromMarker
    {
        public BaseImplaimentNoPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            switch ((QueryBuilderMarkerTypeId)_marker.MarkerTypeId)
            {
                case QueryBuilderMarkerTypeId.Reference: return new ReferenceBaseImplaimentNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Address: return new AddressBaseImplaimentNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Link: return new LinkBaseImplaimentNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Enumeration: return new EnumerationBaseImplaimentNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                default: return new SampleBaseImplaimentNoPeriodicFrom(_marker, _entity).FormatFromQuery();
            }
        }
    }
}

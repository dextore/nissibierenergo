﻿using HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic.NoImplementNoPeriodicFromSubTypes;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic
{
    public class NoImplementNoPeriodicFrom : NoPeriodicFromMarker
    {
        public NoImplementNoPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            switch ((QueryBuilderMarkerTypeId)_marker.MarkerTypeId)
            {
                case QueryBuilderMarkerTypeId.Reference: return new ReferenceNoImplementNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Address: return new AddressNoImplementNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Link: return new LinkNoImplementNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Enumeration: return new EnumerationNoImplementNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                default: return new SampleNoImplementNoPeriodicFrom(_marker, _entity).FormatFromQuery();
            }
        }
    }
}

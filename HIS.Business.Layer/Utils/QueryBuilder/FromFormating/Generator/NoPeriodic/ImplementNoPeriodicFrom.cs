﻿using HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic.ImplaimentNoPeriodicFromSubTypes;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic
{
    public class ImplementNoPeriodicFrom : NoPeriodicFromMarker
    {
        public ImplementNoPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            switch ((QueryBuilderMarkerTypeId)_marker.MarkerTypeId)
            {
                case QueryBuilderMarkerTypeId.Reference: return new ReferenceImplementNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Address: return new AddressImplementNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Link: return new LinkImplementNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Enumeration: return new EnumerationImplementNoPeriodicFrom(_marker, _entity).FormatFromQuery();
                default: return new SampleImplementNoPeriodicFrom(_marker, _entity).FormatFromQuery();
            }
        }
    }
}

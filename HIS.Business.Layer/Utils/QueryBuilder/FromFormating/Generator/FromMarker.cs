﻿using HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.NoPeriodic;
using HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.Periodic;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator
{
    public static class FromMarker
    {
        public static string Generate(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity)
        {
            return marker.IsPeriodic
                ? new PeriodicFromMarker(marker, entity).FormatFromQuery()
                : new NoPeriodicFromMarker(marker, entity).FormatFromQuery();
        }
        
        public static string AddServiceFilds(EntityQueryBuilderInfo entity, int baTypeId)
        {
            return 
                $"LEFT JOIN [SOStates] AS [states]\n" +
                $"\tON [states].StateId = [{entity.MvcAlias}].StateId\n" +

                $"LEFT JOIN SOBATypeStates AS [{entity.MvcAlias}-SOBATypeStates]\n" +
                $"\tON [{entity.MvcAlias}-SOBATypeStates].StateId =  [states].StateId\n" +
                $"\tAND [{entity.MvcAlias}-SOBATypeStates].BATypeId = {baTypeId}\n";
        }
    }
}

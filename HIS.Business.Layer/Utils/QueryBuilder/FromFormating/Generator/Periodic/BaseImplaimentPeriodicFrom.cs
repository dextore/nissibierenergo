﻿using HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.Periodic.BaseImplaimentPeriodicFromSubTypes;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.Periodic
{
    public class BaseImplaimentPeriodicFrom : PeriodicFromMarker
    {
        public BaseImplaimentPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            switch ((QueryBuilderMarkerTypeId)_marker.MarkerTypeId)
            {
                case QueryBuilderMarkerTypeId.Reference: return new ReferenceBaseImplaimentPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Address: return new AddressBaseImplaimentPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Link: return new LinkBaseImplaimentPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Enumeration: return new EnumerationBaseImplaimentPeriodicFrom(_marker, _entity).FormatFromQuery();
                default: return new SampleBaseImplaimentPeriodicFrom(_marker, _entity).FormatFromQuery();
            }
        }
    }
}

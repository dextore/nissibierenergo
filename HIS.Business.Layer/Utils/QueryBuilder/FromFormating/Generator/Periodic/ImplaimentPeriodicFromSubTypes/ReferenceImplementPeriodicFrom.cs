﻿using System;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.Periodic.ImplaimentPeriodicFromSubTypes
{
    public class ReferenceImplementPeriodicFrom : ImplementPeriodicFrom
    {
        public ReferenceImplementPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            if (string.IsNullOrEmpty(_marker.MarkerView))
                throw new Exception($"marker.MarkerView IsNullOrEmpty value: {_marker.MarkerView}");

            return 
                $"LEFT JOIN [{_marker.ImplementTypeName}] AS [MVP{_marker.MarkerId}]\n" +
                $"\tON [MVP{_marker.MarkerId}].BAId = [{_entity.MvcAlias}].BAId\n" +
                $"\tAND [MVP{_marker.MarkerId}].MarkerId = {_marker.MarkerId}" +
                $"\tAND [MVP{_marker.MarkerId}].StartDate <= {"{d '" + DateTime.Now.ToString("yyyy-MM-dd") + "'}"}\n" +
                $"\tAND [MVP{_marker.MarkerId}].EndDate > {"{d '" + DateTime.Now.ToString("yyyy-MM-dd") + "'}"}\n" +
         
                $"LEFT JOIN [{_marker.MarkerView}] AS [ViewMVP{_marker.MarkerId}]\n" +
                $"\tON [MVP{_marker.MarkerId}].{_marker.ImplementTypeField} = [ViewMVP{_marker.MarkerId}].ItemId\n";
        }
    }
}

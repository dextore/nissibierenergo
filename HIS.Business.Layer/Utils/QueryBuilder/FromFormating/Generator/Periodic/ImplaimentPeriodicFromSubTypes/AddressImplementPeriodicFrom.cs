﻿using System;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.Periodic.ImplaimentPeriodicFromSubTypes
{
    public class AddressImplementPeriodicFrom : ImplementPeriodicFrom
    {
        public AddressImplementPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            return 
                $"LEFT JOIN [{_marker.ImplementTypeName}] AS [MVP{_marker.MarkerId}]\n" +
                $"\tON [MVP{_marker.MarkerId}].BAId = [{_entity.MvcAlias}].BAId\n" +
                $"\tAND [MVP{_marker.MarkerId}].MarkerId = {_marker.MarkerId}" + 
                $"\tAND [MVP{_marker.MarkerId}].StartDate <= {"{d '" + DateTime.Now.ToString("yyyy-MM-dd") + "'}"}\n" +
                $"\tAND [MVP{_marker.MarkerId}].EndDate > {"{d '" + DateTime.Now.ToString("yyyy-MM-dd") + "'}"}\n" +

                $"LEFT JOIN [{_marker.LinkedBaTypeImplaiment}] AS [LinkMVP{_marker.MarkerId}-{_marker.LinkedBaTypeImplaiment}]\n" +
                $"\tON [MVP{_marker.MarkerId}].{_marker.ImplementTypeField} = [LinkMVP{_marker.MarkerId}-{_marker.LinkedBaTypeImplaiment}].BAId\n";
        }
    }
}

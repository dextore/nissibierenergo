﻿using System;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.Periodic.BaseImplaimentPeriodicFromSubTypes
{
    public class SampleBaseImplaimentPeriodicFrom : BaseImplaimentPeriodicFrom
    {
        public SampleBaseImplaimentPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatFromQuery()
        {
            return 
                $"LEFT JOIN [MABaseAncestorMarkerValuePeriods] AS [MVPB{_marker.MarkerId}]\n" +
                $"\tON [MVPB{_marker.MarkerId}].BAId = [{_entity.MvcAlias}].BAId\n" +
                $"\tAND [MVPB{_marker.MarkerId}].MarkerId = {_marker.MarkerId}\n" +
                $"\tAND [MVPB{_marker.MarkerId}].StartDate >= {"{d '" + DateTime.Now.ToString("yyyy-MM-dd") + "'}"}\n" +
                $"\tAND [MVPB{_marker.MarkerId}].EndDate < {"{d '" + DateTime.Now.ToString("yyyy-MM-dd") + "'}"}\n";
        }
    }

}

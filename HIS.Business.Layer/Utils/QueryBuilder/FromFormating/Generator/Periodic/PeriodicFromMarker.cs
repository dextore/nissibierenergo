﻿using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.Periodic
{
    public class PeriodicFromMarker : FromMarkerStrategy
    {
        public PeriodicFromMarker(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity)
        {
            _marker = marker;
            _entity = entity;
        }

        public override string FormatFromQuery()
        {
            switch (GetMarkerImplementation())
            {
                case QueryBuilderMarkerImplementation.Implement: return new ImplementPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerImplementation.Base: return new BaseImplaimentPeriodicFrom(_marker, _entity).FormatFromQuery();
                default: return "";
            }
        }
    }

    

    
}

﻿using HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.Periodic.ImplaimentPeriodicFromSubTypes;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.FromFormating.Generator.Periodic
{
    public class ImplementPeriodicFrom : PeriodicFromMarker
    {
        public ImplementPeriodicFrom(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }


        public override string FormatFromQuery()
        {
            switch ((QueryBuilderMarkerTypeId)_marker.MarkerTypeId)
            {
                case QueryBuilderMarkerTypeId.Reference: return new ReferenceImplementPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Address: return new AddressImplementPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Link: return new LinkImplementPeriodicFrom(_marker, _entity).FormatFromQuery();
                case QueryBuilderMarkerTypeId.Enumeration: return new EnumerationImplementPeriodicFrom(_marker, _entity).FormatFromQuery();
                default: return new SampleImplementPeriodicFrom(_marker, _entity).FormatFromQuery();
            }
        }
    }
}

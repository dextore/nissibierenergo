﻿using System.Collections.Generic;
using System.Text;
using HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator;
using HIS.DAL.DB.Handlers.QueryBuilder;
using Unity;
using Unity.Interception.Utilities;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating
{
    public class SelectSection : BaseSelectGenerator
    {
        private readonly StringBuilder _selectBuilder = new StringBuilder("SELECT\n\t");
        
        public SelectSection(IUnityContainer resolver, int baTypeId, IQueryBuilderHandler queryBuilderHandler, IEnumerable<string> selectedPropertyes)
            : base(resolver, baTypeId, queryBuilderHandler, selectedPropertyes)
        {
        }

        public override string GenerateQuerysSection()
        {
            _selectBuilder.Append(SelectMarker.AddBaidServiceFilds(_entitiy));
            _markers.ForEach(marker => _selectBuilder.Append(SelectMarker.Generate(marker, _entitiy)));
            _selectBuilder.Append(SelectMarker.AddServiceFilds(_entitiy));

            var result = _selectBuilder.ToString();

            _selectBuilder.Clear();
            return result;
        }
    }
}
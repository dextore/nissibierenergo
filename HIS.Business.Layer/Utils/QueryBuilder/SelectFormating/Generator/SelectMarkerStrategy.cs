﻿using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator
{
    public abstract class SelectMarkerStrategy
    {
        protected MarkersQueryBuilder _marker;
        protected EntityQueryBuilderInfo _entity;

        public QueryBuilderMarkerImplementation GetMarkerImplementation()
        {
            if (_marker.ImplementTypeName != _entity.ImplementTypeName 
                && !string.IsNullOrEmpty(_marker.ImplementTypeName))
                return QueryBuilderMarkerImplementation.Implement;
            if (string.IsNullOrEmpty(_marker.ImplementTypeName))
                return QueryBuilderMarkerImplementation.Base;
            else return QueryBuilderMarkerImplementation.NoImplement;
        }

        public abstract string FormatSelectQuery();
    }
}

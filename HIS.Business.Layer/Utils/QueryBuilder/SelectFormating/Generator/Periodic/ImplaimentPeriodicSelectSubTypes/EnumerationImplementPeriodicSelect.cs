﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.Periodic.ImplaimentPeriodicSelectSubTypes
{
    public class EnumerationImplementPeriodicSelect : ImplementPeriodicSelect
    {
        public EnumerationImplementPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            return
                $"[MVP{_marker.MarkerId}].{_marker.ImplementTypeField} AS [{_entity.MvcAlias}//{_marker.MvcAlias}],\n\t" +
                $"[ValuesMVP{_marker.MarkerId}].ItemName AS [{_entity.MvcAlias}//{_marker.MvcAlias}//ItemName],\n\t";
        }
    }
}

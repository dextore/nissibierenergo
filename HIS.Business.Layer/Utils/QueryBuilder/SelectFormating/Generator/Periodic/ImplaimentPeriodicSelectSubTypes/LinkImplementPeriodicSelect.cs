﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.Periodic.ImplaimentPeriodicSelectSubTypes
{
    public class LinkImplementPeriodicSelect : ImplementPeriodicSelect
    {
        public LinkImplementPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            return $"[MVP{_marker.MarkerId}].sValue AS [{_entity.MvcAlias}//{_marker.MvcAlias}],\n\t" +
                   $"[LinkMVP{_marker.MarkerId}-{_marker.LinkedBaTypeImplaiment}].{_marker.LinkedBaTypeFieldName ?? "Name"} AS [{_entity.MvcAlias}//{_marker.MvcAlias}//{_marker.LinkedBaTypeFieldName ?? "Name"}],\n\t";
        }
    }
}

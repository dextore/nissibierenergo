﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.Periodic.ImplaimentPeriodicSelectSubTypes
{
    public class AddressImplementPeriodicSelect : ImplementPeriodicSelect
    {
        public AddressImplementPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            return $"[MVP{_marker.MarkerId}].sValue AS [{_entity.MvcAlias}//{_marker.MvcAlias}],\n\t" +
                   $"[LinkMVP{_marker.MarkerId}-{_marker.LinkedBaTypeImplaiment}].Name AS [{_entity.MvcAlias}//{_marker.MvcAlias}//Name],\n\t";
        }
    }
}

﻿using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.Periodic.ImplaimentPeriodicSelectSubTypes;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.Periodic
{
    public class ImplementPeriodicSelect : PeriodicSelectMarker
    {
        public ImplementPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            switch ((QueryBuilderMarkerTypeId)_marker.MarkerTypeId)
            {
                case QueryBuilderMarkerTypeId.Reference:   return new ReferenceImplementPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Address:     return new AddressImplementPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Link:        return new LinkImplementPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Enumeration: return new EnumerationImplementPeriodicSelect(_marker, _entity).FormatSelectQuery();
                default:                                   return new SampleImplementPeriodicSelect(_marker, _entity).FormatSelectQuery();
            }
        }
    }
}

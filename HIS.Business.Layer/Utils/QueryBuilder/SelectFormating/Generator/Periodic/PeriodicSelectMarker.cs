﻿using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.Periodic
{
    public class PeriodicSelectMarker : SelectMarkerStrategy
    {
        public PeriodicSelectMarker(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity)
        {
            _marker = marker;
            _entity = entity;
        }

        public override string FormatSelectQuery()
        {
            switch (GetMarkerImplementation())
            {
                case QueryBuilderMarkerImplementation.Implement: return new ImplementPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerImplementation.Base: return new BaseImplaimentPeriodicSelect(_marker, _entity).FormatSelectQuery();
                default: return "";
            }
        }
    }
}

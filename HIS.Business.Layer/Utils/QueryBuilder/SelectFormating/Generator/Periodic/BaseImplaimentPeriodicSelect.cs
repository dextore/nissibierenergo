﻿using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.Periodic.BaseImplaimentPeriodicSelectSubTypes;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.Periodic
{
    public class BaseImplaimentPeriodicSelect : PeriodicSelectMarker
    {
        public BaseImplaimentPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            switch ((QueryBuilderMarkerTypeId)_marker.MarkerTypeId)
            {
                case QueryBuilderMarkerTypeId.Reference: return new ReferenceBaseImplaimentPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Address: return new AddressBaseImplaimentPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Link: return new LinkBaseImplaimentPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Enumeration: return new EnumerationBaseImplaimentPeriodicSelect(_marker, _entity).FormatSelectQuery();
                default: return new SampleBaseImplaimentPeriodicSelect(_marker, _entity).FormatSelectQuery();
            }
        }
    }
}

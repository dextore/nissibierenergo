﻿using System;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.Periodic.BaseImplaimentPeriodicSelectSubTypes
{
    public class ReferenceBaseImplaimentPeriodicSelect : BaseImplaimentPeriodicSelect
    {
        public ReferenceBaseImplaimentPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            if (string.IsNullOrEmpty(_marker.MarkerView))
                throw new Exception($"marker.MarkerView IsNullOrEmpty value: {_marker.MarkerView}");

            return $"[MVPB{_marker.MarkerId}].sValue AS [{_entity.MvcAlias}//{_marker.MvcAlias}],\n\t" +
                   //$"[ViewMVPB{_marker.MarkerId}].ItemName AS [{_entity.MvcAlias}//{_marker.MvcAlias}//ItemName],\n\t";
                   $"[ViewMVPB{_marker.MarkerId}].ItemName AS [{_entity.MvcAlias}//{_marker.ImplementTypeField}//ItemName],\n\t";
        }
    }
}

﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.Periodic.BaseImplaimentPeriodicSelectSubTypes
{
    public class EnumerationBaseImplaimentPeriodicSelect : BaseImplaimentPeriodicSelect
    {
        public EnumerationBaseImplaimentPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            return
                $"[MVPB{_marker.MarkerId}].{_marker.ImplementTypeField} AS [{_entity.MvcAlias}//{_marker.MvcAlias}],\n\t" +
                $"[ValuesMVPB{_marker.MarkerId}].ItemName AS [{_entity.MvcAlias}//{_marker.MvcAlias}//ItemName],\n\t";
        }
    }
}

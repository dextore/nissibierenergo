﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.Periodic.BaseImplaimentPeriodicSelectSubTypes
{
    public class SampleBaseImplaimentPeriodicSelect : BaseImplaimentPeriodicSelect
    {
        public SampleBaseImplaimentPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            return $"[MVPB{_marker.MarkerId}].sValue AS [{_entity.MvcAlias}//{_marker.MvcAlias}],\n\t";
        }
    }

}

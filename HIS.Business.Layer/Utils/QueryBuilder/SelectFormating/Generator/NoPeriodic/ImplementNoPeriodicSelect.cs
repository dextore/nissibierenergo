﻿using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic.ImplaimentNoPeriodicSelectSubTypes;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic
{
    public class ImplementNoPeriodicSelect : NoPeriodicSelectMarker
    {
        public ImplementNoPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            switch ((QueryBuilderMarkerTypeId)_marker.MarkerTypeId)
            {
                case QueryBuilderMarkerTypeId.Reference: return new ReferenceImplementNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Address: return new AddressImplementNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Link: return new LinkImplementNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Enumeration: return new EnumerationImplementNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                default: return new SampleImplementNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
            }
        }
    }
}

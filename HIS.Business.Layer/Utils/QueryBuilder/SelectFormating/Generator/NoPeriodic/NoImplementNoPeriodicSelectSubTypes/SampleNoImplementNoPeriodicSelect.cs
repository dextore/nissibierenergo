﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic.BaseImplaimentNoPeriodicSelectSubTypes
{
    public class SampleNoImplementNoPeriodicSelect : NoImplementNoPeriodicSelect
    {
        public SampleNoImplementNoPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            return $"[{_entity.MvcAlias}].{_marker.ImplementTypeField} AS [{_entity.MvcAlias}//{_marker.MvcAlias}],\n\t";
        }
    }
}

﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic.BaseImplaimentNoPeriodicSelectSubTypes
{
    public class LinkNoImplementNoPeriodicSelect : NoImplementNoPeriodicSelect
    {
        public LinkNoImplementNoPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            return $"[MV{_marker.MarkerId}].{_marker.ImplementTypeField} AS [{_entity.MvcAlias}//{_marker.MvcAlias}],\n\t" +
                   $"[LinkMV{_marker.MarkerId}-{_marker.LinkedBaTypeImplaiment}].{_marker.LinkedBaTypeFieldName ?? "Name"}  AS [{_entity.MvcAlias}//{_marker.MvcAlias}//{_marker.LinkedBaTypeFieldName ?? "Name"}],\n\t";
        }
    }
}

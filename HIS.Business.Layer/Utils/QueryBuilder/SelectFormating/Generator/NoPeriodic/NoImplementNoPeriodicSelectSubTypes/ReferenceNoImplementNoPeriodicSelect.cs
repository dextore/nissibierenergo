﻿using System;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic.BaseImplaimentNoPeriodicSelectSubTypes
{
    public class ReferenceNoImplementNoPeriodicSelect : NoImplementNoPeriodicSelect
    {
        public ReferenceNoImplementNoPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            if (string.IsNullOrEmpty(_marker.MarkerView))
                throw new Exception($"marker.MarkerView IsNullOrEmpty value: {_marker.MarkerView}");

            return
                $"[{_entity.MvcAlias}].{_marker.ImplementTypeField} AS [{_entity.MvcAlias}//{_marker.ImplementTypeField}],\n\t" +
                $"[ViewMV{_marker.MarkerId}].ItemName AS [{_entity.MvcAlias}//{_marker.ImplementTypeField}//ItemName],\n\t";
        }
    }
}

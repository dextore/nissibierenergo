﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic.BaseImplaimentNoPeriodicSelectSubTypes
{
    public class AddressNoImplementNoPeriodicSelect : NoImplementNoPeriodicSelect
    {
        public AddressNoImplementNoPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            return $"[MV{_marker.MarkerId}].{_marker.ImplementTypeField} AS [{_entity.MvcAlias}//{_marker.MvcAlias}],\n\t" +
                   $"[LinkMV{_marker.MarkerId}-{_marker.LinkedBaTypeImplaiment}].Name AS [{_entity.MvcAlias}//{_marker.MvcAlias}//Name],\n\t";
        }
    }
}

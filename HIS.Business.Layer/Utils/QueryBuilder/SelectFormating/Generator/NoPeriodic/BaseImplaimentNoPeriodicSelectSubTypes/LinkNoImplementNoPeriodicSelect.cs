﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic.BaseImplaimentNoPeriodicSelectSubTypes
{
    public class LinkBaseImplaimentNoPeriodicSelect : BaseImplaimentNoPeriodicSelect
    {
        public LinkBaseImplaimentNoPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            return $"[MVB{_marker.MarkerId}].sValue AS [{_entity.MvcAlias}//{_marker.MvcAlias}],\n\t" +
                   $"[LinkMVB{_marker.MarkerId}-{_marker.LinkedBaTypeImplaiment}].{_marker.LinkedBaTypeFieldName ?? "Name"} AS [{_entity.MvcAlias}//{_marker.MvcAlias}//{_marker.LinkedBaTypeFieldName ?? "Name"}],\n\t";
        }
    }
}

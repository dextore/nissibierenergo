﻿using System;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic.BaseImplaimentNoPeriodicSelectSubTypes
{
    public class ReferenceBaseImplaimentNoPeriodicSelect : BaseImplaimentNoPeriodicSelect
    {
        public ReferenceBaseImplaimentNoPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            if (string.IsNullOrEmpty(_marker.MarkerView))
                throw new Exception($"marker.MarkerView IsNullOrEmpty value: {_marker.MarkerView}");

            return $"[MVB{_marker.MarkerId}].sValue as [{_entity.MvcAlias}//{_marker.MvcAlias}],\n\t" +
                   $"[ViewMVB{_marker.MarkerId}].ItemName AS [{_entity.MvcAlias}//{_marker.ImplementTypeField}//ItemName],\n\t";
        }
    }
}

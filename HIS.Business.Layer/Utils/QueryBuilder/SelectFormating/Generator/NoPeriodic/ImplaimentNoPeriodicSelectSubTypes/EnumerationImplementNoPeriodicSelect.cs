﻿using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic.ImplaimentNoPeriodicSelectSubTypes
{
    public class EnumerationImplementNoPeriodicSelect : ImplementNoPeriodicSelect
    {
        public EnumerationImplementNoPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            return
                $"[MV{_marker.MarkerId}].{_marker.ImplementTypeField} AS [{_entity.MvcAlias}//{_marker.MvcAlias}],\n\t" +
                $"[ValuesMV{_marker.MarkerId}].ItemName AS [{_entity.MvcAlias}//{_marker.MvcAlias}//ItemName],\n\t";
        }
    }
}

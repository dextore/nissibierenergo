﻿using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic.BaseImplaimentNoPeriodicSelectSubTypes;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic
{
    public class BaseImplaimentNoPeriodicSelect : NoPeriodicSelectMarker
    {
        public BaseImplaimentNoPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            switch ((QueryBuilderMarkerTypeId)_marker.MarkerTypeId)
            {
                case QueryBuilderMarkerTypeId.Reference: return new ReferenceBaseImplaimentNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Address: return new AddressBaseImplaimentNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Link: return new LinkBaseImplaimentNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Enumeration: return new EnumerationBaseImplaimentNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                default: return new SampleBaseImplaimentNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
            }
        }
    }
}

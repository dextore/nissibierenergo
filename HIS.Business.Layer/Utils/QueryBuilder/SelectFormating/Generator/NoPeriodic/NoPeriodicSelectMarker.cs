﻿using System;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic
{
    public class NoPeriodicSelectMarker : SelectMarkerStrategy
    {
        public NoPeriodicSelectMarker(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity)
        {
            _marker = marker;
            _entity = entity;
        }

        public override string FormatSelectQuery()
        {
            switch (GetMarkerImplementation())
            {
                case QueryBuilderMarkerImplementation.Implement: return new ImplementNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerImplementation.NoImplement: return new NoImplementNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerImplementation.Base: return new BaseImplaimentNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerImplementation.Unknown: throw new NotImplementedException();
                default: throw new NotImplementedException();
            }
        }
    }

    
}

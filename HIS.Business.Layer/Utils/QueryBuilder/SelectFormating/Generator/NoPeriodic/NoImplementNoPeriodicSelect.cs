﻿using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic.BaseImplaimentNoPeriodicSelectSubTypes;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic
{
    public class NoImplementNoPeriodicSelect : NoPeriodicSelectMarker
    {
        public NoImplementNoPeriodicSelect(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity) : base(marker, entity) { }

        public override string FormatSelectQuery()
        {
            switch ((QueryBuilderMarkerTypeId)_marker.MarkerTypeId)
            {
                case QueryBuilderMarkerTypeId.Reference: return new ReferenceNoImplementNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Address: return new AddressNoImplementNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Link: return new LinkNoImplementNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                case QueryBuilderMarkerTypeId.Enumeration: return new EnumerationNoImplementNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
                default: return new SampleNoImplementNoPeriodicSelect(_marker, _entity).FormatSelectQuery();
            }
        }
    }
}

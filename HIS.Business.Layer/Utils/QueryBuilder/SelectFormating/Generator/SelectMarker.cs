﻿using HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.NoPeriodic;
using HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator.Periodic;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.Business.Layer.Utils.QueryBuilder.SelectFormating.Generator
{
    public static class SelectMarker
    {
        public static string Generate(MarkersQueryBuilder marker, EntityQueryBuilderInfo entity)
        {
            return marker.IsPeriodic 
                ? new PeriodicSelectMarker(marker, entity).FormatSelectQuery() 
                : new NoPeriodicSelectMarker(marker, entity).FormatSelectQuery();
        }

        public static string AddBaidServiceFilds(EntityQueryBuilderInfo entity)
        {
            return $"[{entity.MvcAlias}].BaId AS [{entity.MvcAlias}//BAId],\n\t" +
                   $"[{entity.MvcAlias}].BATypeId AS [{entity.MvcAlias}//BATypeId],\n\t";
        }

        public static string AddServiceFilds(EntityQueryBuilderInfo entity)
        {
            return $"[states].Name AS [{entity.MvcAlias}//State],\n" +
            $"\t[{entity.MvcAlias}-SOBATypeStates].IsDelState AS [{entity.MvcAlias}//IsDelState],\n" +
            $"\t[{entity.MvcAlias}-SOBATypeStates].IsFirstState AS [{entity.MvcAlias}//IsFirstState]\n";
        }
    }
}

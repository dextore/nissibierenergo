﻿using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;

namespace HIS.Business.Layer.Utils.QueryBuilder
{
    public class Paging : IPaging
    {
        private readonly int From;
        private readonly int To;

        public Paging(int from, int to)
        {
            From = from;
            To = to;
        }

        public string FormatPaggingQuery()
        {
            return $"OFFSET {From} ROWS\n" +
                   $"FETCH NEXT {To} ROWS ONLY\n";
        }
    }
}

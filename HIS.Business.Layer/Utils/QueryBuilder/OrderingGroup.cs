﻿using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;
using HIS.Business.Layer.Utils.QueryBuilder.Models;

namespace HIS.Business.Layer.Utils.QueryBuilder
{
    public class OrderingGroup : Group<IGroupMember>
    {
        public OrderingGroup(QuerySection section, string separator = ",")
            : base(section, separator) { }

        public override void CreateQueryForSection()
        {
            GenerateStringForAllItemsInGroup($"{_separator}\n");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using HIS.Business.Layer.Services;
using HIS.Business.Layer.Utils.QueryBuilder.Exceptions;
using HIS.Business.Layer.Utils.QueryBuilder.Interfaces;
using HIS.Business.Layer.Utils.QueryBuilder.Models;
using HIS.DAL.DB.Handlers.QueryBuilder;
using HIS.Models.Layer.Models.QueryBuilder;
using Unity;
using Unity.Attributes;
using FilteringGroupOperator = HIS.Business.Layer.Utils.QueryBuilder.Models.FilteringGroupOperator;

namespace HIS.Business.Layer.Utils.QueryBuilder
{
    public class QueryBuilderService : ServiceBase, IQueryBuilderService
    {
        [Dependency]
        public IMapper Mapper { get; set; }
        private IQueryFormat _queryFormat;
        private IUnityContainer _resolver;
        private IQueryBuilderHandler _queryBuilderHandler;
        //TODO: Разделить ответственность на: работа с информацией о маркерах, составление запроса - QueryBuilder, исполнение запроса - QueryBuilderService, формирование json представления - QueryBuilderJsonHelper

        public QueryBuilderService(IUnityContainer resolver, IQueryBuilderHandler queryBuilderHandler, IQueryFormat queryFormat)
            : base(resolver)
        {
            _resolver = resolver;
            _queryBuilderHandler = queryBuilderHandler;
            _queryFormat = queryFormat;
        }

        public IEnumerable<CaptionAndAliase> GetCaptionsAndAndAliases(int baTypeId)
        {
            var fieldGenerator = new FieldGenerator(_resolver, baTypeId, _queryBuilderHandler, null);
            _queryFormat.FieldGenerator = fieldGenerator;

            return _queryFormat.FieldGenerator.FormatMarkersAliases();
        }

        public string GetData(QueryBuilderModel queryBuilderModel)
        {
            var query = MakeQuery(queryBuilderModel);
            var fields = GetFieldsAliases();
            try
            {
                var rowCount = Exec(p => p.GetHandler<IQueryBuilderHandler>().CalculateRowCount(query));
                var res = Exec(p => p.GetHandler<IQueryBuilderHandler>().Execute(query, fields, rowCount));
                return res;
            }
            catch (Exception ex)
            {
                throw new DebugException(ex) { SqlReq = query, QBModel = queryBuilderModel };
            }
        }

        private IEnumerable<string> GetFieldsAliases()
        {
            return _queryFormat.FieldGenerator.GeneratedFieldDictionary.Keys;
        }

        private string MakeQuery(QueryBuilderModel queryBuilderModel)
        {
            var qF = _queryFormat.Select(queryBuilderModel.BaTypeId, queryBuilderModel.SelectedPropertyes)
                .From(queryBuilderModel.BaTypeId)
                .Union(queryBuilderModel.NeedFirstLevelOfTypesHierarchy);
            if (queryBuilderModel.FilteringsTreeFields == null)
                qF = qF.Where(queryBuilderModel.FilteringsFields.Select(Mapper.Map<FilteringModel, Filtering>),
                    (FilteringGroupOperator) queryBuilderModel.FilteringsGroupOperator);
            else
                qF = qF.Where(queryBuilderModel.FilteringsTreeFields, (FilteringGroupOperator)queryBuilderModel.FilteringsGroupOperator);

            return qF.OrderBy(queryBuilderModel.OrderingsFields.Select(Mapper.Map<OrderingModel, Ordering>))
                     .OffsetTake(queryBuilderModel.From, queryBuilderModel.To)
                     .BuildQuery();
        }
    }
}
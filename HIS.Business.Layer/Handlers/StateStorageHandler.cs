﻿using HIS.DAL.DB.Context;
using System.Data.Entity;
using System.Linq;

namespace HIS.Business.Layer.Handlers
{
    internal class StateStorageHandler: RequestHandlerBase
    {
        internal void Save(EFStateStorage stateStorage)
        {
            var item = DBContext.StateStorage.Where(m => m.Key == stateStorage.Key).AsNoTracking().FirstOrDefault();
            var entry = DBContext.Entry(stateStorage);
            entry.State = item == null ? EntityState.Added : EntityState.Modified;
            DBContext.ChangeTracker.DetectChanges();
            DBContext.SaveChanges();
        }
    }
}

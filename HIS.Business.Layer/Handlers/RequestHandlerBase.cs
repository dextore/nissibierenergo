﻿using HIS.Business.Layer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HIS.DAL.DB.Context;
using HIS.DAL.Client.Common;

namespace HIS.Business.Layer.Handlers
{
    internal class RequestHandlerBase : IRequestHandler
    {
        public void InitializeContext(HISModelEntities dataContext)
        {
            DBContext = dataContext;
        }

        internal HISModelEntities DBContext { get; private set; }
    }
}

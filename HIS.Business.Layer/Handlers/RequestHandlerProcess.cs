﻿using HIS.DAL.Client.Common;
using HIS.DAL.DB.Context;
using Microsoft.Practices.Unity;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace HIS.Business.Layer.Handlers
{
    public class RequestHandlerProcess: IRequestHandlerProcess
    {
        IUnityContainer _resolver;
        HISModelEntities _dbContext;

        public RequestHandlerProcess(IUnityContainer resolver)
        {
            _resolver = resolver;
            _dbContext = _resolver.Resolve<HISModelEntities>();
        }

        public TResult Go<TResult>(Func<IRequestHandlerProcess, TResult> func)
        {
            using (var transactionContext = _dbContext.Database.BeginTransaction())
            {
                try
                {
                    var result = func(this);
                    transactionContext.Commit();
                    return result;
                }
                catch (Exception)
                {
                    //TO DO can be an entry point for writing to the log ???
                    transactionContext.Rollback();
                    throw;
                }
                finally
                {
                    DetachAll();
                }
            }
        }

        public void Go(Action<IRequestHandlerProcess> action)
        {
            Go<int>(RequestHandlerProcess => {
                action(RequestHandlerProcess);
                return 0;
            });
        }

        public THandler Get<THandler>() where THandler : class, IRequestHandler
        {
            var handler = _resolver.Resolve<THandler>();
            handler.InitializeContext(_dbContext);
            return handler;
        }

        private void DetachAll()
        {
            foreach (DbEntityEntry entityEntry in _dbContext.ChangeTracker.Entries().ToArray())
            {
                if (entityEntry.Entity != null)
                {
                    entityEntry.State = EntityState.Detached;
                }
            }
        }
    }
}

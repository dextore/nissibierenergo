﻿using HIS.Models.Layer.Models.FIAS;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HIS.Models.Layer.Models;


namespace HIS.FIAS.Interfaces
{
    public interface IFiasHandler
    {
        Task<IEnumerable<FiasFindedAddressesWithHousest>> AddrSearchFull(string findStr);

        /// <summary>
        /// По ID дома/строения...
        /// </summary>
        /// <param name="HOUSEID"></param>
        /// <returns></returns>
        Task<FiasFindedAddressesWithHousest> GetAddrFullByHouse(Guid AOID, Guid HOUSEID);

        /// <summary>
        /// По ID земельного участка
        /// </summary>
        /// <param name="STEADID"></param>
        /// <returns></returns>
        Task<FiasFindedAddressesWithHousest> GetAddrFullByStead(Guid AOID, Guid STEADID);

        Task<FiasFindedAddressesWithHousest> GetAddrFullByAOID(Guid AOID);

        Task<IEnumerable<FiasFindedAddress>> SimpleAddrSearch(string findStr);
        Task<FiasFindedAddress> SimpleAddrByAOID(Guid AOID);
        Task<FiasAddressInfo> GetAddressInfo(Guid AOID);

        Task<IEnumerable<FiasRFSubject>> GetRFSubjects(RequestModel model);
        //Task<IEnumerable<FiasDistrict>> GetDistricts(AddressRequestModel model);
        Task<IEnumerable<FiasRegion>> GetRegions(AddressRequestModel model);
        Task<IEnumerable<FiasCity>> GetCities(AddressRequestModel model);
        //Task<IEnumerable<FiasInRegion>> GetInRegions(Guid? AOGUID = null);
        Task<IEnumerable<FiasSettlement>> GetSettlements(AddressRequestModel model);
        Task<IEnumerable<FiasElmStructure>> GetElmStructures(AddressRequestModel model);
        Task<IEnumerable<FiasStreet>> GetStreets(AddressRequestModel model);
        Task<IEnumerable<FiasAddTerritory>> GetAddTerritories(Guid? AOGUID = null);
        Task<IEnumerable<FiasAddTerritoryStreet>> GetAddTerritoryStreets(Guid? AOGUID = null);

        Task<IEnumerable<FiasAddressHouse>> GetAddressHouses(Guid? AOGUID = null);
        Task<IEnumerable<FiasAddressHouse>> GetHouses(AddressRequestModel model);
        Task<IEnumerable<FiasAddressStead>> GetSteads(AddressRequestModel model);
        Task<IEnumerable<FiasDetailAddress>> GetDetailAddrSearch(FiasDetailSearch searchModel);
        Task<FiasXMLAddressData> GetXMLAllParts(Guid AOId, Guid? houseId = null, Guid? steadId = null);
        Task<FiasAddressHouse> GetHouse(Guid? aoGuid, Guid? houseId);
        Task<FiasAddressStead> GetStead(Guid? aoGuid, Guid? steadId);
        Task<FiasAddresObject> GetAddressObject(Guid? aoGuid);
        Task<bool> IsFindFiasAddressObject(string FormalName, string ShortName, int AOLevel, Guid? ParentId);
        Task<bool> IsFindFiasStead(Guid ParentId, string Number, string cadNum = null);
        Task<bool> IsFindFiasHouse(Guid AOGuid, int? EstStatId = null, string HouseNum = null, string BuildNum = null, int? StrStatId = null, string StructNum = null, string CadNumber = null, string PostCode = null);
        Task<IEnumerable<FiasAddress>> GetAddressList(Guid? aoId, Guid? houseId, Guid? steadId, int? liveStatus,string postCode, string CadNumber = null);
        Task<IEnumerable<FiasAddressObjectType>> GetAddressObjectTypes(int level);
        Task<IEnumerable<FiasEstateStatus>> GetEstateStatuses();
        Task<IEnumerable<FiasStructureStatus>> GetStructureStatuses();
        Task<IEnumerable<FiasAddressComparisons>> GetAddressComparisons(string xData);
    }
}

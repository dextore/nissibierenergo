﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HIS.FIAS.Interfaces;
using HIS.Models.Layer.Models;
using HIS.Models.Layer.Models.FIAS;
using Newtonsoft.Json;

namespace HIS.FIAS.Handlers
{
    public class FiasHandler : IFiasHandler
    {
        private readonly string _endPontService;
        public FiasHandler(string endPount)
        {
            _endPontService = endPount;
        }

        public async Task<IEnumerable<FiasFindedAddressesWithHousest>> AddrSearchFull(string findStr)
        {
            const string methodName = "SimpleAddrSearchWithHouses";
            var rqParams = $"findStr={findStr}";

            var result = await GetData<FiasFindedAddressesWithHousest[]>(methodName, rqParams);
            return result.ToArray();
        }

        public async Task<FiasFindedAddressesWithHousest> GetAddrFullByHouse(Guid AOID, Guid HOUSEID)
        {
            //const string methodName = "SimpleAddrSearchWithHouses";
            const string methodName = "GetFullAddressText";
            var rqParams = $"AOID={AOID}&HOUSEID={HOUSEID}";

            var result = await GetData<FiasFindedAddressesWithHousest[]>(methodName, rqParams);
            return result.FirstOrDefault();
        }

        public async Task<FiasFindedAddressesWithHousest> GetAddrFullByStead(Guid AOID, Guid STEADID)
        {
            //const string methodName = "SimpleAddrSearchWithHouses";
            const string methodName = "GetFullAddressText";
            var rqParams = $"AOID={AOID}&STEADID={STEADID}";

            var result = await GetData<FiasFindedAddressesWithHousest[]>(methodName, rqParams);
            return result.FirstOrDefault();
        }

        public async Task<FiasFindedAddressesWithHousest> GetAddrFullByAOID(Guid AOID)
        {
            //const string methodName = "SimpleAddrSearchWithHouses";
            const string methodName = "GetFullAddressText";
            var rqParams = $"AOID={AOID}";

            var result = await GetData<FiasFindedAddressesWithHousest[]>(methodName, rqParams);
            return result.FirstOrDefault();
        }

        public async Task<IEnumerable<FiasFindedAddress>> SimpleAddrSearch(string findStr = null)
        {
            const string methodName = "SimpleAddrSearch";
            var rqParams = $"findStr={findStr}";

            var result = await GetData<FiasFindedAddress[]>(methodName, rqParams);
            return result;
        }

        public async Task<FiasFindedAddress> SimpleAddrByAOID(Guid AOID)
        {
            const string methodName = "SimpleAddrSearch";
            var rqParams = $"AOID={AOID}";

            var result = await GetData<IEnumerable<FiasFindedAddress>>(methodName, rqParams);
            return result.FirstOrDefault();
        }

        public async Task<FiasAddressInfo> GetAddressInfo(Guid AOID)
        {
            const string methodName = "GetAddressInfo";
            var rqParams = $"AOID={AOID}";
            var result = await GetData<FiasAddressInfo[]>(methodName, rqParams);
            return result.FirstOrDefault();
        }

#region ADVANCEDSEARCH

        public async Task<IEnumerable<FiasRFSubject>> GetRFSubjects(RequestModel model)
        {
            const string methodName = "GetRFSubjects";
            var result = await GetData<FiasRFSubject[]>(methodName, "");
            return SetFilter(result, model).ToArray();
        }

        //public async Task<IEnumerable<FiasDistrict>> GetDistricts(AddressRequestModel model)
        //{
        //    return await GetCatalogs<FiasDistrict>("GetDistricts", model);
        //}

        public async Task<IEnumerable<FiasRegion>> GetRegions(AddressRequestModel model)
        {
            return await GetCatalogs<FiasRegion>("GetRegions", model);
        }

        public async Task<IEnumerable<FiasCity>> GetCities(AddressRequestModel model)
        {
            return await GetCatalogs<FiasCity>("GetCities", model);
        }

        //public async Task<IEnumerable<FiasInRegion>> GetInRegions(Guid? AOGUID = null)
        //{
        //    const string methodName = "GetInRegions";
        //    var rqParams = AOGUID != null ? $"AOGUID={AOGUID}" : "";
        //    var result = await GetData<FiasInRegion[]>(methodName, rqParams);
        //    return result;
        //}

        public async Task<IEnumerable<FiasSettlement>> GetSettlements(AddressRequestModel model)
        {
            return await GetCatalogs<FiasSettlement>("GetSettlements", model);
        }

        public async Task<IEnumerable<FiasElmStructure>> GetElmStructures(AddressRequestModel model)
        {
            return await GetCatalogs<FiasElmStructure>("GetElmStructures", model);
        }

        public async Task<IEnumerable<FiasStreet>> GetStreets(AddressRequestModel model)
        {
            return await GetCatalogs<FiasStreet>("GetStreets", model);
        }

        public async Task<IEnumerable<FiasAddTerritory>> GetAddTerritories(Guid? AOGUID = null)
        {
            const string methodName = "GetAddTerritories";
            var rqParams = AOGUID != null ? $"AOGUID={AOGUID}" : "";
            var result = await GetData<FiasAddTerritory[]>(methodName, rqParams);
            return result;
        }

        public async Task<IEnumerable<FiasAddTerritoryStreet>> GetAddTerritoryStreets(Guid? AOGUID = null)
        {
            const string methodName = "GetAddTerritoryStreets";
            var rqParams = AOGUID != null ? $"AOGUID={AOGUID}" : "";
            var result = await GetData<FiasAddTerritoryStreet[]>(methodName, rqParams);
            return result;
        }

        public async Task<IEnumerable<FiasAddressHouse>> GetAddressHouses(Guid? AOGUID = null)
        {
            const string methodName = "GetAddressHouses";
            var rqParams = AOGUID != null ? $"AOGUID={AOGUID}" : "";
            var result = await GetData<FiasAddressHouse[]>(methodName, rqParams);
            return result;
        }

#endregion

        public async Task<IEnumerable<FiasAddressHouse>> GetHouses(AddressRequestModel model)
        {
            const string methodName = "GetAddressHouses";
            var rqParams = model.AOGuid != null ? $"AOGUID={model.AOGuid}" : "";
            var addressHouses = await GetData<FiasAddressHouse[]>(methodName, rqParams);

            return addressHouses.ToArray();
        }

        public async Task<IEnumerable<FiasAddressStead>> GetSteads(AddressRequestModel model)
        {
            const string methodName = "GetAddressSteads";
            var rqParams = model.AOGuid != null ? $"AOGUID={model.AOGuid}" : "";
            var addressHouses = await GetData<FiasAddressStead[]>(methodName, rqParams);

            return addressHouses.ToArray();
        }

        public async Task<FiasAddressHouse> GetHouse(Guid? aoGuid, Guid? houseId)
        {
            const string methodName = "GetAddressHouses";

            var paramList = new List<string>();

            if (aoGuid != null)
                paramList.Add($"AOGUID={aoGuid}");

            if (houseId != null)
                paramList.Add($"HOUSEID={houseId}");

            var addressHouse = await GetData<FiasAddressHouse[]>(methodName, string.Join("&", paramList));

            return addressHouse.FirstOrDefault();
        }

        public async Task<FiasAddressStead> GetStead(Guid? aoGuid, Guid? steadId)
        {
            const string methodName = "GetAddressSteads";

            var paramList = new List<string>();

            if (aoGuid != null)
                paramList.Add($"AOGUID={aoGuid}");

            if (steadId != null)
                paramList.Add($"STEADID={steadId}");

            var addressStead = await GetData<FiasAddressStead[]>(methodName, string.Join("&", paramList));

            return addressStead.FirstOrDefault();
        }

        /// <summary>
        /// Поиск адресного объекта в фиасе.
        /// Используется при создании нового эллемента. Проверяется по заполненым полям
        /// </summary>
        /// <param name="FormalName"></param>
        /// <param name="ShortName"></param>
        /// <param name="AOLevel"></param>
        /// <param name="ParentId"></param>
        /// <returns></returns>
        public async Task<bool> IsFindFiasAddressObject(string FormalName, string ShortName, int AOLevel, Guid? ParentId)
        {
            const string methodName = "FindAOByNISParameters";

            var paramList = new List<string> { $"FormalName={FormalName}" };
            paramList.Add($"ShortName={ShortName}");
            paramList.Add($"AOLevel={AOLevel}");
            if (ParentId != null)
                paramList.Add($"ParentId={ParentId}");

            var addressObject = await GetData<FiasAddresObject>(methodName, string.Join("&", paramList));
            if (addressObject != null)
            {
                return true;
            }
            return false;

        }
        /// <summary>
        /// Поиск дома в фиасе.
        /// Используется при создании нового эллемента. Проверяется по заполненым полям
        /// </summary>
        /// <param name="AOGuid"></param>
        /// <param name="EstStatId"></param>
        /// <param name="HouseNum"></param>
        /// <param name="BuildNum"></param>
        /// <param name="StrStatId"></param>
        /// <param name="StructNum"></param>
        /// <param name="CadNumber"></param>
        /// <returns></returns>

        public async Task<bool> IsFindFiasHouse(Guid AOGuid, int? EstStatId = null, string HouseNum = null, string BuildNum = null, int? StrStatId = null, string StructNum = null , string CadNumber = null, string PostCode = null )
        {
            const string methodName = "FindHouseByNISParameters";

            var paramList = new List<string> { $"AOGuid={AOGuid}" };


            if (EstStatId != null)
                paramList.Add($"EstStatId={EstStatId}");

            if (HouseNum != null && HouseNum != "")
                paramList.Add($"HouseNum={HouseNum}");

            if (BuildNum != null && BuildNum != "")
                paramList.Add($"BuildNum={BuildNum}");

            if (StrStatId != null)
                paramList.Add($"StrStatId={StrStatId}");

            if (StructNum != null && StructNum != "")
                paramList.Add($"StructNum={StructNum}");

            if (PostCode != null && PostCode != "")
                paramList.Add($"PostCode={PostCode}");
            /*
            после появления кадастрового в фиасе добавить
            if (CadNumber != null && CadNumber != "")
                paramList.Add($"CadNum={CadNumber}");
            */
            var houseObject = await GetData<FiasHouse>(methodName, string.Join("&", paramList));
            if (houseObject != null)
            {
                return true;
            }
            return false;

        }
        /// <summary>
        /// Поиск exfcnrf в фиасе.
        /// Используется при создании нового эллемента. Проверяется по заполненым полям
        /// </summary>
        /// <param name="ParentId"></param>
        /// <param name="Number"></param>
        /// <param name="CadNumber"></param>
        /// <returns></returns>
        public async Task<bool> IsFindFiasStead(Guid ParentId, string Number, string CadNumber = null)
        {
            const string methodName = "FindSteadByNISParameters";

            var paramList = new List<string> { $"ParentId={ParentId}" };
            paramList.Add($"Number={Number}");
            /*
             после появления кадастрового в фиасе добавить
             if (CadNumber != null && CadNumber != "")
                 paramList.Add($"CadNum={CadNumber}");
             */

            var steadObject = await GetData<FiasStead>(methodName, string.Join("&", paramList));
            if (steadObject != null)
            {
                return true;
            }
            return false;

        }     

        public async Task<FiasAddresObject> GetAddressObject(Guid? aoGuid)
        {
            const string methodName = "GetAddressObject";
            var rqParams = aoGuid != null ? $"AOGUID={aoGuid}" : "";
            var addressObject = await GetData<FiasAddresObject>(methodName, rqParams);
            return addressObject;
        }

        public async Task<IEnumerable<FiasAddress>> GetAddressList(Guid? aoId, Guid? houseId, Guid? steadId,
            int? liveStatus , string postCode = null , string cadNum = null)
        {
            const string methodName = "GetAddressListForNIS";
            var paramList = new List<string>();

            if (aoId != null)
                paramList.Add($"AOID={aoId}");

            if (houseId != null)
                paramList.Add($"HOUSEID={houseId}");

            if (steadId != null)
                paramList.Add($"STEADID={steadId}");

            if (liveStatus != null)
                paramList.Add($"liveStatus={liveStatus}");

            if (postCode != null && postCode != "")
                paramList.Add($"POSTCODE={postCode}");
         
            if (cadNum != null && postCode != "")
                paramList.Add($"CADNUM={cadNum}");
         

            var addressObject = await GetData<IEnumerable<FiasAddress>>(methodName, string.Join("&", paramList));
            return addressObject;
        }


        public async Task<IEnumerable<FiasDetailAddress>> GetDetailAddrSearch(FiasDetailSearch searchModel)
        {
            const string methodName = "GetDetailAddrSearch";

            var paramList = new List<string>();

            if (searchModel.TypeSearch != null)
                paramList.Add(($"TypeSearch={searchModel.TypeSearch}"));

            if (searchModel.RFSubject != null)
                paramList.Add(($"RFSubject={searchModel.RFSubject}"));

            if (searchModel.District != null)
                paramList.Add(($"District={searchModel.District}"));

            if (searchModel.Region != null)
                paramList.Add(($"Region={searchModel.Region}"));

            if (searchModel.InRegion != null)
                paramList.Add(($"InRegion={searchModel.InRegion}"));

            if (searchModel.Settlement != null)
                paramList.Add(($"Settlement={searchModel.Settlement}"));

            if (searchModel.ElmStructure != null)
                paramList.Add(($"ElmStructure={searchModel.ElmStructure}"));

            if (searchModel.Street != null)
                paramList.Add(($"Street={searchModel.Street}"));

            if (searchModel.Territory != null)
                paramList.Add(($"Territory={searchModel.Territory}"));

            if (searchModel.AddTerritoryStreet != null)
                paramList.Add(($"AddTerritoryStreet={searchModel.AddTerritoryStreet}"));

            if (searchModel.House != null)
                paramList.Add(($"House={searchModel.House}"));

            if (searchModel.PlainCode != null)
                paramList.Add(($"PlainCode={searchModel.PlainCode}"));

            var result = await GetData<FiasDetailAddress[]>(methodName, String.Join("&", paramList));
            return result;
        }

        public async Task<FiasXMLAddressData> GetXMLAllParts(Guid AOId, Guid? houseId = null, Guid? steadId = null)
        {
            const string methodName = "GetXMLAllParts";

            var paramList = new List<string> {$"AOID={AOId}"};

            if (houseId != null)
                paramList.Add($"HOUSEID={houseId}");

            if (steadId != null)
                paramList.Add($"STEADID={steadId}");

            var result = await GetData<FiasXMLAddressData>(methodName, String.Join("&", paramList));
            return result;
        }

        /// <summary>
        /// Получает типы адресных объектов из FIAS
        /// </summary>
        /// <param name="level">Уровень адресного объекта</param>
        /// <returns></returns>
        public async Task<IEnumerable<FiasAddressObjectType>> GetAddressObjectTypes(int level)
        {
            const string methodName = "GetAddressObjectTypes";
            var rqParams = $"level={level}";
            var addressObjectTypes = await GetData<IEnumerable<FiasAddressObjectType>>(methodName, rqParams);
            return addressObjectTypes;
        }

        /// <summary>
        /// Получение типов зданий
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<FiasEstateStatus>> GetEstateStatuses()
        {
            const string methodName = "GetEstateStatuses";
            var estateStatuses = await GetData<IEnumerable<FiasEstateStatus>>(methodName);
            return estateStatuses;
        }

        /// <summary>
        /// Получение типов строений
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<FiasStructureStatus>> GetStructureStatuses()
        {
            const string methodName = "GetStructureStatuses";
            var structureStatuses = await GetData<IEnumerable<FiasStructureStatus>>(methodName);
            return structureStatuses;
        }

        /// <summary>
        /// Получает сопоставленные адреса из ФИАС
        /// </summary>
        /// <param name="xData">XML с адресами для сопоставления</param>
        /// <returns></returns>
        public async Task<IEnumerable<FiasAddressComparisons>> GetAddressComparisons(string xData)
        {
            const string methodName = "AddressComparator";

            var data = new AddrssUnsettles
            {
                XData = xData
            };

            var structureStatuses = await GetData<IEnumerable<FiasAddressComparisons>>(methodName, null, data);
            return structureStatuses;
        }

        private async Task<T> GetData<T>(string methodName, string rqParams = null, object data = null)
        {
            //return await Task.Factory.StartNew<T>(() =>
            //{

                using (WebClient client = new WebClient())
                {
                    var url = UrlCombine(_endPontService, methodName);

                    if (!string.IsNullOrEmpty(rqParams))
                    {
                        url = $"{url}?{rqParams}";
                    }

                    string sData = "";
                    if (data != null)
                    {
                        sData = JsonConvert.SerializeObject(data);

                        client.Headers[HttpRequestHeader.ContentType] = "application/json";
                        client.Encoding = Encoding.UTF8;
                    }

                    //var json = client.UploadString(url, sData);
                    var json = await client.UploadStringTaskAsync(url, "POST", sData);

                    string str = "";

                    str = data != null ? json : ToUtf8(json);

                    var result = JsonConvert.DeserializeObject<T>(str);
                    return result;
                }
            //});
        }

        private static string ToUtf8(string value)
        {
            byte[] bytes = Encoding.Default.GetBytes(value);
            return Encoding.UTF8.GetString(bytes);
        }

        private async Task<IEnumerable<T>> GetCatalogs<T>(string methodName, AddressRequestModel model) where T : FiasCatalogBase
        {
            var rqParams = model.AOGuid != null ? $"AOGUID={model.AOGuid}" : "";
            var result = await GetData<T[]>(methodName, rqParams);
            return SetFilter(result, model).ToArray();
        }
         
        private IEnumerable<T> SetFilter<T>(IEnumerable<T> values, RequestModel model) where T : FiasCatalogBase
        {
            var responce = values.AsQueryable();
            if (!string.IsNullOrWhiteSpace(model.SearchValue))
                responce = responce.Where(m => m.NAME.ToLower().Contains(model.SearchValue.ToLower()));
            responce = responce.Skip(model.Skip).Take(model.Take);
            return responce;
        }

        private static string UrlCombine(string endpoint, string method)
        {
            const char slash = '/';
            var url = endpoint.TrimEnd(slash);
            return $"{url}/{method}";
        }

    }
}

﻿using HIS.DAL.DB.Context;
using System.Data.SqlClient;

namespace HIS.DAL.DB
{
    /// <summary>
    /// Интерфейс обработчика данных в хранилище
    /// </summary>
    public interface IDataContextHandler
    {
        /// <summary>
        /// Инициализация контекста хранилища
        /// </summary>
        /// <param name="dataContext">Контекс хранилища</param>
        void InitializeContext(HISModelEntities dataContext);

        void ExecuteStoredProg(string sql, params SqlParameter[] parameters);
    }
}

﻿using System;

namespace HIS.DAL.DB
{
    /// <summary>
    /// Интерфейс процесса выполнения обработчиков хранилища данных
    /// </summary>
    public interface IDataContextHandlerProcess: IDisposable
    {
        /// <summary>
        /// Получение экземпляра обработчика хранилища данных 
        /// </summary>
        /// <typeparam name="THandler">Интерфейс обработчика</typeparam>
        /// <returns>Экземпляр обработчика контекста хранилища данных</returns>
        THandler GetHandler<THandler>() where THandler : class, IDataContextHandler;

        /// <summary>
        /// Получение обработчика без инициализации контекста БД
        /// </summary>
        /// <typeparam name="THandler">Интерфейс обработчика</typeparam>
        /// <returns>Экземпляр обработчика</returns>
        THandler GetHadlerWithoutContext<THandler>() where THandler : class, IDataContextHandler;
    }
}

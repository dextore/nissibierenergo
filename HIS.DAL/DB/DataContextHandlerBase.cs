﻿using System;
using System.Collections.Generic;
using HIS.DAL.DB.Context;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using HIS.DAL.Client.Models.Markers;
using HIS.Models.Layer.Models.Markers;

namespace HIS.DAL.DB
{
    /// <summary>
    /// Базовый класс обработчика данных в хранилище
    /// </summary>
    public abstract class DataContextHandlerBase: IDataContextHandler
    {
        /// <summary>
        /// Инициализация контекста хранилища данных
        /// </summary>
        /// <param name="dataContext">Контекст хранилища данных</param>
        public void InitializeContext(HISModelEntities dataContext)
        {
            DataContext = dataContext;
        }

        /// <summary>
        /// Контекст хранилища
        /// </summary>
        public HISModelEntities DataContext { get; private set; }

        /// <summary>
        /// Выполняет хранимую пройедуру 
        /// </summary>
        /// <param name="sql">Наименование процедуры</param>
        /// <param name="parameters">Параметры процедуры. Если процедура возвращает значение через параметры - значение сохраняется в соответствующем параметре</param>
        public void ExecuteStoredProg(string sql, params SqlParameter[] parameters)
        {
            if (parameters != null && parameters.Length > 0)
            {
                DataContext.Database.ExecuteSqlCommand(sql, parameters);
            }
            else
            {
                DataContext.Database.ExecuteSqlCommand(sql);
            }
        }

        internal long ExecEntitytUpdateProg(string prog, Dictionary<string, string> paramMaps,
            EntityUpdate model, IEnumerable<MarkerInfo> markersInfo)
        {

            var parameters = new List<SqlParameter>();
            var sbSql = new StringBuilder();
            sbSql.AppendLine(prog);

            var baIdName = "@iBAId";
            var iBAId = new SqlParameter(baIdName, model.BAId ?? (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.BigInt
            };
            parameters.Add(iBAId);
            sbSql.AppendLine($"{baIdName} = {baIdName} OUT");

            foreach (var markerInfo in markersInfo)
            {
                if (!paramMaps.ContainsKey(markerInfo.Name))
                    continue;

                var ppName = paramMaps[markerInfo.Name];
                SqlParameter sqlParameter = null;
                var mValue = model.MarkerValues.FirstOrDefault(m => m.MVCAliase == markerInfo.Name);
                //if (mValue == null)
                //    continue;
                switch ((MarkerType)markerInfo.MarkerType)
                {
                    case MarkerType.MtBoolean: //1	 Логический
                        sqlParameter = new SqlParameter(ppName, mValue == null || string.IsNullOrWhiteSpace(mValue.Value)
                            ? (object)DBNull.Value
                            : bool.Parse(mValue.Value))
                        {
                            SqlDbType = System.Data.SqlDbType.Bit
                        };
                        break;
                    case MarkerType.MtDecimal: //3	 Вещественный
                        sqlParameter = new SqlParameter(ppName, mValue == null || string.IsNullOrWhiteSpace(mValue.Value)
                            ? (object)DBNull.Value
                            : decimal.Parse(mValue.Value))
                        {
                            SqlDbType = System.Data.SqlDbType.Decimal
                        };
                        break;
                    case MarkerType.MtString: //4	 Символьный
                        sqlParameter = new SqlParameter(ppName, mValue == null || string.IsNullOrWhiteSpace(mValue.Value)
                            ? (object)DBNull.Value
                            : mValue.Value)
                        {
                            SqlDbType = System.Data.SqlDbType.NVarChar
                        };
                        break;
                    case MarkerType.MtInteger: //2	 Целочисленный
                    case MarkerType.MtList: //5	 Перечень
                        sqlParameter = new SqlParameter(ppName, mValue == null || string.IsNullOrWhiteSpace(mValue.Value)
                            ? (object)DBNull.Value
                            : int.Parse(mValue.Value))
                        {
                            SqlDbType = System.Data.SqlDbType.Int
                        };
                        break;
                    case MarkerType.MtMoney: //6	 Денежный
                        sqlParameter = new SqlParameter(ppName, mValue == null || string.IsNullOrWhiteSpace(mValue.Value)
                            ? (object)DBNull.Value
                            : decimal.Parse(mValue.Value))
                        {
                            SqlDbType = System.Data.SqlDbType.Money
                        };
                        break;
                    case MarkerType.MtDateTime: //10 Дата и Время
                        sqlParameter = new SqlParameter(ppName, mValue == null || string.IsNullOrWhiteSpace(mValue.Value)
                            ? (object)DBNull.Value
                            : DateTime.Parse(mValue.Value))
                        {
                            SqlDbType = System.Data.SqlDbType.DateTime
                        };
                        break;
                    case MarkerType.MtEntityRef: //11 Ссылка на сущность
                    case MarkerType.MtAddress: //12 Ссылка на сущность адреса
                    case MarkerType.MtReference: //13 Ссылка на справочник
                        sqlParameter = new SqlParameter(ppName, mValue == null || string.IsNullOrWhiteSpace(mValue.Value)
                            ? (object)DBNull.Value
                            : long.Parse(mValue.Value))
                        {
                            SqlDbType = System.Data.SqlDbType.BigInt
                        };
                        break;
                }
                parameters.Add(sqlParameter);
                sbSql.AppendLine($",{ppName} = {ppName}");
            }

            ExecuteStoredProg(sbSql.ToString(), parameters.ToArray());

            return (long)iBAId.Value;
        }

    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.DAL.DB.Context
{
    using System;
    using System.Collections.Generic;
    
    public partial class EFCAContract
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EFCAContract()
        {
            this.RIReceiptInvoices = new HashSet<RIReceiptInvoices>();
            this.CAContractInvents = new HashSet<EFCAContractInvent>();
        }
    
        public long BAId { get; set; }
        public int BATypeId { get; set; }
        public int StateId { get; set; }
        public string Number { get; set; }
        public Nullable<System.DateTime> DocDate { get; set; }
        public long CAId { get; set; }
        public Nullable<long> LSId { get; set; }
        public Nullable<int> TypeId { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public Nullable<int> CreatorId { get; set; }
    
        public virtual EFBABaseAncestor BABaseAncestors { get; set; }
        public virtual EFBAType BATypes { get; set; }
        public virtual EFLSLegalSubject LSLegalSubjects { get; set; }
        public virtual EFSOState SOStates { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RIReceiptInvoices> RIReceiptInvoices { get; set; }
        public virtual EFUser HISUsers { get; set; }
        public virtual EFLSCompanyArea LSCompanyAreas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EFCAContractInvent> CAContractInvents { get; set; }
    }
}

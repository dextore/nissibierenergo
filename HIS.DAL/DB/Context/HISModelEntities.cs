﻿namespace HIS.DAL.DB.Context
{
    public partial class HISModelEntities
    {
        public HISModelEntities(string connectionString)                      
            : base($"metadata=res://*/DB.Context.HIS.csdl|res://*/DB.Context.HIS.ssdl|res://*/DB.Context.HIS.msl;provider=System.Data.SqlClient;provider connection string=\"{connectionString}\";")
        { 
        }
    }
}

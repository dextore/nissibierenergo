//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.DAL.DB.Context
{
    using System;
    using System.Collections.Generic;
    
    public partial class EFWHInventoryTransaction
    {
        public long RecId { get; set; }
        public long CAId { get; set; }
        public long DocId { get; set; }
        public long InventoryId { get; set; }
        public long AccId { get; set; }
        public long WarehouseId { get; set; }
        public byte RecTypeId { get; set; }
        public bool IsIncome { get; set; }
        public double Quantity { get; set; }
        public decimal VAT { get; set; }
        public double PriceVAT { get; set; }
        public bool IsVATIncluded { get; set; }
        public decimal StoreCost { get; set; }
        public Nullable<decimal> SellCost { get; set; }
        public Nullable<decimal> SellCostVAT { get; set; }
        public System.DateTime OperationDate { get; set; }
    
        public virtual EFIInventory IInventory { get; set; }
        public virtual EFPoACompanyAreasPlanOfAccount PoACompanyAreasPlanOfAccounts { get; set; }
        public virtual EFPORecType PORecTypes { get; set; }
        public virtual EFWHWarehouse WHWarehouses { get; set; }
        public virtual EFBABaseAncestor BABaseAncestors { get; set; }
        public virtual EFLSCompanyArea LSCompanyAreas { get; set; }
    }
}

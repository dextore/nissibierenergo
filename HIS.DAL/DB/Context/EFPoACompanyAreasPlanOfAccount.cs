//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.DAL.DB.Context
{
    using System;
    using System.Collections.Generic;
    
    public partial class EFPoACompanyAreasPlanOfAccount
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EFPoACompanyAreasPlanOfAccount()
        {
            this.WHInventoryTransactions = new HashSet<EFWHInventoryTransaction>();
        }
    
        public long BAId { get; set; }
        public int BATypeId { get; set; }
        public int StateId { get; set; }
        public long AccId { get; set; }
        public long SupplierId { get; set; }
        public Nullable<long> ParentId { get; set; }
        public string Num { get; set; }
        public string PlnNum { get; set; }
        public string QuickNum { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public Nullable<int> TypeId { get; set; }
        public bool IsAggregate { get; set; }
        public int TreeLevel { get; set; }
        public string NameShift { get; set; }
        public string FullName { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EFWHInventoryTransaction> WHInventoryTransactions { get; set; }
    }
}

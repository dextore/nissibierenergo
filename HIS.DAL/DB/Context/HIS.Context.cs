﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.DAL.DB.Context
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class HISModelEntities : DbContext
    {
        private HISModelEntities()
            : base("name=HISModelEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<EFBABaseAncestor> EFBABaseAncestors { get; set; }
        public virtual DbSet<EFBASearchType> EFBASearchTypes { get; set; }
        public virtual DbSet<EFBAType> EFBATypes { get; set; }
        public virtual DbSet<EFMABaseAncestorMarkerItemValuePeriod> EFMABaseAncestorMarkerItemValuePeriods { get; set; }
        public virtual DbSet<EFMABaseAncestorMarkerItemValue> EFMABaseAncestorMarkerItemValues { get; set; }
        public virtual DbSet<EFMABaseAncestorMarkerValuePeriod> EFMABaseAncestorMarkerValuePeriods { get; set; }
        public virtual DbSet<EFMABaseAncestorMarkerValue> EFMABaseAncestorMarkerValues { get; set; }
        public virtual DbSet<EFMABATypeMarker> EFMABATypeMarkers { get; set; }
        public virtual DbSet<EFMABATypeStateMarker> EFMABATypeStateMarkers { get; set; }
        public virtual DbSet<EFMAMarker> EFMAMarkers { get; set; }
        public virtual DbSet<EFMAMarkerType> EFMAMarkerTypes { get; set; }
        public virtual DbSet<EFMAMarkerValueList> EFMAMarkerValueList { get; set; }
        public virtual DbSet<EFCREmail> EFCREmails { get; set; }
        public virtual DbSet<EFCRPhone> EFCRPhones { get; set; }
        public virtual DbSet<EFCRContact> EFCRContacts { get; set; }
        public virtual DbSet<EFSOState> EFSOStates { get; set; }
        public virtual DbSet<EFITaxRate> EFITaxRates { get; set; }
        public virtual DbSet<EFSOBATypeStateOperation> EFSOBATypeStateOperations { get; set; }
        public virtual DbSet<EFSOOperation> EFSOOperations { get; set; }
        public virtual DbSet<EFSOBATypeState> EFSOBATypeStates { get; set; }
        public virtual DbSet<SOBATypeOperations> SOBATypeOperations { get; set; }
        public virtual DbSet<AOHouses> AOHouses { get; set; }
        public virtual DbSet<AOAddressObjects> AOAddressObjects { get; set; }
        public virtual DbSet<AOSteads> AOSteads { get; set; }
        public virtual DbSet<AOAddresses> AOAddresses { get; set; }
        public virtual DbSet<Banks> Banks { get; set; }
        public virtual DbSet<EFLSLegalSubjectParamValuePeriod> EFLSLegalSubjectParamValuePeriods { get; set; }
        public virtual DbSet<EFLSLegalSubjectParamValue> EFLSLegalSubjectParamValues { get; set; }
        public virtual DbSet<EFLSLegalSubject> EFLSLegalSubjects { get; set; }
        public virtual DbSet<BankParamValuePeriods> BankParamValuePeriods { get; set; }
        public virtual DbSet<BankParamValues> BankParamValues { get; set; }
        public virtual DbSet<RefBranches> RefBranches { get; set; }
        public virtual DbSet<EFIGroupParamValuePeriods> IGroupParamValuePeriods { get; set; }
        public virtual DbSet<EFIGroupParamValues> IGroupParamValues { get; set; }
        public virtual DbSet<EFIGroups> IGroups { get; set; }
        public virtual DbSet<EFIInventory> IInventory { get; set; }
        public virtual DbSet<EFIInventoryParamValuePeriods> IInventoryParamValuePeriods { get; set; }
        public virtual DbSet<EFIInventoryParamValues> IInventoryParamValues { get; set; }
        public virtual DbSet<EFRefSysUnitClasses> RefSysUnitClasses { get; set; }
        public virtual DbSet<EFRefSysUnits> RefSysUnits { get; set; }
        public virtual DbSet<LSBranchItemValues> LSBranchItemValues { get; set; }
        public virtual DbSet<EFLSBankAccountItemValue> EFLSBankAccountItemValues { get; set; }
        public virtual DbSet<EFRefBankAccountParamValuePeriod> EFRefBankAccountParamValuePeriods { get; set; }
        public virtual DbSet<EFRefBankAccountParamValue> EFRefBankAccountParamValues { get; set; }
        public virtual DbSet<EFRefBankAccount> EFRefBankAccounts { get; set; }
        public virtual DbSet<EFLSAddressItemValue> EFLSAddressItemValues { get; set; }
        public virtual DbSet<MABATypeOptionalMarkerItems> MABATypeOptionalMarkerItems { get; set; }
        public virtual DbSet<MABATypeOptionalMarkers> MABATypeOptionalMarkers { get; set; }
        public virtual DbSet<EFvLSOrganisationLegalForm> vLSOrganisationLegalForms { get; set; }
        public virtual DbSet<EFvLSPersonLegalForm> vLSPersonLegalForms { get; set; }
        public virtual DbSet<EFBABaseAncestorsJournal> BABaseAncestors_Journal { get; set; }
        public virtual DbSet<EFWHInventoryTransaction> EFWHInventoryTransactions { get; set; }
        public virtual DbSet<EFPoACompanyAreasPlanOfAccount> EFPoACompanyAreasPlanOfAccounts { get; set; }
        public virtual DbSet<EFPORecType> EFPORecTypes { get; set; }
        public virtual DbSet<EFWHWarehouse> EFWHWarehouses { get; set; }
        public virtual DbSet<EFFADocAcceptFAssetsParamValuePeriods> FADocAcceptFAssetsParamValuePeriods { get; set; }
        public virtual DbSet<EFFADocAcceptFAssetsParamValues> FADocAcceptFAssetsParamValues { get; set; }
        public virtual DbSet<RIReceiptInvoiceRows> RIReceiptInvoiceRows { get; set; }
        public virtual DbSet<RIReceiptInvoicesParamValuePeriods> RIReceiptInvoicesParamValuePeriods { get; set; }
        public virtual DbSet<RIReceiptInvoicesParamValues> RIReceiptInvoicesParamValues { get; set; }
        public virtual DbSet<RIReceiptInvoices> RIReceiptInvoices { get; set; }
        public virtual DbSet<EFCAContract> CAContracts { get; set; }
        public virtual DbSet<EFFADocAcceptFAssetsRows> FADocAcceptFAssetsRows { get; set; }
        public virtual DbSet<EFFADocAcceptFAssets> FADocAcceptFAssets { get; set; }
        public virtual DbSet<EFMateriallyLiablePersons> MateriallyLiablePersons { get; set; }
        public virtual DbSet<FAssetsParamValuePeriods> FAssetsParamValuePeriods { get; set; }
        public virtual DbSet<FAssets> FAssets { get; set; }
        public virtual DbSet<EFvFALocations> vFALocations { get; set; }
        public virtual DbSet<EFFADocMovFAssetsParamValuePeriods> FADocMovFAssetsParamValuePeriods { get; set; }
        public virtual DbSet<EFFADocMovFAssetsParamValues> FADocMovFAssetsParamValues { get; set; }
        public virtual DbSet<EFMateriallyLiablePersonsParamValuePeriods> MateriallyLiablePersonsParamValuePeriods { get; set; }
        public virtual DbSet<EFMateriallyLiablePersonsParamValues> MateriallyLiablePersonsParamValues { get; set; }
        public virtual DbSet<EFFADocMovFAssets> FADocMovFAssets { get; set; }
        public virtual DbSet<EFUser> EFUsers { get; set; }
        public virtual DbSet<EFLSCompanyArea> EFLSCompanyAreas { get; set; }
        public virtual DbSet<EFCAContractInvent> EFCAContractInvents { get; set; }
        public virtual DbSet<EFFADocMovFAssetsRow> FADocMovFAssetsRows { get; set; }
        public virtual DbSet<EFvFAMovReasons> EFvFAMovReason { get; set; }
    
        public virtual ObjectResult<EFSystemSearchEntity> Desk_GetSearchEntities(Nullable<int> iSearchType, string sSearchText)
        {
            var iSearchTypeParameter = iSearchType.HasValue ?
                new ObjectParameter("iSearchType", iSearchType) :
                new ObjectParameter("iSearchType", typeof(int));
    
            var sSearchTextParameter = sSearchText != null ?
                new ObjectParameter("sSearchText", sSearchText) :
                new ObjectParameter("sSearchText", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<EFSystemSearchEntity>("Desk_GetSearchEntities", iSearchTypeParameter, sSearchTextParameter);
        }
    
        public virtual ObjectResult<EFSystemTreeEntity> Desk_GetTreeEntities(string sTreeName, Nullable<long> iTreeId, Nullable<long> iTreeParentId, Nullable<long> iParentBaId, Nullable<bool> isParent)
        {
            var sTreeNameParameter = sTreeName != null ?
                new ObjectParameter("sTreeName", sTreeName) :
                new ObjectParameter("sTreeName", typeof(string));
    
            var iTreeIdParameter = iTreeId.HasValue ?
                new ObjectParameter("iTreeId", iTreeId) :
                new ObjectParameter("iTreeId", typeof(long));
    
            var iTreeParentIdParameter = iTreeParentId.HasValue ?
                new ObjectParameter("iTreeParentId", iTreeParentId) :
                new ObjectParameter("iTreeParentId", typeof(long));
    
            var iParentBaIdParameter = iParentBaId.HasValue ?
                new ObjectParameter("iParentBaId", iParentBaId) :
                new ObjectParameter("iParentBaId", typeof(long));
    
            var isParentParameter = isParent.HasValue ?
                new ObjectParameter("isParent", isParent) :
                new ObjectParameter("isParent", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<EFSystemTreeEntity>("Desk_GetTreeEntities", sTreeNameParameter, iTreeIdParameter, iTreeParentIdParameter, iParentBaIdParameter, isParentParameter);
        }
    
        public virtual ObjectResult<BAFullTextSearchItem> GetFullTextSearchResults(string sFindStr)
        {
            var sFindStrParameter = sFindStr != null ?
                new ObjectParameter("sFindStr", sFindStr) :
                new ObjectParameter("sFindStr", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<BAFullTextSearchItem>("GetFullTextSearchResults", sFindStrParameter);
        }
    
        public virtual ObjectResult<AOAddress> AddressFind(string sFindStr, Nullable<int> iSourceTypeId)
        {
            var sFindStrParameter = sFindStr != null ?
                new ObjectParameter("sFindStr", sFindStr) :
                new ObjectParameter("sFindStr", typeof(string));
    
            var iSourceTypeIdParameter = iSourceTypeId.HasValue ?
                new ObjectParameter("iSourceTypeId", iSourceTypeId) :
                new ObjectParameter("iSourceTypeId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AOAddress>("AddressFind", sFindStrParameter, iSourceTypeIdParameter);
        }
    
        public virtual ObjectResult<FTSAddressList> GetFTSAddressList(Nullable<System.Guid> uAOId, Nullable<System.Guid> uHouseId, Nullable<System.Guid> uSteadId)
        {
            var uAOIdParameter = uAOId.HasValue ?
                new ObjectParameter("uAOId", uAOId) :
                new ObjectParameter("uAOId", typeof(System.Guid));
    
            var uHouseIdParameter = uHouseId.HasValue ?
                new ObjectParameter("uHouseId", uHouseId) :
                new ObjectParameter("uHouseId", typeof(System.Guid));
    
            var uSteadIdParameter = uSteadId.HasValue ?
                new ObjectParameter("uSteadId", uSteadId) :
                new ObjectParameter("uSteadId", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<FTSAddressList>("GetFTSAddressList", uAOIdParameter, uHouseIdParameter, uSteadIdParameter);
        }
    
        public virtual ObjectResult<FlatType> GetFlatTypeList(Nullable<bool> bIsVisible)
        {
            var bIsVisibleParameter = bIsVisible.HasValue ?
                new ObjectParameter("bIsVisible", bIsVisible) :
                new ObjectParameter("bIsVisible", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<FlatType>("GetFlatTypeList", bIsVisibleParameter);
        }
    
        public virtual ObjectResult<AOAddressObject> GetAddressObjectList(Nullable<System.Guid> uParentGuid, Nullable<int> iAOLevel, Nullable<int> iSourceTypeId)
        {
            var uParentGuidParameter = uParentGuid.HasValue ?
                new ObjectParameter("uParentGuid", uParentGuid) :
                new ObjectParameter("uParentGuid", typeof(System.Guid));
    
            var iAOLevelParameter = iAOLevel.HasValue ?
                new ObjectParameter("iAOLevel", iAOLevel) :
                new ObjectParameter("iAOLevel", typeof(int));
    
            var iSourceTypeIdParameter = iSourceTypeId.HasValue ?
                new ObjectParameter("iSourceTypeId", iSourceTypeId) :
                new ObjectParameter("iSourceTypeId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AOAddressObject>("GetAddressObjectList", uParentGuidParameter, iAOLevelParameter, iSourceTypeIdParameter);
        }
    
        public virtual ObjectResult<AOHouse> GetHouseList(Nullable<System.Guid> uParentGuid, Nullable<int> iSourceTypeId)
        {
            var uParentGuidParameter = uParentGuid.HasValue ?
                new ObjectParameter("uParentGuid", uParentGuid) :
                new ObjectParameter("uParentGuid", typeof(System.Guid));
    
            var iSourceTypeIdParameter = iSourceTypeId.HasValue ?
                new ObjectParameter("iSourceTypeId", iSourceTypeId) :
                new ObjectParameter("iSourceTypeId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AOHouse>("GetHouseList", uParentGuidParameter, iSourceTypeIdParameter);
        }
    
        public virtual ObjectResult<AOStead> GetSteadList(Nullable<System.Guid> uParentGuid, Nullable<int> iSourceTypeId)
        {
            var uParentGuidParameter = uParentGuid.HasValue ?
                new ObjectParameter("uParentGuid", uParentGuid) :
                new ObjectParameter("uParentGuid", typeof(System.Guid));
    
            var iSourceTypeIdParameter = iSourceTypeId.HasValue ?
                new ObjectParameter("iSourceTypeId", iSourceTypeId) :
                new ObjectParameter("iSourceTypeId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AOStead>("GetSteadList", uParentGuidParameter, iSourceTypeIdParameter);
        }
    
        public virtual ObjectResult<AOAddressResult> GetAddressList(Nullable<System.Guid> uAOId, Nullable<System.Guid> uHouseId, Nullable<System.Guid> uSteadId, Nullable<int> iActStateId, Nullable<int> iSourceTypeId)
        {
            var uAOIdParameter = uAOId.HasValue ?
                new ObjectParameter("uAOId", uAOId) :
                new ObjectParameter("uAOId", typeof(System.Guid));
    
            var uHouseIdParameter = uHouseId.HasValue ?
                new ObjectParameter("uHouseId", uHouseId) :
                new ObjectParameter("uHouseId", typeof(System.Guid));
    
            var uSteadIdParameter = uSteadId.HasValue ?
                new ObjectParameter("uSteadId", uSteadId) :
                new ObjectParameter("uSteadId", typeof(System.Guid));
    
            var iActStateIdParameter = iActStateId.HasValue ?
                new ObjectParameter("iActStateId", iActStateId) :
                new ObjectParameter("iActStateId", typeof(int));
    
            var iSourceTypeIdParameter = iSourceTypeId.HasValue ?
                new ObjectParameter("iSourceTypeId", iSourceTypeId) :
                new ObjectParameter("iSourceTypeId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AOAddressResult>("GetAddressList", uAOIdParameter, uHouseIdParameter, uSteadIdParameter, iActStateIdParameter, iSourceTypeIdParameter);
        }
    
        public virtual int AO_AddressObjectUpdate(ObjectParameter uAOId, ObjectParameter uAOGuid, string sFormalName, Nullable<int> iAOLevel, string sShortName, Nullable<System.Guid> uParentId, string xData)
        {
            var sFormalNameParameter = sFormalName != null ?
                new ObjectParameter("sFormalName", sFormalName) :
                new ObjectParameter("sFormalName", typeof(string));
    
            var iAOLevelParameter = iAOLevel.HasValue ?
                new ObjectParameter("iAOLevel", iAOLevel) :
                new ObjectParameter("iAOLevel", typeof(int));
    
            var sShortNameParameter = sShortName != null ?
                new ObjectParameter("sShortName", sShortName) :
                new ObjectParameter("sShortName", typeof(string));
    
            var uParentIdParameter = uParentId.HasValue ?
                new ObjectParameter("uParentId", uParentId) :
                new ObjectParameter("uParentId", typeof(System.Guid));
    
            var xDataParameter = xData != null ?
                new ObjectParameter("xData", xData) :
                new ObjectParameter("xData", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AO_AddressObjectUpdate", uAOId, uAOGuid, sFormalNameParameter, iAOLevelParameter, sShortNameParameter, uParentIdParameter, xDataParameter);
        }
    
        public virtual int AO_HouseUpdate(ObjectParameter uHouseId, Nullable<System.Guid> uHouseGuid, string sHouseNum, string sBuildNum, string sStrucNum, Nullable<int> iStrStateId, Nullable<int> iEstStateId, Nullable<System.Guid> uAOId, string xData)
        {
            var uHouseGuidParameter = uHouseGuid.HasValue ?
                new ObjectParameter("uHouseGuid", uHouseGuid) :
                new ObjectParameter("uHouseGuid", typeof(System.Guid));
    
            var sHouseNumParameter = sHouseNum != null ?
                new ObjectParameter("sHouseNum", sHouseNum) :
                new ObjectParameter("sHouseNum", typeof(string));
    
            var sBuildNumParameter = sBuildNum != null ?
                new ObjectParameter("sBuildNum", sBuildNum) :
                new ObjectParameter("sBuildNum", typeof(string));
    
            var sStrucNumParameter = sStrucNum != null ?
                new ObjectParameter("sStrucNum", sStrucNum) :
                new ObjectParameter("sStrucNum", typeof(string));
    
            var iStrStateIdParameter = iStrStateId.HasValue ?
                new ObjectParameter("iStrStateId", iStrStateId) :
                new ObjectParameter("iStrStateId", typeof(int));
    
            var iEstStateIdParameter = iEstStateId.HasValue ?
                new ObjectParameter("iEstStateId", iEstStateId) :
                new ObjectParameter("iEstStateId", typeof(int));
    
            var uAOIdParameter = uAOId.HasValue ?
                new ObjectParameter("uAOId", uAOId) :
                new ObjectParameter("uAOId", typeof(System.Guid));
    
            var xDataParameter = xData != null ?
                new ObjectParameter("xData", xData) :
                new ObjectParameter("xData", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AO_HouseUpdate", uHouseId, uHouseGuidParameter, sHouseNumParameter, sBuildNumParameter, sStrucNumParameter, iStrStateIdParameter, iEstStateIdParameter, uAOIdParameter, xDataParameter);
        }
    
        public virtual int AO_SteadUpdate(ObjectParameter uSteadId, ObjectParameter uSteadGuid, string sNumber, Nullable<System.Guid> uParentId, string xData)
        {
            var sNumberParameter = sNumber != null ?
                new ObjectParameter("sNumber", sNumber) :
                new ObjectParameter("sNumber", typeof(string));
    
            var uParentIdParameter = uParentId.HasValue ?
                new ObjectParameter("uParentId", uParentId) :
                new ObjectParameter("uParentId", typeof(System.Guid));
    
            var xDataParameter = xData != null ?
                new ObjectParameter("xData", xData) :
                new ObjectParameter("xData", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AO_SteadUpdate", uSteadId, uSteadGuid, sNumberParameter, uParentIdParameter, xDataParameter);
        }
    
        public virtual ObjectResult<AddressUnsettleList> GetAddressUnsettleList()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AddressUnsettleList>("GetAddressUnsettleList");
        }
    
        public virtual ObjectResult<AOAddressData> GetAddressData(Nullable<long> iBAId)
        {
            var iBAIdParameter = iBAId.HasValue ?
                new ObjectParameter("iBAId", iBAId) :
                new ObjectParameter("iBAId", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AOAddressData>("GetAddressData", iBAIdParameter);
        }
    
        public virtual int RF_BankParamUpdate(Nullable<long> iBAId, Nullable<int> iMarkerId, ObjectParameter dStartDate, ObjectParameter dEndDate, string sNote)
        {
            var iBAIdParameter = iBAId.HasValue ?
                new ObjectParameter("iBAId", iBAId) :
                new ObjectParameter("iBAId", typeof(long));
    
            var iMarkerIdParameter = iMarkerId.HasValue ?
                new ObjectParameter("iMarkerId", iMarkerId) :
                new ObjectParameter("iMarkerId", typeof(int));
    
            var sNoteParameter = sNote != null ?
                new ObjectParameter("sNote", sNote) :
                new ObjectParameter("sNote", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("RF_BankParamUpdate", iBAIdParameter, iMarkerIdParameter, dStartDate, dEndDate, sNoteParameter);
        }
    
        public virtual ObjectResult<AO_GetAdvancedSearchList_Result> AO_GetAdvancedSearchList(Nullable<System.Guid> uAOId, Nullable<System.Guid> uHouseId, Nullable<System.Guid> uSteadId, Nullable<int> iActStateId, Nullable<int> iSourceTypeId, string sPostCode, string sCadNum)
        {
            var uAOIdParameter = uAOId.HasValue ?
                new ObjectParameter("uAOId", uAOId) :
                new ObjectParameter("uAOId", typeof(System.Guid));
    
            var uHouseIdParameter = uHouseId.HasValue ?
                new ObjectParameter("uHouseId", uHouseId) :
                new ObjectParameter("uHouseId", typeof(System.Guid));
    
            var uSteadIdParameter = uSteadId.HasValue ?
                new ObjectParameter("uSteadId", uSteadId) :
                new ObjectParameter("uSteadId", typeof(System.Guid));
    
            var iActStateIdParameter = iActStateId.HasValue ?
                new ObjectParameter("iActStateId", iActStateId) :
                new ObjectParameter("iActStateId", typeof(int));
    
            var iSourceTypeIdParameter = iSourceTypeId.HasValue ?
                new ObjectParameter("iSourceTypeId", iSourceTypeId) :
                new ObjectParameter("iSourceTypeId", typeof(int));
    
            var sPostCodeParameter = sPostCode != null ?
                new ObjectParameter("sPostCode", sPostCode) :
                new ObjectParameter("sPostCode", typeof(string));
    
            var sCadNumParameter = sCadNum != null ?
                new ObjectParameter("sCadNum", sCadNum) :
                new ObjectParameter("sCadNum", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AO_GetAdvancedSearchList_Result>("AO_GetAdvancedSearchList", uAOIdParameter, uHouseIdParameter, uSteadIdParameter, iActStateIdParameter, iSourceTypeIdParameter, sPostCodeParameter, sCadNumParameter);
        }
    
        public virtual ObjectResult<AO_HouseData_Result> AO_HouseData(Nullable<System.Guid> uHouseGuid)
        {
            var uHouseGuidParameter = uHouseGuid.HasValue ?
                new ObjectParameter("uHouseGuid", uHouseGuid) :
                new ObjectParameter("uHouseGuid", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AO_HouseData_Result>("AO_HouseData", uHouseGuidParameter);
        }
    
        public virtual ObjectResult<AO_SteadData_Result> AO_SteadData(Nullable<System.Guid> uSteadGuid)
        {
            var uSteadGuidParameter = uSteadGuid.HasValue ?
                new ObjectParameter("uSteadGuid", uSteadGuid) :
                new ObjectParameter("uSteadGuid", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AO_SteadData_Result>("AO_SteadData", uSteadGuidParameter);
        }
    
        public virtual int InventoryCopy(ObjectParameter iBAId, Nullable<long> iParentId, Nullable<long> iCAId, Nullable<long> iFromBAId, Nullable<System.DateTime> dStartDate)
        {
            var iParentIdParameter = iParentId.HasValue ?
                new ObjectParameter("iParentId", iParentId) :
                new ObjectParameter("iParentId", typeof(long));
    
            var iCAIdParameter = iCAId.HasValue ?
                new ObjectParameter("iCAId", iCAId) :
                new ObjectParameter("iCAId", typeof(long));
    
            var iFromBAIdParameter = iFromBAId.HasValue ?
                new ObjectParameter("iFromBAId", iFromBAId) :
                new ObjectParameter("iFromBAId", typeof(long));
    
            var dStartDateParameter = dStartDate.HasValue ?
                new ObjectParameter("dStartDate", dStartDate) :
                new ObjectParameter("dStartDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("InventoryCopy", iBAId, iParentIdParameter, iCAIdParameter, iFromBAIdParameter, dStartDateParameter);
        }
    
        public virtual int InventoryParamDelete(Nullable<long> iBAId, Nullable<int> iMarkerId, Nullable<System.DateTime> dStartDate)
        {
            var iBAIdParameter = iBAId.HasValue ?
                new ObjectParameter("iBAId", iBAId) :
                new ObjectParameter("iBAId", typeof(long));
    
            var iMarkerIdParameter = iMarkerId.HasValue ?
                new ObjectParameter("iMarkerId", iMarkerId) :
                new ObjectParameter("iMarkerId", typeof(int));
    
            var dStartDateParameter = dStartDate.HasValue ?
                new ObjectParameter("dStartDate", dStartDate) :
                new ObjectParameter("dStartDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("InventoryParamDelete", iBAIdParameter, iMarkerIdParameter, dStartDateParameter);
        }
    
        public virtual int InventoryParamUpdate(Nullable<long> iBAId, Nullable<int> iMarkerId, ObjectParameter dStartDate, ObjectParameter dEndDate, string sNote)
        {
            var iBAIdParameter = iBAId.HasValue ?
                new ObjectParameter("iBAId", iBAId) :
                new ObjectParameter("iBAId", typeof(long));
    
            var iMarkerIdParameter = iMarkerId.HasValue ?
                new ObjectParameter("iMarkerId", iMarkerId) :
                new ObjectParameter("iMarkerId", typeof(int));
    
            var sNoteParameter = sNote != null ?
                new ObjectParameter("sNote", sNote) :
                new ObjectParameter("sNote", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("InventoryParamUpdate", iBAIdParameter, iMarkerIdParameter, dStartDate, dEndDate, sNoteParameter);
        }
    
        public virtual int InventoryUpdate(ObjectParameter iBAId, string sCode, string sName, Nullable<long> iParentId, Nullable<long> iCAId, Nullable<int> iKindId, Nullable<int> iTypeId, Nullable<int> iResourceTypeId, Nullable<int> iBuyUnitId, Nullable<int> iSaleUnitId, Nullable<int> iStockUnitId)
        {
            var sCodeParameter = sCode != null ?
                new ObjectParameter("sCode", sCode) :
                new ObjectParameter("sCode", typeof(string));
    
            var sNameParameter = sName != null ?
                new ObjectParameter("sName", sName) :
                new ObjectParameter("sName", typeof(string));
    
            var iParentIdParameter = iParentId.HasValue ?
                new ObjectParameter("iParentId", iParentId) :
                new ObjectParameter("iParentId", typeof(long));
    
            var iCAIdParameter = iCAId.HasValue ?
                new ObjectParameter("iCAId", iCAId) :
                new ObjectParameter("iCAId", typeof(long));
    
            var iKindIdParameter = iKindId.HasValue ?
                new ObjectParameter("iKindId", iKindId) :
                new ObjectParameter("iKindId", typeof(int));
    
            var iTypeIdParameter = iTypeId.HasValue ?
                new ObjectParameter("iTypeId", iTypeId) :
                new ObjectParameter("iTypeId", typeof(int));
    
            var iResourceTypeIdParameter = iResourceTypeId.HasValue ?
                new ObjectParameter("iResourceTypeId", iResourceTypeId) :
                new ObjectParameter("iResourceTypeId", typeof(int));
    
            var iBuyUnitIdParameter = iBuyUnitId.HasValue ?
                new ObjectParameter("iBuyUnitId", iBuyUnitId) :
                new ObjectParameter("iBuyUnitId", typeof(int));
    
            var iSaleUnitIdParameter = iSaleUnitId.HasValue ?
                new ObjectParameter("iSaleUnitId", iSaleUnitId) :
                new ObjectParameter("iSaleUnitId", typeof(int));
    
            var iStockUnitIdParameter = iStockUnitId.HasValue ?
                new ObjectParameter("iStockUnitId", iStockUnitId) :
                new ObjectParameter("iStockUnitId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("InventoryUpdate", iBAId, sCodeParameter, sNameParameter, iParentIdParameter, iCAIdParameter, iKindIdParameter, iTypeIdParameter, iResourceTypeIdParameter, iBuyUnitIdParameter, iSaleUnitIdParameter, iStockUnitIdParameter);
        }
    
        public virtual ObjectResult<MABA_TypeOptionalMarkerList_Result> MABA_TypeOptionalMarkerList(Nullable<int> iBATypeId, Nullable<int> iMarkerId, Nullable<int> iItemId)
        {
            var iBATypeIdParameter = iBATypeId.HasValue ?
                new ObjectParameter("iBATypeId", iBATypeId) :
                new ObjectParameter("iBATypeId", typeof(int));
    
            var iMarkerIdParameter = iMarkerId.HasValue ?
                new ObjectParameter("iMarkerId", iMarkerId) :
                new ObjectParameter("iMarkerId", typeof(int));
    
            var iItemIdParameter = iItemId.HasValue ?
                new ObjectParameter("iItemId", iItemId) :
                new ObjectParameter("iItemId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MABA_TypeOptionalMarkerList_Result>("MABA_TypeOptionalMarkerList", iBATypeIdParameter, iMarkerIdParameter, iItemIdParameter);
        }
    
        [DbFunction("HISModelEntities", "GetHISUsers")]
        public virtual IQueryable<GetHISUsers_Result> GetHISUsers()
        {
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<GetHISUsers_Result>("[HISModelEntities].[GetHISUsers]()");
        }
    
        public virtual ObjectResult<EFBAMarkerValueEntity> GetMarkerValueSelect(Nullable<long> iBAId, Nullable<bool> bIsCollectible)
        {
            var iBAIdParameter = iBAId.HasValue ?
                new ObjectParameter("iBAId", iBAId) :
                new ObjectParameter("iBAId", typeof(long));
    
            var bIsCollectibleParameter = bIsCollectible.HasValue ?
                new ObjectParameter("bIsCollectible", bIsCollectible) :
                new ObjectParameter("bIsCollectible", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<EFBAMarkerValueEntity>("GetMarkerValueSelect", iBAIdParameter, bIsCollectibleParameter);
        }
    
        public virtual int DeleteParamDocAccepеAssets(Nullable<long> iBAId, Nullable<int> iMarkerId, Nullable<System.DateTime> dStartDate)
        {
            var iBAIdParameter = iBAId.HasValue ?
                new ObjectParameter("iBAId", iBAId) :
                new ObjectParameter("iBAId", typeof(long));
    
            var iMarkerIdParameter = iMarkerId.HasValue ?
                new ObjectParameter("iMarkerId", iMarkerId) :
                new ObjectParameter("iMarkerId", typeof(int));
    
            var dStartDateParameter = dStartDate.HasValue ?
                new ObjectParameter("dStartDate", dStartDate) :
                new ObjectParameter("dStartDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteParamDocAccepеAssets", iBAIdParameter, iMarkerIdParameter, dStartDateParameter);
        }
    
        public virtual int UpdateParamDocAcceptAssets(Nullable<long> iBAId, Nullable<int> iMarkerId, ObjectParameter dStartDate, ObjectParameter dEndDate, string sNote)
        {
            var iBAIdParameter = iBAId.HasValue ?
                new ObjectParameter("iBAId", iBAId) :
                new ObjectParameter("iBAId", typeof(long));
    
            var iMarkerIdParameter = iMarkerId.HasValue ?
                new ObjectParameter("iMarkerId", iMarkerId) :
                new ObjectParameter("iMarkerId", typeof(int));
    
            var sNoteParameter = sNote != null ?
                new ObjectParameter("sNote", sNote) :
                new ObjectParameter("sNote", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateParamDocAcceptAssets", iBAIdParameter, iMarkerIdParameter, dStartDate, dEndDate, sNoteParameter);
        }
    
        public virtual int DeleteDocAcceptAssetsRows(Nullable<long> iRecId, Nullable<long> iFADocAcceptId)
        {
            var iRecIdParameter = iRecId.HasValue ?
                new ObjectParameter("iRecId", iRecId) :
                new ObjectParameter("iRecId", typeof(long));
    
            var iFADocAcceptIdParameter = iFADocAcceptId.HasValue ?
                new ObjectParameter("iFADocAcceptId", iFADocAcceptId) :
                new ObjectParameter("iFADocAcceptId", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteDocAcceptAssetsRows", iRecIdParameter, iFADocAcceptIdParameter);
        }
    
        public virtual ObjectResult<ReceiptInvoiceInventSelect> GetReceiptInvoiceInventSelect(Nullable<long> iDocId)
        {
            var iDocIdParameter = iDocId.HasValue ?
                new ObjectParameter("iDocId", iDocId) :
                new ObjectParameter("iDocId", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<ReceiptInvoiceInventSelect>("GetReceiptInvoiceInventSelect", iDocIdParameter);
        }
    
        public virtual ObjectResult<ReceiptInvoiceSelect> GetReceiptInvoiceSelect(Nullable<long> iCAId)
        {
            var iCAIdParameter = iCAId.HasValue ?
                new ObjectParameter("iCAId", iCAId) :
                new ObjectParameter("iCAId", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<ReceiptInvoiceSelect>("GetReceiptInvoiceSelect", iCAIdParameter);
        }
    
        public virtual int UpdateDocAcceptAssetsRows(ObjectParameter iRecId, Nullable<long> iFADocAcceptId, Nullable<long> iInventoryId, string sInventoryNumber, string sSerialNumber, Nullable<System.DateTime> dReleaseDate, Nullable<System.DateTime> dCheckDate, string sBarCode, Nullable<long> iEmployeeId)
        {
            var iFADocAcceptIdParameter = iFADocAcceptId.HasValue ?
                new ObjectParameter("iFADocAcceptId", iFADocAcceptId) :
                new ObjectParameter("iFADocAcceptId", typeof(long));
    
            var iInventoryIdParameter = iInventoryId.HasValue ?
                new ObjectParameter("iInventoryId", iInventoryId) :
                new ObjectParameter("iInventoryId", typeof(long));
    
            var sInventoryNumberParameter = sInventoryNumber != null ?
                new ObjectParameter("sInventoryNumber", sInventoryNumber) :
                new ObjectParameter("sInventoryNumber", typeof(string));
    
            var sSerialNumberParameter = sSerialNumber != null ?
                new ObjectParameter("sSerialNumber", sSerialNumber) :
                new ObjectParameter("sSerialNumber", typeof(string));
    
            var dReleaseDateParameter = dReleaseDate.HasValue ?
                new ObjectParameter("dReleaseDate", dReleaseDate) :
                new ObjectParameter("dReleaseDate", typeof(System.DateTime));
    
            var dCheckDateParameter = dCheckDate.HasValue ?
                new ObjectParameter("dCheckDate", dCheckDate) :
                new ObjectParameter("dCheckDate", typeof(System.DateTime));
    
            var sBarCodeParameter = sBarCode != null ?
                new ObjectParameter("sBarCode", sBarCode) :
                new ObjectParameter("sBarCode", typeof(string));
    
            var iEmployeeIdParameter = iEmployeeId.HasValue ?
                new ObjectParameter("iEmployeeId", iEmployeeId) :
                new ObjectParameter("iEmployeeId", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateDocAcceptAssetsRows", iRecId, iFADocAcceptIdParameter, iInventoryIdParameter, sInventoryNumberParameter, sSerialNumberParameter, dReleaseDateParameter, dCheckDateParameter, sBarCodeParameter, iEmployeeIdParameter);
        }
    
        public virtual int UpdateDocAcceptAssets(ObjectParameter iBAId, string sNumber, Nullable<System.DateTime> dDocDate, Nullable<long> iCAId, Nullable<long> iLSId, Nullable<long> iWHId, Nullable<long> iRIId, string sEvidence, string sNote)
        {
            var sNumberParameter = sNumber != null ?
                new ObjectParameter("sNumber", sNumber) :
                new ObjectParameter("sNumber", typeof(string));
    
            var dDocDateParameter = dDocDate.HasValue ?
                new ObjectParameter("dDocDate", dDocDate) :
                new ObjectParameter("dDocDate", typeof(System.DateTime));
    
            var iCAIdParameter = iCAId.HasValue ?
                new ObjectParameter("iCAId", iCAId) :
                new ObjectParameter("iCAId", typeof(long));
    
            var iLSIdParameter = iLSId.HasValue ?
                new ObjectParameter("iLSId", iLSId) :
                new ObjectParameter("iLSId", typeof(long));
    
            var iWHIdParameter = iWHId.HasValue ?
                new ObjectParameter("iWHId", iWHId) :
                new ObjectParameter("iWHId", typeof(long));
    
            var iRIIdParameter = iRIId.HasValue ?
                new ObjectParameter("iRIId", iRIId) :
                new ObjectParameter("iRIId", typeof(long));
    
            var sEvidenceParameter = sEvidence != null ?
                new ObjectParameter("sEvidence", sEvidence) :
                new ObjectParameter("sEvidence", typeof(string));
    
            var sNoteParameter = sNote != null ?
                new ObjectParameter("sNote", sNote) :
                new ObjectParameter("sNote", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateDocAcceptAssets", iBAId, sNumberParameter, dDocDateParameter, iCAIdParameter, iLSIdParameter, iWHIdParameter, iRIIdParameter, sEvidenceParameter, sNoteParameter);
        }
    }
}

﻿using HIS.DAL.DB.Context;
using System;
using HIS.DAL.Client.Services.Auth;
using HIS.DAL.DB.Handlers.Auth;
using Unity;

namespace HIS.DAL.DB
{
    /// <summary>
    /// Реализация процесса выполнения обработчиков хранилища данных 
    /// </summary>
    public class DataContextHandlerProcess: IDataContextHandlerProcess
    {
        readonly IUnityContainer _resolver;
        private readonly HISModelEntities _dbContext;
        private IAuthService _authServie;

        public DataContextHandlerProcess(IUnityContainer resolver)
        {
            _resolver = resolver;

            _authServie = _resolver.Resolve<IAuthService>();
            var authHandler = _resolver.Resolve<IAuthHandler>();
            string connectionString = authHandler.GetCurrentDBConnectionString();
            if (!string.IsNullOrEmpty(connectionString))
            {
                _dbContext = new HISModelEntities(connectionString);
            }
        }

        /// <summary>
        /// Получение обработчика работы с данными хранилища
        /// </summary>
        /// <typeparam name="THandler">Интерфейс обработчика</typeparam>
        /// <returns>Экземпляр обработчика</returns>
        public THandler GetHandler<THandler>() where THandler : class, IDataContextHandler
        {
            if (_dbContext == null)
                throw new NotSupportedException("Handler content not found!");

            var handler = _resolver.Resolve<THandler>();
            handler.InitializeContext(_dbContext);
            return handler;
        }

        /// <summary>
        /// Получение обработчика без инициализации контекста БД
        /// </summary>
        /// <typeparam name="THandler">Интерфейс обработчика</typeparam>
        /// <returns>Экземпляр обработчика</returns>
        public THandler GetHadlerWithoutContext<THandler>() where THandler : class, IDataContextHandler
        {
            var handler = _resolver.Resolve<THandler>();            
            return handler;
        }


        /// <summary>
        /// Выполнить действие в рамках текущего процесса в транзакции
        /// </summary>
        /// <param name="func">Делегат действия</param>
        public TResult DoWithTransaction<TResult>(Func<IDataContextHandlerProcess, TResult> func)
        {
            int userId = _authServie.GetCurrentUserId();
            _dbContext.Database.Connection.Open();
            using (var transactionContext = _dbContext.Database.BeginTransaction())
            {
                try
                {
                    //if (user != 0)
                    this.SetCurrentUser(userId);

                    TResult result = func(this);

                    ClearCurrentUser();
                    transactionContext.Commit();
                    return result;
                }
                catch (Exception)
                {
                    //TODO can be an entry point for writing to the log ???

                    transactionContext.Rollback();
                    throw;
                }
                //finally
                //{
                //    DetachAll();
                //}
            }
        }

        /// <summary>
        /// Выполнить действие в рамках текущего процесса
        /// </summary>
        /// <param name="func">Делегат действия</param>
        public TResult Do<TResult>(Func<IDataContextHandlerProcess, TResult> func)
        {
            _dbContext?.Database.Connection.Open();

            try
            {
                TResult result = func(this);
                return result;
            }
            catch (Exception)
            {
                //TODO can be an entry point for writing to the log ???
                throw;
            }
        }

        //private void DetachAll()
        //{
        //    foreach (DbEntityEntry entityEntry in _dbContext.ChangeTracker.Entries().ToArray())
        //    {
        //        if (entityEntry.Entity != null)
        //        {
        //            entityEntry.State = EntityState.Detached;
        //        }
        //    }
        //}

        private void SetCurrentUser(int userId)
        {
            string sql = $@"CREATE TABLE #tUserData ( UserId INT )
                            DECLARE @UserId INT = {userId}
                            INSERT INTO #tUserData
                            SELECT UserId = @UserId";

            _dbContext.Database.ExecuteSqlCommand(sql);
        }

        private void ClearCurrentUser()
        {
            string sql = $@"IF OBJECT_ID('tempdb..#tUserData') IS NOT NULL 
                                DROP TABLE #tUserData";

            _dbContext.Database.ExecuteSqlCommand(sql);
        }

        public void Dispose()
        {
            _dbContext?.Database.Connection.Close();
            _dbContext?.Dispose();
        }
    }
}

﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.QueryBuilder;

namespace HIS.DAL.DB.Handlers.QueryBuilder
{
    public interface IQueryBuilderHandler : IDataContextHandler
    {
        EntityQueryBuilderInfo GetEntityQueryBuilderInfo(long baTypeId);
        IEnumerable<MarkersQueryBuilder> GetMarkersForBaTypeId(long baTypeId);
        IEnumerable<TreeTypes> GetTreeTypeses(int baTypeId);
        string Execute(string sql, IEnumerable<string> fields, string rowCount);
        string CalculateRowCount(string sql);
    }
}

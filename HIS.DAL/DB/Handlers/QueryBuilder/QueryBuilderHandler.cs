﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using HIS.DAL.Client.Models.QueryBuilder;
using Newtonsoft.Json;

namespace HIS.DAL.DB.Handlers.QueryBuilder
{
    public class QueryBuilderHandler : DataContextHandlerBase, IQueryBuilderHandler
    {
        public EntityQueryBuilderInfo GetEntityQueryBuilderInfo(long baTypeId)
        {
            try
            {
                var efData = DataContext.EFBATypes.FirstOrDefault(b => b.TypeId == baTypeId);
                return new EntityQueryBuilderInfo
                {
                    Name = efData.Name,
                    TypeId = efData.TypeId,
                    ImplementTypeName = efData.ImplementTypeName,
                    ParentId = efData.ParentId.ToString(),
                    MvcAlias = efData.MVCAlias
                };
            }
            catch (Exception e)
            {
                throw new Exception($"Не удалось получить информацию по batypeId = {baTypeId} подробнее в InnerException", e);
            }
        }

        public IEnumerable<MarkersQueryBuilder> GetMarkersForBaTypeId(long baTypeId)
        {
            var sql = $@"SELECT 
                        	m.MarkerId,
                        	IsNull(mm.Name, m.Name) as Name, 
                        	m.MVCAlias,
                        	mm.ImplementTypeName,
                        	isNull(mm.ImplementTypeField, 'sValue') as ImplementTypeField, 
                        	m.BATypeId, 
                        	mm.IsPeriodic, 
                        	mt.SQLServerType,
                        	m.TypeId AS 'MarkerTypeId',
                        	m.ImplementTypeName AS 'MarkerView',
                        	b.ImplementTypeName AS 'LinkedBaTypeImplaiment',
                        	mmLink.ImplementTypeField AS 'LinkedBaTypeFieldName'
                        FROM MABATypeMarkers AS mm
                        INNER JOIN MAMarkers AS m 
                        	ON m.MarkerId = mm.MarkerId 
                        INNER JOIN MAMarkerTypes AS mt 
                        	ON  m.TypeId = mt.TypeId
                        LEFT JOIN BATypes AS b
                        	ON m.BATypeId = b.TypeId
                        LEFT JOIN MABATypeMarkers AS mmLink
                        	ON mmLink.BATypeId = m.BATypeId
                        	AND mmLink.MarkerId = 1
                        WHERE mm.BATypeId = {baTypeId}
                        and mm.IsCollectible = 0";

            var data = DataContext.Database.SqlQuery<MarkersQueryBuilder>(sql);
            return data.ToList();
        }

        public IEnumerable<TreeTypes> GetTreeTypeses(int baTypeId)
        {
            var sql = $@"
                DECLARE @SSQL NVARCHAR(MAX) = '';  
                DECLARE @BATypeId INT = {baTypeId};
                DECLARE @StartDate DATE = GETDATE();
                DECLARE @EndDate DATE = DATEADD(YEAR, -100, @StartDate);
                DECLARE @CAId INT = 51416;
                DECLARE @DocTree TABLE (BATypeId INT, [NAME] NVARCHAR(400), ParentId INT, TreeLevel INT, BranchId INT);
                DECLARE @RowExists TABLE (BATypeId INT, IsExists BIT);
                WITH DocTree AS (
                 SELECT BATypeId = TypeId, [NAME], ParentId = NULL, TreeLevel = 0, BranchId = TypeId, ImplementTypeName, MVCAlias  
                 FROM  dbo.BATypes WHERE TypeId = @BATypeId
                 UNION ALL 
                 SELECT BATypeId = bt.TypeId, [NAME] = bt.[Name], ParentId = dt.BATypeId, TreeLevel = dt.TreeLevel + 1, BranchId = CASE WHEN dt.TreeLevel > 0 THEN dt.BranchId ELSE bt.TypeId END, bt.ImplementTypeName, bt.MVCAlias  
                 FROM dbo.BATypes bt  
                    INNER JOIN DocTree dt ON dt.BATypeId = bt.ParentId             
                )  
                SELECT dt.BATypeId, dt.[NAME], dt.ParentId, dt.TreeLevel, dt.BranchId, dt.ImplementTypeName, dt.MVCAlias
                FROM DocTree dt";

            var data = DataContext.Database.SqlQuery<TreeTypes>(sql);
            return data.ToList();
        }

        private string FormatJsonFromDataReader(IDataReader reader, IEnumerable<string> fields, string rowCount)
        {
            var sb = new StringBuilder("{ \"items\": [ ");

            while (reader.Read())
            {
                int i = 0;
                sb.Append("{ ");
                foreach (var field in fields)
                {
                    var value = reader[i];
                    sb.Append($"\"{field}\"")
                        .Append(": ")
                        .Append(JsonConvert.SerializeObject(value))
                        .Append(",");
                    i++;
                }

                sb.Remove(sb.Length - 1, 1);//убираем последнюю запятую
                sb.Append(" },");
            }

            sb.Remove(sb.Length - 1, 1);//убираем последнюю запятую
            sb.Append($" ], \"rowCount\": {rowCount}");
            sb.Append("}");
            var result = sb.ToString();
            sb.Clear();

            return result;
        }

        public string Execute(string sql, IEnumerable<string> fields, string rowCount)
        {
            string result;

            try
            { 
                var command = DataContext.Database.Connection.CreateCommand();
                command.CommandText = sql;
                using (DataContext.Database.Connection)
                using (var reader = command.ExecuteReader(CommandBehavior.SequentialAccess))
                {
                    result = FormatJsonFromDataReader(reader, fields, rowCount);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public string CalculateRowCount(string sql)
        {
            var result = string.Empty;

            var command = DataContext.Database.Connection.CreateCommand();
            command.CommandText = $"SELECT COUNT(*) FROM ( {ExecludeOrderBySectionFromQuery(sql)} ) as temp";
            try
            {
                using (DataContext.Database.Connection)
                using (var reader = command.ExecuteReader(CommandBehavior.SequentialAccess))
                {
                    while (reader.Read())
                    {
                        result = reader[0].ToString();
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        private string ExecludeOrderBySectionFromQuery(string sql)
        {
            var indexorderby = sql.IndexOf("ORDER BY", StringComparison.Ordinal);

            sql = sql.Substring(0, indexorderby);

            return sql;
        }
    }

    public class TreeTypes
    {
        public string MVCAlias { get; set; }
        public int? BATypeId { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int? TreeLevel { get; set; }
        public int? BranchId { get; set; } 
        public string ImplementTypeName { get; set; }
    }
}

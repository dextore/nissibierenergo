﻿using System;
using System.Text;

namespace HIS.DAL.DB.Handlers.Documents
{
    internal static class QueryHelper
    {
        private const int CompanyAreaMarker = 6;
        private const int DocDateMarker = 17;

        // Просто дерево
        private static string GetDocTypesTree(int baTypeId)
        {
            var sb = new StringBuilder();
            sb.AppendLine($@"DECLARE @BATypeId INT = {baTypeId};");
            sb.AppendLine(@"WITH DocTree AS (
	                        SELECT 
                               BATypeId = TypeId, 
                               [NAME], ParentId = NULL, 
                               TreeLevel = 0, 
                               BranchId = TypeId  
	                        FROM  dbo.BATypes WHERE TypeId = @BATypeId
	                        UNION ALL 
	                        SELECT 
                               BATypeId = bt.TypeId, 
                               [NAME] = bt.[Name], 
                               ParentId = dt.BATypeId, 
                               TreeLevel = dt.TreeLevel + 1, 
                               BranchId = CASE WHEN dt.TreeLevel > 0 THEN dt.BranchId ELSE bt.TypeId END  
	                        FROM dbo.BATypes bt  
                            INNER JOIN DocTree dt ON dt.BATypeId = bt.ParentId)");
            return sb.ToString();
        }

        internal static string GetAllDocTypesTree(int baTypeId)
        {
            var sb = new StringBuilder();
            sb.AppendLine(GetDocTypesTree(baTypeId));
            sb.AppendLine(@"SELECT BATypeId, [NAME], ParentId, TreeLevel, BranchId FROM DocTree;");
            return sb.ToString();
        }

        // Список типов документов с учетом даты и организации с флагом есть или нет документы
        internal static string GetDocTypesExists(int baTypeId, long companyAreaId, DateTime startDate, DateTime endDate)
        {
            var sb = new StringBuilder();

            sb.AppendLine(@"SET NOCOUNT ON");

            sb.AppendLine(@"DECLARE @DocTree TABLE(BATypeId INT, [NAME] NVARCHAR(400), ParentId INT, TreeLevel INT, BranchId INT)");
            sb.AppendLine(GetDocTypesTree(baTypeId));
            sb.AppendLine(@"INSERT INTO @DocTree
                            SELECT dt.BATypeId, dt.[NAME], dt.ParentId, dt.TreeLevel, dt.BranchId  
                            FROM DocTree dt;");

            sb.AppendLine($@"DECLARE @CAId INT = {companyAreaId}");
            sb.AppendLine($@"DECLARE @StartDate DATE = '{startDate}'");
            sb.AppendLine($@"DECLARE @EndDate DATE = '{endDate}'");
            sb.AppendLine(@"DECLARE @RowExists TABLE(BATypeId INT, IsExists BIT)");
            sb.AppendLine(@"DECLARE @SSQL NVARCHAR(MAX) = '';");

            sb.AppendLine(@"WITH DocTree2 AS (
	                            SELECT BATypeId = TypeId, [NAME], ParentId = NULL, TreeLevel = 0, ImplementTypeName 
	                            FROM  dbo.BATypes WHERE TypeId = @BATypeId
	                            UNION ALL 
	                            SELECT BATypeId = bt.TypeId, [NAME] = bt.[Name], ParentId = dt.BATypeId, TreeLevel = dt.TreeLevel + 1, bt.ImplementTypeName 
	                            FROM dbo.BATypes bt  
                                INNER JOIN DocTree2 dt ON dt.BATypeId = bt.ParentId            	
                            )
                            SELECT @SSQL = @SSQL + N' UNION SELECT BATypeId = '+CAST(tm.BATypeId AS NVARCHAR)+', IsExists = CASE WHEN EXISTS( SELECT 1 FROM dbo.'+tm.ImplementTypeName+' WHERE '
                                                          +tm.ImplementTypeField+' >= '''+CONVERT(nvarchar(30), @StartDate, 112)+''' AND '+tm.ImplementTypeField+' <= '''+CONVERT(nvarchar(30), @EndDate, 112)
                                                          +''' AND '+ TT.ImplementTypeField + ' = ' + CAST(@CAId AS NVARCHAR) +' ) THEN 1 ELSE 0 END'
                            FROM MABATypeMarkers tm
                            LEFT JOIN 
                            (
                                SELECT tm.BATypeId, tm.ImplementTypeField 
                                FROM MABATypeMarkers tm
                                INNER JOIN ( SELECT BATypeId FROM DocTree2 ) T ON T.BATypeId = tm.BATypeId
                                WHERE tm.MarkerId = 6 
                            ) TT ON TT.BATypeId = tm.BATypeId
                            INNER JOIN ( SELECT BATypeId FROM DocTree2 ) T ON T.BATypeId = tm.BATypeId
                            WHERE tm.MarkerId = 17
 
                            SET @SSQL = SUBSTRING(@SSQL, CHARINDEX('UNION', @SSQL) + 6, LEN(@SSQL))
                            INSERT INTO @RowExists
                            EXEC(@SSQL)

                            SELECT d.BATypeId, d.[NAME], d.ParentId, d.TreeLevel, d.BranchId, e.IsExists  
                            FROM @DocTree d
                            LEFT JOIN @RowExists e ON e.BATypeId = d.BATypeId
                            WHERE e.IsExists = 1 OR ParentId IS NULL
                            UNION
                            SELECT d.BATypeId, d.[NAME], d.ParentId, d.TreeLevel, d.BranchId, e.IsExists 
                            FROM @DocTree d
                            LEFT JOIN @RowExists e ON e.BATypeId = d.BATypeId
                            INNER JOIN (SELECT dt.BATypeId, dt.TreeLevel, dt.BranchId FROM @DocTree dt 
                                        LEFT JOIN @RowExists e ON e.BATypeId = dt.BATypeId
                                        WHERE e.IsExists = 1
                                       ) tt ON tt.BranchId = d.BranchId AND tt.TreeLevel > d.TreeLevel   
                            WHERE e.IsExists = 0");

            return sb.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using HIS.DAL.Client.Models.Documents;

namespace HIS.DAL.DB.Handlers.Documents
{
    public interface IDocumentsTreeHandler: IDataContextHandler
    {
        IEnumerable<DocumentTreeItem> GetTreeItem(int documentsBaTypeId);
        IEnumerable<DocumentTreeItem> GetFilteredTreeItems(int baTypeId, long companyAreaId, DateTime startDate, DateTime endDate);
    }
}

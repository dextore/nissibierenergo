﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using HIS.DAL.Client.Models.Documents;

namespace HIS.DAL.DB.Handlers.Documents
{
    public class DocumentsTreeHandler : DataContextHandlerBase, IDocumentsTreeHandler
    {
        public IEnumerable<DocumentTreeItem> GetTreeItem(int baTypeId)
        {
            var ssql = QueryHelper.GetAllDocTypesTree(baTypeId);
            return GetItems(ssql);
        }

        public IEnumerable<DocumentTreeItem> GetFilteredTreeItems(int baTypeId, long companyAreaId, DateTime startDate,
            DateTime endDate)
        {
            var ssql = QueryHelper.GetDocTypesExists(baTypeId, companyAreaId, startDate, endDate);
            return GetItems(ssql);
        }

        private IEnumerable<DocumentTreeItem> GetItems(string ssql)
        {
            using (var command = DataContext.Database.Connection.CreateCommand())
            {
                command.CommandText = ssql;

                if (DataContext.Database.CurrentTransaction != null)
                    command.Transaction = DataContext.Database.CurrentTransaction.UnderlyingTransaction;

                var result = new List<DocumentTreeItem>();

                //using (var reader = command.ExecuteReader(CommandBehavior.SequentialAccess))
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        result.Add(ReaderDataConverter(reader));
                    }
                }

                return result;
            }
        }

        private DocumentTreeItem ReaderDataConverter(DbDataReader reader)
        {
            var fields = new Dictionary<string, object>();
            for (var i = 0; i < reader.FieldCount; ++i)
            {
                fields[reader.GetName(i)] = reader[i];
            }

            var baTypeId = Convert.ToInt32(fields["BATypeId"]);
            var name = fields["NAME"].ToString();
            var parentId = fields["ParentId"] == DBNull.Value ? null : (int?)Convert.ToInt32(fields["ParentId"]);
            var treeLevel = Convert.ToInt32(fields["TreeLevel"]);
            var branchId = Convert.ToInt32(fields["BranchId"]);

            return new DocumentTreeItem
            {
                BATypeId = baTypeId,
                Name = name,
                ParentId = parentId,
                TreeLevel = treeLevel,
                BranchId = branchId
            };
        }
    }
}

﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.InventoryObjectsMovementHistory;

namespace HIS.DAL.DB.Handlers.InventoryObjectsMovementHistory
{
    public interface IInventoryObjectsMovementHistoryHandler : IDataContextHandler
    {
        IEnumerable<InventoryAssets> GetFaAssetesList();
        IEnumerable<InventoryAssetsMovementHistory> GetInventoryAssetsMovementHistory(long inventoryAssetsId);
    }
}

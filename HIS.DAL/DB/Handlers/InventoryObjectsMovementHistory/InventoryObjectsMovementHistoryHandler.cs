﻿using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.InventoryObjectsMovementHistory;

namespace HIS.DAL.DB.Handlers.InventoryObjectsMovementHistory
{
    public class InventoryObjectsMovementHistoryHandler : DataContextHandlerBase, IInventoryObjectsMovementHistoryHandler
    {
        public IEnumerable<InventoryAssets> GetFaAssetesList()
        {
            var sql = @"SELECT 
                            f.BAId AS BAId, 
                            f.InventoryNumber AS InventoryNumber, 
                            f.SerialNumber AS SerialNumber,
                            FADAFAR.TransDate AS CreatedDate,
                            f.ReleaseDate AS ReleaseDate, 
                            faf.Number AS DocName,
                            i.Name AS InventoryName,
                            mlp.Name AS MRPName,
                            la.Name AS CompanyAreaName,
                            ls.Name AS LSName,
                            f.BarCode AS BarCode,
                            f.EmployeeId AS EmployeeName,
                            f.CheckDate AS CheckDate,
                            s.StateId AS StateId,
                            s.Name AS [State],
                            f2.FALocation AS Location
                        FROM FAssets AS f 
                        LEFT JOIN LSLegalSubjects AS ls
                        	ON ls.BAId = f.LSId
                        LEFT JOIN MateriallyLiablePersons AS mlp
                        	ON mlp.BAId = f.MLPId
                        LEFT JOIN IInventory AS i
                        	ON i.BAId = f.InventoryId
                        LEFT JOIN FADocAcceptFAssets AS faf
                        	ON faf.BAId = f.FADocAcceptId
                        LEFT JOIN LSCompanyAreas AS la
                                 ON la.BAId = faf.CAId
                        LEFT JOIN SOStates AS s
                        	ON s.StateId = f.StateId
                        LEFT JOIN FALocations AS f2
							ON f2.RecId = f.LocationId
                        LEFT JOIN FADocAcceptFAssetsRows AS FADAFAR
							ON  f.BAId = FADAFAR.FAId";
            var res = DataContext.Database.SqlQuery<InventoryAssets>(sql).ToList();

            return res;
        }

        public IEnumerable<InventoryAssetsMovementHistory> GetInventoryAssetsMovementHistory(long inventoryAssetsId)
        {
            var sql = $@"SELECT 
                         fah.RecId,
                         fah.FAId AS InventoryAssetId,
                         f.InventoryNumber as InventoryAssetNumber, 
                         fah.DocId AS DocId,
                         CASE 
                		    WHEN    fmf.Number IS NOT NULL
		                    THEN fmf.Number
	                    ELSE 
		                    faf.Number END  DocName,
                         fah.MLPId, 
                         mlp.Name AS MRPName, 
                         f2.FALocation AS Location, 
                         fah.StartDate, 
                         fah.EndDate
                        FROM FAssetsHistory AS fah
                        LEFT JOIN FAssets AS f
                        	ON f.BAId = fah.FAId
                        LEFT JOIN FADocMovFAssets AS fmf
							ON fah.DocId = fmf.BAId
                        LEFT JOIN MateriallyLiablePersons AS mlp
                        	ON fah.MLPId = mlp.BAId
                        LEFT JOIN FADocAcceptFAssets AS faf
                        	ON faf.BAId = f.FADocAcceptId
                        LEFT JOIN FALocations AS f2
							ON fah.LocationId = f2.RecId
                        where f.BAId = {inventoryAssetsId}
                        ";
            var res = DataContext.Database.SqlQuery<InventoryAssetsMovementHistory>(sql).ToList();
            return res;

        }
    }
}

﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Inventory;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.DB.Handlers.Inventory
{
    public interface IInventoryHandler : IDataContextHandler
    {
        IEnumerable<IInventory> GetInventorys();
        IEnumerable<IInventory> GetInventoriesByCompanyId(long companyId);
        IInventory GetInventoryById(long BAId);
        IGroups GetInventoryGroupsById(long baid);
        long InventoryUpdate(InventoryData inventory);
        void SaveMarkerValue(long BAId, MarkerValue markerValue);
        IEnumerable<PeriodicMarkerValue> GetPeriodicMarkerHistory(long baId, int markerId);
        IEnumerable<InventoryListItem> GetInventoryList(InventoryListFilters filters);
        IEnumerable<LSCompanyAreas> GetLSCompanyAreasList();
        IEnumerable<RefSysUnits> GetRefSysUnits();
        IEnumerable<IGroups> GetInventoryGroup();
    }
}

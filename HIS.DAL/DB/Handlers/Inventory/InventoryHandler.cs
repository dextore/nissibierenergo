﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.Client.Models.Inventory;
using HIS.DAL.Client.Models.Markers;

//using HIS.Models.Layer.Models;



namespace HIS.DAL.DB.Handlers.Inventory
{
    public class InventoryHandler : DataContextHandlerBase, IInventoryHandler
    {

        public long InventoryUpdate(InventoryData inventory)
        {
            var iBAId = new SqlParameter("@iBAId ", inventory.BAId.HasValue ? inventory.BAId.Value : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.BigInt
            };
            var sName = new SqlParameter("@sName", inventory.Name);
            var sCode = new SqlParameter("@sCode", inventory.Code);
            var iParentId = new SqlParameter("@iParentId", inventory.Inventory.HasValue ? inventory.Inventory.Value : (object)DBNull.Value);
            var iCAId = new SqlParameter("@iCAId", inventory.CompanyArea.HasValue ? inventory.CompanyArea.Value : (object)DBNull.Value);
            var iKindId = new SqlParameter("@iKindId", inventory.InventoryKind.HasValue ? inventory.InventoryKind.Value : (object)DBNull.Value);
            var iTypeId = new SqlParameter("@iTypeId", inventory.InventoryType.HasValue ? inventory.InventoryType.Value : (object)DBNull.Value);
            var iResourceTypeId = new SqlParameter("@iResourceTypeId", inventory.ResourceTypeId.HasValue ? inventory.ResourceTypeId.Value : (object)DBNull.Value);
            var iBuyUnitId = new SqlParameter("@iBuyUnitId", inventory.BuyNatMeaning.HasValue ? inventory.BuyNatMeaning.Value : (object)DBNull.Value);
            var iSaleUnitId = new SqlParameter("@iSaleUnitId", inventory.SaleNatMeaning.HasValue ? inventory.SaleNatMeaning.Value : (object)DBNull.Value);
            var iStockUnitId = new SqlParameter("@iStockUnitId", inventory.StockNatMeaning.HasValue ? inventory.StockNatMeaning.Value : (object)DBNull.Value);



            ExecuteStoredProg(@"[dbo].[Inv_InventoryUpdate]
                                @iBAId  = @iBAId OUT,
                                @sCode  = @sCode ,
                                @sName  = @sName ,
	                            @iParentId  = @iParentId ,
	                            @iCAId 	= @iCAId ,
	                            @iKindId  = @iKindId ,
                                @iTypeId   = @iTypeId  ,
                                @iResourceTypeId = @iResourceTypeId, 
                                @iBuyUnitId   = @iBuyUnitId  ,
                                @iSaleUnitId   = @iSaleUnitId,
	                            @iStockUnitId = @iStockUnitId",
                iBAId,
                sName,
                sCode,
                iParentId,
                iCAId,
                iKindId,
                iTypeId,
                iResourceTypeId,
                iBuyUnitId,
                iSaleUnitId,
                iStockUnitId
            );
            return (long)iBAId.Value;
        }


        public IEnumerable<PeriodicMarkerValue> GetPeriodicMarkerHistory(long baId, int markerId)
        {
            // add display value to output 
            long groupId = 0;

            var markers = DataContext.IInventoryParamValuePeriods.Where(x => x.BAId == baId && x.MarkerId == markerId)
                .Select(x =>  new PeriodicMarkerValue
                {
                    Value = x.sValue,
                    Note = x.Note,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                }).ToList();
            /*
            var markers = (from ipv in DataContext.IInventoryParamValuePeriods
                
                where (ipv.MarkerId == markerId && ipv.BAId == baId)
                select new PeriodicMarkerValue
                {
                    Value = ipv.sValue,
                    Note = ipv.Note,
                    StartDate = ipv.StartDate,
                    EndDate = ipv.EndDate,
                }).ToList();
            */
            foreach (var item in markers)
            {
                item.DisplayValue = "";
                if (markerId == 55)
                {
                    groupId = Convert.ToInt64(item.Value);
                    var igroup  = DataContext.IGroups.Where(x => x.BAId == groupId).FirstOrDefault();
                    item.DisplayValue = igroup.Name;
                }
            }

            return markers;
        }

        public void SaveMarkerValue(long BAId, MarkerValue markerValue)
        {
            if (markerValue.IsDeleted)
            {
                DeleteMarkerValue(BAId, markerValue);
                return;
            }
            UpdateMarkerValue(BAId, markerValue);
        }

        private void UpdateMarkerValue(long BAId, MarkerValue markerValue)
        {
            DateTime tmpDateValue;
            if (DateTime.TryParse(markerValue.Value, out tmpDateValue))
                markerValue.Value = tmpDateValue.ToString("yyyy-MM-dd");

            var iBAId = new SqlParameter("@iBAId", BAId);
            var iMarkerId = new SqlParameter("@iMarkerId", markerValue.MarkerId);

            var dStartDate = new SqlParameter("@dStartDate", markerValue.StartDate.HasValue ? markerValue.StartDate.Value : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.DateTime
            };
            var dEndDate = new SqlParameter("@dEndDate", (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.DateTime
            };
            var vValue = new SqlParameter("@vValue", markerValue.Value);
            var sNote = new SqlParameter("@sNote", string.IsNullOrWhiteSpace(markerValue.Note) ? (object)DBNull.Value : markerValue.Note)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar,
                Size = 4000
            };

            ExecuteStoredProg(@"[dbo].[Inv_InventoryParamUpdate]
                                @iBAId = @iBAId,
                                @iMarkerId = @iMarkerId,	
                                @dStartDate	= @dStartDate OUT,
                                @dEndDate = @dEndDate OUT,
                                @vValue	= @vValue,
                                @sNote = @sNote",
                iBAId,
                iMarkerId,
                dStartDate,
                dEndDate,
                vValue,
                sNote
            );
        }

        private void DeleteMarkerValue(long BAId, MarkerValue markerValue)
        {
            var iBAId = new SqlParameter("@iBAId", BAId);
            var iMarkerId = new SqlParameter("@iMarkerId", markerValue.MarkerId);

            var dStartDate = new SqlParameter("@dStartDate", markerValue.StartDate.HasValue ? markerValue.StartDate.Value : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.DateTime
            };

            ExecuteStoredProg(@"[dbo].[Inv_InventoryParamDelete]
                                @iBAId		= @iBAId,
	                            @iMarkerId	= @iMarkerId,	
	                            @dStartDate	= @dStartDate",
                iBAId,
                iMarkerId,
                dStartDate
            );
        }

        /*
        public IEnumerable<MABA_TypeOptionalMarkerList_Result> GetTypeOptionalMarkerList(long BaTypeId, long markerId, int itemId)
        {
            var iBaTypeId = new SqlParameter("@iBAId", BaTypeId);
            var iMarkerId = new SqlParameter("@iMarkerId", markerId);
            var iItemId = new SqlParameter("@iMarkerId", itemId);


            ExecuteStoredProg(@"[dbo].[MABA_TypeOptionalMarkerList]
                                @@iBATypeId		= @iBATypeId,
	                            @iMarkerId	= @iMarkerId,	
	                            @@iItemId	= @iItemId",
                iBaTypeId,
                iMarkerId,
                @iItemId
            );
        }
        */
                
        public IEnumerable<RefSysUnits>GetRefSysUnits()
        {
         
            return DataContext.RefSysUnits.Select(
                x => new RefSysUnits
                {
                    ID = x.UnitId, // to del
                    UnitId = x.UnitId,
                    UnitClassId = x.UnitClassId,
                    Name = x.Name,
                    Code = x.Code,
                    Symbol = x.Symbol
                }).OrderBy(o =>o.Name).ToList();
        }

        /// <summary>
        /// Плучение номенклатцрных групп
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IGroups> GetInventoryGroup()
        {
            return DataContext.IGroups.Select(
                x => new IGroups
                {
                    BAId = x.BAId,
                    Name = x.Name,
                    ParentId = x.ParentId,
                    Note = x.Note
                }).ToList();
        }

        public IGroups GetInventoryGroupsById(long baid)
        {
            return DataContext.IGroups.Where(w => w.BAId == baid).Select(
                x=> new IGroups
                {
                    BAId = x.BAId,
                    Name = x.Name,
                    ParentId = x.ParentId,
                    Note = x.Note

                }
            ).FirstOrDefault();
        }

        /// <summary>
        /// Получение организаций   
        /// </summary>
        /// <returns></returns>
        public IEnumerable<LSCompanyAreas> GetLSCompanyAreasList()
        {
            return DataContext.EFLSCompanyAreas.Select(
                    x => new LSCompanyAreas
                    {
                        BAId = x.BAId,
                        LSId = x.LSId,
                        Name = x.Name,
                        IsActive = (bool)x.IsActive
                    }
                ).ToList();
        }
        /// <summary>
        /// Получение списка номенклатурных единиц
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IInventory> GetInventorys()
        {
            return DataContext.IInventory.Select(
                x => new IInventory
                {
                    BAId = x.BAId,
                    Name = x.Name,
                    Code = x.Code,
                    ParentId = x.ParentId,
                    CAId = x.CAId,
                    BuyUnitId = x.BuyUnitId,
                    SaleUnitId = x.SaleUnitId,
                    StockUnitId = x.StockUnitId,
                    ResourceTypeId = x.ResourceTypeId,
                    KindId =  x.KindId,
                    TypeId = x.TypeId
                }).ToList();

        }
        /// <summary>
        /// Получение списка номенклатурных единиц для организации
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IInventory> GetInventoriesByCompanyId(long companyId)
        {
            return DataContext.IInventory.Select(
                x => new IInventory
                {
                    BAId = x.BAId,
                    Name = x.Name,
                    Code = x.Code,
                    ParentId = x.ParentId,
                    CAId = x.CAId,
                    BuyUnitId = x.BuyUnitId,
                    SaleUnitId = x.SaleUnitId,
                    StockUnitId = x.StockUnitId,
                    KindId = x.KindId,
                    TypeId = x.TypeId
                })
                .Where(x => x.CAId == companyId)
                .ToList();

        }

        


        public IInventory GetInventoryById(long BAId)
        {
            return DataContext.IInventory.Select(
                    x => new IInventory
                    {
                        BAId = x.BAId,
                        Name = x.Name,
                        Code = x.Code,
                        ParentId = x.ParentId,
                        ParentName = DataContext.IInventory.FirstOrDefault(i=>i.BAId == x.ParentId).Name,
                        CAId = x.CAId,
                        CAName = DataContext.EFLSCompanyAreas.FirstOrDefault(c=>c.BAId == x.CAId).Name,
                        BuyUnitId = x.BuyUnitId,
                        SaleUnitId = x.SaleUnitId,
                        StockUnitId = x.StockUnitId,
                        ResourceTypeId = x.ResourceTypeId,
                        KindId = x.KindId,
                        TypeId = x.TypeId

                    })
                .AsNoTracking()
                .Where(m => m.BAId == BAId)
                .FirstOrDefault();
        }

        public IEnumerable<OptionalMarkers> GetOptionalMarkers(int BATypeId, int markerId, int ItemId)
        {
            return DataContext.MABA_TypeOptionalMarkerList(BATypeId,markerId,ItemId).Select(
                   x => new OptionalMarkers
                   {
                       BATypeId = x.BATypeId,
                       OptionalMarkerId = x.OptionalMarkerId,
                       MarkerId = x.MarkerId,
                       IsVisible = x.IsVisible
                   }).ToList();
        }


        /// <summary>
        /// Список номенклатурных единиц для вывода в datagrid 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<InventoryListItem> GetInventoryList(InventoryListFilters filters)
        {
            /*
            return DataContext.IInventory.GroupJoin(
                DataContext.IInventory,
                inv => inv.ParentId,
                or => or.BAId,
                (inv, or) => new
                {
                    BaId = inv.BAId,
                    BATypeId = inv.BABaseAncestors.TypeId,
                    Name = inv.Name,
                    CAId = inv.CAId,
                    CompanyName = inv.CAIdLSCompanyAreas.Name,
                    ParentId = inv.ParentId,
                    ParentName = or.Select(x => x.Name).FirstOrDefault(),
                    BuyUnitId = inv.BuyUnitId,
                    BuyUnitName = inv.Buy.Name,
                    SaleUnitId = inv.SaleUnitId,
                    SaleUnitName = inv.Sale.Name,
                    StockUnitId = inv.StockUnitId,
                    StockUnitName = inv.Stock.Name,
                    KindId = inv.KindId,
                    TypeId = inv.TypeId

                }
            ).ToList();
            */
            // заменить на linq

            var sName = new SqlParameter("@sName", String.IsNullOrEmpty(filters.Name) ? (object)DBNull.Value : filters.Name);
            var iBAId = new SqlParameter("@iBAId", filters.BAId.HasValue ? filters.BAId : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.BigInt
            };
            var iCAId = new SqlParameter("@iCAId", filters.CAId.HasValue ? filters.CAId : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.BigInt
            };
            var sParentName = new SqlParameter("@sParentName", String.IsNullOrEmpty(filters.ParentName) ? (object)DBNull.Value : filters.ParentName);
            var sStateName = new SqlParameter("@sStateName", String.IsNullOrEmpty(filters.StateName) ? (object)DBNull.Value : filters.StateName);
            var sTypeName  = new SqlParameter("@sTypeName", String.IsNullOrEmpty(filters.TypeName) ? (object)DBNull.Value : filters.TypeName);
            var sKindName = new SqlParameter("@sKindName", String.IsNullOrEmpty(filters.KindName) ? (object)DBNull.Value : filters.KindName);
            var sBuyUnitName = new SqlParameter("@sBuyUnitName", String.IsNullOrEmpty(filters.BuyUnitName) ? (object)DBNull.Value : filters.BuyUnitName);
            var sSaleUnitName = new SqlParameter("@sSaleUnitName", String.IsNullOrEmpty(filters.SaleUnitName) ? (object)DBNull.Value : filters.SaleUnitName);
            var sStockUnitName = new SqlParameter("@sStockUnitName", String.IsNullOrEmpty(filters.StockUnitName) ? (object)DBNull.Value : filters.StockUnitName);


            /*
            var state = DataContext.EFSOStates.Where(x => x.Name == filters.StateName).FirstOrDefault();
            var iState = new SqlParameter("@iState", state == null ? (object)DBNull.Value : state.StateId);
            */

            const string query = @"SELECT  
                                    i.BAId AS BAId,
	                                ba.TypeId AS BATypeId,
                                    i.Code AS Code,
	                                i.Name AS Name,
	                                i.CAId AS CAId,
	                                la.Name AS CompanyName,	
	                                i.ParentId AS ParentId,
	                                perinv.Name AS ParentName ,
	                                i.BuyUnitId AS BuyUnitId,
	                                bu.Name AS BuyUnitName,	
	                                i.SaleUnitId AS SaleUnitId,
	                                su.Name AS SaleUnitName,
	                                i.StockUnitId AS StockUnitId,
	                                stu.Name AS StockUnitName,
	                                i.KindId AS KindId,
	                                mvlk.ItemName AS KindName, 
	                                i.TypeId AS TypeId,
	                                mvlt.ItemName AS TypeName,
                                    st.StateId AS StateId, 
	                                st.Name AS StateName                                   
	                            FROM
	                                [dbo].[IInventory] AS i
                                LEFT JOIN [dbo].RefSysUnits bu ON bu.UnitId = i.BuyUnitId
                                LEFT JOIN [dbo].RefSysUnits su ON su.UnitId = i.SaleUnitId
                                LEFT JOIN [dbo].RefSysUnits stu ON stu.UnitId = i.StockUnitId
                                LEFT JOIN [dbo].[IInventory] AS perinv ON perinv.BAId = i.ParentId
                                LEFT JOIN [dbo].BABaseAncestors AS ba ON ba.BAId = i.BAId
                                LEFT JOIN [dbo].LSCompanyAreas AS la ON la.BAId = i.CAId
                            	LEFT JOIN  SOStates st ON st.StateId = i.StateId
                                LEFT JOIN [dbo].MAMarkerValueList AS mvlk ON mvlk.ItemId = i.KindId AND
	                                mvlk.MarkerId = 57
                                LEFT JOIN [dbo].MAMarkerValueList AS mvlt ON mvlt.ItemId = i.TypeId AND
	                                mvlt.MarkerId = 56
                                WHERE 
                                     i.CAId=@iCAId AND
                                     (i.BAId = @iBAId OR @iBAId IS NULL) AND 
                                     (i.Name LIKE '%'+@sName+'%' OR @sName IS NULL) AND 
                                     (perinv.Name LIKE '%'+@sParentName+'%' OR @sParentName IS NULL) AND                              
                                     (st.Name LIKE '%'+@sStateName+'%' OR @sStateName IS NULL) AND    
                                     (mvlt.ItemName LIKE '%'+@sTypeName+'%' OR @sTypeName IS NULL) AND    
                                     (mvlk.ItemName LIKE '%'+@sKindName+'%' OR @sKindName IS NULL) AND   
                                     (bu.Name  LIKE '%'+@sBuyUnitName+'%' OR @sBuyUnitName IS NULL) AND    
                                     (su.Name LIKE '%'+@sSaleUnitName+'%' OR @sSaleUnitName IS NULL) AND 
                                     (stu.Name LIKE '%'+@sStockUnitName+'%' OR @sStockUnitName IS NULL)";
            return DataContext.Database.SqlQuery<InventoryListItem>(query,
                iCAId,
                iBAId,
                sName , 
                sParentName,
                sStateName,
                sTypeName,
                sKindName,
                sBuyUnitName,
                sSaleUnitName,
                sStockUnitName).ToList();



        }


      
     

    }

}

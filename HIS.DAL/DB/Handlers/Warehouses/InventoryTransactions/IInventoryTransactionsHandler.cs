﻿using System.Linq;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.Warehouses.InventoryTransactions
{
    interface IInventoryTransactionsHandler: IDataContextHandler
    {
        IQueryable<EFWHInventoryTransaction> Get();
    }
}

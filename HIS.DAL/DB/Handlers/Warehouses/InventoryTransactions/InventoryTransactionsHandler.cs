﻿using System.Linq;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.Warehouses.InventoryTransactions
{
    public class InventoryTransactionsHandler: DataContextHandlerBase, IInventoryTransactionsHandler
    {
        public IQueryable<EFWHInventoryTransaction> Get()
        {
            return DataContext.EFWHInventoryTransactions;
        }
    }
}

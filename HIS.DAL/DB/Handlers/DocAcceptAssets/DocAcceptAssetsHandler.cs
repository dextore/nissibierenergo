﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.DB.Context;
using HIS.DAL.Client.Models.DocAcceptAssets;
using HIS.DAL.Client.Models.Markers;
using System.Data.Entity.Core.Objects;

namespace HIS.DAL.DB.Handlers.DocAcceptAssets
{
    public class DocAcceptAssetsHandler : DataContextHandlerBase, IDocAcceptAssetsHandler
    {

        public DocAcceptAssetsData GetDocAcceptAssets(long baId) {
            DocAcceptAssetsData item = DataContext.FADocAcceptFAssets.Where(w => w.BAId == baId)
                .Select(
                x => new DocAcceptAssetsData
                {
                    BAId = baId,
                    BaTypeId = x.BATypeId,
                    CAId = x.CAId,
                    CAName = x.LSCompanyAreas.Name,
                    LSId = x.LSId,
                    LSName = x.LSLegalSubjects.Name,
                    DocDate = x.DocDate,
                    Evidence = x.Evidence,
                    Note = x.Note,
                    Number = x.Number,
                    RIId = x.RIId,
                    RIName = x.RIReceiptInvoices.Number,
                    StateId = x.StateId,                  
                    WHId = x.WHId,
                    WHName = x.WHWarehouses.Name
                }).FirstOrDefault();
            ///заполнить значения маркеров 
            item.HasSpecification = IsHasSpecification((long)item.BAId);
            return item; 
        }
        public IEnumerable<DocAcceptAssetsRow> GetDocAcceptAssetsRows(long docAcceptId)
        {
            return DataContext.FADocAcceptFAssetsRows.Where(w => w.FADocAcceptId == docAcceptId).Select(
                x => new DocAcceptAssetsRow
                {
                    BarCode = x.BarCode,
                    CheckDate = x.CheckDate,
                    EmployeeId = x.EmployeeId,
                    FADocAcceptId = x.FADocAcceptId,
                    FAId = x.FAId,
                    InventoryId = x.InventoryId,
                    InventoryName = x.IInventory.Name,
                    InventoryNumber = x.InventoryNumber,
                    RecId = x.RecId,
                    ReleaseDate = x.ReleaseDate,
                    SerialNumber = x.SerialNumber
                }).ToList();
        }
        public DocAcceptAssetsRow GetDocAcceptAssetsRow(long docAcceptId , long recId)
        {
            return DataContext.FADocAcceptFAssetsRows.Where(w => w.FADocAcceptId == docAcceptId 
                                                                 && w.RecId == recId).Select(
                x => new DocAcceptAssetsRow
                {
                    BarCode = x.BarCode,
                    CheckDate = x.CheckDate,
                    EmployeeId = x.EmployeeId,
                    FADocAcceptId = x.FADocAcceptId,
                    FAId = x.FAId,
                    InventoryId = x.InventoryId,
                    InventoryName = x.IInventory.Name,
                    InventoryNumber = x.InventoryNumber,
                    RecId = x.RecId,
                    ReleaseDate = x.ReleaseDate,
                    SerialNumber = x.SerialNumber
                }).FirstOrDefault();
        }
        public long UpdateDocAcceptAssets(DocAcceptAssetsData saveData)
        { 
            var iBAId = new SqlParameter("@iBAId ", saveData.BAId.HasValue ? saveData.BAId.Value : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.BigInt
            };
            var sNumber = new SqlParameter("@sNumber", saveData.Number);
            var dDocDate = new SqlParameter("@dDocDate", saveData.DocDate.HasValue ? saveData.DocDate.Value : (object)DBNull.Value);
            var iCAId = new SqlParameter("@iCAId", saveData.CAId.HasValue ? saveData.CAId.Value : (object)DBNull.Value);
            var iLSId = new SqlParameter("@iLSId", saveData.LSId.HasValue ? saveData.LSId.Value : (object)DBNull.Value);
            var iWHId = new SqlParameter("@iWHId", saveData.WHId.HasValue ? saveData.WHId.Value : (object)DBNull.Value);
            var iRIId = new SqlParameter("@iRIId", saveData.RIId.HasValue ? saveData.RIId.Value : (object)DBNull.Value);
            var sEvidence = new SqlParameter("@sEvidence", String.IsNullOrEmpty(saveData.Evidence) ? (object)DBNull.Value : saveData.Evidence);
            var sNote = new SqlParameter("@sNote", String.IsNullOrEmpty(saveData.Note) ? (object)DBNull.Value : saveData.Note);
            
            ExecuteStoredProg(@"[dbo].[FADA_DocAcceptFAssetsUpdate]
                                @iBAId       = @iBAId OUT,
                                @sNumber    = @sNumber ,
                                @dDocDate   = @dDocDate ,
	                            @iCAId 	    = @iCAId ,
	                            @iLSId      = @iLSId ,                               
                                @iWHId 	    = @iWHId ,
	                            @iRIId      = @iRIId ,
                                @sEvidence  = @sEvidence  ,
                                @sNote      = @sNote",
                iBAId,
                sNumber,
                dDocDate,
                iCAId,
                iLSId,
                iWHId,
                iRIId,
                sEvidence,
                sNote
            );
            return (long)iBAId.Value;
            
        }
        public void SaveMarkerValue(long BAId, MarkerValue markerValue)
        {
            if (markerValue.IsDeleted)
            {
                DeleteMarkerValue(BAId, markerValue);
                return;
            }
            UpdateMarkerValue(BAId, markerValue);
        }
        private void UpdateMarkerValue(long baId, MarkerValue markerValue)
        {
            // переписать 
            var dStartDate = markerValue.StartDate.HasValue ?
               new ObjectParameter("dStartDate", markerValue.StartDate.Value) :
               new ObjectParameter("dStartDate", typeof(DateTime));

            var dEndDate = markerValue.StartDate.HasValue ?
              new ObjectParameter("dEndDate", markerValue.EndDate.Value) :
              new ObjectParameter("dEndDate", typeof(DateTime));

            DataContext.UpdateParamDocAcceptAssets(baId, markerValue.MarkerId, dStartDate, dEndDate, markerValue.Note);
            
        }
        private void DeleteMarkerValue(long baId, MarkerValue markerValue)
        {
            // переписать 
            DataContext.DeleteParamDocAccepеAssets(baId, markerValue.MarkerId, markerValue.StartDate);
        }
        public bool IsHasSpecification(long baId)
        {
            return DataContext.FADocAcceptFAssetsRows.Any(w => w.FADocAcceptId == baId);            
        }
        // update delete row 
        public long UpdateDocAcceptAssetRow(DocAcceptAssetsRow row)
        {

            var iRecId = new SqlParameter("@iRecId ", row.RecId ?? (object)DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.BigInt
            };
            var iFADocAcceptId = new SqlParameter("@iFADocAcceptId", row.FADocAcceptId)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iInventoryId = new SqlParameter("@iInventoryId", row.InventoryId)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var sInventoryNumber = new SqlParameter("@sInventoryNumber", String.IsNullOrEmpty(row.InventoryNumber) ? (object)DBNull.Value : row.InventoryNumber)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            var sSerialNumber = new SqlParameter("@sSerialNumber", String.IsNullOrEmpty(row.SerialNumber) ? (object)DBNull.Value : row.SerialNumber)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            var dReleaseDate = new SqlParameter("@dReleaseDate", row.ReleaseDate.HasValue ? row.ReleaseDate.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.Date,
            };
            var dCheckDate = new SqlParameter("@dCheckDate", row.CheckDate.HasValue ? row.CheckDate.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.Date,
            };
            var sBarCode = new SqlParameter("@sBarCode", String.IsNullOrEmpty(row.BarCode) ? (object)DBNull.Value : row.BarCode)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            var iEmployeeId = new SqlParameter("@iEmployeeId", row.EmployeeId.HasValue ? row.EmployeeId.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };

            ExecuteStoredProg(@"EXEC [dbo].[FADA_DocAcceptFAssetsRowsUpdate]
                                @iRecId = @iRecId OUT,
                                @iFADocAcceptId = @iFADocAcceptId,
                                @iInventoryId = @iInventoryId,
                                @sInventoryNumber = @sInventoryNumber,
                                @sSerialNumber = @sSerialNumber,
                                @dReleaseDate = @dReleaseDate,
                                @dCheckDate = @dCheckDate,
                                @sBarCode = @sBarCode,
                                @iEmployeeId = @iEmployeeId",
                iRecId,
                iFADocAcceptId,
                iInventoryId,
                sInventoryNumber,
                sSerialNumber,
                dReleaseDate,
                dCheckDate,
                sBarCode,
                iEmployeeId
            );
            return (long)iRecId.Value;

            /*
            var iRecId = row.RecId.HasValue ?
            new ObjectParameter("iRecId", row.RecId) :
            new ObjectParameter("iRecId", typeof(long));
            
            DataContext.UpdateDocAcceptAssetsRows(iRecId,
                row.FADocAcceptId,
                row.InventoryId,
                row.InventoryNumber,
                row.SerialNumber,
                row.ReleaseDate,
                row.CheckDate,
                row.BarCode,
                row.EmployeeId);

            return (long)iRecId.Value;
            */

        }

        public void DeleteDocAcceptAssetRow(DocAcceptAssetsRow row) {

            DataContext.DeleteDocAcceptAssetsRows(row.RecId, row.FADocAcceptId);
        }
        ///
        public IEnumerable<ReceiptInvoiceSelect> GetReceiptInvoiceSelect(long caId) {

           var list = DataContext.GetReceiptInvoiceSelect(caId);
            return list.ToList();
        }
        public IEnumerable<ReceiptInvoiceInventSelect> GetReceiptInvoiceInventSelect(long docId)
        {
            var list = DataContext.GetReceiptInvoiceInventSelect(docId);
            return list.ToList();
        }
    }
}

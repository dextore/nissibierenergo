﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.DocAcceptAssets;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.DocAcceptAssets
{
    public interface IDocAcceptAssetsHandler: IDataContextHandler
    {      
        DocAcceptAssetsData GetDocAcceptAssets(long baId);
        IEnumerable<DocAcceptAssetsRow> GetDocAcceptAssetsRows(long docAcceptId);
        DocAcceptAssetsRow GetDocAcceptAssetsRow(long docAcceptId, long recId);
        long UpdateDocAcceptAssets(DocAcceptAssetsData saveData);
        void SaveMarkerValue(long baId, MarkerValue markerValue);
        long UpdateDocAcceptAssetRow(DocAcceptAssetsRow row);
        void DeleteDocAcceptAssetRow(DocAcceptAssetsRow row);
        IEnumerable<ReceiptInvoiceSelect> GetReceiptInvoiceSelect(long caId);
        IEnumerable<ReceiptInvoiceInventSelect> GetReceiptInvoiceInventSelect(long docId);
        bool IsHasSpecification(long baId);


    } 
}

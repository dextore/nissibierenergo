﻿using HIS.DAL.Client.Models.SystemSearch;

namespace HIS.DAL.DB.Handlers.UniversalSearch
{
    public interface IUniversalSearchHandler : IDataContextHandler
    {
        SearchResult SearchByName(long from, long to, string value, bool showDeleted, int? baTypeId);
        SearchResult SearchByInn(long from, long to, string value, bool showDeleted, int? baTypeId);
    }
}

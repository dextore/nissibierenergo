﻿using System;
using System.Collections.Generic;
using HIS.DAL.Client.Models.SystemSearch;

namespace HIS.DAL.DB.Handlers.UniversalSearch
{
    public class UniversalSearchHandler : DataContextHandlerBase, IUniversalSearchHandler
    {
        public SearchResult SearchByName(long from, long to, string value, bool showDeleted, int? baTypeId)
        {
            var condition = baTypeId.HasValue ? $"ls.BATypeId = {baTypeId} AND " : "";
            condition += showDeleted
                ? $"(ls.[Name] LIKE '%{value}%' OR ls.[FullName] LIKE '%{value}%')"
                : $"(ls.[Name] LIKE '%{value}%' OR ls.[FullName] LIKE '%{value}%') AND ts.IsDelState = 0";
            return GetResult(from, to, condition);
        }

        public SearchResult SearchByInn(long from, long to, string value, bool showDeleted, int? baTypeId)
        {
            var condition = baTypeId.HasValue ? $"ls.BATypeId = {baTypeId} AND " : "";
            condition += showDeleted
                ? $"ls.INN LIKE '%{value}%'"
                : $"ls.INN LIKE '%{value}%' AND ts.IsDelState = 0";

            return GetResult(from, to, condition);
        }

        private SearchResult GetResult(long from, long to, string condition)
        {
            var result = new SearchResult();

            using (var command = DataContext.Database.Connection.CreateCommand())
            {
                command.CommandText = QueryHelper.GetCountQuery(condition); 

                if (DataContext.Database.CurrentTransaction != null)
                    command.Transaction = DataContext.Database.CurrentTransaction.UnderlyingTransaction;

                result.RowCount = Convert.ToInt64(command.ExecuteScalar());
            }

            using (var command = DataContext.Database.Connection.CreateCommand())
            {
                command.CommandText = QueryHelper.GetQuery(from, to, condition);

                if (DataContext.Database.CurrentTransaction != null)
                    command.Transaction = DataContext.Database.CurrentTransaction.UnderlyingTransaction;

                var items = new List<SimpleSearchItem>();
                result.Items = items;

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var fields = new Dictionary<string, object>();
                        for (var i = 0; i < reader.FieldCount; ++i)
                        {
                            fields[reader.GetName(i)] = reader[i];
                        }

                        items.Add( new SimpleSearchItem
                        {
                            BAId = Convert.ToInt64(fields["BAId"]),
                            BATypeId = Convert.ToInt32(fields["BATypeId"]),
                            TypeName = fields["TypeName"].ToString(),
                            StateName = fields["StateName"].ToString(),
                            Code = fields["Code"].ToString(),
                            Name = fields["Name"].ToString(),
                            INN = fields["INN"].ToString(),
                            LegalFormName = fields["lfName"] == DBNull.Value ? null : fields["lfName"].ToString(),
                        });
                    }
                }

                return result;

            }
        }
    }
}

﻿namespace HIS.DAL.DB.Handlers.UniversalSearch
{
    internal static class QueryHelper
    {
        internal static string GetCountQuery(string condition)
        {
            return $@"SELECT COUNT(1)
                     FROM dbo.LSLegalSubjects AS ls
                     INNER JOIN dbo.BATypes AS b ON b.TypeId = ls.BATypeId
                     INNER JOIN [dbo].[SOBATypeStates] ts ON ts.BATypeId = ls.BATypeId AND ts.StateId = ls.StateId
                     INNER JOIN [dbo].[SOStates] s ON s.StateId = ts.StateId 
                     WHERE {condition}";
        }

        internal static string GetQuery(long from, long to, string condition)
        {
            return $@"DECLARE @D DATE = GETDATE(); 

                     WITH LegalSubjects AS
                     (
                         SELECT 
                              RowNumber = ROW_NUMBER() OVER(ORDER BY ls.[Name] ASC)
                             ,ls.BAId
                             ,ls.BATypeId
                             ,ls.StateId
                             ,b.MVCAlias
                             ,StateName = s.[Name]
                             ,ts.IsDelState
                             ,TypeName =  COALESCE(b.ShortName, b.[Name], b.FullName)
                             ,ls.Code
                             ,[Name] = CASE b.MVCAlias WHEN 'Organisation'          THEN ls.[Name] 
                                                       WHEN 'AffiliateOrganisation' THEN ls.[Name]
                                                       WHEN 'Person'                THEN ls.[FullName]
                                                       WHEN 'CommercePerson'        THEN ls.[FullName]
                                       END                                                                                            
                             ,ls.INN
                             ,lfName = COALESCE(olf.[Name], plf.[Name])
                         FROM dbo.LSLegalSubjects AS ls
                         INNER JOIN dbo.BATypes AS b ON b.TypeId = ls.BATypeId
                         INNER JOIN [dbo].[SOBATypeStates] ts ON ts.BATypeId = ls.BATypeId AND ts.StateId = ls.StateId
                         INNER JOIN [dbo].[SOStates] s ON s.StateId = ts.StateId
                         LEFT JOIN dbo.LSLegalSubjectParamValuePeriods AS vpLS ON vpLS.BAId = ls.BAId
                                                                              AND vpLS.StartDate <= @D AND vpLS.EndDate > @D   
                                                                              AND vpLS.MarkerId IN (SELECT MarkerId FROM [dbo].[MAMarkers] WHERE MVCAlias = 'OrganisationLegalForm')
                         LEFT JOIN dbo.LSLegalSubjectParamValuePeriods AS vpP ON vpP.BAId = ls.BAId
                                                                             AND vpP.StartDate <= @D AND vpP.EndDate > @D   
                                                                             AND vpP.MarkerId IN (SELECT MarkerId FROM [dbo].[MAMarkers] WHERE MVCAlias = 'PersonLegalForm')
                         LEFT JOIN [dbo].[vLSOrganisationLegalForms]  olf  ON olf.ItemId = vpLS.sValue  
                         LEFT JOIN [dbo].[vLSPersonLegalForms]        plf  ON plf.ItemId = vpP.sValue
                                                             
                         WHERE {condition}
                     )  
                     SELECT RowNumber
                           ,BAId
                           ,BATypeId
                           ,StateId
                           ,MVCAlias
                           ,StateName
                           ,IsDelState
                           ,TypeName
                           ,Code
                           ,[Name]                                                                                            
                           ,INN
                           ,lfName
                     FROM LegalSubjects WHERE RowNumber BETWEEN {from} AND {to}";
        }

    }
}

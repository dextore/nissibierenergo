﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.DB.Handlers.Entities
{
    public interface IEntitiesHandler: IDataContextHandler
    {
        long? Update(EntityUpdate model, ICollection<MarkerInfo> markersInfo);
    }
}

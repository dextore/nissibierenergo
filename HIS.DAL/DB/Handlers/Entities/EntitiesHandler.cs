﻿using System;
using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.DB.Common;

namespace HIS.DAL.DB.Handlers.Entities
{
    public class EntitiesHandler: DataContextHandlerBase, IEntitiesHandler
    {
        public long? Update(EntityUpdate model, ICollection<MarkerInfo> markersInfo)
        {
            if (!markersInfo
                .Join(model.MarkerValues, (mi) => mi.Id, (mv) => mv.MarkerId, (mi, mv) => new {mi, mv})
                .Any(m => m.mi.IsImplemented && !string.IsNullOrEmpty(m.mv.Value)))
                return null; 

            var alias = DataContext.EFBATypes.First(m => m.TypeId == model.BATypeId)?.MVCAlias;

            if (string.IsNullOrEmpty(alias))
                throw new NotSupportedException($"Save entity type '{model.BATypeId}' not exists");

            var progInfo = EntitytProgsStorage.GetInfo(alias);

            if (progInfo == null)
            {
                throw new NotSupportedException($"Save entity type '{alias}' not implemented");
            }

            progInfo.CheckValues(DataContext, model, markersInfo);

            return ExecEntitytUpdateProg(progInfo.UpdateEntityProg, progInfo.ProgMarkersMap, model, markersInfo);
        }

    }

}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using HIS.DAL.DB.Context;
using HIS.DAL.Client.Models.History;

namespace HIS.DAL.DB.Handlers.History
{
    public class HistoryBuilderMarkers
    {
        public long BAId { get; set; }
        public int BATypeId { get; set; }
        public int MarkerId { get; set; }
        public bool IsPeriodic { get; set; }
        public bool IsCollectible { get; set; }
        public string Name { get; set; }
        public int MarkerType { get; set; }
        public string ImplementTypeField { get; set; }
        public string ImplementTypeName { get; set; }
        public string ReferenceImplementTypeName { set; get; }
        public bool EntityIsPeriodic { get; set; }
        public string EntityImplementTypeName { set; get; }
        public string EntityImplementTypeField { set; get; }      
    }

    public class HistoryHandler : DataContextHandlerBase, IHistoryHandler
    {

        public IEnumerable<GetHISUsers_Result> GetHisUsers()
        {
            return DataContext.GetHISUsers();
        }
        public EFBAType GetBaType(long baId)
        {
            var baseAncestors = DataContext.EFBABaseAncestors.Where(a => a.BAId == baId).FirstOrDefault();  
            if (baseAncestors != null)
            {
                return DataContext.EFBATypes.Where(x => x.TypeId == baseAncestors.TypeId).FirstOrDefault();
            }
            return null;
        }

        public IEnumerable<EntityHistory> GetEntityStateHistory(long baId)
        {
            var userList = DataContext.GetHISUsers().ToList();
            var list = DataContext.BABaseAncestors_Journal.Where(w => w.BAId == baId).Select(
                x => new EntityHistory
                {
                    BAId = x.BAId,
                    UserId = x.UserId,
                    UserName = "Пользователь не определен",
                    OperationDate = x.OperationDate,
                    OperationId = x.OperationId,
                    OperationName = x.SOOperations.Name,
                    SrcStateId = x.SrcStateId,
                    SrcStateName = x.SOStates1.Name,
                    DstStateId = x.DstStateId,
                    DstStateName = x.SOStates.Name 
                }).OrderByDescending(o=>o.OperationDate).ToList();
            
            list.ForEach(item => {
                if (item.UserId.HasValue) 
                {
                    item.UserName = userList.Where(x => x.Id == item.UserId).FirstOrDefault().Description;
                }
            });

            return list;
        }

        public int GetBATypeId(long baId)
        {
            return DataContext.EFBABaseAncestors.Where(x => x.BAId == baId).FirstOrDefault().TypeId;
        }

        public IEnumerable<EntityHistory> GetEntityMarkersHistory(long baId)
        {
            string query = GetResultQuery(baId);
            var list = DataContext.Database.SqlQuery<EntityHistory>(query).ToList();
            var userList = DataContext.GetHISUsers().ToList();
            list.ForEach(item =>
            {
                item.UserName = "Пользователь не определен";
                if (item.UserId.HasValue)
                {
                    item.UserName = userList.FirstOrDefault(x => x.Id == item.UserId).Description;
                }

            });
            return ColoriseEntityGroup(list);
        }

        private IEnumerable<EntityHistory> ColoriseEntityGroup(List<EntityHistory> list)
        {
            var colorised = new List<EntityHistory>();
            var counter = 1;
            var isDouble = false;

            foreach (var entity in list)
            {
                var value = counter;
                if (isDouble)
                {
                    colorised.Add(entity);
                    isDouble = false;
                    continue;
                }
                
                if (list.IndexOf(entity) + 1 < list.Count
                    && entity.GRecId != null
                    && entity.GRecId == list[list.IndexOf(entity) + 1].GRecId
                )
                {
                    entity.Color = value;
                    list[list.IndexOf(entity) + 1].Color = value;
                    isDouble = true;
                }
                else
                {
                    entity.Color = value;
                    isDouble = false;
                }

                counter++;
                colorised.Add(entity);
            }

            return colorised.OrderBy(x => x.Color);
        }

        public IEnumerable<HistoryBuilderMarkers> GetEnityMarkers(int baTypeId)
        {
            string query = $@"SELECT 
	                    mm.BATypeId AS BATypeId,
	                    mm.MarkerId AS MarkerId,
	                    mm.IsPeriodic AS IsPeriodic,
	                    mm.IsCollectible AS IsCollectible,
	                    IsNull(mm.Name, m.Name) AS Name, 
	                    mm.ImplementTypeName AS ImplementTypeName,
	                    mm.ImplementTypeField AS ImplementTypeField,
	                    m.TypeId AS MarkerType,
	                    m.ImplementTypeName AS ReferenceImplementTypeName,
	                    IsNull(mm2.IsPeriodic , 0) AS EntityIsPeriodic,
	                    mm2.ImplementTypeName AS EntityImplementTypeName,
	                    mm2.ImplementTypeField AS EntityImplementTypeField
                    FROM 
	                    MABATypeMarkers AS mm
                    LEFT JOIN MAMarkers AS m ON m.MarkerId = mm.MarkerId
                    LEFT JOIN MABATypeMarkers AS mm2 ON (mm2.MarkerId = 1 
	                    AND mm2.BATypeId = m.BATypeId) 
                    WHERE 
	                    mm.BATypeId = {baTypeId}  
	                    AND mm.MarkerId != 42
                        AND mm.MarkerId != 45";
            return DataContext.Database.SqlQuery<HistoryBuilderMarkers>(query).ToList();
        }

        public string GetResultQuery(long baId)
        {
            string query = "";
            string markerQuery = "";
            int baTypeId = GetBATypeId(baId);
            var markers = GetEnityMarkers(baTypeId);
            markers.ToList().ForEach(item =>
            {
                item.BAId = baId;
                markerQuery = GetMarkerQuery(item);
                if (!String.IsNullOrEmpty(markerQuery))
                {
                    if (String.IsNullOrEmpty(query))
                    {
                        query = markerQuery;
                    }
                    else
                    {
                        query += $" UNION {markerQuery} ";
                    }
                }
            });
            return $"SELECT t.* FROM ( {query} ) AS t ORDER BY t.OperationDate DESC ";       
        }

        public HistoryBuilderMarkers FillEmptyImpletmentName(HistoryBuilderMarkers marker)
        {
            if (!String.IsNullOrEmpty(marker.ImplementTypeName))
            {
                return marker; 
            }
            marker.ImplementTypeField = "Value";
            if (marker.IsCollectible)
            {
                marker.ImplementTypeName = "MABaseAncestorMarkerItemValues";
            }
            else if (marker.IsPeriodic) 
            {
                marker.ImplementTypeName = "MABaseAncestorMarkerValuePeriods";
            }
            else
            {
                marker.ImplementTypeName = "MABaseAncestorMarkerValues";
            }
            return marker;
        }
        public string GetMarkerQuery(HistoryBuilderMarkers marker)
        {
            string query = "";
            string select = "";
            string from = "";
            string where = "";
            var currentMarker = FillEmptyImpletmentName(marker);
            
            switch (currentMarker.MarkerType) {
                //ссылка на сущность 
                case 11:
                //адрес
                case 12:                   
                    select = GetSelectForEntity(currentMarker);
                    from = GetFromForEntity(currentMarker);
                    where = GetDefaultWhere(currentMarker);
                    break;
                //ссылка на справочник
                case 13:
                    select = GetSelectForReference(currentMarker);
                    from = GetFromForReference(currentMarker);
                    where = GetDefaultWhere(currentMarker);
                    break;
                //MAMarkerValueList
                case 5:
                    select = GetSelectForList(currentMarker);
                    from = GetFromForList(currentMarker);
                    where = GetDefaultWhere(currentMarker);                 
                    break;
                default:
                    select = GetDefaultSelect(currentMarker);
                    from = GetDefaultFrom(currentMarker);
                    where = GetDefaultWhere(currentMarker);
                    break;                  
            }
            if (!String.IsNullOrEmpty(select) && !String.IsNullOrEmpty(from))
            {
                query = select+from+where;
            }
            return query;
        }

        public string GetSelectForEntity(HistoryBuilderMarkers marker)
        {
            string select = $@"SELECT  
                               [{marker.ImplementTypeName}].OperationDate,  
                               [{marker.ImplementTypeName}].OperationId,  
                               [{marker.ImplementTypeName}Operation].Name AS OperationName,  
                               [{marker.ImplementTypeName}].UserId, 
                               {marker.MarkerId.ToString()} AS MarkerId, ";
            if (marker.MarkerType == 12) {
                select += $" NULL  AS MarkerValueBAId, ";
            }
            else if (String.IsNullOrEmpty(marker.ImplementTypeField))
            {
                select += $" CAST([{marker.ImplementTypeName}].Value AS BIGINT)  AS MarkerValueBAId, ";
            }
            else
            {
                select += $" CAST([{marker.ImplementTypeName}].{marker.ImplementTypeField} AS BIGINT) AS MarkerValueBAId, ";
            }
            select += $"'{marker.Name}' as MarkerName, ";
            if (marker.EntityIsPeriodic == true)
            {
                select += $"CAST([{marker.EntityImplementTypeName}Entity].Value AS VARCHAR(5000)) AS MarkerValue, ";
            }
            else {
                select += $"CAST ([{marker.EntityImplementTypeName}Entity].{marker.EntityImplementTypeField} AS VARCHAR(5000)) AS MarkerValue, ";
            }
            if (marker.IsPeriodic == true && marker.IsCollectible == false)
            {

                select += $@"   CASE [{marker.ImplementTypeName}].StartDate 
                                    WHEN CONVERT(DATETIME, '1900-01-01 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName}].StartDate 
                                    END StartDate, 
                                CASE [{marker.ImplementTypeName}].EndDate 
                                    WHEN CONVERT(DATETIME, '9999-12-31 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName }].EndDate
                                    END EndDate, 
                                NULL AS ItemId,
                                [{marker.ImplementTypeName}].OldEndDate,
                                [{marker.ImplementTypeName}].OldStartDate, ";

                /*
                                CASE [{marker.ImplementTypeName}].OldEndDate
                                    WHEN CONVERT(DATETIME, '9999-12-31 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName}].OldEndDate 
                                    END OldEndDate,
                                CASE [{marker.ImplementTypeName}].OldStartDate 
                                    WHEN CONVERT(DATETIME, '1900-01-01 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName}].OldStartDate 
                                    END OldStartDate, 
                */

                if (marker.EntityIsPeriodic == true)
                {
                    select += $"CAST([{marker.EntityImplementTypeName}EntityOldValue].Value AS VARCHAR(5000)) AS OldMarkerValue ";
                }
                else
                {
                    select += $"CAST ([{marker.EntityImplementTypeName}EntityOldValue].{marker.EntityImplementTypeField} AS VARCHAR(5000)) AS OldMarkerValue ";
                }

                select += $", [{marker.ImplementTypeName}].RecId, [{marker.ImplementTypeName}].GRecId  ";
            }
            else if (marker.IsCollectible == true)
            {
                select += $@"   CASE [{marker.ImplementTypeName}].StartDate 
                                    WHEN CONVERT(DATETIME, '1900-01-01 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName}].StartDate 
                                    END StartDate, 
                                CASE [{marker.ImplementTypeName}].EndDate 
                                    WHEN CONVERT(DATETIME, '9999-12-31 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName }].EndDate
                                    END EndDate, 
                                [{marker.ImplementTypeName}].ItemId AS ItemId,
                                NULL AS OldEndDate,
                                NULL AS OldStartDate,
                                NULL AS OldMarkerValue,
                                NULL AS RecId,
                                NULL AS GRecId ";
            }
            else
            {
                select += @" NULL AS StartDate, 
                             NULL AS EndDate,
                             NULL AS ItemId,
                             NULL AS OldEndDate,
                             NULL AS OldStartDate,
                             NULL AS OldMarkerValue, 
                             NULL AS RecId,
                             NULL AS GRecId ";
            }

            return select;
        }
        public string GetFromForEntity(HistoryBuilderMarkers marker)
        {
            string from = $@" FROM 
                            [{marker.ImplementTypeName}_Journal] AS [{marker.ImplementTypeName}]
                            LEFT JOIN SOJournalOperations AS [{marker.ImplementTypeName}Operation]
                                ON [{marker.ImplementTypeName}Operation].OperationId = [{marker.ImplementTypeName}].OperationId 
                            LEFT JOIN {marker.EntityImplementTypeName} AS [{marker.EntityImplementTypeName}Entity]
                                ON CAST([{marker.EntityImplementTypeName}Entity].BAId AS BIGINT) = ";

            if (String.IsNullOrEmpty(marker.ImplementTypeField))
            {
                from += $"CAST([{marker.ImplementTypeName}].Value AS BIGINT)";
            }
            else
            {
                from += $"CAST([{marker.ImplementTypeName}].{marker.ImplementTypeField} AS BIGINT)";
            }

            if (marker.IsPeriodic && !marker.IsCollectible)
            {
                from += $@" LEFT JOIN {marker.EntityImplementTypeName} AS [{marker.EntityImplementTypeName}EntityOldValue]
                            ON [{marker.EntityImplementTypeName}EntityOldValue].BAId = CAST([{marker.ImplementTypeName}].OldValue AS BIGINT)";
            }

            //CAST([LSLegalSubjectParamValuePeriods].Value AS BIGINT)
            if (marker.EntityIsPeriodic == true) {
                from += $@" AND [{marker.EntityImplementTypeName}Entity].MarkerId = 1 
                            AND [{marker.EntityImplementTypeName}Entity].EndDate > [{marker.ImplementTypeName}].OperationDate
	                        AND [{marker.EntityImplementTypeName}Entity].StartDate <= [{marker.ImplementTypeName}].OperationDate ";
                            
            }

            return from;
        }

        // reference 
        public string GetSelectForReference(HistoryBuilderMarkers marker)
        {
            string select = $@"SELECT  
                               [{marker.ImplementTypeName}].OperationDate,  
                               [{marker.ImplementTypeName}].OperationId,  
                               [{marker.ImplementTypeName}Operation].Name AS OperationName,  
                               [{marker.ImplementTypeName}].UserId, 
                               {marker.MarkerId.ToString()} AS MarkerId, 
                               NULL AS MarkerValueBAId, 
                               '{marker.Name}' as MarkerName, 
                               CAST([{marker.ReferenceImplementTypeName}].ItemName AS VARCHAR(5000)) AS MarkerValue, ";

            if (marker.IsPeriodic == true && marker.IsCollectible == false)
            {
                select += $@"   CASE [{marker.ImplementTypeName}].StartDate 
                                    WHEN CONVERT(DATETIME, '1900-01-01 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName}].StartDate 
                                    END StartDate, 
                                CASE [{marker.ImplementTypeName}].EndDate 
                                    WHEN CONVERT(DATETIME, '9999-12-31 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName }].EndDate
                                    END EndDate, 
                                NULL AS ItemId,
                                [{marker.ImplementTypeName}].OldEndDate,
                                [{marker.ImplementTypeName}].OldStartDate, ";
                //CASE [{marker.ImplementTypeName}].OldEndDate
                //                    WHEN CONVERT(DATETIME, '9999-12-31 00:00:00',  120) 
                //                    THEN NULL 
                //                    ELSE [{marker.ImplementTypeName}].OldEndDate 
                //                    END OldEndDate,

                //                CASE [{marker.ImplementTypeName}].OldStartDate 
                //                    WHEN CONVERT(DATETIME, '1900-01-01 00:00:00',  120) 
                //                    THEN NULL 
                //                    ELSE [{marker.ImplementTypeName}].OldStartDate 
                //                    END OldStartDate,  ";
                select += $"CAST([{marker.ReferenceImplementTypeName}OldValue].ItemName AS VARCHAR(5000)) AS OldMarkerValue ";

                select += $", [{marker.ImplementTypeName}].RecId, [{marker.ImplementTypeName}].GRecId  ";
            }
            else if (marker.IsCollectible == true)
            {
                select += $@"   CASE [{marker.ImplementTypeName}].StartDate 
                                    WHEN CONVERT(DATETIME, '1900-01-01 00:00:00',  120) 
                                    THEN NULL
                                    ELSE [{marker.ImplementTypeName}].StartDate 
                                    END StartDate, 
                                CASE [{marker.ImplementTypeName}].EndDate 
                                    WHEN CONVERT(DATETIME, '9999-12-31 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName }].EndDate
                                    END EndDate, 
                                [{marker.ImplementTypeName}].ItemId AS ItemId,
                                NULL AS OldEndDate,
                                NULL AS OldStartDate,
                                NULL AS OldMarkerValue, 
                                NULL AS RecId,
                                NULL AS GRecId ";
            }
            else
            {
                select += @" NULL AS StartDate, 
                             NULL AS EndDate,
                             NULL AS ItemId,
                             NULL AS OldEndDate,
                             NULL AS OldStartDate,
                             NULL AS OldMarkerValue,
                             NULL AS RecId,
                             NULL AS GRecId ";

            }

            return select;
        }

        public string GetFromForReference(HistoryBuilderMarkers marker)
        {
            string from = $@" FROM [{marker.ImplementTypeName}_Journal] AS [{marker.ImplementTypeName}]
                              LEFT JOIN SOJournalOperations AS [{marker.ImplementTypeName}Operation] 
                                ON [{marker.ImplementTypeName}Operation].OperationId = [{marker.ImplementTypeName}].OperationId 
                              LEFT JOIN {marker.ReferenceImplementTypeName} AS [{marker.ReferenceImplementTypeName}] 
                                ON  CAST([{marker.ReferenceImplementTypeName}].ItemId AS VARCHAR(5000)) = ";

            if (String.IsNullOrEmpty(marker.ImplementTypeField))
            {
                from += $"[{marker.ImplementTypeName}].Value ";
            }
            else
            {
                from += $"[{marker.ImplementTypeName}].{marker.ImplementTypeField} ";
            }

            if (marker.IsPeriodic && !marker.IsCollectible)
            {
                from += $@"LEFT JOIN {marker.ReferenceImplementTypeName} AS [{marker.ReferenceImplementTypeName}OldValue] 
                                ON CAST([{marker.ReferenceImplementTypeName}OldValue].ItemId AS VARCHAR(5000)) = [{marker.ImplementTypeName}].OldValue ";
            }
            return from;
        }

        // list 
        public string GetSelectForList(HistoryBuilderMarkers marker)
        {

            string select = $@"SELECT  
                               [{marker.ImplementTypeName}].OperationDate,  
                               [{marker.ImplementTypeName}].OperationId,  
                               [{marker.ImplementTypeName}Operation].Name AS OperationName,  
                               [{marker.ImplementTypeName}].UserId, 
                               {marker.MarkerId.ToString()} AS MarkerId, 
                               NULL AS MarkerValueBAId, 
                               '{marker.Name}' as MarkerName, 
                               CAST([{marker.ImplementTypeName}MVL].ItemName AS VARCHAR(5000)) AS MarkerValue, ";

            if (marker.IsPeriodic == true && marker.IsCollectible == false)
            {
                select += $@"   CASE [{marker.ImplementTypeName}].StartDate 
                                    WHEN CONVERT(DATETIME, '1900-01-01 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName}].StartDate 
                                    END StartDate, 
                                CASE [{marker.ImplementTypeName}].EndDate 
                                    WHEN CONVERT(DATETIME, '9999-12-31 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName }].EndDate
                                    END EndDate, 
                                NULL AS ItemId,
                                [{marker.ImplementTypeName}].OldEndDate,
                                [{marker.ImplementTypeName}].OldStartDate, ";
                //CASE [{marker.ImplementTypeName}].OldEndDate
                //    WHEN CONVERT(DATETIME, '9999-12-31 00:00:00',  120)
                //    THEN NULL 
                //    ELSE [{marker.ImplementTypeName}].OldEndDate 
                //    END OldEndDate,

                //CASE [{marker.ImplementTypeName}].OldStartDate 
                //    WHEN CONVERT(DATETIME, '1900-01-01 00:00:00',  120) 
                //    THEN NULL 
                //    ELSE [{marker.ImplementTypeName}].OldStartDate 
                //    END OldStartDate, ";
                // old value 
                select += $"CAST([{marker.ImplementTypeName}MVLOldValue].ItemName AS VARCHAR(5000)) AS OldMarkerValue ";
                select += $", [{marker.ImplementTypeName}].RecId, [{marker.ImplementTypeName}].GRecId  ";
            }
            else if (marker.IsCollectible == true)
            {
                select += $@"   CASE [{marker.ImplementTypeName}].StartDate 
                                    WHEN CONVERT(DATETIME, '1900-01-01 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName}].StartDate 
                                    END StartDate, 
                                CASE [{marker.ImplementTypeName}].EndDate 
                                    WHEN CONVERT(DATETIME, '9999-12-31 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName }].EndDate
                                    END EndDate, 
                                [{marker.ImplementTypeName}].ItemId AS ItemId ";           
            }
            else
            {
                select += @" NULL AS StartDate, 
                             NULL AS EndDate,
                             NULL AS ItemId,
                             NULL AS OldEndDate,
                             NULL AS OldStartDate,
                             NULL AS OldMarkerValue,
                             NULL AS RecId,
                             NULL AS GRecId ";

            }

            return select;
        }
        public string GetFromForList(HistoryBuilderMarkers marker)
        {
            string from = $@" FROM [{marker.ImplementTypeName}_Journal] AS [{marker.ImplementTypeName}]
                              LEFT JOIN SOJournalOperations AS [{marker.ImplementTypeName}Operation]
                                ON [{marker.ImplementTypeName}Operation].OperationId = [{marker.ImplementTypeName}].OperationId
                              LEFT JOIN MAMarkerValueList AS [{marker.ImplementTypeName}MVL]
                                ON [{ marker.ImplementTypeName}MVL].MarkerId = {marker.MarkerId}
                                AND [{marker.ImplementTypeName}MVL].ItemId = ";

            if (String.IsNullOrEmpty(marker.ImplementTypeField))
            {
                from += $"[{marker.ImplementTypeName}].Value";
            }
            else
            {
                from += $"[{marker.ImplementTypeName}].{marker.ImplementTypeField}";
            }

            if (marker.IsPeriodic && !marker.IsCollectible)
            {
                from += $@" LEFT JOIN MAMarkerValueList AS [{marker.ImplementTypeName}MVLOldValue]
                                ON [{ marker.ImplementTypeName}MVL].MarkerId = {marker.MarkerId}
                                AND [{marker.ImplementTypeName}MVL].ItemId = [{marker.ImplementTypeName}].OldValue ";
            }

            return from;
        }

        /// string / int / 
        public string GetDefaultSelect(HistoryBuilderMarkers marker)
        {
            string select = $@"SELECT  
                               [{marker.ImplementTypeName}].OperationDate,  
                               [{marker.ImplementTypeName}].OperationId,  
                               [{marker.ImplementTypeName}Operation].Name AS OperationName,  
                               [{marker.ImplementTypeName}].UserId, 
                               {marker.MarkerId.ToString()} AS MarkerId, 
                               NULL AS MarkerValueBAId, 
                               '{marker.Name}' as MarkerName, ";
            string selectValue = "";

            if (marker.IsPeriodic || String.IsNullOrEmpty(marker.ImplementTypeField))
            {
                selectValue = $" CAST([{marker.ImplementTypeName}].Value AS VARCHAR(5000)) AS MarkerValue, ";
                // дата и время 
                if (marker.MarkerType == 10) {
                    selectValue = $"CAST( CONVERT(varchar,[{marker.ImplementTypeName}].Value,104) AS VARCHAR(5000)) AS MarkerValue, ";
                }
            }  else {

                selectValue = $" CAST([{marker.ImplementTypeName}].{marker.ImplementTypeField} AS VARCHAR(5000)) AS MarkerValue, ";
                if (marker.MarkerType == 10)
                {
                    selectValue = $"CAST(CONVERT(varchar,[{marker.ImplementTypeName}].{ marker.ImplementTypeField},104)  AS VARCHAR(5000)) AS MarkerValue, ";
                }
            }


            select += selectValue;

            if (marker.IsPeriodic == true && marker.IsCollectible == false)
            {
                select += $@"   CASE [{marker.ImplementTypeName}].StartDate 
                                    WHEN CONVERT(DATETIME, '1900-01-01 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName}].StartDate 
                                    END StartDate, 
                                CASE [{marker.ImplementTypeName}].EndDate 
                                    WHEN CONVERT(DATETIME, '9999-12-31 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName }].EndDate
                                    END EndDate, 
                                NULL AS ItemId,
                                [{marker.ImplementTypeName}].OldEndDate,
                                [{marker.ImplementTypeName}].OldStartDate, ";
                //CASE [{marker.ImplementTypeName}].OldEndDate
                //    WHEN CONVERT(DATETIME, '9999-12-31 00:00:00',  120) 
                //    THEN NULL 
                //    ELSE [{marker.ImplementTypeName}].OldEndDate 
                //    END OldEndDate,

                //CASE [{marker.ImplementTypeName}].OldStartDate 
                //    WHEN CONVERT(DATETIME, '1900-01-01 00:00:00',  120) 
                //    THEN NULL 
                //    ELSE [{marker.ImplementTypeName}].OldStartDate 
                //    END OldStartDate, ";

                //old value
                select += $"CAST([{marker.ImplementTypeName}].OldValue AS VARCHAR(5000)) AS OldMarkerValue ";
                select += $", [{marker.ImplementTypeName}].RecId, [{marker.ImplementTypeName}].GRecId  ";
            }
            else if (marker.IsCollectible == true)
            {
                select += $@"   CASE [{marker.ImplementTypeName}].StartDate 
                                    WHEN CONVERT(DATETIME, '1900-01-01 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName}].StartDate 
                                    END StartDate, 
                                CASE [{marker.ImplementTypeName}].EndDate 
                                    WHEN CONVERT(DATETIME, '9999-12-31 00:00:00',  120) 
                                    THEN NULL 
                                    ELSE [{marker.ImplementTypeName }].EndDate
                                    END EndDate, 
                                [{marker.ImplementTypeName}].ItemId AS ItemId ";
            }
            else {
                select += @" NULL AS StartDate, 
                             NULL AS EndDate,
                             NULL AS ItemId,
                             NULL AS OldEndDate,
                             NULL AS OldStartDate,
                             NULL AS OldMarkerValue,
                             NULL AS RecId,
                             NULL AS GRecId";

            }

            return select;
        }

        public string GetDefaultFrom(HistoryBuilderMarkers marker)
        {
            return $@" FROM [{marker.ImplementTypeName}_Journal] AS [{marker.ImplementTypeName}]
                       LEFT JOIN SOJournalOperations AS [{marker.ImplementTypeName}Operation] 
                            ON [{marker.ImplementTypeName}Operation].OperationId = [{marker.ImplementTypeName}].OperationId ";
        }
        public string GetDefaultWhere(HistoryBuilderMarkers marker)
        {
            string where = $@" WHERE [{marker.ImplementTypeName}].BAId = {marker.BAId.ToString()}";

            if (marker.IsCollectible || marker.IsPeriodic || String.IsNullOrEmpty(marker.ImplementTypeField)
                || marker.ImplementTypeName == "MABaseAncestorMarkerValues")
            {
                where += $" AND  [{marker.ImplementTypeName}].MarkerId ={marker.MarkerId}";
            }
            else {
                where += $" AND  [{marker.ImplementTypeName}].{marker.ImplementTypeField} IS NOT NULL ";
            }
            
            return where;

        }
    }
}

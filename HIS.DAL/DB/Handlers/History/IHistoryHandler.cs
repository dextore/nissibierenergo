﻿using System.Collections.Generic;
using HIS.DAL.DB.Context;
using HIS.DAL.Client.Models.History;

namespace HIS.DAL.DB.Handlers.History
{
    public interface IHistoryHandler : IDataContextHandler
    {
        IEnumerable<EntityHistory> GetEntityStateHistory(long baId);
        IEnumerable<EntityHistory> GetEntityMarkersHistory(long baId);
        string GetResultQuery(long baId);
        EFBAType GetBaType(long baId);

    }



}

﻿using HIS.DAL.DB.Context;
using System.Data.SqlClient;
using System.Linq;

namespace HIS.DAL.DB.Handlers.Common
{
    public class BaseAncestorsHandler : DataContextHandlerBase, IBaseAncestorsHandler
    {
        public EFBABaseAncestor GetBaseAncestor(long baId)
        {
            return this.DataContext.EFBABaseAncestors.Where(a => a.BAId == baId).FirstOrDefault();
        }

        public IQueryable<EFBABaseAncestor> GetBaseAncestors()
        {
            return this.DataContext.EFBABaseAncestors;
        }

        public int StateChange(long baId, int operationId)
        {
            ExecuteStoredProg(@"[dbo].[BA_StateChange]
	                          @iBAId		= @iBAId,
	                          @iOperationId	= @iOperationId",
                new SqlParameter("@iBAId", baId),
                new SqlParameter("@iOperationId", operationId));
            return 1;
        }
    }
}

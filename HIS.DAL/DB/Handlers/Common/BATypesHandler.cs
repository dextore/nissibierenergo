﻿using HIS.DAL.DB.Context;
using System.Linq;

namespace HIS.DAL.DB.Handlers.Common
{
    public class BATypesHandler: DataContextHandlerBase, IBATypesHandler
    {
        public IQueryable<EFBAType> GetTypes()
        {
            return DataContext.EFBATypes;
        }
    }
}

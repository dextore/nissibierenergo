﻿using HIS.DAL.DB.Context;
using System.Linq;

namespace HIS.DAL.DB.Handlers.Common
{
    public interface IBATypesHandler: IDataContextHandler
    {
        IQueryable<EFBAType> GetTypes();
    }
}

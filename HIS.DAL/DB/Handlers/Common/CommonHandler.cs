﻿using HIS.DAL.DB.Context;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.Client.Models.Common;


namespace HIS.DAL.DB.Handlers.Common
{
    public class CommonHandler: DataContextHandlerBase, ICommonHandler
    {
        /// <summary>
        /// Получить все сущности системы
        /// </summary>
        /// <returns></returns>
        public IQueryable<EFBABaseAncestor> GetBABaseAncestor()
        {
            return DataContext.EFBABaseAncestors.AsNoTracking();
        }

        /// <summary>
        /// Справочник типов сущности
        /// </summary>
        /// <returns></returns>
        public IQueryable<EFBAType> GetBATypes()
        {
            return DataContext.EFBATypes.AsNoTracking();
        }

        public IQueryable<MABATypeOptionalMarkers> GetOptionalMarkers()
        {
            return DataContext.MABATypeOptionalMarkers.AsNoTracking();
        }

        /// <summary>
        /// Справочник состояний сущностей
        /// </summary>
        /// <returns></returns>
        public IQueryable<EFSOState> GetSOStates()
        {
            return DataContext.EFSOStates.AsNoTracking();
        }

        /// <summary>
        /// Получение коллекции элементов дерева
        /// </summary>
        /// <param name="sTreeName">Наименование типа дерева</param>
        /// <param name="iEntityId">Идентификатор сущности (если NULL, то возвращать список сущностей корневого уровня)</param>
        /// <param name="iEntityParentId">Идентификатор родительской сущности</param>
        /// <param name="parentBaId">Родительский идентификатор СУЩНОСТИ для текущего элемента дерева</param>
        /// <param name="isParent">Признак родительского элемента (т.е. разворот дерева необходимо сформировать "вверх" от заданного элемента до корневого элемента)</param>
        /// <returns></returns>
        public IEnumerable<EFSystemTreeEntity> GetSystemTreeEntities(string sTreeName, long? iEntityId, long? iEntityParentId, long? parentBaId, bool? isParent)
        {
            return DataContext.Desk_GetTreeEntities(sTreeName, iEntityId, iEntityParentId, parentBaId, isParent).ToList();
        }

        public IEnumerable<string> CheckErrors(long baId)
        {
            return DataContext.Database.SqlQuery<string>(@"[dbo].[BF_CheckErrors] @iBAId = @iBAId", new SqlParameter("iBAId", baId)).ToList();

        }

        public IEnumerable<EntityViewItem> GetEntityViewsItems(int baTypeId)
        {
            string query = @"
                DECLARE @tempTable table (MarkerId int, ViewName varchar(max))
                DECLARE @query VARCHAR (MAX)

                INSERT INTO @tempTable 
                SELECT 
	                mm.MarkerId AS MarkerId,
	                m.ImplementTypeName AS ViewName
                FROM 
	                [dbo].[MABATypeMarkers] mm
                LEFT JOIN [dbo].[MAMarkers] AS m ON m.MarkerId = mm.MarkerId
                WHERE 
	                mm.BATypeId = @iBATypeId AND m.TypeId = 13

                SELECT @query = COALESCE(@query + CONCAT(' UNION SELECT ', t.MarkerId, ' AS MarkerId, ','''',t.ViewName,'''',' AS ViewName , [i',t.ViewName,'].ItemId AS ItemId , [i',
	                t.ViewName,'].ItemName AS ItemName , [i',t.ViewName,'].IsActive AS IsActive , [i',t.ViewName,'].OrderNum AS OrderNum FROM [dbo].[',t.ViewName ,'] AS [i',t.ViewName,']' ),
	                CONCAT('SELECT ', t.MarkerId, ' AS MarkerId, ', '''',t.ViewName,'''' , ' AS ViewName , [i',t.ViewName,'].ItemId AS ItemId , [i',
	                t.ViewName,'].ItemName AS ItemName , [i',t.ViewName,'].IsActive AS IsActive , [i',t.ViewName,'].OrderNum AS OrderNum FROM [dbo].[',t.ViewName ,'] AS [i',t.ViewName,']' ))
                FROM 
	                @tempTable AS t
                WHERE
                    t.ViewName IS NOT NULL 
		        
                IF (@query IS NOT NULL)	
                    BEGIN 
	                    SET @query = CONCAT('SELECT t.* FROM (', @query,') as t ORDER BY t.MarkerId , t.OrderNum ASC ')
                    END
                
                exec (@query)";

            return DataContext.Database.SqlQuery<EntityViewItem>(query, new SqlParameter("iBATypeId", baTypeId)).ToList();

        }

    }
}

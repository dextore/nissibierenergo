﻿using HIS.DAL.Client.Models.Common;
using HIS.DAL.DB.Context;
using System.Collections.Generic;
using System.Linq;

namespace HIS.DAL.DB.Handlers.Common
{
    public interface ICommonHandler: IDataContextHandler
    {
        /// <summary>
        /// Получить все сущности системы
        /// </summary>
        /// <returns></returns>
        IQueryable<EFBABaseAncestor> GetBABaseAncestor();

        /// <summary>
        /// Справочник типов сущности
        /// </summary>
        /// <returns></returns>
        IQueryable<EFBAType> GetBATypes();

        /// <summary>
        /// Справочник состояний сущностей
        /// </summary>
        /// <returns></returns>
        IQueryable<EFSOState> GetSOStates();

        /// <summary>
        /// Получение коллекции элементов дерева
        /// </summary>
        /// <param name="sTreeName">Наименование типа дерева</param>
        /// <param name="iEntityId">Идентификатор сущности (если NULL, то возвращать список сущностей корневого уровня)</param>
        /// <param name="iEntityParentId">Идентификатор родительской сущности</param>
        /// <param name="parentBaId">Родительский идентификатор СУЩНОСТИ для текущего элемента дерева</param>
        /// <param name="isParent">Признак родительского элемента (т.е. разворот дерева необходимо сформировать "вверх" от заданного элемента до корневого элемента)</param>
        /// <returns></returns>
        IEnumerable<EFSystemTreeEntity> GetSystemTreeEntities(string sTreeName, long? iEntityId, long? iEntityParentId, long? parentBaId, bool? isParent);

        IEnumerable<string> CheckErrors(long baId);
        IEnumerable<EntityViewItem> GetEntityViewsItems(int baTypeId);






    }
}

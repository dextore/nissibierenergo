﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Common;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.Common
{
    public class SystemSearchHandler : DataContextHandlerBase, ISystemSearchHandler
    {
        /// <summary>
        /// Выполнить поиск по заданным критериям
        /// </summary>
        /// <param name="searchType">Тип поиска (по коду договора, по наименованию и т.п.)</param>
        /// <param name="searchText">Строка поиска</param>
        /// <returns></returns>
        public IEnumerable<EFSystemSearchEntity> GetSystemSearchEntities(SystemSearchTypes searchType, string searchText)
        {
            return DataContext.Desk_GetSearchEntities((int)searchType, searchText);
        }

        /// <summary>
        /// Выполнить результат полнотекстового поиска сущностей по заданной строке
        /// </summary>
        /// <param name="searchText">Строка поиска</param>
        /// <returns></returns>
        public IEnumerable<BAFullTextSearchItem> GetSystemFullTextSearchEntities(string searchText)
        {
            return DataContext.GetFullTextSearchResults(searchText);
        }
    }
}
//metadata=res://*/DB.Context.HIS.csdl|res://*/DB.Context.HIS.ssdl|res://*/DB.Context.HIS.msl;provider=System.Data.SqlClient;provider connection string="data source=srv-sqltest;initial catalog=NewHermesModel;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework"
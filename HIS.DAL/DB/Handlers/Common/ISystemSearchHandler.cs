﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Common;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.Common
{
    public interface ISystemSearchHandler : IDataContextHandler
    {
        /// <summary>
        /// Выполнить поиск по заданным критериям
        /// </summary>
        /// <param name="searchType">Тип поиска (по коду договора, по наименованию и т.п.)</param>
        /// <param name="searchText">Строка поиска</param>
        /// <returns></returns>
        IEnumerable<EFSystemSearchEntity> GetSystemSearchEntities(SystemSearchTypes searchType, string searchText);

        /// <summary>
        /// Выполнить результат полнотекстового поиска сущностей по заданной строке
        /// </summary>
        /// <param name="searchText">Строка поиска</param>
        /// <returns></returns>
        IEnumerable<BAFullTextSearchItem> GetSystemFullTextSearchEntities(string searchText);
    }
}

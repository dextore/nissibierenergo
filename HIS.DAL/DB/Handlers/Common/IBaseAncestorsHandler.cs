﻿using HIS.DAL.DB.Context;
using System.Linq;

namespace HIS.DAL.DB.Handlers.Common
{
    public interface IBaseAncestorsHandler: IDataContextHandler
    {
        EFBABaseAncestor GetBaseAncestor(long baId);
        IQueryable<EFBABaseAncestor> GetBaseAncestors();

        int StateChange(long baId, int operationId);
    }
}

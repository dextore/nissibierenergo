﻿using System.Data.SqlClient;

namespace HIS.DAL.DB.Handlers.Tests
{
    public class TestDataHandler : DataContextHandlerBase, ITestDataHandler
    {
        public void UpdateTestData(bool isMessage, string value)
        {
            var bIsMessage = new SqlParameter("@bIsMessage", isMessage);
            var sValue = new SqlParameter("@sValue", value);

            ExecuteStoredProg(@"[test].[updateUpdateTestData]
                                @bIsMessage = @bIsMessage, 
                                @sValue     = @sValue",
                bIsMessage,
                sValue
            );
        }
    }
}

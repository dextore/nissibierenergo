﻿namespace HIS.DAL.DB.Handlers.Tests
{
    public interface ITestDataHandler : IDataContextHandler
    {
        void UpdateTestData(bool isMessage, string value);
    }
}
﻿using System.Linq;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.PostingOperations.CompanyAreasPlanOfAccounts
{
    interface IPoACompanyAreasPlanOfAccountsHandler: IDataContextHandler
    {
        IQueryable<EFPoACompanyAreasPlanOfAccount> Get();
    }
}

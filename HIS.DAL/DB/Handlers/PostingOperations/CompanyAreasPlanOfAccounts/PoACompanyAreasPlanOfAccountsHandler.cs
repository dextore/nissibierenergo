﻿using HIS.DAL.DB.Context;
using System.Linq;

namespace HIS.DAL.DB.Handlers.PostingOperations.CompanyAreasPlanOfAccounts
{
    public class PoACompanyAreasPlanOfAccountsHandler: DataContextHandlerBase, IPoACompanyAreasPlanOfAccountsHandler
    {
        public IQueryable<EFPoACompanyAreasPlanOfAccount> Get()
        {
            return DataContext.EFPoACompanyAreasPlanOfAccounts;
        }
    }
}

﻿using System;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public interface IContractNomenclatureHandler: IDataContextHandler
    {
        /// <summary>
        /// Ноенклатура контракта
        /// </summary>
        /// <param name="contractId">идентиффикатор контракта</param>
        /// <returns></returns>
        //IQueryable<EFCAContractNomenclature> GetNomenclatures(long contractId);

        /// <summary>
        /// Номенклатура контракта
        /// </summary>
        /// <param name="contractNomenclatureId">идентификатор номенклатыры контракта</param>
        /// <returns></returns>
        //EFCAContractNomenclature GetNomenclature(long contractNomenclatureId);

        /// <summary>
        /// Добавить изменить номенклатуру контракта
        /// </summary>
        /// <param name="contractNomId"></param>
        /// <param name="contractId"></param>
        /// <param name="supplierId"></param>
        /// <param name="nomenclatureId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        long Save(long? contractNomId,
                  long contractId,
                  long supplierId,
                  long nomenclatureId,
                  DateTime startDate,
                  DateTime endDate);
    }
}

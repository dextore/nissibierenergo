﻿using HIS.DAL.DB.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.Contracts;
using HIS.DAL.Client.Models.SystemTree;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public class ContractsHandler : DataContextHandlerBase, IContractsHandler
    {
        public IQueryable<EFCAContract> GetContracts()
        {
            return DataContext.CAContracts;
        }

        public EFCAContract GetContract(long contractId)
        {
            return DataContext.CAContracts.Where(c => c.BAId == contractId).FirstOrDefault();
        }

        public IEnumerable<SystemTreeItem> GetLSContracts(long baId)
        {
            return DataContext.CAContracts.Where(m => m.LSId == baId)
                .Select(m => new SystemTreeItem
                    {
                        BAId = m.BAId,
                        Name = m.Number,
                        BATypeId = m.BATypeId,
                        BATypeName = m.BATypes.Name,
                        MVCAlias = m.BATypes.MVCAlias,
                        BATypeNameShort = m.BATypes.ShortName,
                        StateId = m.StateId,
                        StateName = m.SOStates.Name
                    }).ToArray();
        }

        //public long Save(ContractUpdate model)
        //{
        //    //long batypeId = BATypeCodes.ContractId;

        //    //var iBAId =
        //    //    new SqlParameter("@iBAId", model.BAId.HasValue ? model.BAId.Value : (object) DBNull.Value)
        //    //    {
        //    //        Direction = System.Data.ParameterDirection.InputOutput,
        //    //        SqlDbType = System.Data.SqlDbType.BigInt
        //    //    };
        //    //var sNumber = new SqlParameter("@sNumber", model.Name);
        //    //var dDocDate = new SqlParameter("@dDocDate", model.DocDate);
        //    //var iCAId = new SqlParameter("@iCAId", model.CompanyArea);
        //    //var iLSId = new SqlParameter("@iLSId", model.LegalSubject);

        //    //var iBATypeId = new SqlParameter("@iBATypeId", BATypes.Contract);

        //    //var iSupplierId = new SqlParameter("@iSupplierId", supplierId);

        //    //var dStartDate = new SqlParameter("@dStartDate", startDate);

        //    //ExecuteStoredProg(@"[dbo].[CA_ContractUpdate]
        //    //        @iBAId       = @iBAId OUT,
        //    //        @sNumber     = @sNumber,
        //    //        @dDocDate    = @dDocDate,
        //    //    	@iCAId		 = @iCAId,
        //    //    	@iLSId		 = @iLSId,

        //    //        @iBATypeId   = @iBATypeId,
        //    //    	@iLSId		 = @iLSId,
        //    //    	@iSupplierId = @iSupplierId,
        //    //        @dStartDate  = @dStartDate",

        //    //        iBAId,
        //    //        sNumber,
        //    //        dDocDate,
        //    //        iCAId,

        //    //        iBATypeId,
        //    //        iLSId,
        //    //        iSupplierId,
        //    //        sNumber,
        //    //        dStartDate
        //    //);

        //    //return (long)iContractId.Value;

        //    throw new NotImplementedException();
        //}
    }
}

﻿using System.Collections.Generic;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public interface IContractInventsHandler: IDataContextHandler
    {
        IEnumerable<EFCAContractInvent> Get();
    }
}
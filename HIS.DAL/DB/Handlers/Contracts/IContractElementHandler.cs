﻿using System;
using System.Linq;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public interface IContractElementHandler: IDataContextHandler
    {
        //IQueryable<EFCAContractElement> GetElements();
        /// <summary>
        /// Элемент номенклатуры контракта
        /// </summary>
        /// <param name="contractId">идентиффикатор контракта</param>
        /// <param name="nomenclatureId">идентиффикатор номенклатуры</param>/// 
        /// <returns></returns>
        //IQueryable<EFCAContractElement> GetElements(long contractId, long nomenclatureId);
        /// <summary>
        /// Элементы номенклатуры контракта по паренту
        /// </summary>
        /// <param name="contractId">идентиффикатор контракта</param>
        /// <param name="nomenclatureId">идентиффикатор номенклатуры</param>/// 
        /// <param name="parentId">идентиффикатор старшего элемента номенклатуры контракта</param>/// 
        /// <returns></returns>
        IQueryable<ContractElementEF> GetContractElements(long contractId, long nomenclatureId, long? parentId);

        /// <summary>
        /// Элемент номенклатуры контракта по идентификатору
        /// </summary>
        /// <param name="GetContractElement"></param>
        /// <returns></returns>
        ContractElementEF GetContractElement(long GetContractElement);

        /// <summary>
        /// Элемент номенклатуры контракта
        /// </summary>
        /// <param name="contractElementId">идентификатор элемента номенклатыры контракта</param>
        /// <returns></returns>
        //EFCAContractElement GetElement(long contractElementId);

        /// <summary>
        /// Добавить изменить элемент номенклатуры контракта
        /// </summary>
        /// <param name="contractElmId"></param>
        /// <param name="contractId"></param>
        /// <param name="nomenclatureId"></param>
        /// <param name="elementId"></param>
        /// <param name="startDate"></param>
        /// <param name="parentId"></param>
        /// <param name="endDate"></param>
        /// <param name="name"></param>
        /// <param name="note"></param>
        /// <param name="addrId"></param>
        /// <returns></returns>
        long Save(long? contractElmId,
                  long contractId,
                  long nomenclatureId,
                  long elementId,
                  DateTime startDate,
                  long? parentId,
                  DateTime? endDate,
                  string name,
                  string note,
                  long? addrId);

    }
}

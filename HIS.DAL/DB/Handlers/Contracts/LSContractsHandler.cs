﻿using System;
using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.Contracts;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public class LSContractsHandler: DataContextHandlerBase, ILSContractsHandler
    {
        public IEnumerable<LSContract> ByLegalSubject(long lsId)
        {
            var sql = string.Format(Clssql, "{d '" + DateTime.Now.ToString("yyyy-MM-dd") + "'}", lsId);

            var contracts = DataContext.Database.SqlQuery<LSContract>(sql);
            return contracts.Select(m => m).ToArray();
        }

        private const string Clssql = @"SELECT [Contract].[BAId]
                                          ,[Contract].[BATypeId]
                                          ,[Contract].[StateId]
                                          ,[Contract].[Number]
                                          ,[Contract].[DocDate]
                                          ,[Contract].[CAId]
                                          ,[Contract].[LSId]
                                          ,[Contract].[TypeId]
                                          ,[Contract].[StartDate]
                                          ,[Contract].[EndDate]
                                          ,[Contract].[Note]
                                          ,[Contract].[CreatorId]
                                          ,[UKGroup]         = [UKGroupValues].[ItemName]
                                          ,[TypeName]        = [TypeValues].[ItemName]
                                          ,[State]           = [states].[Name]
	                                      ,[IsDelState]      = [SOBATypeStates].IsDelState
	                                      ,[IsFirstState]    = [SOBATypeStates].IsFirstState
                                          ,[CompanyAreaName] = [LSCompanyAreas].[Name]
                                      FROM dbo.CAContracts AS [Contract]
                                      LEFT JOIN dbo.CAContractParamValuePeriods AS [UKGroup] ON [UKGroup].BAId       = [Contract].BAId
	                                                                                      AND [UKGroup].MarkerId   = 201
	                                                                                      AND [UKGroup].StartDate  <= {0}
	                                                                                      AND [UKGroup].EndDate    > {0}
                                      LEFT JOIN dbo.MAMarkerValueList as [UKGroupValues] ON [UKGroupValues].MarkerId = 201
	                                                                                  AND [UKGroupValues].ItemId   = [UKGroup].sValue
                                     LEFT JOIN dbo.MAMarkerValueList as [TypeValues] ON [TypeValues].MarkerId = 200
	                                                                              AND [TypeValues].ItemId   = [Contract] .TypeId
                                     LEFT JOIN dbo.SOStates AS [states] ON [states].StateId = [Contract].StateId
                                     LEFT JOIN dbo.SOBATypeStates AS [SOBATypeStates] ON [SOBATypeStates].StateId  =  [states].StateId
	                                                                             AND [SOBATypeStates].BATypeId = [Contract].BATypeId
                                     LEFT JOIN dbo.LSCompanyAreas AS [LSCompanyAreas] ON [LSCompanyAreas].BAId = [Contract].CAId 
                                     WHERE [Contract].[LSId] = {1}";

    }
}

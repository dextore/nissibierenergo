﻿using System;
using System.Data.SqlClient;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public class ContractElementTermHandler: DataContextHandlerBase, IContractElementTermHandler
    {
        //public IQueryable<EFCAContractElementTermValue> GetContracеElementTerms()
        //{
        //    return DataContext.EFCAContractElementTermValues;
        //}

        //public IQueryable<EFCAContractElementTermValue> GetContracеElementTerms(long contractElementId)
        //{
        //    return DataContext.EFCAContractElementTermValues.Where(cn => cn.BAId == contractElementId);
        //}

        //public EFCAContractElementTermValue GetContractElementTerm(long contractElementId, int markerId, DateTime date)
        //{
        //    return DataContext.EFCAContractElementTermValues.Where(cn => cn.BAId == contractElementId &&
        //                                                                 cn.MarkerId == markerId &&
        //                                                                 cn.StartDate <= date &&
        //                                                                 cn.EndDate >= date).FirstOrDefault();
        //}

        public void Save(long contractElementId, int markerId, DateTime? startDate, DateTime? endDate, string value, string note)
        {
            var iContractElmId = new SqlParameter("@iContractElmId", contractElementId);
            var iTermId = new SqlParameter("@iTermId", markerId);
            var dStartDate = new SqlParameter("@dStartDate", startDate);
            var dEndDate = new SqlParameter("@dEndDate", endDate.HasValue ? endDate.Value : (object)DBNull.Value);
            dEndDate.Direction = System.Data.ParameterDirection.InputOutput;
            dEndDate.SqlDbType = System.Data.SqlDbType.DateTime;
            var vValue = new SqlParameter("@vValue", value);
            var sNote = new SqlParameter("@sNote", !string.IsNullOrEmpty(note) ? note : (object)DBNull.Value);

            ExecuteStoredProg(@"[dbo].[CA_ElementTermUpdate]
                              @iContractElmId = @iContractElmId,
                              @iTermId        = @iTermId,
                              @dStartDate     = @dStartDate,
                              @dEndDate       = @dEndDate,
                              @vValue         = @vValue,
                              @sNote          = @sNote",
                              iContractElmId,
                              iTermId,
                              dStartDate,
                              dEndDate,
                              vValue,
                              sNote);
        }

        public void Delete(long contractElementId, int markerId, DateTime startDate)
        {
            var iContractElmId = new SqlParameter("@iContractElmId", contractElementId);
            var iTermId = new SqlParameter("@iTermId", markerId);
            var dStartDate = new SqlParameter("@dStartDate", startDate);

            ExecuteStoredProg(@"[dbo].[CA_ElementTermDelete]
                              @iContractElmId = @iContractElmId,
                              @iTermId        = @iTermId,
                              @dStartDate     = @dStartDate",
                              iContractElmId,
                              iTermId,
                              dStartDate);
        }

    }
}

﻿using System;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public class ContractNomenclatureTermHandler: DataContextHandlerBase, IContractNomenclatureTermHandler
    {
        //public IQueryable<EFCAContractNomenclatureTermValue> GetContractNomenclatureTerms()
        //{
        //    throw new NotImplementedException();
        //    //return DataContext.EFCAContractNomenclatureTermValues;
        //}

        //public IQueryable<EFCAContractNomenclatureTermValue> GetContractNomenclatureTerms(long contractNomenclatureId)
        //{
        //    throw new NotImplementedException();
        //    //return DataContext.EFCAContractNomenclatureTermValues.Where(cn => cn.BAId == contractNomenclatureId);
        //}

        //public EFCAContractNomenclatureTermValue GetContractNomenclatureTerm(long contractNomenclatureId, int markerId, DateTime date)
        //{
        //    throw new NotImplementedException();
        //    //return DataContext.EFCAContractNomenclatureTermValues.Where(cn => cn.BAId == contractNomenclatureId &&
        //    //                                                                  cn.MarkerId == markerId &&
        //    //                                                                  cn.StartDate <= date &&
        //    //                                                                  cn.EndDate >= date).FirstOrDefault();
        //}
        public void Save(long contractNomenclatureId, int markerId, DateTime? startDate, DateTime? endDate, string value, string note)
        {
            throw new NotImplementedException();
            //var iContractNomId = new SqlParameter("@iContractNomId", contractNomenclatureId);
            //var iTermId = new SqlParameter("@iTermId", markerId);
            //var dStartDate = new SqlParameter("@dStartDate", startDate);
            //var dEndDate = new SqlParameter("@dEndDate", endDate.HasValue ? endDate.Value : (object)DBNull.Value);
            //dEndDate.Direction = System.Data.ParameterDirection.InputOutput;
            //dEndDate.SqlDbType = System.Data.SqlDbType.DateTime;
            //var vValue = new SqlParameter("@vValue", value);
            //var sNote = new SqlParameter("@sNote", !string.IsNullOrEmpty(note) ? note : (object)DBNull.Value);

            //ExecuteStoredProg(@"[dbo].[CA_NomenclatureTermUpdate]
            //                  @iContractNomId = @iContractNomId,
            //                  @iTermId = @iTermId,
            //                  @dStartDate = @dStartDate,
            //                  @dEndDate = @dEndDate,
            //                  @vValue = @vValue,
            //                  @sNote = @sNote",
            //                  iContractNomId,
            //                  iTermId,
            //                  dStartDate,
            //                  dEndDate,
            //                  vValue,
            //                  sNote);
        }

        public void Delete(long contractNomenclatureId, int markerId, DateTime startDate)
        {
            throw new NotImplementedException();
            //var iContractNomId = new SqlParameter("@iContractNomId", contractNomenclatureId);
            //var iTermId = new SqlParameter("@iTermId", markerId);
            //var dStartDate = new SqlParameter("@dStartDate", startDate);
            //
            //ExecuteStoredProg(@"[dbo].[CA_NomenclatureTermDelete]
            //                  @iContractNomId = @iContractNomId,
            //                  @iTermId = @iTermId,
            //                  @dStartDate = @dStartDate",
            //                  iContractNomId,
            //                  iTermId,
            //                  dStartDate);
        }
    }
}

﻿using System.Collections.Generic;
using HIS.DAL.DB.Context;
using System.Linq;
using HIS.DAL.Client.Models.Contracts;
using HIS.DAL.Client.Models.SystemTree;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public  interface IContractsHandler: IDataContextHandler
    {
        /// <summary>
        /// Возвращает список договоров
        /// </summary>
        /// <returns></returns>
        IQueryable<EFCAContract> GetContracts();

        /// <summary>
        /// Возвращает модель договора
        /// </summary>
        /// <param name="contractId">Идентификатор договора</param>
        /// <returns></returns>
        EFCAContract GetContract(long contractId);

        IEnumerable<SystemTreeItem> GetLSContracts(long baId);

        /// <summary>
        /// Сохраняет шапку договора
        /// </summary>
        /// <param name="contractId">Идентификатора договора</param>
        /// <param name="supplierId">Идентификатора поставщика</param>
        /// <param name="consumerId">Идентификатора потребителя</param>
        /// <param name="number">Номер договора</param>
        /// <param name="startDate">Дата начала действия договора</param>
        /// <returns></returns>
        //long Save(ContractUpdate model);
    }
}

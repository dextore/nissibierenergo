﻿using System;
using System.Data.SqlClient;
using System.Linq;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public class ContractElementHandler: DataContextHandlerBase, IContractElementHandler
    {
        //public IQueryable<EFCAContractElement> GetElements() {
        //    return DataContext.EFCAContractElements;
        //}

        //public IQueryable<EFCAContractElement> GetElements(long contractId, long nomenclatureId)
        //{
        //    return DataContext.EFCAContractElements.Where(m => m.ContractId == contractId &&
        //                                                       m.NomenclatureId == nomenclatureId);
        //}

        public IQueryable<ContractElementEF> GetContractElements(long contractId, long nomenclatureId, long? parentId)
        {
            throw new NotImplementedException();
            //    return DataContext.EFCAContractElements
            //        .Where(m => m.ContractId == contractId && 
            //                    m.NomenclatureId == nomenclatureId &&
            //                    m.ParentId == parentId)
            //        .GroupJoin(DataContext.EFMABaseAncestorMarkerValuePeriods.Where(mt => mt.MarkerId == 5),
            //                   r => r.ElementId,
            //                   tm => tm.BAId,
            //                   (r, tm) => new { el = r, tm = tm.DefaultIfEmpty() })
            //        .Select(r => new ContractElementEF
            //        {
            //                            ContractElementId = r.el.BAId,
            //                            ContractId = r.el.ContractId,
            //                            NomenclatureId = r.el.NomenclatureId,
            //                            ElementId = r.el.ElementId,
            //                            StartDate = r.el.StartDate,
            //                            ParentId = r.el.ParentId,
            //                            EndDate = r.el.EndDate,
            //                            IsProject = r.el.IsProject,
            //                            ElementCode = r.el.ElmCode,
            //                            ElementTypeCode = r.el.CEElements.BABaseAncestors.BATypes.MVCAlias,
            //                            ElementType = r.el.CEElements.BABaseAncestors.BATypes.Name,
            //                            Name = r.el.Name,
            //                            Note = r.el.Note,
            //                            Address = r.tm.Any() ? r.tm.FirstOrDefault().sValue : "",
            //                            HasItems = DataContext.EFCAContractElements.Any(c => c.ParentId == r.el.BAId)
            //        });
        }

        public ContractElementEF GetContractElement(long contractElementId)
        {
            throw new NotImplementedException();
            //    return DataContext.EFCAContractElements
            //        .Where(m => m.BAId == contractElementId)
            //                    .Join(DataContext.EFBABaseAncestors,
            //                           c => c.ElementId,
            //                           a => a.BAId,
            //                           (c, b) => new { c, b })
            //                    .Join(DataContext.EFBATypes,
            //                          c => c. b.TypeId,
            //                          t => t.TypeId,
            //                          (c, t) => new { el = c.c, BATypeName = t.Name, BATypeId = t.TypeId })
            //                    .GroupJoin(DataContext.EFMABaseAncestorMarkerValuePeriods.Where(mt => mt.MarkerId == 5),
            //                          r => r.el.ElementId,
            //                          tm => tm.BAId,
            //                          (r, tm) => new { el = r.el, r.BATypeName, r.BATypeId, tm = tm.DefaultIfEmpty() })
            //                    .Select(m => new ContractElementEF
            //                    {
            //                        ContractElementId = m.el.BAId,
            //                        ContractId = m.el.ContractId,
            //                        NomenclatureId = m.el.NomenclatureId,
            //                        ElementId = m.el.ElementId,
            //                        StartDate = m.el.StartDate,
            //                        ParentId = m.el.ParentId,
            //                        EndDate = m.el.EndDate,
            //                        IsProject = m.el.IsProject,
            //                        ElementCode = m.el.ElmCode,
            //                        ElementTypeCode = m.el.CEElements.BABaseAncestors.BATypes.MVCAlias,
            //                        Name = m.el.Name,
            //                        Note = m.el.Note,
            //                        ElementType = m.BATypeName,
            //                        Address = m.tm.Any() ? m.tm.FirstOrDefault().sValue : "",
            //                        HasItems = DataContext.EFCAContractElements.Any(c => c.BAId == m.el.ParentId)
            //                    }).FirstOrDefault(); 
            //}

            //public EFCAContractElement GetElement(long contractElementId)
            //{
            //    return DataContext.EFCAContractElements.Where(m => m.BAId == contractElementId).FirstOrDefault();
        }

        public long Save(long? contractElmId,
                         long contractId,
                         long nomenclatureId,
                         long elementId,
                         DateTime startDate,
                         long? parentId,
                         DateTime? endDate,
                         string name,
                         string note,
                         long? addrId)
        {
            var iContractElmId = new SqlParameter("@iContractElmId", contractElmId.HasValue ? contractElmId.Value : (object)DBNull.Value);
            iContractElmId.Direction = System.Data.ParameterDirection.InputOutput;
            iContractElmId.SqlDbType = System.Data.SqlDbType.BigInt;
            var iContractId = new SqlParameter("@iContractId", contractId);
            var iNomenclatureId = new SqlParameter("@iNomenclatureId", nomenclatureId);
            var iElementId = new SqlParameter("@iElementId", elementId);
            var dStartDate = new SqlParameter("@dStartDate", startDate);
            var iParentId = new SqlParameter("@iParentId", parentId.HasValue ? parentId.Value : (object)DBNull.Value);
            iParentId.Direction = System.Data.ParameterDirection.InputOutput;
            iParentId.SqlDbType = System.Data.SqlDbType.BigInt;
            var dEndDate = new SqlParameter("@dEndDate", endDate.HasValue ? endDate.Value : (object)DBNull.Value);
            dEndDate.Direction = System.Data.ParameterDirection.InputOutput;
            dEndDate.SqlDbType = System.Data.SqlDbType.DateTime;
            var sName = new SqlParameter("@sName", name);
            var sNote = new SqlParameter("@sNote", !string.IsNullOrEmpty(note) ? note : (object)DBNull.Value);
            sNote.Direction = System.Data.ParameterDirection.InputOutput;
            sNote.SqlDbType = System.Data.SqlDbType.NVarChar;
            sNote.Size = 4000;
            var iAddrId = new SqlParameter("@iAddrId", addrId.HasValue ? addrId.Value : (object)DBNull.Value);
            iAddrId.Direction = System.Data.ParameterDirection.InputOutput;
            iAddrId.SqlDbType = System.Data.SqlDbType.BigInt;

            ExecuteStoredProg(@"[dbo].[CA_ElementUpdate]
	                                @iContractElmId	 = @iContractElmId  OUTPUT,
	                                @iContractId	 = @iContractId,
	                                @iNomenclatureId = @iNomenclatureId,
	                                @iElementId		 = @iElementId,
	                                @dStartDate		 = @dStartDate,
	                                @iParentId		 = @iParentId,
	                                @dEndDate		 = @dEndDate,
	                                @sName			 = @sName,
	                                @sNote			 = @sNote,
	                                @iAddrId		 = @iAddrId",
                                iContractElmId,
                                iContractId,
                                iNomenclatureId,
                                iElementId,
                                dStartDate,
                                iParentId,
                                dEndDate,
                                sName,
                                sNote,
                                iAddrId);

            return (long)iContractElmId.Value;

        }

    }
}

public class ContractElementEF
{
    public long? ContractElementId { get; set; }
    public long ContractId { get; set; }
    public long NomenclatureId { get; set; }
    public long ElementId { get; set; }
    public DateTime StartDate { get; set; }
    public long? ParentId { get; set; }
    public DateTime? EndDate { get; set; }
    public bool? IsProject { get; set; }
    public string ElementCode { get; set; }
    public string ElementType { get; set; }
    public string ElementTypeCode { get; set; }
    public string Address { get; set; }
    public string Name { get; set; }
    public string Note { get; set; }
    public bool HasItems { get; set; }
}

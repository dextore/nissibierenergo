﻿using System;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public interface IContractTermsHandler : IDataContextHandler
    {
        //IQueryable<EFCAContractParamValue> GetContractTerms();
        //IQueryable<EFCAContractParamValue> GetContractTerms(long contractId);
        //EFCAContractParamValue GetTerm(long contractId, int markerId, DateTime date);
        void Save(long contractId, int markerId, DateTime? startDate, DateTime? endDate, string value, string note);
        void Delete(long contractId, int markerId, DateTime startDate);
    }
}

﻿using System;
using System.Data.SqlClient;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public class ContractNomenclatureHandler: DataContextHandlerBase, IContractNomenclatureHandler
    {
        //public IQueryable<EFCAContractNomenclature> GetNomenclatures(long contractId)
        //{
        //    return DataContext.EFCAContractNomenclatures.Where(m => m.ContractId == contractId);
        //}

        //public EFCAContractNomenclature GetNomenclature(long contractNomenclatureId)
        //{
        //    return DataContext.EFCAContractNomenclatures.Where(m => m.BAId == contractNomenclatureId).FirstOrDefault();
        //}


        public long Save(long? contractNomId,
                         long contractId,
                         long supplierId,
                         long nomenclatureId,
                         DateTime startDate,
                         DateTime endDate)
        {
            var iContractNomId = new SqlParameter("@iContractNomId", contractNomId.HasValue ? contractNomId.Value : (object)DBNull.Value);
            iContractNomId.Direction = System.Data.ParameterDirection.InputOutput;
            iContractNomId.SqlDbType = System.Data.SqlDbType.BigInt;
            var iContractId = new SqlParameter("@iContractId", contractId);
            var iSupplierId = new SqlParameter("@iSupplierId", supplierId);
            var iNomenclatureId = new SqlParameter("@iNomenclatureId", nomenclatureId);
            var dStartDate = new SqlParameter("@dStartDate", startDate);
            var dEndDate = new SqlParameter("@dEndDate", endDate);

            ExecuteStoredProg(@"[dbo].[CA_NomenclatureUpdate]
                                @iContractNomId	 = @iContractNomId OUTPUT,
	                            @iContractId	 = @iContractId,
	                            @iSupplierId	 = @iSupplierId,
	                            @iNomenclatureId = @iNomenclatureId,
	                            @dStartDate	     = @dStartDate,
	                            @dEndDate	     = @dEndDate",
                                iContractNomId,
                                iContractId,
                                iSupplierId,
                                iNomenclatureId,
                                dStartDate,
                                dEndDate);

            return (long)iContractNomId.Value;
        }
    }
}

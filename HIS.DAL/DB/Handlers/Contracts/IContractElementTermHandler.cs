﻿using System;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public interface IContractElementTermHandler: IDataContextHandler
    {
        /// <summary>
        /// Условия элемента номенклатуры контракта
        /// </summary>
        /// <returns></returns>
        //IQueryable<EFCAContractElementTermValue> GetContracеElementTerms();

        ///// <summary>
        /////  Условия элемента номенклатуры контракта
        ///// </summary>
        ///// <param name="contractElementId">идентификатора элемента номенклатуры контракта</param>
        ///// <returns></returns>
        //IQueryable<EFCAContractElementTermValue> GetContracеElementTerms(long contractElementId);

        ///// <summary>
        /////  Условие элемента номенклатуры контракта
        ///// </summary>
        ///// <param name="contractElementId"></param>
        ///// <param name="markerId"></param>
        ///// <param name="date"></param>
        ///// <returns></returns>
        //EFCAContractElementTermValue GetContractElementTerm(long contractElementId, int markerId, DateTime date);

        /// <summary>
        /// Добавляет / изменяет условие элемента номенклатуры контракта
        /// </summary>
        /// <param name="contractElementId"></param>
        /// <param name="markerId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="value"></param>
        /// <param name="note"></param>
        void Save(long contractElementId, int markerId, DateTime? startDate, DateTime? endDate, string value, string note);

        /// <summary>
        /// Удаляет условие элемента номенклатуры контракта
        /// </summary>
        /// <param name="contractElementId"></param>
        /// <param name="markerId"></param>
        /// <param name="startDate"></param>
        void Delete(long contractElementId, int markerId, DateTime startDate);
    }
}

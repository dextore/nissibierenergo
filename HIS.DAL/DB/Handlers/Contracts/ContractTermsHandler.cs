﻿using System;
using System.Data.SqlClient;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public class ContractTermsHandler : DataContextHandlerBase, IContractTermsHandler
    {
        //public IQueryable<EFCAContractParamValue> GetContractTerms()
        //{
        //    return DataContext.EFCAContractParamValues;
        //}

        //public IQueryable<EFCAContractParamValue> GetContractTerms(long contractId)
        //{
        //    return DataContext.EFCAContractParamValues.Where(t => t.BAId == contractId);
        //}

        //public EFCAContractParamValue GetTerm(long contractId, int markerId, DateTime date)
        //{
        //    return DataContext.EFCAContractParamValues.Where(ct => ct.BAId == contractId &&
        //                                                          ct.MarkerId == markerId &&
        //                                                          ct.StartDate <= date &&
        //                                                          ct.EndDate >= date).FirstOrDefault();
        //}

        public void Save(long contractId, int markerId, DateTime? startDate, DateTime? endDate, string value, string note)
        {
            var iContractId = new SqlParameter("@iContractId", contractId);
            var iTermId = new SqlParameter("@iTermId", markerId);
            var dStartDate = new SqlParameter("@dStartDate", startDate);
            var dEndDate = new SqlParameter("@dEndDate", endDate.HasValue ? endDate.Value : (object)DBNull.Value );
            dEndDate.Direction = System.Data.ParameterDirection.InputOutput;
            dEndDate.SqlDbType = System.Data.SqlDbType.DateTime;
            var vValue = new SqlParameter("@vValue", value);
            var sNote = new SqlParameter("@sNote", !string.IsNullOrEmpty(note) ? note : (object)DBNull.Value);

            ExecuteStoredProg(@"[dbo].[CA_ContractTermUpdate]
                              @iContractId = @iContractId,
                              @iTermId = @iTermId,
                              @dStartDate = @dStartDate,
                              @dEndDate = @dEndDate,
                              @vValue = @vValue,
                              @sNote = @sNote",
                              iContractId,
                              iTermId,
                              dStartDate,
                              dEndDate,
                              vValue,
                              sNote);
        }

        public void Delete(long contractId, int markerId, DateTime startDate)
        {
            var iContractId = new SqlParameter("@iContractId", contractId);
            var iTermId = new SqlParameter("@iTermId", markerId);
            var dStartDate = new SqlParameter("@dStartDate", startDate);

            ExecuteStoredProg(@"[dbo].[CA_ContractTermDelete]
                              @iContractId = @iContractId,
                              @iTermId = @iTermId,
                              @dStartDate = @dStartDate",
                              iContractId,
                              iTermId,
                              dStartDate);

        }
    }
}

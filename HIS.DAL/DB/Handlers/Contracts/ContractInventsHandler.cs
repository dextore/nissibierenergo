﻿using System.Collections.Generic;
using System.Linq;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public class ContractInventsHandler : DataContextHandlerBase, IContractInventsHandler
    {
        public IEnumerable<EFCAContractInvent> Get()
        {
            return DataContext.EFCAContractInvents;
        }
    }
}

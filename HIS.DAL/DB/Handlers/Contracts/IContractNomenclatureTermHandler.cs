﻿using System;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public interface IContractNomenclatureTermHandler: IDataContextHandler
    {
        //IQueryable<EFCAContractNomenclatureTermValue> GetContractNomenclatureTerms();
        //IQueryable<EFCAContractNomenclatureTermValue> GetContractNomenclatureTerms(long contractNomenclatureId);
        //EFCAContractNomenclatureTermValue GetContractNomenclatureTerm(long contractNomenclatureId, int markerId, DateTime date);
        void Save(long contractNomenclatureId, int markerId, DateTime? startDate, DateTime? endDate, string value, string note);
        void Delete(long contractNomenclatureId, int markerId, DateTime startDate);
    }
}

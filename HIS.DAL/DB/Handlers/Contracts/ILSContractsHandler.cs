﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Contracts;

namespace HIS.DAL.DB.Handlers.Contracts
{
    public interface ILSContractsHandler : IDataContextHandler
    {
        IEnumerable<LSContract> ByLegalSubject(long lsId);
    }
}

﻿using HIS.DAL.DB.Context;
using System.Linq;

namespace HIS.DAL.DB.Handlers.TaxRates
{
    public class TaxRatesHandler: DataContextHandlerBase, ITaxRatesHandler
    {
        public IQueryable<EFITaxRate> GetTaxRaites()
        {
            return DataContext.EFITaxRates;
        }
    }
}

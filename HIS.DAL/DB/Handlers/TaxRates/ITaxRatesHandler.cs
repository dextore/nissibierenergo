﻿using HIS.DAL.DB.Context;
using System.Linq;

namespace HIS.DAL.DB.Handlers.TaxRates
{
    public interface ITaxRatesHandler: IDataContextHandler
    {
        IQueryable<EFITaxRate> GetTaxRaites();
    }
}

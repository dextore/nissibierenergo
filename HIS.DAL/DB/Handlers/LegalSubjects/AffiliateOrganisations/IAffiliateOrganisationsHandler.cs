﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.LegalSubjects.AffiliateOrganisations;

namespace HIS.DAL.DB.Handlers.LegalSubjects.AffiliateOrganisations
{
    public interface IAffiliateOrganisationsHandler: ILegalSubjectHandlerBase
    {
        /// <summary>
        ///  Возвращает список филиалов по юр. лицу.
        /// </summary>
        /// <param name="organisactionId"></param>
        /// <returns></returns>
        IEnumerable<AffiliateOrganisation> GetAffiliateOrganisations(long organisactionId);

        /// <summary>
        /// Филиал по юр. лица.
        /// </summary>
        /// <param name="affiliateOrganisationId"></param>
        /// <returns></returns>
        AffiliateOrganisation GetAffiliateOrganisation(long affiliateOrganisationId);

        /// <summary>
        /// Сохраняет изменения юр. лица.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long Update(AffiliateOrganisation model);
    }
}

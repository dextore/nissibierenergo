﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.Client.Models.LegalSubjects.AffiliateOrganisations;
using HIS.DAL.DB.Common;

namespace HIS.DAL.DB.Handlers.LegalSubjects.AffiliateOrganisations
{
    public class AffiliateOrganisationsHandler : LegalSubjectHandlerBase, IAffiliateOrganisationsHandler
    {
        public IEnumerable<AffiliateOrganisation> GetAffiliateOrganisations(long organisactionId)
        {
            return DataContext.EFLSLegalSubjects.AsNoTracking()
                .Where(m => m.ParentId == organisactionId && m.BATypeId == (int)BATypes.AffiliateOrganisation)
                .Select(m => new AffiliateOrganisation
                {
                    LegalPersonId = m.BAId,
                    ParentId = (long)m.ParentId,
                    Name = m.Name,
                    FullName = m.FullName,
                    Code = m.Code,
                    INN = m.INN,
                    Note = m.Note
                }).ToArray();
        }

        public AffiliateOrganisation GetAffiliateOrganisation(long affiliateOrganisationId)
        {
            var organisation = DataContext.EFLSLegalSubjects.AsNoTracking()
                .FirstOrDefault(m =>
                    m.BAId == affiliateOrganisationId && m.BATypeId == (int) BATypes.AffiliateOrganisation);

            if (organisation == null)
                return null; 

            return new AffiliateOrganisation
            {
                LegalPersonId = organisation.BAId,
                ParentId = (long)organisation.ParentId,
                Name = organisation.Name,
                FullName = organisation.FullName,
                Code = organisation.Code,
                INN = organisation.INN,
                Note = organisation.Note
            };
        }

        public long Update(AffiliateOrganisation model)
        {
            var ibaId = new SqlParameter("@iBAId", model.LegalPersonId ?? (object) DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.BigInt
            };

            var sCode = new SqlParameter("@sCode", model.Code ?? (object) DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };

            var sName = new SqlParameter("@sName", model.Name ?? "")
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };

            var sFullName = new SqlParameter("@sFullName", model.FullName ?? "")
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };

            var iParentId = new SqlParameter("@iParentId", model.ParentId);

            var sInn = new SqlParameter("@sINN", model.INN)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 12
            };

            var sNote = new SqlParameter("@sNote", model.Note ?? "")
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 4000
            };

            ExecuteStoredProg(@"[dbo].[LS_AffiliateOrganisationUpdate]
                                @iBAId		= @iBAId OUT,
                                @sCode		= @sCode,
                                @sName		= @sName,
	                            @sFullName	= @sFullName,
	                            @iParentId	= @iParentId,
	                            @sINN		= @sINN,
	                            @sNote		= @sNote",
                                ibaId,
                                sCode,
                                sName,
                                sFullName,
                                iParentId,
                                sInn,
                                sNote);

            long baId = (long)ibaId.Value;

            if (model.MarkerValues == null) return baId;

            foreach (var markerValue in model.MarkerValues)
            {
                SaveMarkerValue(baId, markerValue);
            }

            return baId;
        }

    }
}

﻿namespace HIS.DAL.DB.Handlers.LegalSubjects
{

    internal static class PersonFullFtsHelper
    {
        private const string CSql = @"
DECLARE @D DATE = GETDATE()
DECLARE @BAId BIGINT = {{0}} 

DECLARE @FullName NVARCHAR(255) = ''

SELECT @FullName = @FullName+' '+ISNULL(T.[Name], '') 
FROM ( 
    SELECT Npp = 2, ba.BAId, Name = ISNULL(FirstName, '')  
    FROM BABaseAncestors AS ba
    INNER JOIN LSLegalSubjects AS ls ON ls.BAId = ba.BAId
    LEFT JOIN  [dbo].[BABaseAncestorsFullTextSearch] fts ON fts.BAId = ba.BAId
    WHERE  ba.TypeId IN (11,12,13,14)
      AND  ba.BAId = @BAId  
    UNION
    SELECT Npp = 3, ba.BAId, Name = ISNULL(MiddleName, '')  
    FROM BABaseAncestors AS ba
    INNER JOIN LSLegalSubjects AS ls ON ls.BAId = ba.BAId
    LEFT JOIN  [dbo].[BABaseAncestorsFullTextSearch] fts ON fts.BAId = ba.BAId
    WHERE  ba.TypeId IN (11,12,13,14) 
      AND  ba.BAId = @BAId
    UNION
    SELECT Npp = 2, ba.BAId, Name = ISNULL(FullName, '')  
    FROM BABaseAncestors AS ba
    INNER JOIN LSLegalSubjects AS ls ON ls.BAId = ba.BAId
    LEFT JOIN  [dbo].[BABaseAncestorsFullTextSearch] fts ON fts.BAId = ba.BAId
    WHERE  ba.TypeId IN (11,12,13,14)
      AND  ba.BAId = @BAId  
    UNION
    SELECT Npp = 1, lspvp.BAId, Name = ISNULL(lspvp.[sValue], '') 
    FROM LSLegalSubjectParamValuePeriods AS lspvp
    WHERE @D >= CAST(lspvp.StartDate AS DATE) AND @D <= CAST(lspvp.EndDate AS DATE)
      AND lspvp.MarkerId = 10
      AND lspvp.BAId = @BAId
) T        
ORDER BY T.Npp      
 
SELECT @FullName

IF EXISTS (SELECT 1 FROM [dbo].[BABaseAncestorsFullTextSearch] WHERE BAId = @BAId)
    UPDATE [dbo].[BABaseAncestorsFullTextSearch]
       SET NAME = @FullName 
    WHERE BAId = @BAId
ELSE 
    INSERT INTO  [dbo].[BABaseAncestorsFullTextSearch](BAId, StartDate, EndDate, Name)
    VALUES (@BAId, {d '1990-01-01'}, {d '9999-12-31'}, @FullName)
    
    
SELECT * FROM [dbo].[BABaseAncestorsFullTextSearch] WHERE BAId = @BAId";

        internal static string SqlQuery(long baId)
        {
            var ssql = CSql.Replace("{{0}}", baId.ToString());
            return ssql;
        }
    }
}

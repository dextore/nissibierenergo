﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.Client.Models.BankAccounts;
using HIS.DAL.DB.Common;

namespace HIS.DAL.DB.Handlers.LegalSubjects.LSBankAccounts
{
    public class LSBankAccountHandler: DataContextHandlerBase, ILSBankAccountHandler
    {
        public IEnumerable<LSBankAccount> GetLSBankAccounts(long baId)
        {
            return QueryBankAccounts(baId, null, false).ToArray();
        }

        public IEnumerable<LSBankAccount> GetAllLSBankAccounts(long baId)
        {
            return QueryBankAccounts(baId).ToArray();
        }

        public LSBankAccount Update(LSBankAccount model)
        {
            if (!model.BankAccountId.HasValue || model.BankAccountId == 0) // insert new
            {
                // add bank account
                model.BankAccountId = UpdateBankAccount(model);
                // add marcker
                model.ItemId = InsertMarkerItem((long)model.BAId, (long)model.BankAccountId);
            }
            else
            {
                // update account
                UpdateBankAccount(model);
            }

            return GetLSBankAccount((long)model.BAId, (int)model.ItemId);
        }

        public LSBankAccount GetLSBankAccount(long baId, int itemId)
        {
            return QueryBankAccounts(baId, itemId).FirstOrDefault();
        }

        private IQueryable<LSBankAccount> QueryBankAccounts(long baId, int? itemId = null, bool all = true)
        {
            var result = DataContext.EFLSBankAccountItemValues
                .AsNoTracking().Where(m => m.BAId == baId && (itemId == null || m.ItemId == itemId));

            if (!all)
                result = result.Join(DataContext.EFSOBATypeStates, 
                        r=>new {r.RefBankAccounts.BATypeId, r.RefBankAccounts.StateId}, 
                        s =>new{s.BATypeId, s.StateId}, 
                        (r,s) => new {r, s}).Where(m => m.s.IsFirstState == true).Select(m => m.r);


            return result.Select(r => new LSBankAccount
                {
                    BAId = r.BAId,
                    BankAccountId = r.RefBankAccounts.BAId,
                    MarkerId = r.MarkerId,
                    ItemId = r.ItemId,
                    StateId = r.RefBankAccounts.StateId,
                    StateName = r.RefBankAccounts.BABaseAncestors.SOStates.Name,
                    Number = r.RefBankAccounts.Number,
                    Note = r.RefBankAccounts.Note,
                    StartDate = r.RefBankAccounts.StartDate,
                    EndDate = r.RefBankAccounts.EndDate,
                    IsDefault = r.RefBankAccounts.IsDefault,
                    BankId = r.RefBankAccounts.BankId,
                    BankName = r.RefBankAccounts.Banks.Name
                });
        }

        private long UpdateBankAccount(LSBankAccount model)
        {
            var iBAId = new SqlParameter("@iBAId", model.BankAccountId.HasValue ? model.BankAccountId.Value : (object)DBNull.Value);
            iBAId.Direction = System.Data.ParameterDirection.InputOutput;
            iBAId.SqlDbType = System.Data.SqlDbType.BigInt;
            var sNumber = new SqlParameter("@sNumber", model.Number);
            var iBankId = new SqlParameter("@iBankId", model.BankId);
            var sNote = new SqlParameter("@sNote",
                string.IsNullOrWhiteSpace(model.Note) ? (object) DBNull.Value : model.Note)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar,
                Size = 4000
            };
            var bIsDefault = new SqlParameter("@bIsDefault", model.IsDefault.HasValue ? (bool)model.IsDefault ? 1 : 0 : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.Bit
            };
            var dStartDate = new SqlParameter("@dStartDate", model.StartDate);
            var dEndDate = new SqlParameter("@dEndDate", model.EndDate.HasValue ? model.EndDate : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.DateTime
            };

            ExecuteStoredProg(@"[dbo].[RF_BankAccountUpdate]
                                    @iBAId			= @iBAId OUT,		
                                    @sNumber		= @sNumber,	
	                                @iBankId		= @iBankId,	
	                                @sNote			= @sNote,		
	                                @bIsDefault		= @bIsDefault,	
	                                @dStartDate		= @dStartDate,	
	                                @dEndDate		= @dEndDate",
                                    iBAId,
                                    sNumber,
                                    iBankId,
                                    sNote,
                                    bIsDefault,
                                    dStartDate,
                                    dEndDate);

            return (long)iBAId.Value;
        }

        public bool DeleteBankAccount(long baId, int itemId)
        {
            var iBAId = new SqlParameter("@iBAId", baId);
            var iMarkerId = new SqlParameter("@iMarkerId", MAMarkerIdentifiers.BankAccount); // MarkerCodes.LSBankAccountMarkerId
            var iItemId = new SqlParameter("@iItemId", itemId);

            ExecuteStoredProg(@"[dbo].[LS_BankAccountItemDelete]
                                @iBAId		= @iBAId,
                            	@iMarkerId	= @iMarkerId,
                            	@iItemId	= @iItemId",
                iBAId,
                iMarkerId,
                iItemId);

            return true;
        }

        private int InsertMarkerItem(long baId, long bankAccountId)
        {
            var iBAId = new SqlParameter("@iBAId", baId);
            var iMarkerId = new SqlParameter("@iMarkerId", MAMarkerIdentifiers.BankAccount); // MarkerCodes.LSBankAccountMarkerId
            var iItemId = new SqlParameter("@iItemId", (object) DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.Int
            };
            var iValue = new SqlParameter("@iValue", bankAccountId);

            ExecuteStoredProg(@"[dbo].[LS_BankAccountItemUpdate]
                                    @iBAId		= @iBAId,		
	                                @iMarkerId	= @iMarkerId,	
	                                @iItemId	= @iItemId OUT,		
	                                @iValue		= @iValue",		
                                    iBAId,
                                    iMarkerId,
                                    iItemId,
                                    iValue);

            return (int)iItemId.Value;
        }

    }
}

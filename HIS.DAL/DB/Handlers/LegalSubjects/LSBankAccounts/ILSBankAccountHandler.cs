﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.BankAccounts;

namespace HIS.DAL.DB.Handlers.LegalSubjects.LSBankAccounts
{
    public interface ILSBankAccountHandler : IDataContextHandler
    {
        /// <summary>
        /// Возвращает счета субъекта права.
        /// </summary>
        /// <param name="baId"></param>
        /// <returns></returns>
        IEnumerable<LSBankAccount> GetAllLSBankAccounts(long baId);

        /// <summary>
        /// Возвращает счета субъекта права.
        /// </summary>
        /// <param name="baId"></param>
        /// <returns></returns>
        IEnumerable<LSBankAccount> GetLSBankAccounts(long baId);


        /// <summary>
        /// Сохраняет / добавляет расчетный счет субъекта права
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        LSBankAccount Update(LSBankAccount model);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baId"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        bool DeleteBankAccount(long baId, int itemId);

        /// <summary>
        /// Возвращает счет субъекта права
        /// </summary>
        /// <param name="baId"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        LSBankAccount GetLSBankAccount(long baId, int itemId);
    }
}

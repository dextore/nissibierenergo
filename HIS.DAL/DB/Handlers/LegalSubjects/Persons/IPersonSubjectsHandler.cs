﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.LegalSubjects;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.LegalSubjects.Persons
{
    /// <summary>
    ///  Интерфейс обработчика данных по юридическим лицам
    /// </summary>
    public interface IPersonSubjectsHandler: ILegalSubjectHandlerBase
    {
        /// <summary>
        /// Возвращает реализованные непериодические поля (маркеры) физического лица по идентификатору
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        EFLSLegalSubject GetPerson(long personId);

        /// <summary>
        /// Знамения маркеров физического лица
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        IEnumerable<EFLSLegalSubjectParamValue> GetMarkerValues(long personId);

        /// <summary>
        /// Знамения переодических маркеров физического лица
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        IEnumerable<EFLSLegalSubjectParamValuePeriod> GetPeriodMarkerValues(long personId);

        /// <summary>
        /// insert/update реализованных непериодические маркеров физического лица
        /// </summary>
        long Update(PersonImplements person);

        /// <summary>
        /// Заполняет информацию о физ лице в полнотекстовом поиске
        /// </summary>
        /// <param name="personId"></param>
        //TODO Как только будет сделан общий алгоритм - переделать!!!
        void UpdateFullTextSearch(long personId);

        /// <summary>
        /// Получить данные физ. лица по переданным маркерам 
        /// </summary>
        /// <param name="personInfo"> Представление физ лица в виде personId и коллекции markersId</param>
        /// <returns>Модель вида PersonId + Значения маркеров</returns>
        PersonInfoResponse GetPersonInformation(PersonInfo personInfo);

    }
}

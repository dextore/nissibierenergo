﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.Client.Models.LegalSubjects;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.DB.Common;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.LegalSubjects.Persons
{
    /// <summary>
    /// Обработчик данных сущности физические лица
    /// </summary>
    public class PersonSubjectsHandler : LegalSubjectHandlerBase, IPersonSubjectsHandler
    {

        public EFLSLegalSubject GetPerson(long personId)
        {
            return DataContext.EFBABaseAncestors.AsNoTracking()
                .FirstOrDefault(m => m.BAId == personId && (BATypes)m.TypeId == BATypes.Person)
                ?.LSLegalSubjects;
        }

        public IEnumerable<EFLSLegalSubjectParamValue> GetMarkerValues(long personId)
        {
            return DataContext.EFLSLegalSubjectParamValues
                .AsNoTracking()
                .Where(m => m.BAId == personId);
        }

        public IEnumerable<EFLSLegalSubjectParamValuePeriod> GetPeriodMarkerValues(long personId)
        {
            return DataContext.EFLSLegalSubjectParamValuePeriods
                .AsNoTracking()
                .Where(m => m.BAId == personId);
        }

        public long Update(PersonImplements person)
        {
            var iPersonId = new SqlParameter("@iPersonId", person.PersonId.HasValue ? person.PersonId.Value : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.BigInt
            };
            var sFirstName = new SqlParameter("@sFirstName", person.FirstName);
            var sMiddleName = new SqlParameter("@sMiddleName", person.MiddleName);

            var dBirthDate = new SqlParameter("@dBirthDate", person.BirthDate ?? (object)DBNull.Value);
            var iGender = new SqlParameter("@iGender", person.Gender ?? (object)DBNull.Value);
            var sINN = new SqlParameter("@sINN", string.IsNullOrWhiteSpace(person.INN) ? (object)DBNull.Value : person.INN);

            ExecuteStoredProg(@"[dbo].[LS_PersonUpdate]
                                @iBAId = @iPersonId OUT,
	                            @sFirstName = @sFirstName,
	                            @sMiddleName = @sMiddleName,
	                            @dBirthDate	= @dBirthDate,
	                            @iGender = @iGender,
	                            @sINN = @sINN",
                iPersonId,
                sFirstName,
                sMiddleName,
                dBirthDate,
                iGender,
                sINN
            );

            return (long)iPersonId.Value;
        }

        public void UpdateFullTextSearch(long personId)
        {
            var ssql = PersonFullFtsHelper.SqlQuery(personId);
            DataContext.Database.ExecuteSqlCommand(ssql);
        }

        public PersonInfoResponse GetPersonInformation(PersonInfo personInfo)
        {
            var markers = new List<MarkerValue>();
            foreach (var markerId in personInfo.MarkersId)
            {
                var marker = DataContext.EFLSLegalSubjectParamValuePeriods
                    .Where(x => x.BAId == personInfo.PersonId
                                && x.MarkerId == markerId)
                    .Select(x => new MarkerValue
                    {
                        Note = x.Note,
                        StartDate = x.StartDate,
                        Value = x.sValue,
                        MarkerId = x.MarkerId
                    })
                    .OrderByDescending(x => x.StartDate)
                    .FirstOrDefault();

                

                if (marker == default(MarkerValue))
                {
                    marker = DataContext.EFLSLegalSubjectParamValues
                        .Where(x => x.BAId == personInfo.PersonId 
                                    && x.MarkerId == markerId)
                        .Select(x => new MarkerValue
                        {
                            Note = x.Note,
                            Value = x.sValue,
                            MarkerId = x.MarkerId
                        })
                        .FirstOrDefault();
                    if (marker == default(MarkerValue)) continue;
                }
                marker = GetPersonInformationDisplayValue(marker);
                markers.Add(marker);
            }
            
            return new PersonInfoResponse
            {
                Markers = markers,
                PersonId = personInfo.PersonId
            };
        }

        public IEnumerable<ListItem> GetPassportInformation()
        {
            return DataContext.EFMAMarkerValueList.Where(listValues => listValues.MarkerId == 90)
                .Select(passoprtValueList => new ListItem
                {
                    ItemId = passoprtValueList.ItemId,
                    Value = passoprtValueList.ItemName
                });
        }

        private MarkerValue GetPersonInformationDisplayValue(MarkerValue markerValue)
        {
            
            switch (markerValue.MarkerId)
            {
                case 5:
                case 15:
                case 16:
                case 121:
                case 122:
                {
                    var refId = Int32.Parse(markerValue.Value);
                    var displayValue = DataContext.AOAddresses
                                       .FirstOrDefault(address => address.BAId == refId)?.Name;
                    markerValue.DisplayValue = displayValue;
                    break;
                }
                default:
                {
                    markerValue.DisplayValue = markerValue.Value;
                    break;
                }
            }

            return markerValue;
        }
        
    }
}

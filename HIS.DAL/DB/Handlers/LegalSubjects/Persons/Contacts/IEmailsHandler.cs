﻿using System.Linq;
using HIS.DAL.Client.Models.LegalSubjects.Persons.Contacts;

namespace HIS.DAL.DB.Handlers.LegalPersons.Contacts
{
    public interface IEmailsHandler: IDataContextHandler
    {
        IQueryable<ContactEmailInfo> GetContactEmails();
        long Update(ContactEmail email);
    }
}

﻿using System.Linq;
using HIS.DAL.Client.Models.LegalSubjects.Persons.Contacts;

namespace HIS.DAL.DB.Handlers.LegalPersons.Contacts
{
    public interface IPhonesHandler: IDataContextHandler
    {
        IQueryable<ContactPhoneInfo> GetContactPhones();
        long Update(ContactPhone phone);
    }
}

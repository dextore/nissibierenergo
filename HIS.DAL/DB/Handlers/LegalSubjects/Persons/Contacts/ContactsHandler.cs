﻿using HIS.DAL.DB.Common;
using System;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.Client.Models.LegalSubjects.Persons.Contacts;

namespace HIS.DAL.DB.Handlers.LegalPersons.Contacts
{
    public class ContactsHandler : DataContextHandlerBase, IContactsHandler
    {
        public IQueryable<ContactInfo> GetContacts(long legalPersonId, IQueryable<ContactPhoneInfo> phones, IQueryable<ContactEmailInfo> emails)
        {
            var contacts = DataContext.EFCRContacts
                          .GroupJoin(DataContext.EFMABaseAncestorMarkerValuePeriods.Where(v => v.BAId == legalPersonId && v.MarkerId == MAMarkerIdentifiers.ContactList),
                                     r => r.BAId.ToString(),
                                     c => c.sValue,
                                     (r, c) => new { Contact = r, MarkerValues = c })
                          .SelectMany(r => r.MarkerValues.Select(c => new { Contact = r.Contact, MarkerValue = c }))
                          .GroupJoin(DataContext.EFMAMarkerValueList.Where(m => m.MarkerId == MAMarkerIdentifiers.ContactList),
                                     r => r.Contact.ContactTypeId,
                                     vl => vl.ItemId,
                                     (r, vl) => new { MarkerValue = r.MarkerValue, Contact = r.Contact, ContactTypes = vl })
                          .SelectMany(r => r.ContactTypes.Select(c => new { MarkerValue = r.MarkerValue, Contact = r.Contact, ContactType = c }))
                          .GroupJoin(phones, r => r.Contact.BAId, p => p.ContactId, (r, p) => new { MarkerValue = r.MarkerValue, Contact = r.Contact, ContactType = r.ContactType, Phones = p.DefaultIfEmpty() })
                          .GroupJoin(emails, r => r.Contact.BAId, e => e.ContactId, (r, e) => new { MarkerValue = r.MarkerValue, Contact = r.Contact, ContactType = r.ContactType, Phones = r.Phones, Emails = e.DefaultIfEmpty() })
                          .AsNoTracking()
                          .Select(r => new ContactInfo
                          {
                              ContactId = r.Contact.BAId,
                              StartDate = r.MarkerValue.StartDate,
                              EndDate = r.MarkerValue.EndDate,
                              ContactTypeId = r.Contact.ContactTypeId,
                              ContactTypeName = r.ContactType.ItemName,
                              Note = r.Contact.Note,
                              Phones = r.Phones,
                              Emails = r.Emails
                          });

            return contacts;
        }

        public long UpdateContact(Contact contact)
        {
            var iContactId = new SqlParameter("@iContactId", contact.ContactId.HasValue ? contact.ContactId.Value : (object)DBNull.Value);
            iContactId.Direction = System.Data.ParameterDirection.InputOutput;
            iContactId.SqlDbType = System.Data.SqlDbType.BigInt;
            var iContactTypeId = new SqlParameter("@iContactTypeId", contact.ContactTypeId);
            var sNote = new SqlParameter("@sNote", contact.Note);

            ExecuteStoredProg(@"[dbo].[CR_ContactUpdate]
                    @iContactId = @iContactId OUT,
                    @iContactTypeId = @iContactTypeId,
                    @sNote = @sNote",
                    iContactId,
                    @iContactTypeId,
                    @sNote);

            return (long)iContactId.Value;
        }
    }
}

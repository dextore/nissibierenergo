﻿using System.Linq;
using HIS.DAL.Client.Models.LegalSubjects.Persons.Contacts;

namespace HIS.DAL.DB.Handlers.LegalPersons.Contacts
{
    public interface IContactsHandler: IDataContextHandler
    {
        IQueryable<ContactInfo> GetContacts(long legalPersonId, IQueryable<ContactPhoneInfo> phones, IQueryable<ContactEmailInfo> emails);
        long UpdateContact(Contact contact);
    }
}

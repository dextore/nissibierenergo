﻿using HIS.DAL.DB.Common;
using System;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.Client.Models.LegalSubjects.Persons.Contacts;

namespace HIS.DAL.DB.Handlers.LegalPersons.Contacts
{
    public class EmailsHandler : DataContextHandlerBase, IEmailsHandler 
    {
        public IQueryable<ContactEmailInfo> GetContactEmails()
        {
            return DataContext.EFCREmails.GroupJoin(DataContext.EFMABaseAncestorMarkerItemValues.Where(v => v.MarkerId == MAMarkerIdentifiers.EmailList),
                                       m => m.BAId.ToString(),
                                       mv => mv.sValue,
                                       (m, mv) => new { Email = m, MarkerValues = mv.DefaultIfEmpty() })
                            .SelectMany(r => r.MarkerValues.Select(mv => new { MarkerValue = mv, Email = r.Email }))
                            .GroupJoin(DataContext.EFMAMarkerValueList.Where(v => v.MarkerId == MAMarkerIdentifiers.MailConfirmType),
                                       r => r.Email.SendConfirmTypeId,
                                       sc => sc.ItemId,
                                       (r, sc) => new { MarkerValue = r.MarkerValue, Email = r.Email, SendConfirmTypes = sc.DefaultIfEmpty() })
                            .GroupJoin(DataContext.EFMABaseAncestorMarkerItemValues.Where(l => l.MarkerId == MAMarkerIdentifiers.MailType),
                                      r => r.Email.BAId,
                                      v => v.BAId,
                                      (r, v) => new { MarkerValue = r.MarkerValue, Email = r.Email, SendConfirmTypes = r.SendConfirmTypes, MailingAssignments = v.DefaultIfEmpty() })
                            .Select(i => new ContactEmailInfo
                            {
                                ContactId = i.MarkerValue.BAId,
                                EmailId = i.Email.BAId,
                                Email = i.Email.Email,
                                FirstName = i.Email.FirstName,
                                MiddleName = i.Email.MiddleName,
                                LastName = i.Email.LastName,
                                MailConfirmTypeId = i.Email.SendConfirmTypeId,
                                MailConfirmTypeName = i.SendConfirmTypes.FirstOrDefault() == null ? null : i.SendConfirmTypes.FirstOrDefault().ItemName,
                                MailTypes = i.MailingAssignments.GroupJoin(DataContext.EFMAMarkerValueList.Where(v => v.MarkerId == 26), r => r.sValue, vl => vl.ItemId.ToString(), (r, vl) => new { r, vl })
                                  .SelectMany(r => r.vl.Select(v => new MailTypeInfo
                                  {
                                      MarkerItemId = r.r.ItemId,
                                      Value = r.r.sValue,
                                      ItemName = v.ItemName
                                  }))
                            });
        }

        public long Update(ContactEmail email)
        {
            var iEmailId = new SqlParameter("@iEmailId", email.EmailId.HasValue ? email.EmailId.Value : (object)DBNull.Value );
            iEmailId.Direction = System.Data.ParameterDirection.InputOutput;
            iEmailId.SqlDbType = System.Data.SqlDbType.BigInt;
            var sEmail = new SqlParameter("@sEmail", email.Email);
            var sFirstName = new SqlParameter("@sFirstName", email.FirstName);
            var sMiddleName = new SqlParameter("@sMiddleName", email.MiddleName);
            var sLastName = new SqlParameter("@sLastName", email.LastName);
            var iSendConfirmTypeId = new SqlParameter("@iSendConfirmTypeId", email.MailConfirmTypeId.HasValue ? email.MailConfirmTypeId.Value : (object)DBNull.Value);
            iSendConfirmTypeId.SqlDbType = System.Data.SqlDbType.Int;

            ExecuteStoredProg(@"[dbo].[CR_EmailUpdate]
                    @iEmailId = @iEmailId OUT,
                    @sEmail = @sEmail,  
                    @sFirstName = @sFirstName,
                    @sMiddleName = @sMiddleName,
                    @sLastName = @sLastName,
                    @iSendConfirmTypeId = @iSendConfirmTypeId",
                    iEmailId,
                    sEmail,
                    sFirstName,
                    sMiddleName,
                    sLastName,
                    iSendConfirmTypeId);

            return (long)iEmailId.Value;
        }
    }
}

﻿using System.Linq;
using HIS.DAL.DB.Common;
using System.Data.SqlClient;
using System;
using System.Data;
using HIS.DAL.Client.Models.LegalSubjects.Persons.Contacts;

namespace HIS.DAL.DB.Handlers.LegalPersons.Contacts
{
    public class PhonesHandler : DataContextHandlerBase, IPhonesHandler
    {
        public IQueryable<ContactPhoneInfo> GetContactPhones()
        {
            var phones = DataContext.EFCRPhones.GroupJoin(DataContext.EFMABaseAncestorMarkerItemValues.Where(v => v.MarkerId == MAMarkerIdentifiers.PhoneList),
                                p => p.BAId.ToString(),
                                v => v.sValue,
                                (p, v) => new { MarkerValues = v, Phone = p })
                       .SelectMany(r => r.MarkerValues.Select(mv => new { Phone = r.Phone, MarkerValue = mv }))
                       .GroupJoin(DataContext.EFMAMarkerValueList.Where(v => v.MarkerId == MAMarkerIdentifiers.PhoneType),
                                  r => r.Phone.PhoneTypeId,
                                  v => v.ItemId,
                                  (r, v) => new { MarkerValue = r.MarkerValue, Phone = r.Phone, PhoneTypes = v })
                       .SelectMany(r => r.PhoneTypes.Select(pt => new { MarkerValue = r.MarkerValue, Phone = r.Phone, PhoneType = pt }))
                       .GroupJoin(DataContext.EFMAMarkerValueList.Where(v => v.MarkerId == MAMarkerIdentifiers.MailConfirmType),
                                  r => r.Phone.SendConfirmTypeId,
                                  v => v.ItemId,
                                  (r, v) => new { MarkerValue = r.MarkerValue, Phone = r.Phone, PhoneType = r.PhoneType, SendConfirmType = v.DefaultIfEmpty() })
                       .GroupJoin(DataContext.EFMABaseAncestorMarkerItemValues.Where(l => l.MarkerId == MAMarkerIdentifiers.MailType),
                                  r => r.Phone.BAId,
                                  v => v.BAId,
                                 (r, v) => new { MarkerValue = r.MarkerValue, Phone = r.Phone, PhoneType = r.PhoneType, SendConfirmType = r.SendConfirmType, MailingAssignments = v.DefaultIfEmpty() })
                       .Select(i => new ContactPhoneInfo
                       {
                           ContactId = i.MarkerValue.BAId,
                           PhoneId = i.Phone.BAId,
                           Number = i.Phone.Number,
                           PhoneTypeId = i.Phone.PhoneTypeId,
                           PhoneTypeName = i.PhoneType.ItemName,
                           FirstName = i.Phone.FirstName,
                           MiddleName = i.Phone.MiddleName,
                           LastName = i.Phone.LastName,
                           MailConfirmTypeId = i.Phone.SendConfirmTypeId,
                           MailConfirmTypeName = i.SendConfirmType.FirstOrDefault() == null ? null : i.SendConfirmType.FirstOrDefault().ItemName,
                           MailTypes = i.MailingAssignments.GroupJoin(DataContext.EFMAMarkerValueList.Where(v => v.MarkerId == 26), r => r.sValue, vl => vl.ItemId.ToString(), (r, vl) => new { r, vl })
                                  .SelectMany(r => r.vl.Select(v => new MailTypeInfo
                                  {
                                      MarkerItemId = r.r.ItemId,
                                      Value = r.r.sValue,
                                      ItemName = v.ItemName
                                  }))
                       });

            return phones;
        }

        public long Update(ContactPhone phone)
        {
            var iPhoneId = new SqlParameter("@iPhoneId", phone.PhoneId.HasValue ? phone.PhoneId.Value : (object)DBNull.Value);
            iPhoneId.Direction = ParameterDirection.InputOutput;
            iPhoneId.SqlDbType = SqlDbType.BigInt;
            var sNumber = new SqlParameter("@sNumber", phone.Number);
            var iPhoneTypeId = new SqlParameter("@iPhoneTypeId", phone.PhoneTypeId);
            var sFirstName = new SqlParameter("@sFirstName", phone.FirstName);
            var sMiddleName = new SqlParameter("@sMiddleName", phone.MiddleName);
            var sLastName = new SqlParameter("@sLastName", phone.LastName);
            var iSendConfirmTypeId = new SqlParameter("@iSendConfirmTypeId", phone.MailConfirmTypeId.HasValue ? phone.MailConfirmTypeId.Value : (object)DBNull.Value);
            iSendConfirmTypeId.SqlDbType = SqlDbType.Int;

            ExecuteStoredProg(@"[dbo].[CR_PhoneUpdate]
                    @iPhoneId = @iPhoneId OUT,
                    @sNumber = @sNumber, 
                    @iPhoneTypeId = @iPhoneTypeId,  
                    @sFirstName = @sFirstName,
                    @sMiddleName = @sMiddleName,
                    @sLastName = @sLastName,
                    @iSendConfirmTypeId = @iSendConfirmTypeId",
                    iPhoneId,
                    sNumber,
                    iPhoneTypeId,
                    sFirstName,
                    sMiddleName,
                    sLastName,
                    iSendConfirmTypeId);

            return (long)iPhoneId.Value;
        }
    }
}

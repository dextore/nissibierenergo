﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.Client.Models.LegalSubjects.Persons;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.LegalSubjects.Persons
{
    /// <summary>
    /// Обработчик данных по юридическим лицам
    /// </summary>
    public class PersonsHandler: DataContextHandlerBase, IPersonsHandler
    {
        public EFLSLegalSubject GetLegalPerson(long baId)
        {
            return DataContext.EFLSLegalSubjects.AsNoTracking().FirstOrDefault(o => o.BAId == baId);
        }

        public EFLSLegalSubject GetParentLegalPerson(long affiliateId)
        {
            return DataContext.EFLSLegalSubjects.AsNoTracking()
                .FirstOrDefault(o => 
                    o.BAId == DataContext.EFLSLegalSubjects.FirstOrDefault(m => m.BAId == affiliateId).ParentId);
        }

        /// <summary>
        /// Получение всех юрилических лиц
        /// </summary>
        /// <returns></returns>
        public IQueryable<EFLSLegalSubject> GetLegalPersons()
        {
            return DataContext.EFLSLegalSubjects.AsNoTracking();
        }

        public long Update(LegalPerson legalPerson)
        {
            var ibaId = new SqlParameter("@iBAId", legalPerson.LegalPersonId ?? (object) DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.BigInt
            };

            var sCode = new SqlParameter("@sCode", legalPerson.Code ?? (object) DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };

            var sName = new SqlParameter("@sName", legalPerson.Name ?? "")
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };

            var sFullName = new SqlParameter("@sFullName", legalPerson.FullName ?? "")
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };

            var sInn = new SqlParameter("@sINN", legalPerson.INN)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 12
            };

            var sNote = new SqlParameter("@sNote", legalPerson.Note ?? "")
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 4000
            };

            ExecuteStoredProg(@"[dbo].[LS_OrganisationUpdate]
                    @iBAId = @iBAId OUT,
                    @sCode = @sCode OUT,
                    @sName = @sName,
	                @sFullName = @sFullName,
                    @sINN = @sINN,
                    @sNote = @sNote",
                    ibaId,
                    sCode,
                    sName,
                    sFullName,
                    sInn,
                    sNote
            );

            return (long)ibaId.Value;
        }


        public bool CheckThatInnOnlyOne(string inn, long sourceBaid)
        {
            return !DataContext.EFLSLegalSubjects.Any(person => person.BATypeId == 11
                                                                && person.BAId != sourceBaid
                                                                && person.INN == inn);
        }
    }
}

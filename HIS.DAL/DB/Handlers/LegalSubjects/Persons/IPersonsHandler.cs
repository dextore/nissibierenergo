﻿using System.Linq;
using HIS.DAL.Client.Models.LegalSubjects.Persons;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.LegalSubjects.Persons
{
    /// <summary>
    /// Обработчик данных по юрилическим лицам
    /// </summary>
    public interface IPersonsHandler: IDataContextHandler
    {
        /// <summary>
        /// Получить все юридические лица
        /// </summary>
        /// <returns></returns>
        IQueryable<EFLSLegalSubject> GetLegalPersons();

        /// <summary>
        /// Получает юридическое лицо по идентификатору филиала
        /// </summary>
        /// <param name="affiliateId"></param>
        /// <returns></returns>
        EFLSLegalSubject GetParentLegalPerson(long affiliateId);

        /// <summary>
        /// Получить юридическое лицо
        /// </summary>
        /// <returns></returns>
        EFLSLegalSubject GetLegalPerson(long BAId);

        /// <summary>
        /// Сохраняет юридическое лицо
        /// </summary>
        /// <param name="legalPerson"></param>
        /// <returns></returns>
        long Update(LegalPerson legalPerson);

        /// <summary>
        /// Проверяет есть ли юридические лица с таким ИНН
        /// </summary>
        /// <param name="inn">Инн</param>
        /// <returns>false - если есть</returns>
        bool CheckThatInnOnlyOne(string inn, long sourceBaid);
    }
}

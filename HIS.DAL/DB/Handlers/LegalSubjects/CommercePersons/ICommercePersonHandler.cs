﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.LegalSubjects.CommercePerson;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.DB.Handlers.LegalSubjects.CommercePersons
{
    public interface ICommercePersonHandler : ILegalSubjectHandlerBase
    {
        CommercePerson GetCommercePerson(long personId);
        CommercePerson GetLegalSubjectByParent(long parentId);
        IEnumerable<MarkerValue> GetAddresseList(long baId);
        long Update(CommercePersonUpdate person);
        void SaveAddresses(long baId, IEnumerable<MarkerValue> addresses);
    }
}

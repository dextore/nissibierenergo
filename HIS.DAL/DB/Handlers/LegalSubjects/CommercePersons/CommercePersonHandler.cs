﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.Client.Models.LegalSubjects.CommercePerson;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.DB.Common;

namespace HIS.DAL.DB.Handlers.LegalSubjects.CommercePersons
{
    public class CommercePersonHandler : LegalSubjectHandlerBase, ICommercePersonHandler
    {
        public CommercePerson GetCommercePerson(long personId)
        {
            return DataContext.EFLSLegalSubjects.Where(ls => ls.BAId == personId).Select(r => new CommercePerson
            {
                BAId = r.BAId,
                ParentId = r.ParentId
            }).FirstOrDefault();
        }

        public CommercePerson GetLegalSubjectByParent(long parentId)
        {
            var result = DataContext.EFLSLegalSubjects.Where(ls => ls.ParentId == parentId).AsNoTracking()
                .Join(DataContext.EFBABaseAncestors.AsNoTracking(), ls => ls.BAId, ba => ba.BAId, (ls, ba) => new {ls, ba})
                .Where(r => DataContext.EFSOBATypeStates.Any(s => s.StateId == r.ba.StateId && s.BATypeId == r.ba.TypeId && s.IsDelState == false))
                .Select(r =>
                    new
                    {
                        r.ba.BAId
                    }
                );

            var subject = result.FirstOrDefault();

            if (subject == null)
                return null;

            return new CommercePerson
            {
                BAId = subject.BAId,
                ParentId = parentId,
                MarkerValues = new List<MarkerValue>()
            };
        }

        public IEnumerable<MarkerValue> GetAddresseList(long baId)
        {

            return DataContext.EFLSAddressItemValues.AsNoTracking()
                .Where(m => m.BAId == baId && m.MarkerId == MAMarkerIdentifiers.PostAddress)
                .Join(DataContext.AOAddresses.AsNoTracking(), baiv => baiv.Value, a => a.BAId, (baiv, a) => new { baiv, a })
                .Select(r => new MarkerValue
                {
                    MarkerId = r.baiv.MarkerId,
                    ItemId = r.baiv.ItemId,
                    Value = r.baiv.sValue,
                    StartDate = r.baiv.StartDate,
                    EndDate = r.baiv.EndDate,
                    Note = r.baiv.Note,
                    DisplayValue = r.a.Name,
                    IsDeleted = false
                }).ToArray();

            //return DataContext.EFLSBankAccountItemValues.AsNoTracking()
            //    .Where(m => m.BAId == baId && m.MarkerId == MAMarkerIdentifiers.PostAddress)
            //    .Join(DataContext.AOAddresses.AsNoTracking(), baiv => baiv.Value, a => a.BAId, (baiv, a) => new {baiv, a})
            //    .Select(r => new MarkerValue
            //    {
            //        MarkerId = r.baiv.MarkerId,
            //        ItemId = r.baiv.ItemId,
            //        Value = r.baiv.sValue,
            //        StartDate = r.baiv.StartDate, 
            //        EndDate = r.baiv.EndDate,
            //        Note = r.baiv.Note,
            //        DisplayValue = r.a.Name,
            //        IsDeleted = false
            //    }).ToArray();
        }

        
        public long Update(CommercePersonUpdate commercePerson)
        {
            // проверка , что бы не писалась лишний раз история изменения ИП
            if (commercePerson.BAId.HasValue && commercePerson.ParentId.HasValue)
            {
                return (long)commercePerson.BAId;
            }

            var iPersonId = new SqlParameter("@iPersonId", commercePerson.BAId.HasValue ? commercePerson.BAId.Value : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.BigInt
            };
            var iParentId = new SqlParameter("@iParentId", commercePerson.ParentId ?? (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.BigInt
            };

            ExecuteStoredProg(@"[dbo].[LS_CommercePersonUpdate]
                                    @iBAId = @iPersonId OUT,
                                    @iParentId = @iParentId", 
                iPersonId,
                iParentId);

            var personId = (long)iPersonId.Value;

            /*
            отключил , так как обновление маркеров происходит в CommercePersonService!
            if (commercePerson.MarkerValues == null) return personId;
            foreach (var markerValue in commercePerson.MarkerValues)
            {
                SaveMarkerValue(personId, markerValue);
            }
            */

            return personId;
        }

        public void SaveAddresses(long baId, IEnumerable<MarkerValue> addresses)
        {
            foreach (var address in addresses)
            {
                if (address.IsDeleted)
                {
                    DeleteAddress(baId, address);
                    continue;
                }

                UpdateAddress(baId, address);
            }
        }

        private void UpdateAddress(long baId, MarkerValue marker)
        {
            var iBAId = new SqlParameter("@iBAId", baId);
            var iMarkerId = new SqlParameter("@iMarkerId", MAMarkerIdentifiers.PostAddress);
            var iItemId = new SqlParameter("@iItemId", marker.ItemId.HasValue ? marker.ItemId : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            var dStartDate = new SqlParameter("@dStartDate", marker.StartDate.HasValue ? marker.StartDate : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.DateTime
            };
            var dEndDate = new SqlParameter("dEndDate", marker.EndDate.HasValue ? marker.EndDate : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.DateTime
            };
            var vValue = new SqlParameter("@vValue", marker.Value);
            var sNote = new SqlParameter("@sNote", string.IsNullOrEmpty(marker.Note) ? (object) DBNull.Value : marker.Note)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar,
                Size = 4000
            };

            ExecuteStoredProg(@"[dbo].[LS_LegalSubjectParamItemUpdate]
                                @iBAId		= @iBAId,
	                            @iMarkerId	= @iMarkerId,
	                            @iItemId	= @iItemId,	
	                            @dStartDate	= @dStartDate OUT,
	                            @dEndDate	= @dEndDate,
	                            @vValue		= @vValue,
	                            @sNote		= @sNote",
                                iBAId,
                                iMarkerId,
                                iItemId,
                                dStartDate,
                                dEndDate,
                                vValue,
                                sNote);
        }

        private void DeleteAddress(long baId, MarkerValue marker)
        {
            var iBAId = new SqlParameter("@iBAId", baId);
            var iMarkerId = new SqlParameter("@iMarkerId", MAMarkerIdentifiers.PostAddress);
            var iItemId = new SqlParameter("@iItemId", marker.ItemId);

            ExecuteStoredProg(@"[dbo].[LS_LegalSubjectParamItemDelete]
                                @iBAId		= @iBAId,
	                            @iMarkerId	= @iMarkerId,
	                            @iItemId	= @iItemId",
                                iBAId,
                                iMarkerId,
                                iItemId);

        }

    }
}

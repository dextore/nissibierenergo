﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HIS.DAL.Client.Models.SystemTree;

namespace HIS.DAL.DB.Handlers.LegalSubjects
{
    public class LegalSubjectHandler: LegalSubjectHandlerBase, ILegalSubjectHandler
    {
        public SystemTreeItem GetTreeItem(long baId)
        {
            var ba =  DataContext.EFBABaseAncestors.FirstOrDefault(m => m.BAId == baId);

            if (ba == null)
                return null;

            return DataContext.EFLSLegalSubjects.Where(m => m.BAId == baId)
                .Select(m => new SystemTreeItem { 
                    BAId = m.BAId,
                    Name = m.FullName,
                    BATypeId = m.BATypeId,
                    MVCAlias = ba.BATypes.MVCAlias,
                    BATypeName = ba.BATypes.FullName,
                    BATypeNameShort = ba.BATypes.ShortName,
                    StateId = m.StateId,
                    StateName = ba.SOStates.Name
                }).FirstOrDefault();
        }

        public SystemTreeItem GetParentTreeItem(long baId)
        {
            var ba = DataContext.EFLSLegalSubjects.FirstOrDefault(n => n.BAId == baId);
            if (ba != null && ba.ParentId == null)
                return null;

            var parentBa = DataContext.EFBABaseAncestors.FirstOrDefault(m => m.BAId == ba.ParentId);

            if (parentBa == null)
                return null;

            return DataContext.EFLSLegalSubjects.Where(m => m.BAId == ba.ParentId)
                .Select(m => new SystemTreeItem
                {
                    BAId = m.BAId,
                    Name = m.FullName,
                    BATypeId = m.BATypeId,
                    MVCAlias = parentBa.BATypes.MVCAlias,
                    BATypeName = parentBa.BATypes.FullName,
                    BATypeNameShort = parentBa.BATypes.ShortName,
                    StateId = m.StateId,
                    StateName = parentBa.SOStates.Name
                }).FirstOrDefault();
        }

        public IEnumerable<SystemTreeItem> GetChildrenTreeItems(long baId)
        {
            return DataContext.EFLSLegalSubjects
                .Join(DataContext.EFBATypes, ls=>ls.BATypeId, t=>t.TypeId, (ls, t)=>new {ls, t})
                .Join(DataContext.EFSOStates, r => r.ls.StateId, s => s.StateId, (r, s) => new {r.ls, r.t, s})
                .Where(m => m.ls.ParentId == baId)
                .Select(m => new SystemTreeItem
                {
                    BAId = m.ls.BAId,
                    Name = m.ls.FullName,
                    BATypeId = m.ls.BATypeId,
                    MVCAlias = m.t.MVCAlias,
                    BATypeName = m.t.FullName,
                    BATypeNameShort = m.t.ShortName,
                    StateId = m.ls.StateId,
                    StateName = m.s.Name
                }).ToArray();
        }
    }
}

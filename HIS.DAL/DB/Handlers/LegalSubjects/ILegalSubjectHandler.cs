﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.SystemTree;

namespace HIS.DAL.DB.Handlers.LegalSubjects
{
    public interface ILegalSubjectHandler : ILegalSubjectHandlerBase
    {
        SystemTreeItem GetTreeItem(long baId);
        SystemTreeItem GetParentTreeItem(long baId);
        IEnumerable<SystemTreeItem> GetChildrenTreeItems(long baId);
    }
}
﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.LegalSubjects
{
    public interface ILegalSubjectHandlerBase: IDataContextHandler
    {
        /// <summary>
        /// Возвращает сушности субъеты права
        /// </summary>
        /// <returns></returns>
        IEnumerable<EFLSLegalSubject> GetLegalSubjects();

        /// <summary>
        /// Сохраняет изменение значения маркера
        /// </summary>
        /// <param name="markerValue"></param>
        void SaveMarkerValue(long legalSubjectId, MarkerValue markerValue);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.LegalSubjects
{
    public abstract class LegalSubjectHandlerBase : DataContextHandlerBase, ILegalSubjectHandlerBase
    {

        public IEnumerable<EFLSLegalSubject> GetLegalSubjects()
        {
            return DataContext.EFLSLegalSubjects;
        }

        public void SaveMarkerValue(long legalSubjectId, MarkerValue markerValue)
        {
            if (markerValue.IsDeleted)
            {
                DeleteMarkerValue(legalSubjectId, markerValue);
                return;
            }
            UpdateMarkerValue(legalSubjectId, markerValue);
        }

        private void UpdateMarkerValue(long legalSubjectId, MarkerValue markerValue)
        {
            DateTime tmpDateValue;
            if (DateTime.TryParse(markerValue.Value, out tmpDateValue))
                markerValue.Value = tmpDateValue.ToString("yyyy-MM-dd");
            var iBAId = new SqlParameter("@iBAId", legalSubjectId);
            var iMarkerId = new SqlParameter("@iMarkerId", markerValue.MarkerId);

            var dStartDate = new SqlParameter("@dStartDate", markerValue.StartDate.HasValue ? markerValue.StartDate.Value : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.DateTime
            };
            var dEndDate = new SqlParameter("@dEndDate", (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.DateTime
            };
            var vValue = new SqlParameter("@vValue", markerValue.Value);
            var sNote = new SqlParameter("@sNote", string.IsNullOrWhiteSpace(markerValue.Note) ? (object)DBNull.Value : markerValue.Note)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar,
                Size = 4000
            };

            ExecuteStoredProg(@"[dbo].[LS_LegalSubjectParamUpdate]
                                @iBAId = @iBAId,
                                @iMarkerId = @iMarkerId,	
                                @dStartDate	= @dStartDate OUT,
                                @dEndDate = @dEndDate OUT,
                                @vValue	= @vValue,
                                @sNote = @sNote",
                iBAId,
                iMarkerId,
                dStartDate,
                dEndDate,
                vValue,
                sNote
            );
        }

        private void DeleteMarkerValue(long legalSubjectId, MarkerValue markerValue)
        {
            var iBAId = new SqlParameter("@iBAId", legalSubjectId);
            var iMarkerId = new SqlParameter("@iMarkerId", markerValue.MarkerId);

            var dStartDate = new SqlParameter("@dStartDate", markerValue.StartDate.HasValue ? markerValue.StartDate.Value : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.DateTime
            };

            ExecuteStoredProg(@"[dbo].[LS_LegalSubjectParamDelete]
                                @iBAId		= @iBAId,
	                            @iMarkerId	= @iMarkerId,	
	                            @dStartDate	= @dStartDate",
                iBAId,
                iMarkerId,
                dStartDate
            );
        }
    }
}

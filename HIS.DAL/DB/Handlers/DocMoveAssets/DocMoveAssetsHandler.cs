﻿using HIS.DAL.Client.Models.DocMoveAssets;
using HIS.DAL.Client.Models.Markers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace HIS.DAL.DB.Handlers.DocMoveAssets
{
    public class DocMoveAssetsHandler : DataContextHandlerBase, IDocMoveAssetsHandler
    {
        public DocMoveAssetsData GetDocMoveAssetsData(long baId)
        {
            DocMoveAssetsData item = DataContext.FADocMovFAssets
                .Where(w => w.BAId == baId)
                .Select(x => new DocMoveAssetsData
                {
                    BAId = x.BAId,
                    BaTypeId = x.BATypeId,
                    Evidence = x.Evidence,
                    Note = x.Note,
                    DocDate = x.DocDate,
                    CAId = x.CAId,
                    CAName = x.LSCompanyAreas.Name,
                    Number = x.Number,
                    SrcLSId = x.SrcLSId,
                    SrcLSName = x.LSLegalSubjects.Name,
                    DstLSId = x.DstLSId,
                    DstLSName = x.LSLegalSubjects1.Name,
                    SrcMLPId = x.SrcMLPId,
                    SrcMLPName = x.MateriallyLiablePersons1.Name,
                    DstMLPId = x.DstMLPId,
                    DstMLPName = x.MateriallyLiablePersons.Name,
                    StateId = x.StateId,
                    SrcLocationId = x.SrcLocationId,
                    DstLocationId = x.DstLocationId,
                    ReasonId = x.ReasonId
                   
                }).FirstOrDefault();
            item.HasAssets = IsHasAssets(baId);
            return item;
        }
        public bool IsHasAssets(long baId)
        {
            return DataContext.FADocMovFAssetsRows.Any(w => w.FADocMovId == baId);
        }
        public long UpdateDocMoveAssets(DocMoveAssetsData saveData)
        {
            var iBAId = new SqlParameter("@iBAId ", saveData.BAId.HasValue ? saveData.BAId.Value : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.BigInt
            };
            var sNumber = new SqlParameter("@sNumber", saveData.Number);
            var dDocDate = new SqlParameter("@dDocDate", saveData.DocDate .HasValue ? saveData.DocDate.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.Date
            };
            var iSrcLSId = new SqlParameter("@iSrcLSId", saveData.SrcLSId.HasValue ? saveData.SrcLSId.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iDstLSId = new SqlParameter("@iDstLSId", saveData.DstLSId.HasValue ? saveData.DstLSId.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iSrcLocationId = new SqlParameter("@iSrcLocationId", saveData.SrcLocationId.HasValue ? saveData.SrcLocationId.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iDstLocationId = new SqlParameter("@iDstLocationId", saveData.DstLocationId.HasValue ? saveData.DstLocationId.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iSrcMLPId = new SqlParameter("@iSrcMLPId", saveData.SrcMLPId.HasValue ? saveData.SrcMLPId.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iDstMLPId = new SqlParameter("@iDstMLPId", saveData.DstMLPId.HasValue ? saveData.DstMLPId.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iCAId = new SqlParameter("@iCAId", saveData.CAId.HasValue ? saveData.CAId.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iReasonId = new SqlParameter("@iReasonId", saveData.ReasonId.HasValue ? saveData.ReasonId.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var sEvidence = new SqlParameter("@sEvidence", String.IsNullOrEmpty(saveData.Evidence) ? (object)DBNull.Value : saveData.Evidence);
            var sNote = new SqlParameter("@sNote", String.IsNullOrEmpty(saveData.Note) ? (object)DBNull.Value : saveData.Note);

            ExecuteStoredProg(@"EXEC FADM_DocMovFAssetsUpdate
	                            @iBAId = @iBAId OUT,
	                            @sNumber = @sNumber,
	                            @dDocDate = @dDocDate,
	                            @iSrcLSId = @iSrcLSId,
	                            @iDstLSId = @iDstLSId,
	                            @iSrcLocationId = @iSrcLocationId,
	                            @iDstLocationId = @iDstLocationId,
	                            @iSrcMLPId = @iSrcMLPId,
	                            @iDstMLPId = @iDstMLPId,
	                            @iCAId = @iCAId,
	                            @iReasonId = @iReasonId ,
	                            @sEvidence = @sEvidence ,
	                            @sNote = @sNote",
                iBAId,
                sNumber,
                dDocDate,
                iSrcLSId,
                iDstLSId,
                iSrcLocationId,
                iDstLocationId,
                iSrcMLPId,
                iDstMLPId,
                iCAId,
                iReasonId,
                sEvidence,
                sNote


            );
            return (long)iBAId.Value;
         
        }
        public void SaveMarkerValue(long baId, MarkerValue markerValue)
        {
            if (markerValue.IsDeleted)
            {
                DeleteMarkerValue(baId, markerValue);
                return;
            }
            UpdateMarkerValue(baId, markerValue);
        }
        private void UpdateMarkerValue(long baId, MarkerValue markerValue)
        {
            DateTime tmpDateValue;
            if (DateTime.TryParse(markerValue.Value, out tmpDateValue))
                markerValue.Value = tmpDateValue.ToString("yyyy-MM-dd");
            var iBAId = new SqlParameter("@iBAId", baId);
            var iMarkerId = new SqlParameter("@iMarkerId", markerValue.MarkerId);

            var dStartDate = new SqlParameter("@dStartDate", markerValue.StartDate.HasValue ? markerValue.StartDate.Value : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.DateTime
            };
            var dEndDate = new SqlParameter("@dEndDate", (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.DateTime
            };
            var vValue = new SqlParameter("@vValue", markerValue.Value);
            var sNote = new SqlParameter("@sNote", string.IsNullOrWhiteSpace(markerValue.Note) ? (object)DBNull.Value : markerValue.Note)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar,
                Size = 4000
            };

            ExecuteStoredProg(@"[dbo].[FADM_DocMovFAssetsParamUpdate]
                                @iBAId = @iBAId,
                                @iMarkerId = @iMarkerId,	
                                @dStartDate	= @dStartDate OUT,
                                @dEndDate = @dEndDate OUT,
                                @vValue	= @vValue,
                                @sNote = @sNote",
                iBAId,
                iMarkerId,
                dStartDate,
                dEndDate,
                vValue,
                sNote
            );
        }
        private void DeleteMarkerValue(long baId, MarkerValue markerValue)
        {
            var iBAId = new SqlParameter("@iBAId", baId);
            var iMarkerId = new SqlParameter("@iMarkerId", markerValue.MarkerId);

            var dStartDate = new SqlParameter("@dStartDate", markerValue.StartDate.HasValue ? markerValue.StartDate.Value : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.DateTime
            };

            ExecuteStoredProg(@"[dbo].[FADM_DocMovFADocMovFAssetsParamDelete]
                                @iBAId		= @iBAId,
	                            @iMarkerId	= @iMarkerId,	
	                            @dStartDate	= @dStartDate",
                iBAId,
                iMarkerId,
                dStartDate
            );
        }
        public DocMoveAssetsRow GetDocMoveAssetsRow(long docMoveId, long recId)
        {
            var row = DataContext.FADocMovFAssetsRows
                .Where(w => w.FADocMovId == docMoveId && w.RecId == recId)
                .Select(
                x => new DocMoveAssetsRow
                {
                    RecId = x.RecId,
                    FAId = x.FAId,
                    FAName = x.FAssets.InventoryNumber,
                    Evidence = x.Evidence,
                    BarCode = x.BarCode,
                    SerialNumber = x.SerialNumber,
                    CheckDate = x.CheckDate,
                    ReleaseDate = x.ReleaseDate,
                    FADocMovId = x.FADocMovId,
                    EvidenceId = x.EvidenceId,
                    EvidenceName = ""
                }).FirstOrDefault();
            row.EvidenceName = GetEntityNameByBaId(row.EvidenceId);
            return row;
        }
        private string GetEntityNameByBaId(long? baId)
        {
            if (!baId.HasValue || baId == 0) return "";
            const string query = @"
                DECLARE @query VARCHAR (MAX)
                SELECT 
	                @query = CONCAT('SELECT TOP 1 ', mm.ImplementTypeField, ' FROM ' , mm.ImplementTypeName , ' WHERE BAId = ', @baId)
                FROM 
	                BABaseAncestors AS ba 
                LEFT JOIN MABATypeMarkers AS mm ON mm.BATypeId = ba.TypeId
                WHERE
	                ba.BAId = @baId AND 
	                mm.MarkerId = 1                
                exec (@query)";
            return DataContext.Database.SqlQuery<string>(query, new SqlParameter("baId", baId)).FirstOrDefault();

        }
        public IEnumerable<DocMoveAssetsRow> GetDocMoveAssetsRows(long docMoveId)
        {
            var list  = DataContext.FADocMovFAssetsRows
                .Where(w => w.FADocMovId == docMoveId)
                .Select(
                    x => new DocMoveAssetsRow
                    {
                        RecId = x.RecId,
                        FAId = x.FAId,
                        FAName = x.FAssets.InventoryNumber,
                        Evidence = x.Evidence,
                        BarCode = x.BarCode,
                        SerialNumber = x.SerialNumber,
                        CheckDate = x.CheckDate,
                        ReleaseDate = x.ReleaseDate,
                        FADocMovId = x.FADocMovId,
                        EvidenceId = x.EvidenceId,
                        EvidenceName = ""

                    }).ToList();

            list.ForEach(item =>
            {
                item.EvidenceName = GetEntityNameByBaId(item.EvidenceId);
            });

            return list;
        }
        public long UpdateDocMoveAssetRow(DocMoveAssetsRow row)
        {
            var iRecId = new SqlParameter("@iRecId", row.RecId.HasValue ? row.RecId.Value : (object)DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.BigInt
            };
            var iFADocMovId = new SqlParameter("@iFADocMovId", row.FADocMovId)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iFAId = new SqlParameter("@iFAId", row.FAId)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var dReleaseDate = new SqlParameter("@dReleaseDate", row.ReleaseDate.HasValue ? row.ReleaseDate.Value : (object)DBNull.Value){
                    SqlDbType = SqlDbType.Date
            };
            var dCheckDate = new SqlParameter("@dCheckDate", row.CheckDate.HasValue ? row.CheckDate.Value : (object)DBNull.Value)
           {
               SqlDbType = SqlDbType.Date
           };
            var sSerialNumber = new SqlParameter("@sSerialNumber", String.IsNullOrEmpty(row.SerialNumber) ? (object)DBNull.Value : row.SerialNumber);
            var sBarCode = new SqlParameter("@sBarCode", String.IsNullOrEmpty(row.BarCode) ? (object)DBNull.Value : row.BarCode);
            var iEvidenceId = new SqlParameter("@iEvidenceId", (row.EvidenceId.HasValue && row.EvidenceId > 0) ? row.EvidenceId.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var sEvidence = new SqlParameter("@sEvidence", String.IsNullOrEmpty(row.Evidence) ? (object)DBNull.Value : row.Evidence);

            ExecuteStoredProg(@"[dbo].[FADM_DocMovRowsUpdate]
                                @iRecId = @iRecId OUT,
	                            @iFADocMovId = @iFADocMovId,
                                @iFAId = @iFAId,
                                @sSerialNumber = @sSerialNumber,
                                @dReleaseDate = @dReleaseDate,
                                @dCheckDate = @dCheckDate,
                                @sBarCode = @sBarCode,
                                @iEvidenceId = @iEvidenceId,
                                @sEvidence = @sEvidence",
                iRecId,
                iFADocMovId,
                iFAId,
                sSerialNumber,
                dReleaseDate,
                dCheckDate,
                sBarCode,
                iEvidenceId,
                sEvidence
            );
            return (long)iRecId.Value;

        }
        public void DeleteDocMoveAssetRow(DocMoveAssetsRow row)
        {
            var iRecId = new SqlParameter("@iRecId", row.RecId.HasValue ? row.RecId.Value : (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iFADocMovId = new SqlParameter("@iFADocMovId", row.FADocMovId)
            {
                SqlDbType = SqlDbType.BigInt
            };
            ExecuteStoredProg(@"[dbo].[FADM_DocMovFAssetsRowsDelete]
                                @iRecId = @iRecId,
	                            @iFADocMovId = @iFADocMovId",
                iRecId,
                iFADocMovId
            );
        }
        public AssetsData GetAssetsInfo(long baId)
        {
            return DataContext.FAssets
                .Where(x => x.BAId == baId).
                Select(x=>new AssetsData
                {
                    BAId = x.BAId,
                    BATypeId = x.BATypeId,
                    FADocAcceptId = x.FADocAcceptId,
                    InventoryId = x.InventoryId,
                    LSId = x.LSId,
                    InventoryNumber = x.InventoryNumber,
                    SerialNumber = x.SerialNumber,
                    BarCode = x.BarCode,
                    CheckDate = x.CheckDate,
                    ReleaseDate = x.ReleaseDate,
                    EmployeeId = x.EmployeeId,
                    StateId = x.StateId,
                    MLPId = x.MLPId
                }).FirstOrDefault();
        }

      

    }
}

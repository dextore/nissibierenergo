﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.DocMoveAssets;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.DB.Handlers.DocMoveAssets
{
    public interface IDocMoveAssetsHandler: IDataContextHandler
    {
        DocMoveAssetsData GetDocMoveAssetsData(long baId);
        IEnumerable<DocMoveAssetsRow> GetDocMoveAssetsRows(long docMoveId);
        DocMoveAssetsRow GetDocMoveAssetsRow(long docMoveId, long recId);
        long UpdateDocMoveAssets(DocMoveAssetsData saveData);
        void SaveMarkerValue(long baId, MarkerValue markerValue);
        long UpdateDocMoveAssetRow(DocMoveAssetsRow row);
        void DeleteDocMoveAssetRow(DocMoveAssetsRow row);
        bool IsHasAssets(long baId);
        AssetsData GetAssetsInfo(long baId);
    } 
}

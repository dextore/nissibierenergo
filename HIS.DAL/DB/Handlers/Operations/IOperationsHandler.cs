﻿using HIS.DAL.DB.Context;
using System.Linq;

namespace HIS.DAL.DB.Handlers.Operations
{
    public interface IOperationsHandler : IDataContextHandler
    {
        IQueryable<EFSOBATypeStateOperation> GetAllowedEntityOperations(long baId);
        IQueryable<EFSOBATypeState> GetEntityStates(long baTypeId);
    }
}

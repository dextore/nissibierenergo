﻿using HIS.DAL.DB.Context;
using System.Linq;

namespace HIS.DAL.DB.Handlers.Operations
{
    public class OperationsHandler: DataContextHandlerBase, IOperationsHandler
    {
        public IQueryable<EFSOBATypeStateOperation> GetAllowedEntityOperations(long baId)
        {
            var baseAncestor = DataContext.EFBABaseAncestors.FirstOrDefault(b => b.BAId == baId);
            return DataContext.EFSOBATypeStateOperations.Where(o => o.BATypeId == baseAncestor.TypeId && o.SrcStateId == baseAncestor.StateId);
        }

        public IQueryable<EFSOBATypeState> GetEntityStates(long baTypeId)
        {
            return DataContext.EFSOBATypeStates.Where(o => o.BATypeId == baTypeId);
        }

    }
}

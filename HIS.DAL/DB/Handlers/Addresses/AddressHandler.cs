﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.DB.Context;
using HIS.DAL.Client.Models.Address;
using HIS.Models.Layer.Models.Address;

namespace HIS.DAL.DB.Handlers.Addresses
{
    public class AddressHandler : DataContextHandlerBase, IAddressHandler
    {
        public IEnumerable<AOAddress> AddressFind(string findStr, int? sourceType)
        {
            return DataContext.AddressFind(findStr, sourceType).ToArray();
        }

        public FTSAddressList AddressByHouseId(Guid AOid, Guid houseId)
        {
            return DataContext.GetFTSAddressList(AOid, houseId, null).FirstOrDefault();
        }

        public FTSAddressList AddressBySteadId(Guid AOid, Guid steadId)
        {
            return DataContext.GetFTSAddressList(AOid, null, steadId).FirstOrDefault();
        }

        public FTSAddressList AddressByAOId(Guid AOid)
        {
            return DataContext.GetFTSAddressList(AOid, null, null).FirstOrDefault();
        }


        public IEnumerable<AOAddressObject> GetAddressObjetcList(Guid? parentId, int aoLavel, int? sourceType)
        {
            return DataContext.GetAddressObjectList(parentId, aoLavel, sourceType).ToArray();
        }

        public IEnumerable<AOHouse> GetHouseList(Guid? parentId, int? sourceType)
        {
            return DataContext.GetHouseList(parentId, sourceType).ToArray();
        }

        public IEnumerable<AOStead> GetSteadList(Guid? parentId, int? sourceType)
        {
            return DataContext.GetSteadList(parentId, sourceType).ToArray();
        }

        public AO_HouseData_Result GetHouse(Guid? houseId)
        {
            return DataContext.AO_HouseData(houseId).FirstOrDefault(m => m.HouseId == houseId);
        }

        public AO_SteadData_Result GetStead(Guid? steadId)
        {
            return DataContext.AO_SteadData(steadId).FirstOrDefault(m =>m.SteadId == steadId);
            //return DataContext.GetSteadList(AOGuid, null).FirstOrDefault(m => m.SteadId == steadId);

        }

        public IEnumerable<AOAddressResult> GetAddressList(Guid? aoId, Guid? houseId, Guid? steadId, int? actionStateId, AddressSourceType? sourceType)
        {
            return DataContext.GetAddressList(aoId, houseId, steadId, actionStateId, (int?)sourceType).ToArray();
        }

        public IEnumerable<AO_GetAdvancedSearchList_Result>GetAdvancedSearchList(Guid? aoId, Guid? houseId, Guid? steadId, int? actionStateId, AddressSourceType? sourceType,string postCode = null, string cadNum = null)
        {
            return DataContext.AO_GetAdvancedSearchList(aoId, houseId, steadId, actionStateId, (int?)sourceType, postCode, cadNum).ToArray();
            // aoId, houseId, steadId, actionStateId, (int?)sourceType , postCode, cadNum).ToArray();
        }

    

        public IEnumerable<FlatType> GetFlatTypeList(bool isVisible)
        {
            return DataContext.GetFlatTypeList(isVisible);
        }


        public SteadUpdate GetSteadUpdateInfo(Guid steadId)
        {
            var stead = DataContext.AOSteads.FirstOrDefault(m => m.SteadId == steadId);

            if (stead == null)
                return null;

            return new SteadUpdate
            {
                SteadId = stead.SteadId,
                SteadGuid = stead.SteadGuid,
                Number = stead.Number,
                ParentId = stead.ParentGuid,
                PostCode = stead.PostalCode,
                CadNum = stead.CadNum,
                Latitude = null,
                Longitude = null
            };

    }

        public HouseUpdate GetHouseUpdateInfo(Guid houseId)
        {
            var house = DataContext.AOHouses.FirstOrDefault(m => m.HouseId == houseId);
            var addrObj = DataContext.AOAddressObjects.FirstOrDefault(m => m.AOGuid == house.AOGuid && m.LiveStateId == 1);

            if (house == null)
                return null;

            bool isBuild = false; 
            if (house.IsBuild == true)
            {
                isBuild = true;
            }

            return new HouseUpdate
            {
                HouseId = house.HouseId,
                AOId = addrObj?.AOId,
                EstStateId = house.EstStateId,
                HouseGuid = house.HouseGuid,
                HouseNum = house.HouseNum,
                BuildNum = house.BuildNum,
                StructNum = house.StrucNum,
                StrStateId = house.StrStateId,
                Longitude = null,
                Latitude = null,
                PostCode = house.PostalCode,
                CadNum = house.CadNum,
                IsBuild = isBuild

            };
        }

        public AddressObject GetAddressObject(Guid aoId)
        {
            return DataContext.AOAddressObjects.Where(m => m.AOId == aoId).AsNoTracking().Select(m => new AddressObject
            {
                AOId = m.AOId,
                AOGuid = m.AOGuid,
                FormalName = m.FormalName,
                ShortName = m.ShortName,
                SourceType = (AddressSourceType) m.SourceTypeId,
                PostCode = m.PostalCode
            }).FirstOrDefault();
        }

        public AddressEntity GetAddressEntity(long baId)
        {
            var address = DataContext.AOAddresses.AsNoTracking().FirstOrDefault(m => m.BAId == baId);
            if (address == null)
                return null;

            var date = DateTime.Now;

            var ao = DataContext.AOAddressObjects.AsNoTracking().FirstOrDefault(m => m.AOGuid == address.AOGuid && m.LiveStateId == 1);
            var house = DataContext.AOHouses.AsNoTracking().FirstOrDefault(m => m.HouseGuid == address.HouseGuid && m.StartDate <= date && m.EndDate >=date);
            var stead = DataContext.AOSteads.AsNoTracking().FirstOrDefault(m => m.SteadGuid == address.SteadGuid && m.StartDate <= date && m.EndDate >= date);

            return new AddressEntity()
            {
                BaId = address.BAId,
                Name = address.Name,
                AOId = ao?.AOId,
                AOGuid = address.AOGuid,
                HouseGuid = address.HouseGuid,
                HouseId = house?.HouseId,
                SteadGuid = address.SteadGuid,
                SteadId = stead?.SteadId,
                Location = address.Location,
                FlatNum = address.FlatNum,
                FlatTypeId = address.FlatTypeId,
                POBox = address.POBox,
                IsBuild = address.IsBuild,
                IsActual = address.IsActual
            };
        }

        public AddressUnsettleList GetAddressUnsettleList()
        {
            var result = DataContext.GetAddressUnsettleList();
            return result.FirstOrDefault();
        }

        public AddressUpdateResult AddressUpdate(long? baid, string name, Guid? aoId, Guid? houseId, Guid? steadId,
            string xData, string location, string flatNum, int? flatType, string sPOBox, bool isBuild, bool isActual)
        {
            var piBAId = new SqlParameter("@iBAId", baid.HasValue ? baid : (object) DBNull.Value)
                {
                    Direction = System.Data.ParameterDirection.InputOutput,
                    SqlDbType = System.Data.SqlDbType.BigInt
                };
            var psName = new SqlParameter("@sName", string.IsNullOrWhiteSpace(name) ? (object) DBNull.Value : name)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.NVarChar,
                Size = 400
            };

            ExecuteStoredProg(@" [dbo].[AO_AddressUpdate] @iBAId =	@iBAId OUT,		
	                                                      @sName =	@sName OUT,	
	                                                      @uAOId =	@uAOId,		
	                                                      @uHouseId =	@uHouseId,		
	                                                      @uSteadId =	@uSteadId,		
	                                                      @xData =	@xData,		
                                                          @sLocation =	@sLocation,	
                                                          @sFlatNum	=	@sFlatNum,		
                                                          @iFlatTypeId = @iFlatTypeId,
                                                          @sPOBox = @sPOBox,  	
	                                                      @bIsBuild	= @bIsBuild,		
	                                                      @bIsActial = @bIsActial",
                piBAId,
                psName,
                new SqlParameter("@uAOId", aoId.HasValue ? aoId : (object)DBNull.Value),
                new SqlParameter("@uHouseId", houseId.HasValue ? houseId : (object)DBNull.Value),
                new SqlParameter("@uSteadId", steadId.HasValue ? steadId : (object)DBNull.Value),
                new SqlParameter("@xData", string.IsNullOrWhiteSpace(xData) ? (object)DBNull.Value : xData)
                    {
                        SqlDbType = System.Data.SqlDbType.Xml
                    },
                new SqlParameter("@sLocation", string.IsNullOrWhiteSpace(location) ? (object)DBNull.Value : location),
                new SqlParameter("@sFlatNum", string.IsNullOrWhiteSpace(flatNum) ? (object)DBNull.Value : flatNum),
                new SqlParameter("@iFlatTypeId", flatType.HasValue ? flatType : (object)DBNull.Value),
                new SqlParameter("@sPOBox", !string.IsNullOrWhiteSpace(sPOBox) ? sPOBox : (object)DBNull.Value)
                    {
                        SqlDbType = System.Data.SqlDbType.NVarChar,
                        Size = 100
                },
                new SqlParameter("@bIsBuild", isBuild),
                new SqlParameter("@bIsActial", isActual));

            return new AddressUpdateResult
            {
                BaId = Convert.ToUInt32(piBAId.Value),
                Name = psName.Value.ToString()
            };
        }

        public AddressObjectUpdateResult AddressObjectUpdate(AddressObjectUpdate model, string xData)
        {
            var puAOId = new SqlParameter("@uAOId", model.AOId.HasValue ? model.AOId : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.UniqueIdentifier
            };
            var puAOGuid = new SqlParameter("@uAOGuid", model.AOGuid.HasValue ? model.AOGuid : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.UniqueIdentifier
            };
            var psFormalName = new SqlParameter("@sFormalName", model.FormalName);
            var piAOLevel = new SqlParameter("@iAOLevel", model.AOLevel);
            var psShortName = new SqlParameter("@sShortName", model.ShortName);
            var psPostCode = new SqlParameter("@sPostalCode", model.PostCode);
            var puParentId = new SqlParameter("@uParentId", model.ParentId.HasValue ? model.ParentId : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.UniqueIdentifier
            };
            var pxData = new SqlParameter("@xData", string.IsNullOrWhiteSpace(xData) ? (object) DBNull.Value : xData)
            {
                SqlDbType = System.Data.SqlDbType.Xml
            };

            ExecuteStoredProg(@"[dbo].[AO_AddressObjectUpdate]  @uAOId		 = @uAOId OUT,		
	                                                            @uAOGuid	 = @uAOGuid OUT,		
	                                                            @sFormalName = @sFormalName,
	                                                            @iAOLevel	 = @iAOLevel,	
	                                                            @sShortName	 = @sShortName,	
                                                                @sPostalCode = @sPostalCode,
	                                                            @uParentId	 = @uParentId,	
	                                                            @xData		 = @xData",
                puAOId,
                puAOGuid,
                psFormalName,
                piAOLevel, 
                psShortName,
                psPostCode,
                puParentId, 
                pxData
            );

            return new AddressObjectUpdateResult
            {
                AOId = (Guid)puAOId.Value,
                AOGuid = (Guid)puAOGuid.Value
            };
        }

        public HouseUpdateResult HouseUpdate(HouseUpdate model, string xData)
        {
            var puHouseId = new SqlParameter("@uHouseId", model.HouseId.HasValue ? model.HouseId : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.UniqueIdentifier
            };
            var puHouseGuid = new SqlParameter("@uHouseGuid", model.HouseGuid.HasValue ? model.HouseGuid : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.UniqueIdentifier
            };
            var psHouseNum = new SqlParameter("@sHouseNum", string.IsNullOrEmpty(model.HouseNum) ? (object)DBNull.Value : model.HouseNum)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar,
                Size = 20
            }; 
            var psBuildNum = new SqlParameter("@sBuildNum", string.IsNullOrEmpty(model.BuildNum) ? (object)DBNull.Value : model.BuildNum)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar,
                Size = 10
            }; 
            var psStrucNum = new SqlParameter("@sStrucNum", string.IsNullOrEmpty(model.StructNum) ? (object)DBNull.Value : model.StructNum)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar,
                Size = 10
            };
            var piStrStateId = new SqlParameter("@iStrStateId", model.StrStateId.HasValue ? model.StrStateId : (object)DBNull.Value);
            var piEstStateId = new SqlParameter("@iEstStateId", model.EstStateId.HasValue ? model.EstStateId : (object)DBNull.Value);


            var piPostalCode = new SqlParameter("@sPostalCode", model.PostCode);
            var piCadNumber = new SqlParameter("@sCadNum", model.CadNum);
            var piIsBuild = new SqlParameter("@bIsBuild", model.IsBuild);

            var puAOId = new SqlParameter("@uAOId", model.AOId.HasValue && model.SourceType == AddressSourceType.Custom ? model.AOId : (object) DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.UniqueIdentifier
            };
            var pxData = new SqlParameter("@xData", string.IsNullOrWhiteSpace(xData) ? (object)DBNull.Value : xData)
            {
                SqlDbType = System.Data.SqlDbType.Xml
            };

            ExecuteStoredProg(@"[dbo].[AO_HouseUpdate]  @uHouseId	 = @uHouseId OUT,		
	                                                    @uHouseGuid	 = @uHouseGuid OUT,	
	                                                    @sHouseNum	 = @sHouseNum,	
	                                                    @sBuildNum	 = @sBuildNum,	
	                                                    @sStrucNum	 = @sStrucNum,	
	                                                    @iStrStateId = @iStrStateId,
	                                                    @iEstStateId = @iEstStateId,
	                                                    @sPostalCode = @sPostalCode,
	                                                    @sCadNum     = @sCadNum,
	                                                    @bIsBuild    = @bIsBuild,	
	                                                    @uAOId		 = @uAOId,		
	                                                    @xData		 = @xData",
                    puHouseId,
                    puHouseGuid,
                    psHouseNum,
                    psBuildNum,
                    psStrucNum,
                    piStrStateId,
                    piEstStateId,
                    piPostalCode,
                    piCadNumber,
                    piIsBuild,
                    puAOId,
                    pxData                                                        		
                );

            return new HouseUpdateResult
            {
                HouseId = (Guid)puHouseId.Value,
                HouseGuid = (Guid)puHouseGuid.Value
            };
        }

        public SteadUpdateResult SteadUpdate(SteadUpdate model, string xData)
        {
            var puSteadId = new SqlParameter("@uSteadId", model.SteadId.HasValue ? model.SteadId : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.UniqueIdentifier
            };
            var puSteadGuid = new SqlParameter("@uSteadGuid", model.SteadGuid.HasValue ? model.SteadGuid : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.UniqueIdentifier
            };
            var psNumber = new SqlParameter("@sNumber", model.Number);

            var psPostalcode = new SqlParameter("@sPostalCode", model.PostCode);
            var psCadNumber = new SqlParameter("@sCadNum", model.CadNum);
            var puParentId = new SqlParameter("@uParentId", model.ParentId.HasValue ? model.ParentId : (object)DBNull.Value);
            var pxData = new SqlParameter("@xData", string.IsNullOrWhiteSpace(xData) ? (object)DBNull.Value : xData)
            {
                SqlDbType = System.Data.SqlDbType.Xml
            };

            ExecuteStoredProg(@"[dbo].[AO_SteadUpdate]  @uSteadId	= @uSteadId OUT,		
	                                                    @uSteadGuid	= @uSteadGuid OUT,			
	                                                    @sNumber	= @sNumber,		
	                                                    @sPostalCode= @sPostalCode,
	                                                    @sCadNum	= @sCadNum,
                                                        @uParentId	= @uParentId,
	                                                    @xData		= @xData",
                                puSteadId,
                                puSteadGuid,
                                psNumber,
                                psPostalcode,
                                psCadNumber,
                                puParentId,
                                pxData);

            return new SteadUpdateResult
            {
                SteadId = (Guid)puSteadId.Value,
                SteadGuid = (Guid)puSteadGuid.Value
            };
        }

        public AddressUpdateResult AddressUpdateName(long? baId, string newName)
        {
            var piBAId = new SqlParameter("@iBAId", baId.HasValue ? baId : (object)DBNull.Value)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.BigInt
            };
            var psName = new SqlParameter("@sName", string.IsNullOrWhiteSpace(newName) ? (object)DBNull.Value : newName)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.NVarChar,
                Size = 400
            };
            ExecuteStoredProg(@" [dbo].[AO_AddressNameUpdate] @iBAId =	@iBAId OUT,		
	                                                        @sName =	@sName OUT",
                                                            piBAId,
                                                            psName);
            return new AddressUpdateResult
            {
                BaId = Convert.ToUInt32(piBAId.Value),
                Name = psName.Value.ToString()
            };
        }

        public IEnumerable<AddressHierarchyResponse> GetAddressHierarchy(long? baId)
        {
            var resultList = new List<AddressHierarchyResponse>();
            AddressSourceType outSourceType;
            AddressObjectLevel outAddressObjectLevel;

            var query = DataContext.GetAddressData(baId);
            foreach (var address in query)
            {
                AddressSourceType.TryParse(address.SourceType.ToString(), out outSourceType);
                AddressObjectLevel.TryParse(address.AOLevel.ToString(), out outAddressObjectLevel);
                resultList.Add(new AddressHierarchyResponse
                {
                    AOId = address.AOId,
                    HouseId = address.HouseId,
                    SteadId = address.SteadId,
                    AOGuid = address.AOGuid,
                    ParentGuid = address.ParentGuid,
                    AOLevel = outAddressObjectLevel,
                    SourceType = outSourceType
                });
            }

            return resultList;
        }


        public void BindHouses(Guid firstHouseId, Guid secondHouseId, bool isBind)
        {
            var puHouseGuid = new SqlParameter("@uHouseGuid", firstHouseId)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.UniqueIdentifier
            };
            var puBindHouseGuid = new SqlParameter("@uBindHouseGuid", secondHouseId)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.UniqueIdentifier
            };
            var pbBind = new SqlParameter("@bBind", isBind)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.Bit
            };

            ExecuteStoredProg(@"[dbo].[AO_HouseBind]  @uHouseGuid	  =@uHouseGuid,			
	                                                  @uBindHouseGuid = @uBindHouseGuid,			
	                                                  @bBind 	      = @bBind ",
                puHouseGuid,
                puBindHouseGuid,
                pbBind
                );

        }


        public void AddressSettle(Guid? aoGuid, Guid fiasAOId, Guid? houseGuid, Guid? fiasHouseId, Guid? steadGuid, Guid? fiasSteadId, string xData)
        {
            var puAOGuid = new SqlParameter("@uAOGuid", aoGuid.HasValue ? aoGuid : (object)DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.UniqueIdentifier
            };
            var puFiasAOId = new SqlParameter("@uFiasAOId", fiasAOId)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.UniqueIdentifier
            };

            var puHouseGuid = new SqlParameter("@uHouseGuid", houseGuid.HasValue ? houseGuid : (object)DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.UniqueIdentifier
            };
            var puFiasHouseId = new SqlParameter("@uFiasHouseId", fiasHouseId.HasValue ? fiasHouseId : (object)DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.UniqueIdentifier
            };

            var puSteadGuid = new SqlParameter("@uSteadGuid", steadGuid.HasValue ? steadGuid : (object)DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.UniqueIdentifier
            };
            var puFiasSteadId = new SqlParameter("@uFiasSteadId", fiasSteadId.HasValue ? fiasSteadId : (object)DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.UniqueIdentifier
            };

            var pxData = new SqlParameter("@xData", string.IsNullOrWhiteSpace(xData) ? (object)DBNull.Value : xData)
            {
                SqlDbType = SqlDbType.Xml
            };

            ExecuteStoredProg(@"[dbo].[AO_AddressSettle]  @uAOGuid		= @uAOGuid,			
	                                                      @uFiasAOId	= @uFiasAOId,			
	                                                      @uHouseGuid	= @uHouseGuid,		
	                                                      @uFiasHouseId	= @uFiasHouseId,
	                                                      @uSteadGuid	= @uSteadGuid,		
                                                          @uFiasSteadId	= @uFiasSteadId,
                                                          @xData        = @xData",
                puAOGuid,
                puFiasAOId,
                puHouseGuid,
                puFiasHouseId,
                puSteadGuid,
                puFiasSteadId,
                pxData);
        }
        public bool IsFindAddressObject(AddressObjectUpdate model)
        {
            const string query = @"SELECT [dbo].[AO_IsFindAddressObjectByParameters] (@nFormalName,@nShortName,@iAOLevel,@uParentGuid)";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("nFormalName", model.FormalName) {
                SqlDbType = System.Data.SqlDbType.NVarChar
            });
            parameters.Add(new SqlParameter("nShortName", model.ShortName)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar
            });
            parameters.Add(new SqlParameter("iAOLevel", model.AOLevel)
            {
                SqlDbType = System.Data.SqlDbType.Int
            });
            parameters.Add(new SqlParameter("uParentGuid", model.ParentId.HasValue ? model.ParentId : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.UniqueIdentifier
            });
            return DataContext.Database.SqlQuery<bool>(query, parameters.ToArray()).FirstOrDefault();
        }
        public bool IsFindHouse(HouseUpdate model)
        {
            const string query = @"SELECT [dbo].[AO_IsFindHouseByParameters] (@uAOGUID,@iEstStatId,@nHouseNum,@nBuildNum,@iStrStatId,@nStructNum,@nCadNum,@nPostCode)";
       
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("uAOGUID", model.ParentGuid) {
                SqlDbType = System.Data.SqlDbType.UniqueIdentifier
            });
            parameters.Add(new SqlParameter("iEstStatId", (model.EstStateId != null && model.EstStateId > 0) ? model.EstStateId : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.Int
            });
            parameters.Add(new SqlParameter("nHouseNum", !String.IsNullOrEmpty(model.HouseNum) ? model.HouseNum : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar
            });
            parameters.Add(new SqlParameter("nBuildNum", !String.IsNullOrEmpty(model.BuildNum) ? model.BuildNum : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar
            });
            parameters.Add(new SqlParameter("iStrStatId", (model.StrStateId != null && model.StrStateId > 0) ? model.StrStateId : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.Int
            });
            parameters.Add(new SqlParameter("nStructNum", !String.IsNullOrEmpty(model.StructNum) ? model.StructNum : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar
            });
            parameters.Add(new SqlParameter("nCadNum", !String.IsNullOrEmpty(model.CadNum) ? model.CadNum : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar
            });
            parameters.Add(new SqlParameter("nPostCode", !String.IsNullOrEmpty(model.PostCode) ? model.PostCode : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar
            });

            return  DataContext.Database.SqlQuery<bool>(query, parameters.ToArray()).FirstOrDefault();
        }
        public bool IsFindStead(SteadUpdate model)
        {
            const string query = @"SELECT [dbo].[AO_IsFindSteadByParameters] (@nNumber,@uAOGUID,@nCadNum)";

            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("nNumber", model.Number)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar
            });
            parameters.Add(new SqlParameter("uAOGUID", model.ParentGuid)
            {
                SqlDbType = System.Data.SqlDbType.UniqueIdentifier
            });
            parameters.Add(new SqlParameter("nCadNum", !String.IsNullOrEmpty(model.CadNum) ? model.CadNum : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.NVarChar
            });
            return DataContext.Database.SqlQuery<bool>(query, parameters.ToArray()).FirstOrDefault();
        }


    }
}

﻿using System;
using System.Collections.Generic;
using HIS.DAL.Client.Models.Address;
using HIS.DAL.DB.Context;
using HIS.Models.Layer.Models.Address;

namespace HIS.DAL.DB.Handlers.Addresses
{
    public interface IAddressHandler: IDataContextHandler
    {
        IEnumerable<AOAddress> AddressFind(string findStr, int? sourceType);
        FTSAddressList AddressByHouseId(Guid AOid, Guid houseId);
        FTSAddressList AddressBySteadId(Guid AOid, Guid steadId);
        FTSAddressList AddressByAOId(Guid AOid);
        IEnumerable<AOAddressObject> GetAddressObjetcList(Guid? parentId, int aoLavel, int? sourceType);
        IEnumerable<AOHouse> GetHouseList(Guid? parentId, int? sourceType);
        IEnumerable<AOStead> GetSteadList(Guid? parentId, int? sourceType);

        AO_HouseData_Result GetHouse(Guid? houseId);
        AO_SteadData_Result GetStead(Guid? steadId);

        bool IsFindAddressObject(AddressObjectUpdate model);
        bool IsFindHouse(HouseUpdate model);
        bool IsFindStead(SteadUpdate model);

        IEnumerable<AOAddressResult> GetAddressList(Guid? aoId, Guid? houseId, Guid? steadId,
            int? actionStateId, AddressSourceType? sourceTypeId);
        IEnumerable<AO_GetAdvancedSearchList_Result>GetAdvancedSearchList(Guid? aoId, Guid? houseId, Guid? steadId,
         int? actionStateId, AddressSourceType? sourceTypeId, string postCode = null, string cadNum = null);
        IEnumerable<FlatType> GetFlatTypeList(bool isVisible);
        HouseUpdate GetHouseUpdateInfo(Guid houseId);
        SteadUpdate GetSteadUpdateInfo(Guid steadId);
        AddressObject GetAddressObject(Guid aoId);
        AddressEntity GetAddressEntity(long baId);
        AddressUnsettleList GetAddressUnsettleList();

        AddressUpdateResult AddressUpdate(long? baid, string name, Guid? aoId, Guid? houseId, Guid? steadId,
            string xData, string location, string flatNum, int? flatType, string sPOBox, bool isBuild, bool isActual);

        AddressObjectUpdateResult AddressObjectUpdate(AddressObjectUpdate model, string xData);
        HouseUpdateResult HouseUpdate(HouseUpdate model, string xData);
        SteadUpdateResult SteadUpdate(SteadUpdate model, string xData);
        AddressUpdateResult AddressUpdateName(long? baId, string newName);
        IEnumerable<AddressHierarchyResponse> GetAddressHierarchy(long? baId);

        void AddressSettle(Guid? aoGuid, Guid fiasAOId, Guid? houseGuid, Guid? fiasHouseId, Guid? steadGuid, Guid? fiasSteadId, string xData);
        void BindHouses(Guid firstHouseId, Guid secondHouseId, bool isBind);
    }
}

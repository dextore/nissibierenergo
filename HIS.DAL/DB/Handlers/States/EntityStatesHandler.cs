﻿using System.Collections.Generic;
using System.Linq;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.States
{
    public class EntityStatesHandler: DataContextHandlerBase, IEntityStatesHandler
    {
        public IEnumerable<EFSOBATypeState> GetEntityTypeStates(int baTypeId)
        {
            return DataContext.EFSOBATypeStates.Where(o => o.BATypeId == baTypeId);
        }
    }
}

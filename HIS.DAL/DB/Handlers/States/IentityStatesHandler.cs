﻿using System.Collections.Generic;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.States
{
    public interface IEntityStatesHandler: IDataContextHandler
    {
        IEnumerable<EFSOBATypeState> GetEntityTypeStates(int baTypeId);
    }
}
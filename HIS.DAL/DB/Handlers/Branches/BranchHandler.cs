﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.Client.Models.Branches;


namespace HIS.DAL.DB.Handlers.Branches
{
    public class BranchHandler : DataContextHandlerBase, IBranchHandler
    {
        public IEnumerable<Branch> GetBranches()
        {
            return DataContext.RefBranches.Select(efbranch => new Branch
            {
                Baid = efbranch.BAId,
                ParentId = efbranch.ParentId ?? 0,
                BrnNum = efbranch.BrnNum,
                Name = efbranch.Name,
                TreeLevel = efbranch.TreeLevel
            }).ToList();
        }
        
        public IEnumerable<BranchItem> UpdatePersonBranches(long? comercialPersonId, IEnumerable<BranchItem> branches)
        {
            var errorList = new List<BranchItem>();

            var iBaId = new SqlParameter("@iBAId", comercialPersonId);
            iBaId.Direction = ParameterDirection.Input;
            iBaId.SqlDbType = SqlDbType.BigInt;

            var iMarkerId = new SqlParameter("@iMarkerId", 120);
            iMarkerId.Direction = ParameterDirection.Input;
            iMarkerId.SqlDbType = SqlDbType.BigInt;
            
            var sNote = new SqlParameter("@sNote", string.Empty);
            sNote.Direction = ParameterDirection.Input;
            sNote.SqlDbType = SqlDbType.VarChar;

            foreach (var branch in branches)
            {
                var errorResult = BranchItemCheck(comercialPersonId, branch);
                    
                if (errorResult != string.Empty)
                {
                    errorList.Add(new BranchItem
                    {
                        ItemId = -1,
                        Name = errorResult
                    });
                    continue;
                }

                var iItemId = branch.ItemId.HasValue 
                    ? new SqlParameter("@iItemId", branch.ItemId.Value) 
                    : new SqlParameter("@iItemId", DBNull.Value);
                iMarkerId.Direction = ParameterDirection.InputOutput;
                iMarkerId.SqlDbType = SqlDbType.BigInt;


                var dStartDate = branch.StartDate.HasValue 
                    ? new SqlParameter("@dStartDate", branch.StartDate.Value)
                    : new SqlParameter("@dStartDate", DateTime.Now);
                dStartDate.Direction = ParameterDirection.InputOutput;
                dStartDate.SqlDbType = SqlDbType.Date;

                var dEndDate = branch.EndDate.HasValue 
                    ? new SqlParameter("@dEndDate", branch.EndDate.Value) 
                    : new SqlParameter("@dEndDate", DBNull.Value);
                dEndDate.Direction = ParameterDirection.InputOutput;
                dEndDate.SqlDbType = SqlDbType.Date;

                var iValue = new SqlParameter("@iValue", branch.Baid);
                iValue.Direction = ParameterDirection.Input;
                iValue.SqlDbType = SqlDbType.BigInt;

                ExecuteStoredProg(@"[dbo].[LS_BranchItemUpdate] @iBAId = @iBAId,
                                       @iMarkerId = @iMarkerId,
                                       @iItemId = @iItemId,
                                       @dStartDate = @dStartDate,
                                       @dEndDate = @dEndDate OUT,
                                       @iValue = @iValue,
                                       @sNote = @sNote",
                    iBaId,
                    iMarkerId,
                    dStartDate,
                    iItemId,
                    dEndDate,
                    iValue,
                    sNote);
            }

            var result = GetPersonsBranches(comercialPersonId).ToList();
            result.AddRange(errorList);
            return result;
        }

        public IEnumerable<BranchItem> DeletePersonBranches(long? comercialPersonId, IEnumerable<BranchItem> branches)
        {
            var iBaId = new SqlParameter("@iBAId", comercialPersonId);
            iBaId.Direction = ParameterDirection.Input;
            iBaId.SqlDbType = SqlDbType.BigInt;

            var iMarkerId = new SqlParameter("@iMarkerId", 120);
            iMarkerId.Direction = ParameterDirection.Input;
            iMarkerId.SqlDbType = SqlDbType.BigInt;

            foreach (var branch in branches)
            {
                var iItemId = branch.ItemId.HasValue
                    ? new SqlParameter("@iItemId", branch.ItemId.Value)
                    : new SqlParameter("@iItemId", DBNull.Value);
                iMarkerId.Direction = ParameterDirection.InputOutput;
                iMarkerId.SqlDbType = SqlDbType.BigInt;
                

                ExecuteStoredProg(@"[dbo].[LS_BranchItemDelete] @iBAId = @iBAId,
                                       @iMarkerId = @iMarkerId,
                                       @iItemId = @iItemId",
                    iBaId,
                    iMarkerId,
                    iItemId);
            }

            return GetPersonsBranches(comercialPersonId); 
        }


        public IEnumerable<BranchItem> GetPersonsBranches(long? comercialPersonId)
        {
            return DataContext.LSBranchItemValues
                .Where(lsb => lsb.MarkerId == 120
                              && lsb.BAId == comercialPersonId)
                .Join(DataContext.RefBranches,
                lsbranchItemValues => lsbranchItemValues.Value,
                refBranchesValue => refBranchesValue.BAId,
                (lsbriv, rb) =>
                            new BranchItem
                            {
                                Name = rb.Name,
                                ParentId = rb.ParentId?? 0,
                                Baid = rb.BAId,
                                BrnNum = rb.BrnNum,
                                TreeLevel = rb.TreeLevel,
                                StartDate = lsbriv.StartDate,
                                EndDate = lsbriv.EndDate,
                                ItemId = lsbriv.ItemId
                            })
                .ToList();
        }

        public string BranchItemCheck(long? comercialPersonId, BranchItem branch)
        {
            var iBAIdString = comercialPersonId.HasValue ? comercialPersonId.Value.ToString() : "NULL";
            var iMarkerIdString = "120";
            var itemidString = branch.ItemId.HasValue ? branch.ItemId.Value.ToString() : "NULL";
            var dStartDateString =
                branch.StartDate.HasValue ? "{d'" + branch.StartDate.Value.ToString("yyyy-MM-dd") + "'}" : "NULL";
            var dEndDateString = branch.EndDate.HasValue ? "{d'" + branch.EndDate.Value.ToString("yyyy-MM-dd") + "'}" : "NULL";
            var iValueString = branch.Baid.HasValue ? branch.Baid.Value.ToString() : "NULL";

            var sql =
                $"SELECT dbo.LS_BranchItemCheck ({iBAIdString}, {iMarkerIdString}, {itemidString}, {dStartDateString}, {dEndDateString}, {iValueString})";
            var result = DataContext.Database.SqlQuery<string>(sql).Single();

            return result;
        }
    }
}

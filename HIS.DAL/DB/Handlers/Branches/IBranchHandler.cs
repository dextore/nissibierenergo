﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Branches;

namespace HIS.DAL.DB.Handlers.Branches
{
    public interface IBranchHandler : IDataContextHandler
    {
        IEnumerable<Branch> GetBranches();
        IEnumerable<BranchItem> UpdatePersonBranches(long? comercialPersonId, IEnumerable<BranchItem> branches);
        IEnumerable<BranchItem> DeletePersonBranches(long? comercialPersonId, IEnumerable<BranchItem> branches);
        IEnumerable<BranchItem> GetPersonsBranches(long? comercialPersonId);
        string BranchItemCheck(long? comercialPersonId, BranchItem branch);
    }
}

﻿using System.Linq;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.Catalogs
{
    public class CatalogsHandler: DataContextHandlerBase, ICatalogsHandler
    {
        public IQueryable<EFvLSOrganisationLegalForm> LSOrganisationLegalForms()
        {
            return DataContext.vLSOrganisationLegalForms;
        }

        public IQueryable<EFvLSPersonLegalForm> LSPersonLegalForms()
        {
            return DataContext.vLSPersonLegalForms;
        }
        public IQueryable<EFvFALocations> FALocations()
        {
            return DataContext.vFALocations;
        }
        public IQueryable<EFvFAMovReasons>FAMovReasons()
        {
            return DataContext.EFvFAMovReason;
        }
    }
}

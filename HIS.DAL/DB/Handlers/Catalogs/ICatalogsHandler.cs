﻿using System.Linq;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.Catalogs
{
    public interface ICatalogsHandler: IDataContextHandler
    {
        IQueryable<EFvLSOrganisationLegalForm> LSOrganisationLegalForms();
        IQueryable<EFvLSPersonLegalForm> LSPersonLegalForms();
        IQueryable<EFvFALocations> FALocations();
        IQueryable<EFvFAMovReasons> FAMovReasons();

    }
}

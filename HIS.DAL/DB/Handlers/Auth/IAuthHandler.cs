﻿using System.Linq;
using HIS.Auth.Entities;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Handlers.Auth
{
    public interface IAuthHandler : IDataContextHandler
    {
        /// <summary>
        /// Получение информации о пользователе
        /// </summary>
        /// <param name="userId">ИД пользователя</param>
        /// <returns></returns>       
        EFUser GetUser(int userId);

        /// <summary>
        /// Получение информации о авторизованном пользователе
        /// </summary>
        /// <returns></returns>
        EFUser GetCurrentUser();

        /// <summary>
        /// Текущее имя авторизованного пользователя
        /// </summary>
        /// <returns></returns>
        string GetCurrentUserName();

        /// <summary>
        /// Получение имени текущей БД на которой аутентифицирован пользователь
        /// </summary>
        /// <returns></returns>
        string GetCurrentDBAlias();

        /// <summary>
        /// Получение строки подключения к БД на которой аутентифицирован пользователь
        /// </summary>
        /// <returns></returns>
        string GetCurrentDBConnectionString();

        ///// <summary>
        ///// Получение ролей пользователя
        ///// </summary>
        ///// <param name="userId">ИД пользователя</param>
        ///// <returns></returns>
        //IQueryable<EFRole> GetUserRoles(int userId);

        ///// <summary>
        ///// Получние прав пользователя
        ///// </summary>
        ///// <param name="userId">ИД пользователя</param>
        ///// <returns></returns>
        //IQueryable<EFRight> GetUserRights(int userId);

        /// <summary>
        /// Список БД для авторизации
        /// </summary>
        /// <returns></returns>
        IQueryable<HISDataBaseArea> GetDataBaseAreas();

        /// <summary>
        /// Проверка пользователя на БД авторизации
        /// </summary>
        /// <param name="id">ИД пользователя</param>
        /// <param name="dbAlias">Алиас БД авторизации</param>
        /// <returns></returns>
        bool CheckUser(int id, string dbAlias);
    }
}

﻿using System.Linq;
using System.Web;
using HIS.Auth.DBContext;
using HIS.Auth.Entities;
using HIS.DAL.DB.Context;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;

namespace HIS.DAL.DB.Handlers.Auth
{
    public class AuthHandler: DataContextHandlerBase, IAuthHandler
    {
        private readonly AuthDataBaseContext _authDBContext;

        public AuthHandler()
        {
            _authDBContext = new AuthDataBaseContext();        
        }

        /// <summary>
        /// Контекст запроса OWIN
        /// </summary>
        private IOwinContext OwinContext
        {
            get
            {
                IOwinContext owinContext = HttpContext.Current.GetOwinContext();
                return owinContext;
            }
        }

        /// <summary>
        /// Текущий аутентифицированный пользователь
        /// </summary>
        /// <returns></returns>
        public EFUser GetCurrentUser()
        {  
            return GetUser(OwinContext.Authentication.User.Identity.GetUserId<int>());
        }

        /// <summary>
        /// Текущее имя аутентифицированного пользователя
        /// </summary>
        /// <returns></returns>
        public string GetCurrentUserName()
        {
            return OwinContext.Authentication.User.Identity.Name;
        }

        /// <summary>
        /// Получение имени текущей БД на которой аутентифицирован пользователь
        /// </summary>
        /// <returns></returns>
        public string GetCurrentDBAlias()
        {            
            return OwinContext.Authentication.User.Claims.FirstOrDefault(c => c.Type.Equals("DBAlias"))?.Value;
        }

        /// <summary>
        /// Получение строки подключения к БД на которой аутентифицирован пользователь
        /// </summary>
        /// <returns></returns>
        public string GetCurrentDBConnectionString()
        {
            //return @"data source=srv-sqltest;initial catalog=NewHermesModel;integrated security=False;user id=test;password=test;multipleactiveresultsets=True;application name=EntityFramework";
            string dbAlias = GetCurrentDBAlias();
            return _authDBContext.DataBaseAreas.FirstOrDefault(c => c.Alias.Equals(dbAlias))?.ConnectionString;
        }

        /// <summary>
        /// Получение данных о пользователе БД авторизации
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public EFUser GetUser(int userId)
        {
            return DataContext?.EFUsers.FirstOrDefault(c => c.Id == userId);
        }

        ///// <summary>
        ///// Получение ролей пользователя
        ///// </summary>
        ///// <param name="userId">ИД пользователя</param>
        ///// <returns></returns>
        //public IQueryable<EFRole> GetUserRoles(int userId)
        //{
        //    return DataContext?.EFUserRoles.Where(c => c.UserId == userId).AsQueryable().Select(c => c.HISRoles);
        //}

        ///// <summary>
        ///// Получние прав пользователя
        ///// </summary>
        ///// <param name="userId">ИД пользователя</param>
        ///// <returns></returns>
        //public IQueryable<EFRight> GetUserRights(int userId)
        //{
        //   return GetUserRoles(userId).SelectMany(x => x.HISRights);            
        //}

        /// <summary>
        /// Список БД для авторизации
        /// </summary>
        /// <returns></returns>
        public IQueryable<HISDataBaseArea> GetDataBaseAreas()
        {
            return _authDBContext.DataBaseAreas;
        }

        /// <summary>
        /// Проверка пользователя на ба авторизации
        /// </summary>
        /// <param name="id">ИД пользователя</param>
        /// <param name="dbAlias">Алиас БД авторизации</param>
        /// <returns></returns>
        public bool CheckUser(int id, string dbAlias)
        {
            string connStr =
                _authDBContext.Users.FirstOrDefault(c => c.Id == id)?
                    .DataBaseAreas.FirstOrDefault(a => a.Alias.Equals(dbAlias))?.ConnectionString;

            if (string.IsNullOrEmpty(connStr)) return false;

            HISModelEntities dbContext = new HISModelEntities(connStr);
            if (dbContext.EFUsers.FirstOrDefault(c => c.Id == id) == null)
                return false;

            return true;
        }
    }
}

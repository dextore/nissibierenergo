﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.Client.Models.AdminPanel;
using HIS.DAL.DB.Common;
using HIS.Models.Layer.Models.AdminPanel;

namespace HIS.DAL.DB.Handlers.AdminPanel
{
    public class AdminHandler : DataContextHandlerBase, IAdminHandler
    {
        public IEnumerable<BaTypesView> GetEntities()
        {
            var list = DataContext.EFBATypes.Join(DataContext.EFBASearchTypes,
                    type => type.SearchTypeId,
                    efbasSearchTypes => efbasSearchTypes.TypeId,
                    (x, searchType) => new BaTypesView
                    {
                        Name = x.Name,
                        TypeId = x.TypeId,
                        FullName = x.FullName,
                        Parent = x.ParentId.HasValue ? x.BATypes2.Name : "",
                        ParentId = x.ParentId,
                        MvcAlias = x.MVCAlias,
                        FixOperationHistory = x.FixOperationHistory,
                        IsGroupType = x.IsGroupType,
                        IsImplementTypeName = x.ImplementTypeName,
                        IsImplementType = x.IsImplementType,
                        SearchType = searchType.Name,
                        SearchTypeId = searchType.TypeId,
                        ShortName = x.ShortName
                    })
                .ToList();

            return list;
        }

        public IEnumerable<ItemTableView> GetAvailableValuesForReference(int? markerId)
        {
            var viewTable = DataContext.EFMAMarkers.FirstOrDefault(x => x.MarkerId == markerId)?.ImplementTypeName;
            var sql = $@"SELECT vT.ItemId,
                                vT.ItemName,
                                vT.IsActive,
                                vT.OrderNum as OrderNumber
                         FROM {viewTable} AS vT";

            return DataContext.Database.SqlQuery<ItemTableView>(sql).ToList();
        }

        public IEnumerable<ItemTableView> GetAvailableValuesForList(int? markerId)
        {
            return DataContext.EFMAMarkerValueList.Where(x => x.MarkerId == markerId)
                .Select(x => new ItemTableView
                {
                    ItemId = x.ItemId,
                    ItemName = x.ItemName,
                    IsActive = x.IsActive,
                    OrderNumber = x.OrderNum
                })
                .ToList();
        }

        public IEnumerable<MarkersAdminView> GetMarkers(int? baTypeId)
        {
            var sql = $@"SELECT m.MarkerId AS 'Id',
                         	   m.Name AS 'Name',
                         	   m.MVCAlias AS 'MvcAlias',
                               mt.TypeId AS 'BaTypeLinkId',
                         	   b.Name AS 'BaTypeLink',
                         	   mt.Name AS 'MarkerType',
                         	   mm.IsPeriodic,
                         	   mm.IsCollectible, 
                         	   mm.IsBlocked, 
                         	   mm.IsRequired, 
                         	   mm.FixMarkerHistory AS 'IsFixMarkerHistory',
                         	   CAST(0 AS BIT) AS 'IsOptional',
                         	   CASE 
                         	   WHEN mm.ImplementTypeField IS NULL
                         	   THEN CAST(0 AS BIT)
	                           ELSE CAST(1 AS BIT)
                         	   END IsImplementTypeField,
                               mm.IsInheritToDescendant,
                               mm.ImplementTypeName, 
                         	   mm.ImplementTypeField,
                         	   mm.Name AS 'OverrideName'
                         FROM MAMarkers AS m 
                         LEFT JOIN BATypes AS b
                         	ON b.TypeId = m.BATypeId
                         LEFT JOIN MAMarkerTypes AS mt
                         	ON mt.TypeId = m.TypeId
                         LEFT JOIN MABATypeMarkers AS mm
                         	ON mm.MarkerId = m.MarkerId
                         WHERE mm.BATypeId = {baTypeId}";
            return DataContext.Database.SqlQuery<MarkersAdminView>(sql).ToList();
        }

        public IEnumerable<BaTypesStatesView> GetAvailableSteteForEntiti(int? baTypeId)
        {
            return DataContext.EFSOBATypeStates
                .Where(x => x.BATypeId == baTypeId)
                .Select(x => new BaTypesStatesView
                {
                    StateId = x.StateId,
                    IsFirstState = x.IsFirstState,
                    IsDelState = x.IsDelState,
                    StateName = x.SOStates.Name
                }).ToList();
        }

        public IEnumerable<BaTypesOperationsView> GetAvailableOperationsForEntiti(int? baTypeId)
        {
            return DataContext.SOBATypeOperations
                .Where(x => x.BATypeId == baTypeId)
                .Select(x => new BaTypesOperationsView
                {
                    IsCreateOperation = x.IsCreateOperation,
                    IsDeleteOperation = x.IsDeleteOperation,
                    OperationId = x.OperationId,
                    OperationName = x.SOOperations.Name
                }).ToList();
        }

        public IEnumerable<BaTypesOperationsStateView> GetAvailablOperationsStateForEntiti(int? baTypeId)
        {
            return DataContext.EFSOBATypeStateOperations
                .Where(x => x.BATypeId == baTypeId)
                .Join(DataContext.EFSOOperations, sso => sso.OperationId, s => s.OperationId, (sso, s) =>
                    new BaTypesOperationsStateView
                    {
                        OperationId = sso.OperationId,
                        OperationName = s.Name,
                        IsBlocked = sso.IsBlocked,
                        SourceStateId = DataContext.EFSOStates.FirstOrDefault(x => x.StateId == sso.SrcStateId).StateId,
                        SourceStateName = DataContext.EFSOStates.FirstOrDefault(x => x.StateId == sso.SrcStateId).Name,
                        DestinationStateName =
                            DataContext.EFSOStates.FirstOrDefault(x => x.StateId == sso.DstStateId).Name
                    })
                .ToList();
        }

        public IEnumerable<OnlyMarkersDataView> GetMarkers()
        {
            var sql = $@"SELECT m.MarkerId AS 'Id',
                         	   m.Name AS 'Name',
                         	   m.MVCAlias AS 'MvcAlias',
                         	   b.Name AS 'BaTypeLink',
                               b.TypeId AS 'BaTypeLinkId',
                         	   mt.Name AS 'MarkerType',
                         	   m.ImplementTypeName AS 'ImplementTypeName',
                               mt.TypeId AS 'MarkerTypeId'
                         FROM MAMarkers AS m 
                         LEFT JOIN BATypes AS b
                         	ON b.TypeId = m.BATypeId
                         LEFT JOIN MAMarkerTypes AS mt
                         	ON mt.TypeId = m.TypeId";

            return DataContext.Database.SqlQuery<OnlyMarkersDataView>(sql).ToList();
        }

        public BaTypesView CreateUpdateEntitiType(BaTypesView view)
        {
            var iBATypeId = new SqlParameter("@iBATypeId", view.TypeId ?? (object)DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.BigInt
            };
            var sName = new SqlParameter("@sName", view.Name )
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            var sMVCAlias = new SqlParameter("@sMVCAlias", view.MvcAlias)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            var sShortName = new SqlParameter("@sShortName", view.ShortName ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            var sFullName = new SqlParameter("@sFullName", view.FullName ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            var iParentId = new SqlParameter("@iParentId", view.ParentId ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var bFixOperationHistory = new SqlParameter("@bFixOperationHistory", view.FixOperationHistory)
            {
                SqlDbType = SqlDbType.Bit
            };
            var bIsGroupType = new SqlParameter("@bIsGroupType", view.IsGroupType)
            {
                SqlDbType = SqlDbType.Bit
            };
            var bIsImplementType = new SqlParameter("@bIsImplementType ", view.IsImplementType)
            {
                SqlDbType = SqlDbType.Bit
            };
            var sImplementTypeName = new SqlParameter("@sImplementTypeName", view.IsImplementTypeName ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            var iSearchTypeId = new SqlParameter("@iSearchTypeId", view.SearchTypeId ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };


            ExecuteStoredProg(@"[dbo].[BA_TypeUpdate]
	                                @iBATypeId = @iBATypeId OUT,
	                                @sName = @sName,
	                                @sMVCAlias = @sMVCAlias,
	                                @sShortName = @sShortName,
	                                @sFullName = @sFullName,
	                                @iParentId = @iParentId,
	                                @bFixOperationHistory = @bFixOperationHistory,
	                                @bIsGroupType = @bIsGroupType,
	                                @bIsImplementType = @bIsImplementType,
	                                @sImplementTypeName = @sImplementTypeName,
	                                @iSearchTypeId = @iSearchTypeId",
                iBATypeId,
                sName,
                sMVCAlias,
                sShortName,
                sFullName,
                iParentId,
                bFixOperationHistory,
                bIsGroupType,
                bIsImplementType,
                sImplementTypeName,
                iSearchTypeId);

            return GetEntities().FirstOrDefault(x => x.TypeId == (long)iBATypeId.Value);
        }

        public OnlyMarkersDataView CreateUpdateMarker(OnlyMarkersDataView marker)
        {
            var iMarkerId = new SqlParameter("@iMarkerId", marker.Id ?? (object)DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.BigInt
            };
            var sMVCAlias = new SqlParameter("@sMVCAlias", marker.MvcAlias)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            var iTypeId = new SqlParameter("@iTypeId", marker.MarkerTypeId)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iBATypeId = new SqlParameter("@iBATypeId", marker.BaTypeLinkId ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var sImplementTypeName = new SqlParameter("@sImplementTypeName", marker.ImplementTypeName ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            var sName = new SqlParameter("@sName", marker.Name)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            ExecuteStoredProg(@"[dbo].[MA_MarkerUpdate]
	                                @iMarkerId = @iMarkerId OUT, 
	                                @sMVCAlias = @sMVCAlias, 
	                                @iTypeId = @iTypeId, 
	                                @iBATypeId = @iBATypeId,
	                                @sImplementTypeName = @sImplementTypeName, 
	                                @sName = @sName",
                iMarkerId,
                sMVCAlias,
                iTypeId,
                iBATypeId,
                sImplementTypeName,
                sName);

            return GetMarkers().FirstOrDefault(x => x.Id.Value == (long)iMarkerId.Value);
        }

        public bool DeleteMarkerFromBaType(int baId, MarkersAdminView marker)
        {
            var iMarkerId = new SqlParameter("@iMarkerId", marker.Id)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iBATypeId = new SqlParameter("@iBATypeId", baId)
            {
                SqlDbType = SqlDbType.BigInt
            };
            try
            {
                ExecuteStoredProg(@"[dbo].[MABA_TypeMarkerDelete]
                                    @iBATypeId = @iBATypeId,
	                                @iMarkerId = @iMarkerId",
                    iBATypeId,
                    iMarkerId);
            }
            catch (Exception e)
            {
                throw e;
            }

            return true;
        }

        public MarkersAdminView AddMarkerToBaType(int baId, MarkersAdminView view)
        {
            var iBATypeId = new SqlParameter("@iBATypeId", baId)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iMarkerId = new SqlParameter("@iMarkerId", view.Id)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.BigInt
            };
            var bIsPeriodic = new SqlParameter("@bIsPeriodic", view.IsPeriodic)
            {
                SqlDbType = SqlDbType.Bit
            };
            var bIsCollectible = new SqlParameter("@bIsCollectible", view.IsCollectible)
            {
                SqlDbType = SqlDbType.Bit
            };
            var bIsBlocked = new SqlParameter("@bIsBlocked", view.IsBlocked)
            {
                SqlDbType = SqlDbType.Bit
            };
            var bIsRequired = new SqlParameter("@bIsRequired", view.IsRequired)
            {
                SqlDbType = SqlDbType.Bit
            };
            var bFixMarkerHistiry = new SqlParameter("@bFixMarkerHistiry", view.IsFixMarkerHistory)
            {
                SqlDbType = SqlDbType.Bit
            };
            var bIsInheritToDescendant = new SqlParameter("@bIsInheritToDescendant", true);
            var sImplementTypeName = new SqlParameter("@sImplementTypeName", string.IsNullOrEmpty(view.ImplementTypeName)
                ? (object)DBNull.Value
                : view.ImplementTypeName)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            var sImplementTypeField = new SqlParameter("@sImplementTypeField", string.IsNullOrEmpty(view.ImplementTypeField)
                ? (object)DBNull.Value 
                : view.ImplementTypeField)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            var sName = new SqlParameter("@sName", string.IsNullOrEmpty(view.Name) 
                ? (object)DBNull.Value
                : view.Name)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            ExecuteStoredProg(@"[dbo].[MABA_TypeMarkerUpdate]
	                                @iBATypeId = @iBATypeId,
                                    @iMarkerId = @iMarkerId,
	                                @bIsPeriodic = @bIsPeriodic,
	                                @bIsCollectible = @bIsCollectible,
	                                @bIsBlocked = @bIsBlocked,
	                                @bIsRequired = @bIsRequired,
	                                @bFixMarkerHistiry = @bFixMarkerHistiry,
	                                @bIsInheritToDescendant = @bIsInheritToDescendant,
	                                @sImplementTypeName = @sImplementTypeName,
	                                @sImplementTypeField = @sImplementTypeField,
	                                @sName = @sName",
                    iBATypeId,
                    iMarkerId,
                    bIsPeriodic,
                    bIsCollectible,
                    bIsBlocked,
                    bIsRequired,
                    bFixMarkerHistiry,
                    bIsInheritToDescendant,
                    sImplementTypeName,
                    sImplementTypeField,
                    sName);

            return GetMarkers(baId).FirstOrDefault(x => x.Id == view.Id);
        }

        public bool CanEditLinkOnEntityMarker(int markerId)
        {
            return !DataContext.EFMABATypeMarkers.Any(x => x.MarkerId == markerId);
        }

        public bool DeleteMarker(int markerId)
        {
            var iMarkerId = new SqlParameter("@iMarkerId", markerId)
            {
                SqlDbType = SqlDbType.BigInt
            };
            try
            {
                ExecuteStoredProg(@"[dbo].[MA_MarkerDelete]
	                                @iMarkerId = @iMarkerId",
                    iMarkerId);
            }
            catch (Exception e)
            {
                throw e;
            }

            return true;
        }

        public IEnumerable<BaTypesStatesCreateView> GetAllStates()
        {
            return DataContext.EFSOStates.Select(x =>
                    new BaTypesStatesCreateView { StateId = x.StateId, StateName = x.Name })
                .ToList();
        }

        public BaTypesStatesCreateView CreateUpdateState(BaTypesStatesCreateView state)
        {
            var iStateId = new SqlParameter("@iStateId", state.StateId ?? (object)DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.Int
            };

            var sName = new SqlParameter("@sName", state.StateName)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            
            ExecuteStoredProg(@"[dbo].[SO_StateUpdate]
	                                @iStateId = @iStateId OUT,
                                    @sName = @sName",
                iStateId,
                sName);

            return GetAllStates().FirstOrDefault(x => x.StateId == (int)iStateId.Value);
        }

        public bool DeleteState(int stateId)
        {
            var iStateId = new SqlParameter("@iStateId", stateId)
            {
                SqlDbType = SqlDbType.BigInt
            };
            
            try
            {
                ExecuteStoredProg(@"[dbo].[SO_StateDelete]
	                                @iStateId = @iStateId",
                    iStateId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return true;
        }

        public BaTypesStatesView AddStateToBaType(int baId, BaTypesStatesView state)
        {
            var iBATypeId = new SqlParameter("@iBATypeId", baId)
            {
                SqlDbType = SqlDbType.Int
            };

            var iStateId = new SqlParameter("@iStateId", state.StateId)
            {
                SqlDbType = SqlDbType.Int
            };

            var bIsFirstState = new SqlParameter("@bIsFirstState", state.IsFirstState)
            {
                SqlDbType = SqlDbType.Bit
            };

            var bIsDelState = new SqlParameter("@bIsDelState", state.IsDelState)
            {
                SqlDbType = SqlDbType.Bit
            };

            var sName = new SqlParameter("@sName", (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };

            ExecuteStoredProg(@"[dbo].[SOBA_TypeStateUpdate]
                                    @iBATypeId = @iBATypeId,
                                    @iStateId = @iStateId,
                                    @bIsFirstState = @bIsFirstState,
                                    @bIsDelState = @bIsDelState,
                                    @sName = @sName",
                iBATypeId,
                iStateId,
                bIsFirstState,
                bIsDelState,
                sName);

            return GetAvailableSteteForEntiti(baId).FirstOrDefault(x => x.StateId == (int) iStateId.Value);
        }

        public bool DeleteStateFromBaType(int baId, int stateId)
        {
            var iBATypeId = new SqlParameter("@iBATypeId", baId)
            {
                SqlDbType = SqlDbType.Int
            };

            var iStateId = new SqlParameter("@iStateId", stateId)
            {
                SqlDbType = SqlDbType.Int
            };

            try
            {
                ExecuteStoredProg(@"[dbo].[SOBA_TypeStateDelete]
                                    @iBATypeId = @iBATypeId,
                                    @iStateId = @iStateId",
                        iBATypeId,
                        iStateId);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public BaTypesOperationsView AddOperationToBaType(int baId, BaTypesOperationsView operation)
        {
            var iBATypeId = new SqlParameter("@iBATypeId", baId)
            {
                SqlDbType = SqlDbType.Int
            };

            var iOperationId = new SqlParameter("@iOperationId", operation.OperationId)
            {
                SqlDbType = SqlDbType.Int
            };

            var bIsCreateOperation = new SqlParameter("@bIsCreateOperation", operation.IsCreateOperation)
            {
                SqlDbType = SqlDbType.Bit
            };

            var bIsDeleteOperation = new SqlParameter("@bIsDeleteOperation", operation.IsDeleteOperation)
            {
                SqlDbType = SqlDbType.Bit
            };

            var sName = new SqlParameter("@sName", (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };

            ExecuteStoredProg(@"[dbo].[SOBA_TypeOperationUpdate]
                                    @iBATypeId = @iBATypeId,
                                    @iOperationId = @iOperationId,
                                    @bIsCreateOperation = @bIsCreateOperation,
                                    @bIsDeleteOperation = @bIsDeleteOperation,
                                    @sName = @sName",
                iBATypeId,
                iOperationId,
                bIsCreateOperation,
                bIsDeleteOperation,
                sName);

            return GetAvailableOperationsForEntiti(baId).FirstOrDefault(x => x.OperationId == (int)iOperationId.Value);
        }

        public bool DeleteOperationFromBaType(int baId, int operationId)
        {
            var iBATypeId = new SqlParameter("@iBATypeId", baId)
            {
                SqlDbType = SqlDbType.Int
            };

            var iStateId = new SqlParameter("@iOperationId", operationId)
            {
                SqlDbType = SqlDbType.Int
            };

            try
            {
                ExecuteStoredProg(@"[dbo].[SOBA_TypeOperationDelete]
                                    @iBATypeId = @iBATypeId,
                                    @iOperationId = @iOperationId",
                    iBATypeId,
                    iStateId);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }


        public BaTypesOperationsStateView AddOperationStateToBaType(int baId, BaTypesOperationsStateCreateView operationState)
        {
            var iBATypeId = new SqlParameter("@iBATypeId", baId)
            {
                SqlDbType = SqlDbType.Int
            };

            var iOperationId = new SqlParameter("@iOperationId", operationState.Operation.OperationId)
            {
                SqlDbType = SqlDbType.Int
            };

            var iSrcStateId = new SqlParameter("@iSrcStateId", operationState.StartState.StateId)
            {
                SqlDbType = SqlDbType.Int
            };

            var iDstStateId = new SqlParameter("@iDstStateId", operationState.EndState.StateId)
            {
                SqlDbType = SqlDbType.Int
            };

            var bIsBlocked = new SqlParameter("@bIsBlocked", operationState.IsBlocked)
            {
                SqlDbType = SqlDbType.Bit
            };

            var iRightId = new SqlParameter("@iRightId", (object) DBNull.Value)
            {
                SqlDbType = SqlDbType.Int
            };

            ExecuteStoredProg(@"[dbo].[SOBA_TypeStateOperationUpdate]
                                    @iBATypeId = @iBATypeId,
                                    @iOperationId = @iOperationId,
                                    @iSrcStateId = @iSrcStateId,
                                    @iDstStateId = @iDstStateId,
                                    @bIsBlocked = @bIsBlocked,
                                    @iRightId = @iRightId",
                iBATypeId,
                iOperationId,
                iSrcStateId,
                iDstStateId,
                bIsBlocked,
                iRightId);

            return GetAvailablOperationsStateForEntiti(baId)
                .FirstOrDefault(x => x.OperationId == operationState.Operation.OperationId);

            throw new NotImplementedException();
        }

        public bool DeleteOperationStateToBaType(int baId, int operationStateId, int srcStateId)
        {
            var iBATypeId = new SqlParameter("@iBATypeId", baId)
            {
                SqlDbType = SqlDbType.Int
            };

            var iOperationId = new SqlParameter("@iOperationId", operationStateId)
            {
                SqlDbType = SqlDbType.Int
            };

            var iSrcStateId = new SqlParameter("@iSrcStateId", srcStateId)
            {
                SqlDbType = SqlDbType.Int
            };

            try
            {
                ExecuteStoredProg(@"[dbo].[SOBA_TypeStateOperationDelete]
                                    @iBATypeId = @iBATypeId,
                                    @iOperationId = @iOperationId,
                                    @iSrcStateId = @iSrcStateId",
                    iBATypeId,
                    iOperationId,
                    iSrcStateId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public IEnumerable<BaTypesOperationsCreateView> GetAllOperations()
        {
            return DataContext.EFSOOperations.Select(x =>
                    new BaTypesOperationsCreateView { OperationId = x.OperationId, OperationName = x.Name })
                .ToList();
        }


        public BaTypesOperationsCreateView CreateUpdateOperations(BaTypesOperationsCreateView operations)
        {
            var iOperationId = new SqlParameter("@iOperationId", operations.OperationId ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.InputOutput
            };

            var sName = new SqlParameter("@sName", operations.OperationName)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };
            
            ExecuteStoredProg(@"[dbo].[SO_OperationUpdate]
	                                @iOperationId = @iOperationId OUT,
                                    @sName = @sName",
                iOperationId,
                sName);

            return GetAllOperations().FirstOrDefault(x => x.OperationId == (int)iOperationId.Value);
        }

        public bool DeleteOperations(int operationsId)
        {
            var iOperationId = new SqlParameter("@iOperationId", operationsId)
            {
                SqlDbType = SqlDbType.BigInt
            };
            
            try
            {
                ExecuteStoredProg(@"[dbo].[SO_OperationDelete]
	                                @iOperationId = @iOperationId",
                    iOperationId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public ItemTableView CreateUpdateListItem(int markerId, ItemTableView listItem)
        {
            var iMarkerId = new SqlParameter("@iMarkerId", markerId)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.BigInt
            };
            var iItemId = new SqlParameter("@iItemId", listItem.ItemId ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.InputOutput
            };
            var sItemName = new SqlParameter("@sItemName", string.IsNullOrEmpty(listItem.ItemName)
                ? (object)DBNull.Value
                : listItem.ItemName)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };

            var iOrderNum = new SqlParameter("@iOrderNum", listItem.OrderNumber == 0 
                ? (object)DBNull.Value 
                : listItem.OrderNumber)
            {
                SqlDbType = SqlDbType.Int
            };

            var bIsActive = new SqlParameter("@bIsActive", listItem.IsActive)
            {
                SqlDbType = SqlDbType.Bit
            };
            
            ExecuteStoredProg(@"[dbo].[MA_MarkerValueListUpdate]
                                    @iMarkerId = @iMarkerId,
	                                @iItemId = @iItemId OUT,
	                                @sItemName = @sItemName,
	                                @vValue0 = NULL,
                                    @vValue1 = NULL,
                                    @vValue2 = NULL,
                                    @vValue3 = NULL,
                                    @vValue4 = NULL,
                                    @vValue5 = NULL,
	                                @iOrderNum = @iOrderNum,
	                                @bIsActive = @bIsActive",
                    iMarkerId,
                    iItemId,
                    sItemName,
                    iOrderNum,
                    bIsActive);

            return GetAvailableValuesForList(markerId).FirstOrDefault(x => x.ItemId == (int) iItemId.Value);
        }

        public bool DeleteListItem(int markerId, int listItemId)
        {
            var iMarkerId = new SqlParameter("@iMarkerId", markerId)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.BigInt
            };
            var iItemId = new SqlParameter("@iItemId", listItemId)
            {
                SqlDbType = SqlDbType.Int
            };

            try
            {
                ExecuteStoredProg(@"[dbo].[MA_MarkerValueListDelete]
	                                @iMarkerId = @iMarkerId,
                                    @iItemId = @iItemId",
                    iMarkerId, 
                    iItemId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

    }
}
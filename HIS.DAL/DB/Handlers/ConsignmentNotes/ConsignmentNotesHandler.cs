﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.Client.Models.ConsignmentNotes;

namespace HIS.DAL.DB.Handlers.ConsignmentNotes
{
    public class ConsignmentNotesHandler : DataContextHandlerBase, IConsignmentNotesHandler
    {
        public IEnumerable<ConsignmentNotesDocument> GetConsignmentNotesDocuments()
        {
            return DataContext.RIReceiptInvoices
                .Join(DataContext.EFSOStates, 
                    invoices => invoices.StateId, 
                    state => state.StateId, (invoices, state) =>
                    new ConsignmentNotesDocument
                    {
                        BAId = invoices.BAId,
                        Note = invoices.Note,
                        Number = invoices.Number,
                        CostVAT = invoices.CostVAT,
                        VAT = invoices.VAT,
                        Cost = invoices.Cost,
                        StateId = invoices.StateId,
                        StateDisplay = state.Name,
                        DocDate = invoices.DocDate,
                        BaTypeId = invoices.BATypeId,
                        ContractName = invoices.CAContracts.Number,
                        CompanyAreaName = invoices.LSCompanyAreas.Name,
                        LegalSubjectsName = invoices.LSLegalSubjects.FullName,
                        UserName = invoices.HISUsers.Id.ToString(),
                        WarehouseName = invoices.WHWarehouses.Name
                    }).ToList();
        }

        public ConsignmentNotesDocument GetConsignmentNotesDocument(long baId)
        {
            return DataContext.RIReceiptInvoices
                .Join(DataContext.EFSOStates,
                    invoices => invoices.StateId,
                    state => state.StateId, (invoices, state) =>
                        new ConsignmentNotesDocument
                        {
                            BAId = invoices.BAId,
                            Note = invoices.Note,
                            Number = invoices.Number,
                            CostVAT = invoices.CostVAT,
                            VAT = invoices.VAT,
                            Cost = invoices.Cost,
                            StateId = invoices.StateId,
                            StateDisplay = state.Name,
                            DocDate = invoices.DocDate,
                            BaTypeId = invoices.BATypeId,
                            ContractName = invoices.CAContracts.Number,
                            CompanyAreaName = invoices.LSCompanyAreas.Name,
                            LegalSubjectsName = invoices.LSLegalSubjects.FullName,
                            UserName = invoices.HISUsers.Id.ToString(),
                            WarehouseName = invoices.WHWarehouses.Name
                        }).FirstOrDefault(x => x.BAId == baId);
        }

        public ConsignmentNotesFullDocument GetConsignmentNotesFullDocument(long baId)
        {
            return DataContext.RIReceiptInvoices
                .Join(DataContext.EFSOStates,
                    invoices => invoices.StateId,
                    state => state.StateId, (invoices, state) =>
                        new ConsignmentNotesFullDocument
                        {
                            BAId = invoices.BAId,
                            Note = invoices.Note,
                            Number = invoices.Number,
                            CostVAT = invoices.CostVAT,
                            VAT = invoices.VAT,
                            Cost = invoices.Cost,
                            StateId = invoices.StateId,
                            StateDisplay = state.Name,
                            DocDate = invoices.DocDate,
                            BaTypeId = invoices.BATypeId,
                            ContractName = invoices.CAContracts.Number,
                            ContractId = invoices.ContractId.ToString(),
                            CompanyAreaName = invoices.LSCompanyAreas.Name,
                            CompanyAreaId = invoices.CAId.ToString(),
                            LegalSubjectsName = invoices.LSLegalSubjects.FullName,
                            LegalSubjectId = invoices.LSId.ToString(),
                            UserName = invoices.HISUsers.Id.ToString(),
                            WarehouseName = invoices.WHWarehouses.Name,
                            WhWarehousesId = invoices.WHId.ToString()
                        }).FirstOrDefault(x => x.BAId == baId);
        }

        public ConsignmentNotesDocument SaveConsignmentNotesDocument(ConsignmentNotesDocumentUpdate consignmentNotesDocumentUpdate)
        {
            #region Params
            var ibaId = new SqlParameter("@iBAId", consignmentNotesDocumentUpdate.BAId ?? (object)DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.BigInt
            };

            var sNumber = new SqlParameter("@sNumber", consignmentNotesDocumentUpdate.Number)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };

            var dDocDate = new SqlParameter("@dDocDate ", consignmentNotesDocumentUpdate.DocDate ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.Date,
            };

            var iCAId = new SqlParameter("@iCAId", consignmentNotesDocumentUpdate.CAId)
            {
                SqlDbType = SqlDbType.BigInt
            };

            var iContractId = new SqlParameter("@iContractId ", consignmentNotesDocumentUpdate.ContractId ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.BigInt
            };

            var iLSId = new SqlParameter("@iLSId", consignmentNotesDocumentUpdate.LSId)
            {
                SqlDbType = SqlDbType.BigInt
            };

            var iWHId = new SqlParameter("@iWHId", consignmentNotesDocumentUpdate.WHId)
            {
                SqlDbType = SqlDbType.BigInt
            };

            var sNote = new SqlParameter("@sNote", consignmentNotesDocumentUpdate.Note ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.NVarChar,
                Size = 400
            };

            var fCost = new SqlParameter("@fCost", consignmentNotesDocumentUpdate.Cost ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.Money
            };

            var fVAT = new SqlParameter("@fVAT", consignmentNotesDocumentUpdate.VAT ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.Money
            };

            var fCostVAT = new SqlParameter("@fCostVAT", consignmentNotesDocumentUpdate.CostVAT ?? (object)DBNull.Value)
            {
                SqlDbType = SqlDbType.Money
            };
            #endregion

            ExecuteStoredProg(@"[dbo].[RI_ReceiptInvoicesUpdate]
	                                @iBAId = @iBAId OUT,
	                                @sNumber = @sNumber,
	                                @dDocDate = @dDocDate,
	                                @iCAId = @iCAId,
	                                @iContractId = @iContractId,
	                                @iLSId = @iLSId,
	                                @iWHId = @iWHId,
	                                @sNote = @sNote,
	                                @fCost = @fCost,
	                                @fVAT = @fVAT,
	                                @fCostVAT = @fCostVAT",
                                    ibaId,
                                    sNumber,
                                    dDocDate,
                                    iCAId,
                                    iContractId,
                                    iLSId, 
                                    iWHId,
                                    sNote,
                                    fCost,
                                    fVAT,
                                    fCostVAT);

            return GetConsignmentNotesDocument((long)ibaId.Value);
        }

        

        public IEnumerable<ConsignmentNotesSpecifications> GetConsignmentNotesSpecificationByDocument(long docId)
        {
            return DataContext.RIReceiptInvoiceRows.Where(x => x.DocId == docId)
                .Select(x => new ConsignmentNotesSpecifications
                {
                    PVAT = x.PVAT,
                    CostVAT = x.CostVAT,
                    VAT = x.VAT,
                    Cost = x.Cost,
                    RecId = x.RecId,
                    IsVATIncluded = x.IsVATIncluded,
                    Price = x.Price,
                    PriceVAT = x.PriceVAT,
                    Quantity = x.Quantity,
                    VATRate = x.VATRate,
                    UnitName = x.RefSysUnits.Name,
                    InventoryName = x.IInventory.Name,
                    ConsignmentNotesDocumentName = x.RIReceiptInvoices.Number
                }).ToList();
        }

        public ConsignmentNotesSpecifications SaveConsignmentNotesSpecification(ConsignmentNotesSpecificationsUpdate consignmentNotesSpecificationsUpdate)
        {
            #region Params
            var iRecId = new SqlParameter("@iRecId", consignmentNotesSpecificationsUpdate.RecId ?? (object)DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.BigInt
            };

            var iInventoryId = new SqlParameter("@iInventoryId ", consignmentNotesSpecificationsUpdate.InventoryId)
            {
                SqlDbType = SqlDbType.BigInt,
            };

            var iDocId = new SqlParameter("@iDocId", consignmentNotesSpecificationsUpdate.DocId)
            {
                SqlDbType = SqlDbType.BigInt
            };

            var iUnitId = new SqlParameter("@iUnitId", consignmentNotesSpecificationsUpdate.UnitId)
            {
                SqlDbType = SqlDbType.BigInt
            };

            var fQuantity = new SqlParameter("@fQuantity", consignmentNotesSpecificationsUpdate.Quantity)
            {
                SqlDbType = SqlDbType.BigInt
            };

            var mPrice = new SqlParameter("@mPrice", consignmentNotesSpecificationsUpdate.Price)
            {
                SqlDbType = SqlDbType.Money
            };

            var mPriceVAT = new SqlParameter("@mPriceVAT", consignmentNotesSpecificationsUpdate.PriceVAT)
            {
                SqlDbType = SqlDbType.Money
            };

            var bIsVATIncluded = new SqlParameter("@bIsVATIncluded", consignmentNotesSpecificationsUpdate.IsVATIncluded)
            {
                SqlDbType = SqlDbType.Bit
            };

            var iVATRate = new SqlParameter("@iVATRate", consignmentNotesSpecificationsUpdate.VATRate)
            {
                SqlDbType = SqlDbType.BigInt
            };

            var mCost = new SqlParameter("@mCost", consignmentNotesSpecificationsUpdate.Cost)
            {
                SqlDbType = SqlDbType.Money
            };

            var mVAT = new SqlParameter("@mVAT", consignmentNotesSpecificationsUpdate.VAT)
            {
                SqlDbType = SqlDbType.Money
            };

            var mPVAT = new SqlParameter("@mPVAT", consignmentNotesSpecificationsUpdate.PVAT)
            {
                SqlDbType = SqlDbType.Money
            };

            var mCostVAT = new SqlParameter("@mCostVAT", consignmentNotesSpecificationsUpdate.CostVAT)
            {
                SqlDbType = SqlDbType.Money
            };
            #endregion

            ExecuteStoredProg(@"[dbo].[RI_ReceiptInvoiceRowsUpdate]
	                                @iRecId = @iRecId OUT,
	                                @iInventoryId = @iInventoryId,
	                                @iDocId = @iDocId,
	                                @iUnitId = @iUnitId,
	                                @fQuantity = @fQuantity,
	                                @mPrice = @mPrice,
	                                @mPriceVAT = @mPriceVAT,
	                                @bIsVATIncluded = @bIsVATIncluded,
	                                @iVATRate = @iVATRate,
	                                @mCost = @mCost,
	                                @mVAT = @mVAT,
	                                @mCostVAT = @mCostVAT,
                                    @mPVAT = @mPVAT",
                                    iRecId,
                                    iInventoryId,
                                    iDocId,
                                    iUnitId,
                                    fQuantity,
                                    mPrice,
                                    mPriceVAT,
                                    bIsVATIncluded,
                                    iVATRate,
                                    mCost,
                                    mVAT,
                                    mCostVAT, 
                                    mPVAT);

            return GetConsignmentNotesSpecification((long)iRecId.Value, (long)iDocId.Value);
        }

        public bool DeleteConsignmentNotesSpecification(long recId, long docId)
        {
            var iRecId = new SqlParameter("@iRecId", recId)
            {
                SqlDbType = SqlDbType.BigInt
            };
            var iDocId = new SqlParameter("@iDocId", docId)
            {
                SqlDbType = SqlDbType.BigInt
            };
            try
            {
                //todo: доабвить docid
                ExecuteStoredProg(@"[dbo].[RI_ReceiptInvoiceRowsDelete]
                                       @iRecId = @iRecId,
                                       @iDocId = @iDocId", iRecId, iDocId);
            }
            catch (Exception)
            {
                return false;
            }
            
            return true;
        }

       

        public ConsignmentNotesSpecifications GetConsignmentNotesSpecification(long recId, long docId)
        {
            //TODO: Добавить docId как ключ
            return DataContext.RIReceiptInvoiceRows.Where(x => x.RecId == recId && x.DocId == docId)
                .Select(x => new ConsignmentNotesSpecifications
                {
                    CostVAT = x.CostVAT,
                    VAT = x.VAT,
                    Cost = x.Cost,
                    RecId = x.RecId,
                    IsVATIncluded = x.IsVATIncluded,
                    Price = x.Price,
                    PriceVAT = x.PriceVAT,
                    Quantity = x.Quantity,
                    VATRate = x.VATRate,
                    UnitName = x.RefSysUnits.Name,
                    InventoryName = x.IInventory.Name,
                    ConsignmentNotesDocumentName = x.RIReceiptInvoices.Number,
                    PVAT = x.PVAT
                }).FirstOrDefault();
        }
        public ConsignmentNotesSpecificationsFull GetConsignmentNotesSpecificationFull(long recId, long docId)
        {
            return DataContext.RIReceiptInvoiceRows.Where(x => x.RecId == recId && x.DocId == docId)
                .Select(x => new ConsignmentNotesSpecificationsFull
                {
                    CostVAT = x.CostVAT,
                    VAT = x.VAT,
                    Cost = x.Cost,
                    RecId = x.RecId,
                    IsVATIncluded = x.IsVATIncluded,
                    Price = x.Price,
                    PriceVAT = x.PriceVAT,
                    Quantity = x.Quantity,
                    VATRate = x.VATRate,
                    UnitName = x.RefSysUnits.Name,
                    UnitId = x.UnitId,
                    InventoryName = x.IInventory.Name,
                    InventoryId = x.InventoryId,
                    ConsignmentNotesDocumentName = x.RIReceiptInvoices.Number,
                    DocId = x.DocId,
                    PVAT = x.PVAT
                }).FirstOrDefault();
        }
    }
}

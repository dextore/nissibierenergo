﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.ConsignmentNotes;

namespace HIS.DAL.DB.Handlers.ConsignmentNotes
{
    public interface IConsignmentNotesHandler : IDataContextHandler
    {
        IEnumerable<ConsignmentNotesDocument> GetConsignmentNotesDocuments();
        ConsignmentNotesDocument GetConsignmentNotesDocument(long baId);
        IEnumerable<ConsignmentNotesSpecifications> GetConsignmentNotesSpecificationByDocument(long docId);
        ConsignmentNotesSpecifications GetConsignmentNotesSpecification(long recId, long docId);
        ConsignmentNotesSpecificationsFull GetConsignmentNotesSpecificationFull(long recId, long docId);
        ConsignmentNotesDocument SaveConsignmentNotesDocument(ConsignmentNotesDocumentUpdate consignmentNotesDocumentUpdate);
        ConsignmentNotesSpecifications SaveConsignmentNotesSpecification(ConsignmentNotesSpecificationsUpdate consignmentNotesSpecificationsUpdate);
        bool DeleteConsignmentNotesSpecification(long recId, long docId);
        ConsignmentNotesFullDocument GetConsignmentNotesFullDocument(long baId);
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.DB.Handlers.Markers
{
    public class OptionalMarkerHandler: DataContextHandlerBase, IOptionalMarkerHandler
    {
        public IEnumerable<OptionalMarkersInfo> GetOptionalMarkers(long baTypeId)
        {
            return DataContext.MABATypeOptionalMarkers.Where(m => m.BATypeId == baTypeId)
                .GroupBy(g => new {g.MarkerId, g.IsVisible} ).Select(m => new OptionalMarkersInfo
                {
                    BATypeId = baTypeId,
                    MarkerId = m.Key.MarkerId,
                    IsVisible = m.Key.IsVisible,
                    Optionals = m.Select(n => new OptionalMarker
                    {
                        MarkerId = n.OptionalMarkerId,
                        Items = n.MABATypeOptionalMarkerItems.Select(i => new OptionalItem
                        {
                            IsVisible = i.IsVisible,
                            ItemId = i.ItemId
                        })
                    })
                }).ToArray();
        }
    }
}

﻿namespace HIS.DAL.DB.Handlers.Markers
{
    public interface ICatalogMarkerValueItemsHandler: IDataContextHandler
    {
        /// <summary>
        /// Для маркеров с типом 13 "Значения этого типа ссылается на справочник"
        /// Возвращает JSON строку со списком значений.
        /// </summary>
        /// <param name="markerId"></param>
        /// <returns></returns>
        string GetValueItems(int markerId);
    }
}

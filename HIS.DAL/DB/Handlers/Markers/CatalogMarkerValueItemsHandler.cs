﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace HIS.DAL.DB.Handlers.Markers
{
    public class CatalogMarkerValueItemsHandler: DataContextHandlerBase, ICatalogMarkerValueItemsHandler
    {
        public string GetValueItems(int markerId)
        {
            // Получить представление 
            var viewName = DataContext.EFMAMarkers.Where(m => m.MarkerId == markerId).Select(n => n.ImplementTypeName).FirstOrDefault();
            if (string.IsNullOrEmpty(viewName))
                return null;

            // Получить данные из представления

            var ssql = $"SELECT * FROM {viewName} ORDER BY OrderNum ";

            var cmd = DataContext.Database.Connection.CreateCommand();
            cmd.CommandText = ssql;
            if (DataContext.Database.CurrentTransaction != null)
                cmd.Transaction = DataContext.Database.CurrentTransaction.UnderlyingTransaction;

            var sb = new StringBuilder();
            sb.Append("[");

            using (var reader = cmd.ExecuteReader(CommandBehavior.SequentialAccess))
            {
                var fields = new Dictionary<string, object>();
                var fieldTypes = new Dictionary<string, Type>();

                var delim = "";
                while (reader.Read())
                {
                    for (var i = 0; i < reader.FieldCount; ++i)
                    {
                        fields[reader.GetName(i)] = reader[i];
                        fieldTypes[reader.GetName(i)] = reader.GetFieldType(i);
                    }

                    var delumInternal = "";
                    sb.Append(delim+"{");
                    foreach (KeyValuePair<string, object> kvp in fields)
                    {
                        var value = kvp.Value == DBNull.Value 
                            ? "null" 
                            : HttpUtility.JavaScriptStringEncode(kvp.Value.ToString());
                        if (fieldTypes[kvp.Key] == typeof(string))
                            value = '"'+value+'"';
                        if (fieldTypes[kvp.Key] == typeof(bool))
                            value = value.ToLower();
                        var name =  '"'+kvp.Key+'"';
                        sb.Append($"{delumInternal} {name} : {value}");
                        delumInternal = ",";
                    }
                    sb.Append("}");
                    delim = ",";
                }
            }

            sb.Append("]");
            return sb.ToString();
        }
    }
}

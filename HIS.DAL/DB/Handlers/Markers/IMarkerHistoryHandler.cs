﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.DB.Handlers.Markers
{
    public interface IMarkerHistoryHandler: IDataContextHandler
    {
        IEnumerable<PeriodicMarkerValue> GetPeriodicMarkerHistory(long baId, int markerId);
    }
}

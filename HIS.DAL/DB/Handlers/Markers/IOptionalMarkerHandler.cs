﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.DB.Handlers.Markers
{
    public interface IOptionalMarkerHandler : IDataContextHandler
    {
        IEnumerable<OptionalMarkersInfo> GetOptionalMarkers(long baTypeId);
    }
}

﻿using System;
using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.DB.Handlers.Markers
{
    public interface IEntityMarkersHandler: IDataContextHandler
    {
        void UpdateMarkerValues(string entityMVCAlias, long baId, IEnumerable<MarkerValue> markerValues);
        void UpdateMarkerValue(string entityMVCAlias, long baId, MarkerValue markerValue);
        void DeleteMarkerValue(string entityMVCAlias, long baId, MarkerValue markerValue);
    }
}
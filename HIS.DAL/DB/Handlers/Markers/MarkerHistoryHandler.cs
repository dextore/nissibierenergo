﻿using System.Collections.Generic;
using System.Linq;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.DB.Handlers.Markers
{
    public class MarkerHistoryHandler: DataContextHandlerBase, IMarkerHistoryHandler
    {
        public IEnumerable<PeriodicMarkerValue> GetPeriodicMarkerHistory(long baId, int markerId)
        {
            var typeId = DataContext.EFMAMarkers.FirstOrDefault(m => m.MarkerId == markerId).TypeId;

            string sSql = "";

            switch (typeId)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    sSql = GetSql(baId, markerId);
                    break;
                case 5:
                    sSql = GetListSql(baId, markerId);
                    break;
                case 11:
                    sSql = GetEntityRefSql(baId, markerId);
                    break;
                case 12:
                    sSql = GetAddressSql(baId, markerId);
                    break;
                case 13:
                    sSql = GetCatalogSql(baId, markerId);
                    break;
            }

            return string.IsNullOrEmpty(sSql) ? null : DataContext.Database.SqlQuery<PeriodicMarkerValue>(sSql).ToList();
        }

        private string GetSql(long baId, int markerId)
        {
            var sSql = $@"SELECT SSQL = N'SELECT m.[StartDate], m.[EndDate], [Value] = m.[sValue], m.[Note], [DisplayValue] = '''' 
                                   FROM [dbo].['+ISNULL(tm.ImplementTypeName, 'MABaseAncestorMarkerValuePeriods')+'] AS m
                                   WHERE m.MarkerId = {markerId} AND m.BAId = {baId}'
                       FROM [dbo].[MABATypeMarkers] tm
                       INNER JOIN MAMarkers AS m ON m.MarkerId = tm.MarkerId
                       WHERE m.TypeId IN (1,2,3,4,6,10) 
                         AND tm.IsPeriodic = 1
                         AND m.MarkerId = {markerId}";

            return DataContext.Database.SqlQuery<string>(sSql).FirstOrDefault();
        }

        private string GetListSql(long baId, int markerId)
        {
            var sSql = $@"SELECT SSQL = N'SELECT m.[StartDate], m.[EndDate], [Value] = m.[sValue], m.[Note], [DisplayValue] = mvl.ItemName '
                         +' FROM [dbo].['+ISNULL(tm.ImplementTypeName, 'MABaseAncestorMarkerValuePeriods')+'] AS m '
                         +' LEFT JOIN [dbo].[MAMarkerValueList] mvl ON mvl.MarkerId = m.MarkerId AND mvl.ItemId = CAST(m.[Value] As INT) '
                         +' WHERE m.MarkerId = {markerId} AND m.BAId = {baId}'
                            FROM [dbo].[MABATypeMarkers] tm
                            INNER JOIN MAMarkers AS m ON m.MarkerId = tm.MarkerId
                            WHERE m.TypeId = 5 
                              AND tm.IsPeriodic = 1
                              AND m.MarkerId = {markerId}";

            return DataContext.Database.SqlQuery<string>(sSql).FirstOrDefault();
        }

        private string GetEntityRefSql(long baId, int markerId)
        {
            var sSql = $@"SELECT SSQL = N'SELECT m.[StartDate], m.[EndDate], [Value] = m.[sValue], m.[Note], [DisplayValue] = '
              +CASE 
               WHEN btm.ImplementTypeField IS NOT NULL THEN 'b.['+btm.ImplementTypeField+']'
               ELSE ' bamv.[sValue]'
               END 
              +' FROM [dbo].['+ISNULL(tm.ImplementTypeName, 'MABaseAncestorMarkerValuePeriods')+'] AS m' 
              +CASE 
               WHEN btm.ImplementTypeField IS NOT NULL THEN ' LEFT JOIN [dbo].['+mb.ImplementTypeName+'] b ON b.BAId = CASE WHEN ISNUMERIC(CONVERT(NVARCHAR(MAX), m.Value)) > 0 THEN CAST(m.Value AS BIGINT) ELSE 0 END'
               ELSE ' LEFT JOIN [dbo].[MABaseAncestorMarkerValues] bamv ON bamv.MarkerId = 1 AND bamv.BAId = m.BAId' 
               END
              + ' WHERE m.MarkerId = {markerId} AND m.BAId = {baId}'
                FROM [dbo].[MABATypeMarkers] tm
                INNER JOIN MAMarkers AS m ON m.MarkerId = tm.MarkerId
                LEFT JOIN BATypes AS mb ON mb.TypeId = m.BATypeId
                LEFT JOIN [dbo].[MABATypeMarkers] btm ON btm.BATypeId = mb.TypeId AND btm.MarkerId = 1
                LEFT JOIN [dbo].[MAMarkers] bm ON bm.MarkerId = btm.MarkerId
                WHERE m.TypeId = 11 
                  AND tm.IsPeriodic = 1
                  AND m.MarkerId = {markerId}";

            return DataContext.Database.SqlQuery<string>(sSql).FirstOrDefault();
        }

        private string GetAddressSql(long baId, int markerId)
        {
            var sSql = $@"SELECT SSQL = N'SELECT m.[StartDate], m.[EndDate], [Value] = m.[sValue], m.[Note], [DisplayValue] = a.[Name] '
              +' FROM [dbo].['+ISNULL(tm.ImplementTypeName, 'MABaseAncestorMarkerValuePeriods')+'] AS m '
              +' LEFT JOIN [dbo].[AOAddresses] a ON a.BAId = CASE WHEN ISNUMERIC(CONVERT(NVARCHAR(MAX), m.Value)) > 0 THEN CAST(m.Value AS BIGINT) ELSE 0 END'
              +' WHERE m.MarkerId = {markerId} AND m.BAId = {baId}'
                FROM [dbo].[MABATypeMarkers] tm
                INNER JOIN MAMarkers AS m ON m.MarkerId = tm.MarkerId
                WHERE m.TypeId = 12 
                  AND tm.IsPeriodic = 1 
                  AND m.MarkerId = {markerId}";

            return DataContext.Database.SqlQuery<string>(sSql).FirstOrDefault();
        }

        private string GetCatalogSql(long baId, int markerId)
        {
            var sSql = $@"SELECT SSQL = N'SELECT m.[StartDate], m.[EndDate], [Value] = m.[sValue], m.[Note], [DisplayValue] = r.[ItemName] '
              +' FROM [dbo].['+ISNULL(tm.ImplementTypeName, 'MABaseAncestorMarkerValuePeriods')+'] AS m '
              +' LEFT JOIN [dbo].'+m.ImplementTypeName+' r ON r.ItemId = CASE WHEN ISNUMERIC(CONVERT(NVARCHAR(MAX), m.Value)) > 0 THEN CAST(m.Value AS BIGINT) ELSE 0 END'
              +' WHERE m.MarkerId = {markerId} AND m.BAId = {baId}'
                FROM [dbo].[MABATypeMarkers] tm
                INNER JOIN MAMarkers AS m ON m.MarkerId = tm.MarkerId
                WHERE m.TypeId = 13 
                  AND tm.IsPeriodic = 1
                  AND m.MarkerId = {markerId}";

            return DataContext.Database.SqlQuery<string>(sSql).FirstOrDefault();
        }
    }
}

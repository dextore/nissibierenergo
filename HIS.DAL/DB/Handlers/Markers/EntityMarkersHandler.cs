﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.DB.Common;

namespace HIS.DAL.DB.Handlers.Markers
{
    public class EntityMarkersHandler : DataContextHandlerBase, IEntityMarkersHandler
    {

        public void UpdateMarkerValues(string entityMVCAlias, long baId, IEnumerable<MarkerValue> markerValues)
        {
            // delete!!! 
            // Update 
            foreach (var marker in markerValues)
            {
                if (marker.IsDeleted)
                {
                    DeleteMarkerValue(entityMVCAlias, baId, marker);
                    continue;
                }
                UpdateMarkerValue(entityMVCAlias, baId, marker);
            }
        }

        public void UpdateMarkerValue(string entityMVCAlias, long baId, MarkerValue markerValue)
        {
            var procName = EntitytProgsStorage.GetInfo(entityMVCAlias).UpdateMarkerProg;

            var iBaId = new SqlParameter("@iBAId", baId)
            {
                Direction = ParameterDirection.Input,
                SqlDbType = SqlDbType.BigInt
            };

            var iMarkerId = new SqlParameter("@iMarkerId", markerValue.MarkerId)
            {
                Direction = ParameterDirection.Input,
                SqlDbType = SqlDbType.BigInt
            };

            var dStartDate =
                new SqlParameter("@dStartDate", markerValue.StartDate.HasValue 
                    ? markerValue.StartDate.Value
                    : (object)DBNull.Value)
                {
                    Direction = ParameterDirection.Input,
                    SqlDbType = SqlDbType.DateTime
                };

            var dEndDate = new SqlParameter("@dEndDate", markerValue.EndDate.HasValue
                ? markerValue.EndDate.Value
                : (object) DBNull.Value)
            {
                Direction = ParameterDirection.InputOutput,
                SqlDbType = SqlDbType.DateTime
            };

            var vValue = new SqlParameter("@vValue", string.IsNullOrEmpty(markerValue.Value) 
                ? (object)DBNull.Value
                : markerValue.Value)
            {
                Direction = ParameterDirection.Input,
                SqlDbType = SqlDbType.Variant
            };

            var sNote = new SqlParameter("@sNote", string.IsNullOrEmpty(markerValue.Note)
                ? ""
                : markerValue.Note)
            {
                Direction = ParameterDirection.Input,
                SqlDbType = SqlDbType.VarChar
            };

            ExecuteStoredProg($@"{procName} 
                              @iBAId = @iBAId,
                              @iMarkerId = @iMarkerId,
                              @dStartDate = @dStartDate,
                              @dEndDate = @dEndDate OUT,
                              @vValue = @vValue,
                              @sNote = @sNote",
                iBaId,
                iMarkerId,
                dStartDate,
                dEndDate,
                vValue,
                sNote);

            //return (DateTime)dEndDate.Value;
        }

        public void DeleteMarkerValue(string entityMVCAlias, long baId, MarkerValue markerValue)
        {
            var procName = EntitytProgsStorage.GetInfo(entityMVCAlias).DeleteMarkerProg;

            var iBAId = new SqlParameter("@iBAId", baId);
            var iMarkerId = new SqlParameter("@iMarkerId", markerValue.MarkerId);

            var dStartDate = new SqlParameter("@dStartDate", markerValue.StartDate.HasValue ? markerValue.StartDate.Value : (object)DBNull.Value)
            {
                SqlDbType = System.Data.SqlDbType.DateTime
            };

            ExecuteStoredProg($@"{procName}
                                @iBAId		= @iBAId,
	                            @iMarkerId	= @iMarkerId,	
	                            @dStartDate	= @dStartDate",
                iBAId,
                iMarkerId,
                dStartDate);

        }
    }
}




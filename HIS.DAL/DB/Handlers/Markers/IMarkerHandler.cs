﻿using HIS.DAL.Client.Models.Markers;
using HIS.DAL.DB.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HIS.DAL.DB.Handlers.Markers
{
    public interface IMarkerHandler: IDataContextHandler
    {
        IQueryable<EFMABATypeMarker> GetMABATypeMarkers();
        IQueryable<EFMAMarker> GetMAMarkers();
        IQueryable<EFMAMarkerType> GetMAMarkerTypes();
        IQueryable<EFMABaseAncestorMarkerValue> GetMABaseAncestorMarkerValues(long baId);
        IQueryable<EFMABaseAncestorMarkerValue> GetMABaseAncestorMarkerValues();
        IQueryable<EFMABaseAncestorMarkerValuePeriod> GetMABaseAncestorMarkerValuePeriods();
        IQueryable<EFMABaseAncestorMarkerValuePeriod> GetMABaseAncestorMarkerValuePeriods(long baId);
     
        IQueryable<EFMABaseAncestorMarkerItemValue> GetMABaseAncestorMarkerItemValues(long baId);
        IQueryable<EFMABaseAncestorMarkerItemValue> GetMABaseAncestorMarkerItemValues();
        IQueryable<EFMABaseAncestorMarkerItemValuePeriod> GetMABaseAncestorMarkerItemValuePeriods(long baId);
        IQueryable<EFMAMarkerValueList> GetMAMarkerValueList(int markerId);
        IEnumerable<MarkerValue> GetMarkerValues(long baId, int baTypeId, IEnumerable<int> markerIds);
        IEnumerable<PeriodicMarkerValue> GetAddressMarkerValueHistory(long baId, int baTypeId, int markerId);
        IEnumerable<ReferenceMarkerValueItem> GetCatalogMarkerValueList(int markerId);
        IEnumerable<OptionalMarkers> GetOptionalMarkers(int BATypeId, int markerId, int ItemId);

        IEnumerable<MarkerInfo> GetMarkersInfo(int baTypeId);

        /// <summary>
        /// Значения маркеров экземпляря сущности
        /// </summary>
        /// <param name="baId"></param>
        /// <returns></returns>
        IEnumerable<EFBAMarkerValueEntity> GetMarkerValues(long baId);

        /// <summary>
        /// Значения коллекционных маркеров экземдляра сущности
        /// </summary>
        /// <param name="baId"></param>
        /// <returns></returns>
        IEnumerable<EFBAMarkerValueEntity> GetCollectibleMarkerValues(long baId);

        void SetMarkerValue(long baId, MarkerValueRet marker);
        void SetMarkerValuePeriod(long baId, MarkerValuePeriodRet marker);
        void SetMarkerItemValue(long baId, MarkerItemValue marker);
        void SetMarkerItemValuePeriod(long baId, MarkerItemValuePeriod marker);
        void RemoveMarker(long baId, int MarkerId, DateTime? StartDate = null);
        int? SetMarkerValueList(EFMAMarkerValueList value);
        bool IsOptional(int markerId, int baTypeId);
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace HIS.DAL.DB.Handlers.Markers
{
    internal static class MarkerValuesQueryBuilderHelper
    {
        private const string CSql = @"
DECLARE @CDATA DATE = GETDATE()
DECLARE @sSQL NVARCHAR(MAX) = ''
DECLARE @BAId BIGINT = {{0}}
DECLARE @BATypeId INT = {{1}}

SELECT @sSQL = @sSQL+' '+tmp.ssql
FROM 
(
SELECT 
tm.MarkerId,
tm.ImplementTypeName, 
tm.ImplementTypeField, 
tm.IsPeriodic, 
tm.IsCollectible, 
tm.IsBlocked, 
m.TypeId,
b_ImplementTypeName = b.ImplementTypeName,
ssql = CASE 
       WHEN tm.IsPeriodic = 0
       THEN CASE 
            WHEN m.TypeId = 13
            THEN ''    
            ELSE CASE
                 WHEN tm.ImplementTypeField IS NOT NULL AND (tm.ImplementTypeName IS NOT NULL OR b.ImplementTypeName IS NOT NULL)
                 THEN N'SELECT t.BAId, [MarkerId]='+CAST(tm.MarkerId AS NVARCHAR)+', StartDate={d ''1990-01-01''}, [Value]=CAST(t.'+tm.ImplementTypeField+' AS NVARCHAR), Note=NULL, 
                         DisplayValue= '+CASE WHEN m.TypeId = 12 THEN 'adr.[Name] ' ELSE 'NULL ' END+
                        'FROM dbo.'+CASE WHEN b.ImplementTypeName IS NOT NULL THEN b.ImplementTypeName ELSE tm.ImplementTypeName END+' AS t '+
                         CASE WHEN m.TypeId = 12 THEN 'LEFT JOIN dbo.AOAddresses adr ON adr.[BAId] = CAST(t.'+tm.ImplementTypeField+' AS BIGINT) ' ELSE '' END + 
                        ' WHERE t.BAId = '+CAST(@BAId AS VARCHAR)
                 WHEN tm.ImplementTypeField IS NULL AND tm.ImplementTypeName IS NOT NULL
                 THEN N'SELECT bamv.BAid, bamv.[MarkerId], StartDate={d ''1990-01-01''}, [Value]=CAST(bamv.[sValue] AS NVARCHAR), Note=NULL, 
                        DisplayValue= '+CASE WHEN m.TypeId = 12 THEN 'adr.[Name] ' ELSE 'NULL ' END+ 
                       'FROM [dbo].'+tm.ImplementTypeName+' bamv WHERE bamv.[MarkerId]='+CAST(tm.MarkerId AS NVARCHAR)+
                        CASE WHEN m.TypeId = 12 THEN 'LEFT JOIN dbo.AOAddresses adr ON adr.[BAId] = bamv.Value ' ELSE '' END + 
                       ' AND bamv.BAId = '+CAST(@BAId AS VARCHAR)                     	 
                 ELSE N'SELECT bamv.BAid, bamv.[MarkerId], StartDate={d ''1990-01-01''}, [Value]=CAST(bamv.[sValue] AS NVARCHAR), Note=NULL, 
                        DisplayValue=NULL '+CASE WHEN m.TypeId = 12 THEN 'adr.[Name] ' ELSE 'NULL ' END+  
                       'FROM [dbo].[MABaseAncestorMarkerValues] bamv WHERE bamv.[MarkerId]='+CAST(tm.MarkerId AS NVARCHAR)+
                        CASE WHEN m.TypeId = 12 THEN 'LEFT JOIN dbo.AOAddresses adr ON adr.[BAId] = bamv.Value ' ELSE '' END + 
                       ' AND bamv.BAId = '+CAST(@BAId AS VARCHAR)
                 END                      
       	    END
       WHEN tm.IsPeriodic = 1 
       THEN CASE 
            WHEN m.TypeId = 13
            THEN CASE 
                 WHEN tm.ImplementTypeName IS NULL  
                 THEN 
                    N' SELECT bamvp.BAid, bamvp.[MarkerId], StartDate=bamvp.StartDate, [Value]=bamvp.[sValue], Note=bamvp.Note, 
                              DisplayValue=t.itemName 
                       FROM [dbo].[MABaseAncestorMarkerValuePeriods] bamvp 
                       LEFT JOIN dbo.'+tm.ImplementTypeName+' t ON t.itemId = bamvp.Value 
                       WHERE bamvp.[MarkerId]='+CAST(tm.MarkerId AS NVARCHAR)+' AND CAST(bamvp.StartDate AS DATE) <= '''+CAST(@CDATA AS NVARCHAR)+'''
                                                                                AND CAST(bamvp.EndDate AS DATE) >= '''+CAST(@CDATA AS NVARCHAR)+''' 
                        AND bamvp.BAid = '+CAST(@BAId AS VARCHAR)
                 ELSE 
                    N' SELECT bamvp.BAid, bamvp.[MarkerId], StartDate=bamvp.StartDate, [Value]=bamvp.[sValue], Note=bamvp.Note, 
                              DisplayValue=t.itemName 
                       FROM [dbo].'+tm.ImplementTypeName+' bamvp 
                       LEFT JOIN dbo.'+m.ImplementTypeName+' t ON t.itemId = bamvp.Value 
                       WHERE bamvp.[MarkerId]='+CAST(tm.MarkerId AS NVARCHAR)+' AND CAST(bamvp.StartDate AS DATE) <= '''+CAST(@CDATA AS NVARCHAR)+'''
                       AND CAST(bamvp.EndDate AS DATE) >= '''+CAST(@CDATA AS NVARCHAR)+''' AND bamvp.BAid = '+CAST(@BAId AS VARCHAR)
                 END
            ELSE CASE
                 WHEN tm.ImplementTypeName IS NULL  
                 THEN N'SELECT bamvp.BAid, bamvp.[MarkerId], StartDate=bamvp.StartDate, [Value]=bamvp.[sValue], Note=bamvp.Note, 
                        DisplayValue=NULL'+CASE WHEN  m.TypeId = 12 THEN 'adr.[Name] ' ELSE 'NULL ' END+ 
                       'FROM [dbo].[MABaseAncestorMarkerValuePeriods] bamvp 
                        WHERE bamvp.[MarkerId]='+CAST(tm.MarkerId AS NVARCHAR)+' AND CAST(bamvp.StartDate AS DATE) <= '''+CAST(@CDATA AS NVARCHAR)+''' 
                                                                                 AND CAST(bamvp.EndDate AS DATE) >= '''+CAST(@CDATA AS NVARCHAR)+''''+                        
                        CASE WHEN  m.TypeId = 12 THEN 'LEFT JOIN dbo.AOAddresses adr ON adr.[BAId] = bamvp.Value ' ELSE '' END+
                       ' AND bamvp.BAid = '+CAST(@BAId AS VARCHAR)
                 WHEN tm.ImplementTypeName IS NOT NULL  
                 THEN 
                      N'SELECT vp.BAid, vp.[MarkerId], StartDate=vp.StartDate, [Value]=vp.[sValue], Note=vp.Note, 
                         DisplayValue='+CASE WHEN m.TypeId = 12 THEN ' r.Name ' 
                                ELSE 
                                CASE WHEN tm.ImplementTypeField IS NOT NULL THEN ' r.'+tm.ImplementTypeField  ELSE ' NULL ' END 
                         END+ 
                       ' FROM [dbo].'+tm.ImplementTypeName+' vp '+
                        CASE WHEN  m.TypeId = 12 THEN 'LEFT JOIN dbo.AOAddresses r ON r.[BAId] = vp.sValue ' 
                             WHEN  b.ImplementTypeName IS NOT NULL THEN 'LEFT JOIN dbo.'+b.ImplementTypeName+' r ON r.[BAId] = vp.Value '
                             ELSE ''
                        END+
                       'WHERE vp.[MarkerId]='+CAST(tm.MarkerId AS NVARCHAR)+' 
                        AND CAST(vp.StartDate AS DATE) <= '''+CAST(@CDATA AS NVARCHAR)+'''
                        AND CAST(vp.EndDate AS DATE) >= '''+CAST(@CDATA AS NVARCHAR)+'''
                        AND vp.BAid = '+CAST(@BAId AS VARCHAR)                               
                 END
            END
       END                         
FROM dbo.MABATypeMarkers AS tm
INNER JOIN dbo.BATypes AS b ON b.TypeId = tm.BATypeId 
INNER JOIN dbo.MAMarkers m ON  m.MarkerId = tm.MarkerId
WHERE tm.BATypeId = @BATypeId
{{2}}
) AS tmp 
WHERE tmp.ssql IS NOT NULL 

SET @sSQL = REPLACE(RTRIM(LTRIM(@sSQL)), ' SELECT ', ' UNION SELECT ')

SELECT @sSQL";

        internal static string ValuesSqlQuery(long baId, int baTypeId,  IEnumerable<int> markerIds)
        {
            var ids = markerIds?.ToList() ?? new List<int>();

            var ssql = CSql.Replace("{{0}}", baId.ToString()).Replace("{{1}}", baTypeId.ToString()).Replace("{{2}}",
                        ids.Count > 0
                            ? $" AND tm.MarkerId IN ({ids.Select(m => m.ToString()).Aggregate((a, b) => a.ToString() + ',' + b.ToString())})"
                            : "");

            return ssql;
        }

        private const string CSqlHistory = @"
DECLARE @MarkerId INT = {2}
DECLARE @BATypeId INT = {1}
DECLARE @BaId BIGINT = {0}

DECLARE @sSQL NVARCHAR(MAX) = ''

SELECT 
@sSQL = N'SELECT mv.Value, mv.StartDate, mv.EndDate, mv.Note, DisplayValue=ao.Name  
          FROM dbo.'+mm.ImplementTypeName+' AS mv 
          LEFT JOIN [dbo].[AOAddresses] ao ON ao.[BAId] = CAST(mv.Value AS BIGINT)
          WHERE mv.BAId = '+CAST(@BaId AS NVARCHAR)+' 
            AND mv.MarkerId='+CAST(@MarkerId AS NVARCHAR) 
FROM MAMarkers AS m
INNER JOIN MABATypeMarkers AS mm ON mm.MarkerId = m.MarkerId
WHERE m.[TypeId] = 12 AND m.MarkerId = @MarkerId AND mm.BATypeId = @BATypeId

SELECT @sSQL
";
        
        internal static string PeriodAddresHistorySqlQuery(long baId, int baTypeId, int markerId)
        {
            return string.Format(CSqlHistory, baId, baTypeId, markerId);
        }
    }
}

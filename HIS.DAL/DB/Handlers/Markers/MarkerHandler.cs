﻿using HIS.DAL.Client.Models.Markers;
using HIS.DAL.DB.Context;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace HIS.DAL.DB.Handlers.Markers
{
    public class MarkerHandler : DataContextHandlerBase, IMarkerHandler
    {

        public bool IsOptional(int markerId, int baTypeId)
        {
            var marker = DataContext.MABATypeOptionalMarkers.Where(
                x => x.OptionalMarkerId == markerId && x.BATypeId == baTypeId
                ).FirstOrDefault();
            if (marker != null)
                return true;

            return false;
        }

        public IQueryable<EFMABATypeMarker> GetMABATypeMarkers()
        {
            return DataContext.EFMABATypeMarkers;
        }

        public IQueryable<EFMAMarker> GetMAMarkers()
        {
            return DataContext.EFMAMarkers;
        }

        public IQueryable<EFMAMarkerType> GetMAMarkerTypes()
        {
            return DataContext.EFMAMarkerTypes;
        }

        public IQueryable<EFMABaseAncestorMarkerValue> GetMABaseAncestorMarkerValues(long baId)
        {
            return DataContext.EFMABaseAncestorMarkerValues.Where(mv => mv.BAId == baId);
        }
        public IQueryable<EFMABaseAncestorMarkerValue> GetMABaseAncestorMarkerValues()
        {
            return DataContext.EFMABaseAncestorMarkerValues;
        }

        public IQueryable<EFMABaseAncestorMarkerValuePeriod> GetMABaseAncestorMarkerValuePeriods()
        {
            return DataContext.EFMABaseAncestorMarkerValuePeriods;
        }

        public IQueryable<EFMABaseAncestorMarkerValuePeriod> GetMABaseAncestorMarkerValuePeriods(long baId)
        {
            return DataContext.EFMABaseAncestorMarkerValuePeriods.Where(mv => mv.BAId == baId);
        }
        
        public IQueryable<EFMABaseAncestorMarkerItemValue> GetMABaseAncestorMarkerItemValues(long baId)
        {
            return DataContext.EFMABaseAncestorMarkerItemValues.Where(mv => mv.BAId == baId);
        }
        public IQueryable<EFMABaseAncestorMarkerItemValue> GetMABaseAncestorMarkerItemValues()
        {
            return DataContext.EFMABaseAncestorMarkerItemValues;
        }

        public IQueryable<EFMABaseAncestorMarkerItemValuePeriod> GetMABaseAncestorMarkerItemValuePeriods(long baId)
        {
            return DataContext.EFMABaseAncestorMarkerItemValuePeriods.Where(mv => mv.BAId == baId);
        }

        public IQueryable<EFMAMarkerValueList> GetMAMarkerValueList(int markerId)
        {
            return DataContext.EFMAMarkerValueList.Where(vl => vl.MarkerId == markerId);
        }

        public IEnumerable<MarkerValue> GetMarkerValues(long baId, int baTypeId, IEnumerable<int> markerIds)
        {
            var result = new List<MarkerValue>();

            var ssql = MarkerValuesQueryBuilderHelper.ValuesSqlQuery(baId, baTypeId, markerIds);

            var cmd = DataContext.Database.Connection.CreateCommand();
            cmd.CommandText = ssql;
            if (DataContext.Database.CurrentTransaction != null)
                cmd.Transaction = DataContext.Database.CurrentTransaction.UnderlyingTransaction;

            var paramsSql = cmd.ExecuteScalar().ToString();
            var paramsCmd = DataContext.Database.Connection.CreateCommand();
            paramsCmd.CommandText = paramsSql;

            if (DataContext.Database.CurrentTransaction != null)
                paramsCmd.Transaction = DataContext.Database.CurrentTransaction.UnderlyingTransaction;

            using (var reader = paramsCmd.ExecuteReader(CommandBehavior.SequentialAccess))
            {
                var fields = new Dictionary<string, object>();
                while (reader.Read())
                {
                    for (var i = 0; i < reader.FieldCount; ++i)
                    {
                        fields[reader.GetName(i)] = reader[i];
                    }


                    var markerId = Convert.ToInt32(fields["MarkerId"]);
                    var startDate = fields["StartDate"] == DBNull.Value
                        ? null
                        : (DateTime?) Convert.ToDateTime(fields["StartDate"]);
                    var value = fields["Value"] == DBNull.Value ? null : fields["Value"].ToString();
                    var note = fields["Note"] == DBNull.Value ? null : fields["Note"].ToString();
                    var displayValue = fields["DisplayValue"] == DBNull.Value
                        ? null
                        : fields["DisplayValue"].ToString();


                    result.Add(new MarkerValue
                    {
                        MarkerId = markerId,
                        StartDate = startDate,
                        Value = value,
                        Note = note,
                        DisplayValue = displayValue
                    });
                }
            }

            return result;
        }

        public IEnumerable<PeriodicMarkerValue> GetAddressMarkerValueHistory(long baId, int baTypeId, int markerId)
        {
            var result = new List<PeriodicMarkerValue>();
            var ssql = MarkerValuesQueryBuilderHelper.PeriodAddresHistorySqlQuery(baId, baTypeId, markerId);

            var cmd = DataContext.Database.Connection.CreateCommand();
            cmd.CommandText = ssql;
            var paramsSql = cmd.ExecuteScalar().ToString();

            var paramsCmd = DataContext.Database.Connection.CreateCommand();
            paramsCmd.CommandText = paramsSql;

            using (var reader = paramsCmd.ExecuteReader(CommandBehavior.SequentialAccess))
            {
                var fields = new Dictionary<string, object>();
                while (reader.Read())
                {
                    for (var i = 0; i < reader.FieldCount; ++i)
                    {
                        fields[reader.GetName(i)] = reader[i];
                    }

                    var startDate = fields["StartDate"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(fields["StartDate"]);
                    var endDate = fields["EndDate"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(fields["EndDate"]); 
                    var value = fields["Value"] == DBNull.Value ? null : fields["Value"].ToString();
                    var note = fields["Note"] == DBNull.Value ? null : fields["Note"].ToString();
                    var displayValue = fields["DisplayValue"] == DBNull.Value
                        ? null
                        : fields["DisplayValue"].ToString();


                    result.Add(new PeriodicMarkerValue
                    {
                        StartDate = startDate,
                        EndDate = endDate,
                        Value = value,
                        Note = note,
                        DisplayValue = displayValue
                    });
                }
            }

            return result;
        }

        public IEnumerable<ReferenceMarkerValueItem> GetCatalogMarkerValueList(int markerId)
        {
            // Получить данные из представления
            var viewName = DataContext.EFMAMarkers.Where(m => m.MarkerId == markerId).Select(n => n.ImplementTypeName).FirstOrDefault();
            if (string.IsNullOrEmpty(viewName))
                return null; 
            // numerable.Empty<ReferenceMarkerValueItem>();

            var ssql = $"SELECT ItemId, ItemName FROM {viewName} ORDER BY OrderNum ";
            var result = new List<ReferenceMarkerValueItem>();

            var cmd = DataContext.Database.Connection.CreateCommand();
            cmd.CommandText = ssql;
            if (DataContext.Database.CurrentTransaction != null)
                cmd.Transaction = DataContext.Database.CurrentTransaction.UnderlyingTransaction;

            using (var reader = cmd.ExecuteReader(CommandBehavior.SequentialAccess))
            {
                var fields = new Dictionary<string, object>();
                while (reader.Read())
                {
                    for (var i = 0; i < reader.FieldCount; ++i)
                    {
                        fields[reader.GetName(i)] = reader[i];
                    }

                    var itemId = Convert.ToInt32(fields["ItemId"]);
                    var ItemName = fields["ItemName"].ToString();

                    result.Add(new ReferenceMarkerValueItem
                    {
                        ItemId = itemId,
                        ItemName = ItemName
                    });
                }
            }

            return result;
        }

        public IEnumerable<EFBAMarkerValueEntity> GetCollectibleMarkerValues(long baId)
        {
            return GetBAMarkerValueEntity(baId, true);
        }

        public IEnumerable<MarkerInfo> GetMarkersInfo(int baTypeId)
        {
           return DataContext.EFMABATypeMarkers.Where(tm => tm.BATypeId == baTypeId)
                .Select(tm => new MarkerInfo
                {
                    RefBaTypeId = tm.MAMarkers.BATypeId,
                    Id = tm.MarkerId,
                    SearchTypeId = tm.MAMarkers.BATypes.SearchTypeId,
                    MarkerType = tm.MAMarkers.MAMarkerTypes.TypeId,
                    Name = tm.MAMarkers.MVCAlias,
                    Label = tm.Name == null || tm.Name.Equals("") ? tm.MAMarkers.Name : tm.Name,
                    IsPeriodic = tm.IsPeriodic,
                    IsCollectible = tm.IsCollectible,
                    IsBlocked = tm.IsBlocked,
                    IsRequired = tm.IsRequired,
                    IsImplemented = !(tm.ImplementTypeField == null || tm.ImplementTypeField.Equals("")),
                    ImplementTypeName = tm.ImplementTypeName,
                    CatalogImplementTypeName = tm.MAMarkers.ImplementTypeName,
                    ImplementTypeField = tm.ImplementTypeField,
                    List = tm.MAMarkers.MAMarkerValueList.Select(v => new ListItem
                    {
                        ItemId = v.ItemId,
                        Value = v.ItemName
                    })
                }).ToArray();

         
        }


        public IEnumerable<EFBAMarkerValueEntity> GetMarkerValues(long baId)
        {
            return GetBAMarkerValueEntity(baId, false);
        }

        public void SetMarkerValue(long baId, MarkerValueRet marker)
        {
            ExecSetMarker(baId,
                          marker.MarkerId,
                          marker.Value);
        }

        public void SetMarkerValuePeriod(long baId, MarkerValuePeriodRet marker)
        {
            ExecSetMarker(baId,
                          marker.MarkerId,
                          marker.Value,
                          marker.StartDate);
        }

        public void SetMarkerItemValue(long baId, MarkerItemValue marker)
        {
            ExecSetMarkerItem(
                    baId,
                    marker.MarkerId,
                    marker.ItemId,
                    marker.Value
                );
        }

        public void SetMarkerItemValuePeriod(long baId, MarkerItemValuePeriod marker)
        {
            ExecSetMarkerItem(
                    baId,
                    marker.MarkerId,
                    marker.ItemId,
                    marker.Value,
                    marker.StartDate
                );
        }

        public int? SetMarkerValueList(EFMAMarkerValueList value)
        {
            if (string.IsNullOrEmpty(value.ItemName))
                return null;

            var mValue = DataContext.EFMAMarkerValueList.Where(m => m.MarkerId == value.MarkerId && m.ItemName.ToLower() == value.ItemName.ToLower()).FirstOrDefault();
            if (mValue != null)
                return mValue.ItemId;

            value.ItemId = DataContext.EFMAMarkerValueList.Where(m => m.MarkerId == value.MarkerId).Max(m => m.ItemId) + 1;

            DataContext.EFMAMarkerValueList.Add(value);
            DataContext.SaveChanges();
            return value.ItemId;
        }

        public void RemoveMarker(long baId, 
                                 int MarkerId,
                                 DateTime? StartDate = null)
        {
            ExecuteStoredProg(@"[dbo].[MA_RemoveMarker]
                    @iBAId = @iBAId,
                    @iMarkerId = @iMarkerId,
                    @dStartDate = @dStartDate",
                    new SqlParameter("@iBAId", baId),
                    new SqlParameter("@iMarkerId", MarkerId),
                    new SqlParameter("@dStartDate", StartDate.HasValue ? StartDate.Value : (object)DBNull.Value));
        }


        private void ExecSetMarker(long BAId,
                                   int MarkerId,
                                   string Value,
                                   DateTime? StartDate = null)
        {
            ExecuteStoredProg(@"[dbo].[MA_SetMarker]
                              @iBAId = @iBAId,
                              @iMarkerId = @iMarkerId,
                              @sValue = @sValue,
                              @dStartDate = @dStartDate",
                              new SqlParameter("@iBAId", BAId),
                              new SqlParameter("@iMarkerId", MarkerId),
                              new SqlParameter("@sValue", Value),
                              new SqlParameter("@dStartDate", StartDate.HasValue ? StartDate.Value : (object)DBNull.Value ));
        }


        private void ExecSetMarkerItem(long BAId,
                                       int MarkerId,
                                       int? ItemId,
                                       string Value,
                                       DateTime? StartDate = null)
        {
            ExecuteStoredProg(@"[dbo].[MA_SetMarkerItem]
                    @iBAId = @iBAId,
                    @iMarkerId = @iMarkerId,
                    @iItemId = @iItemId OUT,
                    @sValue = @sValue,
                    @dStartDate = @dStartDate",
                    new SqlParameter("@iBAId", BAId),
                    new SqlParameter("@iMarkerId", MarkerId),
                    new SqlParameter("@iItemId", ItemId.HasValue ? ItemId : (object)DBNull.Value),
                    new SqlParameter("@sValue", Value),
                    new SqlParameter("@dStartDate", StartDate.HasValue ? StartDate.Value : (object)DBNull.Value));
        }

        public IEnumerable<OptionalMarkers> GetOptionalMarkers(int BATypeId, int markerId, int ItemId)
        {
            return DataContext.MABA_TypeOptionalMarkerList(BATypeId, markerId, ItemId).Select(
                   x => new OptionalMarkers
                   {
                       BATypeId = x.BATypeId,
                       OptionalMarkerId = x.OptionalMarkerId,
                       MarkerId = x.MarkerId,
                       IsVisible = x.IsVisible
                   }).ToList();
        }


        private IEnumerable<EFBAMarkerValueEntity> GetBAMarkerValueEntity(long baId, bool isCollectible)
        {
            return DataContext.GetMarkerValueSelect(baId, isCollectible).ToList();
        }
    }
}
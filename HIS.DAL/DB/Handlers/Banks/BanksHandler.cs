﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using HIS.DAL.Client.Models.Banks;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.DB.Handlers.Banks
{
    public class BanksHandler : DataContextHandlerBase, IBanksHandler
    {
        /// <summary>
        /// Обновеление банка\отделения
        /// </summary>
        /// <param name="entity">Сущность банка\отделения банка</param>
        /// <returns>Обновленная сущность банка\отделения банка</returns>
        public BanksEntity Update(BanksEntity entity)
        {
            var iBaId = new SqlParameter("@iBAId", entity.BaId.HasValue 
                                                    ? entity.BaId.Value
                                                    : (object)DBNull.Value);
            iBaId.Direction = ParameterDirection.InputOutput;
            iBaId.SqlDbType = SqlDbType.BigInt;

            var iBatypeId = new SqlParameter("@iBATypeId", entity.BATypeId.HasValue
                                                            ? entity.BATypeId.Value
                                                            :(object)DBNull.Value);
            iBatypeId.Direction = ParameterDirection.InputOutput;
            iBatypeId.SqlDbType = SqlDbType.Int;

            var iParentId = new SqlParameter("@iParentId", entity.ParentId.HasValue
                                                            ? entity.ParentId.Value
                                                            : (object)DBNull.Value);
            iParentId.Direction = ParameterDirection.Input;
            iParentId.SqlDbType = SqlDbType.NVarChar;

            var sBik = new SqlParameter("@sBIK", entity.Bik);
            sBik.Direction = ParameterDirection.Input;
            sBik.SqlDbType = SqlDbType.NVarChar;

            var sInn = new SqlParameter("@sINN", entity.Inn);
            sInn.Direction = ParameterDirection.Input;
            sInn.SqlDbType = SqlDbType.NVarChar;


            var sRkc = new SqlParameter("@sRKC", string.IsNullOrEmpty(entity.Rkc) 
                                                 ? (object)DBNull.Value
                                                 : entity.Rkc);
            sRkc.Direction = ParameterDirection.Input;
            sRkc.SqlDbType = SqlDbType.NVarChar;

            var sOkpo = new SqlParameter("@sOKPO", string.IsNullOrEmpty(entity.Okpo)
                                                        ? (object)DBNull.Value
                                                        : entity.Okpo);
            sOkpo.Direction = ParameterDirection.Input;
            sOkpo.SqlDbType = SqlDbType.NVarChar;

            var addrId = new SqlParameter("@addrId", entity.AddrId.HasValue
                ? entity.AddrId.Value
                : (object)DBNull.Value);
            addrId.Direction = ParameterDirection.Input;
            addrId.SqlDbType = SqlDbType.NVarChar;

            ExecuteStoredProg(@"[dbo].[RF_BankUpdate] @iBAId = @iBAId OUT,
                           @iBATypeId = @iBATypeId OUT,
						   @iParentId = @iParentId,
						   @sBIK = @sBIK,
						   @sINN= @sINN,
						   @sRKC= @sRKC,
						   @sOKPO = @sOKPO,
						   @iaddrId = @addrId",
                           iBaId,
                           iBatypeId,
                           iParentId,
                           sBik,
                           sInn,
                           sRkc,
                           sOkpo,
                           addrId);
            entity.BaId = (long)iBaId.Value;
            entity.BATypeId = (int)iBatypeId.Value;
            entity.DisplayAddr = GetAddrdisplayValue(entity.AddrId, 4);
            return entity;
        }

        public IEnumerable<BanksEntity> GetBanks(Func<Context.Banks, bool> predicate = null)
        {
            if (predicate == null)
                predicate = bank => true;
            var banksParams = GetBanksMarkers();
            
            var data = DataContext.Banks
                                .Where(predicate)
                                .Join(DataContext.EFSOStates,
                                  bankSelector => bankSelector.StateId,
                                  statusSelector => statusSelector.StateId,
                                  (bankSelector, statusSelector) =>
                                  {
                                      var currentMarkers = banksParams.Where(bankParam => bankParam.Baid == bankSelector.BAId);
                                      var result = new BanksEntity
                                      {
                                          BaId = bankSelector.BAId,
                                          AddrId = bankSelector.AddrId,
                                          DisplayAddr = GetAddrdisplayValue(bankSelector.AddrId, 4),
                                          ParentId = (int?)bankSelector.ParentId,
                                          Bik = bankSelector.BIK,
                                          Inn = bankSelector.INN,
                                          Rkc = bankSelector.RKC,
                                          Okpo = bankSelector.OKPO,
                                          Status = statusSelector.Name,
                                          Name = !string.IsNullOrEmpty(GetActualMarker(currentMarkers, 1))                  // Наименование
                                              ? GetActualMarker(currentMarkers, 1)
                                              : "",
                                          CorespondetsAccount = !string.IsNullOrEmpty(GetActualMarker(currentMarkers, 33))  // корп счёт
                                              ? GetActualMarker(currentMarkers, 33)
                                              : string.Empty,

                                          AcceptancePeriod = GetActualMarker(currentMarkers, 34) != null                    // Cрок акцепта (кол-во дней)
                                              ? long.Parse(GetActualMarker(currentMarkers, 34))
                                              : 0
                                      };

                                      if (currentMarkers.Count() != 0)
                                          result.Markers = currentMarkers;

                                      if (bankSelector.ParentId != null) // если отделение
                                      {
                                          result.BankBranchDisplayValue = GetBankBranchDisplayValue(currentMarkers, bankSelector.ParentId);
                                      }

                                      return result;
                                  });
                                return data.ToList();
        }

        public IEnumerable<BanksEntity> GetBanksBranches(long? baId)
        {
            return GetBanks(banksBranch => banksBranch.ParentId == baId);
        }

        public IEnumerable<PeriodicMarkerValue> GetPeriodicBanksMarkerHistory(long baId, int markerId)
        {
                return DataContext.BankParamValuePeriods.Where(x => x.BAId.Equals(baId) && x.MarkerId.Equals(markerId))
                                    .Select(x => new PeriodicMarkerValue
                                                {
                                                    EndDate = x.EndDate,
                                                    Note = x.Note,
                                                    StartDate = x.StartDate,
                                                    Value = x.sValue
                                                })
                                    .OrderByDescending(x => x.StartDate)
                                    .ToList();
        }

        public void RemoveBanksMarker(long baId, int markerId, DateTime? startDate = null)
        {
            ExecuteStoredProg(@"[dbo].[RF_BankParamDelete]
                    @iBAId = @iBAId,
                    @iMarkerId = @iMarkerId,
                    @dStartDate = @dStartDate",
                    new SqlParameter("@iBAId", baId),
                    new SqlParameter("@iMarkerId", markerId),
                    new SqlParameter("@dStartDate", startDate ?? (object)DBNull.Value));
        }

        public string GetBankHeader(long baId)
        {
            return GetBanks(bank => bank.BAId == baId).FirstOrDefault()?.Name;
        }

        public bool CheckThatInnOnlyOne(string inn, long sourceBaid)
        {
            return !DataContext.Banks.Any(bank => bank.INN == inn 
                                                  && bank.ParentId == null
                                                  && bank.BAId != sourceBaid);
        }

        public DateTime SaveMarkerParametr(BankMarker bankMarker)
        {
            var iBaId = new SqlParameter("@iBAId", bankMarker.Baid);
            iBaId.Direction = ParameterDirection.Input;
            iBaId.SqlDbType = SqlDbType.BigInt;

            var iMarkerId = new SqlParameter("@iMarkerId", bankMarker.MarkerId);
            iMarkerId.Direction = ParameterDirection.Input;
            iMarkerId.SqlDbType = SqlDbType.BigInt;

            var dStartDate = new SqlParameter("@dStartDate", bankMarker.StartDate);
            dStartDate.Direction = ParameterDirection.Input;
            dStartDate.SqlDbType = SqlDbType.DateTime;

            var dEndDate = new SqlParameter("@dEndDate", bankMarker.EndDate.HasValue
                ? bankMarker.EndDate.Value
                : (object)DBNull.Value);
            dEndDate.Direction = ParameterDirection.InputOutput;
            dEndDate.SqlDbType = SqlDbType.DateTime;

            var vValue = new SqlParameter("@vValue", bankMarker.Value);
            vValue.Direction = ParameterDirection.Input;
            vValue.SqlDbType = SqlDbType.Variant;

            var sNote = new SqlParameter("@sNote", string.IsNullOrEmpty(bankMarker.Note) 
                                                   ? ""
                                                   : bankMarker.Note);
            sNote.Direction = ParameterDirection.Input;
            sNote.SqlDbType = SqlDbType.VarChar;

            ExecuteStoredProg(@"[dbo].[RF_BankParamUpdate] @iBAId = @iBAId,
                                       @iMarkerId = @iMarkerId,
                                       @dStartDate = @dStartDate,
                                       @dEndDate = @dEndDate OUT,
                                       @vValue = @vValue,
                                       @sNote = @sNote",
                iBaId,
                iMarkerId,
                dStartDate,
                dEndDate,
                vValue,
                sNote);

            return (DateTime)dEndDate.Value;
        }

        private IEnumerable<BankMarker> GetBanksMarkers()
        {
            var banksMarkers = DataContext.BankParamValuePeriods
                .Select(bankParam => new BankMarker
                {
                    Baid = (int)bankParam.BAId,
                    EndDate = bankParam.EndDate,
                    Value = bankParam.sValue,
                    Note = bankParam.Note,
                    StartDate = bankParam.StartDate,
                    MarkerId = bankParam.MarkerId
                }).ToList();

            var paramValues = DataContext.BankParamValues
                .Select(bankParam => new BankMarker
                {
                    Baid = (int)bankParam.BAId,
                    Value = bankParam.sValue,
                    Note = bankParam.Note,
                    MarkerId = bankParam.MarkerId
                }).ToList();
            foreach (var paramValue in paramValues)
            {
                paramValue.Value = DateTime.Parse(paramValue.Value).ToString("yyyy-MM-dd");
                banksMarkers.Add(paramValue);
            }

            return banksMarkers;
        }

        private string GetAddrdisplayValue(long? baId, int aoLevel)
        {
            long refId;
            if (!baId.HasValue)
                return "";
            refId = baId.Value;

            var displayValue = DataContext.AOAddresses
                .FirstOrDefault(address => address.BAId == refId)?.Name ;
            return displayValue;
        }

        private string GetBankBranchDisplayValue(IEnumerable<BankMarker> currentMarkers, long? baId)
        {
            return GetActualMarker(currentMarkers.Where(marker => marker.Baid == baId 
                                                                  && marker.MarkerId == 1).ToList(), 1);
        }

        private string GetActualMarker(IEnumerable<BankMarker> banksParams, int actualMarkerId)
        {
            var collections = banksParams.Where(bankMarker => bankMarker.MarkerId == actualMarkerId);
            if (collections.Count() != 0)
                return collections.OrderByDescending(orderField => orderField.StartDate)
                    .First()
                    .Value;
            return null;
        }
    }
}

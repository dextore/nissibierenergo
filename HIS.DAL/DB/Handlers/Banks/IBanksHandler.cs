﻿using System;
using System.Collections.Generic;
using HIS.DAL.Client.Models.Banks;
using HIS.DAL.Client.Models.Markers;


namespace HIS.DAL.DB.Handlers.Banks
{
    public interface IBanksHandler : IDataContextHandler
    {
        /// <summary>
        /// Обновеление банка\отделения
        /// </summary>
        /// <param name="entity">Сущность банка\отделения банка</param>
        /// <returns>Обновленная сущность банка\отделения банка</returns>
        BanksEntity Update(BanksEntity entity);

        /// <summary>
        /// Получить банки
        /// </summary>
        /// <param name="predicate">Логическое выражение для запроса к бд, секции where</param>
        /// <returns>банки</returns>
        IEnumerable<BanksEntity> GetBanks(Func<Context.Banks, bool> predicate = null);

        /// <summary>
        /// Сохранить маркер банка
        /// </summary>
        /// <param name="bankMarker"></param>
        /// <returns>Дата окончания действия маркера</returns>
        DateTime SaveMarkerParametr(BankMarker bankMarker);

        /// <summary>
        /// Получить отделения
        /// </summary>
        /// <param name="predicate">Логическое выражение для запроса к бд, секции where</param>
        /// <returns>отделения</returns>
        IEnumerable<BanksEntity> GetBanksBranches(long? baId);

        /// <summary>
        /// Получить историю маркера
        /// </summary>
        /// <param name="baId">baid сущности</param>
        /// <param name="markerId">id маркера</param>
        /// <returns>История значений</returns>
        IEnumerable<PeriodicMarkerValue> GetPeriodicBanksMarkerHistory(long baId, int markerId);

        /// <summary>
        /// Удалить маркер
        /// </summary>
        /// <param name="baId">id сущности</param>
        /// <param name="markerId">id маркера</param>
        /// <param name="startDate">??</param>
        void RemoveBanksMarker(long baId, int markerId, DateTime? startDate = null);

        /// <summary>
        /// Получить хэдэр банка для EntityStateControll
        /// </summary>
        /// <param name="baId"></param>
        /// <returns></returns>
        string GetBankHeader(long baId);

        /// <summary>
        /// Проверяет что инн единственный в базе банков
        /// </summary>
        /// <param name="entity">проверяемый инн</param>
        /// <returns>fals - инн уже есть в базе</returns>
        bool CheckThatInnOnlyOne(string inn, long sourceBaid);
    }
}


﻿namespace HIS.DAL.DB.Common
{
    public enum BATypes
    {
        Document = 1,
        Inventory = 2,
        Organisation = 11,
        Person = 12,
        CommercePerson = 13,
        AffiliateOrganisation = 14,
        Object = 22,
        ConPoint = 23,
        RegPoint = 24,
        Contract = 45,
        ContractNomenclature = 46,
        ContractElement = 47,
        Banks = 30,
        BanksBranches = 31,
        BankAccount = 34,
        ConsignmentNotes = 80
      
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.DB.Context;

namespace HIS.DAL.DB.Common
{
    public delegate void CheckMarkerValuesDelegete(HISModelEntities dataContext,  EntityUpdate model, IEnumerable<MarkerInfo> markersInfo);

    public class EntityProgInfo
    {
        internal string Aliase { get; private set; }
        internal string UpdateEntityProg { get; private set; }

        internal string UpdateMarkerProg { get; private set; }
        internal string DeleteMarkerProg { get; private set; }

        internal CheckMarkerValuesDelegete CheckValues { get; private set; }

        internal Dictionary<string, string> ProgMarkersMap {get; private set;}

        internal EntityProgInfo(string alias,
            string updateEntityProg,
            string updateMarkerProg,
            string deleteMarkerProg,
            Dictionary<string, string>  progMarkersMap,
            CheckMarkerValuesDelegete checkValues = null)
        {
            Aliase = alias;
            UpdateEntityProg = updateEntityProg;
            UpdateMarkerProg = updateMarkerProg;
            DeleteMarkerProg = deleteMarkerProg;
            ProgMarkersMap = progMarkersMap;
            CheckValues = checkValues ?? ((dataContext, model, markersInfo) => {});
        }
    }
}

﻿using System;
using System.Collections.Generic;
using HIS.DAL.DB.ProgsInfo;

namespace HIS.DAL.DB.Common
{
    internal static class EntitytProgsStorage
    {
        private static Dictionary<string, EntityProgInfo> _progsInfo = new Dictionary<string, EntityProgInfo>();
        private static void AddProgInfo(EntityProgInfo entityProgInfo)
        {
            _progsInfo.Add(entityProgInfo.Aliase, entityProgInfo);
        }

        static EntitytProgsStorage()
        {
            // Contracts
            AddProgInfo(new ContractProgInfoBuilder().Get());
            // ContractInvent
            AddProgInfo(new ContractInventProgInfoBuilder().Get());
        }

        internal static EntityProgInfo GetInfo(string alias)
        {
            if (!EntitytProgsStorage._progsInfo.ContainsKey(alias))
                throw new NotSupportedException($"Progs config for '{alias}' Not found!");

            return EntitytProgsStorage._progsInfo[alias];
        }
    }
}

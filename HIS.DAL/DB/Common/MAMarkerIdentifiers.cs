﻿namespace HIS.DAL.DB.Common
{
    public sealed class MAMarkerIdentifiers
    {
        public const int Address = 5;
        public const int CompanyArea = 6;
        public const int FactAddress = 16;
        public const int ContactList = 20;
        public const int PhoneList = 21;
        public const int EmailList = 22;
        public const int PhoneType = 23;
        public const int ContactType = 24;
        public const int MailConfirmType = 25;
        public const int MailType = 26;
        public const int PostAddress = 122;
        public const int BankAccount = 132;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HIS.DAL.DB.Common;

namespace HIS.DAL.DB.ProgsInfo
{
    public class ContractProgInfoBuilder: IProgInfoBuilder
    {
        public EntityProgInfo Get()
        {
            return new EntityProgInfo(
                "Contract",
                "[dbo].[CA_ContractUpdate]",
                "[dbo].[CA_ContractParamUpdate]",
                "[dbo].[CA_ContractParamDelete]",
                new Dictionary<string, string>
                {
                    {"Name", "@sNumber"},
                    {"DocDate", "@dDocDate"},
                    {"CompanyArea", "@iCAId"},
                    {"LegalSubject", "@iLSId"},
                    {"ContractType", "@iTypeId"},
                    {"Note", "@sNote"}
                }
            );
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HIS.DAL.Client.Models.Markers;
using HIS.DAL.DB.Common;
using HIS.DAL.DB.Context;
using HIS.DAL.Exceptions;

namespace HIS.DAL.DB.ProgsInfo
{
    public class ContractInventProgInfoBuilder: IProgInfoBuilder
    {
        public EntityProgInfo Get()
        {
            return new EntityProgInfo(
                "ContractInvent",
                "[dbo].[CA_ContractInventUpdate]",
                "[dbo].[CA_ContractInventParamUpdate]",
                "[dbo].[CA_ContractInventParamDelete]",
                new Dictionary<string, string>
                {
                    {"Contract", "@iContractId"},
                    {"Inventory", "@iInvId"},
                    {"StartDate", "@dStartDate"},
                    {"EndDate", "@dEndDate"},
                    {"Note", "@sNote"}
                },
                CheckValues
            );
        }

        private static void CheckValues(HISModelEntities dataContext, EntityUpdate model, IEnumerable<MarkerInfo> markersInfo)
        {
            var nmkValue = model.MarkerValues.FirstOrDefault(m => m.MVCAliase == "Inventory");
            if (nmkValue == null)
                return;

            var nmkId = long.Parse(nmkValue.Value);

            var contractValue = model.MarkerValues.FirstOrDefault(m => m.MVCAliase == "Contract");

            var state = (model.BAId == null)
                ? dataContext.EFSOBATypeStates.First(m => m.IsFirstState == true).SOStates
                : dataContext.EFCAContractInvents.First(m => m.BAId == model.BAId).SOStates;

            if (dataContext.EFSOBATypeStates.Any(m =>
                m.BATypeId == model.BATypeId && m.StateId == state.StateId && m.IsDelState))
            {
                throw new DataValidationException($"Нельзя изменять контракт в состоянии '{state.Name}'");
            }

            var contractId = long.Parse(contractValue.Value);
            if (!dataContext.EFCAContractInvents.Any(m =>
                m.ContractId == contractId && m.InvId == nmkId && m.StateId == state.StateId)) return;

            var nmk = dataContext.IInventory.First(m => m.BAId == nmkId);
            throw new DataValidationException($"В спецификации уже есть номенклатура '{nmk.Name}' в состоянии '{state.Name}'");
        }
    }
}

﻿using HIS.DAL.DB.Common;

namespace HIS.DAL.DB.ProgsInfo
{
    public interface IProgInfoBuilder
    {
        EntityProgInfo Get();
    }
}
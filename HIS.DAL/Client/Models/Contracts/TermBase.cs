﻿using System;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Models.Contracts
{
    public class TermBase: MarkerValueBase
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Note { get; set; }
    }
}

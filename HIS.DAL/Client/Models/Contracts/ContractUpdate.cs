﻿using System;

namespace HIS.DAL.Client.Models.Contracts
{
    public class ContractUpdate
    {
        public long? BAId { get; set; }
        public string Name { get; set; }
        public DateTime? DocDate { get; set; }
        public long CompanyArea { get; set; }
        public long? LegalSubject { get; set; }
        public int ContractType { get; set; }
        public string Note { get; set; }
    }
}

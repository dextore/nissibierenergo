﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.DAL.Client.Models.Contracts
{
    public class ContractInvent
    {
        public int? OrderNum { get; set; }
        public long BAId { get; set; }
        public long ContractId { get; set; }
        public long InventId { get; set; }
        public string InventName { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
    }
}

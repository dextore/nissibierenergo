﻿using System;

namespace HIS.DAL.Client.Models.Contracts
{
    public class LSContract
    {
        public long BAId { get; set; }
        public int BATypeId { get; set; }
        public int StateId { get; set; }
        public string Number { get; set; }
        public DateTime? DocDate { get; set; }
        public long CAId { get; set; }
        public long? LSId { get; set; }
        public int? TypeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Note { get; set; }
        public int CreatorId { get; set; }
        public string UKGroup { get; set; }
        public string TypeName { get; set; }
        public string State { get; set; }
        public bool IsDelState { get; set; }
        public bool IsFirstState { get; set; }
        public string CompanyAreaName { get; set; }
    }
}

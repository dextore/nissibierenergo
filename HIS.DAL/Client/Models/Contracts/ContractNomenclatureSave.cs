﻿using System;

namespace HIS.DAL.Client.Models.Contracts
{
    public class ContractNomenclatureSave
    {
        public long? ContractNomenclatureId { get; set; } 
        public long ContractId { get; set; }
        public long SupplierId { get; set; }
        public long NomenclatureId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}

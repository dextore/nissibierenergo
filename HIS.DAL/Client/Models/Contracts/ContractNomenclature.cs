﻿using HIS.DAL.Client.Models.Nomenclatures;
using System;

namespace HIS.DAL.Client.Models.Contracts
{
    public class ContractNomenclature: Nomenclature
    {
        public long ContractNomenclatureId { get; set; }
        public long ContractId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool? IsProject { get; set; }
    }
}

﻿using HIS.DAL.Client.Models.Elements;
using System;

namespace HIS.DAL.Client.Models.Contracts
{
    public class ContractElement: ElementExt
    {
        public long? ContractElementId { get; set; }
        public long ContractId { get; set; }
        public long NomenclatureId { get; set; }
        public DateTime StartDate { get; set; }
        public long? ParentId { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsProject { get; set; }
        public string ElementCode { get; set; }
        public string Note { get; set; }
        public long? AddrId { get; set; }
        public int OrderNum { get; set; }
        public bool HasItems { get; set; }
    }
}

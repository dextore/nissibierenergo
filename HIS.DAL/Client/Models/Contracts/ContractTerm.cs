﻿namespace HIS.DAL.Client.Models.Contracts
{
    public class ContractTerm: TermBase
    {
        public long ContractId { get; set; }
    }
}

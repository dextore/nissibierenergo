﻿namespace HIS.DAL.Client.Models.Contracts
{
    public class ContractInfo: Contract
    {
        public string SupplierName { get; set; }
        public string ConsumerName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace HIS.DAL.Client.Models.Contracts
{
    public class Contract
    {
        public long? ContractId { get; set; }
        public string Number { get; set; } 
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long? SupplierId { get; set; }
        public long? ConsumerId { get; set; }
        public IEnumerable<ContractTerm> Terms { get; set; }
    }
}

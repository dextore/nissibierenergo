﻿namespace HIS.DAL.Client.Models.Contracts
{
    public class ContractElementTerm : TermBase
    {
        public long ContractElementId { get; set; }
    }
}

﻿namespace HIS.DAL.Client.Models
{
    /// <summary>
    /// Элемент меню, отражающий модуль системы
    /// </summary>
    public class ModuleMenuItem: MenuItem
    {
        /// <summary>
        /// Имя модуля в системе
        /// </summary>
        public string ModuleName { get; set; }
    }
}

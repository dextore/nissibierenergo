﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.DAL.Client.Models.SystemTree
{
    public class SystemTreeItem
    {
        public long BAId { get; set; }
        public string Name { get; set; }
        public long BATypeId { get; set; }
        public string MVCAlias { get; set; }
        public string BATypeName { get; set; }
        public string BATypeNameShort { get; set; }
        public long StateId { get; set; }
        public string StateName { get; set; }
        public IEnumerable<SystemTreeItem> Children { get; set; }
    }
}

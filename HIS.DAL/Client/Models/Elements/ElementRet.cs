﻿using HIS.DAL.Client.Models.Markers;
using System.Collections.Generic;

namespace HIS.DAL.Client.Models.RegPoints
{
    public class ElementRet: Element 
    {
        public IEnumerable<MarkerValueRet> Markers { get; set; }
        public IEnumerable<MarkerValuePeriodRet> Schemas { get; set; }
    }
}

﻿namespace HIS.DAL.Client.Models.Elements
{
    public class ElementItem
    {
        public long? ElementId { get; set; }
        public int BATypeId { get; set; }
        public string Name { get; set; }
    }
}

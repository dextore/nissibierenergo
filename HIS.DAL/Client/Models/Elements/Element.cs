﻿namespace HIS.DAL.Client.Models.RegPoints
{
    public class Element
    {
        public long? ElementId { get; set; }
        public string Name { get; set; }
    }
}

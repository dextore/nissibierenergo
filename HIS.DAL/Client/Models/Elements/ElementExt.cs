﻿namespace HIS.DAL.Client.Models.Elements
{
    public class ElementExt: ElementItem
    {
        public string ElementType { get; set; }
        public string ElementTypeCode { get; set; }
        public string Address { get; set; }
    }
}

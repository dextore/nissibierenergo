﻿using System;

namespace HIS.DAL.Client.Models.BankAccounts
{
    public class LSBankAccount: EntityBase
    {
        public long? BankAccountId { get; set; }
        public int? MarkerId { get; set; }
        public int? ItemId { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public string Number { get; set; }
        public string Note { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsDefault { get; set; }
        public long? BankId { get; set; }
        public string BankName { get; set; }
    }
}

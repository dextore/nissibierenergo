﻿namespace HIS.DAL.Client.Models.States
{
    /// <summary>
    /// Состояние сущности
    /// </summary>
    public class EntityTypeState
    {
        /// <summary>
        /// Тип сущьности
        /// </summary>
        public int BATypeId { get; set; }

        /// <summary>
        /// Идентификатор состояния
        /// </summary>
        public int StateId { get; set; }

        /// <summary>
        /// Наименование состояния
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Начальное состояния
        /// </summary>
        public bool IsFirstState { get; set; }

        /// <summary>
        ///  Состояние удален
        /// </summary>
        public bool IsDelState { get; set; }
    }
}

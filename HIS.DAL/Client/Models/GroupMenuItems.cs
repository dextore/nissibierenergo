﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models
{
    /// <summary>
    /// Именованная группа элементов меню
    /// </summary>
    public class GroupMenuItems: List<MenuItem>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="groupName"></param>
        public GroupMenuItems(string groupName)
        {
            GroupName = groupName;
        }

        /// <summary>
        /// Имя группы меню
        /// </summary>
        public string GroupName { get; set; }          
        
    }
}

﻿namespace HIS.DAL.Client.Models.Nomenclatures
{
    public class NomenclatureSave
    {
        public long? NomenclatureId { get; set; }
        public int BATypeId { get; set; }
        public string Name { get; set; }
        public int BuyNatMeaningId { get; set; }
        public int SaleNatMeaningId { get; set; }
        public int StockNatMeaningId { get; set; }
        public int TaxRateId { get; set; }
    }
}

﻿namespace HIS.DAL.Client.Models.Nomenclatures
{
    public class Nomenclature
    {
        public long? NomenclatureId { get; set; }
        public int BATypeId { get; set; } 
        public string Name { get; set; }
        public int BuyNatMeaningId { get; set; }
        public string BuyNatMeaningName { get; set; }
        public string BuyNatMeaningNote { get; set; }
        public int SaleNatMeaningId { get; set; }
        public string SaleNatMeaningName { get; set; }
        public string SaleNatMeaningNote { get; set; }
        public int StockNatMeaningId { get; set; }
        public string StockNatMeaningName { get; set; }
        public string StockNatMeaningNote { get; set; }
        public int TaxRateId { get; set; }
        public string TaxRateName { get; set; }
        public string TaxRateNote { get; set; }
    }
}

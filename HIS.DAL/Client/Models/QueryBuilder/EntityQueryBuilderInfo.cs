﻿namespace HIS.DAL.Client.Models.QueryBuilder
{
    public class EntityQueryBuilderInfo
    {
        public int TypeId { get; set; }
        public string Name { get; set; }
        public string ImplementTypeName { get; set; }
        public string MvcAlias { get; set; }
        public string ParentId { get; set; }
    }
}

﻿namespace HIS.DAL.Client.Models.QueryBuilder
{
    public class MarkersQueryBuilder
    {
        public int MarkerId { get; set; }
        public string Name { get; set; }
        public string MvcAlias { get; set; }
        public string ImplementTypeField { get; set; }
        public string ImplementTypeName { get; set; }
        public int? BaTypeId { get; set; }
        public bool IsPeriodic { get; set; }
        public bool IsCollectible { get; set; }
        public string SQLServerType { get; set; }
        public int MarkerTypeId { get; set; }
        public string MarkerView { get; set; }
        public string LinkedBaTypeImplaiment { get; set; }
        public string LinkedBaTypeFieldName { get; set; }
    }
}

﻿namespace HIS.DAL.Client.Models.Documents
{
    public class DocumentTreeItem
    {
        public int BATypeId { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int TreeLevel { get; set; }
        public int BranchId { get; set; }
    }
}

﻿namespace HIS.DAL.Client.Models.Inventory
{
    public class InventoryListFilters
    {

        public long? BAId { get; set; }
        public string Name { get; set; }
        public long? CAId { get; set; }
        public string KindName { get; set; }
        public string ParentName { get; set; }
        public string StateName { get; set; }
        public string TypeName { get; set; }
        public string BuyUnitName { get; set; }
        public string SaleUnitName { get; set; }
        public string StockUnitName { get; set; }

    }
}

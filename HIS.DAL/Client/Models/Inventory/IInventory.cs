﻿namespace HIS.DAL.Client.Models.Inventory
{
    public class IInventory
    {
        public long? BAId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public long? ParentId { get; set; }
        public string ParentName { get; set; }
        public long? CAId { get; set; }
        public string CAName { get; set; }
        public int? BuyUnitId { get; set; }
        public int? SaleUnitId { get; set; }
        public int? StockUnitId { get; set; }
        public int? KindId { get; set; }
        public int? TypeId { get; set; }
        public int? ResourceTypeId { get; set; }



    }
}

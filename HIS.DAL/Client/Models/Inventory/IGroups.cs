﻿namespace HIS.DAL.Client.Models.Inventory
{
    public class IGroups
    {
        public long? BAId { get; set; }
        public string Name { get; set; }
        public long? ParentId { get; set; }
        public string Note { get; set; }
    }
}

﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Models.Inventory
{
    public class InventoryData
    {

        public long? BAId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public long? CompanyArea { get; set; }
        public string CompanyName { get; set; }
        public long? Inventory { get; set; }
        public string InventoryName { get; set; }
        public int? BuyNatMeaning { get; set; }
        public int? SaleNatMeaning { get; set; }
        public int? StockNatMeaning { get; set; }
        public int? InventoryKind { get; set; }
        public int? InventoryType { get; set; }
        public int? ResourceTypeId { get; set; }
        public int? SOState { get; set; }
        public IEnumerable<MarkerValue> MarkerValues { get; set; }
        
    }
}

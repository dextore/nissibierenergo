﻿namespace HIS.DAL.Client.Models.Inventory
{
    public class LSCompanyAreas
    {
        public long? BAId { get; set; }
        public string Name { get; set; }
        public long? LSId { get; set; }
        public bool IsActive { get; set; }

    }
}

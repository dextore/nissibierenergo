﻿namespace HIS.DAL.Client.Models.Inventory
{
    public class InventoryListItem
    {

        public long? BAId { get; set; }
        public int BATypeId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public long? CAId { get; set; }
        public string CompanyName { get; set; }
        public int? BuyUnitId { get; set; }
        public string BuyUnitName { get; set; }
        public int? SaleUnitId { get; set; }
        public string SaleUnitName { get; set; }
        public int? StockUnitId { get; set; }
        public string StockUnitName { get; set; }
        public int? KindId { get; set; }
        public string KindName { get; set; }
        public long? ParentId { get; set; }
        public string ParentName { get; set; }
        public int? TypeId { get; set; }
        public string TypeName { get; set; }
        public int? StateId { get; set; }
        public string StateName { get; set; }
 



    }
}

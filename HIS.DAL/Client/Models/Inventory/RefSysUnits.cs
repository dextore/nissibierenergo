﻿
namespace HIS.DAL.Client.Models.Inventory
{
    public class RefSysUnits
    {
        public int ID { get; set; }
        public int UnitId { get; set; }
        public string Name { get; set; }
        public int UnitClassId { get; set; }
        public long? Code { get; set; }
        public string Symbol { get; set; }
    }
}

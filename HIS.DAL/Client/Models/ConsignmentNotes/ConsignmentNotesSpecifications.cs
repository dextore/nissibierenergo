﻿namespace HIS.DAL.Client.Models.ConsignmentNotes
{
    public class ConsignmentNotesSpecifications
    {
        public decimal PVAT { get; set; }
        public long RecId { get; set; }
	    public string InventoryName { get; set; }
	    public string ConsignmentNotesDocumentName { get; set; }
	    public string UnitName { get; set; }
	    public double Quantity { get; set; }
	    public decimal Price { get; set; }
	    public decimal PriceVAT { get; set; }
	    public bool IsVATIncluded { get; set; }
	    public int VATRate { get; set; }
	    public decimal Cost { get; set; }
	    public decimal VAT { get; set; }
	    public decimal CostVAT { get; set; }
    }

    public class ConsignmentNotesSpecificationsUpdate
    {
        public decimal PVAT { get; set; }
        public long? RecId { get; set; }
        public long InventoryId { get; set; }
        public long DocId { get; set; }
        public long UnitId { get; set; }
        public double Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal PriceVAT { get; set; }
        public bool IsVATIncluded { get; set; }
        public int VATRate { get; set; }
        public decimal Cost { get; set; }
        public decimal VAT { get; set; }
        public decimal CostVAT { get; set; }
    }

    public class ConsignmentNotesSpecificationsFull : ConsignmentNotesSpecifications
    {
        public long InventoryId { get; set; }
        public long DocId { get; set; }
        public long UnitId { get; set; }
    }
}

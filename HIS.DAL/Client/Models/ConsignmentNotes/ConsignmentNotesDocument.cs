﻿using System;

namespace HIS.DAL.Client.Models.ConsignmentNotes
{
    public class ConsignmentNotesDocument
    {
        public long? BAId { get; set; }
        public int BaTypeId { get; set; }
        public int StateId { get; set; }
        public string StateDisplay { get; set; }
        public string Number { get; set; }
	    public DateTime? DocDate { get; set; }
        public string CompanyAreaName { get; set; }
        public string LegalSubjectsName { get; set; }
        public string ContractName { get; set; }
        public string WarehouseName { get; set; }
	    public decimal? Cost { get; set; }
	    public decimal? VAT { get; set; }
	    public decimal? CostVAT { get; set; }
        public string UserName { get; set; }
        public string Note { get; set; }
    }

    public class ConsignmentNotesDocumentUpdate
    {
        public long? BAId { get; set; }
        public int BaTypeId { get; set; }
        public int StateId { get; set; }
        public string Number { get; set; }
        public DateTime? DocDate { get; set; }
        public long? CAId { get; set; }
        public long? LSId { get; set; }
        public long? ContractId { get; set; }
        public long? WHId { get; set; }
        public decimal? Cost { get; set; }
        public decimal? VAT { get; set; }
        public decimal? CostVAT { get; set; }
        public int? UserId { get; set; }
        public string Note { get; set; }
    }

    public class ConsignmentNotesFullDocument : ConsignmentNotesDocument
    {
        public string CompanyAreaId { get; set; }
        public string LegalSubjectId { get; set; }
        public string ContractId { get; set; }
        public string WhWarehousesId { get; set; }
    }
}

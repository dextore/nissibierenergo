﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.Markers
{
    public class EntityUpdate
    {
        public long? BAId { get; set; }
        public int BATypeId { get; set; }
        public IEnumerable<MarkerValue> MarkerValues { get; set; }

    }
}

﻿using System;

namespace HIS.DAL.Client.Models.Markers
{

    public class PeriodicMarkerValue
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Value { get; set; }
        public string Note { get; set; }
        public string DisplayValue { get; set; }
    }
}

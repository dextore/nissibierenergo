﻿namespace HIS.DAL.Client.Models.Markers
{
    public class OptionalMarkers
    {
        public int BATypeId { get; set; }
        public int OptionalMarkerId { get; set; }
        public int MarkerId { get; set; }
        public bool IsVisible { get; set; }

    }
}

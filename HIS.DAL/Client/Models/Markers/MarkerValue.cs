﻿using System;

namespace HIS.DAL.Client.Models.Markers
{
    public class MarkerValue
    {
        public int MarkerId { get; set; }
        public int MarkerType { get; set; }
        public string MVCAliase { get; set; }
        public int? ItemId { get; set; }
        public string Value { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsVisible { get; set; }
        public bool IsBlocked { get; set; }
        public string Note { get; set; }
        public string DisplayValue { get; set; }
        public bool IsDeleted { get; set; } = false;
    }
}

﻿namespace HIS.DAL.Client.Models.Markers
{
    public class CatalogMarkerValueItems
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int? OrderNum { get; set; }
    }
}

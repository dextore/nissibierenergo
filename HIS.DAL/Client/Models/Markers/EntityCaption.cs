﻿namespace HIS.DAL.Client.Models.Markers
{
    public class EntityCaption
    {
        public long BAId { get; set; }
        public string Caption { get; set; }
    }
}

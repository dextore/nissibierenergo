﻿namespace HIS.DAL.Client.Models.Markers
{
    public class MarkerValueInfo: MarkerValueBase
    {
        public string MarkerMvcAlias { get; set; }
    }
}

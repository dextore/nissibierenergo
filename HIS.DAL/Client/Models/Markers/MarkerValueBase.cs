﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.Markers
{
    public abstract class MarkerValueBase
    {
        public int MarkerId { get; set; }
        public string MarkerName { get; set; }
        public int MarkerTypeId { get; set; }
        public int? EntityTypeId { get; set; }
        public string MarkerType { get; set; }
        public string Value { get; set; }
        public IEnumerable<MarkerValueListItem> Range { get; set; }
    }
}

﻿namespace HIS.DAL.Client.Models.Markers
{
    public class MarkerItemValue
    {
        public int MarkerId;
        public int ItemId;
        public string Value;
    }
}

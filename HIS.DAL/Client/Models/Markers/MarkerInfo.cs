﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.Markers
{
    public class MarkerInfo
    {
        public int Id { get; set; }
        public int? RefBaTypeId { get; set; }
        public int MarkerType { get; set; }
        public string Name { get; set; } // MVCAlias
        public string Label { get; set; } // Name
        public bool IsPeriodic { get; set; }
        public bool IsRequired { get; set; }
        public bool IsCollectible { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsImplemented { get; set; }
        public bool IsOptional { get; set; }
        public string ImplementTypeName { get; set; }
        public string CatalogImplementTypeName { get; set; }
        public string ImplementTypeField { get; set; }
        public IEnumerable<ListItem> List { get; set; }
        //public IEnumerable<ListItem> Optionals 
        public int? SearchTypeId { get; set; }
    }

    public class ListItem
    {
        public int ItemId { get; set; }
        public string Value { get; set; }
    }
}

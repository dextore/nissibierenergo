﻿namespace HIS.DAL.Client.Models.Markers
{
    public class ReferenceMarkerValueItem
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
    }
}

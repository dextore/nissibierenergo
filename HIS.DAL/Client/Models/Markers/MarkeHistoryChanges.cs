﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.Markers
{
    public class MarkeHistoryChanges
    {
        public long BaId { get; set; }
        public int MarkerId { get; set; }
        public IEnumerable<PeriodicMarkerValue> Removed { get; set; }
        public IEnumerable<PeriodicMarkerValue> Changes { get; set; }
    }
}

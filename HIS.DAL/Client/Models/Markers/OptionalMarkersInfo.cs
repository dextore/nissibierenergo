﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.Markers
{
    public class OptionalMarkersInfo
    {
        public long BATypeId { get; set; }
        public int MarkerId { get; set; }
        public bool IsVisible { get; set; }
        public int OptionalMarkerId { get; set; }
        public IEnumerable<OptionalMarker> Optionals { get; set; }
    }


    public class OptionalMarker
    {
        public int MarkerId { get; set; }
        public IEnumerable<OptionalItem> Items { get; set; }
    }

    public class OptionalItem
    {
        public int ItemId { get; set; }
        public bool IsVisible { get; set; }
    }
}

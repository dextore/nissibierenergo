﻿namespace HIS.DAL.Client.Models.Markers
{
    public class MarkerValueListItem
    {
        public int MarkerId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

﻿namespace HIS.DAL.Client.Models.Markers
{
    public class MarkerValueRet
    {
        public int MarkerId { get; set; }
        public string Value { get; set; }
    }
}

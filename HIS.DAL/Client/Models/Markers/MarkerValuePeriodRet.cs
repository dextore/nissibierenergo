﻿using System;

namespace HIS.DAL.Client.Models.Markers
{
    public class MarkerValuePeriodRet
    {
        public int MarkerId { get; set; }
        public DateTime? StartDate { get; set; }
        public string Value { get; set; }
        public bool Deleted { get; set; }
    }
}

﻿using System;

namespace HIS.DAL.Client.Models.Markers
{
    public class MarkerItemValuePeriod
    {
        public long BAId;
        public int MarkerId;
        public int? ItemId;
        public string Value;
        public DateTime StartDate;
        public DateTime? EndDate;
    }
}

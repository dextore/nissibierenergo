﻿using System;

namespace HIS.DAL.Client.Models.Markers
{
    public class MarkerValueSet
    {
        public long BAId { get; set; }
        public int MarkerId { get; set; }
        public string Value { get; set; }
        public DateTime StartDate { get; set; }
    }
}

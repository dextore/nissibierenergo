﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.Markers
{
    public class EntityMarkersValueList
    {
        public int MarkerId { get; set; }
        public IEnumerable<MarkerItemValue> Values { get; set; } 
    }
}

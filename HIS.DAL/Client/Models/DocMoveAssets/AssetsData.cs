﻿using System;

namespace HIS.DAL.Client.Models.DocMoveAssets
{
    public class AssetsData
    {
        public long BAId { get; set; }
        public int BATypeId { get; set; }
        public int StateId { get; set; }
        public string InventoryNumber { get; set; }
        public string SerialNumber { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public long? FADocAcceptId { get; set; }
        public long? InventoryId { get; set; }
        public long? MLPId { get; set; }
        public long? LSId { get; set; }
        public string BarCode { get; set; }
        public long? EmployeeId { get; set; }
        public DateTime? CheckDate { get; set; }



    }
}


﻿using System;
using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Models.DocMoveAssets
{
    public class DocMoveAssetsData
    {
        public long? BAId { get; set; }
        public int? BaTypeId { get; set; }
        public int? StateId { get; set; }
        public string Number { get; set; }
        public DateTime? DocDate { get; set; }
        public long? SrcLSId { get; set; }
        public string SrcLSName { get; set; }
        public long? DstLSId { get; set; }
        public string DstLSName { get; set; }
        public long? SrcMLPId { get; set; }
        public string SrcMLPName { get; set; }
        public long? DstMLPId { get; set; }
        public string DstMLPName { get; set; }
        public int? SrcLocationId { get; set; }
        public int? DstLocationId { get; set; }
        public long? CAId { get; set; }
        public string CAName { get; set; }
        public long? ReasonId { get; set; }
        public string Note { get; set; }
        public string Evidence { get; set; }
        public bool HasAssets { get; set; }
        public IEnumerable<MarkerValue> MarkerValues { get; set; }

       

    }
}


﻿using System;
namespace HIS.DAL.Client.Models.DocMoveAssets
{
    public class DocMoveAssetsRow
    {
        public long? RecId { get; set; }
        public long FADocMovId { get; set; }
        public long FAId { get; set; }
        public string FAName { get; set; }
        public string SerialNumber { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public DateTime? CheckDate { get; set; }
        public string BarCode { get; set; }
        public long? EvidenceId { get; set; }
        public string EvidenceName { get; set; }
        public string Evidence { get; set; }
    }
}

﻿using System;

namespace HIS.DAL.Client.Models.InventoryObjectsMovementHistory
{

    public class InventoryAssetsMovementHistory
    {
        public long? RecId { get; set; }
        public long InventoryAssetId { get; set; }
        public string InventoryAssetNumber { get; set; }
        public long? DocId { get; set; }
        public string DocName { get; set; }
        public long? MRPId { get; set; }
        public string MRPName { get; set; }
        public string Location { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}

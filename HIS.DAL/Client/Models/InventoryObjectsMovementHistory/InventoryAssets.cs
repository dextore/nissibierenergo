﻿using System;

namespace HIS.DAL.Client.Models.InventoryObjectsMovementHistory
{
    public class InventoryAssets
    {
        public long? BAId { get; set; }
        public string InventoryNumber { get; set; }
        public string SerialNumber { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string DocName { get; set; }
        public string InventoryName { get; set; }
        public string CompanyAreaName { get; set; }
        public string MRPName { get; set; }
        public string LSName { get; set; }
        public string BarCode { get; set; }
        public string EmployeeName { get; set; }
        public string Location { get; set; }
        public DateTime? CheckDate { get; set; }
        public string State { get; set; }
        public int StateId { get; set; }
    }
}

﻿using System;
using HIS.Models.Layer.Models.Address;

namespace HIS.DAL.Client.Models.Address
{
    public class AddressObject
    {
        public Guid AOId { get; set; }
        public Guid AOGuid { get; set; }
        public string FormalName { get; set; }
        public string ShortName { get; set; }
        public AddressSourceType SourceType { get; set; }
        public string PostCode { get; set; }
    }
}

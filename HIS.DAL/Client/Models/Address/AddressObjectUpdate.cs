﻿using System;

namespace HIS.DAL.Client.Models.Address
{
    public class AddressObjectUpdate
    {
        public Guid? AOId { get; set; }
        public Guid? AOGuid { get; set; }
        public string FormalName { get; set; }
        public int AOLevel { get; set; }
        public string ShortName { get; set; }
        public Guid? ParentId { get; set; }
        public string PostCode { get; set; }
        public string CadNum { get; set; }

    }
}

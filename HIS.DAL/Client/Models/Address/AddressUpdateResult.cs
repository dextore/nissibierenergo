﻿namespace HIS.DAL.Client.Models.Address
{
    public class AddressUpdateResult
    {
        public long BaId { get; set; }
        public string Name { get; set; }
    }
}

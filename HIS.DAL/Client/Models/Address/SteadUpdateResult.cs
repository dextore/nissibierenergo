﻿using System;

namespace HIS.DAL.Client.Models.Address
{
    public class SteadUpdateResult
    {
        public Guid SteadId { get; set; }
        public Guid SteadGuid { get; set; }
        public bool IsError { get; set; }
        public string ErrorDescription { get; set; }

    }
}

﻿using System;

namespace HIS.DAL.Client.Models.Address
{
    public class SteadUpdate
    {
        public Guid? SteadId { get; set; }
        public Guid? SteadGuid { get; set; }		
        public string Number { get; set; }
        public Guid? ParentId { get; set; }
        public Guid? ParentGuid { get; set; }
        public string PostCode { get; set; }
        public string CadNum { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
    }
}

﻿using System;

namespace HIS.DAL.Client.Models.Address
{
    public class HouseUpdateResult
    {
        public Guid HouseId { get; set; }
        public Guid HouseGuid { get; set; }
        public bool IsError { get; set; }
        public string ErrorDescription { get; set; }

    }
}

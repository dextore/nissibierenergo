﻿using System;

namespace HIS.DAL.Client.Models.Address
{
    public class AddressEntity
    {
        public long BaId { get; set; }
        public string Name { get; set; }
        public Guid? AOId { get; set; }
        public Guid? AOGuid { get; set; }
        public Guid? HouseGuid { get; set; }
        public Guid? HouseId { get; set; }
        public Guid? SteadGuid { get; set; }
        public Guid? SteadId { get; set; }
        public string Location { get; set; }
        public string FlatNum { get; set; }
        public int? FlatTypeId { get; set; }
        public string POBox { get; set; }
        public bool IsBuild { get; set; }
        public bool IsActual { get; set; }
    }
}

﻿using System;

namespace HIS.DAL.Client.Models.Address
{
    public class AddressObjectUpdateResult
    {
        public Guid AOId { get; set; }
        public Guid AOGuid { get; set; }
        public bool IsError { get; set; }
        public string ErrorDescription { get; set; }

    }
}

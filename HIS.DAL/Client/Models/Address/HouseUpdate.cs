﻿using System;
using HIS.Models.Layer.Models.Address;

namespace HIS.DAL.Client.Models.Address
{
    public class HouseUpdate
    {
        public Guid? HouseId { get; set; }
        public Guid? HouseGuid { get; set; }
        public string HouseNum { get; set; }
        public string BuildNum { get; set; }
        public string StructNum { get; set; }
        public int? StrStateId { get; set; }
        public int? EstStateId { get; set; }
        public Guid? AOId { get; set; }
        public Guid? ParentGuid { get; set; }
        public AddressSourceType SourceType { get; set; }
        public string PostCode { get; set; }
        public string CadNum { get; set; }
        public bool IsBuild { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
    }

}

﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Models
{
    public abstract class EntityBase
    {
        public long? BAId { get; set; }
        public IEnumerable<MarkerValue> MarkerValues { get; set; }
    }
}

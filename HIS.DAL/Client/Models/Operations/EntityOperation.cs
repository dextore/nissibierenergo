﻿namespace HIS.DAL.Client.Models.Operations
{
    /// <summary>
    /// Операция сущности
    /// </summary>
    public class EntityOperation
    {
        /// <summary>
        /// Тип сущьности
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// Идентификатор операции
        /// </summary>
        public int OperationId { get; set; }

        /// <summary>
        /// Наименование операции
        /// </summary>
        public string OperationName { get; set; }

        /// <summary>
        /// Начальное состояния
        /// </summary>
        public bool IsFirstState { get; set; }

        /// <summary>
        ///  Состояние удален
        /// </summary>
        public bool IsDelState { get; set; }


        /// <summary>
        /// Идентификатор состояние сущности
        /// </summary>
        public int SrcStateId { get; set; }

        /// <summary>
        /// Идентификатор целевого состояния сущности
        /// </summary>
        public int DestStateId { get; set; }

        /// <summary>
        /// Заблокированно
        /// </summary>
        public bool IsBlocked { get; set; }
    }
}

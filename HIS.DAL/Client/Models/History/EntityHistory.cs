﻿using System;


namespace HIS.DAL.Client.Models.History
{
    public class EntityHistory
    {
        public long BAId { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public DateTime OperationDate { get; set; }
        public string OperationName { get; set; }
        public int OperationId { get; set; }
        public int? SrcStateId { get; set; }
        public string SrcStateName { get; set; }
        public int? DstStateId { get; set; }
        public string DstStateName { get; set; }
        public int? MarkerId { get; set; }
        public string MarkerName { get; set; }
        public string MarkerValue { get; set; }
        public string OldMarkerValue { get; set; }
        public long? MarkerValueBAId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? OldStartDate { get; set; }
        public DateTime? OldEndDate { get; set; }
        public int? ItemId { get; set; }
        public long? RecId { get; set; }
        public long? GRecId { get; set; }

        public long Color { get; set; }
    }
}

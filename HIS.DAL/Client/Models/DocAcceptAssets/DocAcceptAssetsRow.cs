﻿using System;
namespace HIS.DAL.Client.Models.DocAcceptAssets
{
    public class DocAcceptAssetsRow
    {
        public long? RecId { get; set; }
        public long FADocAcceptId { get; set; }
        public long InventoryId { get; set; }
        public string InventoryName { get; set; }
        public string InventoryNumber { get; set; }
        public string SerialNumber { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public DateTime? CheckDate { get; set; }
        public string BarCode { get; set; }
        public long? EmployeeId { get; set; }
        public long? FAId { get; set; }
    }
}

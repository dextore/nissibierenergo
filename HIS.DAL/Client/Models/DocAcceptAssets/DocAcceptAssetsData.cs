﻿using System;
using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Models.DocAcceptAssets
{
    public class DocAcceptAssetsData
    {
        public long? BAId { get; set; }
        public int? BaTypeId { get; set; }
        public int? StateId { get; set; }
        public string Number { get; set; }
        public DateTime? DocDate { get; set; }
        public long? RIId { get; set; }
        public string RIName { get; set; }
        public long? LSId { get; set; }
        public string LSName { get; set; }
        public long? WHId { get; set; }
        public string WHName { get; set; }
        public long? CAId { get; set; }
        public string CAName { get; set; }
        public string Note { get; set; }
        public string Evidence { get; set; }
        public bool HasSpecification { get; set; }
        public IEnumerable<MarkerValue> MarkerValues { get; set; }

    }
}


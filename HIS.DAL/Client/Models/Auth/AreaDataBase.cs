﻿namespace HIS.DAL.Client.Models.Auth
{
    /// <summary>
    /// Модель описания БД для работы
    /// </summary>
    public class AreaDataBase
    {
        /// <summary>
        /// Алиас
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// Наименование БД
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Описание БД
        /// </summary>
        public string Desc { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.DAL.Client.Models.BaseAncestors
{
    public class StateInfo
    {
        public int BATypeId { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public bool IsFirstState { get; set; }
        public bool IsDelState { get; set; }
    }
}

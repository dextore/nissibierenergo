﻿namespace HIS.DAL.Client.Models.BaseAncestors
{
    public class EntityState
    {
        public long BAId { get; set; }
        public int BATypeId { get; set; }
        public string TypeName { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public bool IsFirstState { get; set; }
        public bool IsDelState { get; set; }

    }
}

﻿using HIS.DAL.Client.Models.LegalSubjects.Persons;

namespace HIS.DAL.Client.Models.Suppliers
{
    public class SupplierItem: LegalPersonImplemented
    {
        public long SupplierId { get; set; }
    }
}

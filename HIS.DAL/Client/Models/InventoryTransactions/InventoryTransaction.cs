﻿using System;

namespace HIS.DAL.Client.Models.InventoryTransactions
{
    public class InventoryTransaction
    {
        public long RecId { get; set; }
        public long CAId { get; set; }
        public string CompanyName { get; set; }
        public long DocId { get; set; }
        public long InventoryId { get; set; }
        public string InventoryName { get; set; }
        public long AccId { get; set; }
        public string AccountName { get; set; }
        public string AccountNum { get; set; }
        public long WarehouseId { get; set; }
        public string WarehouseName { get; set; }
        public int RecTypeId { get; set; }
        public string RecTypeName { get; set; }
        public bool IsIncome { get; set; }
        public double Quantity { get; set; }
        public decimal VAT { get; set; }
        public double PriceVAT { get; set; }
        public bool IsVATIncluded { get; set; }
        public decimal? StoreCost { get; set; }
        public decimal? SellCost { get; set; }
        public decimal? SellCostVAT { get; set; }
        public DateTime OperationDate { get; set; }
    }   
}

﻿namespace HIS.DAL.Client.Models.Catalogs
{
    public abstract class LegalSubjectFormBase
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public bool IsActive { get; set; }
        public int OrderNum { get; set; }
    }
}

﻿using System.Data;

namespace HIS.DAL.Client.Models.AdminPanel
{
    public class MarkersAdminView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string MvcAlias { get; set; }
        public string BaTypeLink { get; set; }
        public int? BaTypeLinkId { get; set; }
        public string MarkerType { get; set; }
        public bool IsPeriodic { get; set; }
        public bool IsCollectible { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsFixMarkerHistory { get; set; }
        public bool IsImplementTypeField { get; set; }
        public bool IsInheritToDescendant { get; set; }
        public bool IsRequired { get; set; }
        public bool IsOptional { get; set; }
        public string ImplementTypeName { get; set; }
        public string ImplementTypeField { get; set; }
        public string OverrideName { get; set; }
        
    }

    public class OnlyMarkersDataView
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string MvcAlias { get; set; }
        public string BaTypeLink { get; set; }
        public int? BaTypeLinkId { get; set; }
        public string MarkerType { get; set; }
        public string ImplementTypeName { get; set; }
        public int MarkerTypeId { get; set; }
    }
}

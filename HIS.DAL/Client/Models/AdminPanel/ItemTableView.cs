﻿namespace HIS.DAL.Client.Models.AdminPanel
{
    public class ItemTableView
    {
        public int? ItemId { get; set; }
        public string ItemName { get; set; }
        public bool IsActive { get; set; }
        public int OrderNumber { get; set; }
    }
}

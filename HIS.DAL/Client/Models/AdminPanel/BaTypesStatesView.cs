﻿namespace HIS.DAL.Client.Models.AdminPanel
{
    public class BaTypesStatesView
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
        public bool IsFirstState { get; set; }
        public bool IsDelState { get; set; }
    }
    public class BaTypesStatesCreateView
    {
        public int? StateId { get; set; }
        public string StateName { get; set; }
    }
}

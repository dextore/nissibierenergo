﻿namespace HIS.DAL.Client.Models.AdminPanel
{
    public class BaTypesOperationsView
    {
        public int OperationId { get; set; }
        public string OperationName { get; set; }
        public bool IsCreateOperation { get; set; }
        public bool IsDeleteOperation { get; set; }
    }

    public class BaTypesOperationsCreateView
    {
        public int? OperationId { get; set; }
        public string OperationName { get; set; }
    }
}

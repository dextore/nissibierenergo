﻿namespace HIS.DAL.Client.Models.AdminPanel
{
    public class BaTypesView
    {
        public long? TypeId { get; set; }
        public string Name { get; set; }
        public string MvcAlias { get; set; }
        public string Parent { get; set; }
        public long? ParentId { get; set; }
        public bool FixOperationHistory { get; set; }
        public string IsImplementTypeName { get; set; }
        public bool IsImplementType { get; set; }
        public string SearchType { get; set; }
        public long? SearchTypeId { get; set; }
        public bool IsGroupType { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
    }
}

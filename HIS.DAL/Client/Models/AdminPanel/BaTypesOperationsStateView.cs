﻿namespace HIS.DAL.Client.Models.AdminPanel
{
    public class BaTypesOperationsStateView
    {
        public int OperationId { get; set; }
        public string OperationName { get; set; }
        public string SourceStateName { get; set; }
        public int SourceStateId { get; set; }
        public string DestinationStateName { get; set; }
        public bool IsBlocked { get; set; }
    }

    public class BaTypesOperationsStateCreateView
    {
        public BaTypesOperationsCreateView Operation { get; set; }
        public BaTypesStatesCreateView StartState { get; set; }
        public BaTypesStatesCreateView EndState { get; set; }
        public bool IsBlocked { get; set; }
    }
}
 
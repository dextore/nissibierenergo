﻿using System;

namespace HIS.DAL.Client.Models.Banks
{
    public class BankMarker
    {
        public int Baid { get; set; }
        public int MarkerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Value { get; set; }
        public string Note { get; set; }
    }
}

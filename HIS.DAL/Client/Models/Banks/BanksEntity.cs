﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.Banks
{
    public class BanksEntity
    {
        public long? BaId { get; set; }
        public int? BATypeId { get; set; }
        public int? ParentId { get; set; }
        public string Bik { get; set; }
        public string Inn { get; set; }
        public string Rkc { get; set; }
        public string Okpo { get; set; }
        public long? AddrId { get; set; }
        public string DisplayAddr { get; set; }

        public string BankBranchDisplayValue { get; set; }

        public string Name { get; set; }
        public string CorespondetsAccount { get; set; }
        public long AcceptancePeriod { get; set; }
        //public DateTime? ChangeDate { get; set; }
        //public DateTime? ActionDate {get;set;}
        public string Status { get; set; }
        public IEnumerable<BankMarker> Markers { get; set; }
    }
}

﻿using System;

namespace HIS.DAL.Client.Models.Common
{
    /// <summary>
    /// Информация о сущности полнотекстового поиска
    /// </summary>
    public class FullTextSearchEntityInfoItem
    {
        /// <summary>
        /// ИД сущности
        /// </summary>
        public long BaId { get; set; }
        /// <summary>
        /// ИД типа сущности
        /// </summary>
        public int TypeId { get; set; }
        /// <summary>
        /// Наименование типа сущности
        /// </summary>
        public string TypeName { get; set; }
        /// <summary>
        /// ИД состояния сущности
        /// </summary>
        public int StateId { get; set; }
        /// <summary>
        /// Наименование состояния сущности
        /// </summary>
        public string StateName { get; set; }
        /// <summary>
        /// Дата начала действия имени сущности
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Дата окончания действия имени сущности
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// Наименование сущности
        /// </summary>
        public string Name { get; set; }
    }
}

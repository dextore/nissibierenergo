﻿namespace HIS.DAL.Client.Models.Common
{
    public class EntityViewItem
    {
        public int MarkerId { get; set; }
        public string ViewName { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public bool IsActive { get; set; }
        public int OrderNum { get; set; }
    }


}

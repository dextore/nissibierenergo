﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.Common
{
    /// <summary>
    /// Коллекция элементов дерева
    /// </summary>
    public class EntityInfoTreeCollection
    {
        /// <summary>
        /// Элементы дерева
        /// </summary>
        public IEnumerable<EntityInfoTreeItem> Items { get; set; }
    }
}

﻿namespace HIS.DAL.Client.Models.Common
{
    /// <summary>
    /// Модель состояния элемента управления
    /// </summary>
    public class StateStorageItem
    {
        /// <summary>
        /// Ключ (наименование сосотояния)
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// Значение
        /// </summary>
        public string Value { get; set; }
    }
}

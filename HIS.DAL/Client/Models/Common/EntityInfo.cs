﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Models.Common
{
    public class EntityInfo
    {
        public int BATypeId { get; set; }
        public string Name { get; set; }
        public string MVCAlias { get; set; }
        public int? ParentBATypeId { get; set; }
        public bool IsImplementType { get; set; }
        public bool IsGroupType { get; set; }
        public string ImplementTypeName { get; set; }
        public int SearchTypeId { get; set; }
        public IEnumerable<EntityStates> States { get; set; }
        public IEnumerable<MarkerInfo> MarkersInfo { get; set; }
    }
    public class EntityStates
    {
        public int StateId { get; set; }
        public string Name { get; set; }
    }
}

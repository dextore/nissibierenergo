﻿namespace HIS.DAL.Client.Models.Common
{
    /// <summary>
    /// Тыпы системного поиска
    /// </summary>
    public enum SystemSearchTypes
    {
        /// <summary>
        /// По коду договора
        /// </summary>
        CONTRACT_CODE = 0x0101,
        /// <summary>
        /// По наименованию
        /// </summary>
        NAME = 0x0102,
        /// <summary>
        /// По ИНН
        /// </summary>
        INN = 0x0103,
        /// <summary>
        /// По адресу
        /// </summary>
        ADDRESS = 0x0104
    }
}

﻿namespace HIS.DAL.Client.Models.Common
{
    /// <summary>
    /// Модель - константа
    /// </summary>
    public class Constant
    {
        /// <summary>
        /// Наименование константы
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Значение константы
        /// </summary>
        public string Value { get; set; }
    }
}

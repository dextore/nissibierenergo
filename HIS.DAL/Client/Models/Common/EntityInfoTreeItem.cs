﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.Common
{
    /// <summary>
    /// Элемент дерева
    /// </summary>
    public class EntityInfoTreeItem: EntityInfoItem
    {
        /// <summary>
        /// ИД в дереве 
        /// </summary>
        public long TreeId { get; set; }
        /// <summary>
        /// Ссылка на родительские элемент в дереве
        /// </summary>
        public long? ParentTreeId { get; set; }
        /// <summary>
        /// Признак наличия вложенных элементов
        /// </summary>
        public bool IsHasChildrenItems { get; set; }
        /// <summary>
        /// Родительский идентификатор именно СУЩНОСТИ для текущего элемента дерева
        /// </summary>
        public long? ParentBaId { get; set; }
        /// <summary>
        /// Вложенные элементы сущности
        /// </summary>
        public IEnumerable<EntityInfoTreeItem> Items { get; set; }

    }
}

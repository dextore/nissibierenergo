﻿namespace HIS.DAL.Client.Models.Common
{
    /// <summary>
    /// Информация о сущности
    /// </summary>
    public class EntityInfoItem
    {
        /// <summary>
        /// ИД сущности
        /// </summary>
        public long BaId { get; set; }
        /// <summary>
        /// Тип сущности в "дереве" системы
        /// </summary>
        public EntityTreeTypes TreeType { get; set; }
        /// <summary>
        /// ИД типа сущности
        /// </summary>
        public int? TypeId { get; set; }
        /// <summary>
        /// Наименование типа сущности
        /// </summary>
        public string TypeName { get; set; }
        /// <summary>
        /// Алиас типа сущности
        /// </summary>
        public string MVCAlias { get; set; }
        /// <summary>
        /// ИД родительского типа сущности
        /// </summary>
        public int? ParentTypeId { get; set; }
        /// <summary>
        /// Наименование реализации типа сущности
        /// </summary>
        public string ImplementTypeName { get; set; }
        /// <summary>
        /// ИД состояния сущности
        /// </summary>
        public int? StateId { get; set; }
        /// <summary>
        /// Наименование состояния сущности
        /// </summary>
        public string StateName { get; set; }
        /// <summary>
        /// Код сущности
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Наименование сущности
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Полное наименование
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Описание сущности
        /// </summary>
        public string Desc { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.Common
{
    /// <summary>
    /// Коллекция сущностей полнотекстового поиска
    /// </summary>
    public class FullTextSearchEntityInfoItemCollection
    {
        /// <summary>
        /// Элементы коллекции сущностей полнотекстового поиска
        /// </summary>
        public IEnumerable<FullTextSearchEntityInfoItem> Items { get; set; }
    }
}

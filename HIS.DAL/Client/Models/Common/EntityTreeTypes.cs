﻿namespace HIS.DAL.Client.Models.Common
{
    //
    // Типы сущностей в "деревьях" системы
    //
    public enum EntityTreeTypes
    {
        // Рутовая сущность в системе
        SO_ROOT = 0x0101,
        // Элемент системы (реальная сущность, ориентироваться по typeId)
        SO_ELEMENT = 0x0102,
        // Группа элементов (виртуальная сущность, не заведенная в БД)
        SO_GROUP = 0x0103
    }
}

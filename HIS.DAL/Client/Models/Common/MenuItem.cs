﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.Common
{
    /// <summary>
    /// Элемент меню
    /// </summary>
    public class MenuItem
    {
        /// <summary>
        /// Наименование элемента
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание элемента
        /// </summary>
        public string Desc { get; set; }
       
        /// <summary>
        /// Список элементов данного элемента меню
        /// </summary>
        public IEnumerable<MenuItem> SubItems { get; set; }        
    }
}

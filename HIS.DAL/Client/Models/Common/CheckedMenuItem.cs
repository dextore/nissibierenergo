﻿namespace HIS.DAL.Client.Models.Common
{
    /// <summary>
    /// Элемент меню с возмощностью выбора (CheckBox)
    /// </summary>
    public class CheckedMenuItem: MenuItem
    {
        /// <summary>
        /// Признак "отмеченного" элемента
        /// </summary>
        public bool IsChecked { get; set; } = false;
    }
}

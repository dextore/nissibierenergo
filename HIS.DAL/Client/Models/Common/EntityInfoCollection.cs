﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.Common
{
    /// <summary>
    /// Коллекция описаний сущностей
    /// </summary>
    public class EntityInfoCollection
    {
        public IEnumerable<EntityInfoItem> Items { get; set; }
    }
}

﻿namespace HIS.DAL.Client.Models.SystemSearch
{
    public class SimpleSearchItem
    {
        public long BAId { get; set; }
        public int BATypeId { get; set; }
        public string TypeName { get; set; }
        public string StateName { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string INN { get; set; }
        public string LegalFormName { get; set; }
    }
}

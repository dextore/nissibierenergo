﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.SystemSearch
{
    public class SearchResult
    {
        public IEnumerable<SimpleSearchItem> Items { get; set; }
        public long RowCount { get; set; }
    }
}

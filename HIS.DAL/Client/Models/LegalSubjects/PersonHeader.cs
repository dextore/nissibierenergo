﻿namespace HIS.DAL.Client.Models.LegalSubjects
{
    public class PersonHeader
    {
        public long PersonId { get; set; }
        public string FullName { get; set; }
        public string Code { get; set; }
    }
}

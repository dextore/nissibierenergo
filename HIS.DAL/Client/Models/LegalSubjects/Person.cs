﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Models.LegalSubjects
{
    public class Person: PersonImplements
    {
        public IEnumerable<MarkerValue> MarkerValues { get; set; }
    }
}

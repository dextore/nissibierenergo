﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.LegalSubjects.Persons;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Models.LegalSubjects.AffiliateOrganisations
{
    public class AffiliateOrganisation: LegalPersonImplemented
    {
        public long ParentId { get; set; }
        public IEnumerable<MarkerValue> MarkerValues { get; set; }
    }
}

﻿using System;

namespace HIS.DAL.Client.Models.LegalSubjects
{
    public class PersonImplements
    {
        public long? PersonId { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public byte? Gender { get; set; }
        public string INN { get; set; }
    }
}

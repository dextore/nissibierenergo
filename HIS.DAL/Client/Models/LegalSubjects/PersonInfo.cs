﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.LegalSubjects
{
    public class PersonInfo : PersonInfoBase
    {
        public IEnumerable<long> MarkersId { get; set; }
    }
}

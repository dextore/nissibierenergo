﻿namespace HIS.DAL.Client.Models.LegalSubjects.CommercePerson
{
    public class CommercePersonUpdate : EntityBase
    {
        public long? ParentId { get; set; }
    }
}

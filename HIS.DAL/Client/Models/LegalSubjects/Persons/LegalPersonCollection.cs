﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.LegalSubjects.Persons
{
    /// <summary>
    /// Коллекция юридических лиц 
    /// </summary>
    public class LegalPersonCollection
    {
        public IEnumerable<LegalPersonImplemented> Items { get; set; }
        public int TotalCount { get; set; }
    }
}

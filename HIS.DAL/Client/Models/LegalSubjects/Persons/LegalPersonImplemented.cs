﻿namespace HIS.DAL.Client.Models.LegalSubjects.Persons
{
    public class LegalPersonImplemented
    {
        public long? LegalPersonId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string INN { get; set; }
        public string Note { get; set; }
        public int BATypeId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace HIS.DAL.Client.Models.LegalSubjects.Persons.Contacts
{
    public class Contact
    {
        public long? ContactId { get; set; }
        public DateTime StartDate { get; set; }
        public int ContactTypeId { get; set; }
        public string Note { get; set; }

        public bool Deleted { get; set; }
        public IEnumerable<ContactPhone> Phones { get; set; }
        public IEnumerable<ContactEmail> Emails { get; set; }
    }
}

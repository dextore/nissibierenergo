﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.LegalSubjects.Persons.Contacts
{
    public class ContactPhone
    {
        public long? PhoneId { get; set; }
        public int ItemId { get; set; }
        public int PhoneTypeId { get; set; }
        public string Number { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int? MailConfirmTypeId { get; set; }
        public bool Deleted { get; set; }
        public IEnumerable<MailType> MailTypes { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace HIS.DAL.Client.Models.LegalSubjects.Persons.Contacts
{
    public class ContactInfo
    {
        public long ContactId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ContactTypeId { get; set; }
        public string ContactTypeName { get; set; }
        public string Note { get; set; }
        public IEnumerable<ContactPhoneInfo> Phones { get; set; }
        public IEnumerable<ContactEmailInfo> Emails { get; set; }
    }
}

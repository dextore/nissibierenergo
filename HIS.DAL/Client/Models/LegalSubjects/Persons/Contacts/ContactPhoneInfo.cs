﻿using System.Collections.Generic;

namespace HIS.DAL.Client.Models.LegalSubjects.Persons.Contacts
{
    public class ContactPhoneInfo
    {
        public long ContactId { get; set; }
        public long PhoneId { get; set; }
        public string Number { get; set; }
        public int PhoneTypeId { get; set; }
        public string PhoneTypeName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int? MailConfirmTypeId { get; set; }
        public string MailConfirmTypeName { get; set; }
        public IEnumerable<MailTypeInfo> MailTypes { get; set; }
    }
}

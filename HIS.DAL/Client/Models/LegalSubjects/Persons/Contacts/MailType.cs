﻿namespace HIS.DAL.Client.Models.LegalSubjects.Persons.Contacts
{
    public class MailType
    {
        public int MarkerItemId { get; set; }
        public string Value { get; set; }
        public bool IsDeleted { get; set; }
    }
}

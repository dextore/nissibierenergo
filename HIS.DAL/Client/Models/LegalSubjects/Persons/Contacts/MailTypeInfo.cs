﻿namespace HIS.DAL.Client.Models.LegalSubjects.Persons.Contacts
{
    public class MailTypeInfo
    {
        public int MarkerItemId { get; set; }
        public string Value { get; set; }
        public string ItemName { get; set; }
    }
}

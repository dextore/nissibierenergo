﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Models.LegalSubjects.Persons
{
    public class LegalPerson: LegalPersonImplemented
    {
        public IEnumerable<MarkerValue> Markers { get; set; }
    }
}

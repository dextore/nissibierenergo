﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Models.LegalSubjects
{
    public class PersonUpdate: PersonImplements
    {
        public IEnumerable<MarkerValue> MarkerValues;
    }
}

﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Models.LegalSubjects
{
    public class PersonInfoResponse : PersonInfoBase
    {
        public IEnumerable<MarkerValue> Markers { get; set; }
    }
}

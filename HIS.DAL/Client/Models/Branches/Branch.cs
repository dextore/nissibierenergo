﻿namespace HIS.DAL.Client.Models.Branches
{
    public class Branch
    {
        public long? Baid { get; set; }
        public long ParentId { get; set; }
        public string BrnNum { get; set; }
        public string Name { get; set; }
        public int TreeLevel { get; set; }
    }
}

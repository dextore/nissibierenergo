﻿using System;

namespace HIS.DAL.Client.Models.Branches
{
    public class BranchItem : Branch
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public long? ItemId { get; set; }
    }
}

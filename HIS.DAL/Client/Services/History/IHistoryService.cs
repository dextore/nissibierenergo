﻿using HIS.DAL.Client.Models.History;
using System.Collections.Generic;

namespace HIS.DAL.Client.Services.History
{
    public interface IHistoryService
    {
        IEnumerable<EntityHistory> GetEntityStateHistory(long baId);
        IEnumerable<EntityHistory> GetEntityMarkersHistory(long baId);
        string GetResultQuery(long baId);
        string GetEntityData(long baId);
        
    }
}

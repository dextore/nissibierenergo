﻿using HIS.DAL.Client.Models.Elements;
using HIS.DAL.Client.Models.RegPoints;
using System.Collections.Generic;

namespace HIS.DAL.Client.Services.RegPoints
{
    public interface IRegPointsService
    {
        IEnumerable<Element> Get();
        Element GetItem(long baId);
        Element Save(ElementRet regPoint);
        IEnumerable<ElementExt> GetRegPoints();
        ElementExt GetRegPoint(long baId);
    }
}

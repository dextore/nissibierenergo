﻿using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Services.Entities
{
    public interface IEntitiesService
    {

        long Update(EntityUpdate model);
    }
}
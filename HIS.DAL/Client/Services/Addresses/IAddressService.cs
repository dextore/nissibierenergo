﻿using HIS.Models.Layer.Models.FIAS;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HIS.DAL.Client.Models.Address;
using HIS.DAL.DB.Context;
using HIS.Models.Layer.Models;
using HIS.Models.Layer.Models.Address;

namespace HIS.DAL.Client.Services.Fias
{
    public interface IAddressService
    {
        Task<IEnumerable<AddressFullName>> AddrSearchFull(string findStr);
        Task<AddressFullName> GetAddrFullByHouseId(Guid AOId, Guid houseId);
        Task<AddressFullName> GetAddrFullBySteadId(Guid AOId, Guid steadId);

        Task<AddressFullName> GetAddrFullByAOId(Guid AOId);

        //Task<IEnumerable<FiasFindedAddress>> SimpleAddrSearch(string findStr = null);
        //Task<FiasFindedAddress> SimpleAddrByAOID(Guid AOID);
        Task<AddressAOIdAOGuid> GetAddressInfo(Guid AOId);

        Task<IEnumerable<AddressCatalog>> GetRFSubjects(RequestModel model);
        Task<IEnumerable<AddressCatalog>> GetRegions(AddressRequestModel model);
        Task<IEnumerable<AddressCatalog>> GetCities(AddressRequestModel model);
        Task<IEnumerable<AddressCatalog>> GetSettlements(AddressRequestModel model);
        Task<IEnumerable<AddressCatalog>> GetElmStructures(AddressRequestModel model);
        Task<IEnumerable<AddressCatalog>> GetStreets(AddressRequestModel model);

        Task<IEnumerable<AddressCatalog>> GetFiasAddressObjetcList(AddressRequestModel model);

        //Task<IEnumerable<FiasAddTerritory>> GetAddTerritories(Guid? AOGuid = null);
        //Task<IEnumerable<FiasAddTerritoryStreet>> GetAddTerritoryStreets(Guid? AOGuid = null);
        //Task<IEnumerable<FiasAddressHouse>> GetAddressHouses(Guid? AOGuid = null);
        Task<IEnumerable<AddressHouse>> GetHouses(AddressRequestModel model);
        Task<IEnumerable<AddressStead>> GetSteads(AddressRequestModel model);
        Task<IEnumerable<AddressFindResult>> GetAddressList(AddressResultRequestModel model);
        Task<IEnumerable<FiasDetailAddress>> GetDetailAddrSearch(FiasDetailSearch searchModel);

        AddressUpdateResult AddressNameUpdate(long? baId, string newName);
        IEnumerable<FlatType> GetFlatTypeList(bool isVisible);
        Task<AddressHouse> GetHouse(Guid? AOGuid, Guid? houseId);
        Task<AddressStead> GetStead(Guid? AOGuid, Guid? steadId);
        HouseUpdate GetHouseUpdateInfo(Guid houseId);
        SteadUpdate GetSteadUpdateInfo(Guid steadId);
        Task<AddressCatalog> GetAddressObject(Guid? aoGuid);
        Task<AddressCatalog> GetAddressObjectCatalog(Guid aoGuid);
        Task<IEnumerable<AddressObjectTypeModel>> GetAddressObjectTypes(int level);
        Task<IEnumerable<AddressEstateStatus>> GetEstateStatuses();
        Task<IEnumerable<AddressStructureStatus>> GetStructureStatuses();
        AddressObject GetAddressObject(Guid aoId);
        AddressEntity GetAddressEntity(long baId);
        Task<IEnumerable<AddressComparisonsModel>> GetAddressUnsettleComparison();

        Task<AddressUpdateResult> AddressUpdate(long? baId, string name, Guid aoId, Guid? houseId, Guid? steadId,
            string location,
            string flatNum, int? flatType, string sPOBox, bool isBuild, bool isActual, AddressSourceType sourceType);

        Task<AddressObjectUpdateResult> AddressObjectUpdate(AddressObjectUpdate model);
        Task<HouseUpdateResult> HouseUpdate(HouseUpdate model);
        Task<SteadUpdateResult> SteadUpdate(SteadUpdate model);
        IEnumerable<AddressHierarchyResponse> GetAddressHierarchy(long baId);
        Task<bool> AddressSettle(IEnumerable<AddressSettleRequestModel> model);
        bool BindHouses(BindHouseModel model);
    }
}

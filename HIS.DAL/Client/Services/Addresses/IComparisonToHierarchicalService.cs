﻿using System.Collections.Generic;
using HIS.Models.Layer.Models.Address;

namespace HIS.DAL.Client.Services.Addresses
{
    public interface IComparisonToHierarchicalService
    {
        IEnumerable<AddressComparisonsDpModel> Convert(IEnumerable<AddressComparisonsModel> items);
    }
}

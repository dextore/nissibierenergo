﻿using HIS.DAL.Client.Models.Elements;
using HIS.DAL.Client.Models.RegPoints;
using System.Collections.Generic;

namespace HIS.DAL.Client.Services.ConPoints
{
    public interface IConPointService
    {
        IEnumerable<Element> Get();
        Element GetItem(long baId);
        Element Save(ElementRet regPoint);
        IEnumerable<ElementExt> GetConPoints();
        ElementExt GetConPoint(long baId);
    }
}

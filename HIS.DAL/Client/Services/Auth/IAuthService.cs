﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Auth;

namespace HIS.DAL.Client.Services.Auth
{
    /// <summary>
    /// Сервис аутентификации/авторизации
    /// </summary>
    public interface IAuthService
    {

        /// <summary>
        /// Проверка прав пользователя
        /// </summary>
        /// <param name="strRights">Права, перечисленные в строке через запятую: "righth1,right2"</param>
        /// <returns>Результат проверки наличия всех заданных прав у пользователя</returns>
        bool CheckRights(string strRights);

        /// <summary>
        /// Проверка наличия ролей пользователя
        /// </summary>
        /// <param name="strRoles">Роли, перечисленные в строке через запятую: "role1,role2"</param>
        /// <returns>Результат проверки наличия заданных ролей у пользователя</returns>
        bool CheckRoles(string strRoles);

        /// <summary>
        /// Проверка наличия пользователей в БД авторизации
        /// </summary>
        /// <param name="strUsers">Пользователи, перечисленные в строке через запятую: "user1,user2"</param>
        /// <returns>Результат проверки наличия пользователей в БД авторизации</returns>
        bool CheckUsers(string strUsers);

        /// <summary>
        /// Получение текущего пользователя
        /// </summary>
        /// <returns></returns>
        int GetCurrentUserId();

        /// <summary>
        /// Список БД для авторизации
        /// </summary>
        /// <returns></returns>
        IEnumerable<AreaDataBase> GetDataBaseAreas();

        /// <summary>
        /// Проверка пользователя на ба авторизации
        /// </summary>
        /// <param name="id">ИД пользователя</param>
        /// <param name="dbAlias">Алиас БД авторизации</param>
        /// <returns></returns>
        bool CheckUser(int id, string dbAlias);
    }
}

﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.AdminPanel;

namespace HIS.DAL.Client.Services.AdminPanel
{
    public interface IAdminService
    {
        IEnumerable<BaTypesView> GetEntities();
        IEnumerable<MarkersAdminView> GetMarkers(int? baTypeId);
        IEnumerable<OnlyMarkersDataView> GetMarkers();
        IEnumerable<ItemTableView> GetAvailableValuesForList(int? markerId);
        IEnumerable<ItemTableView> GetAvailableValuesForReference(int? markerId);
        IEnumerable<BaTypesStatesView> GetAvailableSteteForEntiti(int? baTypeId);
        IEnumerable<BaTypesOperationsView> GetAvailableOperationsForEntiti(int? baTypeId);
        IEnumerable<BaTypesOperationsStateView> GetAvailablOperationsStateForEntiti(int? baTypeId);
        BaTypesView CreateUpdateEntitiType(BaTypesView view);
        OnlyMarkersDataView CreateUpdateMarker(OnlyMarkersDataView marker);
        bool DeleteMarkerFromBaType(int baId, MarkersAdminView marker);
        MarkersAdminView AddMarkerToBaType(int baId, MarkersAdminView view);
        bool CanEditLinkOnEntityMarker(int markerId);
        bool DeleteMarker(int markerId);
        BaTypesStatesCreateView CreateUpdateState(BaTypesStatesCreateView state);
        bool DeleteState(int stateId);
        BaTypesOperationsCreateView CreateUpdateOperations(BaTypesOperationsCreateView operations);
        bool DeleteOperations(int operationsId);
        IEnumerable<BaTypesStatesCreateView> GetAllStates();
        IEnumerable<BaTypesOperationsCreateView> GetAllOperations();
        ItemTableView CreateUpdateListItem(int markerId, ItemTableView listItem);
        bool DeleteListItem(int markerId, int listItemId);
        BaTypesStatesView AddStateToBaType(int baId, BaTypesStatesView state);
        bool DeleteStateFromBaType(int baId, int stateId);

        BaTypesOperationsView AddOperationToBaType(int baId, BaTypesOperationsView state);
        bool DeleteOperationFromBaType(int baId, int operationId);

        BaTypesOperationsStateView AddOperationStateToBaType(int baId, BaTypesOperationsStateCreateView operationState);
        bool DeleteOperationStateToBaType(int baId, int operationStateId, int srcStateId);
    }
}

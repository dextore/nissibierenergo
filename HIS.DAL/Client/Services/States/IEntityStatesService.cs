﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.States;

namespace HIS.DAL.Client.Services.States
{
    /// <summary>
    /// Сервис состояний сущности
    /// </summary>
    public interface IEntityStatesService
    {
        /// <summary>
        /// Возвращает коллекцию возможных состояний сущности
        /// </summary>
        /// <param name="baTypeId"></param>
        /// <returns></returns>
        IEnumerable<EntityTypeState> GetEntityTypeStates(int baTypeId);
    }
}
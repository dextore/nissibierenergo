﻿using HIS.DAL.Client.Models.Operations;
using System.Collections.Generic;
using HIS.DAL.Client.Models.BaseAncestors;

namespace HIS.DAL.Client.Services.Operations
{
    public interface IOperationsService
    {
        IEnumerable<EntityOperation> GetAllowedEntityOperations(long baId);
        IEnumerable<StateInfo> GetEntityStates(long baTypeId);
    }
}

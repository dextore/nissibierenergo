﻿using HIS.DAL.Client.Models.Markers;
using System.Collections.Generic;

namespace HIS.DAL.Client.Services.Markers
{
    public interface IMarkersService
    {
        /// <summary>
        /// Список значений маркеров сущности.
        /// </summary>
        /// <param name="baId">идентификатор сущности</param>
        /// <returns></returns>
        IEnumerable<MarkerValue> GetEntityMarkerValues(long baId, IEnumerable<int> markerIds);

        /// <summary>
        /// Список переодических маркеров сущности
        /// </summary>
        /// <param name="baTypeId">идентификатор типа сущности</param>/// 
        /// <param name="markerId">идентификатор маркера</param>
        /// <returns></returns>
        IEnumerable<PeriodicMarkerValue> GetPeriodicMarkerHistory(long baId, int markerId);
        
        /// <summary>
        /// Возвращает список значений маркера 
        /// </summary>
        /// <param name="markerId">Идентифиатор маркера</param>
        /// <returns></returns>
        IEnumerable<MarkerValueListItem> GetMarkerValueList(int markerId);

        /// <summary>
        /// Возвращает перечень значений маркеров по типу сущности 
        /// </summary>
        /// <param name="entityId">идентификатор сущности</param>
        /// <returns></returns>
        IEnumerable<EntityMarkersValueList> GetEntityMarkersValue(long baId);

        /// <summary>
        /// Обозначение для ссылочной сущности
        /// </summary>
        /// <param name="baId"></param>
        /// <returns></returns>
        EntityCaption GetEntityCaption(long baId);

        /// <summary>
        /// Возвращает список с описанием маркеров сущности
        /// </summary>
        /// <param name="baTypeId"></param>
        /// <returns></returns>
        IEnumerable<MarkerInfo> GetEntityMarkersInfo(int baTypeId);

        IEnumerable<ReferenceMarkerValueItem> GetCatalogMarkerValueList(int markerId);

        /// <summary>
        /// Получение опциональных маркеров
        /// </summary>
        /// <param name="BATypeId"></param>
        /// <param name="markerId"></param>
        /// <param name="ItemId"></param>
        /// <returns></returns>
        IEnumerable<OptionalMarkers> GetOptionalMarkers(int BATypeId, int markerId, int ItemId);

        /// <summary>
        /// Список значений маркеров сущности
        /// </summary>
        /// <param name="baId"></param>
        /// <param name="names">Фильтр на наименованиям MVCAlias</param>
        /// <returns></returns>
        IEnumerable<MarkerValue> EntityMarkerValues(long baId, ICollection<string> names);

        /// <summary>
        /// Список значений коллекций маркера
        /// </summary>
        /// <param name="baId"></param>
        /// <param name="name">Наименование маркера MVCAlias</param>
        /// <returns></returns>
        IEnumerable<MarkerValue> GetCollectibleMarkerValues(long baId, string name);

        /// <summary>
        /// Добавляет изменяет значение маркера
        /// </summary>
        /// <param name="markerValue"></param>
        /// <returns></returns>
        MarkerValueInfo SaveMarkerValue(MarkerValueSet markerValue);

        /// <summary>
        /// Сохраняет историю по переодическому маркеру
        /// </summary>
        /// <param name="markeHistoryChanges"></param>
        /// <returns></returns>
        PeriodicMarkerValue SaveMarkerHistory(MarkeHistoryChanges markeHistoryChanges);
    }
}

﻿namespace HIS.DAL.Client.Services.Markers
{
    public interface ICatalogMarkerValueItemsService
    {
        /// <summary>
        /// Для маркеров с типом 13 "Значения этого типа ссылается на справочник"
        /// Возвращает JSON строку со списком значений.
        /// </summary>
        /// <param name="markerId"></param>
        /// <returns></returns>
        string GetValueItems(int markerId);
    }
}
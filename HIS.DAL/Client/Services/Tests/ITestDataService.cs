﻿namespace HIS.DAL.Client.Services.Tests
{
    public interface ITestDataService
    {
        void UpdateTestData(bool isMessage, string value);
    }
}
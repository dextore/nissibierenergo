﻿using System;
using System.Collections.Generic;
using HIS.DAL.Client.Models.Documents;

namespace HIS.DAL.Client.Services.Documents
{
    public interface IDocumentsService
    {
        /// <summary>
        /// Иерархический список типов сущностей наследников от базового типа Document вместе с базовым типом
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentTreeItem> GetTreeItem();

        /// <summary>
        /// Иерархический список типов сущностей наследников от базового типа Document вместе с базовым типом.
        /// Возвращаются только типы у которых есть сущности подходяшие под заданный фильтр.
        /// </summary>
        /// <param name="companyAreaId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IEnumerable<DocumentTreeItem> GetFilteredTreeItems(long companyAreaId, DateTime startDate, DateTime endDate);
    }
}

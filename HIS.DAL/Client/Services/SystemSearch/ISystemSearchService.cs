﻿using HIS.DAL.Client.Models.Common;

namespace HIS.DAL.Client.Services.SystemSearch
{
    public interface ISystemSearchService
    {
        EntityInfoCollection GetSearchEntities(SystemSearchTypes searchType, string searchText);

        FullTextSearchEntityInfoItemCollection GetFullTextSearchEntities(string searchText);
    }
}

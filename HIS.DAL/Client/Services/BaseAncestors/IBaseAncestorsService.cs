﻿using HIS.DAL.Client.Models.BaseAncestors;

namespace HIS.DAL.Client.Services.BaseAncestors
{
    public interface IBaseAncestorsService
    {
        EntityState GetEntitytState(long baId);
        int StateChange(long baId, int operationId);
    }
}

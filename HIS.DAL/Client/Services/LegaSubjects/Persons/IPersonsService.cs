﻿using HIS.DAL.Client.Models.LegalSubjects;

namespace HIS.DAL.Client.Services.LegaSubjects.Persons
{
    /// <summary>
    /// Интерфейс сервиса сущности физические лица
    /// </summary>
    public interface IPersonsService
    {
        Person GetPerson(long personId);
        PersonHeader GetPersonHeader(long personId);
        long Update(PersonUpdate person);
        PersonInfoResponse GetPersonInformation(PersonInfo personInfo);
        PersonInfoResponse UpdatePersonInformation(PersonInfoUpdate personInfo);
    }
}
﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.LegalSubjects.CommercePerson;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Services.LegaSubjects.CommercePersons
{
    public interface ICommercePersonService
    {
        /// <summary>
        /// Возвращает информацию о коммерческой деятельности физ. лица
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        CommercePerson GetCommercePerson(long personId);

        /// <summary>
        /// Возвращает информацию о коммерческой деятельности физ. лица по идентификатору физ. юлица
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        CommercePerson GetOPFByParent(long parentId);

        /// <summary>
        /// Возвращает коллекцию маркеров адресов
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        // TODO Нужно перенести в сервис маркеров. 
        IEnumerable<MarkerValue> GetAddressList(long baId);

        /// <summary>
        /// Сохраняет информацию о коммерческой деятельности физ. лица
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        CommercePerson UpdateCommercePerson(CommercePersonUpdate requestModel);

        /// <summary>
        /// Сохраняет адреса  коммерческой деятельности физ. лица
        /// </summary>
        /// <param name="addresses"></param>
        /// <returns></returns>
        // TODO Нужно перенести в сервис маркеров. 
        IEnumerable<MarkerValue> SaveAddresList(long baId, IEnumerable<MarkerValue> addresses);
    }
}

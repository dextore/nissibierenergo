﻿using HIS.DAL.Client.Models.LegalSubjects;

namespace HIS.DAL.Client.Services.LegaSubjects
{
    /// <summary>
    /// Интерфейс сервиса для работы с юридическим лицом
    /// </summary>
    public interface ILegalSubjectsService
    {
        /// <summary>
        /// Форвращает запись c заданым parentId в любом состоянии кроме удален
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        LegalSubject GetByParentId(long parentId);

        /// <summary>
        /// Создание/изменение физического лица
        /// </summary>
        /// <param name="legalSubject"></param>
        /// <returns></returns>
        long Update(LegalSubject legalSubject);
    }
}

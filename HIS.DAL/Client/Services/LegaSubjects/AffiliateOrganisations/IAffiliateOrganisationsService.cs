﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.LegalSubjects.AffiliateOrganisations;

namespace HIS.DAL.Client.Services.LegaSubjects.AffiliateOrganisations
{
    public interface IAffiliateOrganisationsService
    {
        /// <summary>
        /// Возвращает список филиалов по юр. лицу.
        /// </summary>
        /// <param name="organisactionId"></param>
        /// <returns></returns>
        IEnumerable<AffiliateOrganisation> GetAffiliateOrganisations(long organisactionId);

        /// <summary>
        /// Филиал юр. лица
        /// </summary>
        /// <param name="affiliateOrganisactionId"></param>
        /// <returns></returns>
        AffiliateOrganisation GetAffiliateOrganisation(long affiliateOrganisactionId, IEnumerable<int> markersIds);

        /// <summary>
        /// Сохраняет изменения физ. лица.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        AffiliateOrganisation Update(AffiliateOrganisation model);
    }
}

﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.BankAccounts;

namespace HIS.DAL.Client.Services.LegaSubjects.BankAccounts
{
    /// <summary>
    /// Сервис счетов 
    /// </summary>
    public interface ILSBankAccountService
    {
        /// <summary>
        /// Возвращает список расчетных счетов субъекта права
        /// </summary>
        /// <param name="baId"></param>
        /// <returns></returns>
        IEnumerable<LSBankAccount> GetLSBankAccounts(long baId);

        /// <summary>
        /// Возвращает список расчетных счетов субъекта права
        /// </summary>
        /// <param name="baId"></param>
        /// <returns></returns>
        IEnumerable<LSBankAccount> GetAllLSBankAccounts(long baId);

        /// <summary>
        /// Возвращает расчетныы счет субъекта права
        /// </summary>
        /// <param name="baId"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        LSBankAccount GetLSBankAccount(long baId, int itemId);

        /// <summary>
        /// Создает / изменяен расчетный счет субъекта права
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        LSBankAccount Update(LSBankAccount model);

        /// <summary>
        /// Удалить смаркер счета. (Удаляется связь счета с субъектом права, счет остается!!!)
        /// </summary>
        /// <param name="legalSubjectId"></param>
        /// <param name="markerId"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        bool Remove(long baId, int itemId);
    }
}

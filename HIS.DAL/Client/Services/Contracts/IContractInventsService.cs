﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Contracts;

namespace HIS.DAL.Client.Services.Contracts
{
    public interface IContractInventsService
    {
        IEnumerable<ContractInvent> GetInvents(long contractId, IEnumerable<int> states);
        ContractInvent GetInventItem(long baId);
    }
}
﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Contracts;

namespace HIS.DAL.Client.Services.Contracts
{
    public interface ILSContractsService
    {
        IEnumerable<LSContract> ByLegalSubject(long lsId);
    }
}
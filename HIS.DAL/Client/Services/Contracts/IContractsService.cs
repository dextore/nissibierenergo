﻿using HIS.DAL.Client.Models.Contracts;
using System.Collections.Generic;

namespace HIS.DAL.Client.Services.Contracts
{
    public interface IContractsService
    {

        /// <summary>
        /// Возвращает список контрактов
        /// </summary>
        /// <returns></returns>
        //IEnumerable<Contract> GetContracts();

        /// <summary>
        /// Возвращает контракт по идентификатору
        /// </summary>
        /// <param name="contractId">Идентификатор контракта</param>
        /// <returns></returns>
        //ContractInfo GetContract(long contractId);

        /// <summary>
        /// Возвращает договора по контрагенту
        /// </summary>
        /// <param name="contractorId"></param>
        /// <returns></returns>
        IEnumerable<Contract> GetContractsByContractor(long contractorId);

        /// <summary>
        ///  Сохраняет изменения в контракте или создает новый 
        /// </summary>
        /// <param name="contract">Модель контракта</param>
        /// <returns></returns>
        //ContractInfo Save(Contract contract);
    }
}

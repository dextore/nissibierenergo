﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Branches;

namespace HIS.DAL.Client.Services.Branches
{
    public interface IBranchService
    {
        IEnumerable<Branch> GetBranches();
        IEnumerable<BranchItem> UpdatePersonBranches(long? comercialPersonId, IEnumerable<BranchItem> branches);
        IEnumerable<BranchItem> DeletePersonBranches(long? comercialPersonId, IEnumerable<BranchItem> branches);
        IEnumerable<BranchItem> GetPersonsBranches(long? comercialPersonId);
        string CheckBranchForCorrect(long? comercialPersonId, BranchItem branch);
    }
}

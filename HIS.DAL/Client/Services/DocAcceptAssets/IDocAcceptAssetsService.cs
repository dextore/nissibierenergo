﻿
using HIS.DAL.Client.Models.DocAcceptAssets;
using System.Collections.Generic;
using HIS.DAL.DB.Context;

namespace HIS.DAL.Client.Services.DocAcceptAssets
{
    public interface IDocAcceptAssetsService
    {

        DocAcceptAssetsData GetDocAcceptAssets(long baId);
        IEnumerable<DocAcceptAssetsRow> GetDocAcceptAssetsRows(long docAcceptId);
        DocAcceptAssetsRow GetDocAcceptAssetsRow(long docAcceptId, long recId);
        IEnumerable<ReceiptInvoiceSelect> GetReceiptInvoiceSelect(long caId);
        IEnumerable<ReceiptInvoiceInventSelect> GetReceiptInvoiceInventSelect(long docAcceptId);
        long UpdateDocAcceptAssets(DocAcceptAssetsData saveData);
        DocAcceptAssetsRow UpdateDocAcceptAssetRow(DocAcceptAssetsRow model);
        bool DeleteDocAcceptAssetRow(DocAcceptAssetsRow model);
        bool IsHasSpecification(long baId);
    }
}
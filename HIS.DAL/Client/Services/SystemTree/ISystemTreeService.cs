﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Common;
using HIS.DAL.Client.Models.SystemTree;

namespace HIS.DAL.Client.Services.SystemTree
{
    public interface ISystemTreeService
    {
        /// <summary>
        /// Получение коллекции элементов дерева
        /// </summary>
        /// <param name="treeName">Наименование типа дерева</param>
        /// <param name="entityId">Идентификатор сущности (если NULL, то возвращать список сущностей корневого уровня)</param>
        /// <param name="entityParentId">Идентификатор родительской сущности</param>
        /// <param name="isParent">Признак родительского элемента (т.е. разворот дерева необходимо сформировать "вверх" от заданного элемента до корневого элемента)</param>
        /// <returns></returns>
        EntityInfoTreeCollection GetTreeEntities(string treeName, long? entityId, long? entityParentId, long? parentBaId, bool? isParent = false);

        SystemTreeItem GetTreeItem(long baId);
        IEnumerable<SystemTreeItem> GetTreeItemChildren(long baId);
    }
}

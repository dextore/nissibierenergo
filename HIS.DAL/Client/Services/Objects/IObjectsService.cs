﻿using HIS.DAL.Client.Models.Elements;
using HIS.DAL.Client.Models.RegPoints;
using System.Collections.Generic;

namespace HIS.DAL.Client.Services.Objects
{
    public interface IObjectsService
    {
        IEnumerable<Element> Get();
        Element GetItem(long baId);
        Element Save(ElementRet regPoint);
        IEnumerable<ElementExt> GetObjects();
        ElementExt GetObject(long baId);
    }
}

﻿using HIS.DAL.Client.Models.SystemSearch;

namespace HIS.DAL.Client.Services.UniversalSearch
{
    public interface IUniversalSearchService
    {
        /// <summary>
        /// Поиск по наименованию
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="name"></param>
        /// <param name="showDeleted"></param>
        /// <returns></returns>
        SearchResult SearchByName(long from, long to, string name, bool showDeleted, int? baTypeId);

        /// <summary>
        /// Поиск по ИНН
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="name"></param>
        /// <param name="showDeleted"></param>
        /// <param name="baTypeId"></param>/// 
        /// <returns></returns>
        SearchResult SearchByInn(long from, long to, string name, bool showDeleted, int? baTypeId);

    }
}
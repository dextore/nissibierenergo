﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Banks;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Services.Bank
{
    public interface IBanksService
    {
        BanksEntity Update(BanksEntity entity, int userId);
        BanksEntity GetBank(long? baId);
        IEnumerable<BanksEntity> GetBanks();
        IEnumerable<BanksEntity> GetBanksBranches(long? baId);
        IEnumerable<PeriodicMarkerValue> GetPeriodicBanksMarkerHistory(long baId, int markerId);
        PeriodicMarkerValue SaveMarkerValue(MarkeHistoryChanges marker);
        string GetBankHeader(long baId);
    }
}

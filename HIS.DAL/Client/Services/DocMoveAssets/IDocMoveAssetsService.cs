﻿
using HIS.DAL.Client.Models.DocMoveAssets;
using System.Collections.Generic;

namespace HIS.DAL.Client.Services.DocMoveAssets
{
    public interface IDocMoveAssetsService
    {
        DocMoveAssetsData GetDocMoveAssetsData(long baId);
        bool IsHasAssets(long baId);
        long UpdateDocMoveAssets(DocMoveAssetsData saveData);
        DocMoveAssetsRow GetDocMoveAssetsRow(long docMoveId, long recId);
        IEnumerable<DocMoveAssetsRow> GetDocMoveAssetsRows(long docMoveId);
        DocMoveAssetsRow UpdateDocMoveAssetRow(DocMoveAssetsRow row);
        bool DeleteDocMoveAssetRow(DocMoveAssetsRow model);
        AssetsData GetAssetsInfo(long baId);
    }
}

﻿using HIS.DAL.Client.Models.Nomenclatures;
using HIS.DAL.DB;
using System.Collections.Generic;
using System.Linq;

namespace HIS.DAL.Client.Services.Nomenclatures
{
    public interface INomenclatureService
    {
        /// <summary>
        /// Возвращает номенклатуру
        /// </summary>
        /// <returns></returns>
        IEnumerable<Nomenclature> GetNomenclatures();

        /// <summary>
        /// Добавляет изменняет позицию номенклатуры
        /// </summary>
        /// <param nomenclature>Номенклатура модель</param>
        /// <returns></returns>
        Nomenclature Save(NomenclatureSave nomenclature);
        
        IQueryable<Nomenclature> SelectNomenclatures(IDataContextHandlerProcess process);
    }
}

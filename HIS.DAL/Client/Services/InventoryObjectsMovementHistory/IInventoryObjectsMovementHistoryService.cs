﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.InventoryObjectsMovementHistory;

namespace HIS.DAL.Client.Services.InventoryObjectsMovementHistory
{
    public interface IInventoryObjectsMovementHistoryService
    {
        IEnumerable<InventoryAssets> GetFaAssetesList();
        IEnumerable<InventoryAssetsMovementHistory> GetInventoryAssetsMovementHistory(long inventoryAssetsId);
    }

}

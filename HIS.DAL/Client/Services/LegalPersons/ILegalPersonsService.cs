﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.LegalSubjects;
using HIS.DAL.Client.Models.LegalSubjects.Persons;
using HIS.DAL.Client.Models.Markers;

namespace HIS.DAL.Client.Services.LegalPersons
{
    public interface ILegalPersonsService
    {
        /// <summary>
        /// Получение модели юридического лица с маркерами
        /// </summary>
        /// <param name="id">идентификатор юридического лица</param>
        /// <returns></returns>
        LegalPerson GetLegalPerson(long id, IEnumerable<int> markersIds);

        /// <summary>
        /// Юридическое лицо по идентификатору филиала
        /// </summary>
        /// <param name="affiliateId"></param>
        /// <returns></returns>
        LegalPerson GetParentLegalPerson(long affiliateId);

        /// <summary>
        /// Изменяет данные или создает новое юридическое лицо в зависимости от поля LegalPersonId
        /// </summary>
        /// <param name="legalPersonUpdate">модель с данными юридического лица</param>
        /// <returns></returns>
        LegalPersonImplemented Save(LegalPerson legalPerson);

        PersonInfoResponse UpdatePersonInformation(long legalPersonId,IEnumerable<MarkerValue> model);

        PersonInfoResponse GetPersonInformation(long legalPersonId, IEnumerable<long> markersIds);
    }
}

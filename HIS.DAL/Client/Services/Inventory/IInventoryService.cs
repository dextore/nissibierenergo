﻿using HIS.DAL.Client.Models.Inventory;
using System.Collections.Generic;

namespace HIS.DAL.Client.Services.Inventory
{
    public interface IInventoryService
    {
        long InventoryUpdate(InventoryData inventory);
        IEnumerable<IInventory> GetInventories();
        IEnumerable<IInventory> GetInventoriesByCompanyId(long companyId);
        IEnumerable<InventoryListItem> GetInventoriesList(InventoryListFilters filters);
        InventoryData GetInventoryDataModel(long BAId);
        IEnumerable<LSCompanyAreas> GetLSCompanyAreas();
        IEnumerable<RefSysUnits> GetRefSysUnits();
        IEnumerable<IGroups> GetInventoriesGroup();
    }
}

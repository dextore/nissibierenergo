﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.InventoryTransactions;

namespace HIS.DAL.Client.Services.InventoryTransactions
{
    public interface IInventoryTransactionsService
    {
        IEnumerable<InventoryTransaction> GetByDocument(long baId);
    }
}

﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Common;

namespace HIS.DAL.Client.Services.Common
{
    /// <summary>
    /// Сервис общей функциональности системы
    /// </summary>
    public interface ICommonService
    {
        /// <summary>
        /// Получение информации о сущености
        /// </summary>
        /// <param name="entityId">ИД сущности</param>
        /// <returns>Модель сущности, либо null, если информация по сущности не определена</returns>
        EntityInfoItem GetEntityInfo(long entityId);

        /// <summary>
        /// Возвращает информацию о сущностях
        /// </summary>
        /// <returns></returns>
        IEnumerable<EntityInfo> GetEntitiesInfo();

        /// <summary>
        /// Получение наименования сущности
        /// </summary>
        /// <param name="entityId">ИД сущности</param>
        /// <returns>Наименование</returns>
        string GetEntityName(long entityId);

        /// <summary>
        /// Получение полного наименования сущности
        /// </summary>
        /// <param name="entityId">ИД сущности</param>
        /// <returns>Полное наименование</returns>
        string GetEntityFullName(long entityId);

        /// <summary>
        /// Получение описания имени сущности
        /// </summary>
        /// <param name="entityId">ИД сущности</param>
        /// <returns>Описание</returns>
        string GetEntityDescription(long entityId);

        IEnumerable<string> CheckErrors(long baId);
        IEnumerable<EntityViewItem> GetEntityViewsItems(int baTypeId);
    }
}

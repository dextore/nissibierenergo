﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Common;

namespace HIS.DAL.Client.Services.Common
{
    /// <summary>
    /// Интерфейс для работы с главным меню 
    /// </summary>
    public interface IMainMenuService
    {
        /// <summary>
        /// Получение дерева меню
        /// </summary>
        /// <returns></returns>
        IEnumerable<MenuItem> GetMenu();
    }
}

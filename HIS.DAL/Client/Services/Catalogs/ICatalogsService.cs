﻿using System.Collections.Generic;
using HIS.DAL.Client.Models.Catalogs;

namespace HIS.DAL.Client.Services.Catalogs
{
    public interface ICatalogsService
    {
        IEnumerable<OrganisationLegalForm> OrganisationLegalForms();
        IEnumerable<PersonLegalForm> PersonLegalForms();
        IEnumerable<FALocationsForm> FALocations();
        IEnumerable<FAMovReasonsForm> FAMovReasons();

    }
}

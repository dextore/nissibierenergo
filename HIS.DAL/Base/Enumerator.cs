﻿using System.Collections;
using System.Collections.Generic;

namespace HIS.DAL.Base
{
    /// <summary>
    /// Клас шаблона перечислителя
    /// </summary>
    /// <typeparam name="T">Тип перечисляемого значения</typeparam>
    public class Enumerator<T>: IEnumerator<T>
    {
        private readonly List<T> items;
        private int position;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="itemsList"></param>
        public Enumerator(List<T> itemsList)
        {
            items = itemsList;
            position = -1;
        }

        /// <summary>
        /// Текущий элемент
        /// </summary>
        public T Current => items[position];

        object IEnumerator.Current => Current;       

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            if (++position >= items.Count)
            {
                return false;
            }

            return true;
        }

        public void Reset()
        {
            position = -1;
        }
    }
}

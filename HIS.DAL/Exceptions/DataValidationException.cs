﻿using System;

namespace HIS.DAL.Exceptions
{
    public class DataValidationException : Exception
    {
        public DataValidationException(string message) : base(message)
        {

        } 
    }
}

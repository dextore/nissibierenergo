﻿using HIS.Auth.DBContext;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HIS.Auth.Entities
{
    public class HISRoleStore : RoleStore<HISRole, int, HISUserRole>
    {
        public HISRoleStore(AuthDataBaseContext context) : base(context)
        {
        }
    }
}
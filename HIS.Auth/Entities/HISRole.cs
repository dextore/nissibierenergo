﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HIS.Auth.Entities
{
    /// <summary>
    /// Роль пользователя в системе
    /// </summary>
    public class HISRole : IdentityRole<int, HISUserRole>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public HISRole(): base()
        {
        }

        /// <summary>
        /// Конструкор 
        /// </summary>
        /// <param name="name">Наименование роли</param>
        /// <param name="description">Описание роли</param>
        public HISRole(string name, string description)
        {
            Name = name;
            Description = description;
        }                                    

        /// <summary>
        /// ИД родительской роли
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// Родительская роль
        /// </summary>        
        public HISRole Parent { get; set; }

        /// <summary>
        /// Вложенные роли
        /// </summary>
        public virtual ICollection<HISRole> Roles { get; set; }

        /// <summary>
        /// Описание роли
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Права данной роли
        /// </summary>
        public virtual ICollection<HISRight> Rights { get; set; }
    }
}
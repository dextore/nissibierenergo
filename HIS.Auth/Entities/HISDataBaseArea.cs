﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HIS.Auth.Entities
{
    /// <summary>
    /// Области 
    /// </summary>
    public class HISDataBaseArea
    {
        /// <summary>
        /// ИД базы данных
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DBAreaId { get; set; }

        /// <summary>
        /// Алиас
        /// </summary>
        [Index(IsUnique = true)]
        [StringLength(50)]
        public string Alias { get; set; }

        /// <summary>
        /// Наименование БД
        /// </summary>
        public string Name { get; set; }        


        /// <summary>
        /// Описание БД
        /// </summary>
        public string Desc { get; set; }

        /// <summary>
        /// Строка подключения к БД 
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Пользователи данной БД
        /// </summary>
        public virtual ICollection<HISUser> Users { get; set; }
    }
}
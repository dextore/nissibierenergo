﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.Cookies;

namespace HIS.Auth.Entities
{
    /// <summary>
    /// Системный пользователь
    /// </summary>
    public class HISUser: IdentityUser<int, HISUserLogin, HISUserRole, HISUserClaim>
    {
        /// <summary>
        /// ФИО пользователя
        /// </summary>
        public HISUserFIO UserFIO { get; set; }

        /// <summary>
        /// Описание пользователя 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Номер группы пользователя (для возможности объединений пользователей в единую группу)
        /// </summary>
        public int? GroupNum { get; set; }

        /// <summary>
        /// Признак главного пользователя (от имени которого происходят все действия) при объединении пользователей в группу
        /// </summary>
        public bool? IsMainInGroup { get; set; }               

        /// <summary>
        /// Признак доменного пользователя
        /// </summary>
        public bool IsDomainUser { get; set; }

        /// <summary>
        /// Группы, в которых учавствует пользователь
        /// </summary>
        //public virtual ICollection<HISGroupUser> Groups { get; set; }

        /// <summary>
        /// БД на которые транслируется данный пользователь 
        /// </summary>
        public virtual ICollection<HISDataBaseArea> DataBaseAreas { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(HISUserManager manager, CookieValidateIdentityContext context)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            // Add custom user claims here            
            Claim dbAliasClaim = userIdentity.Claims.FirstOrDefault(c => c.Type.Equals("DBAlias"));

            if (dbAliasClaim == null)
            {
                dbAliasClaim = context.Identity.Claims.FirstOrDefault(c => c.Type.Equals("DBAlias"));
                if (dbAliasClaim != null)
                {
                    userIdentity.AddClaim(dbAliasClaim);
                }
            }

            return userIdentity;
        }
    }
}
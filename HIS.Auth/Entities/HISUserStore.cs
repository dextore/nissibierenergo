﻿using HIS.Auth.DBContext;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HIS.Auth.Entities
{
    public class HISUserStore : UserStore<HISUser, HISRole, int, HISUserLogin, HISUserRole, HISUserClaim>
    {
        public HISUserStore(AuthDataBaseContext context) : base(context)
        {
        }
    }
}
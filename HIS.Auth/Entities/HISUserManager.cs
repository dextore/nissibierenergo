﻿using HIS.Auth.DBContext;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace HIS.Auth.Entities
{
    public class HISUserManager : UserManager<HISUser, int>
    {
        public HISUserManager(IUserStore<HISUser, int> store) : base(store)
        {
        }

        public static HISUserManager Create(IdentityFactoryOptions<HISUserManager> options, Microsoft.Owin.IOwinContext context)
        {
            AuthDataBaseContext db = context.Get<AuthDataBaseContext>();            
            HISUserManager manager = new HISUserManager(new HISUserStore(db));

            // Конфигурация валидации пользователя
            manager.UserValidator = new HISUserValidator(manager);

            // Конфигурация логики валидации пароля пользователя
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };            

            return manager;
        }
    }
}
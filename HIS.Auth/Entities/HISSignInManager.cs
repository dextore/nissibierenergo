﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;

namespace HIS.Auth.Entities
{
    /// <summary>
    /// Менеджер входа в систему
    /// </summary>
    public class HISSignInManager : SignInManager<HISUser, int>
    {
        private readonly IOwinContext _owinContext;

        public HISSignInManager(HISUserManager userManager, IOwinContext context)
            : base(userManager, context.Authentication)
        {
            _owinContext = context;
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(HISUser user)
        {
            return user.GenerateUserIdentityAsync((HISUserManager)UserManager, new CookieValidateIdentityContext(_owinContext, new AuthenticationTicket(new ClaimsIdentity(), new AuthenticationProperties()), new CookieAuthenticationOptions() ));
        }

        public static HISSignInManager Create(IdentityFactoryOptions<HISSignInManager> options, IOwinContext context)
        {
            return new HISSignInManager(context.GetUserManager<HISUserManager>(), context);
        }


        /// <summary>
        /// Аутентификация пользователя по расширенной аутентификации (доменной)
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <param name="dbAlias">Псевдоним БД авторизации</param>
        /// <param name="isPersistent"></param>
        /// <returns></returns>
        public async Task<SignInStatus> ExternalSignInAsync(ExternalLoginInfo loginInfo, string dbAlias, bool isPersistent)
        {        
            HISUser user = await UserManager.FindAsync(loginInfo.Login);
            if (user == null) return SignInStatus.Failure;
            user.Claims.Add(new HISUserClaim() { ClaimType = "DBAlias", ClaimValue = dbAlias });
            _owinContext.Set<string>("AuthDBAlias", dbAlias);
          
            return await ExternalSignInAsync(loginInfo, isPersistent);
           
        }

        /// <summary>
        /// Аутентификация пользователя по логину и паролю
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <param name="password">Пароль</param>
        /// <param name="dbAlias">Псевдоним БД авторизации</param>
        /// <param name="isPersistent"></param>
        /// <param name="shouldLockout"></param>
        /// <returns></returns>
        public async Task<SignInStatus> PasswordSignInAsync(string userName, string password, string dbAlias, bool isPersistent, bool shouldLockout)
        {            
            HISUser user = await UserManager.FindAsync(userName, password);
            if (user == null) return SignInStatus.Failure;
            user.Claims.Add(new HISUserClaim() { ClaimType = "DBAlias", ClaimValue = dbAlias });
            _owinContext.Set<string>("AuthDBAlias", dbAlias);
            
            return await base.PasswordSignInAsync(userName, password, isPersistent, shouldLockout);          
        }

    }
}
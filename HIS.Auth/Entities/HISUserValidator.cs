﻿using Microsoft.AspNet.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace HIS.Auth.Entities
{
    public class HISUserValidator : UserValidator<HISUser, int>
    {
        public HISUserValidator(HISUserManager mgr) : base(mgr)
        {
            AllowOnlyAlphanumericUserNames = false;
            RequireUniqueEmail = false;
        }

        public override async Task<IdentityResult> ValidateAsync(HISUser user)
        {
            IdentityResult result = await base.ValidateAsync(user);

            if (user.IsDomainUser && string.IsNullOrEmpty(user.Description))
            {
                var errors = result.Errors.ToList();
                errors.Add("Доменный пользователь должен содержать описание.");
                result = new IdentityResult(errors);
            }

            return result;
        }
    }
}
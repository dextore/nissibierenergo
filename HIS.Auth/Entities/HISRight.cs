﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HIS.Auth.Entities
{
    /// <summary>
    /// Модель права системы
    /// </summary>
    public class HISRight
    {
        /// <summary>
        /// ИД права
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RightId { get; set; }     
                
        /// <summary>
        /// ИД родительского права
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// Родительское право
        /// </summary>        
        public HISRight Parent { get; set; }
        
        /// <summary>
        /// Вложенные права
        /// </summary>
        public virtual ICollection<HISRight> Rights { get; set; }

        /// <summary>
        /// Наименование права
        /// </summary>
        [Required]
        [MaxLength(256)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        /// <summary>
        /// Описание права
        /// </summary>
        public string Description { get; set; }        

        /// <summary>
        /// Роли, в которые входит данное право
        /// </summary>
        public virtual ICollection<HISRole> Roles { get; set; }

    }
}
﻿using System.ComponentModel.DataAnnotations.Schema;

namespace HIS.Auth.Entities
{
    /// <summary>
    ///  ФИО исполнителя системы
    /// </summary>
    public class HISUserFIO
    {

        /// <summary>
        /// Конструктор
        /// </summary>
        public HISUserFIO()
        {
            
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="lastName">Фамилия</param>
        /// <param name="firstName">Имя</param>
        /// <param name="middleName">Отчетсво</param>
        public HISUserFIO(string lastName, string firstName, string middleName)
        {
            LastName = lastName;
            FirstName = firstName;
            MiddlName = middleName;
        }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; } = string.Empty;

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; } = string.Empty;

        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddlName { get; set; } = string.Empty;

        /// <summary>
        /// Полное наименование 
        /// </summary>
        [NotMapped]
        public string FullName => ToString();

        /// <summary>
        /// Короткое наименование
        /// </summary>
        [NotMapped]
        public string ShortName {
            get
            {
                var lName = string.IsNullOrEmpty(LastName.Trim()) ? string.Empty : LastName;
                var fName = string.IsNullOrEmpty(FirstName.Trim()) ? string.Empty : FirstName;
                var mName = string.IsNullOrEmpty(MiddlName.Trim()) ? string.Empty : MiddlName;

                if (string.IsNullOrEmpty(lName))
                {
                    fName = string.Empty;
                    mName = string.Empty;
                }

                if (string.IsNullOrEmpty(fName))
                {
                    mName = string.Empty;
                }

                if (!string.IsNullOrEmpty(lName))
                {
                    lName = $"{lName} ";
                }

                if (!string.IsNullOrEmpty(fName))
                {
                    fName = $"{fName[0]}. ";
                }

                if (!string.IsNullOrEmpty(mName))
                {
                    mName = $"{mName[0]}.";
                }

                return $"{lName}{fName}{mName}";
            }
        }

        public override string ToString()
        {
            return $"{LastName} {FirstName} {MiddlName}";
        }        
    }
}
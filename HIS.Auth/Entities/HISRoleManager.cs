﻿using HIS.Auth.DBContext;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace HIS.Auth.Entities
{
    /// <summary>
    /// Менеджер ролей пользователя
    /// </summary>
    public class HISRoleManager : RoleManager<HISRole, int>
    {
        public HISRoleManager(IRoleStore<HISRole, int> store) : base(store)
        {
        }

        public static HISRoleManager Create(IdentityFactoryOptions<HISRoleManager> options, Microsoft.Owin.IOwinContext context)
        {
            return new HISRoleManager(new HISRoleStore(context.Get<AuthDataBaseContext>()));
        }
    }
}
﻿using System.Collections.Generic;
using System.Data.Entity;
using HIS.Auth.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HIS.Auth.DBContext
{
    /// <summary>
    /// Контекст БД аутентификации
    /// </summary>
    public class AuthDataBaseContext : IdentityDbContext<HISUser, HISRole, int, HISUserLogin, HISUserRole, HISUserClaim>
    {

        public AuthDataBaseContext() : base("HISAuthConnection")
        {
        }
       
        public static AuthDataBaseContext Create()
        {
            return new AuthDataBaseContext();                     
        }

        /// <summary>
        /// Права системы
        /// </summary>
        public DbSet<HISRight> Rights { get; set; }        

        /// <summary>
        /// Базы данных 
        /// </summary>
        public DbSet<HISDataBaseArea> DataBaseAreas { get; set; }   


        static AuthDataBaseContext()
        {
            Database.SetInitializer<AuthDataBaseContext>(new IdentityDbInit());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<HISUser>().ToTable("HISUsers");
            modelBuilder.Entity<HISUserRole>().ToTable("HISUserRoles");
            modelBuilder.Entity<HISUserLogin>().ToTable("HISUserLogins");
            modelBuilder.Entity<HISUserClaim>().ToTable("HISUserClaims");
            modelBuilder.Entity<HISRole>().ToTable("HISRoles");

            modelBuilder.Entity<HISRole>()
                .HasMany(c => c.Rights)
                .WithMany(p => p.Roles)
                .Map(m =>
                {
                    m.ToTable("HISRoleRights");
                    m.MapLeftKey("RoleId");
                    m.MapRightKey("RightId");
                });

            modelBuilder.Entity<HISRight>()
                .HasMany(c => c.Rights)
                .WithOptional(o => o.Parent)
                .HasForeignKey(s => s.ParentId);

            modelBuilder.Entity<HISRole>()
                .HasMany(c => c.Roles)
                .WithOptional(o => o.Parent)
                .HasForeignKey(s => s.ParentId);

            modelBuilder.Entity<HISDataBaseArea>()
                .HasMany(c => c.Users)
                .WithMany(p => p.DataBaseAreas)
                .Map(m =>
                {
                    m.ToTable("HISDataBaseAreaUsers");
                    m.MapLeftKey("DBAreaId");
                    m.MapRightKey("UserId");
                });            
        }
    }

    /// <summary>
    /// Класс инициализации контекста БД (первоначальные настройки ролей/пользователей и т.п.)
    /// </summary>
    public class IdentityDbInit : DropCreateDatabaseIfModelChanges<AuthDataBaseContext>
    {
        protected override void Seed(AuthDataBaseContext context)
        {
            PerformInitialSetup(context);
            base.Seed(context);
        }
        public void PerformInitialSetup(AuthDataBaseContext context)
        {
            var userManager = new HISUserManager(new HISUserStore(context));
            var roleManager = new HISRoleManager(new HISRoleStore(context));

            var rolePublic = new HISRole("public", "Публичная роль"); // Данная роль должна быть у каждого пользователя системы 

            var roleSysAdmin = new HISRole("sysadmin", "Системный администратор");
            var roleAccountAdmin = new HISRole("accountadmin", "Администратор пользователей");
            var roleAdmins = new HISRole("administrators", "Администраторы")
            {
                Roles = new List<HISRole>()
                {
                    roleSysAdmin,
                    roleAccountAdmin
                }
            };          

            // Добавление ролей 
            roleManager.Create(rolePublic);
            roleManager.Create(roleAdmins);

            HISDataBaseArea hisFirstDBArea = new HISDataBaseArea()
            {               
                Alias = "nskes",
                Name = "ОАО \"Новосибирскэнергосбыт\"",
                Desc = "БД деятельности компании ОАО \"Новосибирскэнергосбыт\"",
                ConnectionString = "data source=srv-sqltest;initial catalog=NewHermesModel;integrated security=False;user id=test;password=test;multipleactiveresultsets=True;application name=EntityFramework"
            };

            HISDataBaseArea hisTestDBArea = new HISDataBaseArea()
            {
                Alias = "nskes_test",
                Name = "ОАО \"Новосибирскэнергосбыт\" (ТЕСТ)",
                Desc = "Тестовая БД деятельности компании ОАО \"Новосибирскэнергосбыт\"",
                ConnectionString = "data source=srv-sqltest;initial catalog=NewHermesModel;integrated security=False;user id=test;password=test;multipleactiveresultsets=True;application name=EntityFramework"
            };

            context.DataBaseAreas.Add(hisFirstDBArea);
            context.DataBaseAreas.Add(hisTestDBArea);

            var userAdmin = new HISUser()
            {
                UserName = "Admin",
                Description = "Административный пользователь",
                Email = "KachevIV@nskes.ru",
                IsDomainUser = false,
                EmailConfirmed = true,
                UserFIO = new HISUserFIO()
                {
                    FirstName = "Администратор",
                    LastName = string.Empty,
                    MiddlName = string.Empty
                },
                DataBaseAreas = new List<HISDataBaseArea>() { hisFirstDBArea, hisTestDBArea } 
            };

            // Создания административного пользователя
            var reslut = userManager.Create(userAdmin, "Pa$$w0rd");

            if (reslut.Succeeded)
            {
                // Добавление публичной роли пользователю
                userManager.AddToRole(userAdmin.Id, rolePublic.Name);

                // Добавление административной роли административному пользователю
                userManager.AddToRole(userAdmin.Id, roleSysAdmin.Name);
                userManager.AddToRole(userAdmin.Id, roleAccountAdmin.Name);                
            }

            context.Rights.Add(new HISRight()
            {
                Name = "AccountUsers",
                Description = "Группа прав работы с пользователями системы",
                Rights = new List<HISRight>()
                {
                    new HISRight()
                    {
                        Name = "CreateUser",
                        Description = "Создание пользователя в системе",
                        Roles = new List<HISRole>() { roleAccountAdmin }
                    },
                    new HISRight()
                    {
                        Name = "BlockUser",
                        Description = "Блокирование пользователя системы",
                        Roles = new List<HISRole>() { roleAccountAdmin }
                    }
                }
            });            
        }
    }
}